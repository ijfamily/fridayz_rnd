<?php
/**
 * Gear CMS
 *
 * Пакет украинской локализации
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Language
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // компонент
    'setting component' => 'Настройка компонента',
    'properties' => 'Свойства',

    'The data has already been sent' => 'Дані вже були відправлені!',
    'You too often sending messages' => 'Ви занадто часто відправляэте повідомлення!',
    'To work correctly, the application must enable cookies' => 'Для коректної роботи програми необхідно включити cookies',
    'On this page is limited number of outgoing messages'    => 'На цій сторінці обмежена кількість відправлених повідомлень',
    'Your message recognized as spam and will not be accepted more' => 'Ваше повідомлення розпізнано як СПАМ і більше прийматися не будуть',
    'Unable to send a message, a server error' => 'Неможливо відправити повідомлення, помилка сервера',
    'E-mail' => 'E-mail',
    'User name' => 'Iм\'я користувача',
    'Address' => 'Адреса',
    'Phone' => 'Телефон',
    'Password' => 'Пароль',
    'You must fill the field "%s"' => 'Необхідно заповнити поле "%s"',
    'You did not fill the field "%s"' => 'Ви неправильно заповнили поле"%s"',
    'The length of the field "%s" must be from %s characters' => 'Довжина значення поля "%s", повинна бути від %s-і символів!',
    'The max length of the field "%s" must be no more than %s characters' => 'Максимальна довжина "%s", не повинна перевищувати %s-і символів!',
    'Error processing forms' => 'Помилка обробки форми!',
    'Unauthorized access attempt' => 'Спроба несанкціонованого доступу до системи (підміна даних)!',
    'Password confirmation does not match' => 'Підтвердження пароля не збігається',
    'Change the data was successful' => 'Зміна даних виконано успішно',
    'A user with the name "%s" already exists' => 'Користувач з ім\'ям "%s" вже існує!',
    'A user with this e-mail "%s" already exists' => 'Користувач з таким e-mail адресою "%s" вже існує!',
    'An attempt to substitute the captcha code' => 'Спроба підміни коду капчі!',
    'You incorrectly entered the code from the image' => 'Ви невірно ввели код на зображенні!',
    'Recovery account' => 'Відновлення облікового запису',
    'Your account has been created successfully' => 'Ваш обліковий запис створено успішно',
    'Confirmation of registration' => 'Підтвердження реєстрації',
    'A user with this e-mail "%s" not exists' => 'Користувач з таким e-mail адресою "%s" не існує!',
    'You are in an incorrect username or password, please try again' => 'Ви неправильно ввели e-mail адресу або пароль, будь ласка спробуйте ще раз',
);
?>