<?php
/**
 * Gear CMS
 *
 * Компонент "Виджет социальной сети Twitter"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Widget
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: component.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CpWidgetTwitter
 */
Gear::library('/Components/Widget/Twitter');

/**
 * Компонент виджета социальной сети Twitter
 * 
 * @category   Gear
 * @package    Widget
 * @subpackage Twitter
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: component.php 2016-01-01 12:00:00 Gear Magic $
 */
class WidgetTwitter extends CpWidgetTwitter
{}
?>