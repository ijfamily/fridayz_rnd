<?php
/**
 * Gear CMS
 *
 * Пакет русской локализации
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Language
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // компонент
    'setting component' => 'Настройка компонента',
    'properties' => 'Свойства',

    // исключения формы
    'The data has already been sent' => 'Данные уже были отправлены!',
    'You too often sending messages' => 'Вы слишком часто отправляете сообщения!',
    'To work correctly, the application must enable cookies' => 'Для корректной работы приложения необходимо включить cookies',
    'On this page is limited number of outgoing messages'    => 'На этой странице ограничено количество отправляемых сообщений',
    'Your message recognized as spam and will not be accepted more' => 'Ваше сообщение распознано как СПАМ и больше приниматься не будут',
    'Unable to send a message, a server error' => 'Невозможно отправить сообщение, ошибка сервера',
    'Unauthorized access attempt' => 'Попытка несанкциоинированного доступа к системе (подмена данных)!',
    'Captcha code error' => 'Вы неправильно ввели код на картинке!',
    'Error processing forms' => 'Ошибка обработки формы!',

    // форма восстановления аккаунта
    'e-mail or login' => 'E-mail или логин',
    'Remind password' => 'Восстановление пароля',

    // сообщения формы
    'A user with the username or e-mail does not exist' => 'Пользователь с указанным логином или е-mail адресом не существует!'
);
?>