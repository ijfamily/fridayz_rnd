<?php
/**
 * Gear CMS
 *
 * Компонент "Форма аторизации пользователя"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Form
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: component.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CpFormSignIn, CjFormSignIn
 */
Gear::library('/Components/Form/SignIn');

/**
 * Форма аторизации пользователя
 * 
 * @category   Gear
 * @package    Form
 * @subpackage SignIn
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: component.php 2016-01-01 12:00:00 Gear Magic $
 */
class FormSignIn extends CpFormSignIn
{}

/**
 * Форма аторизации пользователя (для AJAX)
 * 
 * @category   Gear
 * @package    Form
 * @subpackage SignIn
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: component.php 2016-01-01 12:00:00 Gear Magic $
 */
class jFormSignIn extends CjFormSignIn
{}
?>