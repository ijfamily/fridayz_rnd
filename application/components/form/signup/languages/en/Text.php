<?php
/**
 * Gear CMS
 *
 * Пакет английской (британской) локализации
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Language
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // компонент
    'setting component' => 'Настройка компонента',
    'properties' => 'Свойства',

    // исключения формы
    'The data has already been sent' => 'Данные уже были отправлены!',
    'You too often sending messages' => 'Вы слишком часто отправляете сообщения!',
    'To work correctly, the application must enable cookies' => 'Для корректной работы приложения необходимо включить cookies',
    'On this page is limited number of outgoing messages'    => 'На этой странице ограничено количество отправляемых сообщений',
    'Your message recognized as spam and will not be accepted more' => 'Ваше сообщение распознано как СПАМ и больше приниматься не будут',
    'Unable to send a message, a server error' => 'Невозможно отправить сообщение, ошибка сервера',
    'Unauthorized access attempt' => 'Попытка несанкциоинированного доступа к системе (подмена данных)!',
    'Captcha code error' => 'Вы неправильно ввели код на картинке!',
    'Error processing forms' => 'Ошибка обработки формы!',

    // форма регистрации
    'User first name' => 'Имя',
    'User last name' => 'Фамилия',
    'Image code' => 'Код на картинке',
    'Select country from list' => 'Выберите из списка страну!',
    'Select region from list' => 'Выберите из списка регион!',
    'Select city from list' => 'Выберите из списка город, если его нет - заполните поле!',
    'E-mail' => 'E-mail',
    'User name' => 'Имя пользователя',
    'Address' => 'Адрес',
    'Phone' => 'Телефон',
    'Password' => 'Пароль',
    'Password confirm' => 'Подтверждение пароля',

    // сообщения формы
    'User registration' => 'Регистрация пользователя ',
    'You have successfully been registered' => 'Поздравляем, Вы успешно прошли регистрацию!',
    'You are not familiar with the site rules' => 'Вы не ознакомились с правилами сайта!',
    'Password confirmation does not match' => 'Подтверждение пароля не совпадает',
    'A user with the name "%s" already exists' => 'Пользователь с именем "%s" уже существует!',
    'A user with this e-mail "%s" already exists' => 'Пользователь с таким e-mail адресом "%s" уже существует!',
    'An attempt to substitute the captcha code' => 'Попытка подмены кода капчи!',
    'Your account has been created successfully' => 'Ваша учетная запись создана успешно',
    'Confirmation of registration' => 'Подтверждение регистрации',
    'A user with this e-mail "%s" not exists' => 'Пользователь с таким e-mail адресом "%s" не существует!',
    'You are in an incorrect username or password, please try again' => 'Вы неправильно ввели e-mail адрес или пароль, пожалуйста попробуйте еще раз',
    'User with your ip address is already registered on our site' => 'Пользователь с Вашим IP адресом уже зарегистрирован на нашем сайте!',
    'The maximum number of users exceeds the allowable limit' => 'Извините, но максимальное количество пользователей зарегистрированных на сайте превышает допустимый предел!'
);
?>