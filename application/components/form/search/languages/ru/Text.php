<?php
/**
 * Gear CMS
 *
 * Пакет русской локализации
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Language
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // компонент
    'setting component' => 'Настройка компонента',
    'properties' => 'Свойства',

    // форма поиска
    'Search this site' => 'Поиск на сайте',
    'To search you must enter a word from %s to %s characters' => 'Для поиска необходимо ввести слово от %s-х до %s-х символов',
    'next' => 'следующая',
    'last' => 'конец',
    'first' => 'начало',
    'previos' => 'предыдущая',
    'Showing records %s to %s of %s' => 'Записи с <b>%s</b> по <b>%s</b>, всего <b>%s</b>',
);
?>