<?php
/**
 * Gear CMS
 *
 * Пакет компонент "Календари"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Calendar
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: component.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

Gear::library('/Components/Calendar');

/**
 * Компонент "Календарь" (для AJAX запросов)
 * 
 * @category   Gear
 * @package    Calendar
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: component.php 2016-01-01 12:00:00 Gear Magic $
 */
class jCalendar extends CjCalendar
{}
?>