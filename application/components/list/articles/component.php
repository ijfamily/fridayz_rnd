<?php
/**
 * Gear CMS
 *
 * Компонент "Список статей"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    List
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: component.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CpListArticles, CjListArticles
 */
Gear::library('/Components/List/Articles');

/**
 * Компонент списка статей
 * 
 * @category   Gear
 * @package    List
 * @subpackage Articles
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: component.php 2016-01-01 12:00:00 Gear Magic $
 */
class ListArticles extends CpListArticles
{}


/**
 * Компонент списка статей (для AJAX)
 * 
 * @category   Gear
 * @package    List
 * @subpackage Articles
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: component.php 2016-01-01 12:00:00 Gear Magic $
 */
class jListArticles extends CjListArticles
{}
?>