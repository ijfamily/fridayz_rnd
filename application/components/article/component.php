<?php
/**
 * Gear CMS
 *
 * Пакет компонент "Статьи"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Article
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: component.php 2016-01-01 21:00:00 Gear Magic $
 */

defined('_INC') or die;

Gear::library('/Components/Article');

/**
 * Компонент статьи
 * 
 * @category   Gear
 * @package    Article
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: component.php 2016-01-01 21:00:00 Gear Magic $
 */
class Article extends CpArticle
{}


/**
 * Компонент схемы расположения страниц
 * 
 * @category   Gear
 * @package    Article
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: component.php 2016-01-01 21:00:00 Gear Magic $
 */
class ArticleSchema extends CpArticleSchema
{}
?>