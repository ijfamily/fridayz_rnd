<?php
/**
 * Gear CMS
 *
 * Компонент "Слайдер Nivo"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Slider
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: component.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CpSliderNivo
 */
Gear::library('/Components/Slider/Nivo');

/**
 * Компонент Nivo слайдер
 * jQuery Nivo Slider v2.7.1
 * http://nivo.dev7studios.com
 * 
 * @category   Gear
 * @package    Slider
 * @subpackage Nivo
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: component.php 2016-01-01 12:00:00 Gear Magic $
 */
class SliderNivo extends CpSliderNivo
{}
?>