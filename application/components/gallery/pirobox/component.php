<?php
/**
 * Gear CMS
 *
 * Компонент "Галерея Pirobox"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Gallery
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: component.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CpGalleryPirobox
 */
Gear::library('/Components/Gallery/Pirobox');

/**
 * Компонент галереи для jQuery Pirobox
 * 
 * @category   Gear
 * @package    Gallery
 * @subpackage Pirobox
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: component.php 2016-01-01 12:00:00 Gear Magic $
 */
class GalleryPirobox extends CpGalleryPirobox
{}
?>