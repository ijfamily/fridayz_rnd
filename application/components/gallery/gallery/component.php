<?php
/**
 * Gear CMS
 *
 * Компонент "Галерея Simple"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Gallery
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: component.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CpGallerySimple
 */
Gear::library('/Components/Gallery/Gallery');

/**
 * Компонент галереи для jQuery Simple Gallery
 * 
 * @category   Gear
 * @package    Gallery
 * @subpackage Simple
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: component.php 2016-01-01 12:00:00 Gear Magic $
 */
class Gallery extends CpGallery
{}
?>