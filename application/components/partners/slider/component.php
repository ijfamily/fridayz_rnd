<?php
/**
 * Gear CMS
 *
 * Компонент "Слайдер партнёров"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Partner
 * @copyright  Copyright (c) 2013-2017 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: component.php 2017-08-24 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CpPartnerSlider
 */
Gear::library('/Components/Partners/Slider');

/**
 * Компонент слайдера партнёров
 * 
 * @category   Gear
 * @package    Partner
 * @subpackage Slider
 * @copyright  Copyright (c) 2013-2017 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: component.php 2017-08-24 12:00:00 Gear Magic $
 */
class PartnersSlider extends CpPartnersSlider
{}
?>