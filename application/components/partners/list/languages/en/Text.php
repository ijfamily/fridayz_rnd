<?php
/**
 * Gear CMS
 *
 * Пакет британской (английской) локализации
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Language
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // компонент
    'setting component' => 'Настройка компонента',
    'add album' => 'Добавить изображение',
    'edit' => 'Редактировать',
    'albums' => 'Альбомы',
    'property' => 'Свойства',
    'setting item' => 'Настройка элемента',

    // список
    'next'    => 'next',
    'last'    => 'last',
    'first'   => 'first',
    'previos' => 'previos',
    'Showing records %s to %s of %s' => 'Showing records <b>%s</b> to <b>%s</b> of <b>%s</b>',

    // фотоальбомы
    'album' => 'Фотоотчет'
);
?>