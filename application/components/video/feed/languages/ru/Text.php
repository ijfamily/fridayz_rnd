<?php
/**
 * Gear CMS
 *
 * Пакет русской локализации
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Language
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // компонент
    'add video'  => 'Добавить видеозапись',
    'videos'     => 'Видеозаписи',
    'properties' => 'Свойства',
    'setting component' => 'Настройка компонента',
    'setting element'   => 'Настройка элемента',
    'edit'              => 'Редактировать'
);
?>