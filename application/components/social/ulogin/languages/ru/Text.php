<?php
/**
 * Gear CMS
 *
 * Пакет русской локализации
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Language
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    'Unable to retrieve user data' => 'Невозможно получить данные пользователя, неправильно указан адаптер социальной сети!',
    'Unable to retrieve data from the server' => 'Невозможно получить данные пользователя от сервера (%s)!',
    'You can not add your social network account' => 'Невозможно добавить ваш аккаунт социальной сети!',
    'You can not create a user account' => 'Невозможно создать ваш аккаунт (обратитесь к администратору сайта)!',
    'You can not update a user account' => 'Невозможно обновить ваш аккаунт (обратитесь к администратору сайта)!',
    'You can not access the site to your social network account' => 'Невозможно получить доступ на сайте к аккаунту вашей социальной сети!'
);
?>