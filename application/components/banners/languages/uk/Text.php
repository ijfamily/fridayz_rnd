<?php
/**
 * Gear CMS
 *
 * Пакет украинской локализации
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Language
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    'The data has already been sent' => 'Дані вже були відправлені!',
    'You too often sending messages' => 'Ви занадто часто відправляэте повідомлення!',
    'To work correctly, the application must enable cookies' => 'Для коректної роботи програми необхідно включити cookies',
    'On this page is limited number of outgoing messages'    => 'На цій сторінці обмежена кількість відправлених повідомлень',
    'Your message recognized as spam and will not be accepted more' => 'Ваше повідомлення розпізнано як СПАМ і більше прийматися не будуть',
    'Unable to send a message, a server error' => 'Неможливо відправити повідомлення, помилка сервера',

    // CMFormAccountRestore
    'Restore account' => 'Відновлення облікового запису',
    'Can`t recovery account' => 'Неможливо відновити обліковий запис',
    'Mail for restore account already send' => 'Лист для відновлення облікового запису вже відправлено на Ваш e-mail',

    // CMFormAccountUser, CMFormAccountLogin, CMFormAccountCreate
    'E-mail' => 'E-mail',
    'User name' => 'Iм\'я користувача',
    'Address' => 'Адреса',
    'Phone' => 'Телефон',
    'Password' => 'Пароль',
    'You must fill the field "%s"' => 'Необхідно заповнити поле "%s"',
    'You did not fill the field "%s"' => 'Ви неправильно заповнили поле"%s"',
    'The length of the field "%s" must be from %s characters' => 'Довжина значення поля "%s", повинна бути від %s-і символів!',
    'The max length of the field "%s" must be no more than %s characters' => 'Максимальна довжина "%s", не повинна перевищувати %s-і символів!',
    'Error processing forms' => 'Помилка обробки форми!',
    'Unauthorized access attempt' => 'Спроба несанкціонованого доступу до системи (підміна даних)!',
    'Password confirmation does not match' => 'Підтвердження пароля не збігається',
    'Change the data was successful' => 'Зміна даних виконано успішно',
    'A user with the name "%s" already exists' => 'Користувач з ім\'ям "%s" вже існує!',
    'A user with this e-mail "%s" already exists' => 'Користувач з таким e-mail адресою "%s" вже існує!',
    'An attempt to substitute the captcha code' => 'Спроба підміни коду капчі!',
    'You incorrectly entered the code from the image' => 'Ви невірно ввели код на зображенні!',
    'Recovery account' => 'Відновлення облікового запису',
    'Your account has been created successfully' => 'Ваш обліковий запис створено успішно',
    'Confirmation of registration' => 'Підтвердження реєстрації',
    'A user with this e-mail "%s" not exists' => 'Користувач з таким e-mail адресою "%s" не існує!',
    'You are in an incorrect username or password, please try again' => 'Ви неправильно ввели e-mail адресу або пароль, будь ласка спробуйте ще раз',

    // CMFormAccountConfirm
    'Empty email' => 'Неможливо активувати обліковий запис, відсутній e-mail',
    'Empty code' => 'Неможливо активувати обліковий запис, відсутній код',
    '2' => 'Помилка активації облікового запису!',
    '1' => 'Неможливо активувати обліковий запис',

    // CMFormFeedback
    'Thank you for your message' => 'Спасибі за Ваше повідомлення!',
    'Text'   => 'Текст повідомлення',
    'E-mail' => 'Ваш e-mail',

    // CMFormQuiz
    'Thank you for your response' => 'Дякуємо за вашу відповідь!',
    'Which option' => 'Виберіть варіант відповіді!',
    'You have already voted' => 'Ви вже голосували!',

    // CMFormComment
    'Thank you for your comment' => 'Спасибо за ваш коментар',

    // CMFormSearchArticle
    'Search this site' => 'Пошук на сайті',
    'To search you must enter a word from %s to %s characters' => 'Для пошуку необхідно ввести слово від %s-х до %s-х символів',

    // CMFormFastCart
    'For an application, you have to fill in the email' => 'Для получения заявки на почту, Вам необходимо заполнить поле "email"'
);
?>