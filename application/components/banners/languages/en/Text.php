<?php
/**
 * Gear CMS
 *
 * Пакет английской (британской) локализации
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Language
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    'The data has already been sent' => 'The data has already been sent!',
    'You too often sending messages' => 'You too often sending messages!',
    'To work correctly, the application must enable cookies' => 'To work correctly, the application must enable cookies',
    'On this page is limited number of outgoing messages'    => 'On this page is limited number of outgoing messages',
    'Your message recognized as spam and will not be accepted more' => 'Your message recognized as spam and will not be accepted more',
    'Unable to send a message, a server error' => 'Unable to send a message, a server error',

    // CMFormAccountRestore
    'Restore account' => 'Account recovery',
    'Can`t recovery account' => 'Unable to recover account',
    'Mail for restore account already send' => 'Letter to restore the account has been sent to your e-mail',

    // CMFormAccountUser, CMFormAccountLogin, CMFormAccountCreate
    'E-mail' => 'E-mail',
    'User name' => 'User name',
    'Address' => 'Address',
    'Phone' => 'Phone',
    'Password' => 'Password',
    'You must fill the field "%s"' => 'Field is required "%s"',
    'You did not fill the field "%s"' => 'You did not fill the field "%s"',
    'The length of the field "%s" must be from %s characters' => 'The length of the field "%s" must be from %s characters!',
    'The max length of the field "%s" must be no more than %s characters' => 'The max length of the field "%s" must be no more than %s characters!',
    'Error processing forms' => 'Error processing forms!',
    'Unauthorized access attempt' => 'Unauthorized access attempt!',
    'Password confirmation does not match' => 'Password confirmation does not match',
    'Change the data was successful' => 'Change the data was successful',
    'A user with the name "%s" already exists' => 'A user with the name "%s" already exists!',
    'A user with this e-mail "%s" already exists' => 'Пользователь с таким e-mail адресом "%s" уже существует!',
    'An attempt to substitute the captcha code' => 'An attempt to substitute the captcha code!',
    'You incorrectly entered the code from the image' => 'You incorrectly entered the code from the image!',
    'Recovery account' => 'Recovery account',
    'Your account has been created successfully' => 'Your account has been created successfully',
    'Confirmation of registration' => 'Confirmation of registration',
    'A user with this e-mail "%s" not exists' => 'A user with this e-mail "%s" not exists!',
    'You are in an incorrect username or password, please try again' => 'You are in an incorrect username or password, please try again',

    // CMFormAccountConfirm
    'Empty email' => 'Unable to activate the account, there is no e-mail',
    'Empty code' => 'Unable to activate the account, there is no code',
    '2' => 'Activation error account!',
    '1' => 'Unable to activate account',

    // CMFormFeedback
    'Thank you for your message' => 'Thank you for your message!',

    // CMFormQuiz
    'Thank you for your response' => 'Thank you for your response!',
    'Which option' => 'Which option!',
    'You have already voted' => 'You have already voted!',

    // CMFormComment
    'Thank you for your comment' => 'Thank you for your comment',

    // CMFormSearchArticle
    'Search this site' => 'Search on the site',
    'To search you must enter a word from %s to %s characters' => 'To search you must enter a word from %s to %s characters',

    // CMFormFastCart
    'For an application, you have to fill in the email' => 'Для получения заявки на почту, Вам необходимо заполнить поле "email"'
);
?>