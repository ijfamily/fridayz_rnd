<?php
/**
 * Gear CMS
 *
 * Пакет русской локализации
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Language
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    'The data has already been sent' => 'Данные уже были отправлены!',
    'You too often sending messages' => 'Вы слишком часто отправляете сообщения!',
    'To work correctly, the application must enable cookies' => 'Для корректной работы приложения необходимо включить cookies',
    'On this page is limited number of outgoing messages'    => 'На этой странице ограничено количество отправляемых сообщений',
    'Your message recognized as spam and will not be accepted more' => 'Ваше сообщение распознано как СПАМ и больше приниматься не будут',
    'Unable to send a message, a server error' => 'Невозможно отправить сообщение, ошибка сервера',

    // CmFormRemindPassword
    'Restore account' => 'Восстановление учётной записи',
    'Can`t recovery account' => 'Невозможно восстановить учётную запись',
    'Mail for restore account already send' => 'Письмо для восстановления учётной записи уже отправлено на Ваш e-mail',

    // CmFormUser, CmFormSignIn, CmFormSignUp
    'Client name' => 'Имя',
    'Client last name' => 'Фамилия',
    'Contact phone' => 'Телефон',
    'Image code' => 'Код на картинке',
    'Select country from list' => 'Выберите из списка страну!',
    'Select region from list' => 'Выберите из списка регион!',
    'Select city from list' => 'Выберите из списка город, если его нет - заполните поле!',
    'E-mail' => 'E-mail',
    'User name' => 'Имя пользователя',
    'Address' => 'Адрес',
    'Phone' => 'Телефон',
    'Password' => 'Пароль',
    'Password confirm' => 'Подтверждение пароля',
    'You must fill the field "%s"' => 'Необходимо заполнить поле "%s"',
    'You did not fill the field "%s"' => 'Вы неправильно заполнили поле "%s"',
    'The length of the field "%s" must be from %s characters' => 'Длина значения поля "%s", должна быть от %s-и символов!',
    'The max length of the field "%s" must be no more than %s characters' => 'Максимальная длина поля "%s", не должна превышить %s-и символов!',
    'Error processing forms' => 'Ошибка обработки формы!',
    'Unauthorized access attempt' => 'Попытка несанкциоинированного доступа к системе (подмена данных)!',
    'Captcha code error' => 'Вы неправильно ввели код на картинке!',
    'Password confirmation does not match' => 'Подтверждение пароля не совпадает',
    'Change the data was successful' => 'Изменение данных выполнено успешно',
    'A user with the name "%s" already exists' => 'Пользователь с именем "%s" уже существует!',
    'A user with this e-mail "%s" already exists' => 'Пользователь с таким e-mail адресом "%s" уже существует!',
    'An attempt to substitute the captcha code' => 'Попытка подмены кода капчи!',
    'You incorrectly entered the code from the image' => 'Вы неверно ввели код на картинке!',
    'Recovery account' => 'Восстановление учётной записи',
    'Your account has been created successfully' => 'Ваша учетная запись создана успешно',
    'Confirmation of registration' => 'Подтверждение регистрации',
    'A user with this e-mail "%s" not exists' => 'Пользователь с таким e-mail адресом "%s" не существует!',
    'You are in an incorrect username or password, please try again' => 'Вы неправильно ввели e-mail адрес или пароль, пожалуйста попробуйте еще раз',

    // CmFormSignConfirm
    'Empty email' => 'Невозможно активировать учетную запись, отсутствует e-mail',
    'Empty code' => 'Невозможно активировать учетную запись, отсутствует код',
    '2' => 'Ошибка активации учетной записи!',
    '1' => 'Невозможно активировать учетную запись',

    // CmFormFeedback
    'Thank you for your message' => 'Cпасибо за Ваше сообщение!',
    'Feedback form' => 'Форма обратной связи',
    'Feedback' => 'Обратная связь',
    'Message' => 'Сообщение',

    // CmFormCallback
    'Callback form' => 'Форма обратного вызова',
    'Callback' => 'Обратный вызов',
    'Our manager in the near future you will be contacted' => 'Наш менеджер в ближайшее время с вами свяжется',

    // CmFormArticleComment
    'Thank you for your comment' => 'Cпасибо за ваш комментарий',

    // CmFormSearchArticle
    'Search this site' => 'Поиск на сайте',
    'To search you must enter a word from %s to %s characters' => 'Для поиска необходимо ввести слово от %s-х до %s-х символов',
    'next'    => 'следующая',
    'last'    => 'конец',
    'first'   => 'начало',
    'previos' => 'предыдущая',
    'Showing records %s to %s of %s' => 'Записи с <b>%s</b> по <b>%s</b>, всего <b>%s</b>',

    // CmFormOrder
    'Order form' => 'Форма заявки',
    'Thank you for your order' => 'Спасибо за Вашу заявку, мы в ближайшее время с вами свяжемся!',
);
?>