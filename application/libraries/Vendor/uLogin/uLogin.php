<?php
/**
 * Gear CMS
 *
 * uLogin виджет подключения к интернет-сервисам без необходимости повторной регистрации
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Vendor
 * @package    uLogin
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: uLogin.php 2016-01-01 21:00:00 Gear Magic $
 */

/**
 * Класс виджета подключения к интернет-сервисам без необходимости повторной регистрации
 * 
 * @category   Vendor
 * @package    uLogin
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: uLogin.php 2016-01-01 12:00:00 Gear Magic $
 */
class uLogin
{
    /**
     * Кому предназначен запрос (если форм на странице много)
     *
     * @var string
     */
    public $url = 'http://ulogin.ru/token.php';

    /**
     * Ключ социальной сети
     *
     * @var string
     */
    public $token = '';

    /**
     * Конструктор класса
     * 
     * @return void
     */
    public function __construct()
    {
        $this->token = isset($_GET['token']) ? $_GET['token'] : '';
    }

    /**
     * Возращает данные пользователя
     * 
     * @return mixed
     */
    public function getUserData()
    {
        $content = file_get_contents($this->url . '?token=' . $this->token . '&host=' . $_SERVER['HTTP_HOST']);
        if ($content === false) {
            return false;
        }

        $user = json_decode($content, true);
        if (isset($user['photo'])) {
            if ($user['photo'] == 'https://ulogin.ru/img/photo.png')
                $user['photo'] = '/data/images/profile-photo-none.png';
        } else
            $user['photo'] = '/data/images/profile-photo-none.png';
        if (isset($user['error'])) {
            GMessage::to('form', 'error', 'Unable to retrieve data from the server', 'social/ulogin/', array($user['error']));
            return false;
        }

        return $user;
    }

	/**
	 * "Обменивает" токен на пользовательские данные
	 */
	protected function getUserFromToken() {
		global $config;
		$response = false;
		if ($this->token){
			$host = $_SERVER['SERVER_NAME'];
			$data = array(
				'cms' => 'dle',
				'version' => $config['version_id'],
			);
			$request = 'http://ulogin.ru/token.php?token=' . $this->token . '&host=' . $host . '&data='.base64_encode(json_encode($data));
			$response = $this->getResponse($request);
		}
		return $response;
	}

    /**
     * Результат запроса к сервису
     * 
     * @return boolean
     */
	private function getResponse($url = '', $do_abbort=true) {
		$result = false;

		if (in_array('curl', get_loaded_extensions())) {
			$request = curl_init($url);
			curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($request, CURLOPT_BINARYTRANSFER, 1);
			$result = curl_exec($request);
		}elseif (function_exists('file_get_contents') && ini_get('allow_url_fopen')){
			$result = file_get_contents($url);
		}

		if (!$result) {
			if ($do_abbort) {
				$this->sendMessage(array(
					'title' => 'ulogin_read_response_error',
					'msg' => 'ulogin_read_response_error_text',
					'type' => 'error'
				));
			}
			return false;
		}

		return $result;
	}

	/**
	 * Заргузка аватара пользователя
	 * @param $photo_url
	 * @param $member_id
	 * @return string
	 */
	private function setAvatar($photo_url, $member_id) {
		global $config;
		
		$user_group_id = $member_id['user_group'];
		$user_id = $member_id['user_id'];

		if ((int)$user_group_id <= 0) {
			return '';
		}

		$user_group = $this->model->getUserGroupById($user_group_id);

		if( $photo_url && intval($user_group['max_foto']) > 0) {

			$res = $this->getResponse($photo_url, false);
			$res = (!$res && in_array('curl', get_loaded_extensions())) ? file_get_contents($photo_url) : $res;

			if(!$res) {
				return '';
			}

			$savepath = ROOT_DIR . "/uploads/fotos/";

			if (!is_dir($savepath) || !is_writable($savepath)) {
				return '';
			}

			$tmp_name = "foto_" . $user_id . '.tmp';

			$handle = fopen($savepath . $tmp_name, "w");
			$fileSize = fwrite($handle, $res);
			fclose($handle);

			if(!$fileSize)
			{
				@unlink($savepath . $tmp_name);
				return '';
			}


			list($width, $height, $image_type) = getimagesize( $savepath . $tmp_name );
			if ($width == 0 || $height == 0) {
				return '';
			}

			switch ( $image_type ) {
				case IMAGETYPE_GIF:
					$file_ext = 'gif';
					break;
				case IMAGETYPE_JPEG:
					$file_ext = 'jpg';
					break;
				case IMAGETYPE_PNG:
					$file_ext = 'png';
					break;
				default:
					$file_ext = 'jpg';
					break;
			}

			$photo_name = str_replace('.tmp', '.' . $file_ext, $tmp_name);


			include_once ENGINE_DIR . '/classes/thumb.class.php';
			@chmod( $savepath . $tmp_name, 0666 );

			$thumb = new thumbnail( $savepath . $tmp_name);

			$min_size = min($width, $height, $user_group['max_foto']);

			if( $thumb->size_auto($min_size . "x" . $min_size)) {
				$thumb->jpeg_quality( $config['jpeg_quality'] );
				$thumb->save( $savepath . $photo_name );
			} else {
				if($file_ext == "jpg" || $file_ext == "jpeg") {
					$thumb->jpeg_quality( $config['jpeg_quality'] );
					$thumb->save( $savepath . $photo_name );
				} else {
					@rename( $savepath . $tmp_name, $savepath . $photo_name );
				}
			}
			@unlink( $savepath . $tmp_name );
			@chmod( $savepath . $photo_name, 0666 );

			return $photo_name;
		}

		return '';
	}
 
    /**
     * Проверка существования ключа
     * 
     * @return boolean
     */
    public function hasToken()
    {
        return !empty($this->token);
    }
}