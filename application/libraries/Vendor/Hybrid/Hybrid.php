<?php
/**
 * HybridAuth
 *
 * Виджет подключения к интернет-сервисам без необходимости повторной регистрации
 *
 * http://hybridauth.sourceforge.net | http://github.com/hybridauth/hybridauth
 * (c) 2009-2015, HybridAuth authors | http://hybridauth.sourceforge.net/licenses.html
 * 
 * @category   Vendor
 * @package    HybridAuth
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Hybrid.php 2016-01-01 21:00:00 Gear Magic $
 */

/**
 * Класс виджета подключения к интернет-сервисам без необходимости повторной регистрации
 * 
 * @category   Vendor
 * @package    HybridAuth
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Hybrid.php 2016-01-01 12:00:00 Gear Magic $
 */
class Hybrid
{
    /**
     * Ключ социальной сети
     *
     * @var string
     */
    public $token = '';

    /**
     * Конструктор класса
     * 
     * @return void
     */
    public function __construct()
    {
        $this->token = isset($_GET['token']) ? $_GET['token'] : '';
    }

    /**
     * Возращает данные пользователя
     * 
     * @return mixed
     */
    public function getUserData()
    {
        require_once('Hybrid/Auth.php');

        try {
            // hybridauth EP
            $hybridauth = new Hybrid_Auth($providers);
            $auth = 'Vkontakte';
            $twitter = $hybridauth->authenticate($auth);
            $is_user_logged_in = $twitter->isUserConnected();
            // get the user profile 
            $user_profile = $twitter->getUserProfile();
    
            // access user profile data
            //echo "Ohai there! U are connected with: <b>{$twitter->id}</b><br />";
            //echo "As: <b>{$user_profile->displayName}</b><br />";
            //echo "And your provider user identifier is: <b>{$user_profile->identifier}</b><br />";  
            
            // or even inspect it
            //echo "<pre>" . print_r( $user_profile, true ) . "</pre><br />";
            // uncomment the line below to get user friends list
            // $twitter->getUserContacts();
            // uncomment the line below to post something to twitter if you want to
            // $twitter->setUserStatus( "Hello world!" );
            // ex. on how to access the twitter api with hybridauth
            //     Returns the current count of friends, followers, updates (statuses) and favorites of the authenticating user.
            //     https://dev.twitter.com/docs/api/1/get/account/totals
            //$account_totals = $twitter->api()->get( 'account/totals.json' );
    
            // print recived stats 
            //echo "Here some of yours stats on Twitter:<br /><pre>" . print_r( $account_totals, true ) . "</pre>";
    
            // logout
            echo "Logging out.."; 
            $twitter->logout();
        }
        catch (Exception $e) {
            // In case we have errors 6 or 7, then we have to use Hybrid_Provider_Adapter::logout() to 
            // let hybridauth forget all about the user so we can try to authenticate again.
            
            // Display the recived error, 
            // to know more please refer to Exceptions handling section on the userguide
            switch( $e->getCode() ){ 
                case 0 : $errMessage =  "Unspecified error."; break;
                case 1 : $errMessage =  "Hybridauth configuration error."; break;
                case 2 : $errMessage =  "Провайдер социальной сети не правильно настроен."; break;
                case 3 : $errMessage = "Неизвестный или отключен провайдер социальной сети."; break;
                case 4 : $errMessage =  "Отсутствующие учетные данные приложения провайдера социальной сети."; break;
                case 5 : $errMessage =  "Ошибка аутентификации. Пользователь отменил проверку подлинности или поставщик отклонил соединение."; 
                    break;
                case 6 : $errMessage =  "Запрос Профиль пользователя не удалось. Скорее всего, пользователь не подключен к провайдеру социальной сети и он должен аутентифицироваться."; 
                    $twitter->logout();
                    break;
                case 7 : $errMessage =  "Пользователь не подключен к провайдеру социальной сети."; 
                    $twitter->logout();
                    break;
                case 8 : $errMessage =  "Провайдер социальной сети не поддерживает эту функцию."; break;
            }

            // well, basically your should not display this to the end user, just give him a hint and move on..
            $errMessage .= "<br />" . $e->getMessage();
            //echo "<hr /><h3>Trace</h3> <pre>" . $e->getTraceAsString() . "</pre>"; 
        }
    }

    /**
     * Проверка существования ключа
     * 
     * @return boolean
     */
    public function hasToken()
    {
        return !empty($this->token);
    }
}