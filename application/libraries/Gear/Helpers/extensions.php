<?php
/**
 * Gear CMS
 *
 * Описание допустимых расширений файлов
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 *
 * @category   Gear
 * @package    Config
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Files.php 2016-01-01 12:00:00  Gear Magic $
 */

defined('_INC') or die;

return array(
    // vector images
    'cdr' => 'Corel Draw',
    'cgm' => 'Computer Graphics Metafile',
    'eps' => 'Encapsulated PostScript format',
    'ps'  => 'PostScript',
    'svg' => 'Scalable Vector Graphics',
    'wmf' => 'Windows Metafile',
    // images
    'bmp'  => 'Windows or OS/2 bitmap',
    'cpt'  => 'Corel PHOTO-PAINT bitmap',
    'gig'  => 'Graphics Interchange Format',
    'hdr'  => 'High Dynamic Range',
    'jpeg' => 'Joint Photographic Experts Group',
    'jpg'  => 'Joint Photographic Experts Group',
    'jpe'  => 'Joint Photographic Experts Group',
    'jp2'  => 'Joint Photographic Experts Group',
    'pcx'  =>  'ZSoft PaintBrush',
    'pdn'  => 'Paint.NET Image',
    'png'  => 'Portable Network Graphics',
    'psd'  => 'Photoshop document',
    'raw'  => 'Truevision Targa',
    'tga'  => 'Truevision Targa',
    'tif'  => 'Tagged Image Format',
    'tiff' => 'Tagged Image Format',
    'hdp'  => 'Windows Media Photo',
    'wdp'  => 'Windows Media Photo',
    'xpm'  => 'X pixmap',
    // video
    '3gp' => 'Mobile video',
    'asf' => 'Advanced Streaming Format',
    'avi' => 'Audio Video Interleave',
    'bik' => 'BinkVideo',
    'flv' => 'Flash Video',
    'mkv' => 'Matroska',
    'MOV' => '',
    'mxf' => 'Material eXchange Format',
    'Ogg' => 'Tarkin & Theora',
    'mov' => 'QuickTime',
    'qt'  => 'QuickTime',
    'smk' => 'Smacker',
    'swf' => 'ShockWave File',
    'vob' => 'DVD-Video File',
    'wmv' => 'Windows meta video',
    // documents
    'pdf'  => 'Portable Document Format (.PDF)',
    'djv'  => 'DjVu',
    'txt'  => 'Text document (.TXT)',
    'rar'  => 'Archive WinRAR (.RAR)',
    'zip'  => 'Archive ZIP - WinRAR (.ZIP)',
    'doc'  => 'Document Microsoft Word (.DOC)',
    'docx' => 'Document Microsoft Word (.DOCX)',
    'xls'  => 'Document Microsoft Excel (.XLS)',
    'xlsx' => 'Document Microsoft Excel (.XLSX)',
);
?>