<?php
/**
 * Gear CMS
 * 
 * Пакет документа
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Document.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Класс документа
 * 
 * @category   Gear
 * @package    Core
 * @copyright  Copyright (c) 2012-2015 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Document.php 2016-01-01 12:00:00 Gear Magic $
 */
class GDocument
{
    const TAG_BREAK = '<p>[break]</p>';

    /**
     * Состояние страницы ("page", "print") по который компоненты определяют
     * что с ней делать (вывести для печати и т.д.)
     *
     * @var string
     */
    public static $state = 'page';

    /**
     * Изображения в статье ({image1}, {image2}, ...)
     *
     * @var array
     */
    public $images = array();

    /**
     * Видео в статье ({video1}, {video2}, ...)
     *
     * @var array
     */
    public $video = array();

    /**
     * Документы в статье ({doc1}, {doc2}, ...)
     *
     * @var array
     */
    public $documents = array();

    /**
     * Статус возращаемый клиенту
     *
     * @var integer
     */
    public $code = 200;

    /**
     * Заголовок статьи
     *
     * @var string
     */
    public $header;

    /**
     * Показывать заголовок статьи
     *
     * @var boolean
     */
    public $showHeader = true;

    /**
     * Название статьи на вкладе браузера
     * (пример: "название на вкладке" => "{HOST -} название на вкладке")
     *
     * @var string
     */
    public $title;

    /**
     * Идинд. статьи в базе данных (article_id)
     *
     * @var integer
     */
    public $id = 0;

    /**
     * Идинд. статьи в базе данных (page_id)
     *
     * @var integer
     */
    public $pageId = 0;

    /**
     * Идинд. шаблона статьи (template_id)
     *
     * @var integer
     */
    public $templateId = 0;

    /**
     * Разбор статьи (если есть компоненты)
     *
     * @var boolean
     */
    public $parsing = false;

    /**
     * Если страница сайта кэшируется
     *
     * @var boolean
     */
    public $caching = false;

    /**
     * Просмотр страницы перед печатью
     *
     * @var boolean
     */
    public $print = false;

    /**
     * Если в документе есть блоки лендинга
     *
     * @var boolean
     */
    public $isLanding = false;

    /**
     * Дата правки статьи
     *
     * @var string
     */
    public $date = '';

    /**
     * Файл шаблона схемы страницы
     *
     * @var string
     */
    public $schema = '';

    /**
     * Файл шаблона статьи
     *
     * @var string
     */
    public $template = '';

    /**
     * Данные статьи полученные из базы данных
     *
     * @var array
     */
    protected $_data = array();

    /**
     * !!!Using article template for output overlay components
     *
     * @var boolean
     */
    public $isUseTemplate = true;

    /**
     * Текст статьи
     *
     * @var string
     */
    protected $_text;

    /**
     * Текст на выходе документа (полученный из getRenderText)
     *
     * @var string
     */
    public $html;

    /**
     * Выставляется в машрутезаторе запросов до инициализации данных приложения
     * (в том случаи если нет необходимости делать запрос на вывод статьи и есть 
     * возможность управлять выводом данных статьи через схему)
     *
     * @var boolean
     */
    public $schemaRouter = false;

    /**
     * Ошибки для которых предусмотрен шаблон
     *
     * @var array
     */
    protected $_errorCodes = array(
         400 => array('Bad Request', 'error_400.tpl', 'error_article_400.tpl'),
         401 => array('Unauthorized', 'error_401.tpl', 'error_article_401.tpl'),
         403 => array('Forbidden', 'error_403.tpl', 'error_article_403.tpl'),
         404 => array('Not Found', 'error_404.tpl', 'error_article_404.tpl'),
         500 => array('Internal Server Error', 'error_500.tpl', 'error_article_500.tpl')
    );

    /**
     * Мета на странице <meta name="..." content="...">
     * где array("name" => "content", ...)
     *
     * @var array
     */
    protected $_meta;

    /**
     * Свойства для разметки Open Graph <meta property="og:..." content="...">
     * где array("name" => "content", ....)
     *
     * @var array
     */
    protected $_metaOg;

    /**
     * Массив CSS стилей документа полученных через рендер компонентов
     *
     * @var array
     */
    protected $_css;

    /**
     * Массив объявленных CSS стилей
     *
     * @var array
     */
    protected $_styleDec;

    /**
     * Массив JS скриптов документа полученных через рендер компонентов
     *
     * @var array
     */
    protected $_script;

    /**
     * Массив объявленных JS скриптов
     *
     * @var array
     */
    protected $_scriptDec;

    /**
     * Массив объявленных JQuery скриптов
     *
     * @var array
     */
    protected $_jqueryDec;

    /**
     * Массив атрибутов компонентов документа
     * (вносятся изменения через CPage)
     *
     * @var array
     */
    protected $_components = array();

    /**
     * Атрибуты компонентов полученные из страницы (component_attributes)
     *
     * @var array
     */
    public $componentAttrs = array();

    /**
     * Возращает атрибут документа
     * 
     * @param  string $name название
     * @return mixed
     */
    public function get($name)
    {
        if ($name == 'meta')
            return $this->_meta;

        return $this->$name;
    }

    /**
     * Устанавливает атрибуты документа
     * 
     * @param  mixed $name название
     * @param  mixed $value значение
     * @return mixed
     */
    public function set($name, $value = null)
    {
        if (is_array($name)) {
            foreach ($name as $key => $value) {
                $this->_set($key, $value);
            }
            return $name;
        } else
            return $this->_set($name, $value);
    }

    /**
     * Устанавливает атрибуты документа
     * 
     * @param  mixed $name название
     * @param  mixed $value значение
     * @return mixed
     */
    protected function _set($name, $value)
    {
        if ($name == 'meta') {
            foreach ($value as $mName => $mValue) {
                $this->addMeta($mName, $mValue);
            }
            return $value;
        }

        return $this->$name = $value;
    }

    /**
     * Добавление тега <meta name="" content="">
     * 
     * @param  string $name название
     * @param  string $conten содержимое
     * @return mixed
     */
    public function addMeta($name, $content)
    {
        $this->_meta[$name] = $content;
    }

    /**
     * Добавление свойств в разметку Open Graph
     * 
     * @param  string $name название свойства
     * @param  string $conten содержимое
     * @return mixed
     */
    public function addMetaOg($name, $content)
    {
        $this->_metaOg[$name] = $content;
    }

    /**
     * Возращает мету документа
     * (добавленную через addMeta)
     * 
     * @return array
     */
    public function getMeta()
    {
        return $this->_meta;
    }

    /**
     * Возращает свойства для разметки Open Graph
     * 
     * @return array
     */
    public function getMetaOg()
    {
        return $this->_metaOg;
    }

    /**
     * Возращает теги <meta name="" content=""> документа
     * 
     * @return string
     */
    public function getMetaTags()
    {
        // если указан загаловок вкладки
        if ($this->title) {
            $title = $this->title;
        } else {
            // если указан загаловок статьи
            if ($this->header) {
                $title = $this->header;
            } else {
                // если указан файл
                if (Gear::$app->url->filename)
                    $title = Gear::$app->url->filename;
                else
                    $title = Gear::$app->url->path;
            }
        }

        // title
        if (Gear::$app->domainId > 0) {
            $titleTpl = Gear::$app->getSiteDomain(2);
            $title = sprintf($titleTpl, $title);
        } else {
            // для главной страницы пользователь сам вписывает загаловок, иначе шаблон
            if (!Gear::$app->url->isRoot())
                $title = sprintf(Gear::$app->config->get('TITLE', ''), $title);
        }

        if (!isset($this->_meta['generator']))
            $this->_meta['generator'] = GVersion::getPoweredBy();
        if (!isset($this->_meta['robots']))
            $this->_meta['robots'] = 'index, follow';
        if (empty($this->_meta))
            return _n_t . "<title>" . $title . "</title>\n\t";
        $this->_meta['title'] = $title;
        // если нет ключевых слов
        if (empty($this->_meta['keywords']))
            $this->_meta['keywords'] = htmlspecialchars_decode(Gear::$app->config->get('META/KEYWORDS', ''), ENT_QUOTES);
        // если нет описания
        if (empty($this->_meta['description']))
            $this->_meta['description'] = htmlspecialchars_decode(Gear::$app->config->get('META/DESCRIPTION', ''), ENT_QUOTES);

        $meta = '<title>' . $title . "</title>" . _n_t;
        $meta.= "<meta charset=\"" . Gear::$app->config->get('CHARSET', 'utf-8') . "\" />";
        $wasKeywords = false;
        $wasDescription = false;
        foreach ($this->_meta as $name => $content) {
            if ($name == 'title') continue;
            $meta .= _n_t . '<meta name="' . $name . '" content="' . str_replace('"', '&quot;', $content) /*htmlspecialchars($content, ENT_QUOTES)*/ . '" />';
        }

        // если разметка Open Graph включена и в документе нет ошибок
        if (Gear::$app->config->get('OPENGRAPH') && $this->isOk()) {
            $meta .= $this->getMetaOgTags();
        }

        // если отладка
        if (Gear::$app->debug) {
            Gear::$app->debug->info($this->_meta, 'Meta на странице');
            Gear::$app->debug->info($this->_metaOg, 'Meta Open Graph на странице');
        }

        // если используются канонические ссылки
        if (Gear::$app->config->get('CANONICAL') && $this->isOk()) {
            $meta .= _n_t . '<link rel="canonical" href="' . Gear::$app->url->getWithoutQuery() . '" />';
        }/* else
            $meta .= _n_t . '<link rel="canonical" href="' . Gear::$app->url->host . '" />';*/

        return $meta;
    }

    /**
     * Возращает разметку для Open Graph
     * 
     * @return string
     */
    public function getMetaOgTags()
    {
        $this->_metaOg['og:title'] = $this->_meta['title'];
        //if (empty($this->_metaOg['og:url'])) {
            /*if (Gear::$app->url->path = '/')
                $this->_metaOg['og:url'] = Gear::$app->url->host . '/';
            else*/
        $this->_metaOg['og:url'] = Gear::$app->url->host . Gear::$app->url->path;
        //}
        // если одно изображение для всех статей и категорий
        $ogImage = '';
        if (Gear::$app->config->get('OPENGRAPH/IMAGE'))
            $ogImage = Gear::$app->config->get('OPENGRAPH/IMAGE');
        else
            if (!empty($this->_data['page_image']))
                $ogImage = Gear::$app->url->host . $this->_data['page_image'];
        if ($ogImage) {
            $this->_metaOg['og:image'] = $ogImage;
            $this->_metaOg['og:image:url'] = $ogImage;
        }

        if (empty($this->_metaOg['og:description']))
            $this->_metaOg['og:description'] = $this->_meta['description'];
        if (empty($this->_metaOg['og:type'])) {
            $this->_metaOg['og:type'] = 'article';
            $this->_metaOg['article:tag'] = $this->_meta['keywords'];
            if (!empty($this->_data['published_date']))
                $this->_metaOg['article:published_time'] =  date('Y-m-d', strtotime($this->_data['published_date'])) . ' ' . $this->_data['published_time'];
            if (!empty($this->_data['page_meta_author']))
                $this->_metaOg['article:author'] = $this->_data['page_meta_author'];
        }
        $meta = '';
        foreach ($this->_metaOg as $name => $content) {
            $meta .= _n_t . '<meta property="' . $name . '" content="' . str_replace('"', '&quot;', $content) . '" />';
        }

        return $meta;
    }

    /**
     * Добавление объявления JS в документ
     * 
     * @param  string $text JS скрипт
     * @param  string $type тип MIME
     * @return mixed
     */
    public function addScriptDeclaration($text, $type = 'text/javascript', $comment = '')
    {
        $this->_scriptDec[] = array($text, $type, $theme, $comment);
    }

    /**
     * Добавление объявления JQuery в документ
     * 
     * @param  string $text JQuery скрипт
     * @return mixed
     */
    public function addJQueryDeclaration($text)
    {
        $this->_jqueryDec[] = $text;
    }

    /**
     * Возращает объявления JS документа
     * (добавленные через addScriptDeclaration)
     * 
     * @return array
     */
    public function getScriptDeclaration()
    {
        return $this->_scriptDec;
    }

    /**
     * Возращает "<script ..." документа
     * 
     * @return string
     */
    public function getScriptDeclarationTags()
    {
        $jtext = $text = '';

        // если отладка
        if (Gear::$app->debug) {
            if ($this->_scriptDec)
                Gear::$app->debug->info($this->_scriptDec, 'Для тега "script"');
            if ($this->_jqueryDec)
                Gear::$app->debug->info($this->_jqueryDec, 'Для тега jQuery "script"');
        }

        if ($this->_jqueryDec) {
            $count = sizeof($this->_jqueryDec);
            for ($i = 0; $i < $count; $i++)
                $jtext .= $this->_jqueryDec[$i] . _n_t;
            $jtext = '<script type="text/javascript">' . _n_t
                  . '$(document).ready(function(){' . _n_t
                  . $jtext
                  . "});" . _n_t . "</script>";
        }

        // если использовать динамические баннера
        /*
        if (Gear::$app->config->get('BANNERS'))
            $jtext .= '<script type="text/javascript" src="{host}/request/banners/" rel="nofollow"></script>';
        */

        if (empty($this->_scriptDec)) return $jtext;

        $text = '';
        $count = sizeof($this->_scriptDec);
        for ($i = 0; $i < $count; $i++) {
            $scr = $this->_scriptDec[$i];
            if ($scr[1])
                $type = ' type="' . $scr[1] . '"';
            else
                $type = '';
            $text .= '<script' . $type . ">" . _n_t;
            $text .= $scr[0];
            $text .= "</script>";
        }

        return $jtext . $text;
    }

    /**
     * Добавление скрипта JS в документ
     * 
     * @param  string $url ресурс JS скрипт
     * @param  string $type тип MIME
     * @param  string $theme использовать тему
     * @param  boolean $outer подключить скрипт извне
     * @return void
     */
    public function addScript($url, $type = 'text/javascript', $theme = false, $outer = false, $comment = '')
    {
        if (!isset($this->_script[$url]))
            $this->_script[$url] = array();

        $this->_script[$url] = array($url, $type, $theme, $outer, $comment);
    }

    /**
     * Возращает JS скрипты документа
     * (добавленную через addScript)
     * 
     * @return array
     */
    public function getScript()
    {
        return $this->_script;
    }

    /**
     * Возращает скрипт инициализации ядра Gear
     * 
     * @return string
     */
    public function getScriptInitGear()
    {
        return
            _n_t . '<script type="text/javascript">var gearInit = {'
                 . 'visits: ' . (Gear::$app->config->get('CONSIDER/VISITS') ? 'true' : 'false') . ', '
                 . 'sef: ' . (Gear::$app->config->get('SEF') ? 'true' : 'false') . ', '
                 . 'sefType: ' . Gear::$app->config->get('SEF/TYPE')
                 . ' };</script>';
    }

    /**
     * Возращает "<script ..." документа
     * 
     * @return string
     */
    public function getScriptTags()
    {
        $gearScript = $this->getScriptInitGear();

        if (empty($this->_script)) return $gearScript;

        $text = _n_t;
        $index = 0;
        $count = sizeof($this->_script);
        $host = Gear::$app->url->host;
        foreach ($this->_script as $url => $attr) {
            // тип
            if ($attr[1])
                $type = ' type="' . $attr[1] . '"';
            else
                $type = '';
            // если есть комментарий
            if ($attr[4])
                $text .= '<!-- ' . $attr[4] . ' -->' . _n_t;
            // если ресурс извне
            if ($attr[3])
                $text .= '<script' . $type . ' src="'. $url . '"></script>';
            else {
                // если использовать тему
                if ($attr[2])
                    $src = $host . '/themes/' . Gear::$app->config->get('THEME') . '/js/'. $url;
                else
                    $src = $host . '/themes/gear/js/'. $url;
                $text .= '<script' . $type . ' src="'. $src . '"></script>';
            }
            if ($index < $count - 1) $text .= _n_t;
            $index++;
        }

        return $text . $gearScript;
    }

    /**
     * Добавление объявления css в документ
     * 
     * @param  string $text текст css
     * @param  string $type тип MIME
     * @param  string $media
     * @param  string $attribs атрибуты стиля
     * @return mixed
     */
    public function addStyleDeclaration($text, $type = 'text/css', $media = '', $attribs = '')
    {
        $this->_styleDec[] = array($text, $type, $media, $attribs);
    }

    /**
     * Возращает объявления css документа
     * (добавленные через addStyleDeclaration)
     * 
     * @return array
     */
    public function getStyleDeclaration()
    {
        return $this->_styleDec;
    }

    /**
     * Возращает "<style ..." документа
     * 
     * @return string
     */
    public function getStyleDeclarationTags()
    {
        if (empty($this->_styleDec)) return '';

        // если отладка
        if (Gear::$app->debug) {
            Gear::$app->debug->info($this->_styleDec, 'Для тега "style"');
        }

        $text = '';
        $count = sizeof($this->_styleDec);
        for ($i = 0; $i < $count; $i++) {
            $css = $this->_styleDec[$i];
            if ($css[1])
                $type = ' type="' . $css[1] . '"';
            else
                $type = '';
            if ($css[2])
                $media = ' media="' . $css[2] . '" ';
            else
                $media = ' ';

            $text .= '<style' . $type . $media . $css[3] . ">" . _n_t;
            $text .= $css[0];
            $text .= _n_t . "</style>";
        }

        return $text;
    }

    /**
     * Добавление css в документ
     * 
     * @param  string $url ресурс css скрипт
     * @param  string $type тип MIME
     * @param  string $media
     * @param  string $attribs атрибуты стиля
     * @param  string $theme использовать тему
     * @return mixed
     */
    public function addStyleSheet($url, $type = 'text/css', $media = '', $attribs = '', $theme = false, $comment = '')
    {
        if (!isset($this->_css[$url]))
            $this->_css[$url] = array();

        $this->_css[$url] = array($url, $type, $media, $attribs, $theme, $comment);
    }

    /**
     * Возращает css документа
     * (добавленную через addStyleSheet)
     * 
     * @return array
     */
    public function getStyleSheet()
    {
        return $this->_css;
    }

    /**
     * Возращает "<link ..." документа
     * 
     * @return string
     */
    public function getStyleSheetTags()
    {
        $text = '';

        if (empty($this->_css) && !Gear::$app->config->get('RSS')) return '';

        $host = Gear::$app->url->host;
        if ($this->_css) {
            $text = _n_t;
            $index = 0;
            $count = sizeof($this->_css);
            foreach ($this->_css as $url => $attr) {
                if ($attr[1])
                    $type = ' type="' . $attr[1] . '"';
                else
                    $type = '';
                if ($attr[2])
                    $media = ' media="' . $attr[2] . '" ';
                else
                    $media = ' ';
                // используется тема
                if ($attr[4])
                    $href = '/' . Gear::$app->config->get('THEME') . '/css/' . $url;
                else
                    $href = '/gear/css/' . $url;
                // если есть комментарий
                if ($attr[5])
                    $text .= "<!-- " . $attr[5] . " -->" . _n_t;
                $text .= '<link rel="stylesheet"' . $media . $attr[3] . $type . ' href="' . $host . '/themes' . $href . '" />';
                if ($index < $count - 1)
                    $text .= _n_t;
                $index++;
            }
        }
        // если есть RSS канал
        if (Gear::$app->config->get('RSS')) {
            $router = Gear::$app->config->getFrom('ROUTER', 'route');
            if ($router)
                if (isset($router['rss']))
                    $text .= _n_t . '<link rel="alternate" type="application/rss+xml" title="' . Gear::$app->config->get('NAME') . ' RSS" href="' . $host . $router['rss']['value'] . '" />';
        }

        return $text;
    }

    /**
     * Возращает статус ошибки страницы
     *
     * @param  $code $code если код не указан возращает текущий код
     * @return array
     */
    public function getErrorCode($code = 0)
    {
        if (empty($code))
            $code = $this->code;
        if (isset($this->_errorCodes[$code]))
            return $this->_errorCodes[$code];

        return false;
    }

    /**
     * Устанавливает статус возращаемый клиенту
     *
     * @param  integer $code статус
     * @return mixed
     */
    public function setCode($code)
    {
        if (($ecode = $this->getErrorCode($code)) !== false) {
            header('HTTP/1.1 ' . $code . ' ' . $ecode[0]);
        }
        // если вывод ошибок в шаблоне статьи
        if (Gear::$app->config->get('ERROR/ARTICLE')) {
            $this->title = GText::_('Error') . ' ' . $code;
            // подключаем шаблон с кодом ошибки
            $this->setText(Gear::content('error_article_' . $code . '.tpl'));
        }
        $this->code = $code;
    }

    /**
     * Установка атрибутов компоненту в документе
     * (через GPage)
     * 
     * @param  string $name класс атрибута
     * @param  array $attr атрибуты компонента
     * @return mixed
     */
    public function component($name, $attr)
    {
        // использует GPage для скрытия компонента
        if (!isset($attr['hidden']))
            $attr['hidden'] = false;

        $this->_components[$name] = $attr;
    }

    /**
     * Проверка существования атрибутов компонента в документе
     * (через GPage)
     * 
     * @param  string $name класс атрибута
     * @return mixed
     */
    public function isComponent($name)
    {
        if (isset($this->_components[$name]))
            return $this->_components[$name];
        else
            return false;
    }

    /**
     * Устанавливает данные статьи
     *
     * @param  array $data данные статьи
     * @return array 
     */
    public function setFromArticle($data)
    {
        // если есть настройки компонентов
        if (!empty($data['component_attributes'])) {
            $this->componentAttrs = json_decode($data['component_attributes'], true);
        }
        // если в документе есть блоки лендинга
        if (isset($data['article_landing']))
            $this->isLanding = $data['article_landing'];
        // если есть идин. шаблона статьи
        if (isset($data['template_id']))
            $this->templateId = $data['template_id'];
        // если есть идин. страницы
        if (isset($data['page_id']))
            $this->pageId = $data['page_id'];
        // если есть идин. статьи
        if (isset($data['article_id']))
            $this->id = $data['article_id'];
        // если есть вывод 
        if (isset($data['article_print']))
            $this->print = $data['article_print'];
        // дата правки статьи 
        if (isset($data['article_date']))
            $this->date = $data['article_date'];
        // если есть разбор статьи
        if (isset($data['page_parsing']))
            $this->parsing = $data['page_parsing'];
        // если страница кэшируется
        if (isset($data['article_caching']))
            $this->caching = $data['article_caching'];
        // если есть загаловок для вкладки браузера
        if (isset($data['page_title']))
            $this->title = $data['page_title'];
        // если есть загаловок статьи
        if (isset($data['page_header']))
            $this->header = $data['page_header'];
        // показывать загаловок статьи
        $this->showHeader = !empty($data['article_header']);
        // если есть шаблон статьи
        if (isset($data['template_filename']))
            $this->template = $data['template_filename'];
        // добавление меты
        if (!empty($data['page_meta_description']))
            $this->addMeta('description', str_replace(array("\r\n", "\n", "\r"), '', trim($data['page_meta_description'])));
        if (!empty($data['page_meta_keywords']))
            $this->addMeta('keywords', str_replace(array("\r\n", "\n", "\r"), '', trim($data['page_meta_keywords'])));
        if (!empty($data['page_meta_robots']))
            $this->addMeta('robots', $data['page_meta_robots']);
        if (!empty($data['page_meta_author']))
            $this->addMeta('author', $data['page_meta_author']);
        if (!empty($data['page_meta_copyright']))
            $this->addMeta('copyright', $data['page_meta_copyright']);
        if (!empty($data['page_meta_generator']))
            $this->addMeta('generator', $data['page_meta_generator']);

        $this->_data = $data;
    }

    /**
     * Возращает счётчики посещений
     *
     * @return mixed
     */
    public function getCounter()
    {
        if (Gear::$app->config->get('NOTINDEX'))
            return '';
        else
            return Gear::content('counter.tpl', false, true);
    }

    /**
     * Возращает данные статьи по ключу (если он есть)
     *
     * @param  string $key ключ
     * @return mixed
     */
    public function getData($key = '')
    {
        if ($key)
            if (isset($this->_data[$key]))
                return $this->_data[$key];
            else
                return '';

        return $this->_data;
    }

    /**
     * Устанавливает текст статьи
     *
     * @param  string $text текст статьи
     * @return void
     */
    public function setText($text = '')
    {
        $this->_text = $text;
    }

    /**
     * Устанавливает состояние статьи
     *
     * @param  string $state состояние
     * @return void
     */
    public function setState($state)
    {
        GDocument::$state = $state;
    }

    /**
     * Возращает текст статьи
     *
     * @return string
     */
    public function getText()
    {
        return $this->_text;
    }

    /**
     * Если страница отображаема
     *
     * @return boolean
     */
    public function isOk()
    {
        return $this->code == 200;
    }

    /**
     * Проверяет есть ли статья
     *
     * @return boolean
     */
    public function isEmpty()
    {
        return empty($this->_text);
    }

    /**
     * Возращает все теги статьи (видео, изображаения, документы)
     *
     * @return array
     */
    public function getTags()
    {
        return $this->images + $this->documents + $this->video;
    }

    /**
     * Возращает контент статьи для вывода
     *
     * @return string
     */
    public function getComponents($html)
    {
        // если в статье есть компоненты
        if ($this->parsing) {
                $cmp = new GComponent();
                $cmp->tagSearch = 'component';
                $cmp->html = $html;
                return $cmp->getContent();
        }

        if (empty($html))
            return GText::_('Page in the publication process');

        return $html;
    }

    /**
     * Возращает атрибуты компонента полученные из страницы статьи
     *
     * @return array
     */
    public function getComponentAttrs($componentId, $attr = array())
    {
        if (isset($this->componentAttrs[$componentId])) {
            if ($attr)
                return array_merge($attr, $this->componentAttrs[$componentId]);
            else
                return $this->componentAttrs[$componentId];
        }

        return false;
    }

    /**
     * Разбиение текста на части
     *
     * @return array
     */
    public function breakText()
    {
        if (strpos($this->html, self::TAG_BREAK) !== false)
            return explode(self::TAG_BREAK, $this->html);
        else
            return array();
    }

    /**
     * Замена глольных тегов
     *
     * @return string
     */
    public function replaceAlias($text)
    {
        $chost = Gear::$app->url->scheme . '://' . Gear::$app->config->get('HOST');
        $host = Gear::$app->url->host;

        $tags = array(
            '{scheme}' => Gear::$app->url->scheme . '://',
            '{lang}'   => GFactory::getLanguage('alias'),
            '{chost}'  => $chost,
            '{host}'   => $host,
            '{theme}'  => $host . '/' . PATH_THEME . Gear::$app->config->get('THEME'),
            '{ctheme}' => $chost . '/' . PATH_THEME . Gear::$app->config->get('THEME'),
            '{gear}'   => $chost . '/' . PATH_THEME . 'gear'
        );

        return strtr($text, $tags);
    }

    /**
     * Возращает текста статьи
     *
     * @return void
     */
    public function getRenderText()
    {
        // все теги статьи
        $tags = $this->getTags();
        // если есть теги, выполняем подстановку
        if ($tags)
            $this->_text = strtr($this->_text, $tags);

        return $this->html = $this->getComponents($this->_text);
    }

    /**
     * Вывод текста статьи
     *
     * @return void
     */
    public function renderText()
    {
        echo $this->getRenderText();
    }

    /**
     * Вывод ошибки
     *
     * @return void
     */
    public function renderError()
    {
        // если предусмотрен код ошибки
        if (($ecode = $this->getErrorCode()) !== false) {
            // если вывод ошибок в шаблоне статьи
            if (Gear::$app->config->get('ERROR/ARTICLE'))
                echo Gear::content($ecode[2]);
            else
                Gear::content($ecode[1], true);
            
        }
    }
}
?>