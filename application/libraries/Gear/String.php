<?php
/**
 * Gear CMS
 * 
 * Пакет отбработки строк
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: String.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Класс работы со строками
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: String.php 2016-01-01 12:00:00 Gear Magic $
 */
class GString
{
    /**
     * Из UTF в кириллицу
     * 
     * @params string $str строка
     * @return string
     */
    public static function utfToCyr($str)
    {
        $arr_replace_utf = array('\u0410', '\u0430','\u0411','\u0431','\u0412','\u0432',
        '\u0413','\u0433','\u0414','\u0434','\u0415','\u0435','\u0401','\u0451','\u0416',
        '\u0436','\u0417','\u0437','\u0418','\u0438','\u0419','\u0439','\u041a','\u043a',
        '\u041b','\u043b','\u041c','\u043c','\u041d','\u043d','\u041e','\u043e','\u041f',
        '\u043f','\u0420','\u0440','\u0421','\u0441','\u0422','\u0442','\u0423','\u0443',
        '\u0424','\u0444','\u0425','\u0445','\u0426','\u0446','\u0427','\u0447','\u0428',
        '\u0448','\u0429','\u0449','\u042a','\u044a','\u042d','\u044b','\u042c','\u044c',
        '\u042d','\u044d','\u042e','\u044e','\u042f','\u044f');
        $arr_replace_cyr = array('А', 'а', 'Б', 'б', 'В', 'в', 'Г', 'г', 'Д', 'д', 'Е', 'е',
        'Ё', 'ё', 'Ж','ж','З','з','И','и','Й','й','К','к','Л','л','М','м','Н','н','О','о',
        'П','п','Р','р','С','с','Т','т','У','у','Ф','ф','Х','х','Ц','ц','Ч','ч','Ш','ш',
        'Щ','щ','Ъ','ъ','Ы','ы','Ь','ь','Э','э','Ю','ю','Я','я');

        return str_replace($arr_replace_utf, $arr_replace_cyr, $str);
    }

    /**
     * Склонение слова
     * 
     * @params integer $number
     * @params array $titles ('диск', 'диска', 'дисков')
     * @return string
     */
    public static function intMorphy($number, $titles)
    {
        $cases = array (2, 0, 1, 1, 1, 2);
    
        return $titles[ ($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)] ];
    }

    /**
     * Транслит строки
     * 
     * @params string $str строка
     * @params string $lang язык ("ru", "uk")
     * @return string
     */
    public static function convert($str, $lang)
    {
        if (!isset(self::$langs[$lang])) return false;

        return strtr($str, self::$langs[$lang]);
    }

    /**
     * Генерация URL адреса из транслита строки
     * 
     * @params string $str строка
     * @params string $lang язык ("ru", "uk")
     * @return string
     */
    public static function toUrl($str, $lang)
    {
        if (($str = self::convert($str, $lang)) === false) return false;

        // в нижний регистр
        $str = strtolower(trim($str));
        // заменям все ненужное нам на "-"
        $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
        $str = trim($str, '-');

        return $str;
    }

    /**
     * Убрать символы из строки
     * 
     * @params string $str строка
     * @params boolean $tags уберать теги
     * @return string
     */
    public static function stripStr($str, $tags = true)
    {
        if ($tags)
            $str = strip_tags($str);
        $str = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $str);

        return trim($str);
    }

    /**
     * Убрать символы из строки
     * 
     * @params string $str строка
     * @params boolean $tags уберать теги
     * @return string
     */
    public static function stripTitle($str, $tags = true)
    {
        $str = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $str);
        $str = str_replace(array('<br>', '<br/>', '<br />'), ' ', $str);
        if ($tags)
            $str = strip_tags($str);

        return trim($str);
    }

    /**
     * Копирование строки
     * 
     * @params string $subject строка
     * @params string $start начальная позиция
     * @params string $end конечная позиция
     * @params boolean $strip убрать символы
     * @return string
     */
    public static function copyStr($subject, $start, $end, $strip = false)
    {
        $str = mb_substr($subject, $start, $end - $start, 'UTF8');
        if ($strip)
            return self::stripStr($str, false);
        else
            return $str;
    }

    /**
     * Для поиска в sql запросе
     * 
     * @params string $str строка поиска
     * @return string
     */
    public static function toSearchAtSql($str)
    {
        if (strlen($str) == 0) return '';

        $str = strip_tags($str);
        $str = strtolower($str);
        $str = preg_replace('~[^a-z0-9 \x80-\xFF]~i', "", $str);
        $str = trim($str);
 
        return $str;
    }
}
?>