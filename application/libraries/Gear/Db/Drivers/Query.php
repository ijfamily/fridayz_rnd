<?php
/**
 * Обработка SQL запросов сервера базы данных
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Database
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Query.php 2013-12-30 12:00 Gear Magic $
 */

/**
 * Абстрактный класс обработки SQL запросов
 * 
 * @category   Gear
 * @package    Database
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Query.php 2013-12-30 12:00 Gear Magic $
 */
abstract class GDbQueryAbstract
{
    /**
     * Результат запроса - ассоциативный массив
     */
    const SQL_ASSOC  = 1;

    /**
     * Результат запроса - нумерованный массив
     */
    const SQL_NUM    = 2;

    /**
     * Результат запроса - нумерованный массив и 
     * ассоциативный массив
     */
    const SQL_BOTH   = 3;

    /**
     * Результат запроса - массив объектов
     */
    const SQL_OBJECT = 4;

    /**
     * Количество полей в последнем запросе
     * 
     * @var integer
     */
    protected $_countFields = 0;

    /**
     * Количество записей в последнем запросе
     * 
     * @var integer
     */
    protected $_countRecords = 0;

    /**
     * Выполнение запроса (возращает функция Execute)
     * 
     * @var bool
     */
    protected $_executed = false;

    /**
     * Указатель на SQL запрос
     * 
     * @var integer
     */
    protected $_handle;

    /**
     * SQL запрос
     * 
     * @var string
     */
    protected $_sql = '';

    /**
     * Текущей индекс записи
     * 
     * @var integer
     */
    protected $_recordIndex = 0;

    /**
     * Тип возращаемых записей
     * 
     * @var integer
     */
    protected $_typeRecord;

    /**
     * Сылка на экземпляр класса драйвера для работы с базой данных
     *
     * @var mixed
     */
    protected $_db = null;

    /**
     * Конструктор
     * 
     * @param mixed $db cылка на экземпляр класса драйвера для работы с базой данных
     * @param integer $typeRecord тип возращаемой записи
     * @return void
     */
    public function __construct($db = null, $typeRecord = self::SQL_ASSOC)
    {
        if ($db != null)
            $this->_db = $db;
        else
            $this->_db = GFactory::getDb();
        $this->_typeRecord = $typeRecord;
    }

    /**
     * Возращает запись SQL запроса по указаному индексу
     * 
     * @param  integer $recordIndex идекс записи
     * @param  integer $typeRecord тип записи
     * @return mixed
     */
    protected function recordByIndex($recordIndex, $typeRecord = self::SQL_ASSOC)
    {}

    /**
     * Проверка чтения записей SQL запроса
     * 
     * @return bool
     */
    public function eof()
    {
        return $this->_countRecords - 1 < $this->_recordIndex;
    }

    /**
     * Выполнение SQL запроса
     * 
     * @param  string $sql SQL запрос
     * @return integer
     */
    public function execute($sql)
    {}

    /**
     * Обработка escape последовательности символов строки
     * 
     * @param  string $str строка
     * @param  boolean $addQuote добавлять кавычки "'" в начало и в конец строки
     * @return mixed
     */
    public function escapeStr($str, $addQuote = true)
    {
        if ($str === 0 || $str === false) return '0';
        if ($str === true) return '1';
        if ($str == null) return 'NULL';
        if (is_int($str) || empty($str)) return $str;
        if(get_magic_quotes_gpc())
            $str = stripslashes($str);
        if ($addQuote)
            return '\'' . mysql_real_escape_string($str, $this->_db->getHandle()) . '\'';
        else
            return mysql_real_escape_string($str, $this->_db->getHandle());
    }

    /**
     * Возращает первую запись SQL запроса
     * 
     * @return mixed
     */
    public function first()
    {
        $this->_recordIndex = 0;

        return $this->recordByIndex($this->_recordIndex);
    }

    /**
     * Возращает количество обработанных записей в последнем SQL запросе
     * 
     * @return integer
     */
    public function getAffectedRows()
    {}

    /**
     * Возращает количетсво полей в последнем SQL запросе
     * 
     * @return ineteger
     */
    public function getCoundFields()
    {
        return $this->_countFields;
    }

    /**
     * Возращает количетсво записей в последнем SQL запросе
     * 
     * @return ineteger
     */
    public function getCountRecords()
    {
        return $this->_countRecords;
    }

    /**
     * Возращает ошибки обработки SQL запроса
     * 
     * @param  boolean $details детальный вывод ошибки
     * @return mixed
     */
    public function getError($details = false)
    {}

    /**
     * Возращает ошибку при обработки SQL запроса
     * 
     * @param  boolean $details детальный вывод ошибки
     * @return mixed
     */
    public function getFoundRows()
    {}

    /**
     * Возращает последний идин-к добавленной зиписи
     * 
     *@return integer
     */
    public function getLastInsertId()
    {}

    /**
     * Возращает последний SQL запрос
     * 
     * @return string
     */
    public function getSQL()
    {
        return $this->_sql;
    }

    /**
     * Возращает запись SQL запроса
     * 
     * @param  string $sql SQL запрос
     * @return mixed
     */
    public function getRecord($sql = '')
    {}

    /**
     * Возращает записи SQL запроса
     * 
     * @param  string $sql SQL запрос
     * @return mixed
     */
    public function getRecords($sql = '')
    {}

    /**
     * Возращает тип записи
     * 
     * @return integer
     */
    public function getTypeRecord()
    {
        return $this->_typeRecord;
    }

    /**
     * Проверяет является ли 1-я запись в SQL запросе
     * 
     * @return bool
     */
    public function isFirstRecord()
    {
        return $this->_recordIndex == 1;
    }

    /**
     * Проверяет является ли последняя запись в SQL запросе
     * 
     * @return bool
     */
    public function isLastRecord()
    {
        return $this->_countRecords == $this->_recordIndex;
    }

    /**
     * Возращает последнюю запись SQL запроса
     * 
     * @return mixed
     */
    public function last()
    {
        $this->_recordIndex = $this->_countRecords - 1;

        return $this->recordByIndex($this->_recordIndex);
    }

    /**
     * Возращает следующею запись SQL запроса
     * 
     * @return mixed
     */
    public function next()
    {
        return $this->recordByIndex($this->_recordIndex++);
        
    }

    /**
     *  Возращает предыдущую запись SQL запроса
     * 
     * @return mixed
     */
    public function prior()
    {
        if ($this->_recordIndex >= 1)
            $this->_recordIndex--;

        return $this->_recordByIndex($this->_recordIndex);
    }

    /**
     * Установка типа возращаемых записей
     * 
     * @return void
     */
    public function setTypeRecord($typeRecord)
    {
        $this->_typeRecord = $typeRecord;
    }

    /**
     * Сбросить аутоинкремент таблицы
     * 
     * @param  string $table таблица
     * @return boolean
     */
    public function dropAutoIncrement($table)
    {}

    /**
     * Удалить все записи из таблицы
     * 
     * @param  string $table таблица
     * @return boolean
     */
    public function delete($table)
    {}

    /**
     * Удалить все записи из таблицы
     * 
     * @param  string $table таблица
     * @return boolean
     */
    public function clear($table)
    {
        if ($this->delete($table) !== true)
            return false;
        else
            return $this->dropAutoIncrement($table);
    }

    /**
     * Обновление записей таблицы
     * 
     * @param  string $table название таблицы базы данных
     * @param  array $data ассоциативный массив полей таблицы и их значений ("field1" => "value1", ...)
     * @param  string $primary первичный ключ (или любое поле) таблицы
     * @param  mixed $id идентификатор записи(ей) (например: "1" или "1,2,3,4")
     * @param  array $orderby ассоциативный массив полей в сортеровке таблицы ("field1", "field2", ...)
     * @return mixed
     */
    public function update($table, $data = array(), $primary = '', $id = '',  $orderby = array())
    {
        // если нет данных для обновления
        if (empty($data))
            return false;
        // формирование условий в SQL запросе
        $conditions = '';
        if ($id) {
            // если число
            if (is_integer($id))
                $conditions = " \nWHERE `$primary`=$id ";
            // если строка
            else
                $conditions = " \nWHERE `$primary` IN ($id) ";
        }
        // формирование полей и значений для SQL запроса
        $values = array();
        foreach ($data as $field => $value)
            $values[] = $field . '=' . $this->escapeStr($value);
        // формирование SQL запроса
        $sql = 'UPDATE ' . $table . ' SET ' . implode(', ', $values) . $conditions;
        // если есть сортировка
        if (sizeof($orderby) > 0)
            $sql .= " \nORDER BY " . implode(', ', $orderby);

        return $this->execute($sql);
    }

    /**
     * Добавление записей в таблицу
     * 
     * @param string $table название таблицы базы данных
     * @param  array $data ассоциативный массив полей таблицы и их значений ("field1" => "value1", ...)
     * @return mixed
     */
    public function insert($table, $data = array())
    {
        // формирование полей и значений для SQL запроса
        $values = $fields = array();
        foreach ($data as $field => $value) {
            $values[] = $this->escapeStr($value);
            $fields[] = $field;
        }
        // формирование SQL запроса
        $sql = 'INSERT INTO ' . $table . ' ( ' . implode(', ', $fields) . ') VALUES (' . implode(', ', $values) . ')';

        return $this->execute($sql);
    }
}
?>