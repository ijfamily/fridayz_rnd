<?php
/**
 * ���� ������
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Database
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Db.php 2013-12-30 12:00 Gear Magic $
 */

/**
 * ����������� ����� ����������� � ������� ���� ������
 * 
 * @category   Gear
 * @package    Database
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Db.php 2013-12-30 12:00 Gear Magic $
 */
abstract class GDbAbstract
{
    /**
     * ��� ���������� - �� ���������
     */
    const CONNECTION = 1;

    /**
     * ��� ���������� - ���������� ���������� � ��������
     */
    const PERSISTENT = 2;

    /**
     * C��������� � ��������
     * 
     * @var bool
     */
    protected $_connected = false;

    /**
     * ��������� �� ���������� � ��������
     * 
     * @var integer
     */
    protected $_handle = null;

    /**
     * ������ ����������� � �������
     * 
     * @var string
     */
    protected $_password = 'root';

    /**
     * ���� ����������� � �������
     * 
     * @var integer
     */
    protected $_port = 3306;

    /**
     * ����� ���� ������
     * 
     * @var string
     */
    protected $_schema = 'test';

    /**
     * IP ��� DNS ��� �������
     * 
     * @var string
     */
    protected $_server = '';

    /**
     * ��� ������������
     * 
     * @var string
     */
    protected $_username = 'root';

    /**
     * ���������
     * 
     * @var string
     */
    protected $_charset = 'utf8';

    /**
     * �����������
     * 
     * @param  array $init ������ ������������� �������� ����������� � ������� ���� ������
     * @return void
     */
    public function __construct($init)
    {
        // IP ��� DNS ��� �������
        $this->_server = isset($init['DB/SERVER']) ? $init['DB/SERVER'] : $this->_server;
        // ���� ����������� � �������
        $this->_port = isset($init['DB/PORT']) ? $init['DB/PORT'] : $this->_port;
        // ����� ���� ������
        $this->_schema = isset($init['DB/SCHEMA']) ? $init['DB/SCHEMA'] : $this->_schema;
        // ��� ������������
        $this->_username = isset($init['DB/USERNAME']) ? $init['DB/USERNAME'] : $this->_username;
        // ������ ����������� � �������
        $this->_password = isset($init['DB/PASSWORD']) ? $init['DB/PASSWORD'] : $this->_password;
        // ���������
        $this->_charset = isset($init['DB/CHARSET']) ? $init['DB/CHARSET'] : $this->_charset;
    }

    /**
     * ����������
     * 
     * @return void
     */
    public function __destruct()
    {
        // �������� ���������� � ��������
        $this->disconnect();
    }

    /**
     * �������� ���������� � ��������
     * 
     * @return void
     */
    public function connect($type = self::PERSISTENT)
    {}

    /**
     * �������� ���������� � ��������
     * 
     * @return void
     */
    public function disconnect()
    {}

    /**
     * ���������� ������ ��� ��������� ��������� SQL �������
     * 
     * @param  boolean $details ������ ��������� �� ������
     * @return mixed
     */
    public function getError($details = false)
    {}

    /**
     * ��������� ��������� �� ���������� � �������� ���� ������
     * 
     * @return resource
     */
    public function getHandle()
    {
        return $this->_handle;
    }

    /**
     * ��������� ��� ���������� � ��������
     * 
     * @return integer
     */
    public function getType()
    {
        return $this->_type;
    }

    /**
     * �������� ���������� � ��������
     *  
     * @return boolean
     */
    public function isConnected()
    {
        return $this->_connected;
    }

    /**
     * ����� ����� ���� ������
     * 
     * @param  string $schema ����� ���� ������
     * @return void
     */
    public function select($schema = '')
    {}

    /**
     * ��������� ���������
     * 
     * @param  string $charset ��� ���������
     * @return boolean
     */
    public function setCharset($charset = '')
    {}
}
?>