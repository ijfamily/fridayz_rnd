<?php
/**
 * ������� ����������� � ������� ���� ������ "MySQL"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Database
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Db.php 2013-12-30 12:00 Gear Magic $
 */

/**
 * @see GDbAbstract
 */
require_once('Gear/Db/Drivers/Db.php');

/**
 * ����� �������� ����������� � ������� ���� ������ "MySQL"
 * 
 * @category   Gear
 * @package    Database
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Db.php 2013-12-30 12:00 Gear Magic $
 */
class GDb extends GDbAbstract
{
    /**
     * ���� ����������� � �������
     * 
     * @var integer
     */
    protected $_port = 3306;

    /**
     * �������� ���������� � ��������
     * 
     * @param  integer $type ��� ���������� � ��������
     * @return void
     */
    public function connect($type = self::PERSISTENT)
    {
        // ����������� �� ���������� � ��������
        if ($this->isConnected()) return;

        try {
            $this->_handle = @mysql_connect($this->_server . ($this->_port ? ':' . $this->_port : ''), $this->_username, $this->_password);
            if ($this->_handle === false)
                throw new GException('Connection error', 
                                     'Error when making a connection to the server', $this->getError());
            // ����� ����� ���� ������
            if ($this->select() === false)
                throw new GException('Connection error', 
                                     'Error when making a connection to the server', $this->getError());
            // ��������� ���������
            if ($this->setCharset() === false)
                throw new GException('Connection error', 
                                     'Error when making a connection to the server', $this->getError());
        } catch(GException $e) {
            $e->renderException(array(), true);
        }
        // �������� ���������� � ��������
        $this->_connected = true;
        // ��� ���������� � ��������
        $this->_type = $type;
    }

    /**
     * �������� ���������� � ��������
     * 
     * @return void
     */
    public function disconnect()
    {
        // ���� ���������� ���������� ������
        if (is_resource($this->_handle))
            return mysql_close($this->_handle);

        return false;
    }

    /**
     * ���������� ������ ��� ��������� ��������� SQL �������
     * 
     * @param  boolean $details ������ ��������� �� ������
     * @return mixed
     */
    public function getError($details = false)
    {
        if ($details) {
            if (!$this->_handle)
                return array('error' => mysql_error(), 'code' => mysql_errno());
            else
                return array('error' => mysql_error($this->_handle), 'code' => mysql_errno($this->_handle));
        } else
            if (!$this->_handle)
                return mysql_error();
            else
                return mysql_error($this->_handle);
    }

    /**
     * ����� ����� ���� ������
     * 
     * @param  string $schema ����� ���� ������
     * @return boolean
     */
    public function select($schema = '')
    {
        // ���� ����� �� �������, �� ������� �� �������� �����������
        if (empty($schema))
            $schema = $this->_schema;

        return mysql_select_db($schema);
    }

    /**
     * ��������� ���������
     * 
     * @param  string $charset ��� ���������
     * @return boolean
     */
    public function setCharset($charset = '')
    {
        // ���� ��������� �� �������, �� ������� �� �������� �����������
        if (empty($charset))
            $charset = $this->_charset;

         return mysql_set_charset($this->_charset, $this->_handle);
    }
}
?>