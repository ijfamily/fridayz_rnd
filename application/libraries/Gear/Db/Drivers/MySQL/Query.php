<?php
/**
 * Обработка SQL запросов сервера базы данных "MySQL"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Database
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Query.php 2013-12-30 12:00 Gear Magic $
 */

/**
 * @see GDbQueryAbstract
 */
require_once('Gear/Db/Drivers/Query.php');

/**
 * Класс обработки SQL запросов сервера базы данных "MySQL"
 * 
 * @category   Gear
 * @package    Database
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Query.php 2013-12-30 12:00 Gear Magic $
 */
class GDbQuery extends GDbQueryAbstract
{
    /**
     * Возращает запись SQL запроса по указаному индексу
     * 
     * @param  integer $recordIndex идекс записи
     * @param  integer $typeRecord тип записи
     * @return mixed
     */
    protected function recordByIndex($recordIndex, $typeRecord = self::SQL_ASSOC)
    {
        // если записей нет в последнем запросе
        if ($this->_countRecords == 0)
            return array();
        // если тип записи не указан, то берется из конструктора класса
        if (empty($typeRecord))
            $typeRecord = $this->_typeRecord;
        // поиск записи
        if (mysql_data_seek($this->_handle, $recordIndex)) {
            if ($typeRecord == self::SQL_OBJECT)
                $data = mysql_fetch_object($this->_handle);
            else
                $data = mysql_fetch_array($this->_handle, $this->_typeRecord);
        }

        return $data;
    }

    /**
     * Выполнение SQL запроса
     * 
     * @param  string $sql SQL запрос
     * @return boolean
     */
    public function execute($sql)
    {
        $this->_executed = false;
        // збрасываем счётчики записей
        $this->_recordIndex = $this->_countRecords = $this->_countFields = $this->_handle = 0;
        // чтобы знать последний запрос
        $this->_sql = $sql;
        // если SQL запрос не указан
        if (empty($sql))
            return false;
        // ресурс последнего запроса
        $this->_handle = mysql_query($sql);
        // если запрос был успешный, возращает кол-о записей и полей
        if ($this->_handle === false)
            return false;
        if (is_resource($this->_handle)) {
            $this->_countRecords = @mysql_num_rows($this->_handle);
            $this->_countFields = @mysql_num_fields($this->_handle);
        }

        return $this->_executed = true;
    }

    /**
     * Возращает количество обработанных записей в последнем SQL запросе
     * 
     * @return integer
     */
    public function getAffectedRows()
    {
        if (is_resource($this->_handle))
            return mysql_affected_rows($this->_handle);

        return -1;
    }

    /**
     * Возращает ошибку при обработки SQL запроса
     * 
     * @param  boolean $details детальный вывод ошибки
     * @return mixed
     */
    public function getError($details = false)
    {
        if ($details)
            return array('error' => mysql_error($this->_db->getHandle()), 'code' => mysql_errno($this->_db->getHandle()));
        else
            return mysql_error($this->_db->getHandle());
    }

    /**
     * Возращает общее количество записей в последнем SQL запросе
     * 
     * @return mixed
     */
    public function getFoundRows()
    {
        if ($this->execute('SELECT FOUND_ROWS()')) {
            $arr = mysql_fetch_array($this->_handle, self::SQL_NUM);

            return $arr[0];
        }

        return false;
    }

    /**
     * Возращает последний идин-к добавленной зиписи
     * 
     *@return integer
     */
    public function getLastInsertId()
    {
        if (is_resource($this->_db->getHandle()))
            return mysql_insert_id($this->_db->getHandle());

        return false;
    }

    /**
     * Возращает запись SQL запроса
     * 
     * @param  string $sql SQL запрос
     * @return mixed
     */
    public function getRecord($sql = '')
    {
        if (empty($sql))
            return $this->first();

        if ($this->execute($sql))
            return $this->first();

        return false;
    }

    /**
     * Возращает записи SQL запроса
     * 
     * @param  string $sql SQL запрос
     * @return mixed
     */
    public function getRecords($sql = '')
    {
        $data = array();
        if ($sql)
            if (!$this->execute($sql))
                return false;
        while (!$this->eof())
            $data[] = $this->next();

        return $data;
    }

    /**
     * Сбросить аутоинкремент таблицы
     * 
     * @param  string $table таблица
     * @return boolean
     */
    public function dropAutoIncrement($table)
    {
        return $this->execute('ALTER TABLE `' . $table . '` auto_increment=1');
    }

    /**
     * Удалить все записи из таблицы
     * 
     * @param  string $table таблица
     * @return boolean
     */
    public function delete($table)
    {
        return $this->execute('DELETE FROM `' . $table . '`');
    }

    /**
     * Удалить все записи из таблицы
     * 
     * @param  string $table таблица
     * @return boolean
     */
    public function clear($table)
    {
        if ($this->delete($table) !== true)
            return false;
        else
            return $this->dropAutoIncrement($table);
    }
}
?>