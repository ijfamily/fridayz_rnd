<?php
/**
 * Обработка SQL запросов к таблицам базы данных
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Database
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Table.php 2013-12-30 12:00 Gear Magic $
 */

/**
 * @see GDbTableAbstract
 */
require_once('Gear/Db/Drivers/Table.php');

/**
 * Абстрактный класс обработки SQL запросов к таблицам базы данных
 * 
 * @category   Gear
 * @package    Database
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Table.php 2013-12-30 12:00 Gear Magic $
 */
class GDbTable extends GDbTableAbstract
{
    /**
     * Обновление записей таблицы
     * 
     * @param  string $table название таблицы базы данных
     * @param  array $data ассоциативный массив полей таблицы и их значений ("field1" => "value1", ...)
     * @param  string $primary первичный ключ (или любое поле) таблицы
     * @param  mixed $id идентификатор записи(ей) (например: "1" или "1,2,3,4")
     * @param  array $orderby ассоциативный массив полей в сортеровке таблицы ("field1", "field2", ...)
     * @return mixed
     */
    protected function _update($table, $data = array(), $primary = '', $id = '',  $orderby = array())
    {
        // если нет данных для обновления
        if (empty($data))
            return false;
        // формирование условий в SQL запросе
        $conditions = '';
        if ($id) {
            // если число
            if (is_integer($id))
                $conditions = " \nWHERE `$primary`=$id ";
            // если строка
            else
                $conditions = " \nWHERE `$primary` IN ($id) ";
        }
        // формирование полей и значений для SQL запроса
        $values = array();
        foreach ($data as $field => $value)
            $values[] = $field . '=' . $this->query->escapeStr($value);
        // формирование SQL запроса
        $sql = 'UPDATE ' . $table . ' SET ' . implode(', ', $values) . $conditions;
        //die($sql);
        // если есть сортировка
        if (sizeof($orderby) > 0)
            $sql .= " \nORDER BY " . implode(', ', $orderby);

        return $this->query->execute($sql);
    }

    /**
     * Обновление записей таблицы по условию
     * 
     * @param string $table название таблицы базы данных
     * @param  array $data ассоциативный массив полей таблицы и их значений ("field1" => "value1", ...)
     * @param  mixed $where ассоциативный массив ("field1" => "value1", ...) или строка ("field1" = "value1" AND ...)
     * @param  array $orderby ассоциативный массив полей в сортеровке таблицы ("field1", "field2", ...)
     * @return mixed
     */
    protected function _updateBy($table, $data = array(), $where = array(), $orderby = array())
    {
        // если нет данных для обновления
        if (emtpy($data))
            return false;
        // формирование условий в SQL запросе
        $conditions = '';
        // если массив
        if (is_array($where)) {
            // формирование полей и значений для SQL запроса
            $values = array();
            foreach ($where as $field => $value)
                $values[] = $field . '=' . $this->query->escapeStr($value);
            $conditions = " \nWHERE " . implode(' AND ', $values);
        // если строка
        } else
            $conditions = " \nWHERE " . $where;
        // формирование полей и значений для SQL запроса
        $values = array();
        foreach ($data as $field => $value)
            $values[] = $field . '=' . $this->query->escapeStr($value);
        // формирование SQL запроса
        $sql = 'UPDATE ' . $table . ' SET ' . implode(', ', $values) . $conditions;
        // если есть сортировка
        if (sizeof($orderby) > 0)
            $sql .= " \nORDER BY " . implode(', ', $orderby);

        return $this->query->execute($sql);
    }

    /**
     * Удаление записей из таблицы
     * 
     * @param string $table название таблицы базы данных
     * @param  string $primary первичный ключ (или любое поле) таблицы
     * @param  mixed $id идентификатор записи(ей) (например: "1" или "1,2,3,4")
     * @param  mixed $limit количество записей, если нет необходимости - false
     * @return mixed
     */
    protected function _delete($table, $primary = '', $id = '', $limit = false)
    {
        // формирование условий в SQL запросе
        $conditions = '';
        if ($id) {
            // если число
            if (is_integer($id))
                $conditions = " \nWHERE `$primary`=$id ";
            // если строка
            else
                $conditions = " \nWHERE `$primary` IN ($id) ";
        }
        $limit = $limit === false ? '' : ' LIMIT ' . $limit;
        // формирование SQL запроса
        $sql = 'DELETE FROM ' . $table . $conditions . $limit;

        return $this->query->execute($sql);
    }

    /**
     * Удаление записей из таблицы по условию
     * 
     * @param string $table название таблицы базы данных
     * @param  mixed $where ассоциативный массив ("field1" => "value1", ...) или строка ("field1" = "value1" AND ...)
     * @param  mixed $limit количество записей, если нет необходимости - false
     * @return boolean
     */
    protected function _deleteBy($table, $where = array(), $limit = false)
    {
        // формирование условий в SQL запросе
        $conditions = '';
        // если массив
        if (is_array($where)) {
            // формирование полей и значений для SQL запроса
            $values = array();
            foreach ($where as $field => $value)
                $values[] = $field . '=' . $this->query->escapeStr($value);
            $conditions = " \nWHERE " . implode(' AND ', $values);
        // если строка
        } else
            $conditions = " \nWHERE " . $where;
        $limit = $limit === false ? '' : ' LIMIT ' . $limit;
        // формирование SQL запроса
        $sql = 'DELETE FROM ' . $table . $conditions . $limit;

        return $this->query->execute($sql);
    }

    /**
     * Добавление записей в таблицу
     * 
     * @param string $table название таблицы базы данных
     * @param  array $data ассоциативный массив полей таблицы и их значений ("field1" => "value1", ...)
     * @return mixed
     */
    protected function _insert($table, $data = array())
    {
        // формирование полей и значений для SQL запроса
        $values = $fields = array();
        foreach ($data as $field => $value) {
            $values[] = $this->query->escapeStr($value);
            $fields[] = $field;
        }
        // формирование SQL запроса
        $sql = 'INSERT INTO ' . $table . ' ( ' . implode(', ', $fields) . ') VALUES (' . implode(', ', $values) . ')';

        return $this->query->execute($sql);
    }

    /**
     * Возращает запись из таблицы по ее идент.
     * 
     * @param string $table название таблицы базы данных
     * @param  string $primary первичный ключ (или любое поле) таблицы
     * @param  mixed $id идентификатор записи(ей) (например: "1" или "1,2,3,4")
     * @return mixed
     */
    protected function _getRecord($table, $primary = '', $id = '')
    {
        // формирование SQL запроса
        $sql = 'SELECT * FROM ' . $table . ' WHERE ' . $primary . '=' . $this->query->escapeStr($id);

        return $this->query->getRecord($sql);
    }
}
?>