<?php
/**
 * Обработка SQL запросов к таблицам базы данных
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Database
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Table.php 2013-12-30 12:00 Gear Magic $
 */

/**
 * Абстрактный класс обработки SQL запросов к таблицам базы данных
 * 
 * @category   Gear
 * @package    Database
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Table.php 2013-12-30 12:00 Gear Magic $
 */
abstract class GDbTableAbstract
{
    /**
     * Название таблицы
     * @var string
     */
    protected $_table = '';

    /**
     * Первичный ключ таблицы
     * @var string
     */
    protected $_primary = '';

    /**
     * Поля таблицы
     * @var array
     */
    protected $_fields = array();

    /**
     * Сылка на экземпляр класса обработки SQL запросов
     *
     * @var mixed
     */
    public $query = null;

    /**
     * Конструктор
     * 
     * @param  string $table название тыблицы
     * @param  string $primary название первичного поля
     * @param  array $fields поля таблицы
     * @return void
     */
    public function __construct($table, $primary, $fields = array())
    {
        // название таблицы
        $this->_table = $table;
        // первичный ключ таблицы
        $this->_primary = $primary;
        // поля таблицы
        $this->_fields = $fields;
        // экземпляр класса обработки SQL запросов
        $this->query = new GDbQuery();
    }

    /**
     * Обновление записей таблицы
     * 
     * @param  string $table название таблицы базы данных
     * @param  array $data ассоциативный массив полей таблицы и их значений ("field1" => "value1", ...)
     * @param  string $primary первичный ключ (или любое поле) таблицы
     * @param  mixed $id идентификатор записи(ей) (например: "1" или "1,2,3,4")
     * @param  array $orderby ассоциативный массив полей в сортеровке таблицы ("field1", "field2", ...)
     * @return mixed
     */
    protected function _update($table, $data = array(), $primary = '', $id = '', $orderby = array())
    {}

    /**
     * Обновление записей таблицы
     * 
     * @param  array $data ассоциативный массив полей таблицы и их значений ("field1" => "value1", ...)
     * @param  mixed $id идентификатор записи(ей) (например: "1" или "1,2,3,4")
     * @param  array $orderby ассоциативный массив полей в сортеровке таблицы ("field1", "field2", ...)
     * @return mixed
     */
    public function update($data = array(), $id = '', $orderby = array())
    {
        $this->_update($this->_table, $data, $this->_primary, $id, $orderby);
    }

    /**
     * Обновление записей таблицы по условию
     * 
     * @param string $table название таблицы базы данных
     * @param  array $data ассоциативный массив полей таблицы и их значений ("field1" => "value1", ...)
     * @param  mixed $where ассоциативный массив ("field1" => "value1", ...) или строка ("field1" = "value1" AND ...)
     * @param  array $orderby ассоциативный массив полей в сортеровке таблицы ("field1", "field2", ...)
     * @return mixed
     */
    protected function _updateBy($table, $data = array(), $where = array(), $orderby = array())
    {}

    /**
     * Обновление записей таблицы по условию
     * 
     * @param  array $data ассоциативный массив полей таблицы и их значений ("field1" => "value1", ...)
     * @param  mixed $where ассоциативный массив ("field1" => "value1", ...) или строка ("field1" = "value1" AND ...)
     * @param  array $orderby ассоциативный массив полей в сортеровке таблицы ("field1", "field2", ...)
     * @return mixed
     */
    public function updateBy($data = array(), $where = array(), $orderby = array())
    {
        $this->_updateBy($this->_table, $data, $where, $orderby);
    }

    /**
     * Удаление записей из таблицы
     * 
     * @param string $table название таблицы базы данных
     * @param  string $primary первичный ключ (или любое поле) таблицы
     * @param  mixed $id идентификатор записи(ей) (например: "1" или "1,2,3,4")
     * @param  mixed $limit количество записей, если нет необходимости - false
     * @return mixed
     */
    protected function _delete($table, $primary = '', $id = '', $limit = false)
    {}

    /**
     * Удаление записей из таблицы
     * 
     * @param  mixed $id идентификатор записи(ей) (например: "1" или "1,2,3,4")
     * @param  mixed $limit количество записей, если нет необходимости - false
     * @return mixed
     */
    public function delete($id = '', $limit = false)
    {
        return $this->_delete($this->_table, $this->_primary, $id, $limit);
    }

    /**
     * Удаление записей из таблицы по условию
     * 
     * @param string $table название таблицы базы данных
     * @param  mixed $where ассоциативный массив ("field1" => "value1", ...) или строка ("field1" = "value1" AND ...)
     * @param  mixed $limit количество записей, если нет необходимости - false
     * @return boolean
     */
    protected function _deleteBy($table, $where = array(), $limit = false)
    {}

    /**
     * Удаление записей из таблицы по условию
     * 
     * @param  mixed $where ассоциативный массив ("field1" => "value1", ...) или строка ("field1" = "value1" AND ...)
     * @param  mixed $limit количество записей, если нет необходимости - false
     * @return boolean
     */
    public function deleteBy($where = array(), $limit = false)
    {
        return $this->_deleteBy($this->_table, $where, $limit);
    }

    /**
     * Добавление записей в таблицу
     * 
     * @param string $table название таблицы базы данных
     * @param  array $data ассоциативный массив полей таблицы и их значений ("field1" => "value1", ...)
     * @return mixed
     */
    protected function _insert($table, $data = array())
    {}

    /**
     * Добавление записей в таблицу
     * 
     * @param  array $data ассоциативный массив полей таблицы и их значений ("field1" => "value1", ...)
     * @return mixed
     */
    public function insert($data = array())
    {
        return $this->_insert($this->_table, $data);
    }

    /**
     * Возращает последний идин-к добавленной зиписи
     * 
     *@return integer
     */
    public function getLastInsertId()
    {
        return $this->query->getLastInsertId();
    }

    /**
     * Удалить все записи из таблицы
     * 
     * @return boolean
     */
    public function clear()
    {
        return $this->query->clear($this->_table);
    }

    /**
     * Сбросить аутоинкремент таблицы
     * 
     * @return boolean
     */
    public function dropAutoIncrement()
    {
        return $this->query->dropAutoIncrement($this->_table);
    }

    /**
     * Возращает запись из таблицы по ее идент.
     * 
     * @param string $table название таблицы базы данных
     * @param  string $primary первичный ключ (или любое поле) таблицы
     * @param  mixed $id идентификатор записи(ей) (например: "1" или "1,2,3,4")
     * @return mixed
     */
    protected function _getRecord($table, $primary = '', $id = '')
    {}

    /**
     * Возращает запись из таблицы по ее идент.
     * 
     * @param  mixed $id идентификатор записи(ей) (например: "1" или "1,2,3,4")
     * @return mixed
     */
    public function getRecord($id = '')
    {
        return $this->_getRecord($this->_table, $this->_primary, $id);
    }
}
?>