<?php
/**
 * ���� ������
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Database
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Db.php 2013-12-30 12:00 Gear Magic $
 */

/**
 * @see GDbAbstract
 */
require_once('Gear/Db/Drivers/Db.php');

/**
 * ����� ����������� � ������� ���� ������
 * 
 * @category   Gear
 * @package    Database
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Db.php 2013-12-30 12:00 Gear Magic $
 */
class GDb extends GDbAbstract
{
    /**
     * ������� ����������� � "MySQL"
     */
    const DRIVER_MYSQL   = 'MySQL';

    /**
     * ������� ����������� � "MSSQL"
     */
    const DRIVER_MSSQL   = 'MSSQL';

    /**
     * ������� ����������� � "MYSQLi"
     */
    const DRIVER_MYSQLI  = 'MYSQLI';

    /**
     * ������� ����������� � "ODBC"
     */
    const DRIVER_ODBC    = 'ODBC';

    /**
     * ������� ����������� � "SQLite"
     */
    const DRIVER_SQLITE  = 'SQLite';

    /**
     * ������� ����������� � "Oracle server"
     */
    const DRIVER_OCI8    = 'OCI8';

    /**
     * ������� ����������� � "PostgreSQL"
     */
    const DRIVER_POSTGRE = 'PostgreSQL';

    /**
     * ����� �� ��������� ������ 
     *
     * @var mixed
     */
    protected static $_instance = null;

    /**
     * ����� �� ��������� ������ �������� ��� ������ � ����� ������
     *
     * @var mixed
     */
    protected $_db = null;

    /**
     * �����������
     * 
     * @param  array $init ������ ������������� �������� ����������� � ������� ���� ������
     * @param  integer $driver ������� ����������� � ������� ���� ������ (�� ��������� � "MySQL")
     * @return void
     */
    public function __construct($init, $driver = self::DRIVER_MYSQL)
    {
        parent::__construct($init);

        // ������������� ��������
        $this->_driver = isset($init['DB_DRIVER']) ? $init['DB_DRIVER'] : $driver;
        // ������������� ������� ��� ������ � ����� ������
        $this->loadDriver($this->_driver);
        // ������������� �������� ����������� � ������� ���� ������
        $this->_db = new GDbDriver($init);
        
    }

    /**
     * ������������� ������� ��� ������ � ����� ������
     * 
     * @param  string $driver ������� ����������� � ������� ���� ������
     * @return void
     */
    protected function loadDriver($driver)
    {
        // ����������� �������� ��� ������ � �������� ���� ������
        require_once('Drivers/' . $driver . '/Db.php');
        // ����������� �������� ��� ������ � SQL ��������� ���� ������
        require_once('Drivers/' . $driver . '/Query.php');
        // ����������� �������� ��� ������ � SQL ��������� ���� ������
        require_once('Drivers/' . $driver . '/Table.php');
    }


    /**
     * �������� ���������� � ��������
     * 
     * @param  integer $type ��� ���������� � ��������
     * @return void
     */
    public function connect($type = GDbDriver::TYPE_PERSISTENT)
    {
        $this->_db->connect($type);
    }

    /**
     * �������� ���������� � ��������
     * 
     * @return void
     */
    public function disconnect()
    {
        if ($this->_db != null)
            return $this->_db->disconnect();
    }

    /**
     * ���������� ������ ��� ��������� ��������� SQL �������
     * 
     * @param  boolean $details ������ ��������� �� ������
     * @return mixed
     */
    public function getError($details = false)
    {
        return $this->_db->getError($details);
    }

    /**
     * ��������� ��������� �� ���������� � �������� ���� ������
     * 
     * @return resource
     */
    public function getHandle()
    {
        return $this->_db->getHandle();
    }

    /**
     * ���������� ��������� ������� ����������� � �������
     * 
     * @return string
     */
    public function getDriver()
    {
        return $this->_driver;
    }

    /**
     * ��������� ��������� �� ��������� ��������� ������ (���� ����� �� ������, ������� ���)
     * 
     * @param  array $init ������ ������������� �������� ����������� � ������� ���� ������
     * @param  integer $driver ������� ����������� � ������� ���� ������ (�� ��������� � "MySQL")
     * @return mixed
     */
    public static function getInstance($init, $driver = self::DRIVER_MYSQL)
    {
        if (null === self::$_instance) {
            self::$_instance = new self($init, $driver);
        }

        return self::$_instance;
    }

    /**
     * ��������� ��������� �� ��������� ���������� ������
     *
     * @return mixed
     */
    public static function getObject()
    {
        return self::$_instance;
    }

    /**
     * ����� ����� ���� ������
     * 
     * @param  string $schema ����� ���� ������
     * @return boolean
     */
    public function select($schema = '')
    {
        return $this->_db->select($schema);
    }

    /**
     * ��������� ���������
     * 
     * @param  string $charset ��� ���������
     * @return boolean
     */
    public function setCharset($charset = '')
    {
        return $this->_db->setCharset($charset);
    }
}
?>