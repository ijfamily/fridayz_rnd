<?php
/**
 * Gear
 *
 * ������� ���� ������
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Database
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Driver.php 2013-12-30 12:00 Gear Magic $
 */

/**
 * ����� �������� ����������� � ������� ���� ������
 * 
 * @category   Gear
 * @package    Database
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Driver.php 2013-12-30 12:00 Gear Magic $
 */
class GDbDriver
{
    /**
     * ������� ����������� � "MySQL"
     */
    const MYSQL   = 'MySQL';

    /**
     * ������� ����������� � "MSSQL"
     */
    const MSSQL   = 'MSSQL';

    /**
     * ������� ����������� � "MYSQLi"
     */
    const MYSQLI  = 'MYSQLI';

    /**
     * ������� ����������� � "ODBC"
     */
    const ODBC    = 'ODBC';

    /**
     * ������� ����������� � "SQLite"
     */
    const SQLITE  = 'SQLite';

    /**
     * ������� ����������� � "Oracle server"
     */
    const OCI8    = 'OCI8';

    /**
     * ������� ����������� � "PostgreSQL"
     */
    const POSTGRE = 'PostgreSQL';

    /**
     * �������� ��������
     *
     * @var string
     */
    public static $name = '';

    /**
     * ��������� �����������
     *
     * @var array
     */
    public static $config = array();

    /**
     * ����� �� ��������� ������ ���� ������
     *
     * @var object
     */
    public static $db = null;


    /**
     * ������������� ������� ��� ������ � ����� ������
     * 
     * @param  string $driver ������� ����������� � ������� ���� ������
     * @return void
     */
    public static function loadDriver($driver)
    {
        // ����������� �������� ��� ������ � �������� ���� ������
        require_once('Drivers/' . $driver . '/Db.php');
        // ����������� �������� ��� ������ � SQL ��������� ���� ������
        require_once('Drivers/' . $driver . '/Query.php');
        // ����������� �������� ��� ������ � SQL ��������� ���� ������
        require_once('Drivers/' . $driver . '/Table.php');
    }


    /**
     * ��������� ��������� �� ��������� ������ ���� ������
     * 
     * @param  array $config ��������� �����������
     * @param  string $driver �������� �������� ���� �����
     * @return object
     */
    public static function get($config, $driver = self::MYSQL)
    {
        // �������� ��������
        self::$name = isset($config['DB_DRIVER']) ? $config['DB_DRIVER'] : $driver;
        // ��������� �����������
        self::$config = $config;
        // ������������� ������� ��� ������ � ����� ������
        self::loadDriver(self::$name);

        // ������������� �������� ����������� � ������� ���� ������
        return self::$db = new GDb($config);
    }
}
?>