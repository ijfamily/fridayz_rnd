<?php
/**
 * Gear CMS
 * 
 * Пакет аутентификации пользователя
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Auth.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Класс аутентификации пользователя
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Auth.php 2016-01-01 12:00:00 Gear Magic $
 */
class GAuth
{
     /**
     * Конструктор класса
     * 
     * @return void
     */
    public function __construct()
    {
        $this->sess = GFactory::getSession();
        $this->sess->start();
    }

     /**
     * Выход пользователя
     * 
     * @return void
     */
    public function signOut()
    {
        setcookie('user_signin', 0, 0, '/');
        setcookie('user_name', '', 0, '/');
        $this->setId(null);
        $this->setUser(null);
        $this->setNetWork(null);
    }

     /**
     * Аутентификацию пользователя
     * 
     * @return void
     */
    public function signIn($userId, $data)
    {
        setcookie('user_signin', 1, 0, '/');
        setcookie('user_name', $data['profile_first_name'] . ' ' . $data['profile_last_name'], 0, '/');
        $this->setId($userId);
        $this->setUser($data);
    }

     /**
     * Проверка пользователя на аутентификацию
     * 
     * @return boolean
     */
    public function check()
    {
        return $this->sess->get('user_id', false) != false;
    }

     /**
     * Возращает идентификатор пользователя
     * 
     * @return integer
     */
    public function getId()
    {
        return $this->sess->get('user_id', false);
    }

     /**
     * Установка идентификатора пользователя
     * 
     * @param  integer $id идентификатора пользователя
     * @return void
     */
    public function setId($id)
    {
        return $this->sess->set('user_id', $id);
    }

     /**
     * Возращает данные пользователя
     * 
     * @return mixed
     */
    public function getUser()
    {
        return $this->sess->get('user', false);
    }

     /**
     * Возращает имя пользователя
     * 
     * @return mixed
     */
    public function getUserName()
    {
        $user = $this->getUser();
        if (!$user) return '';

        return $user['profile_first_name'] . ' ' . $user['profile_last_name'];
    }

     /**
     * Возращает шаблон данных пользователя
     * 
     * @param  string $text шаблон для отображения данных пользователя
     * @return string
     */
    public function getUserTemplate($text)
    {
        $user = $this->getUser();
        if (!$user) return '';

        if (empty($user['profile_avatar']))
            $user['profile_avatar'] = '/data/images/profile-photo-none.png';
        else {
            if ($user['profile_avatar'] == 'https://ulogin.ru/img/photo.png')
                $user['profile_avatar'] = '/data/images/profile-photo-none.png';
        }

        return str_replace(
            array('{fname}', '{lname}', '{avatar}'),
            array($user['profile_first_name'], $user['profile_last_name'], $user['profile_avatar']),
            $text
        );
    }

     /**
     * Установка данных пользователя
     * 
     * @param  array $data данные пользователя
     * @return void
     */
    public function setUser($data)
    {
        $this->sess->set('user', $data);
    }

     /**
     * Возращает соц.сеть через которую была авторизация
     * 
     * @return mixed
     */
    public function getNetwork()
    {
        return $this->sess->get('network', false);
    }

     /**
     * Возращает e-mail пользователя
     * 
     * @return mixed
     */
    public function getEmail()
    {
        $user = $this->getUser();
        if (!$user) return '';

        if (isset($user['contact_email']))
            return $user['contact_email'];
        else
            return '';
    }

     /**
     * Возращает название соц.сети через которую была авторизация
     * 
     * @return string
     */
    public function getNetworkName()
    {
        if (($network = $this->getNetwork()) != false) {
            return $network['network_name'];
        } else
            return '';
    }

     /**
     * Устанавливает соц.сеть через которую была авторизация
     * 
     * @param  array $data данные соц.сети
     * @return void
     */
    public function setNetWork($data)
    {
        $this->sess->set('network', $data);
    }
}
?>