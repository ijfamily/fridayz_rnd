<?php
/**
 * Gear CMS
 * 
 * Пакет обработки запросов форм
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Input.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Класс обработки запросов форм
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Input.php 2016-01-01 12:00:00 Gear Magic $
 */
class GInput
{
    /**
     * Сылка на экземпляр класса
     *
     * @var mixed
     */
    protected static $_instance = null;

    /**
     * Данные запроса
     *
     * @var string
     */
    protected $_data = array();

    /**
     * Метод запроса
     *
     * @var string
     */
    public $method = '';

    /**
     * Указатель на экземпляр класса файла
     *
     * @var object
     */
    public $file = null;


    /**
     * Конструктор
     * 
     * @return void
     */
    public function __construct()
    {
        // метода запроса
        $this->method = $_SERVER['REQUEST_METHOD'];
        // данные запроса
        $this->_data = &$_REQUEST;
        if (sizeof($_FILES) > 0)
            $this->file = new GInputFile();
    }

    /**
     * Возращает указатель на созданный экземпляр класса (если класс не создан, создаст его)
     * 
     * @return mixed
     */
    public static function getInstance()
    {
        if (null === self::$_instance)
            self::$_instance = new self();

        return self::$_instance;
    }

    /**
     * Сброс полей формы (в том случаи если у поля свойство "resetable" = true)
     * 
     * @param  array $fields поля формы
     * @return void
     */
    public function reset(&$fields)
    {
        foreach ($fields as $field => $attr) {
            if ($this->has($field)) {
                $resetable = isset($fields[$field]['resetable']) ? $fields[$field]['resetable'] : true;
                if ($resetable)
                    $fields[$field]['value'] = $fields[$field]['default'];
                $this->_data[$field] = '';
            }
        }
    }

    /**
     * Проверка полей формы
     * 
     * @param  array $fields поля формы
     * @param  string $path локализация языка (название полей и исключений)
     * @return void
     */
    public function check(&$fields, $path)
    {
        $error = false;
        foreach ($fields as $field => $attr) {
            // название поля
            $title = empty($attr['title']) ? '' : GText::_($attr['title'], $path);
            // используется ли поле
            $use = isset($attr['use']) ? $attr['use'] : false;
            // проверять ли поле
            $check = isset($attr['check']) ? $attr['check'] : false;
            // позволить не вводить текст
            $allowBlank = isset($attr['allowBlank']) ? $attr['allowBlank'] : true;

            // используется ли поле
            if (!$check) continue;
            // если поле затребовано, но его нет в запросе
            if (!$this->has($field) && !$allowBlank) {
                GMessage::to('form', 'error', GText::_('You must fill the field "%s"', 'exception', $title));
                $fields[$field]['value'] = '';
                $error = true;
                continue;
            }
            
            // убрать html текст
            $strip = isset($attr['strip']) ? $attr['strip'] : false;
            // проверка регулярным выражением
            $match = isset($attr['match']) ? $attr['match'] : false;
            // мин. длина
            $minLength = isset($attr['minLength']) ? $attr['minLength'] : false;
            // макс. длина
            $maxLength = isset($attr['maxLength']) ? $attr['maxLength'] : false;
            // значение
            $value = $this->get($field);
            // введено ли значение
            $isEmpty = empty($value);
            // поле для вывода на форму
            if (is_string($value))
                $fields[$field]['value']= htmlspecialchars($value);
            // размер значения
            if (is_string($value))
                $length = mb_strlen($value, 'UTF-8');
            else
            if (is_array($value))
                $length = sizeof($value);
            else
                $length = 0;
            // если необходимо заполнить поле
            if (!$allowBlank && $isEmpty) {
                GMessage::to('form', 'error', GText::_('You must fill the field "%s"', 'exception', $title));
                $error = true;
                continue;
            }
            // убрать html текст
            if ($strip)
                if (!$allowBlank || ($allowBlank && !$isEmpty))
                    $this->_data[$field] = strip_tags($value);
            // проверка регулярным выражением
            if ($match)
                if (!$allowBlank || ($allowBlank && !$isEmpty))
                    if (!preg_match($match, $value)) {
                        GMessage::to('form', 'error', GText::_('You did not fill the field "%s"', 'exception', $title));
                        $error = true;
                        continue;
                    }
            // мин. длина
            if ($minLength !== false)
                if (!$allowBlank || ($allowBlank && !$isEmpty))
                    if ($minLength > $length) {
                        GMessage::to('form', 'error', GText::_('The length of the field "%s" must be from %s characters', 'exception', array($title, $minLength)));
                        $error = true;
                        continue;
                    }
            // макс. длина
            if ($maxLength !== false)
                if (!$allowBlank || ($allowBlank && !$isEmpty))
                    if ($maxLength < $length) {
                        GMessage::to('form', 'error', GText::_('The max length of the field "%s" must be no more than %s characters', 'exception', array($title, $maxLength)));
                        $error = true;
                        continue;
                    }
            $this->_data[$field] = $value;
        }

        return !$error;
    }

    /**
     * Вызов метода класса
     * 
     * @param  array $name название метода
     * @param  array $arguments аргументы метода
     * @return void
     */
    public function __call($name, $arguments)
    {
        if (empty($arguments[1]))
            $default = '';
        else
            $default = $arguments[1];
        switch ($name) {
            case 'getInt': return (int) $this->get($arguments[0], $arguments[1]);

            case 'getBool': return (bool) $this->get($arguments[0], $arguments[1]);

            case 'getString': return (string) $this->get($arguments[0], $arguments[1]);

            case 'getFloat': return (float) $this->get($arguments[0], $arguments[1]);
        }
    }

     /**
     * Возращает количество полей в запросе
     * 
     * @return integer
     */
    public function count()
    { 
        return count($this->_data);
    }

     /**
     * Проверка существования переменной $name в запросе
     * 
     * @param  string $name имя переменной
     * @return boolean
     */
    public function has($name)
    { 
        return isset($this->_data[$name]);
    }

     /**
     * Возращает true если есть данные в запросе
     * 
     * @return boolean
     */
    public function hasData()
    {
        return (bool)(sizeof($this->_data) > 0);
    }

     /**
     * Проверка метода запроса (POST, GET)
     * 
     * @return boolean
     */
    public function isMethod($method)
    {
        return $this->method == $method;
    }

    /**
     * Возращает значение ключа из метода POST, если ключ
     * не найден - возращает значение по умолчанию
     * 
     * @param  string $key ключ в массиве GET
     * @param  string $default значение по умолчанию
     * @return mixed
     */
    public function get($name, $default = '')
    {
        if (isset($this->_data[$name]))
            return $this->_data[$name];
        else
            return $default;
    }

    /**
     * Возращает значение ключа из метода POST, если ключ
     * не найден - возращает значение по умолчанию
     * 
     * @param  string $key ключ в массиве GET
     * @param  string $default значение по умолчанию
     * @return mixed
     */
    public function getInt($name, $default = '')
    {
        return (int) $this->get($name, $default);
    }

    /**
     * Возращает значение ключа из метода POST, если ключ
     * не найден - возращает значение по умолчанию
     * 
     * @param  string $key ключ в массиве GET
     * @param  string $default значение по умолчанию
     * @return mixed
     */
    public function getBool($name, $default = '')
    {
        return (bool) $this->get($name, $default);
    }

    /**
     * Проверка существования $name
     * 
     * @param  string $name название
     * @return boolean
     */
    public function isEmpty($name)
    {
        return empty($this->_data[$name]);
    }

    /**
     * Возращает данные
     * 
     * @return mixed
     */
    public function getData()
    {
        return $this->_data;
    }

    /**
     * Устанавливает значение ключа для $_REQUEST
     * 
     * @param  string $name ключ
     * @param  string $value значение ключа, если null - удаляет ключ
     * @return mixed
     */
    public function set($name, $value = null)
    {
        if ($value == null)
            unset($this->_data[$name]);
        else
            $this->_data[$name] = $value;
    }

    /**
     * Возращает метод запроса
     * 
     * @return string
     */
    public function getMethod()
    {
        return strtoupper($_SERVER['REQUEST_METHOD']);
    }

    /**
     * Возращает часть sql запроса для проверки ip адреса в базе
     * 
     * @param  string $ipAddr ip адрес
     * @param  string $fieldIp поле с ip адресом
     * @param  string $fieldMask поле с маской
     * @return string
     */
    public function getQueryIp($ipAddr, $fieldIp, $fieldMask)
    {
        $ipInINT = ip2long($ipAddr);
        $filter  = '';
        $mask      = 0;
        $separator = '';
        for ($bits = 0; $bits <= 32; $bits++ ) {
            $network = $ipInINT & $mask;
            $filter .= $separator . sprintf("($fieldIp = %u AND $fieldMask = %u)", $network, $mask);
            //print sprintf("($fieldIp = %s AND $fieldMask = %s)", long2ip($network), long2ip($mask)) . '<br>';
            //exit;
            $separator = ' OR ';
            $mask = ($mask >> 1) | 2147483648;
        }

        return $filter;
    }
}


/**
 * Класс обработки запросов форм
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Input.php 2016-01-01 12:00:00 Gear Magic $
 */
class GInputFile
{
    /**
     * Конструктор
     * 
     * @return void
     */
    public function __construct()
    {
        // данные запроса
        $this->_data = &$_FILES;
    }

    /**
     * Возращает значение ключа из $_FILES, если $name
     * не найден - возращает false
     * 
     * @param  string $name имя поля выбора файла
     * @return mixed
     */
    public function get($name)
    {
        if (isset($this->_data[$name]))
            return $this->_data[$name];
        else
            return false;
    }

    /**
     * Возращает Mime-тип файла
     * 
     * @param  string $name имя поля выбора файла
     * @return mixed
     */
    public function getType($name)
    {
        if (($file = $this->get($name)) === false)
            return false;
        else
            return $file['type'];
    }

    /**
     * Возращает размер в байтах принятого файла
     * 
     * @param  string $name имя поля выбора файла
     * @return mixed
     */
    public function getSize($name)
    {
        if (($file = $this->get($name)) === false)
            return false;
        else
            return $file['size'];
    }

    /**
     * Возращает имя загружаемого файла
     * 
     * @param  string $name имя поля выбора файла
     * @return mixed
     */
    public function getName($name)
    {
        if (($file = $this->get($name)) === false)
            return false;
        else
            return $file['name'];
    }

    /**
     * Возращает временное имя
     * 
     * @param  string $name имя поля выбора файла
     * @return mixed
     */
    public function getTmpName($name)
    {
        if (($file = $this->get($name)) === false)
            return false;
        else
            return $file['tmp_name'];
    }

    /**
     * Возращает ошибку которая может возникнуть при загрузке файла
     * 
     * @param  string $name имя поля выбора файла
     * @return mixed
     */
    public function getError($name)
    {
        if (($file = $this->get($name)) === false)
            return false;
        else
            return (int) $file['error'];
    }

    /**
     * Возращает расширение файла
     * 
     * @param  string $name имя поля выбора файла
     * @return mixed
     */
    public function getExtension($name)
    {
        if (($file = $this->get($name)) === false)
            return false;
        else
            return strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
    }

    public function isValidExtension($name, $exts)
    {
        if (($ext = $this->getExtension($name)) === false)
            return false;

        return in_array($ext, $exts);
    }

    /**
     * Возращает ошибку которая может возникнуть при загрузке файла
     * 
     * @param  string $name имя поля выбора файла
     * @return mixed
     */
    public function getErrorMsg($name)
    {
        if ($code = $this->getError($name) == false)
            return false;
        switch ($code) {
            case UPLOAD_ERR_INI_SIZE: return 'Upload file size exceeded the UPLOAD_MAX_FILESIZE';
            case UPLOAD_ERR_FORM_SIZE: return 'Upload file size exceeded the MAX_FILE_SIZE';
            case UPLOAD_ERR_PARTIAL: return 'The uploaded file was only partially loaded';
            case UPLOAD_ERR_NO_FILE: return 'File was not loaded';
            case UPLOAD_ERR_OK: return '';
        }

        return 'Unknow file error';
    }

    /**
     * Был ли выбран файл клиентом
     * 
     * @param  string $name имя поля выбора файла
     * @return boolean
     */
    public function has($name)
    {
        if (empty($this->_data[$name])) return false;
        if (empty($this->_data[$name]['tmp_name'])) return false;

        return true;
    }

    /**
     * Проверка на наличие ошибки в загрузке файла
     * 
     * @param  string $name имя поля выбора файла
     * @return boolean
     */
    public function hasError($name)
    {
        if ($code = $this->getError($name) == false)
            return true;
        if ($code != UPLOAD_ERR_OK)
            return true;
    }

    /**
     * Возращает загруженный контент файла
     * 
     * @param  string $name имя поля выбора файла
     * @param  array $exts допустимые расширения файла
     * @return string
     */
    public function getContent($name, $exts)
    {
        try {
            if (!$this->has($name))
                throw new GException('Error', 'File was not loaded');
            if ($this->hasError())
                throw new GException('Error', $this->getErrorMsg());
            if ($this->isValidExtension($name, $exts))
                throw new GException('Error', 'The download does not match the expansion');
        } catch(GException $e) {}

         return file_get_contents($this->getTmpName($name), true);
    }

    /**
     * Возращает загруженный контент файла
     * 
     * @param  string $name имя поля выбора файла
     * @param  string $path путь или новое название файла
     * @return void
     */
    public function uploadTo($name, $path)
    {
        try {
            if (!$this->has($name))
                throw new GException('Error', 'File was not loaded');
            if ($this->hasError())
                throw new GException('Error', $this->getErrorMsg());
            if ($this->isValidExtension($name, $exts))
                throw new GException('Error', 'The download does not match the expansion');
            $file = $this->get($name);
            if (is_dir($path))
                $path .= basename($file['name']);
            if (!move_uploaded_file($file['tmp_name'], $path))
                throw new GException('Error', 'Unable to move file "%s"', $path);
        } catch(GException $e) {}
    }
}
?>