<?php
/**
 * Gear CMS
 * 
 * �������� ��������
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Pack.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * ����� �������� �������
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Pack.php 2016-01-01 12:00:00 Gear Magic $
 */
class GPackJS
{

    /**
     * �������� ���� ������ � �������� �� �� ����������
     * 
     * @param  string $path �������
     * @param  array $extensions ���������� ������ (array('html', ...))
     * @param  boolean $deep ��������
     * @return mixed
     */
    public static function getBatText($path, $format, $dirExceptions = array(), $fileExceptions = array())
    {
        $str = '';
        if (!is_dir($path))
            return 'The directory "' . $path . '" can not be opened';
        $handle = @opendir($path);
        if ($handle === false)
            return 'The directory "' . $path . '" can not be opened';
        while(false !== ($file = readdir($handle))) {
            if($file == '.' || $file == '..') continue;
            $dir = $path . $file;
            // ���� �������
            if (is_dir($dir . '/')) {
                if ($dirExceptions) {
                    if (in_array($dir, $dirExceptions))
                        continue;
                }
                $str .= self::getBatText($dir . '/', $format, $dirExceptions, $fileExceptions);
            }
            if ($fileExceptions)
                if (in_array($file, $fileExceptions)) continue;
            $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));

            if ($ext != 'js') continue;
            $newFile = pathinfo($file, PATHINFO_FILENAME) . '.min.';
            $str .= sprintf($format, $dir, $path . $newFile . $ext) . "\r\n";
        }
        closedir($handle);
        return $str;
    }

    /**
     * �������� bat �����
     * 
     * @param  string $path �������
     * @param  array $extensions ���������� ������ (array('html', ...))
     * @param  boolean $deep ��������
     * @return mixed
     */
    public static function makeBatFile($filename, $path, $format, $dirExceptions = array(), $fileExceptions = array())
    {
        $text = self::getBatText($path, $format, $dirExceptions, $fileExceptions);

        return file_put_contents($filename, $text);
    }

    /**
     * �������� ����� �������� � ���������
     * 
     * @param  string $path �������
     * @param  string $prefix ������ ����� �� ��������
     * @param  string $text ������������� �����
     * @param  boolean $deep ��������
     * @return mixed
     */
    public static function getFilesText($path, $prefix, &$text)
    {
        $str = '';
        if (!is_dir($path))
            return 'The directory "' . $path . '" can not be opened';
        $handle = @opendir($path);
        if ($handle === false)
            return 'The directory "' . $path . '" can not be opened';
        while(false !== ($file = readdir($handle))) {
            if($file == '.' || $file == '..') continue;
            $dir = $path . $file;
            // ���� �������
            if (is_dir($dir . '/')) {
                self::getFilesText($dir . '/', $prefix, $text);
            }
            $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));

            if ($ext != 'js') continue;
            if (strpos(pathinfo($file, PATHINFO_BASENAME), $prefix) !== false) {
                $text .= "/* " . $file . " */ \r\n" . file_get_contents($dir) . "\r\n\r\n";
                echo 'Pack "', $file, '"<br>';
                if ($text === false)
                    $text = 'Cant open file "' . $dir . '";' . "\r\n";
            }
        }
        closedir($handle);
    }

    /**
     * ��������� ��������� ������� ������ � ���� ����
     * 
     * @param  string $filename �������� �����
     * @param  string $path ������� ������
     * @param  string $prefix ������ ����� �� ��������
     * @param  boolean $deep ��������
     * @return mixed
     */
    public static function packFilesToOne($filename, $path, $prefix)
    {
        $text = '';
        self::getFilesText($path, $prefix, $text);

        return file_put_contents($filename, $text);
    }

    /**
     * ������� ����������� ����� ��������
     * 
     * @param  string $path �������
     * @param  array $fileExceptions ���������� ��� ������
     * @param  boolean $deep ��������
     * @return mixed
     */
    public static function deleteMinJs($path, $fileExceptions = array())
    {
        $str = '';
        if (!is_dir($path))
            echo 'The directory "', $path, '" can not be opened';
        $handle = @opendir($path);
        if ($handle === false)
            echo 'The directory "', $path, '" can not be opened';
        while(false !== ($file = readdir($handle))) {
            if($file == '.' || $file == '..') continue;
            $dir = $path . $file;
            // ���� �������
            if (is_dir($dir . '/')) {
                self::deleteMinJs($dir . '/', $fileExceptions);
            }
            if ($fileExceptions)
                if (in_array($file, $fileExceptions)) continue;
            $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
            if ($ext != 'js') continue;
            if (strpos(pathinfo($file, PATHINFO_BASENAME), 'min.js') !== false) {
                unlink($dir);
                echo 'Remove "', $dir, '"<br>';
            }
        }
        closedir($handle);
    }
}
?>