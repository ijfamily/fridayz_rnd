<?php
/**
 * Gear CMS
 *
 * Кэш
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Core
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Cache.php 2016-01-01 16:00:00 Gear Magic $
 */

/**
 * Кэш
 * 
 * @category   Gear
 * @package    Core
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Cache.php 2016-01-01 16:00:00 Gear Magic $
 */
class GCache
{
    /**
     * Путь к кэшу
     *
     * @var string
     */
    protected static $_path;

    /**
     * Кэшировать только 1-н раз
     *
     * @var bool
     */
    protected static $_isOnce = false;

    /**
     * Файл кэша
     *
     * @var string
     */
    protected static $_filename;

    /**
     * Название генератора кэша
     *
     * @var string
     */
    public $generator = 'article';

    /**
     * Идент. контента кэша
     *
     * @var integer
     */
    public $generatorId = 0;

    /**
     * Заметка о созданном кэше
     *
     * @var string
     */
    public $note = '';

    /**
     * Выполлнять кэширование без запроса в URL 
     * (http://domain.com?key=value, отбрасывается "?key=value")
     *
     * @var string
     */
    public $withoutQuery = false;

    /**
     * Конструктор
     *
     * @param  string $path путь к кэшу
     * @return void
     */
    public function __construct($path = '')
    {
        // путь к кешу
        if (empty($path))
            self::$_path = PATH_CACHE;
        else
            self::$_path = $path;
        // кеш файла
        self::$_filename = self::getFileName();
    }

    /**
     * Возращает хэш запроса
     *
     * @return string
     */
    public static function getHash()
    {
        return Gear::$app->domainId . md5($_SERVER['REQUEST_URI']);
    }

    /**
     * Возвращает сгенерированное название файла
     *
     * @return string
     */
    public static function getFileName()
    {
        if (empty(self::$_filename))
            return self::getHash() . '.tmp';

        return self::$_filename;
    }

    /**
     * Проверка существования кэша
     *
     * @return bool
     */
    public static function isExist()
    {
        return file_exists(self::$_path . self::getFileName());
    }

    /**
     * Добавление сведения о кэше в базу данных
     *
     * @param  array $params сведения о кэше
     * @return void
     */
    public static function insertDb($params = array())
    {
        $data= array(
            'cache_domain'    => $_SERVER['SERVER_NAME'],
            'cache_domain_id' => Gear::$app->domainId,
            'cache_date'      => date('Y-m-d H:i:s'),
            'cache_agent'     => $_SERVER['HTTP_USER_AGENT'],
            'cache_uri'       => $_SERVER['REQUEST_URI'],
            'cache_ipaddress' => $_SERVER['REMOTE_ADDR']
        );
        $table = new GDbTable('site_cache', 'cache_id');
        $table->insert(array_merge($data, $params));
    }

    /**
     * Обновление файла кэша
     *
     * @param  string $data данные кэша
     * @return string
     */
    public function update($data)
    {
        if ($this->withoutQuery)
            if (sizeof($_GET) > 1)
                return '';

        GTimer::start('update cache');
        $filename = $this->getFileName();
        // добавить сведения о кеше в базу данных
        $this->insertDb(
            array('cache_filename' => $filename, 'cache_generator_id' => $this->generatorId, 'cache_generator' => $this->generator,
                  'cache_note' => $this->note)
        );
        $result = file_put_contents(self::$_path . $filename, $data, FILE_USE_INCLUDE_PATH);
        GTimer::stop('update cache');

        return $result;
    }

    /**
     * Возращает кэш из файла
     *
     * @return string
     */
    public static function select()
    {
        return file_get_contents(self::$_path . self::getFileName(), true);
    }

    /**
     * Вывод данных кэша (для html)
     * 
     * @param  boolean $end если true - вывод тегов "</body></html>"
     * @return void
     */
    public function render($end = true)
    {
        GTimer::start('load cache');
        if (($data = self::select()) !== false) {
            $url = GFactory::getURL();
            // http://{host}/rss или http://{host}/.../...xml
            if ($url->filename) {
                $ext = strtolower(pathinfo($url->filename, PATHINFO_EXTENSION));
            } else
                $ext = '';
            if ($ext == 'xml' || $url->is('/rss/')) {
                header("Content-Type:text/xml");
                echo $data;
            } else {
                echo $data;
                if ($end) {
                    echo "\r\n</body>";
                    echo "\r\n</html>";
                }
            }
        }
        GTimer::stop('load cache');
    }
}
?>