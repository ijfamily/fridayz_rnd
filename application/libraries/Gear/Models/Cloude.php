<?php
/**
 * Gear CMS
 *
 * Модель данных "Облако тегов"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Cloude.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Модель данных облака тегов
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Cloude.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmCloudeTags extends GModelItems
{
   /**
     * Количество записей в списке
     * 
     * @var integer
     */
    public $limit = 0;

   /**
     * Сортируемое поле (date)
     * 
     * @var string
     */
    public $sort = 'index';

   /**
     * Сортируемое поле (date)
     * 
     * @var string
     */
    protected $_sortFields = array('index' => 'tag_index');

   /**
     * Вид сортировки (asc, desc)
     * 
     * @var string
     */
    public $sortType = 'desc';

   /**
     * Выбраный тег
     * 
     * @var string
     */
    public $select = '';

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // сортируемое поле (date)
        $sort = $this->get('sort', $this->sort);
        if (isset($this->_sortFields[$sort]))
            $this->sort = $this->_sortFields[$sort];
        else
            $this->sort = $this->_sortFields[$this->sort];
        // вид сортировки (asc, desc)
        $this->sortType = $this->get('sort-type', $this->sortType);
        if ($this->sortType != 'asc' && $this->sortType != 'desc')
            $this->sortType = 'desc';
        // выбраный тег
        $this->select = $this->get('select', $this->select);
    }

    /**
     * Возращает часть SQL запроса (сортировка)
     *
     * @return string
     */
    protected function getOrder()
    {
        return ' ORDER BY `' . $this->sort . '` ' . $this->sortType;
    }

    /**
     * Возвращает SQL запрос сформированный из GET запроса пользователя 
     * 
     * @return void
     */
    protected function getQuery()
    {
        $sql = 'SELECT * FROM `site_tags` `a` WHERE `tag_published`=1 ';

        return $sql;
    }

    /**
     * Возращает элементы ленты
     *
     * @return void
     */
    public function getItems()
    {
        GTimer::start('tags items');
        // обработчик SQL запросов
        $query = new GDbQuery();
        $sql = $this->getQuery() . $this->getOrder();
        if ($query->execute($sql) === false)
            throw new GException('Query error (Internal Server Error)', 'Response from the database: %s', $query->getError(), __CLASS__);

        $result = array();
        while (!$query->eof()) {
            $item = $query->next();
            if ($this->sef)
                $url = '/tag/' . urlencode($item['tag_name']) . '/';
            else
                $url = '/?do=tag&tag=' . urlencode($item['tag_name']);
            $result[] = array(
                'id'    => $item['tag_id'],
                'name'  => $item['tag_name'],
                'index' => $item['tag_index'],
                'title' => $item['tag_title'],
                'lat'   => $item['tag_lat'],
                'url'   => $url,
                'active' => $this->select == $item['tag_name'] || $this->select == $item['tag_lat']
            );
        }
        GTimer::stop('tags items');

        return $result;
    }
}
?>