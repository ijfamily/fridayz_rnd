<?php
/**
 * Gear CMS
 *
 * Модель данных "Лента"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Feed.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Базовый класс модели данных ленты
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Feed.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmFeed extends GModelItems
{
   /**
     * Количество записей в списке
     * 
     * @var integer
     */
    public $limit = 10;

   /**
     * Сортируемое поле (date)
     * 
     * @var string
     */
    public $sort = 'date';

   /**
     * Сортируемое поле (date)
     * 
     * @var string
     */
    protected $_sortFields = array('date' => 'published_date');

   /**
     * Вид сортировки (asc, desc)
     * 
     * @var string
     */
    public $sortType = 'desc';

    /**
     * Идентификатор вида статьи
     *
     * @var integer
     */
    public $typeId = 2;

    /**
     * Идентификатор категории статьи
     *
     * @var integer
     */
    public $categoryId = 0;

    /**
     * Категоря по умолчанию
     *
     * @var bool
     */
    public $categoryDefault = false;

    /**
     * Вывод коротких тегов в статье
     *
     * @var boolean
     */
    public $_useShortTags = false;

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        $this->ellipsisHeader = (int) $this->get('ellipsis-header', 0);
        // идент. категории статей (1,2,3, ...)
        $this->categoryId = (int) $this->get('category-id', $this->categoryId);
        // категори я по умолчанию
        $this->categoryDefault = $this->get('category-default', false);
        // идент. категории статей (1,2,3, ...)
        $this->typeId = (int) $this->get('type-id', $this->typeId);
        // количество записей в списке
        $this->limit = abs((int) $this->get('limit', $this->limit));
        if (empty($this->limit) || $this->limit > 100)
            $this->limit = 10;
        // сортируемое поле (date)
        $sort = $this->get('sort', $this->sort);
        if (isset($this->_sortFields[$sort]))
            $this->sort = $this->_sortFields[$sort];
        else
            $this->sort = $this->_sortFields[$this->sort];
        // вид сортировки (asc, desc)
        $this->sortType = $this->get('sort-type', $this->sortType);
        if ($this->sortType != 'asc' && $this->sortType != 'desc')
            $this->sortType = 'desc';
        // использование коротки тегов
        $this->_useShortTags = Gear::$app->config->get('ARTICLE/TAGS');
        // произвольная сортировка списка
        $this->dataRandom = $this->get('data-random', false);
    }

    /**
     * Возращает часть SQL запроса (количество записей)
     *
     * @return string
     */
    protected function getLimit()
    {
        return ' LIMIT 0,' . $this->limit;
    }

    /**
     * Возращает часть SQL запроса (сортировка)
     *
     * @return string
     */
    protected function getOrder()
    {
        if ($this->dataRandom)
            return ' ORDER BY RAND()';
        else
            return ' ORDER BY `' . $this->sort . '` ' . $this->sortType . ', `a`.`article_id` desc';
    }

    /**
     * Возвращает SQL запрос сформированный из GET запроса пользователя 
     * 
     * @return void
     */
    protected function getQuery()
    {
        $sql = 'SELECT `a`.*, `ac`.*, `p`.* '
             . 'FROM `site_articles` `a` JOIN `site_pages` `p` ON `a`.`article_id`=`p`.`article_id` AND '
             . '`a`.`published`=1 AND `p`.`language_id`=' . GFactory::getLanguage('id')
             . ' JOIN `site_categories` `ac` USING(`category_id`) '
             . 'WHERE 1 ';
        if ($this->categoryDefault) {
            if (!empty(Gear::$app->category['category_id']))
                $sql .= ' AND `category_id`=' . Gear::$app->category['category_id'];
        } else
        if ($this->categoryId)
            $sql .= ' AND `category_id`=' . $this->categoryId;
        if ($this->typeId)
            $sql .= ' AND `type_id`=' . $this->typeId;
        // если лента на главной странице
        if (Gear::$app->url->isRoot())
            $sql .= ' AND `a`.`published_main`=1';
        else
            $sql .= ' AND `a`.`published_feed`=1';
         

        return $sql;
    }

    /**
     * Возращает список коротких тегов
     * 
     * @param  string $str строка тегов
     * @return string
     */
    public function getTagsItem($str)
    {
        if (empty($str)) return '';

        $str = str_replace(array('#', '-', '_'), '', $str);
        $list = explode(' ', $str);
        $result = '';
        foreach ($list as $index => $tag) {
            $result .= '<a href="{chost}/tag/' . urlencode($tag) . '/">' . $tag . '</a>';
        }

        return $result;
    }

    /**
     * Возращает элементы ленты
     *
     * @return void
     */
    public function getItems($eachItem = null)
    {
        GTimer::start('feed items');
        // обработчик SQL запросов
        $query = new GDbQuery();
        $sql = $this->getQuery() . $this->getOrder() . $this->getLimit();
        if ($query->execute($sql) === false)
            throw new GException('Query error (Internal Server Error)', 'Response from the database: %s', $query->getError(), __CLASS__);

        $ln = GFactory::getLanguage('prefixUri');
        $result = array();
        while (!$query->eof()) {
            $item = $query->next();
            if ($this->sef)
                $url = $this->ln . $item['category_uri'] . $item['article_id'] . '-' . $item['article_uri'];
            else
                $url = '/?a=' . $item['article_id'];
            if ($eachItem == null) {
                if ($this->ellipsisHeader > 0)
                     $item['page_header'] = mb_strimwidth($item['page_header'], 0, $this->ellipsisHeader, '...', 'UTF8');

                $eitem = array(
                    'id'     => $item['article_id'],
                    'title'  => $item['page_header'],
                    'img'    => $item['page_image'],
                    'text'   => $item['page_html_short'],
                    'plain'  => $item['page_html_plain'],
                    'date'   => !$this->get('date-hide') ? $item['published_date'] : '',
                    'time'   => $item['published_time'],
                    'uri'    => $item['article_uri'],
                    'visits' => (int) $item['article_visits'],
                    'author' => $item['page_meta_author'],
                    'url'    => $url,
                    'ln'     => $this->ln,
                    'tags'   => $this->_useShortTags ? $this->getTagsItem($item['page_tags']) : '',
                    'announce-date' => $item['announce_date'],
                    'announce-time' => $item['announce_time']
                );
                $result[] = $eitem;
            } else {
                $eitem = $eachItem($item, $self);
                $eitem['tags'] = $this->_useShortTags ? $this->getTagsItem($item['page_tags']) : '';
                $result[] = $eitem;
            }
        }
        GTimer::stop('feed items');

        return $result;
    }
}
?>