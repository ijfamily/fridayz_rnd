<?php
/**
 * Gear CMS
 *
 * Модель данных "Форма"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Forms.php 2016-01-01 21:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Базовый класс модели данных формы
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Forms.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmForm extends GModel
{
    /**
     * Кому предназначен запрос (если форм на странице много)
     *
     * @var string
     */
    public $target = 'form';

    /**
     * Поля формы (вида "{alias1} => array('field' => {field1}, ...), ....")
     * Каждое поле имеет ключ: "field" (название поля), "minLength" (мин. длина символов),
     * "maxLength" (макс. длина символов), "match" (регулярное выражение), "strip" (убирает html теги),
     * "title" (загаловок поля)
     * 
     * @var array
     */
    protected $_fields = array();

    /**
     * Проверять хэш
     *
     * @var boolean
     */
    protected $_checkHash = true;

    /**
     * Метод получения данных
     *
     * @var string
     */
    protected $_method = 'POST';

    /**
     * Если выполнен запрос - true
     *
     * @var boolean
     */
    protected $_isSubmit = false;

    /**
     * Выполнять проверку запроса
     *
     * @var boolean
     */
    protected $_checkSubmit = true;

    /**
     * Переход по адресу если запрос был успешен (свойство компонента)
     *
     * @var string
     */
    public $location = '';

    /**
     * Проверять капчу (свойство компонента)
     *
     * @var boolean
     */
    public $checkCaptcha = false;

    /**
     * Отправлять на email сообщение после обработки формы (свойство компонента)
     *
     * @var boolean
     */
    public $sendMail = false;

    /**
     * Отправлять смс сообщение после обработки формы (свойство компонента)
     *
     * @var boolean
     */
    public $sendSms = false;

    /**
     * Ресурс для отправки смс сообщений (свойство компонента)
     *
     * @var string
     */
    public $smsUrl = '';

    /**
     * Email адрес для отправленных сообщений (свойство компонента)
     *
     * @var arrya
     */
    public $toMails = array();

    /**
     * Выполнять проверку запроса на SPAM (свойство компонента)
     *
     * @var boolean
     */
    public $checkSpam = false;

    /**
     * Название формы (свойство компонента)
     *
     * @var string
     */
    public $formName = '';

    /**
     * Сообщение при успешной орбработки формы (свойство компонента)
     *
     * @var string
     */
    public $msgSuccess = '';

    /**
     * Тема письма приходящего от клиента (свойство компонента)
     *
     * @var object
     */
    public $mailSubject = '';

    /**
     * Время ожидания между запросами в сек. (свойство компонента)
     *
     * @var integer
     */
    public $waitingTime = 0;

    /**
     * Количество сообщений которые может отправить клиент (свойство компонента)
     *
     * @var integer
     */
    public $maxMessages = 0;

    /**
     * Обработчик запросов формы
     *
     * @var object
     */
    public $input = null;

    /**
     * Указатель экземпляра класса почты
     *
     * @var object
     */
    public $mail = null;

    /**
     * Идентификатор последней добавленной или измененной записи
     *
     * @var integer
     */
    public $recordId = 0;

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // время ожидания между запросами в сек.
        $this->waitingTime = (int) $this->get('waiting-time', $this->waitingTime);
        // количество сообщений которые может отправить клиент
        $this->maxMessages = (int) $this->get('max-messages', $this->maxMessages);
        // выполнять проверку запроса на SPAM
        $this->checkSpam = $this->get('check-spam', $this->checkSpam);
        // выполнять проверку капчи
        $this->checkCaptcha = $this->get('check-captcha', $this->checkCaptcha);
        // название формы
        $this->formName = $this->get('form-name', $this->formName);
        // отправлять на email сообщение после обработки формы администратору
        $this->sendMail = $this->get('send-mail', $this->sendMail);
        // отправлять смс сообщение после обработки формы
        $this->sendSms = $this->get('send-sms', $this->sendSms);
        // ресурс для отправки смс сообщений
        $this->smsUrl = $this->get('sms-url', $this->smsUrl);
        // переход по адресу если запрос был успешен
        $this->location = $this->get('location', $this->location);
        // тема письма приходящего от клиента
        $this->mailSubject = $this->get('mail-subject', $this->mailSubject);
        // email адрес для отправленных сообщений
        $this->toMails = $this->get('emails', $this->toMails);
        if ($this->toMails)
            $this->toMails = explode(';', str_replace(' ', '', $this->toMails));

        // обработчик запросов к форме
        $this->input = Gear::$app->input;
        // почта
        $this->mail = GFactory::getMail();
        // запуск сессии
        GFactory::getSession()->start();
        // установить соединение с сервером
        GFactory::getDb()->connect();
        $this->detectForm();
    }

    /**
     * Инициализация формы
     * 
     * @return void
     */
    public function detectForm()
    {
         $sess = GFactory::getSession();
         $this->_success = $sess->get('success', '', $this->id);
         // если был сабмит формы в предыдущий раз, выводим сообщение
         if ($this->_success) {
            $sess->set('success', '', $this->id);
         }
        // проверять сабмит формы
        if ($this->_checkSubmit) {
            // был ли запрос
            $this->_isSubmit = ($this->input->get('target') == $this->target) && ($this->input->getMethod() == $this->_method);
            // если выполнен запрос
            if (!$this->_isSubmit) {
                $this->_success = '';
                $sess->set('success', '', $this->id);
                // если есть счетчик количества сообщений
                if ($this->maxMessages) {
                    $sess->set('countMsg', 0, $this->id);
                }
                // если есть задержка между запросами
                if ($this->waitingTime) {
                    $sess->set('startWait', @time(), $this->id);
                }
                return;
            }
        }
        // валидация данных формы на корректность заполнения
        if ($this->isChecked() !== true) {
            // проверить задержку между сообщениями
            if ($this->waitingTime) {
                $sess->set('startWait', @time(), $this->id);
            }
            $this->_isSubmit = false;
            return;
        }
        // обработка данных формы
        $this->submit();
        // если успех после обработки данных формы
        if ($this->_success) {
            $sess->set('success', $this->_success, $this->id);
            // сбрасывать только те поля у которых есть свойство "resetable"
            $this->input->reset($this->_fields);
            // если есть счетчик количества сообщений
            if ($this->maxMessages) {
                $countMsg = (int) $sess->get('countMsg', 0, $this->id);
                $sess->set('countMsg', $countMsg + 1, $this->id);
            }
            if ($this->waitingTime) {
                $sess->set('startWait', @time(), $this->id);
            }
            // если необходим переход
            if ($this->location) {
                //header('location: ' . $this->location);
                //exit;
            }
        }
    }

    /**
     * Обработка данных формы
     * 
     * @return void
     */
    protected function submit()
    {}

    /**
     * Валидация данных формы на корректность заполнения
     * 
     * @return boolean
     */
    protected function isChecked()
    {
        $sess = GFactory::getSession();
        // проверка существования куков
        if (empty($_COOKIE[$sess->getName()])) {
            GMessage::to('form', 'error', 'To work correctly, the application must enable cookies', $this->path);
            return false;
        }
        // проверка на спам
        if ($this->checkSpam) {
            if (!$this->isNotSpam()) return false;
        }
        // проверить счетчик количества сообщений
        if ($this->maxMessages) {
            if ($sess->get('countMsg', 0, $this->id) > $this->maxMessages) {
                GMessage::to('form', 'error', 'On this page is limited number of outgoing messages', $this->path);
                return false;
            }
        }
        // проверить хэш
        if ($this->_checkHash)
            if (!$this->isValidHash()) {
                $this->input->reset($this->_fields);
                GMessage::to('form', 'error', 'The data has already been sent', $this->path);
                return false;
            }
        // проверить задержку между сообщениями
        if ($this->waitingTime) {
            $endWait = @time();
            $startWait = $sess->get('startWait', 0, $this->id);
            $time = abs($startWait - $endWait);
            if ($time < (int) $this->waitingTime) {
                GMessage::to('form', 'error', 'You too often sending messages', $this->path);
                return false;
            }
        }
        // проверить каптчу
        if ($this->checkCaptcha)
            if (!$this->isValidCaptcha()) {
                GMessage::to('form', 'error', 'Captcha code error', $this->path);
                return false;
            }
        // проверить поля формы
        if (!$this->isValidFields())
            return false;

        return true;
    }

    /**
     * Валидация каптчи
     * 
     * @return boolean
     */
    protected function isValidCaptcha()
    {
        // если captcha правильна
        $sess = GFactory::getSession();
        $sess->start();

        return $sess->has('captcha') && $sess->get('captcha') === $this->input->get('code');
    }

    /**
     * Валидация хэш
     * 
     * @return boolean
     */
    protected function isValidHash()
    {
        Gear::library('/Hash');

        return GHash::check($this->input->get('token'), GFactory::getSession()->get('hash', '', $this->id));
    }

    /**
     * Валидация полей формы
     * 
     * @return boolean
     */
    protected function isValidFields()
    {
        return $this->input->check($this->_fields, $this->path);
    }

    /**
     * Валидация сообщения на спам
     * 
     * @return mixed
     */
    protected function isNotSpam()
    {
        // установить соединение с сервером
        GFactory::getDb()->connect();
        $query = new GDbQuery();
        // условия запроса для поиска адреса с маской
        $addr = $_SERVER['REMOTE_ADDR'];
        $sql = $this->input->getQueryIp($addr, 'spam_ipaddress', 'spam_mask');
        $sql = 'SELECT COUNT(*) `count` FROM `site_spam` WHERE ' . $sql;
        if (($spam = $query->getRecord($sql)) === false) {
            GMessage::to('form', 'error', 'Error processing forms', $this->path);
            return false;
        }
        if (!empty($spam))
            if ($spam['count'] > 0) {
                GMessage::to('form', 'error', 'Your message recognized as spam and will not be accepted more', $this->path);
                return false;
            }

        return true;
    }

    /**
     * Возращает данные формы для отправки в бд
     * 
     * @return array
     */
    public function getData()
    {
        $result = array();
        foreach($this->_fields as $field => $attr) {
            // используется для формирования запроса к бд
            $record = isset($attr['record']) ? $attr['record'] : true;
            // если поле используется
            if ($attr['use'] && $record) {
                if (($value = $this->input->get($field)) != null) {
                    $result[$attr['field']] = $value;
                } else {
                    $result[$attr['field']] = $attr['default'];
                }
            }
        }

        return $result;
    }

    /**
     * Возращает поля с их значениями
     * 
     * @param  $datac ассоц-й массив вида "{alias1}' => '', ..."
     * @return array
     */
    public function getFields($fields = array())
    {
        $result = array();

        if ($this->_checkHash) {
            Gear::library('/Hash');
            $hash = GHash::get();
            GFactory::getSession()->set('hash', $hash['hash'], $this->id);
            $result['token'] = $hash['token'];
        }

        if ($this->_isSubmit) {
            foreach($this->_fields as $field => $attr) {
                // если значение збрасывается
                $resetable = isset($attr['resetable']) ? $attr['resetable'] : false;
                // если поле используется
                if ($attr['use']) {
                    if ($resetable)
                        $result[$field] = '';
                    else {
                        $result[$field] = $this->input->get($field, '');
                    }
                }
            }
        } else {
            foreach($this->_fields as $field => $attr) {
                // если значение збрасывается
                $resetable = isset($attr['resetable']) ? $attr['resetable'] : false;
                // если поле используется
                if ($attr['use']) {
                    if ($resetable) {
                        if (isset($attr['default']))
                            $result[$field] = $attr['default'];
                        else
                            $result[$field] = '';
                    } else {
                        $result[$field] = $this->input->get($field, '');
                    }
                } else
                    $result[$field] = '';
            }
        }

        return $result;
    }

    /**
     * Был ли сабмит формы
     * 
     * @return boolean
     */
    public function isSubmit()
    {
        return $this->_isSubmit;
    }
}
?>