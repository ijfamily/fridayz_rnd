<?php
/**
 * Gear CMS
 *
 * Модель данных "RSS"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Rss.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Модель данных RSS ленты
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Rss.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmRss extends GModel
{
    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // установить соединение с сервером
        GFactory::getDb()->connect();
    }

    /**
     * Возращает список статей
     *
     * @param object $query указатель на запрос
     * @return void
     */
    public function getArticles($query = null)
    {
        if (is_null($query))
            $query = new GDbQuery();
        // статьи
        $sql = 'SELECT `p`.*, `a`.*, `c`.`category_uri` '
             . 'FROM `site_pages` `p`, `site_articles` `a` LEFT JOIN `site_categories` `c` USING(`category_id`) '
             . 'WHERE `a`.`article_id`=`p`.`article_id` AND `a`.`domain_id`=' . Gear::$app->domainId . ' AND '
             . '`a`.`article_id`<>1 AND `a`.`published`=1 AND `a`.`article_rss`=1 AND `p`.`language_id`=' . GFactory::getLanguage('id');
        if ($query->execute($sql) === false)
            die('Can`t create RSS Feed (response from the database: '. $query->getError() . ')');

        $items = array();
        while (!$query->eof()) {
            $page = $query->next();
            if ($page['page_html_short'])
                $descript = strip_tags(str_replace(array('<br>', '<br/>'), array("\r\n", "\r\n"), $page['page_html_short']));
            else
                $descript = '';
            $url = GArticles::genUrl($page, $this->ln, $this->sef, true);
            $items[] = array(
                'title'       => $page['page_header'],
                'url'         => $url,
                'author'      => $page['page_meta_author'],
                'date'        => date('D, d M Y H:i:s', strtotime($page['published_date'] . ' ' . $page['published_time'])),
                'description' => $descript
            );
        }

        return $items;
    }
}
?>