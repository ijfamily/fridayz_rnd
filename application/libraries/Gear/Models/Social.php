<?php
/**
 * Gear CMS
 *
 * Модель данных "Адаптер подключение к социальным сетям"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Social.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Базовый класс модели адаптера подключение к социальным сетям
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Social.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmSocial extends GModel
{
    /**
     * Инициализация адаптера
     * 
     * @return void
     */
    public function initialize()
    {}

    /**
     * Возращает список социальных сетей
     * 
     * @return string
     */
    public function getIcons()
    {
        $list = Gear::$app->config->getFrom('SOCIAL');

        $str = '';
        foreach ($list as $alias => $item) {
            if ($item['enabled']) {
                $alias = strtolower($alias);
                $str .= "<a href=\"{chost}/signin/?auth=$alias;\" class=\"big_provider $alias_big\"></a>";
            }
        }

        return $str;
    }
}
?>