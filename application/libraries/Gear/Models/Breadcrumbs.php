<?php
/**
 * Gear CMS
 *
 * Модель данных "Хлебные крошки"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Breadcrumbs.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Модель данных хлебных крошек
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Breadcrumbs.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmBreadcrumbs extends GModel
{
    /**
     * Возращает элементы списка
     *
     * @return void
     */
    public function getItems()
    {
        // если данные переданы другими компонентами
        if ($items = Gear::from('Breadcrumbs', 'items')) {
            return $items;
        }

        // данные страницы сайта
        $data = Gear::$app->document->getData(); 
        if ((empty($data) && empty(Gear::$app->category)) || !Gear::$app->document->isOk()) return array();

        // если нет категории, но есть статья
        if (empty(Gear::$app->category)) {
            // если нет нулевой навигации
            if (!Gear::$app->config->get('BREADCRUMBS/ZERO', false)) return array();

            // статья
            if (empty($data['page_breadcrumb']))
                $title = $data['page_header'];
            else
                $title = $data['page_breadcrumb'];

            return array(
                array('label' => GText::_('label_home', $this->get('path')), 'title' => GText::_('title_home', $this->get('path')), 'url' => empty($ln) ? '/' : $ln),
                array('title' => $title)
        );
        }
        $cat = Gear::$app->category;
        // обработчик SQL запросов
        $query = new GDbQuery();
        // если ЧПУ работает
        if ($this->sef)
            $sql = 'SELECT * '
                 . 'FROM `site_categories` `c` '
                 . 'WHERE '
                 . '`category_level`<=' . $cat['category_level'] . ' AND '
                 . '`category_left`<=' . $cat['category_left'] . ' AND '
                 . '`category_right`>=' . $cat['category_right'] . ' AND '
                 . '`c`.`category_id`<>1 AND `c`.`category_visible`=1 AND `c`.`domain_id`=' . Gear::$app->domainId . ' ORDER BY `c`.`category_left` ASC';
        else
            $sql = 'SELECT `c`.*, `a`.`article_id` '
                 . "FROM `site_categories` `c` JOIN `site_articles` `a` ON `a`.`category_id`=`c`.`category_id` AND `a`.`article_uri`='index.html' "
                 . 'WHERE '
                 . '`category_level`<=' . $cat['category_level'] . ' AND '
                 . '`category_left`<=' . $cat['category_left'] . ' AND '
                 . '`category_right`>=' . $cat['category_right'] . ' AND '
                 . '`c`.`category_id`<>1 AND `c`.`category_visible`=1 AND `c`.`domain_id`=' . Gear::$app->domainId . ' ORDER BY `c`.`category_left` ASC';

        if ($query->execute($sql) === false)
            throw new GException('Query error (Internal Server Error)', 'Response from the database: %s', $query->getError(), __CLASS__);

        $result = array(
            array('label' => GText::_('label_home', $this->get('path')), 'title' => GText::_('title_home', $this->get('path')), 'url' => empty($ln) ? '/' : $ln)
        );

        while (!$query->eof()) {
            $item = $query->next();
            // для категорий которые на другом домене, но главная страница в виде списка
            if ($item['category_uri'] == '/') continue;
            // если ЧПУ
            if ($this->sef)
                $url = $this->ln . $item['category_uri'];
            else
                $url = '?a=' . $item['article_id'];
            $result[] = array('title' => $item['category_name'], 'url' => $url, 'id' => $item['category_id']);
        }

        // если ЧПУ
        if ($this->sef)
            $show = $data['article_uri'] != 'index.html' && !empty(Gear::$app->url->filename);
        else
            $show = Gear::$app->dataPage['article_uri'] != 'index.html';

        if ($show) {
            // статья
            if (empty($data['page_breadcrumb']))
                $title = $data['page_header'];
            else
                $title = $data['page_breadcrumb'];
            $result[] = array('title' => $title);
        }

        return $result;
    }
}
?>