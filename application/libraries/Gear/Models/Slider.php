<?php
/**
 * Gear CMS
 *
 * Модель данных "Слайдер"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Slider.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Базовый класс модели данных слайдера
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Slider.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmSlider extends GModelItems
{
    /**
     * Возращает элементы слайдера
     *
     * @return void
     */
    public function getItems()
    {
        $doc = GFactory::getDocument();
        // данные страницы сайта
        $data = $doc->getData();
        if (empty($data)) return array();
        // если указана статья
        if ($this->dataId)
            $sql = '`slider_id`=' . $this->dataId;
        else
            $sql = '`article_id`=' . $doc->id . ' OR `article_id` IS NULL';
        // обработчик SQL запросов
        $query = new GDbQuery();
        $sql = 'SELECT `i`.* '
             . 'FROM `site_slider_images` `i` '
             . 'JOIN (SELECT `slider_id` FROM `site_sliders` WHERE '. $sql . ' ORDER BY `article_id` DESC LIMIT 0,1) `s` USING(`slider_id`) '
             . 'WHERE `i`.`image_visible`=1 '
             . ' ORDER BY `image_index` ASC';
        if ($query->execute($sql) === false)
            throw new GException('Query error (Internal Server Error)', 'Response from the database: %s', $query->getError(), __CLASS__);

        $ln = GFactory::getLanguage('prefixUri');
        $result = array();
        while (!$query->eof()) {
            $item = $query->next();
            $result[] = array(
                'id'      => $item['image_id'],
                'src'     => $item['image_entire_filename'],
                'title'   => $item['image_title'],
                'title-1' => $item['image_title_1'],
                'text'    => $item['image_text'],
            );
        }

        return $result;
    }
}
?>