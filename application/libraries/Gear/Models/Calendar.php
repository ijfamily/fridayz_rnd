<?php
/**
 * Gear CMS
 *
 * Модель данных "Календарь"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Calendar.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Класс модели данных "Календарь"
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Calendar.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmjCalendar extends GModel
{
    /**
     * Обработчик SQL запроса
     *
     * @var object
     */
    public $query;

    /**
     * Обработчик запросов формы
     *
     * @var object
     */
    public $input = null;

    /**
     * Диапазон дат (от)
     *
     * @var string
     */
    public $dateFrom = '';

    /**
     * Диапазон дат (до)
     *
     * @var string
     */
    public $dateTo = '';

    /**
     * Идент. категории статьи
     *
     * @var integer
     */
    public $categoryId = 0;

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // обработчик запросов к форме
        $this->input = GFactory::getApplication()->input;
        // установить соединение с сервером
        GFactory::getDb()->connect();

        // диапазон дат
        $date = $this->input->get('date', false);
        if ($date) {
            if ($time = strtotime($date)) {
                $date = date('Y-m-', $time);
                $this->dateFrom = $date . '01';
                $this->dateTo = $date . '31';
            }
        }
        // идент. категории статьи
        $this->categoryId = (int) $this->input->get('category', 0);
    }

    /**
     * Возращает список дат календаря
     * 
     * @return array
     */
    public function getDates()
    {
        if (empty($this->dateFrom)) return array();

        // обработчик SQL запросов
        $query = new GDbQuery();
        $sql = "SELECT COUNT(*) `count`, `published_date` FROM `site_articles` "
             . "WHERE `published_date` BETWEEN '$this->dateFrom' AND '$this->dateTo' AND `published`=1 AND `domain_id`=" . Gear::$app->domainId;
        if ($this->categoryId)
            $sql .= ' AND `category_id`=' . $this->categoryId;
        $sql .= ' GROUP BY `published_date`';
        if ($query->execute($sql) === false)
            throw new GException('Query error (Internal Server Error)', 'Response from the database: %s', $query->getError(), __CLASS__);
        $items = array();
        while (!$query->eof()) {
            $date = $query->next();
            $day = date('j', strtotime($date['published_date']));
            if ($date['count'] > 0)
                $items[] = array(
                    'count' => $date['count'],
                    'day'   => $day
                );
        }

        return $items;
    }
}
?>