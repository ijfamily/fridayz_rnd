<?php
/**
 * Gear CMS
 *
 * Модель данных "Поиск статей"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Search.php 2016-01-01 21:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CmForm
 */
Gear::library('/Models/Form');

/**
 * @see CmList
 */
Gear::library('/Models/List');

/**
 * Модель данных формы поиска статьи
 * 
 * @category   Gear
 * @package    Models
 * @subpackage Form
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Search.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmFormSearchArticle extends CmForm
{

    /**
     * Метод получения данных
     *
     * @var string
     */
    protected $_method = 'GET';

    /**
     * Выполнять проверку запроса
     *
     * @var boolean
     */
    protected $_checkSubmit = false;

    /**
     * Поля формы (вида "{alias1} => {field1}, ....")
     *
     * @var array
     */
    protected $_fields = array(
        'q' => array(
            'use'       => true,
            'field'     => 'query',
            'strip'     => true,
            'maxLength' => 255,
            'minLength' => 3,
            'title'     => 'Search this site'
        )
    );

   /**
     * Строка поиска
     * 
     * @var string
     */
    protected $_search = false;

    /**
     * Этот метод вызывается сразу после создания компонента модели
     * Используется для инициализации атрибутов модели не переданных через конструктор
     * 
     * @return void
     */
    public function construct()
    {
        parent::construct();

        // загаловок вкладки браузера
        GFactory::getDocument()->title = GText::_('Search this site', 'search');
    }

    /**
     * Возращает поля с их значениями
     * 
     * @param  $datac ассоц-й массив вида "{alias1}' => '', ..."
     * @return array
     */
    public function getFields($fields = array())
    {
        $result = array('q' => $this->input->get('q'));

        return $result;
    }

    /**
     * Валидация данных формы на корректность заполнения
     * 
     * @return boolean
     */
    protected function isChecked()
    {
        // проверить поля формы
        if (($error = $this->isValidFields()) !== true) {
            $this->_error = $error;
            return false;
        }

        return true;
    }

    /**
     * Удалить не нужное
     * 
     * @return string
     */
    protected function cleanSearch($data){
        $data = strip_tags($data);
        $data = preg_replace('~[^a-z0-9 \x80-\xFF]~i', "", $data);
        $data = trim($data);
 
        return $data;
    }

    /**
     * Возращает поля с их значениями
     * 
     * @param  $datac ассоц-й массив вида "{alias1}' => '', ..."
     * @return array
     */
    public function getSearch()
    {
        return $this->_search;
    }

    /**
     * Обработка данных формы
     * 
     * @return void
     */
    protected function submit()
    {
        $q = $this->input->get('q');
        if ($q) {
            $l = mb_strlen($q, 'UTF-8');
            if ($l < 3 || $l > 64)
                $this->_error= GText::_('To search you must enter a word from %s to %s characters', 'search', array(3, 64));
            else
                $this->_search = $this->cleanSearch($q);
        }
    }
}


/**
 * Список статей в поиске
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Search.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmListSearchArticle extends CmList
{
   /**
     * Строка поиска
     * 
     * @var string
     */
    protected $_search = '';

   /**
     * Количество записей выводимых на странице
     * 
     * @var integer
     */
    protected $pageLimit = 4;

    /**
     * Конструктор запроса
     * 
     * @return string
     */
    public function setSearch($value)
    {
        $this->_search = mysql_real_escape_string($value);
    }

    /**
     * Конструктор запроса
     * 
     * @return string
     */
    protected function query()
    {
        if (empty($this->_search)) return '';

        return
            'SELECT SQL_CALC_FOUND_ROWS `a`.*, `p`.*, `ac`.`category_uri` '
          . 'FROM `site_articles` `a` JOIN (SELECT * FROM `site_pages` WHERE `language_id`=%lang) `p` USING(`article_id`) '
          . 'JOIN `site_categories` `ac` USING(`category_id`) '
          . 'WHERE `a`.`article_search`=1 AND MATCH (`page_html_plain`, `page_header`) AGAINST (\'*' . $this->_search . '*\' in boolean mode) %limit';
    }

    /**
     * Возвращает обработаный текст для вывода
     * 
     * @param  string $text текст
     * @param  string $search искать
     * @param  integer $limit количество символов
     * @return string
     */
    protected function getText($text, $search, $limit = 400)
    {
        if (empty($text)) return '';

        if (mb_strlen($text, 'UTF-8') > $limit)
            $text = mb_substr($text, 0, $limit, 'UTF-8') . ' ...';
        $patterns = array("/$search/", "/\{image[0-9]\}/");
        $replacements = array('<span class="sel">' . $search . '</search>', ' ');

        return preg_replace($patterns, $replacements, $text);
    }

    /**
     * Возращает элемент списка
     * 
     * @param  array $record запись
     * @param  integer $index порядковый номер
     * @return void
     */
    public function getItem($record, $index)
    {
        $item = parent::getItem($record, $index);

        $item['category-uri'] = $record['category_uri'];
        // если ЧПУ работает
        if ($this->sef) {
            // если новость
            if ($record['type_id'] == 2)
                $record['article_uri'] = $record['article_id'] . '-' . $record['article_uri'];

            $item['url'] = $this->ln . ($record['category_uri'] != '/' ? $record['category_uri'] : '') . $record['article_uri'];
        } else
            $item['url'] = '/?a=' . $record['article_id'];

        return $item;
    }

    /**
     * Возвращает для каждой ссылки навигации параметры
     * (должны начинаться с "&")
     *
     * @return string
     */
    protected function getPagePrefix()
    {
        return '&' . 'q=' . $this->_search;
    }

}
?>