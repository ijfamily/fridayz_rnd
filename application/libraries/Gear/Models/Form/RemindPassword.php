<?php
/**
 * Gear CMS
 *
 * Модель данных "Форма восстановления учётной записи пользователя"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: RemindPassword.php 2016-01-01 21:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CmForm
 */
Gear::library('/Models/Form');

/**
 * Модель данных формы восстановления учётной записи пользователя
 * 
 * @category   Gear
 * @package    Models
 * @subpackage Form
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: RemindPassword.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmFormRemindPassword extends CmForm
{
    /**
     * Кому предназначен запрос (если форм на странице много)
     *
     * @var string
     */
    public $target = 'from-remindpassword';

    /**
     * Поля формы (вида "{alias1} => {field1}, ....")
     *
     * @var array
     */
    protected $_fields = array(
        'name' => array(
            'check'      => true,
            'use'        => true,
            'field'      => 'name',
            'default'    => '',
            'title'      => 'E-mail or name',
            'allowBlank' => false
        )
    );

    /**
     * Обработка данных формы
     * 
     * @return void
     */
    protected function submit()
    {
        Gear::library('/User');

        // проверка существования учётной записи пользователя
        $user = GUser::getBy($this->input->get('name'), 'login or e-mail');
        if (!$user) {
            GMessage::to('form', 'error', 'A user with the username or e-mail does not exist', $this->path);
            GUserLog::add(array(
                'log_action'  => 'remind password',
                'log_text'    => 'A user with the username or e-mail does not exist',
                'log_success' => 0
            ));
            return;
        }
        // создание кода восстановления
        if (!($code = GUser::setRecoveryCode($user['user_id']))) {
            GMessage::to('form', 'error', 'Error processing forms', $this->path);
            GUserLog::add(array(
                'log_action'  => 'remind password',
                'log_text'    => 'Incorrect recovery hash',
                'log_email'   => $user['contact_email'],
                'user_id'     => $user['user_id'],
                'log_success' => 0
            ));
            return;
        }
        // отправка письма пользователю
        $mail = array(
            'type'     => 'template',
            'template' => 'mail_remind_password.tpl.php',
            'to'       => $user['contact_email'],
            'from'     => Gear::$app->config->get('MAIL/ADMIN'),
            'subject'  => GText::_('Remind password', $this->path) . Gear::$app->config->get('MAIL/HEADER'),
            'data'     => array(
                'id'   => $user['user_id'],
                'code' => $code,
                'hash' => GUser::getRecoveryHash($user['user_id']),
                'host' => Gear::$app->config->get('HOME'),
                'domain' => Gear::$app->config->get('HOST'),
                'site' => Gear::$app->config->get('NAME')
            )
        );
        if ($this->mail->send($mail) == false) {
            GMessage::to('form', 'error', 'Unable to send a message, a server error', $this->path);
            GUserLog::add(array(
                'log_action'  => 'remind password',
                'log_text'    => 'Unable to send a message, a server error',
                'log_email'   => $user['contact_email'],
                'user_id'     => $user['user_id'],
                'log_success' => 0
            ));
            return;
        }
        GMessage::to('form', 'success', 'Successfully sent an email with instructions account recovery', $this->path);
        GUserLog::add(array(
            'log_action' => 'remind password',
            'log_text'   => 'Successfully sent an email with instructions account recovery',
            'log_email'  => $user['contact_email'],
            'user_id'    => $user['user_id']
        ));
    }
}
?>