<?php
/**
 * Gear CMS
 *
 * Модель данных "Форма обратной связи"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Feedback.php 2016-01-01 21:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CmForm
 */
Gear::library('/Models/Form');

/**
 * Модель данных формы обратной связи
 * 
 * @category   Gear
 * @package    Models
 * @subpackage Form
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Feedback.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmFormFeedback extends CmForm
{
    /**
     * Кому предназначен запрос (если форм на странице много)
     *
     * @var string
     */
    public $target = 'form-feedback';

    /**
     * Поля формы (вида "{alias1} => {field1}, ....")
     *
     * @var array
     */
    protected $_fields = array(
        // фамилия имя отчество
        'name' => array(
            'check'      => true,
            'use'        => true,
            'field'      => 'feedback_name',
            'default'    => '',
            'strip'      => true,
            'maxLength'  => 50,
            'title'      => 'Full name',
            'allowBlank' => false
        ),
        // телефон
        'phone' => array(
            'check'      => true,
            'use'        => true,
            'field'      => 'feedback_phone',
            'default'    => '',
            'strip'      => true,
            'maxLength'  => 15,
            'title'      => 'Phone',
            'allowBlank' => true
        ),
        // текст сообщения
        'text' => array(
            'check'      => true,
            'use'        => true,
            'field'      => 'feedback_text',
            'default'    => '',
            'strip'      => true,
            'minLength'  => 3,
            'maxLength'  => 1000,
            'title'      => 'Text',
            'allowBlank' => false
        ),
        // адрес email
        'email' => array(
            'check'      => true,
            'use'        => true,
            'field'      => 'feedback_email',
            'default'    => '',
            'match'      => '/^([0-9a-z]([-_.]?[0-9a-z])*@[0-9a-z]([-.]?[0-9a-z])*\\.[a-wyz][a-z](fo|g|l|m|mes|o|op|pa|ro|seum|t|u|v|z)?)$/',
            'title'      => 'E-mail',
            'allowBlank' => false
        )
    );

    /**
     * Обработка данных формы
     * 
     * @return void
     */
    protected function submit()
    {
        // добавление сообщения
        $table = new GDbTable('site_feedback', 'feedback_id');
        $data = $this->getData();
        $data['feedback_form'] = $this->formName;
        $data['feedback_date'] = date('Y-m-d H:i:s');
        $data['feedback_ip'] = $_SERVER['REMOTE_ADDR'];
        $ip = ip2long($_SERVER['REMOTE_ADDR']);
        if (!($ip == -1 || $ip === false))
            $data['feedback_ip_int'] = sprintf("%u", $ip);
        $data['feedback_url'] = Gear::$app->url->path;
        $data['feedback_browser'] = Gear::$app->browser->fullName;
        $data['feedback_os'] = Gear::$app->browser->os;
        $data['language_id'] = GFactory::getLanguage('id');
        // добавление записи в таблицу 
        if ($table->insert($data) === false) {
            GMessage::to('form', 'error', GText::_('Error processing forms', 'forms'));
            return;
        }
        // если необходимо отправить письмо
        if ($this->sendMail) {
            $to = explode(';', GFactory::getConfig()->get('MAIL/ADMIN'));
            // если в свойствах указаны адреса
            if ($this->toMails)
                $to = array_merge($to, $this->toMails);
            // тема письма
            if ($this->mailSubject)
                $subject = $this->mailSubject;
            else
                $subject =  GText::_('Feedback form', $this->path);
            $mail = array(
                'type'     => 'template',
                'template' => 'form_feedback_mail.tpl.php',
                'to'       => $to,
                'from'     => GFactory::getConfig()->get('MAIL/HOST'),
                'subject'  => $subject,
                'data'     => $data
            );
            if ($this->mail->send($mail) == false) {
                GMessage::to('form', 'error', 'Unable to send a message, a server error', $this->path);
                return;
            }
        }
        $this->_success = $this->get('msg-success', $this->msgSuccess);
        if (empty($this->_success))
            GMessage::to('form', 'success', 'Thank you for your message', $this->path);
        // если не задан переход при успешном сабмите формы
        if (empty($this->location)) {
            $this->location = $_SERVER['REQUEST_URI'];
        }
    }
}


/**
 * Модель данных формы обратной связи (для AJAX запросов)
 * 
 * @category   Gear
 * @package    Models
 * @subpackage Forms
 * @copyright  Copyright (c) 2014-2015 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Feedback.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmjFormFeedback extends CmForm
{
    /**
     * Поля формы (вида "{alias1} => {field1}, ....")
     *
     * @var array
     */
    protected $_fields = array(
        // фамилия имя отчество
        'name' => array(
            'check'      => true,
            'use'        => true,
            'field'      => 'feedback_name',
            'default'    => '',
            'strip'      => true,
            'maxLength'  => 50,
            'title'      => 'Full name',
            'allowBlank' => true
        ),
        // телефон
        'phone' => array(
            'check'      => true,
            'use'        => true,
            'field'      => 'feedback_phone',
            'default'    => '',
            'strip'      => true,
            'maxLength'  => 15,
            'title'      => 'Phone',
            'allowBlank' => true
        ),
        // текст сообщения
        'text' => array(
            'check'      => true,
            'use'        => true,
            'field'      => 'feedback_text',
            'default'    => '',
            'strip'      => true,
            'minLength'  => 3,
            'maxLength'  => 1000,
            'title'      => 'Message',
            'allowBlank' => false
        ),
        // адрес email
        'email' => array(
            'check'      => true,
            'use'        => true,
            'field'      => 'feedback_email',
            'default'    => '',
            'match'      => '/^([0-9a-z]([-_.]?[0-9a-z])*@[0-9a-z]([-.]?[0-9a-z])*\\.[a-wyz][a-z](fo|g|l|m|mes|o|op|pa|ro|seum|t|u|v|z)?)$/',
            'title'      => 'E-mail',
            'allowBlank' => false
        )
    );

    /**
     * Обработка данных формы
     * 
     * @return void
     */
    protected function submit()
    {
        // установить соединение с сервером
        GFactory::getDb()->connect();
        // добавление сообщения
        $table = new GDbTable('site_feedback', 'feedback_id');
        $data = $this->getData();
        $data['feedback_form'] = $this->formName;
        $data['feedback_date'] = date('Y-m-d H:i:s');
        $data['feedback_ip'] = $_SERVER['REMOTE_ADDR'];
        $ip = ip2long($_SERVER['REMOTE_ADDR']);
        if (!($ip == -1 || $ip === false))
            $data['feedback_ip_int'] = sprintf("%u", $ip);
        $data['feedback_url'] = Gear::$app->url->path;
        $data['feedback_browser'] = Gear::$app->browser->fullName;
        $data['feedback_os'] = Gear::$app->browser->os;
        $data['language_id'] = GFactory::getLanguage('id');
        // добавление записи в таблицу 
        if ($table->insert($data) === false) {
            $this->_error = GText::_('Error processing forms', 'forms');
            return;
        }
        // если необходимо отправить письмо
        if ($this->sendMail) {
            $to = explode(';', GFactory::getConfig()->get('MAIL/ADMIN'));
            // если в свойствах указаны адреса
            if ($this->toMails)
                $to = array_merge($to, $this->toMails);
            // тема письма
            if ($this->mailSubject)
                $subject = $this->mailSubject;
            else
                $subject =  GText::_('Feedback form', 'forms');
            $mail = array(
                'type'     => 'template',
                'template' => 'form_feedback_mail.tpl.php',
                'to'       => $to,
                'from'     => GFactory::getConfig()->get('MAIL/HOST'),
                'subject'  => $subject,
                'data'     => $data
            );
            if ($this->mail->send($mail) == false) {
                $this->_error = GText::_('Unable to send a message, a server error', 'forms');
                return;
            }
        }
        $this->_success = $this->get('msg-success', $this->msgSuccess);
        if (empty($this->_success))
            $this->_success = GText::_('Thank you for your message', 'forms');
    }
}
?>