<?php
/**
 * Gear CMS
 *
 * Модель данных "Форма восстановления аккаунта пользователя"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: RecoveryPassword.php 2016-01-01 21:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CmForm
 */
Gear::library('/Models/Form');

/**
 * Модель данных формы восстановления аккаунта пользователя
 * 
 * @category   Gear
 * @package    Models
 * @subpackage Form
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: RecoveryPassword.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmFormRecoveryPassword extends CmForm
{
    /**
     * Кому предназначен запрос (если форм на странице много)
     *
     * @var string
     */
    public $target = 'from-recoverypassword';

    /**
     * Поля формы (вида "{alias1} => {field1}, ....")
     *
     * @var array
     */
    protected $_fields = array(
        'code' => array(
            'check'      => true,
            'use'        => true,
            'field'      => 'code',
            'default'    => '',
            'title'      => 'Code',
            'allowBlank' => false
        )
    );

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        $this->hash = Gear::$app->url->getVar('hash', false);

        parent::__construct($attr);
    }

    /**
     * Обработка данных формы
     * 
     * @return void
     */
    protected function submit()
    {
        Gear::library('/User');

        // проверка существования учётной записи пользователя
        $user = GUser::getBy($this->input->get('code'), 'code');
        if (!$user) {
            GMessage::to('form', 'error', 'Incorrect recovery code', $this->path);
            GUserLog::add(array('log_action' => 'recovery password', 'log_text' => 'Incorrect recovery code', 'log_success' => 0));
            return;
        }
        // проверка хеш-кода
        if (!GUser::checkHash($this->hash, $user['user_id'])) {
            GMessage::to('form', 'error', 'Incorrect recovery hash', $this->path);
            GUserLog::add(array(
                'log_action'  => 'recovery password',
                'log_text'    => 'Incorrect recovery hash',
                'log_email'   => $user['contact_email'],
                'user_id'     => $user['user_id'],
                'log_success' => 0
            ));
            return;
        }
        // сброс пароля
        if (!($password = GUser::resetPassword($user['user_id']))) {
            GMessage::to('form', 'error', 'Unable to reset the password', $this->path);
            GUserLog::add(array(
                'log_action'  => 'recovery password',
                'log_text'    => 'Unable to reset the password',
                'log_email'   => $user['contact_email'],
                'user_id'     => $user['user_id'],
                'log_success' => 0
            ));
            return;
        }
        // отправка письма пользователю
        $mail = array(
            'type'     => 'template',
            'template' => 'mail_recovery_password.tpl.php',
            'to'       => $user['contact_email'],
            'from'     => Gear::$app->config->get('MAIL/ADMIN'),
            'subject'  => GText::_('Remind password', $this->path) . Gear::$app->config->get('MAIL/HEADER'),
            'data'     => array(
                'id'       => $user['user_id'],
                'host'     => Gear::$app->config->get('HOME'),
                'domain'   => Gear::$app->config->get('HOST'),
                'site'     => Gear::$app->config->get('NAME'),
                'e-mail'   => $user['contact_email'],
                'password' => $password
            )
        );
        if ($this->mail->send($mail) == false) {
            GMessage::to('form', 'error', 'Unable to send a message, a server error', $this->path);
            GUserLog::add(array(
                'log_action'  => 'recovery password',
                'log_text'    => 'Unable to send a message, a server error',
                'log_email'   => $user['contact_email'],
                'user_id'     => $user['user_id'],
                'log_success' => 0
            ));
            return;
        }
        GMessage::to('form', 'success', 'Successfully sent an email with instructions account recovery', $this->path);
        GUserLog::add(array(
            'log_action' => 'recovery password',
            'log_text'   => 'Successfully sent an email with instructions account recovery',
            'log_email'  => $user['contact_email'],
            'user_id'    => $user['user_id'],
        ));
    }
}
?>