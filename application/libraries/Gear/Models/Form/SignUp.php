<?php
/**
 * Gear CMS
 *
 * Модель данных "Форма регистрации пользователя"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: SignUp.php 2016-01-01 21:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CmForm
 */
Gear::library('/Models/Form');

/**
 * Модель данных формы регистрации пользователя
 * 
 * @category   Gear
 * @package    Models
 * @subpackage Form
 * @copyright  Copyright (c) 2014-2015 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: SignUp.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmFormSignUp extends CmForm
{
    /**
     * Поля формы (вида "{alias1} => array('field' => {field1}, ...), ....")
     *
     * @var array
     */
    protected $_fields = array(
        // имя пользователя
        'fname' => array(
            'use'        => true,
            'field'      => 'profile_first_name',
            'default'    => '',
            'strip'      => true,
            'maxLength'  => 50,
            'minLength'  => 3,
            'title'      => 'User first name',
            'allowBlank' => false,
            'check'      => true
        ),
        // фамилия пользователя
        'lname' => array(
            'use'        => true,
            'field'      => 'profile_last_name',
            'default'    => '',
            'strip'      => true,
            'maxLength'  => 50,
            'minLength'  => 3,
            'title'      => 'User last name',
            'allowBlank' => false,
            'check'      => true
        ),
        // адрес email
        'email' => array(
            'use'        => true,
            'field'      => 'contact_email',
            'default'    => '',
            'match'      => '/^([0-9a-z]([-_.]?[0-9a-z])*@[0-9a-z]([-.]?[0-9a-z])*\\.[a-wyz][a-z](fo|g|l|m|mes|o|op|pa|ro|seum|t|u|v|z)?)$/',
            'title'      => 'E-mail',
            'allowBlank' => false,
            'check'      => true
        ),
        // пароль
        'password' => array(
            'resetable'  => true,
            'use'        => true,
            'field'      => 'user_password',
            'default'    => '',
            'strip'      => false,
            'minLength'  => 6,
            'maxLength'  => 32,
            'title'      => 'Password',
            'allowBlank' => false,
            'check'      => true
        ),
        // подтв. пароля
        'password-cnf' => array(
            'resetable'  => true,
            'record'     => false,
            'use'        => true,
            'field'      => 'password-cnf',
            'default'    => '',
            'strip'      => false,
            'minLength'  => 6,
            'maxLength'  => 32,
            'title'      => 'Password confirm',
            'allowBlank' => false
        ),
        // код
        'code' => array(
            'use'        => true,
            'record'     => false,
            'field'      => 'code',
            'default'    => '',
            'strip'      => false,
            'title'      => 'Image code',
            'allowBlank' => false
        )
    );

    /**
     * Валидация данных формы на корректность заполнения
     * 
     * @return boolean
     */
    protected function isChecked()
    {
        // проверка поля "ознакмолен(а) с правилами сайта"
        if (Gear::$app->config->getFrom('USERS', 'SIGNUP/RULES'))
            if (!$this->input->get('rules', false)) {
                GMessage::to('form', 'error', 'You are not familiar with the site rules', $this->path);
                return false;
            }

        return parent::isChecked();
    }

    /**
     * Валидация полей формы
     * 
     * @return boolean
     */
    protected function isValidFields()
    {

        if (!parent::isValidFields()) return false;

        // проверка значения поля "пароль"
        if ($this->input->get('password') != $this->input->get('password-cnf')) {
            GMessage::to('form', 'error', 'Password confirmation does not match', $this->path);
            return false;
        }
        $this->input->set('password-cnf', null);
        // код капчи
        $this->input->set('code', null);

        return true;
    }

    /**
     * Обработка данных формы
     * 
     * @return void
     */
    protected function submit()
    {
        Gear::library('/User');

        // максимальное количество зарегистрированных пользователей
        if (($max = Gear::$app->config->getFrom('USERS', 'SIGNUP/MAX')) > 0) {
            if (GUser::isMaximum($max)) {
                GMessage::to('form', 'error', 'The maximum number of users exceeds the allowable limit', $this->path);
                GUserLog::add(array(
                    'log_action'  => 'signup',
                    'log_text'    => 'The maximum number of users exceeds the allowable limit',
                    'log_email'   => $user['contact_email'],
                    'user_id'     => $user['user_id'],
                    'log_success' => 0
                ));
                return;
            }
        }
        // проверка существования пользователя
        if ($user = GUser::getBy($this->input->get('email'), 'e-mail')) {
            GMessage::to('form', 'error', GText::_('A user with this e-mail "%s" already exists', $this->path, array($this->input->get('email'))));
            GUserLog::add(array(
                'log_action'  => 'signup',
                'log_text'    => 'A user with this e-mail already exists',
                'log_email'   => $user['contact_email'],
                'user_id'     => $user['user_id'],
                'log_success' => 0
            ));
            return;
        }
        // регистрация нескольких пользователей с одного IP адреса
        if (!Gear::$app->config->getFrom('USERS', 'SIGNUP/ONE-IP'))
            if (GUser::isOneIp($_SERVER['REMOTE_ADDR'])) {
                GMessage::to('form', 'error', 'User with your ip address is already registered on our site', $this->path);
                GUserLog::add(array(
                    'log_action'  => 'signup',
                    'log_text'    => 'User with your ip address is already registered on our site',
                    'log_email'   => $user['contact_email'],
                    'user_id'     => $user['user_id'],
                    'log_success' => 0
                ));
                return;
            }
        // создание пользователя
        $data = $this->getData();
        // если необходимо подтверждение регистрации
        if (Gear::$app->config->getFrom('USERS', 'SIGNUP/CONFIRM'))
            $data['user_confirm_code'] = GUser::getConfirmCode();
        if (!($userId = GUser::add($data))) {
            GMessage::_('form', 'error', 'Error processing forms', $this->path);
            GUserLog::add(array(
                'log_action'  => 'signup',
                'log_text'    => 'Can`t create user',
                'log_success' => 0
            ));
            return;
        }
        // отправить e-mail уведомителю
        if ($this->sendMail) {
            $mail = array(
                'type'     => 'template',
                'template' => 'mail_signup_admin.tpl.php',
                'to'       => explode(Gear::$app->config->get('MAIL/NOTIFICATION'), ','),
                'from'     => Gear::$app->config->get('MAIL/ADMIN'),
                'subject'  => GText::_('User registration', $this->path) . Gear::$app->config->get('MAIL/HEADER'),
                'data'     => $data
            );
            if ($this->mail->send($mail) == false) {
                GUserLog::add(array(
                    'log_action'  => 'signup',
                    'log_text'    => 'Unable to send a message, a server error',
                    'log_email'   => $data['contact_email'],
                    'user_id'     => $data['user_id'],
                    'log_success' => 0
                ));
                /**
                 * независимо от результата продолжить регистрацию
                 * $this->_error = GText::_('Unable to send a message, a server error', $this->get('path'));
                 * return;
                 */
            }
        }
        // отправить e-mail пользователю для подтверждения регистрации
        if (Gear::$app->config->getFrom('USERS', 'SIGNUP/CONFIRM')) {
            $mail = array(
                'type'     => 'template',
                'template' => 'mail_signup_confirm.tpl.php',
                'to'       => $data['contact_email'],
                'from'     => Gear::$app->config->get('MAIL/HOST'),
                'subject'  => GText::_('Confirmation of registration', $this->path) . Gear::$app->config->get('MAIL/HEADER'),
                'data'     => array(
                    'host'     => Gear::$app->config->get('HOME'),
                    'domain'   => Gear::$app->config->get('HOST'),
                    'site'     => Gear::$app->config->get('NAME'),
                    'e-mail'   => $data['contact_email'],
                    'code'     => $data['user_confirm_code'],
                    'password' => $data['user_password']
                )
            );
            if ($this->mail->send($mail) == false) {
                GMessage::to('form', 'error', 'Unable to send a message, a server error', $this->path);
                GUserLog::add(array(
                    'log_action'  => 'signup',
                    'log_text'    => 'Unable to send a message to admin, a server error',
                    'log_email'   => $data['contact_email'],
                    'user_id'     => $userId,
                    'log_success' => 0
                ));
                return;
            }
        // если без необходимости подтверждения регистрации
        } else {
            GFactory::getAuth()->signIn($userId, $data);
        }
        // если не задан переход при успешном сабмите формы
        if (empty($this->location)) {
            GMessage::to('form', 'success', 'You have successfully been registered', $this->path);
            $this->location = $_SERVER['REQUEST_URI'];
        }
        GUserLog::add(array(
            'log_action'  => 'signup',
            'log_text'    => 'Success',
            'log_email'   => $data['contact_email'],
            'user_id'     => $userId,
            'log_success' => 0
        ));
        if (!Gear::$app->config->getFrom('USERS', 'SIGNUP/CONFIRM'))
            Gear::redirect($this->location);
    }
}


/**
 * Модель данных подтверждения регистрации пользователя
 * 
 * @category   Gear
 * @package    Models
 * @subpackage Form
 * @copyright  Copyright (c) 2014-2015 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: SignUp.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmFormSignUpConfirm extends GModel
{
    public $code = '';

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // установить соединение с сервером
        GFactory::getDb()->connect();
        // код подтверждения
        $this->code = $this->url->getVar('code');
    }

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function checkConfirm()
    {
        Gear::library('/User');

        if (empty($this->code)) return false;

        $confirm = GUser::confirm($this->code);

        GUserLog::add(array(
            'log_action'  => 'confirm',
            'log_text'    => $confirm ? 'Success' : 'Error',
            'log_success' => (int) $confirm
        ));

        return $confirm;
    }
}
?>