<?php
/**
 * Gear CMS
 *
 * Модель данных "Форма авторизации пользователя"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: SignIn.php 2016-01-01 21:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CmForm
 */
Gear::library('/Models/Form');

/**
 * Модель данных формы авторизации пользователя
 * 
 * @category   Gear
 * @package    Models
 * @subpackage Form
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: SignIn.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmFormSignIn extends CmForm
{
    /**
     * Кому предназначен запрос (если форм на странице много)
     *
     * @var string
     */
    public $target = 'form-signin';

    /**
     * Поля формы (вида "{alias1} => {field1}, ....")
     *
     * @var array
     */
    protected $_fields = array(
        // адрес email
        'email' => array(
            'check'      => true,
            'use'        => true,
            'field'      => 'contact_email',
            'default'    => '',
            'match'      => '/^([0-9a-z]([-_.]?[0-9a-z])*@[0-9a-z]([-.]?[0-9a-z])*\\.[a-wyz][a-z](fo|g|l|m|mes|o|op|pa|ro|seum|t|u|v|z)?)$/',
            'title'      => 'E-mail',
            'allowBlank' => true
        ),
        // пароль
        'password' => array(
            'check'      => true,
            'use'        => true,
            'field'      => 'user_password',
            'default'    => '',
            'strip'      => false,
            'minLength'  => 6,
            'maxLength'  => 32,
            'title'      => 'Password',
            'allowBlank' => false
        )
    );

    /**
     * Переход по адресу если запрос был успешен
     *
     * @var string
     */
    public $location = '/';

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        $this->type = Gear::$app->config->getFrom('USERS', 'SIGNIN/TYPE');

        parent::__construct($attr);
    }

    /**
     * Обработка данных формы
     * 
     * @return void
     */
    protected function submit()
    {
        Gear::library('/User');

        // проверка существования пользователя
        $user = GUser::getBy($this->input->get('name'), $this->type);
        if ($user == false) {
            switch ($this->type) {
                case 'login':
                    $msg = 'You are in an incorrect username or password, please try again';
                    GMessage::to('form', 'error', $msg, $this->path);
                    break;

                case 'e-mail':
                    $msg = 'You are in an incorrect e-mail or password, please try again';
                    GMessage::to('form', 'error', $msg, $this->path);
                    break;

                case 'login_or_e-mail': 
                    $msg = 'You are in an incorrect username (e-mail) or password, please try again';
                    GMessage::to('form', 'error', $msg, $this->path);
                    break;
            }
            GUserLog::add(array(
                'log_action'  => 'signin',
                'log_text'    => $msg,
                'log_email'   => $this->input->get('name'),
                'log_success' => 0
            ));
            return;
        }
        // если аккаунт не подтвержден
        if (empty($user['user_confirm_date'])) {
            $msg = 'Your account is not activated';
            GMessage::to('form', 'error', $msg, $this->path);
            GUserLog::add(array(
                'log_action'  => 'signin',
                'log_text'    => $msg,
                'log_email'   => $this->input->get('name'),
                'log_success' => 0
            ));
            return;
        }
        $auth = GFactory::getAuth();
        $auth->signIn($user['user_id'], $user);
        GUserLog::add(array(
            'log_action' => 'signin',
            'log_text'   => 'Success',
            'log_email'  => $user['contact_email'],
            'user_id'    => $user['user_id']
        ));
        Gear::redirect('/');
    }
}


/**
 * Модель данных формы авторизации пользователя (для AJAX запросов)
 * 
 * @category   Gear
 * @package    Models
 * @subpackage Forms
 * @copyright  Copyright (c) 2014-2015 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: SignIn.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmjFormSignIn extends CmForm
{
    /**
     * Проверять хэш
     *
     * @var boolean
     */
    protected $_checkHash = false;

    /**
     * Кому предназначен запрос (если форм на странице много)
     *
     * @var string
     */
    public $target = 'form-signin';

    /**
     * Поля формы (вида "{alias1} => {field1}, ....")
     *
     * @var array
     */
    protected $_fields = array(
        // адрес email
        'email' => array(
            'check'      => true,
            'use'        => true,
            'field'      => 'contact_email',
            'default'    => '',
            'match'      => '/^([0-9a-z]([-_.]?[0-9a-z])*@[0-9a-z]([-.]?[0-9a-z])*\\.[a-wyz][a-z](fo|g|l|m|mes|o|op|pa|ro|seum|t|u|v|z)?)$/',
            'title'      => 'E-mail',
            'allowBlank' => true
        ),
        // пароль
        'password' => array(
            'check'      => true,
            'use'        => true,
            'field'      => 'user_password',
            'default'    => '',
            'strip'      => false,
            'minLength'  => 6,
            'maxLength'  => 32,
            'title'      => 'Password',
            'allowBlank' => false
        )
    );

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        $this->type = Gear::$app->config->getFrom('USERS', 'SIGNIN/TYPE');

        parent::__construct($attr);
    }

    /**
     * Обработка данных формы
     * 
     * @return void
     */
    protected function submit()
    {
        Gear::library('/User');
        // проверка существования пользователя
        $user = GUser::getBy($this->input->get('name'), $this->type);
        if ($user == false || empty($user)) {
            switch ($this->type) {
                case 'login':
                    $msg = 'You are in an incorrect username or password, please try again';
                    GMessage::to('form', 'error', $msg, $this->path);
                    break;

                case 'e-mail':
                    $msg = 'You are in an incorrect e-mail or password, please try again';
                    GMessage::to('form', 'error', $msg, $this->path);
                    break;

                case 'login_or_e-mail': 
                    $msg = 'You are in an incorrect username (e-mail) or password, please try again';
                    GMessage::to('form', 'error', $msg, $this->path);
                    break;

                default:
                    GMessage::to('form', 'error', 'Unknow type signin', $this->path);
            }
            GUserLog::add(array(
                'log_action'  => 'signin',
                'log_text'    => $msg,
                'log_email'   => $this->input->get('name'),
                'log_success' => 0
            ));
            return;
        }
        // если есть необходимость подтверждать регистрацию
        if (Gear::$app->config->getFrom('USERS', 'SIGNUP/CONFIRM')) {
            // если аккаунт не подтвержден
            if (empty($user['user_confirm_date'])) {
                $msg = 'Your account is not activated';
                GMessage::to('form', 'error', $msg, $this->path);
                GUserLog::add(array(
                    'log_action'  => 'signin',
                    'log_text'    => $msg,
                    'log_email'   => $this->input->get('name'),
                    'log_success' => 0
                ));
                return;
            }
        }
        $auth = GFactory::getAuth();
        $auth->signIn($user['user_id'], $user);
        GUserLog::add(array(
            'log_action' => 'signin',
            'log_text'   => 'Success',
            'log_email'  => $user['contact_email'],
            'user_id'    => $user['user_id']
        ));
    }
}

?>