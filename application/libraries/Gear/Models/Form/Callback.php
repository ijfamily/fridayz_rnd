<?php
/**
 * Gear CMS
 *
 * Модель данных "Форма обратного вызова"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Callback.php 2016-01-01 21:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CmForm
 */
Gear::library('/Models/Form');

/**
 * Модель данных формы обратного вызова
 * 
 * @category   Gear
 * @package    Models
 * @subpackage Form
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Callback.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmFormCallback extends CmForm
{
    /**
     * Кому предназначен запрос (если форм на странице много)
     *
     * @var string
     */
    public $target = 'form-callback';

    /**
     * Поля формы (вида "{alias1} => {field1}, ....")
     *
     * @var array
     */
    protected $_fields = array(
        // фамилия имя отчество
        'name' => array(
            'check'      => true,
            'use'        => true,
            'field'      => 'feedback_name',
            'default'    => '',
            'strip'      => true,
            'maxLength'  => 50,
            'title'      => 'Full name',
            'allowBlank' => false
        ),
        // телефон
        'phone' => array(
            'check'      => true,
            'use'        => true,
            'field'      => 'feedback_phone',
            'default'    => '',
            'strip'      => true,
            'maxLength'  => 15,
            'title'      => 'Phone',
            'allowBlank' => false
        ),
    );

    /**
     * Обработка данных формы
     * 
     * @return void
     */
    protected function submit()
    {
        // добавление сообщения
        $table = new GDbTable('site_feedback', 'feedback_id');
        $data = $this->getData();
        $data['feedback_form'] = $this->formName;
        $data['feedback_date'] = date('Y-m-d H:i:s');
        $data['feedback_ip'] = $_SERVER['REMOTE_ADDR'];
        $ip = ip2long($_SERVER['REMOTE_ADDR']);
        if (!($ip == -1 || $ip === false))
            $data['feedback_ip_int'] = sprintf("%u", $ip);
        $data['feedback_url'] = Gear::$app->url->path;
        $data['feedback_browser'] = Gear::$app->browser->fullName;
        $data['feedback_os'] = Gear::$app->browser->os;
        $data['language_id'] = GFactory::getLanguage('id');
        // добавление записи в таблицу 
        if ($table->insert($data) === false) {
            GMessage::to('form', 'error', 'Error processing forms', $this->path);
            return;
        }
        // если необходимо отправить письмо
        if ($this->sendMail) {
            $to = explode(';', GFactory::getConfig()->get('MAIL/ADMIN'));
            // если в свойствах указаны адреса
            if ($this->toMails)
                $to = array_merge($to, $this->toMails);
            // тема письма
            if ($this->mailSubject)
                $subject = $this->mailSubject;
            else
                $subject =  GText::_('Callback form', 'forms');
            $mail = array(
                'type'     => 'template',
                'template' => 'form_callback_mail.tpl.php',
                'to'       => $to,
                'from'     => GFactory::getConfig()->get('MAIL/HOST'),
                'subject'  => $subject,
                'data'     => $data
            );
            if ($this->mail->send($mail) == false) {
                GMessage::to('form', 'error', 'Unable to send a message, a server error', $this->path);
                return;
            }
        }
        $this->_success = $this->get('msg-success', $this->msgSuccess);
        if (empty($this->_success))
           GMessage::to('form', 'success', 'Our manager in the near future you will be contacted', $this->path);
        // если не задан переход при успешном сабмите формы
        if (empty($this->location)) {
            $this->location = $_SERVER['REQUEST_URI'];
        }
    }
}


/**
 * Модель данных формы обратного вызова (для AJAX запросов)
 * 
 * @category   Gear
 * @package    Models
 * @subpackage Forms
 * @copyright  Copyright (c) 2014-2015 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Callback.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmjFormCallback extends CmjForm
{
    /**
     * Поля формы (вида "{alias1} => {field1}, ....")
     *
     * @var array
     */
    protected $_fields = array(
        // фамилия имя отчество
        'name' => array(
            'check'      => true,
            'use'        => true,
            'field'      => 'feedback_name',
            'default'    => '',
            'strip'      => true,
            'maxLength'  => 50,
            'title'      => 'Full name',
            'allowBlank' => true
        ),
        // телефон
        'phone' => array(
            'check'      => true,
            'use'        => true,
            'field'      => 'feedback_phone',
            'default'    => '',
            'strip'      => true,
            'maxLength'  => 15,
            'title'      => 'Phone',
            'allowBlank' => true
        )
    );

    /**
     * Обработка данных формы
     * 
     * @return void
     */
    protected function submit()
    {
        // установить соединение с сервером
        GFactory::getDb()->connect();
        // добавление сообщения
        $table = new GDbTable('site_feedback', 'feedback_id');
        $data = $this->getData();
        $data['feedback_form'] = $this->formName;
        $data['feedback_date'] = date('Y-m-d H:i:s');
        $data['feedback_ip'] = $_SERVER['REMOTE_ADDR'];
        $ip = ip2long($_SERVER['REMOTE_ADDR']);
        if (!($ip == -1 || $ip === false))
            $data['feedback_ip_int'] = sprintf("%u", $ip);
        $data['feedback_url'] = Gear::$app->url->path;
        $data['feedback_browser'] = Gear::$app->browser->fullName;
        $data['feedback_os'] = Gear::$app->browser->os;
        $data['language_id'] = GFactory::getLanguage('id');
        // добавление записи в таблицу 
        if ($table->insert($data) === false) {
            $this->_error = GText::_('Error processing forms', 'forms');
            return;
        }
        // если необходимо отправить письмо
        if ($this->sendMail) {
            $to = explode(';', GFactory::getConfig()->get('MAIL/ADMIN'));
            // если в свойствах указаны адреса
            if ($this->toMails)
                $to = array_merge($to, $this->toMails);
            // тема письма
            if ($this->mailSubject)
                $subject = $this->mailSubject;
            else
                $subject =  GText::_('Callback form', 'forms');
            $mail = array(
                'type'     => 'template',
                'template' => 'form_callback_mail.tpl.php',
                'to'       => $to,
                'from'     => GFactory::getConfig()->get('MAIL/HOST'),
                'subject'  => $subject,
                'data'     => $data
            );
            if ($this->mail->send($mail) == false) {
                $this->_error = GText::_('Unable to send a message, a server error', 'forms');
                return;
            }
        }
        $this->_success = $this->get('msg-success', $this->msgSuccess);
        if (empty($this->_success))
            $this->_success = GText::_('Our manager in the near future you will be contacted', 'forms');
    }
}
?>