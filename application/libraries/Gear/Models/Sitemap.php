<?php
/**
 * Gear CMS
 *
 * Модель данных "Карта сайта"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Sitemap.php 2016-01-01 21:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Модель данных карты сайта
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Sitemap.php 2016-01-01 21:00:00 Gear Magic $
 */
class CmSitemap extends GModel
{
    /**
     * Вероятная частота изменения страницы
     *
     * @var array
     */
    protected $_changefreq = array(
        'a' => 'always', 'h' => 'hourly', 'd' => 'daily', 'w' => 'weekly', 'm' => 'monthly', 'y' => 'yearly', 'n' => 'never'
    );

    /**
     * Маскирование символов
     *
     * @return string
     */
    protected function maskingSymbols($str)
    {
        if ($str == '/')
            return '';
        else
            return str_replace(array('&', '\'', '"','> ', '<'), array('&amp;', '&apos;', '&quot;', '&gt;','&lt;'), $str);
    }

    /**
     * Вывод XML статей
     *
     * @param object $query указатель на запрос
     * @return void
     */
    protected function XMLArticles($query = null)
    {
        if (is_null($query))
            $query = new GDbQuery();
        $sql = 'SELECT `p`.*, `a`.*, `c`.`category_uri` '
             . 'FROM `site_pages` `p`, `site_articles` `a` LEFT JOIN `site_categories` `c` USING(`category_id`) '
             . 'WHERE `a`.`article_id`=`p`.`article_id` AND '
             . '`a`.`article_id`<>1 AND `a`.`published`=1 AND `a`.`article_sitemap`=1 AND '
             . '`a`.`domain_id`=' . Gear::$app->domainId . ' AND `p`.`language_id`=' . GFactory::getLanguage('id');
        if ($query->execute($sql) === false) die('Can`t create Sitemap (bad request)');
        while (!$query->eof()) {
            $rec = $query->next();
            if (empty($rec['published_date'])) continue;
            echo "<url>\r\n";
            echo '<loc>';
            echo $this->maskingSymbols(GArticles::genUrl($rec, $this->ln, $this->sef, true));
            echo "</loc>\r\n";
            echo '<lastmod>' . date('Y-m-d', strtotime($rec['published_date'])) . "</lastmod>\r\n";
            // если указан приоритет для страницы
            if ($rec['sitemap_priority'] > 0) {
                echo '<priority>' . $rec['sitemap_priority'] . "</priority>\r\n";
            }
            // если указана частота изменения страницы
            if ($rec['sitemap_changefreq'])
                if (isset($this->_changefreq[$rec['sitemap_changefreq']]))
                    echo '<changefreq>' . $this->_changefreq[$rec['sitemap_changefreq']] . "</changefreq>\r\n";
            echo "</url>\r\n";
        }
    }

    /**
     * Вывод XML категорий статей
     *
     * @param object $query указатель на запрос
     * @return void
     */
    protected function XMLCategories($query = null)
    {
        if (is_null($query))
            $query = new GDbQuery();
        $sql = 'SELECT * FROM `site_categories` WHERE `list`=1 AND `category_visible`=1 AND `domain_id`=' . Gear::$app->domainId;
        if ($query->execute($sql) === false) die('Can`t create Sitemap (bad request)');
        while (!$query->eof()) {
            $rec = $query->next();
            if ($rec['category_uri'] == '/') continue;
            if (empty($rec['sys_date_update']))
                $date = $rec['sys_date_insert'];
            else
                $date = $rec['sys_date_update'];
            if (empty($date))
                $date = date('Y-m-d');
            echo "<url>\r\n";
            echo '<loc>http://' . $_SERVER['SERVER_NAME'] . '/';
            if ($rec['category_uri'])
                echo $rec['category_uri'];
            echo "</loc>\r\n";
            echo '<lastmod>' . $date . "</lastmod>\r\n";
            echo "</url>\r\n";
        }
    }

    /**
     * Вывод XML
     *
     * @return void
     */
    public function createXML()
    {
        // установить соединение с сервером
        GFactory::getDb()->connect();
        // обработчик SQL запросов
        $query = new GDbQuery();

        echo '<?xml version="1.0" encoding="UTF-8"?>', "\r\n";
        echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">', "\r\n";
        $this->XMLArticles($query);
        $this->XMLCategories($query);
        echo "</urlset>\r\n";
    }
}
?>