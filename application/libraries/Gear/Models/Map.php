<?php
/**
 * Gear CMS
 *
 * Модель данных "Карта сайта"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Map.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Модель данных карты сайта
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Map.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmMap extends GModel
{
    /**
     * Возращает дерево элементов
     *
     * @return array
     */
    public function getItems()
    {
        // установить соединение с сервером
        GFactory::getDb()->connect();
        $query = new GDbQuery();
        // категории статей
        $sql = 'SELECT `a`.*, `p`.*, `c`.* FROM `site_articles` `a` JOIN `site_categories` `c` USING(`category_id`) JOIN `site_pages` `p` ON `a`.`article_id`=`p`.`article_id` AND `a`.`article_map`=1 AND '
             . '`a`.`published`=1 AND `p`.`language_id`=' . GFactory::getLanguage('id') . ' AND '
             . '`a`.`article_uri`="index.html" ORDER BY `category_left` ASC';
        if ($query->execute($sql) === false)
            throw new GException('Query error (Internal Server Error)', 'Response from the database: %s', $query->getError(), __CLASS__);
        $items = array('cats' => array(), 'pages' => array());
        $indexes = array();
        $index = 0;
        while (!$query->eof()) {
            $item = $query->next();
            if ($this->sef)
                $url = $item['category_uri'];
            else
                $url = '?a=' . $item['article_id'];
            $items['cats'][] = array(
                'id'       => $item['article_id'],
                'name'     => $item['category_name'],
                'url'      => $url,
                'level'    => $item['category_level'],
                'is-root'  => false,
                'disabled' => false,
                'pages'    => array()
            );
            $indexes[$item['category_id']] = $index;
            $index++;
        }

        // статьи
        $sql = 'SELECT * FROM `site_articles` `a` JOIN `site_pages` `p` ON `a`.`article_id`=`p`.`article_id` AND `a`.`article_map`=1 AND '
             . '`a`.`published`=1 AND `p`.`language_id`=' . GFactory::getLanguage('id') . ' AND '
             . '`a`.`article_uri`<>"index.html" ORDER BY `article_index` ASC';
        if ($query->execute($sql) === false)
            throw new GException('Query error (Internal Server Error)', 'Response from the database: %s', $query->getError(), __CLASS__);
        while (!$query->eof()) {
            $item = $query->next();
            if ($this->sef)
                $url = $item['article_uri'];
            else
                $url = '?a=' . $item['article_id'];
            $pageItem = array(
                'id'       => $item['article_id'],
                'name'     => $item['page_header'],
                'url'      => $url,
                'is-root'  => false,
                'disabled' => false
            );
            if (!empty($item['category_id'])) {
                if (isset($indexes[$item['category_id']])) {
                    $index = $indexes[$item['category_id']];
                    // если ЧПУ работает
                    if ($this->sef)
                        $pageItem['url'] = $items['cats'][$index]['url'] . $pageItem['url'];
                    $items['cats'][$index]['pages'][] = $pageItem;
                    continue;
                }
            }
            $items['pages'][] = $pageItem;
        }

        return $items;
    }
}
?>