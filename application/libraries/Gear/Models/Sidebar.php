<?php
/**
 * Gear CMS
 *
 * Модель данных "Боковое меню"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Sidebar.php 2016-01-01 21:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Базовый класс модели данных бокового меню
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Sidebar.php 2016-01-01 21:00:00 Gear Magic $
 */
class CmSidebar extends GModel
{
    /**
     * Обработчик SQL запроса
     *
     * @var object
     */
    public $query;

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // установить соединение с сервером
        GFactory::getDb()->connect();
        // обработчик SQL запросов
        $this->query = new GDbQuery();
    }
}
?>