<?php
/**
 * Gear CMS
 *
 * Модель данных "Список документов"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Documents.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CmList
 */
Gear::library('/Models/List');

/**
 * Список документов
 * 
 * @category   Gear
 * @package    Models
 * @subpackage List
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Documents.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmListDocuments extends CmList
{
    /**
     * Идентификатор статьи, документы которой выводятся
     *
     * @var integer
     */
    protected $_articleId;

    /**
     * Тип документа который выводится в списке ("doc", "xls", ...)
     *
     * @var string
     */
    protected $_type = '';

    /**
     * Вывод документа в список (в зависимости от сотояния "архив")
     *
     * @var mixed
     */
    protected $_archive = 0;

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // ексли указан идентификатор статьи
        $this->_articleId = $this->get('data-article', $this->_sid);
        if (empty($this->_articleId))
            $this->_articleId = GFactory::getDocument('id');
        // тип документа ("doc", ...)
        $this->_type = $this->get('data-type', $this->_type);
        // архив
        $this->_archive = $this->get('data-archive', $this->_archive);
    }

    /**
     * Конструктор запроса
     * 
     * @return string
     */
    protected function query()
    {
        return
            'SELECT * FROM `site_documents` `cd`, `site_documents_l` `cdl` '
          . 'WHERE `cdl`.`document_id`=`cd`.`document_id` AND '
          . '`cdl`.`language_id`=%lang AND '
          . '`cd`.`document_visible`=1 AND '
          . '`cd`.`document_archive`=' . $this->_archive . ' AND '
          . '`cd`.`article_id`=' . $this->_articleId
          . ($this->_type ? " AND `cd`.`document_type`='{$this->_type}'" : '')
          . ' ORDER BY `cd`.`document_index` ASC';
    }

    /**
     * Вывод элемента списка
     * 
     * @param  array $item элемент списка из базы данных
     * @param  integer $index порядковый номер
     * @return void
     */
    public function getItem($item, $index)
    {
            if (empty($item['document_title']))
                $title = '';
            else
                $title = str_replace('"', '&#34;', $item['document_title']);
            if (empty($item['document_filesize']))
                $size = '';
            else
                $size = ' (' . GFile::sizeToStr($item['document_filesize']) . ')';

        return array(
            'title' => $title,
            'size'  => $size,
            'type'  => $item['document_type'],
            'src'   => $item['document_filename']
        );
    }
}
?>