<?php
/**
 * Gear CMS
 *
 * Модель данных "Список альбома изображений"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Albums.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CmList
 */
Gear::library('/Models/List');

/**
 * Модель фотоальбома (для AJAX)
 * 
 * @category   Gear
 * @package    Models
 * @subpackage List
 * @copyright  Copyright (c) 2013-2016 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Albums.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmjAlbumsImages extends CmjList
{
    /**
     * Идентификатор статьи
     *
     * @var integer
     */
    protected $_articleId;

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        $input = GFactory::getApplication()->input;
        // ексли указан идентификатор статьи
        $this->articleId = (int) $input->get('a', $this->articleId);
    }

    /**
     * Конструктор запроса
     * 
     * @return string
     */
    protected function query()
    {
        return
           'SELECT SQL_CALC_FOUND_ROWS `al`.`album_title`, `a`.*, `ai`.`image_thumb_filename` '
         . 'FROM `site_albums_l` `al`, `site_albums` `a` '
         . 'LEFT JOIN `site_album_images` `ai` ON `ai`.`image_id`=`a`.`cover_id` '
         . 'WHERE `a`.`album_id`=`al`.`album_id` AND'
         . '`al`.`language_id`=%lang AND '
         . '`a`.`album_visible`=1 AND '
         . '`a`.`article_id`=' . $this->articleId
         .' ORDER BY `a`.`album_index` ASC %limit';
    }

    /**
     * Вывод элемента списка
     * 
     * @param  array $item элемент списка из базы данных
     * @param  integer $index порядковый номер
     * @return void
     */
    public function getItem($item, $index)
    {
        return array(
            'id'    => $item['album_id'],
            'src'   => empty($item['image_thumb_filename']) ? 'none.png' : $item['image_thumb_filename'],
            'url'   => '?id=' . $item['album_id'],
            'title' => $item['album_title'],
            'class' => empty($item['image_thumb_filename']) ? 'class="none"' : ''
        );
    }
}


/**
 * Список фотоальбомов
 * 
 * @category   Gear
 * @package    Models
 * @subpackage List
 * @copyright  Copyright (c) 2013-2016 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Albums.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmAlbums extends CmList
{
    /**
     * Идентификатор категории статьи
     *
     * @var integer
     */
    public $categoryId = 0;

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // ексли указан идентификатор категории статьи
        if (empty(Gear::$app->category['category_id']))
            $categoryId = 0;
        else
            $categoryId = Gear::$app->category['category_id'];
        $this->categoryId = $this->get('list-category-id', $categoryId);
    }

    /**
     * Конструктор запроса
     * 
     * @return string
     */
    protected function query()
    {
        $sql = 'SELECT SQL_CALC_FOUND_ROWS `g`.*, `i`.`image_entire_filename` `cover` FROM `site_albums` `g` '
             . 'LEFT JOIN `site_album_images` `i` ON `i`.`album_id`=`g`.`album_id` AND `i`.`image_cover`=1 '
             . 'WHERE `g`.`published`=1';
         // если есть фильтарция списка через календарь
         if ($this->dateOn)
            $sql .= " AND `g`.`published_date`='" . $this->dateOn['date'] . "' ";
        // если необходимы вывод статей в подкатегориях
        $sql .= ' AND `g`.`category_id`=' . $this->categoryId;
        $sql .= ' ORDER BY `g`.`published_date` DESC %limit';

        return $sql;
    }

    /**
     * Возращает элемент списка
     * 
     * @param  array $record запись
     * @param  integer $index порядковый номер
     * @return void
     */
    public function getItem($record, $index)
    {
        return array(
            'id'     => $record['album_id'],
            'index'  => $record['album_index'],
            'name'   => $record['album_name'],
            'desc'   => $record['album_description'],
            'cover'  => $record['cover'],
            'folder' => $record['album_folder'],
            'date'   => $record['published_date'],
            'time'   => $record['published_time'],
            'ln'     => $this->ln
        );
    }
}


/**
 * Модель данных фотоальбома
 * 
 * @category   Gear
 * @package    Models
 * @subpackage List
 * @copyright  Copyright (c) 2013-2016 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Albums.php 2013-07-01 12:00:00 Gear Magic $
 */
class CmAlbumImages extends CmList
{
    /**
     * Идентификатор галереи
     *
     * @var integer
     */
    public $albumId = 0;

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // идентификатор фотоальбома
        $this->albumId = (int) $this->url->getSeg(1, 0);
    }

    /**
     * Конструктор запроса
     * 
     * @return string
     */
    protected function query()
    {
        return
            'SELECT SQL_CALC_FOUND_ROWS `g`.*, `il`.*, `i`.* FROM (SELECT * FROM `site_albums` WHERE `album_id`=' . $this->albumId .') `g` '
          . 'JOIN `site_album_images` `i` ON `i`.`album_id`=`g`.`album_id` AND `i`.`image_visible`=1 '
          . 'LEFT JOIN `site_album_images_l` `il` ON `i`.`image_id`=`il`.`image_id` AND `il`.`language_id`=%lang AND `i`.`image_visible`=1 '
          . ' ORDER BY `i`.`image_index` ASC %limit';
    }

    /**
     * Возращает данные фотоальбома
     * 
     * @return mixed
     */
    public function getAlbum()
    {
        if (empty($this->albumId)) return false;

        $sql = 'SELECT * FROM `site_albums` WHERE `album_id`=' . $this->albumId;
        if (($rec = $this->_query->getRecord($sql)) === false)
            throw new GException('Query error (Internal Server Error)', 'Response from the database: %s', $this->_query->getError(), __CLASS__ . '::' . __FUNCTION__);
        // если статья не опубликована
        if (!empty($rec))
            if (!$rec['published']) {
                // если открыта админпанель и есть привью альбома
                if (!(Gear::$app->hasToken && isset($_GET['preview'])))
                    $rec = false;
            }

        return $rec;
    }

    /**
     * Вывод элемента списка
     * 
     * @param  array $item элемент списка из базы данных
     * @param  integer $index порядковый номер
     * @return void
     */
    public function getItem($item, $index)
    {
        if ($item['image_title'])
            $alt = $title = $item['image_title'];
        else
            $alt = $title = $item['album_name'];

        return array(
            'id' => $item['image_id'],
            'i' => array('file' => $item['image_entire_filename']),
            't' => array('file' => $item['image_thumb_filename'], 'title' => $item['image_thumb_title']),
            'folder' => $item['album_folder'],
            'title'  => $title,
            'alt'    => $alt
        );
    }
}
?>