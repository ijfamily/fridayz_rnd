<?php
/**
 * Gear CMS
 *
 * Модель данных "Список изображений галереи"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Gallery.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CmList
 */
Gear::library('/Models/List');

/**
 * Список изображений галереи
 * 
 * @category   Gear
 * @package    Models
 * @subpackage List
 * @copyright  Copyright (c) 2013-2016 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Gallery.php 2013-07-01 12:00:00 Gear Magic $
 */
class CmGalleryImages extends CmList
{
   /**
     * Псевдоним поля (из $_fieldAlias) используемый для сортировки по умолчанию
     * 
     * @var string
     */
    protected $_field = 'date';

    /**
     * Идентификатор галереи
     *
     * @var integer
     */
    public $galleryId;

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // ексли указан идентификатор статьи
        $this->galleryId = $this->get('data-id', $this->galleryId);
    }

    /**
     * Конструктор запроса
     * 
     * @return string
     */
    protected function query()
    {
        return
            'SELECT SQL_CALC_FOUND_ROWS * FROM (SELECT * FROM `site_gallery` WHERE `gallery_id`=' . $this->galleryId .') `g` '
          . 'JOIN `site_gallery_images` `i` USING(`gallery_id`) JOIN `site_gallery_images_l` `il` ON `i`.`image_id`=`il`.`image_id` AND `il`.`language_id`=%lang AND `i`.`image_visible`=1 '
          . ' ORDER BY `i`.`image_index` DESC, `i`.`image_id` DESC %limit';
    }

    /**
     * Вывод элемента списка
     * 
     * @param  array $item элемент списка из базы данных
     * @param  integer $index порядковый номер
     * @return void
     */
    public function getItem($item, $index)
    {
        
        if (empty($item['image_title'])) {
            $alt = $this->_['photo'] . $item['image_index'];
            $title = $this->_['photo'] . $item['image_index'];
        } else {
            $alt = $title = $item['image_title'];
        }

        return array(
            'id' => $item['image_id'],
            'i' => array('file' => $item['image_entire_filename']),
            't' => array('file' => $item['image_thumb_filename'], 'title' => $item['image_thumb_title']),
            'folder' => $item['gallery_folder'],
            'title'  => $title,
            'alt'    => $alt
        );
    }
}


/**
 * Список изображений галереи (AJAX)
 * 
 * @category   Gear
 * @package    Models
 * @subpackage List
 * @copyright  Copyright (c) 2013-2016 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Gallery.php 2016-01-01 12:00:00 Gear Magic $
 */

class CmjGalleryImages extends CmjList
{
    /**
     * Идентификатор галереи
     *
     * @var integer
     */
    public $galleryId;

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        $input = GFactory::getApplication()->input;
        // ексли указан идентификатор галереи
        $this->galleryId = (int) $input->get('g', $this->galleryId);
    }

    /**
     * Конструктор запроса
     * 
     * @return string
     */
    protected function query()
    {
        return
            'SELECT SQL_CALC_FOUND_ROWS * FROM (SELECT * FROM `site_gallery` WHERE `gallery_id`=' . $this->galleryId .') `g` '
          . 'JOIN `site_gallery_images` `i` USING(`gallery_id`) JOIN `site_gallery_images_l` `il` ON `i`.`image_id`=`il`.`image_id` AND `il`.`language_id`=%lang AND `i`.`image_visible`=1 '
          . ' ORDER BY `i`.`image_index` ASC %limit';
    }

    /**
     * Вывод элемента списка
     * 
     * @param  array $item элемент списка из базы данных
     * @param  integer $index порядковый номер
     * @return void
     */
    public function getItem($item, $index)
    {
        if (empty($item['image_title'])) {
            $alt = $this->_['photo'] . $item['image_index'];
            $title = $this->_['photo'] . $item['image_index'];
        } else {
            $alt = $item['image_title'];
            $title = $item['image_title'];
        }

        return array(
            'i' => array('file' => $item['image_entire_filename']),
            't' => array('file' => $item['image_thumb_filename']),
            'folder' => $item['gallery_folder'],
            'title'  => $title,
            'alt'    => $alt
        );
    }
}
?>