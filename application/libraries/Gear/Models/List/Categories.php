<?php
/**
 * Gear CMS
 *
 * Модель данных "Список категорий статей"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Categories.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CmList
 */
Gear::library('/Models/List');

/**
 * Список категорий статей
 * 
 * @category   Gear
 * @package    Models
 * @subpackage List
 * @copyright  Copyright (c) 2013-2016 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Categories.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmListCategories extends CmList
{
   /**
     * Псевдонимы полей используемые для сортировки списка
     * вида: array('alias' => 'field', ... )
     * 
     * @var array
     */
    protected $_fieldAlias = array(
        'date'   => 'category_name',
        'header' => 'category_name'
    );

    /**
     * Идентификатор категории статьи
     *
     * @var integer
     */
    protected $_categoryId;

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // ексли указан идентификатор категории статьи
        if (empty(Gear::$app->category['category_id']))
            $categoryId = 0;
        else
            $categoryId = Gear::$app->category['category_id'];
        $this->_categoryId = $this->get('list-category-id', $categoryId);
    }

    /**
     * Конструктор запроса
     * 
     * @return string
     */
    protected function query()
    {
        $cat = Gear::$app->category;
        $sql = 
           'SELECT SQL_CALC_FOUND_ROWS * FROM `site_categories` '
         . 'WHERE `category_left`>' . $cat['category_left'] . ' AND `category_right`<' . $cat['category_right'];
        // если необходим вывод всех подкатегорий
        if (!$this->subCategory)
            $sql .= ' AND `category_level`=' . ($cat['category_level'] + 1);
        $sql .= ' %order %limit';

        return $sql;
    }

    /**
     * Возращает элемент списка
     * 
     * @param  array $record запись
     * @param  integer $index порядковый номер
     * @return void
     */
    public function getItem($record, $index)
    {
        // если ЧПУ
        if ($this->sef)
            $url = $this->ln . $record['category_uri'];
        else
            $url = '/?c=' . $record['category_id'];

        return array(
            'id'     => $record['category_id'],
            'title'  => $record['category_name'],
            'uri'    => $record['category_uri'],
            'ln'     => $this->ln,
            'url'   => $url
        );
    }
}
?>