<?php
/**
 * Gear CMS
 *
 * Модель данных "Список статей"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Articles.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CmList
 */
Gear::library('/Models/List');

/**
 * Список статей
 * 
 * @category   Gear
 * @package    Models
 * @subpackage List
 * @copyright  Copyright (c) 2013-2016 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Articles.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmListArticles extends CmList
{
    /**
     * Идентификатор вида статьи
     *
     * @var integer
     */
    public $typeId;

    /**
     * Идентификатор категории статьи
     *
     * @var integer
     */
    public $categoryId;

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // ексли указан идентификатор категории статьи
        if (empty(Gear::$app->category['category_id']))
            $categoryId = 0;
        else
            $categoryId = Gear::$app->category['category_id'];
        $this->categoryId = $this->get('list-category-id', $categoryId);
        // вид статьи
        $this->typeId = $this->get('list-article-type', $this->typeId);
    }

    /**
     * Возращает часть SQL запроса (сортировка)
     *
     * @return string
     */
    protected function getOrder()
    {
        $str = parent::getOrder();
        if (empty($str))
            $str = 'ORDER BY `a`.`article_id` desc ';
        else
            $str .= ',`a`.`article_id` desc ';

        return $str;
    }

    /**
     * Конструктор запроса
     * 
     * @return string
     */
    protected function query()
    {
        $sql = 
           'SELECT SQL_CALC_FOUND_ROWS `a`.*, `p`.*, `ac`.`category_uri` '
         . 'FROM `site_articles` `a` JOIN `site_pages` `p` ON `a`.`article_id`=`p`.`article_id` AND `a`.`published`=1 AND `p`.`language_id`=%lang '
         . 'JOIN `site_categories` `ac` USING(`category_id`) '
         . 'WHERE `a`.`published_feed`=1 AND `a`.`domain_id`=' . Gear::$app->domainId . ' ';
         // если есть фильтарция списка через календарь
         if ($this->dateOn)
            $sql .= " AND `a`.`published_date`='" . $this->dateOn['date'] . "' ";
        // если необходимы вывод статей в подкатегориях
        if ($this->subCategory) {
            $cat = Gear::$app->category;
            $sql .= 'AND `ac`.`category_left`>=' . $cat['category_left'] . ' AND `ac`.`category_right`<=' . $cat['category_right'];
        } else
            $sql .= 'AND `a`.`category_id`=' . $this->categoryId;
        // если по виду
        if ($this->typeId)
            $sql .= ' AND `type_id`=' . $this->typeId;
        $sql .= ' %order %limit';

        return $sql;
    }

    /**
     * Возращает элемент списка
     * 
     * @param  array $record запись
     * @param  integer $index порядковый номер
     * @return void
     */
    public function getItem($record, $index)
    {
        $item = parent::getItem($record, $index);

        $item['category-uri'] = $record['category_uri'];
        // если ЧПУ работает
        if ($this->sef) {
            // если новость
            if ($record['type_id'] == 2)
                $record['article_uri'] = $record['article_id'] . '-' . $record['article_uri'];

            $item['url'] = $this->ln . ($record['category_uri'] != '/' ? $record['category_uri'] : '') . $record['article_uri'];
        } else
            $item['url'] = '/?a=' . $record['article_id'];

        return $item;
    }
}


/**
 * Список статей (AJAX)
 * 
 * @category   Gear
 * @package    Models
 * @subpackage List
 * @copyright  Copyright (c) 2013-2016 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Articles.php 2013-07-01 12:00:00 Gear Magic $
 */
class CmjListArticles extends CmjList
{
    /**
     * Идентификатор вида статьи
     *
     * @var integer
     */
    protected $_typeId;

    /**
     * Идентификатор категории статьи
     *
     * @var integer
     */
    protected $_categoryId;

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // если указана категория статей
        $this->_categoryId = Gear::$app->input->get('c', $this->_categoryId);
        // вид статьи
        $this->_typeId =  Gear::$app->input->get('at', $this->_typeId);
    }

    /**
     * Конструктор запроса
     * 
     * @return string
     */
    protected function query()
    {
        $sql = 
           'SELECT SQL_CALC_FOUND_ROWS `a`.*, `p`.*, `ac`.`category_uri` '
         . 'FROM `site_articles` `a` JOIN `site_pages` `p` ON `a`.`article_id`=`p`.`article_id` AND `a`.`published`=1 AND `p`.`language_id`=%lang '
         . 'JOIN `site_categories` `ac` USING(`category_id`) '
         . 'WHERE `a`.`published_feed`=1 AND `a`.`domain_id`=' . Gear::$app->domainId;
         // если есть фильтарция списка через календарь
         if ($this->dateOn)
            $sql .= " AND `a`.`published_date`='" . $this->dateOn['date'] . "' ";
        // если необходимы вывод статей в подкатегориях
        if ($this->subCategory) {
            $cat = Gear::$app->category;
            $sql .= 'AND `ac`.`category_left`>=' . $cat['category_left'] . ' AND `ac`.`category_right`<=' . $cat['category_right'];
        } else
            $sql .= 'AND `a`.`category_id`=' . $this->categoryId;
        // если по виду
        if ($this->typeId)
            $sql .= ' AND `type_id`=' . $this->typeId;
        $sql .= ' %order %limit';

        return $sql;
    }
}
?>