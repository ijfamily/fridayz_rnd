<?php
/**
 * Gear CMS
 *
 * Модель данных "Список статей по выбранному тегу"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Tag.php 2016-01-01 21:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CmList
 */
Gear::library('/Models/List');

/**
 * Список статей по выбранному тегу
 * 
 * @category   Gear
 * @package    Models
 * @subpackage List
 * @copyright  Copyright (c) 2013-2016 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Tag.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmListTag extends CmList
{
   /**
     * Поиск по тегу
     * 
     * @var string
     */
    public $tag = '';

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        $this->tag = urldecode(mysql_real_escape_string($this->url->getSeg(1)));
    }

    /**
     * Конструктор запроса
     * 
     * @return string
     */
    protected function query()
    {
        if (empty($this->tag)) return '';

        return
            'SELECT SQL_CALC_FOUND_ROWS `a`.*, `p`.*, `c`.`category_uri` FROM `site_articles` `a` '
          . 'JOIN `site_categories` `c` ON `c`.`category_id`=`a`.`category_id` AND `c`.`category_visible`=1 '
          . 'JOIN `site_pages` `p` ON `p`.`language_id`=%lang AND `p`.`article_id`=`a`.`article_id`'
          . 'WHERE `article_search`=1 AND `a`.`published`=1 AND MATCH (`page_tags`) AGAINST (\'*' . $this->tag . '*\' in boolean mode) ORDER BY `a`.`published_date` DESC %limit ';
    }

    /**
     * Возращает элемент списка
     * 
     * @param  array $record запись
     * @param  integer $index порядковый номер
     * @return void
     */
    public function getItem($record, $index)
    {
        $item = parent::getItem($record, $index);

        $item['url'] = GArticles::genUrl($record, $this->ln, $this->sef, false);

        return $item;
    }
}
?>