<?php
/**
 * Gear CMS
 *
 * Модель данных "Список статей в архиве"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Archive.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CmList
 */
Gear::library('/Models/List/Articles');

/**
 * Список статей в архиве
 * 
 * @category   Gear
 * @package    Models
 * @subpackage List
 * @copyright  Copyright (c) 2013-2016 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Archive.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmListArchive extends CmListArticles
{
    /**
     * Идентификатор вида статьи (0 - все, 1 - статья, 2 - новость)
     *
     * @var integer
     */
    public $typeId;

    /**
     * Дата публикаций статей (например: 2016, 01/2016, 01/01/2016)
     *
     * @var string
     */
    public $date = '';

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // вид статьи
        $this->typeId = $this->get('list-article-type', $this->typeId);
        // дата публикации
        $this->date = $this->get('list-date', $this->date);
        if (empty($this->date))
            $this->date = Gear::from('ListArchive', 'date', false);
        else
            $this->date = explode('/', trim($this->url->path, '/'));
    }

    /**
     * Конструктор запроса
     * 
     * @return string
     */
    protected function query()
    {
        $sql = 
           'SELECT SQL_CALC_FOUND_ROWS `a`.*, `p`.*, `ac`.`category_uri`, `ac`.`category_name` '
         . 'FROM `site_articles` `a` JOIN `site_pages` `p` ON `a`.`article_id`=`p`.`article_id` AND `p`.`language_id`=%lang '
         . 'JOIN `site_categories` `ac` USING(`category_id`) '
         . 'WHERE `a`.`published`=1 ';
        // по категории
        if ($this->categoryId)
            $sql .= ' AND `category_id`=' . $this->categoryId;
        // если по виду
        if ($this->typeId)
            $sql .= ' AND `type_id`=' . $this->typeId;
        switch (sizeof($this->date)) {
            case 1: $sql .= " AND `a`.`published_date` BETWEEN '{$this->date[0]}-01-01' AND '{$this->date[0]}-12-31'"; break;
            case 2: $sql .= " AND `a`.`published_date` BETWEEN '{$this->date[1]}-{$this->date[0]}-01' AND '{$this->date[1]}-{$this->date[0]}-31'"; break;
            case 3: $sql .= " AND `a`.`published_date`='{$this->date[2]}-{$this->date[1]}-{$this->date[0]}'"; break;
        }
        $sql .= ' %order %limit';

        return $sql;
    }

    /**
     * Возращает элемент списка
     * 
     * @param  array $record запись
     * @param  integer $index порядковый номер
     * @return void
     */
    public function getItem($record, $index)
    {
        $item = parent::getItem($record, $index);

        // если ЧПУ работает
        if (!$this->sef)
                $item['category-uri'] = '?c=' . $record['category_id'];
        $item['category-name'] = $record['category_name'];

        return $item;
    }
}
?>