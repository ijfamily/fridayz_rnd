<?php
/**
 * Gear CMS
 *
 * Модель данных "Рейтинг материала"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Rating.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Модель данных рейтинга материала
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Rating.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmjRating extends GModel
{
    /**
     * Вид запроса
     *
     * @var array
     */
    public $types = array(
        'place' => array('table' => 'place_names', 'field' => 'place_id')
    );

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // установить соединение с сервером
        GFactory::getDb()->connect();
    }

    /**
     * Если вид существует
     *
     * @params string $name вид запроса
     * @return boolean
     */
    public function isExistType($name)
    {
        return isset($this->types[$name]);
    }

    /**
     * Форматирование строки идентификатора
     *
     * @params string $str строка с идент.
     * @return boolean
     */
    public function formatId($str)
    {
        $list = explode('-', $str);
        if (!isset($list[1]))
            return 0;
        else
            return (int) $list[1];
    }

    /**
     * Возращает рейтинг
     *
     * @params string $id идент. записи
     * @params string $type строка с идент.
     * @return boolean
     */
    public function getRating($type, $id)
    {
        $alias = $this->types[$type];

        $query = GFactory::getQuery();
        $sql = 'SELECT * FROM `' . $alias['table'] . '` WHERE `' . $alias['field'] . '`=' . $id . ' AND `rating`=1';
        
        if (($rec = $query->getRecord($sql)) === false)
            return false;
        if (empty($rec))
            return false;

        return array(
            'value' => (int) $rec['rating_value'],
            'votes' => (int) $rec['rating_votes']
        );
    }

    /**
     * Обновление рейтинга записи
     * 
     * @params string $type вид
     * @params string $data данные рейтинга
     * @params string $id идент. записи
     * @return boolean
     */
    public function updateRating($type, $data, $id)
    {
        $alias = $this->types[$type];

        $query = GFactory::getQuery();
        if ($query->update($alias['table'], $data, $alias['field'], $id) !== true)
            return false;

        return true;
    }
}
?>