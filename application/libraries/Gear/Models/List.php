<?php
/**
 * Gear CMS
 *
 * Модель данных "Список"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: List.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Базовый класс модели данных списка
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: List.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmList extends GModelItems
{
   /**
     * Псевдонимы полей используемые для сортировки списка
     * вида: array('alias' => 'field', ... )
     * 
     * @var array
     */
    protected $_fieldAlias = array(
        'date'   => 'published_date',
        'header' => 'page_header'
    );

   /**
     * Псевдоним поля (из $_fieldAlias) используемый для сортировки по умолчанию
     * 
     * @var string
     */
    protected $_field = 'name';

   /**
     * Количество страниц полученных в предыдущем запросе
     * $countPages = $countRecords / $pageLimit
     * 
     * @var integer
     */
    public $countPages = 0;

   /**
     * Количество записей на странице
     * 
     * @var integer
     */
    public $countRecords = 0;

   /**
     * Количество записей в запросе
     * 
     * @var integer
     */
    public $countItems = 0;

   /**
     * Вид сортировки "a" -> "по возрастанию" или "d" -> "по убыванию"
     * 
     * @var string
     */
    protected $order = 'a';

   /**
     * Вид сортировки "a" -> "по возрастанию" или "d" -> "по убыванию"
     * 
     * @var string
     */
    public $view = 'list';

   /**
     * Текущая страница (начинается с 1)
     * 
     * @var integer
     */
    public $pageIndex = 1;

   /**
     * Количество записей выводимых на странице
     * 
     * @var integer
     */
    protected $pageLimit = 10;

   /**
     * Учитывать подкатегории
     * 
     * @var boolean
     */
    protected $subCategory = false;

   /**
     * Допустимый диапазон вывода записей на странице
     * 
     * @var integer
     */
    protected $limitRange = array(4, 24);

   /**
     * Один из параметров в SQL запросе (LIMIT {start, limit})
     * $pageStart = ($pageIndex - 1) * $pageLimit
     * 
     * @var integer
     */
    protected $pageStart = 0;

   /**
     * Все переменные списка полученные из GET запроса
     * 
     * @var array
     */
    public $vars = array();

   /**
     * Если ошибки возникнут при обработки переменных запроса
     * 
     * @var boolean
     */
    protected $_error = false;

   /**
     * Значения переменных запроса по умолчанию
     * 
     * @var array
     */
    protected $_default = array();

   /**
     * Списку котрому адрессуются все переходы
     * 
     * @var string
     */
    protected $_target = '';

   /**
     * Параметры в href панели навигации
     * 
     * @var string
     */
    public $prefix = '';

    /**
     * Идентификатор статьи в которой находится список элементов
     * (необходим в том случаи если каждому элементу списка указывается статья)
     * 
     * @var integer
     */
    public $articleId;

   /**
     * Первый элемент списка
     * 
     * @var array
     */
    public $firstItem = array();

   /**
     * Последний элемент списка
     * 
     * @var array
     */
    public $lastItem = array();

   /**
     * Последний идент. элемента списка
     * 
     * @var integer
     */
    public $lastId = 0;

   /**
     * Последний идент. элемента списка
     * 
     * @var integer
     */
    public $recordFrom = 0;

   /**
     * Последний идент. элемента списка
     * 
     * @var integer
     */
    public $recordTo = 0;

   /**
     * Параметр (date-on="2016-01-01") фильтрации списка через календарь
     * 
     * @var mixed
     */
    public $dateOn = false;

    /**
     * Вывод коротких тегов в статье
     *
     * @var boolean
     */
    public $_useShortTags = false;

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // если указана страница
        $this->pageIndex = $this->get('list-page', $this->pageIndex);
        // если указано количесто записей в списке
        $this->pageLimit = $this->get('list-limit', $this->pageLimit);
        // если указан вид сортировки
        $this->_field = $this->get('list-field', $this->_field);
        // если указан псевдоним поля сортировки
        $this->order = $this->get('list-order', $this->order);
        // ексли указан идентификатор статьи
        $this->articleId = $this->get('article-id', $this->articleId);
        // учитывать подкатегорию
        $this->subCategory = $this->get('list-subcategory', $this->subCategory);
        // параметр фильтрации списка через календарь
        if (Gear::$app->config->get('CALENDAR')) {
            if ($dateOn = $this->uri->getVar('date-on')) {
                $this->dateOn = array(
                    'timestamp' => strtotime($dateOn),
                    'date'  => date('Y-m-d', strtotime($dateOn)),
                    'day'   => date('d', strtotime($dateOn)),
                    'month' => date('m', strtotime($dateOn)),
                    'year'  => date('Y', strtotime($dateOn))
                );
            }
        }
        // сборка переменных
        $this->variables();
        // использование коротких тегов
        $this->_useShortTags = Gear::$app->config->get('ARTICLE/TAGS');
    }

    /**
     * Сборка переменных
     * 
     * @return void
     */
    protected function variables()
    {
        // определение значений по умолчанию
        $this->_default = array(
            'page' => $this->pageIndex, 'o' => $this->order, 'f' => $this->_field, 'l' => $this->pageLimit, 't' => $this->view
        );

        $target = false;
        $uri = GFactory::getURL();
        // если в классе выставлено что список должен принимать параметры которые указанные для $_target
        if ($this->_target)
            $target = $this->_target == $uri->getVar('tr');
        else
            $target = !$uri->hasVar('tr');

        // текущая страница
        if ($target)
            $this->pageIndex = (int)$uri->getVar('page', $this->pageIndex);
        if ($this->pageIndex <= 0)
            $this->pageIndex = 1;
        $this->vars['page'] = $this->pageIndex;
        // количество записей на странице
        if ($target)
            $limit = (int)$uri->getVar('l', $this->pageLimit);
        else
            $limit = $this->pageLimit;
        if ($limit >= $this->limitRange[0] && $limit <= $this->limitRange[1])
            $this->pageLimit = $limit;
        $this->vars['l'] = $this->pageLimit;
        // вид сортировки
        if ($target)
            $this->order = $uri->getVar('o', $this->order);
        if ($this->order != 'a' && $this->order != 'd')
            $this->order = 'a';
        $this->vars['o'] = $this->order;
        // сортируемое поле
        if ($target)
            $field = $uri->getVar('f', $this->_field);
        else
            $field = $this->_field;
        if (isset($this->_fieldAlias[$field]))
            $this->_field = $field;
        $this->vars['f'] = $this->_field;
        // тип вывода записей в виде списка или плиток
        if ($target)
            $this->view = $uri->getVar('t', $this->view);
        $this->vars['t'] = $this->view;

        // чтобы переменные не повторялись в запросе, их необходимо убрать
        Gear::$app->url->remove(array('page', 'o', 'f', 'l', 't'));
    }

    /**
     * Возращает часть SQL запроса (количество записей)
     *
     * @return string
     */
    protected function getLimit()
    {
        if ($this->pageLimit == 0)
            return '';

        $this->pageStart = ($this->pageIndex - 1) * $this->pageLimit;

        return 'LIMIT ' . $this->pageStart . ', ' . $this->pageLimit;
    }

    /**
     * Возращает вид списка
     *
     * @return string
     */
    public function getType()
    {
        return $this->view;
    }

    /**
     * Возращает часть SQL запроса (сортировка)
     *
     * @return string
     */
    protected function getOrder()
    {
        switch ($this->order) {
            // сортировка "по возрастанию"
            case 'a':
                $order = 'ASC';
                break;

            // сортировка "по убыванию"
            case 'd':
                $order = 'DESC';
                break;

            // по умолчанию
            default:
                $order = 'ASC';
        }
        // если существует сортируемое поле
        if (isset($this->_fieldAlias[$this->_field]))
            return 'ORDER BY ' . $this->_fieldAlias[$this->_field] . ' ' . $order;

        return '';
    }

    /**
     * Возвращает SQL запрос сформированный из GET запроса пользователя 
     * 
     * @param  $sql SQL запрос
     * @return void
     */
    protected function getQuery($sql = '')
    {
        $from = array('%limit', '%order', '%lang');
        $to = array($this->getLimit(), $this->getOrder(), GFactory::getLanguage('id'));

        return str_replace($from, $to, $sql);
    }

    /**
     * Обработка списка записей после его формирования (getItems)
     * 
     * @return void
     */
    public function items()
    {}

    /**
     * Возращает элемент списка
     * 
     * @param  array $record запись
     * @param  integer $index порядковый номер
     * @return void
     */
    public function getItem($record, $index)
    {
        // если есть тип статьи
        if (isset($record['type_id']))
            // если тип не "Статья"
            if ($record['type_id'] != 1)
                $record['article_uri'] = $record['article_id'] . '-' . $record['article_uri'];

        return array(
            'id'     => $record['article_id'],
            'title'  => $record['page_header'],
            'img'    => $record['page_image'],
            'text'   => $record['page_html_short'],
            'date'   => $record['published_date'],
            'time'   => $record['published_time'],
            'uri'    => $record['article_uri'],
            'url'    => $record['article_uri'],
            'visits' => $record['article_visits'],
            'author' => $record['page_meta_author'],
            'tags'   => $this->_useShortTags ? $this->getTagsItem($record['page_tags']) : '',
            'ln'     => $this->ln,
            'category-uri'  => '',
            'announce-date' => $record['announce_date'],
            'announce-time' => $record['announce_time']
        );
    }

    /**
     * Возращает список коротких тегов
     * 
     * @param  string $str строка тегов
     * @return string
     */
    public function getTagsItem($str)
    {
        if (empty($str)) return '';

        $str = str_replace(array('#', '-', '_'), '', $str);
        $list = explode(' ', $str);
        $result = '';
        foreach ($list as $index => $tag) {
            $result .= '<a href="{chost}/tag/' . urlencode($tag) . '/">' . $tag . '</a>';
        }

        return $result;
    }

    /**
     * Возращает список элементов
     * 
     * @return array
     */
    public function getItems()
    {
        // если возникла ошибка при обработки переменных запроса
        if ($this->_error) return array();

        // конструктор запроса
        $this->_sql = $this->query();

        // обработчик SQL запросов
        if (empty($this->_sql)) return array();

        GTimer::start('list items');
        $sql = $this->getQuery($this->_sql);
        if ($this->_query->execute($sql) === false)
            throw new GException('Query error (Internal Server Error)', 'Response from the database: %s', $this->_query->getError(), __CLASS__ . '::' . __FUNCTION__);

        $items = $item = array();
        $index = 0;
        while (!$this->_query->eof()) {
            $item = $this->getItem($this->_query->next(), $index);
            $items[] = $item;
            $index++;
        }
        if (empty($items)) return array();
        // количество записей на странице
        $this->countRecords = (int) $this->_query->getFoundRows();
        // количество страниц
        if ($this->countRecords == 0)
            $this->countPages = 0;
        else {
            if ($this->pageLimit == 0)
                $this->countPages = 1;
            else
                $this->countPages = (int) ceil($this->countRecords / $this->pageLimit);
            $this->countItems = sizeof($items);
            $this->firstItem = $items[0];
            $this->lastItem = $items[sizeof($items) - 1];
            $this->recordFrom = ($this->pageIndex - 1) * $this->pageLimit + 1;
            $this->recordTo = $this->pageIndex * $this->pageLimit;
            if ($this->recordTo > $this->countRecords)
                $this->recordTo = $this->countRecords;
        }
        // текущая страница
        if ($this->pageIndex > $this->countPages)
            $this->pageIndex = $this->countPages;
        // обработка списка записей
        $this->items();
        GTimer::stop('list items');

        return $items;
    }

    /**
     * Возвращает для каждой ссылки навигации параметры
     * (должны начинаться с "&")
     *
     * @return string
     */
    protected function getPagePrefix()
    {
        if ($this->prefix)
            $pr = '&' . $this->prefix;
        else
            $pr = $this->prefix;
        if ($this->_target)
            $pr .= '&tr=' . $this->_target;

        return $pr;
    }

    /**
     * Возращает пагинацию списка
     *
     * @return string
     */
    public function getPagination()
    {
        $pagination = array();
        $vars = Gear::$app->url->getQuery();
        if ($vars)
            $vars .= '&';
        // если возникла ошибка при обработки переменных запроса
        if ($this->_error) return $pagination;
        // если нет страницы для отображения записей
        if ($this->countPages <= 1) return '';

        $prefix = $this->getPagePrefix();
        // до
        $countLeft = 3;
        if ($this->pageIndex - $countLeft - 1 > 0) {
            $pagination[] = array('type' => 'first', 'page' => 1, 'url' => '?' . $vars . 'page=1' . $prefix);
            $pagination[] = array('type' => 'prev', 'page' => ($this->pageIndex - 1), 'url' => '?' . $vars . 'page=' . ($this->pageIndex - 1) . $prefix);
            $pagination[] = array('type' => 'more', 'page' => 0, 'url' => '');
        }
        for ($i = $countLeft; $i >=1; $i--)
            if ($this->pageIndex - $i > 0) {
                $pagination[] = array('type' => 'page', 'page' => ($this->pageIndex - $i), 'url' => '?' . $vars . 'page=' . ($this->pageIndex - $i) . $prefix);
            }
        // текущая
        $pagination[] = array('type' => 'active', 'page' => $this->pageIndex, 'url' => '');
        // после
        $countRight = 3;
        for ($i = 1; $i <= $countRight; $i++)
        if ($this->pageIndex + $i <= $this->countPages) {
            $pagination[] = array('type' => 'page', 'page' => ($this->pageIndex + $i), 'url' => '?' . $vars . 'page=' . ($this->pageIndex + $i) . $prefix);
        }
        if ($this->pageIndex + $countRight + 1 <= $this->countPages) {
            $pagination[] = array('type' => 'more', 'page' => 0, 'url' => '');
            $pagination[] = array('type' => 'next', 'page' => ($this->pageIndex + 1), 'url' => '?' . $vars . 'page=' . ($this->pageIndex + 1) . $prefix);
            $pagination[] = array('type' => 'last', 'page' => $this->countPages, 'url' => '?' . $vars . 'page=' . $this->countPages . $prefix);
        }

        return $pagination;
    }
}


/**
 * Базовый класс списка (AJAX)
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: List.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmjList extends GModelItems
{
   /**
     * Псевдонимы полей используемые для сортировки списка
     * вида: array('alias' => 'field', ... )
     * 
     * @var array
     */
    protected $_fieldAlias = array(
        'date'   => 'published_date',
        'header' => 'page_header'
    );

   /**
     * Псевдоним поля (из $_fieldAlias) используемый для сортировки по умолчанию
     * 
     * @var string
     */
    protected $_field = 'name';

   /**
     * Количество страниц полученных в предыдущем запросе
     * $countPages = $countRecords / $pageLimit
     * 
     * @var integer
     */
    protected $countPages = 0;

   /**
     * Количество записей на странице
     * 
     * @var integer
     */
    public $countRecords = 0;

   /**
     * Количество записей в запросе
     * 
     * @var integer
     */
    public $countItems = 0;

   /**
     * Вид сортировки "a" -> "по возрастанию" или "d" -> "по убыванию"
     * 
     * @var string
     */
    protected $order = 'a';

   /**
     * Вид сортировки "a" -> "по возрастанию" или "d" -> "по убыванию"
     * 
     * @var string
     */
    public $view = 'list';

   /**
     * Текущая страница (начинается с 1)
     * 
     * @var integer
     */
    public $pageIndex = 1;

   /**
     * Количество записей выводимых на странице
     * 
     * @var integer
     */
    protected $pageLimit = 10;

   /**
     * Учитывать подкатегории
     * 
     * @var boolean
     */
    protected $subCategory = false;

   /**
     * Допустимый диапазон вывода записей на странице
     * 
     * @var integer
     */
    protected $limitRange = array(4, 24);

   /**
     * Один из параметров в SQL запросе (LIMIT {start, limit})
     * $pageStart = ($pageIndex - 1) * $pageLimit
     * 
     * @var integer
     */
    protected $pageStart = 0;

   /**
     * Все переменные списка полученные из GET запроса
     * 
     * @var array
     */
    public $vars = array();

   /**
     * Если ошибки возникнут при обработки переменных запроса
     * 
     * @var boolean
     */
    protected $_error = false;

   /**
     * Значения переменных запроса по умолчанию
     * 
     * @var array
     */
    protected $_default = array();

   /**
     * Первый элемент списка
     * 
     * @var array
     */
    public $firstItem = array();

   /**
     * Последний элемент списка
     * 
     * @var array
     */
    public $lastItem = array();

   /**
     * Последний идент. элемента списка
     * 
     * @var integer
     */
    public $lastId = 0;

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // сборка переменных
        $this->variables();
    }

    /**
     * Сборка переменных
     * 
     * @return void
     */
    protected function variables()
    {
        $input = GFactory::getApplication()->input;

        // определение значений по умолчанию
        $this->_default = array(
            'p' => $this->pageIndex, 'o' => $this->order, 'f' => $this->_field, 'l' => $this->pageLimit, 't' => $this->view
        );
        // текущая страница
        $this->pageIndex = (int) $input->get('p', $this->pageIndex);
        if ($this->pageIndex <= 0)
            $this->pageIndex = 1;
        $this->vars['p'] = $this->pageIndex;
        // количество записей на странице
        $limit = (int) $input->get('l', $this->pageLimit);
        if ($limit >= $this->limitRange[0] && $limit <= $this->limitRange[1])
            $this->pageLimit = $limit;
        $this->vars['l'] = $this->pageLimit;
        // вид сортировки
        $this->order = $input->get('o', $this->order);
        if ($this->order != 'a' && $this->order != 'd')
            $this->order = 'a';
        $this->vars['o'] = $this->order;
        // сортируемое поле
        $field = $input->get('f', $this->_field);
        if (isset($this->_fieldAlias[$field]))
            $this->_field = $field;
        $this->vars['f'] = $this->_field;
        // тип вывода записей в виде списка или плиток
        $this->view = $input->get('t', $this->view);
        $this->vars['t'] = $this->view;
        // учитывать подкатегорию
        $this->subCategory = $input->get('sc', $this->view);
        $this->vars['sc'] = $this->subCategory;
    }

    /**
     * Возращает часть SQL запроса (количество записей)
     *
     * @return string
     */
    protected function getLimit()
    {
        $this->pageStart = ($this->pageIndex - 1) * $this->pageLimit;

        return 'LIMIT ' . $this->pageStart . ', ' . $this->pageLimit;
    }

    /**
     * Возращает вид списка
     *
     * @return string
     */
    public function getType()
    {
        return $this->view;
    }

    /**
     * Возращает часть SQL запроса (сортировка)
     *
     * @return string
     */
    protected function getOrder()
    {
        switch ($this->order) {
            // сортировка "по возрастанию"
            case 'a':
                $order = 'ASC';
                break;

            // сортировка "по убыванию"
            case 'd':
                $order = 'DESC';
                break;

            // по умолчанию
            default:
                $order = 'ASC';
        }
        // если существует сортируемое поле
        if (isset($this->_fieldAlias[$this->_field]))
            return 'ORDER BY ' . $this->_fieldAlias[$this->_field] . ' ' . $order;

        return '';
    }

    /**
     * Возвращает SQL запрос сформированный из GET запроса пользователя 
     * 
     * @param  $sql SQL запрос
     * @return void
     */
    protected function getQuery($sql = '')
    {
        $from = array('%limit', '%order', '%lang');
        $to = array($this->getLimit(), $this->getOrder(), GFactory::getLanguage('id'));

        return str_replace($from, $to, $sql);
    }

    /**
     * Обработка списка записей после его формирования (getItems)
     * 
     * @return void
     */
    public function items()
    {}

    /**
     * Возращает элемент списка
     * 
     * @param  array $record запись
     * @param  integer $index порядковый номер
     * @return void
     */
    public function getItem($record, $index)
    {
        return array(
            'id'     => $record['article_id'],
            'title'  => $record['page_header'],
            'img'    => $record['page_image'],
            'text'   => $record['page_html_short'],
            'date'   => $record['published_date'],
            'time'   => $record['published_time'],
            'uri'    => $record['article_uri'],
            'ln'     => $this->ln,
            'category-uri' => '',
        );
    }

    /**
     * Возращает список элементов
     * 
     * @return array
     */
    public function getItems()
    {
        // если возникла ошибка при обработки переменных запроса
        if ($this->_error) return array();

        // конструктор запроса
        $this->_sql = $this->query();

        // обработчик SQL запросов
        if (empty($this->_sql)) return array();

        $sql = $this->getQuery($this->_sql);
        if ($this->_query->execute($sql) === false)
            throw new GException('Query error (Internal Server Error)', 'Response from the database: %s', $this->_query->getError(), __CLASS__ . '::' . __FUNCTION__);

        $items = array();
        $index = 0;
        while (!$this->_query->eof()) {
            $items[] = $this->getItem($this->_query->next(), $index);
            $index++;
        }
        if (empty($items)) return array();
        // количество записей на странице
        $this->countRecords = $this->_query->getFoundRows();
        // количество страниц
        if ($this->countRecords == 0)
            $this->countPages = 0;
        else {
            $this->countPages = ceil($this->countRecords / $this->pageLimit);
            $this->countItems = sizeof($items);
            $this->firstItem = $items[0];
            $this->lastItem = $items[sizeof($items) - 1];
        }
        // текущая страница
        if ($this->pageIndex > $this->countPages)
            $this->pageIndex = $this->countPages;
        // обработка списка записей
        $this->items();

        return $items;
    }
}
?>