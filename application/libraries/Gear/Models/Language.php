<?php
/**
 * Gear CMS
 *
 * Модель данных "Языки сайта"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Language.php 2016-01-01 21:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Модель данных вывода языка сайта
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Language.php 2016-01-01 21:00:00 Gear Magic $
 */
class CmLanguage extends GModel
{
    /**
     * Возаращет список элементов
     *
     * @return array
     */
    public function getItems()
    {
        
        // доступные языки
        $langs = GFactory::getConfig()->getParams('LANGUAGES');
        // выбранный язык
        $alias = GFactory::getLanguage('prefix');
        unset($langs[$alias]);

        return $langs;
    }
}
?>