<?php
/**
 * Gear CMS
 *
 * Модель данных "Меню сайта"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Menu.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Базовый класс модели данных меню
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Menu.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmMenu extends GModel
{
    /**
     * Возращает список пунктов меню
     *
     * @return array
     */
    public function getItems()
    {
        $query = new GDbQuery();
        $sql = 
            'SELECT * FROM `site_menu_items` `i` JOIN `site_menu_items_l` `l` ON `l`.`item_id`=`i`.`item_id` AND `l`.`language_id`=' . GFactory::getLanguage('id')
          . ' WHERE `i`.`item_visible`=1 AND `i`.`menu_id`=' . $this->dataId . ' ORDER BY `i`.`item_index` ASC';
        if ($query->execute($sql) === false)
            throw new GException('Query error (Internal Server Error)', 'Response from the database: %s', $query->getError(), __CLASS__);

        // для сайтов на других доменах
        $domains = Gear::$app->config->getFrom('DOMAINS', 'indexes');
        $chost = Gear::$app->config->get('HOST') . '/';

        $nodes = $cur = array();
        while (!$query->eof()) {
            $item = $query->next();

            // для сайтов на других доменах
            if ($domains) {
                if (isset($domains[$item['domain_id']]))
                    $host = $domains[$item['domain_id']][0] . '/';
                else
                    $host = $chost;
            } else
                $host = $chost;

            $cur = &$nodes[$item['item_id']];
            $cur['parent-id'] = $item['item_parent_id'];
            $cur['name'] = $item['item_name'];
            $cur['active'] = false;
            if ($this->sef)
                $cur['url'] = $host . $item['item_url'];
            else {
                if (empty($item['article_id']))
                    $cur['url'] = $host . $item['item_url'];
                else
                    $cur['url'] = $host . '?a=' . $item['article_id'];
            }
            /*
            if ($cur['url']) {
                if (!$url->isRoot()) {
                    if ($cur['url'] != '/')
                        $cur['active'] = strpos($url->path, $cur['url']) !== false;
                } else
                    if ($cur['url'] == '/')
                        $cur['active'] = true;
            }
            */
            $cur['id'] = $item['item_id'];
            $nodes[$item['item_parent_id']]['children'][$item['item_id']] = &$cur;
        }
        if (empty($nodes[1]['children']))
            return array();
        else
            return $nodes[1]['children'];
    }
}


/**
 * Модель данных касакадного меню
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Menu.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmMenuCascade extends GModel
{
    /**
     * Предок элементов меню меню
     *
     * @var mixed
     */
    protected $_node = array();

   /**
     * Количество уровней вложенности
     * 
     * @var integer
     */
    public $level = 2;

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // если указано количество уровней вложенности
        $this->_level = $this->get('level', $this->level);
    }

    /**
     * Возращает загаловок меню
     *
     * @return void
     */
    public function getTitle()
    {
        // данные статьи
        $data = GFactory::getDocument()->getData();
        // если выбран дочерний узел
        if ($data['article_level'] > 2) {
            $query = new GDbQuery();
            // родитель
            $sql = 'SELECT `p`.`page_menu`, `a`.* '
                 . 'FROM `site_articles` `a`, `site_pages` `p` '
                 . 'WHERE `a`.`article_id`=`p`.`article_id` AND '
                 . '`p`.`language_id`=' . GFactory::getLanguage('id') . ' AND '
                 . '`a`.`article_right`-`a`.`article_left`>1 AND `a`.`article_level`=2 AND '
                 . '`a`.`article_left`<=' . $data['article_left']
                 .' ORDER BY `a`.`article_left` DESC limit 0,1';
            $this->_node = $query->getRecord($sql);
        // если выбран родитель
        } else
            $this->_node = $data;

        return $this->_node['page_menu'];
    }

    /**
     * Возращает элементы меню
     *
     * @return array
     */
    public function getItems()
    {
        // если нет родителя
        if (empty($this->_node)) return array();
        // если нет потомков
        if ($this->_node['article_right'] - $this->_node['article_left'] == 1) return array();

        $query = new GDbQuery();
        // потомки родителя
        $sql = 'SELECT `a`.`article_id` `id`, `p`.`page_menu` `name`, `ap`.`article_id` `parent_id`, '
             . '`a`.`article_index` `index`, 1 `enabled`, '
             . '`a`.`article_uri`'
             . 'FROM `site_pages` `p`, `site_articles` `a` '
             . 'LEFT JOIN `site_articles` `ap` ON `a`.`article_left`>`ap`.`article_left` AND '
             . '`a`.`article_right`<`ap`.`article_right` AND '
             . '`a`.`article_level`-1=`ap`.`article_level` '
             . 'WHERE `a`.`article_id`=`p`.`article_id` AND '
             . '`p`.`language_id`=' . GFactory::getLanguage('id') . ' AND '
             . '`a`.`article_left`>' . $this->_node['article_left'] . ' AND '
             . '`a`.`article_right`<' . $this->_node['article_right'] . ' AND `a`.`article_level`<=' 
             . ($this->level + 2)
             . ' ORDER BY `a`.`article_index`';
        if ($query->execute($sql) === false)
            throw new GException('Query error (Internal Server Error)', 'Response from the database: %s', $query->getError(), __CLASS__);

        $levels = $tree = $cur = array();
        $ln = GFactory::getLanguage('prefixUri');
        while (!$query->eof()) {
            $record = $query->next();
            $cur = &$levels[$record['id']];
            $cur['enabled'] = $record['enabled'];
            $cur['parent_id'] = $record['parent_id'];
            $cur['name'] = $record['name'];
            $cur['index'] = $record['index'];
            $cur['url'] = '/#';
            if ($record['enabled']) {
                $url = '';
                $cur['url'] = $ln . $record['article_uri'];
            }
            $cur['id'] = $record['id'];
            if($record['parent_id'] == $this->_node['article_id'])
                $tree[$record['id']] = &$cur;
            else
                $levels[$record['parent_id']]['children'][$record['id']] = &$cur;
        }

        return $tree;
    }
}
?>