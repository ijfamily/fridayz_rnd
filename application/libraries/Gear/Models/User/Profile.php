<?php
/**
 * Gear CMS
 *
 * Модель данных "Профиль пользователя"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Profile.php 2016-01-01 21:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CmForm
 */
Gear::library('/Models/Form');

/**
 * Модель данных профиля пользователя
 * 
 * @category   Gear
 * @package    Models
 * @subpackage User
 * @copyright  Copyright (c) 2014-2015 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Profile.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmUserProfile extends CmForm
{
    /**
     * Поля формы (вида "{alias1} => array('field' => {field1}, ...), ....")
     *
     * @var array
     */
    protected $_fields = array(
        // имя пользователя
        'fname' => array(
            'use'        => true,
            'field'      => 'profile_first_name',
            'default'    => '',
            'strip'      => true,
            'maxLength'  => 50,
            'minLength'  => 3,
            'title'      => 'User first name',
            'allowBlank' => false,
            'check'      => true
        ),
        // фамилия пользователя
        'lname' => array(
            'use'        => true,
            'field'      => 'profile_last_name',
            'default'    => '',
            'strip'      => true,
            'maxLength'  => 50,
            'minLength'  => 3,
            'title'      => 'User last name',
            'allowBlank' => false,
            'check'      => true
        ),
        // адрес email
        'email' => array(
            'use'        => true,
            'field'      => 'contact_email',
            'default'    => '',
            'match'      => '/^([0-9a-z]([-_.]?[0-9a-z])*@[0-9a-z]([-.]?[0-9a-z])*\\.[a-wyz][a-z](fo|g|l|m|mes|o|op|pa|ro|seum|t|u|v|z)?)$/',
            'title'      => 'E-mail',
            'allowBlank' => false,
            'check'      => true
        ),
        // пароль
        'password' => array(
            'resetable'  => true,
            'use'        => true,
            'field'      => 'user_password',
            'default'    => '',
            'strip'      => false,
            'minLength'  => 6,
            'maxLength'  => 32,
            'title'      => 'Password',
            'allowBlank' => false,
            'check'      => true
        ),
        // подтв. пароля
        'password-cnf' => array(
            'resetable'  => true,
            'record'     => false,
            'use'        => true,
            'field'      => 'password-cnf',
            'default'    => '',
            'strip'      => false,
            'minLength'  => 6,
            'maxLength'  => 32,
            'title'      => 'Password confirm',
            'allowBlank' => false
        )
    );

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        // пользователь
        $this->auth = GFactory::getAuth();

        parent::__construct($attr);
    }

    /**
     * Возращает поля с их значениями
     * 
     * @param  $datac ассоц-й массив вида "{alias1}' => '', ..."
     * @return array
     */
    public function getFields($fields = array())
    {
        $fields = parent::getFields($fields);

        if ($this->auth->check()) {
            $data = $this->auth->getUser();
            $fields['fname'] = $data['profile_first_name'];
            $fields['lname'] = $data['profile_last_name'];
            $fields['email'] = $data['contact_email'];
        }

        return $fields;
    }

    /**
     * Валидация полей формы
     * 
     * @return boolean
     */
    protected function isValidFields()
    {
        if (!parent::isValidFields()) return false;

        // проверка значения поля "пароль"
        if ($this->input->get('password') != $this->input->get('password-cnf')) {
            GMessage::to('form', 'error', 'Password confirmation does not match', $this->path);
            return false;
        }
        $this->input->set('password-cnf', null);
        // код капчи
        $this->input->set('code', null);

        return true;
    }

    /**
     * Обработка данных формы
     * 
     * @return void
     */
    protected function submit()
    {
        Gear::library('/User');

        // данные пользователя
        $userId = $this->auth->getId();
        // проверка существования пользователя
        if ($user = GUser::getBy($this->input->get('email'), 'e-mail')) {
            // чтобы мыло не совпало с другим пользователем
            if ($user['user_id'] != $userId) {
                GMessage::to('form', 'error', 'A user with this e-mail "%s" already exists', $this->path, array($this->input->get('email')));
                GUserLog::add(array(
                    'log_action'  => 'update',
                    'log_text'    => 'A user with this e-mail "' . $user['contact_email'] . '" already exists',
                    'log_email'   => $user['contact_email'],
                    'user_id'     => $user['user_id'],
                    'log_success' => 0
                ));
                return;
            }
        }
        // обновить аккаунт
        $data = $this->getData();
        $data['user_password'] = md5($data['user_password']);
        if (($dataMail = GUser::update($data, $userId)) === false) {
            GMessage::to('form', 'error', 'Error processing forms', $this->path);
            GUserLog::add(array(
                'log_action'  => 'update',
                'log_text'    => 'Can`t update password',
                'log_email'   => $user['contact_email'],
                'user_id'     => $user['user_id'],
                'log_success' => 0
            ));
            return;
        }
        // обновить сессию
        if (!($user = GUser::get($userId))) {
            GMessage::to('form', 'error', 'Error processing forms', $this->path);
            GUserLog::add(array(
                'log_action'  => 'update',
                'log_text'    => 'Can`t get user data',
                'user_id'     => $userId,
                'log_success' => 0
            ));
            return;
        }
        $this->auth->setUser($user);
        // добавление в журнал
        GUserLog::add(array(
            'user_id'     => $userId,
            'log_action'  => 'update',
            'log_text'    => 'Success',
            'log_email'   => $this->auth->getEmail(),
            'log_network' => $this->auth->getNetworkName()
        ));
        GMessage::to('form', 'success', 'You have successfully been registered', $this->path);
    }
}
?>