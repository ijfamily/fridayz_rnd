<?php
/**
 * Gear CMS
 *
 * Модель данных "Баннера"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Banners.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Класс модели данных "Баннера"
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Banners.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmjBanners extends GModel
{
    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // установить соединение с сервером
        GFactory::getDb()->connect();
    }

    /**
     * Подсчет визитов
     * 
     * @return void
     */
    public function getItems()
    {
        $items = array();
        $query = new GDbQuery();
        $sql = 'SELECT * FROM `site_banners` WHERE `banner_visible`=1';
        if ($query->execute($sql) !== true) return array();
        while (!$query->eof()) {
            $rec = $query->next();
            $type = (int) $rec['banner_type'];
            switch ($type) {
                // фоновая реклама
                case 1:
                    $style = 'background:';
                    if ($rec['banner_bg_color'])
                        $style .= $rec['banner_bg_color'] . ' ';
                    $style .= 'url("' . $rec['banner_image'] . '") ';
                    $style .= $rec['banner_bg_repeat'] . ' ';
                    if ($rec['banner_bg_fix'])
                        $style .= 'fixed ';
                    if ($rec['banner_bg_posx'])
                        $style .= $rec['banner_bg_posx'] . ' ';
                    else
                        $style .= $rec['banner_bg_posx_type'] . ' ';
                    if ($rec['banner_bg_posy'])
                        $style .= $rec['banner_bg_posy'] . ' ';
                    else
                        $style .= $rec['banner_bg_posy_type'] . ' ';
                    $banner = array(
                        'type'  => $type,
                        'url'   => $rec['banner_url'],
                        'style' => $style
                    );
                    break;

                // баннер
                case 2:
                    $banner = array(
                        'type' => $type,
                        'html' => '<a href="' . $rec['banner_url'] . '" title="' . $rec['banner_name'] . '"><img src="' . $rec['banner_image'] . '"></a>'
                    );
                    break;

                // кода баннера
                case 3:
                    $code = strtr($rec['banner_code'], array("\n" => '', "'" => "\'"));
                    $banner = array(
                        'type' => $type,
                        'code' => $code
                    );
                    break;
            }
            $banner['exceptions'] = $rec['banners_exceptions'];

            if (isset($items[$rec['banner_selector']]))
                $items[$rec['banner_selector']]['items'][] = $banner;
            else
                $items[$rec['banner_selector']] = array('items' => array($banner));
        }

        return $items;
    }
}
?>