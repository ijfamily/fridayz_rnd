<?php
/**
 * Gear CMS
 *
 * Модель данных "Список видеозаписей"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Partners
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: List.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CmList
 */
Gear::library('/Models/List');

/**
 * Список видеозаписей
 * 
 * @category   Gear
 * @package    Partners
 * @subpackage List
 * @copyright  Copyright (c) 2013-2016 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: List.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmVideoList extends CmList
{
    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // ексли указан идентификатор категории статьи
        if (empty(Gear::$app->category['category_id']))
            $categoryId = 0;
        else
            $categoryId = Gear::$app->category['category_id'];
        $this->categoryId = $this->get('list-category-id', $categoryId);
    }

    /**
     * Конструктор запроса
     * 
     * @return string
     */
    protected function query()
    {
        $sql = 'SELECT SQL_CALC_FOUND_ROWS * FROM `site_video` WHERE `published`=1 ';
         // если есть фильтарция списка через календарь
         if ($this->dateOn)
            $sql .= " AND `published_date`='" . $this->dateOn['date'] . "' ";
        // если необходимы вывод статей в подкатегориях
        if ($this->categoryId)
            $sql .= ' AND `category_id`=' . $this->categoryId;
        $sql .= ' ORDER BY `published_date` DESC %limit';

        return $sql;
    }

    /**
     * Возращает элемент списка
     * 
     * @param  array $record запись
     * @param  integer $index порядковый номер
     * @return void
     */
    public function getItem($record, $index)
    {
        return array(
            'id'        => $record['video_id'],
            'type'      => $record['video_type'],
            'code'      => $record['video_code'],
            'title'     => $record['video_title'],
            'text'      => $record['video_text'],
            'uploaded'  => $record['video_uploaded'],
            'thumbnail' => $record['video_thumbnail'],
            'duration'  => $record['video_duration'],
            'width'     => $record['video_width'],
            'height'    => $record['video_height'],
            'tags'      => $record['video_tags'],
            'script'    => $record['video_script'],
            'frame'     => $record['video_frame'],
            'ln'        => $this->ln
        );
    }
}
?>