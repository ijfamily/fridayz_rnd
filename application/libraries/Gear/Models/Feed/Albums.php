<?php
/**
 * Gear CMS
 *
 * Модель данных "Лента фотоальбомов"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Feed.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Модель ленты фотоальбомов
 * 
 * @category   Gear
 * @package    Models
 * @subpackage Feed
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Feed.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmFeedAlbums extends GModelItems
{
   /**
     * Количество записей в списке
     * 
     * @var integer
     */
    public $limit = 0;

   /**
     * Сортируемое поле (date)
     * 
     * @var string
     */
    public $sort = 'date';

   /**
     * Сортируемое поле (date)
     * 
     * @var string
     */
    protected $_sortFields = array('date' => 'published_date');

   /**
     * Вид сортировки (asc, desc)
     * 
     * @var string
     */
    public $sortType = 'desc';

    /**
     * Идентификатор категории статьи
     *
     * @var integer
     */
    public $categoryId = 0;

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // идент. категории статей (1,2,3, ...)
        $this->categoryId = (int) $this->get('category-id', $this->categoryId);
        // количество записей в списке
        $this->limit = abs((int) $this->get('limit', $this->limit));
        if (empty($this->limit) || $this->limit > 100)
            $this->limit = 10;
        // сортируемое поле (date)
        $sort = $this->get('sort', $this->sort);
        if (isset($this->_sortFields[$sort]))
            $this->sort = $this->_sortFields[$sort];
        else
            $this->sort = $this->_sortFields[$this->sort];
        // вид сортировки (asc, desc)
        $this->sortType = $this->get('sort-type', $this->sortType);
        if ($this->sortType != 'asc' && $this->sortType != 'desc')
            $this->sortType = 'desc';
    }

    /**
     * Возращает часть SQL запроса (количество записей)
     *
     * @return string
     */
    protected function getLimit()
    {
        if ($this->limit)
            return ' LIMIT 0,' . $this->limit;
        else
            return '';
    }

    /**
     * Возращает часть SQL запроса (сортировка)
     *
     * @return string
     */
    protected function getOrder()
    {
        return ' ORDER BY `' . $this->sort . '` ' . $this->sortType;
    }

    /**
     * Возвращает SQL запрос сформированный из GET запроса пользователя 
     * 
     * @return void
     */
    protected function getQuery()
    {
        $sql = 'SELECT `g`.*, `i`.`image_entire_filename` `cover` FROM `site_albums` `g` '
             . 'LEFT JOIN `site_album_images` `i` ON `i`.`album_id`=`g`.`album_id` AND `i`.`image_cover`=1 '
             . 'WHERE `published`=1';
        if ($this->categoryId)
            $sql .= ' AND `category_id`=' . $this->categoryId;

        return $sql;
    }

    /**
     * Возращает элементы ленты
     *
     * @return void
     */
    public function getItems()
    {
        // обработчик SQL запросов
        $query = new GDbQuery();
        $sql = $this->getQuery() . $this->getOrder() . $this->getLimit();
        if ($query->execute($sql) === false)
            throw new GException('Query error (Internal Server Error)', 'Response from the database: %s', $query->getError(), __CLASS__);

        $ln = GFactory::getLanguage('prefixUri');
        $result = array();
        while (!$query->eof()) {
            $item = $query->next();
            $result[] = array(
                'id'    => $item['album_id'],
                'date'  => $item['published_date'],
                'name'  => $item['album_name'],
                'desc'  => $item['album_description'],
                'cover' => $item['cover'],
                'folder' => $item['album_folder'],
                'ln'    => $this->ln
            );
        }

        return $result;
    }
}
?>