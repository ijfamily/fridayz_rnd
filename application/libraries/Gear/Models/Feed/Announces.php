<?php
/**
 * Gear CMS
 *
 * Модель данных "Лента аноносов, объявлений"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Feed.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CmFeed
 */
Gear::library('/Models/Feed');

/**
 * Модель данных ленты аноносов, объявлений
 * 
 * @category   Gear
 * @package    Models
 * @subpackage Feed
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Feed.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmFeedAnnounces extends CmFeed
{
   /**
     * Сортируемое поле (date)
     * 
     * @var string
     */
    protected $_sortFields = array('date' => 'announce_date');

    /**
     * Возращает часть SQL запроса (сортировка)
     *
     * @return string
     */
    protected function getOrder()
    {
        if ($this->sort == 'announce_date')
            return ' ORDER BY `announce_date` DESC, `announce_time` ' . $this->sortType;

        return ' ORDER BY `' . $this->sort . '` ' . $this->sortType;
    }
}
?>