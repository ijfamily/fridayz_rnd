<?php
/**
 * Gear CMS
 *
 * Модель данных "Лента архива статей"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Feed.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Модель данных ленты архива статей
 * 
 * @category   Gear
 * @package    Models
 * @subpackage Feed
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Feed.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmFeedArchive extends GModelItems
{
   /**
     * Количество записей в списке
     * 
     * @var integer
     */
    public $limit = 0;

   /**
     * Сортируемое поле (date)
     * 
     * @var string
     */
    public $sort = 'date';

   /**
     * Сортируемое поле (date)
     * 
     * @var string
     */
    protected $_sortFields = array('date' => 'published_date');

   /**
     * Вид сортировки (asc, desc)
     * 
     * @var string
     */
    public $sortType = 'desc';

    /**
     * Идентификатор вида статьи
     *
     * @var integer
     */
    public $typeId = 2;

    /**
     * Идентификатор категории статьи
     *
     * @var integer
     */
    public $categoryId = 0;

    /**
     * Группировать дату публикации (day, month, year)
     *
     * @var string
     */
    public $groupBy = 'month';

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // идент. категории статей (1,2,3, ...)
        $this->categoryId = (int) $this->get('category-id', $this->categoryId);
        // идент. категории статей (1,2,3, ...)
        $this->typeId = (int) $this->get('type-id', $this->typeId);
        // количество записей в списке
        $this->limit = abs((int) $this->get('limit', $this->limit));
        if (empty($this->limit) || $this->limit > 100)
            $this->limit = 10;
        // сортируемое поле (date)
        $sort = $this->get('sort', $this->sort);
        if (isset($this->_sortFields[$sort]))
            $this->sort = $this->_sortFields[$sort];
        else
            $this->sort = $this->_sortFields[$this->sort];
        // вид сортировки (asc, desc)
        $this->sortType = $this->get('sort-type', $this->sortType);
        if ($this->sortType != 'asc' && $this->sortType != 'desc')
            $this->sortType = 'desc';
        // группировать по дате публикации
        $this->groupBy = $this->get('group-by', $this->groupBy);
        if ($this->groupBy != 'day' && $this->groupBy != 'month' && $this->groupBy != 'year')
            $this->groupBy != 'month';
    }

    /**
     * Возращает часть SQL запроса (количество записей)
     *
     * @return string
     */
    protected function getLimit()
    {
        if ($this->limit)
            return ' LIMIT 0,' . $this->limit;
        else
            return '';
    }

    /**
     * Возращает часть SQL запроса (сортировка)
     *
     * @return string
     */
    protected function getOrder()
    {
        return ' ORDER BY `' . $this->sort . '` ' . $this->sortType;
    }

    /**
     * Возвращает SQL запрос сформированный из GET запроса пользователя 
     * 
     * @return void
     */
    protected function getQuery()
    {
        $sql = 'SELECT `a`.`published_date`, DATE_FORMAT(`a`.`published_date`, \'%Y\') `year`, DATE_FORMAT(`a`.`published_date`, \'%m\') `month`, DATE_FORMAT(`a`.`published_date`, \'%d\') `day`, COUNT(*) `count` '
             . 'FROM `site_articles` `a` JOIN `site_pages` `p` ON `a`.`article_id`=`p`.`article_id` AND '
             . '`a`.`published`=1 AND `p`.`language_id`=' . GFactory::getLanguage('id')
             . ' WHERE 1';
        if ($this->categoryId)
            $sql .= ' AND `category_id`=' . $this->categoryId;
        if ($this->typeId)
            $sql .= ' AND `type_id`=' . $this->typeId;
        $sql .= ' GROUP BY `' . $this->groupBy .'`';

        return $sql;
    }

    /**
     * Возращает элементы ленты
     *
     * @return void
     */
    public function getItems()
    {
        // обработчик SQL запросов
        $query = new GDbQuery();
        $sql = $this->getQuery() . $this->getOrder() . $this->getLimit();
        if ($query->execute($sql) === false)
            throw new GException('Query error (Internal Server Error)', 'Response from the database: %s', $query->getError(), __CLASS__);

        $ln = GFactory::getLanguage('prefixUri');
        $result = array();
        while (!$query->eof()) {
            $item = $query->next();
            switch ($this->groupBy) {
                case 'day': $date = $item['day'] . '/' . $item['month'] . '/' . $item['year']; break;
                case 'month': $date = $item['month'] . '/' . $item['year']; break;
                case 'year': $date = $item['year']; break;
            }
            if ($this->sef)
                $url = $this->ln . $date . '/';
            else
                $url = '/?do=archive&date=' . $date;
            $result[] = array(
                'date'  => $item['published_date'],
                'day'   => $item['day'],
                'month' => $item['month'],
                'year'  => $item['year'],
                'count' => $item['count'],
                'ln'    => $this->ln,
                'url'   => $url
            );
        }

        return $result;
    }
}
?>