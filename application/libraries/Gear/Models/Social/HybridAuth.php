<?php
/**
 * Gear CMS
 *
 * Модель данных "HybridAuth подключения к интернет-сервисам"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: HybridAuth.php 2016-01-01 21:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CmSocial
 */
Gear::library('/Models/Social');

/**
 * Модель данных HybridAuth подключения к интернет-сервисам
 * 
 * @category   Gear
 * @package    Models
 * @subpackage Social
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: HybridAuth.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmSocialHybridAuth extends CmSocial
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'hybridauth.tpl';

    /**
     * Адаптер HybridAuth подключения к интернет-сервисам
     *
     * @var object
     */
    public $hybrid;

    /**
     * Аутентификация пользователя
     *
     * @var object
     */
    public $auth;

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        Gear::vendor('/Hybrid/Hybrid');

        parent::__construct($attr);

        // адаптер HybridAuth
        $this->hybrid = new Hybrid();
        // аутентификация пользователя
        $this->auth = GFactory::getAuth();
    }

    /**
     * Инициализация адаптера
     * 
     * @return void
     */
    public function initialize()
    {}

    /**
     * Возращает шаблон данных пользователя
     * 
     * @param  array $data данные пользователя
     * @return string
     */
    protected function getTplData($data)
    {}

    /**
     * Возращает список социальных сетей
     * 
     * @return string
     */
    public function getIcons()
    {}
}
?>