<?php
/**
 * Gear CMS
 *
 * Модель данных "uLogin подключения к интернет-сервисам"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: uLogin.php 2016-01-01 21:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CmSocial
 */
Gear::library('/Models/Social');

/**
 * Модель данных uLogin подключения к интернет-сервисам
 * 
 * @category   Gear
 * @package    Models
 * @subpackage Social
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: uLogin.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmSocialuLogin extends CmSocial
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'ulogin.tpl';

    /**
     * Адаптер uLogin подключения к интернет-сервисам
     *
     * @var object
     */
    public $ulogin;

    /**
     * Аутентификация пользователя
     *
     * @var object
     */
    public $auth;

    /**
     * Аутентификация пользователя
     *
     * @var object
     */
    protected $mpath = 'social/ulogin/';

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        Gear::vendor('/uLogin/uLogin');

        parent::__construct($attr);

        // адаптер uLogin
        $this->ulogin = new uLogin();
        // аутентификация пользователя
        $this->auth = GFactory::getAuth();

        GText::addText($this->mpath);
    }

    /**
     * Инициализация адаптера
     * 
     * @return void
     */
    public function initialize()
    {
        // если есть токен
        if (!$this->ulogin->hasToken()) return;
        // данные пользователя
        if (($data = $this->ulogin->getUserData()) != true) {
            GMessage::to('form', 'error', 'Unable to retrieve user data', $this->mpath);
            return;
        }
        // шаблон данных пользователя
        $data = $this->getTplData($data);
        // проверка существования аккаунта соц.сети пользователя на сайте
        $network = GUserNetworks::getBy($data['uid'], 'network_uid');
        // если нет аккаунта пользователя в соц. сети
        if (!$network) {
            // добавление аккаунта соц. сети
            $network = array(
                'network_uid'      => $data['uid'],
                'network_name'     => $data['network'],
                'netwokr_identity' => $data['identity'],
                'network_email'    => $data['email'],
                'network_avatar'   => $data['photo']
            );
            $nid = GUserNetworks::add($network);
            // если ошибка запроса
            if ($nid == false) {
                GMessage::to('form', 'error', 'You can not add your social network account', $this->mpath);
                return;
            }
            // создание аккаунта пользователя
            $user = array(
                'profile_first_name' => $data['first_name'],
                'profile_last_name'  => $data['last_name'],
                'profile_avatar'     => $data['photo'],
                'contact_email'      => $data['email']
            );
            $uid = GUser::add($user);
            // если ошибка запроса
            if ($uid == false) {
                GMessage::to('form', 'error', 'You can not create a user account', $this->mpath);
                return;
            }
            // обновление аккаунта соц. сети
            if (GUserNetworks::update(array('user_id' => $uid), $nid) == false) {
                GMessage::to('form', 'error', 'You can not update a user account', $this->mpath);
                return;
            }
            // авторизация пользователя
            $this->auth->signIn($uid, $user);
            $this->auth->setNetwork($network);
            // добавление в журнал
            GUserLog::add(array(
                'user_id'     => $uid,
                'log_action'  => 'signin and signup',
                'log_email'   => $this->auth->getEmail(),
                'log_network' => $this->auth->getNetworkName()
            ));
            Gear::redirect('/');
        } else {
            // если ошибка запроса
            if (($user = GUser::get($network['user_id'])) == false) {
                GMessage::to('form', 'error', 'You can not access the site to your social network account', $this->mpath);
                return;
            }
            // авторизация пользователя
            $this->auth->signIn($network['user_id'], $user);
            $this->auth->setNetwork($network);
            // добавление в журнал
            GUserLog::add(array(
                'user_id'     => $network['user_id'],
                'log_action'  => 'signin',
                'log_email'   => $this->auth->getEmail(),
                'log_network' => $this->auth->getNetworkName()
            ));
            Gear::redirect('/');
        }
    }

    /**
     * Возращает шаблон данных пользователя
     * 
     * @param  array $data данные пользователя
     * @return string
     */
    protected function getTplData($data)
    {
        return array_merge(array(
            'profile'        => '',
            'verified_email' => 0,
            'first_name'     => '',
            'last_name'      => '',
            'email'          => '',
            'uid'            => '',
            'identity'       => '',
            'network'        => '',
            'photo'          => ''
        ), $data);
    }

    /**
     * Возращает список социальных сетей
     * 
     * @return string
     */
    public function getIcons()
    {
        $filename = './' . PATH_TEMPLATE_COMMON . '/' . $this->template;
        try {
            if (file_exists($filename)) {
                return Gear::content($this->template, false, true);
            } else
                throw new GException('Component error', 'Unable to load template "%s"', $filename);
        } catch(GException $e) {}
    }
}
?>