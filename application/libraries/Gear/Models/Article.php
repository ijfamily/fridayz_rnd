<?php
/**
 * Gear CMS
 *
 * Модель данных "Статьи"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Article.php 2016-01-01 21:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Модель данных главной страницы
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Article.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmArticleMain extends GModel
{
    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // установить соединение с сервером
        GFactory::getDb()->connect();
    }

    /**
     * Отправить в кэш
     * 
     * @return void
     */
    public function toCache()
    {
        $cache = GFactory::getCache();
        $cache->generator = 'index';
        $cache->note = 'component "ArticleMain"';
    }
}
?>