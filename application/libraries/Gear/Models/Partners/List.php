<?php
/**
 * Gear CMS
 *
 * Модель данных "Список альбома изображений"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Partners
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: List.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CmList
 */
Gear::library('/Models/List');

/**
 * Список фотоальбомов
 * 
 * @category   Gear
 * @package    Partners
 * @subpackage List
 * @copyright  Copyright (c) 2013-2016 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: List.php 2016-01-01 12:00:00 Gear Magic $
 */
class CmPartnersList extends CmList
{
    /**
     * Идентификатор категории статьи
     *
     * @var integer
     */
    public $categoryId = 0;

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // ексли указан идентификатор категории статьи
        if (empty(Gear::$app->category['category_id']))
            $categoryId = 0;
        else
            $categoryId = Gear::$app->category['category_id'];
        $this->categoryId = $this->get('list-category-id', $categoryId);
        $sess = GFactory::getSession();
        $sess->start();
        $this->storeLikes = $sess->get('like', false);
    }

    /**
     * Конструктор запроса
     * 
     * @return string
     */
    protected function query()
    {
        $sql = 'SELECT `p`.*, `c`.* FROM `place_names` `p` JOIN `site_categories` `c` USING(`category_id`) WHERE `p`.`published`=1 ';
         // если есть фильтарция списка через календарь
         if ($this->dateOn)
            $sql .= " AND `p`.`published_date`='" . $this->dateOn['date'] . "' ";
        // если выводить все в подкатегориях
        if (Gear::$app->category['list_subcategory']) {
            $sql .= ' AND `c`.`category_left`>=' . Gear::$app->category['category_left'] . ' AND `c`.`category_right`<=' . Gear::$app->category['category_right'];
        } else
            // если необходимы вывод статей в подкатегориях
            $sql .= ' AND `p`.`category_id`=' . $this->categoryId;
        //$sql .= ' ORDER BY `p`.`rating_value`/`p`.`rating_votes` DESC %limit';
        $sql .= ' ORDER BY `p`.`likes_votes` DESC %limit';

        return $sql;
    }

    /**
     * Возращает элемент списка
     * 
     * @param  array $record запись
     * @param  integer $index порядковый номер
     * @return void
     */
    public function getItem($record, $index)
    {
        if (!empty($record['rating_value']))
            $average = round ($record['rating_value'] / $record['rating_votes'], 2);
        else
            $average = 0;
        $state = round($average);
        $hasStore = isset($this->storeLikes['place' . $record['place_id']]);

        return array(
            'id'       => $record['place_id'],
            'name'     => $record['place_name'],
            'folder'   => $record['place_folder'],
            'image'    => $record['place_image'],
            'cover'    => $record['place_logo'],
            'cat-image' => $record['category_image'],
            'uri'      => $record['place_uri'],
            'address'  => $record['place_address'],
            'kitchen'  => $record['place_kitchen'],
            'schedule' => $record['place_schedule'],
            'extended' => $record['place_extended'],
            'phone'    => $record['place_phone'],
            'site'     => $record['place_site'],
            'delivery'      => (int) $record['place_delivery'],
            'delivery-note' => $record['place_delivery_note'],
            'date'     => $record['published_date'],
            'time'     => $record['published_time'],
            'rating'   => $record['rating'],
            'rating-average' => $average,
            'rating-state'   => round($average),
            'like'         => $record['likes'] && !$hasStore,
            'like-votes'   => $record['likes_votes'],
            'ln'       => $this->ln,
			'place_coord_ln'   => $record['place_coord_ln'],
			'place_coord_lt'       => $record['place_coord_lt'],
        );
    }
}
?>