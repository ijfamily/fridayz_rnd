<?php
/**
 * Gear CMS
 *
 * Модель данных "Слайдер партнеров"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2017 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Slider.php 2017-08-24 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Класс модели слайдера партнеров
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2017 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Slider.php 2017-08-24 12:00:00 Gear Magic $
 */
class CmPartnersSliderBottom extends GModelItems
{
    /**
     * Возращает элементы слайдера
     *
     * @return void
     */
    public function getItems()
    {
        $query = new GDbQuery();
        $sql = 'SELECT * FROM `place_names` WHERE `published`=1 ORDER BY `likes_votes` DESC LIMIT 4';
        if ($query->execute($sql) === false)
            throw new GException('Query error (Internal Server Error)', 'Response from the database: %s', $query->getError(), __CLASS__);

        $result = array();
        while (!$query->eof()) {
            $item = $query->next();
            //if (empty($item['place_logo'])) continue;
            // если адресов много
            if ($item['place_address']) {
                $items = explode(';', $item['place_address']);
                $address = $items[0];
            } else
                $address = $item['place_address'];
            // если телефоно много
            if ($item['place_phone']) {
                $items = explode(',', $item['place_phone']);
                $phone = $items[0];
            } else
                $phone = $item['place_phone'];
            //
            $site = $item['place_site'];
            if (!empty($site)) {
                if ($site[0] == '!')
                    $site = '//' . trim($site, '!');
                else
                    $site = '{host}/' . $item['place_uri'];
            } else
                $site = '{host}/' . $item['place_uri'];

            $result[] = array(
                'id'         => $item['place_id'],
                'background' => $item['slider_image'],
                'logo'       => $item['slider_logo'],
                'name'       => $item['place_name'],
                'address'    => $address,
                'phone'      => $phone,
                'uri'        => $site,
                'folder'     => $item['place_folder'],
				'cover'    => $item['place_logo']
            );
        }

        return $result;
    }
}
?>