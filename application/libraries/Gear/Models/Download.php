<?php
/**
 * Gear CMS
 *
 * Модель данных "Загрузка файла"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Download.php 2016-01-01 21:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Модель данных загрузки файлов
 * 
 * @category   Gear
 * @package    Models
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Download.php 2016-01-01 21:00:00 Gear Magic $
 */
class CmDownload extends GModel
{
    /**
     * Каталог с загруженными файлами
     *
     * @var string
     */
    public $path = 'data/docs/';

    /**
     * Выбор файла
     * 
     * @param  string $filename название файла
     * @return void
     */
    public function setFile($filename)
    {
        $this->_filename = $filename;
    }

    /**
     * Вывод файла
     *
     * @return boolean
     */
    public function viewFile()
    {
        // проверка существования файла
        $filename = $this->path . $this->_filename;
        if (file_exists($filename))
            $data = file_get_contents($filename, true);
        else
            return false;
        // установить соединение с сервером
        GFactory::getDb()->connect();
        // обработчик SQL запросов
        $query = new GDbQuery();
        // обновление счетчика
        $sql = 'UPDATE `site_documents` SET `document_download`=`document_download` + 1 '
             . 'WHERE `document_filename`=' .$query->escapeStr($this->_filename);
        $query->execute($sql);
        // определение mime
        $mimes = GFactory::getConfig()->get('MIMES');
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if (isset($mimes[$ext]))
            $mime = $mimes[$ext];
        else
            $mime = $mimes['txt'];
        // определение загаловков
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== FALSE) {
            header('Content-Type: "' . $mime . '"');
            header('Content-Disposition: attachment; filename="' . $this->_filename . '"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header("Content-Transfer-Encoding: binary");
            header('Pragma: public');
            header("Content-Length: ". sizeof($data));
        } else {
            header('Content-Type: "' . $mime . '"');
            header('Content-Disposition: attachment; filename="' . $this->_filename . '"');
            header("Content-Transfer-Encoding: binary");
            header('Expires: 0');
            header('Pragma: no-cache');
            header("Content-Length: ". strlen($data));
        }
        echo $data;

       return true;
    }
}
?>