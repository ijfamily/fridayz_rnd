<?php
/**
 * Gear CMS
 * 
 * Пакет отбработки дат
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Date.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Класс обработки дат
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Date.php 2016-01-01 12:00:00 Gear Magic $
 */
class GDate
{
    /**
     * Сылка на экземпляр класса
     *
     * @var mixed
     */
    protected static $_instance = null;

    /**
     * Часовой пояс
     *
     * @var sstring
     */
    protected $_tz = '';

    /**
     * Конструктор
     * 
     * @return void
     */
    public function __construct($timezone = '')
    {
        if (empty($this->_tz))
            try {
                $this->_tz = $timezone;
                if (date_default_timezone_set($this->_tz) === false)
                    throw new GException('Сonfiguration error', 'Can not set the time zone "%s"', $this->_tz);
            } catch(GException $e) {
                $e->renderException(array(), PATH_TEMPLATE_SYS . 'exception.php');
            }
    }

    /**
     * Возращает указатель на созданный экземпляр класса (если класс не создан, создаст его)
     * 
     * @return mixed
     */
    public static function getInstance()
    {
        if (null === self::$_instance)
            self::$_instance = new self();

        return self::$_instance;
    }

    /**
     * Возращает день недели
     * 
     * @param  integer $day день
     * @param  boolean $shortName сокращенное название
     * @param  boolean $lowercase в нижнем регистре
     * @return string
     */
    public static function getWeek($day, $shortName = true, $lowercase = false)
    {
        if ($shortName) {
            if ($lowercase) {
                switch ($day) {
                    case 0: return GText::_('sun', 'date');
                    case 1: return GText::_('mon', 'date');
                    case 2: return GText::_('tue', 'date');
                    case 3: return GText::_('wed', 'date');
                    case 4: return GText::_('thu', 'date');
                    case 5: return GText::_('fri', 'date');
                    case 6: return GText::_('sat', 'date');
                }
            } else {
                switch ($day) {
                    case 0: return GText::_('Sun', 'date');
                    case 1: return GText::_('Mon', 'date');
                    case 2: return GText::_('Tue', 'date');
                    case 3: return GText::_('Wed', 'date');
                    case 4: return GText::_('Thu', 'date');
                    case 5: return GText::_('Fri', 'date');
                    case 6: return GText::_('Sat', 'date');
                }
            }
        } else {
            if ($lowercase) {
                switch ($day) {
                    case 0: return GText::_('sunday', 'date');
                    case 1: return GText::_('monday', 'date');
                    case 2: return GText::_('tuesday', 'date');
                    case 3: return GText::_('wednesday', 'date');
                    case 4: return GText::_('thursday', 'date');
                    case 5: return GText::_('friday', 'date');
                    case 6: return GText::_('saturday', 'date');
                }
            } else {
                switch ($day) {
                    case 0: return GText::_('Sunday', 'date');
                    case 1: return GText::_('Monday', 'date');
                    case 2: return GText::_('Tuesday', 'date');
                    case 3: return GText::_('Wednesday', 'date');
                    case 4: return GText::_('Thursday', 'date');
                    case 5: return GText::_('Friday', 'date');
                    case 6: return GText::_('Saturday', 'date');
                }
            }
        }
    }

    /**
     * Возращает месяц
     * 
     * @param  integer $day день
     * @param  boolean $shortName сокращенное название
     * @param  boolean $lowercase в нижнем регистре
     * @return string
     */
    public static function getMonth($day, $shortName = true, $lowercase = false)
    {
        if ($shortName) {
            if ($lowercase) {
                switch ($day) {
                    case 1: return GText::_('Jan', 'date');
                    case 2: return GText::_('Feb', 'date');
                    case 3: return GText::_('Mar', 'date');
                    case 4: return GText::_('Apr', 'date');
                    case 5: return GText::_('May', 'date');
                    case 6: return GText::_('Jun', 'date');
                    case 7: return GText::_('Jul', 'date');
                    case 8: return GText::_('Aug', 'date');
                    case 9: return GText::_('Sep', 'date');
                    case 10: return GText::_('Oct', 'date');
                    case 11: return GText::_('Nov', 'date');
                    case 12: return GText::_('Dec', 'date');
                }
            } else {
                switch ($day) {
                    case 1: return GText::_('jan', 'date');
                    case 2: return GText::_('feb', 'date');
                    case 3: return GText::_('mar', 'date');
                    case 4: return GText::_('apr', 'date');
                    case 5: return GText::_('may', 'date');
                    case 6: return GText::_('jun', 'date');
                    case 7: return GText::_('jul', 'date');
                    case 8: return GText::_('aug', 'date');
                    case 9: return GText::_('sep', 'date');
                    case 10: return GText::_('oct', 'date');
                    case 11: return GText::_('nov', 'date');
                    case 12: return GText::_('dec', 'date');
                }
            }
        } else {
            if ($lowercase) {
                switch ($day) {
                    case 1: return GText::_('january', 'date');
                    case 2: return GText::_('february', 'date');
                    case 3: return GText::_('march', 'date');
                    case 4: return GText::_('april', 'date');
                    case 5: return GText::_('may', 'date');
                    case 6: return GText::_('june', 'date');
                    case 7: return GText::_('july', 'date');
                    case 8: return GText::_('august', 'date');
                    case 9: return GText::_('september', 'date');
                    case 10: return GText::_('october', 'date');
                    case 11: return GText::_('november', 'date');
                    case 12: return GText::_('december', 'date');
                }
            } else {
                switch ($day) {
                    case 1: return GText::_('January', 'date');
                    case 2: return GText::_('February', 'date');
                    case 3: return GText::_('March', 'date');
                    case 4: return GText::_('April', 'date');
                    case 5: return GText::_('May', 'date');
                    case 6: return GText::_('June', 'date');
                    case 7: return GText::_('July', 'date');
                    case 8: return GText::_('August', 'date');
                    case 9: return GText::_('September', 'date');
                    case 10: return GText::_('October', 'date');
                    case 11: return GText::_('November', 'date');
                    case 12: return GText::_('December', 'date');
                }
            }
        }
    }

    /**
     * Возращает информацию о дате / времени
     * 
     * @param  string $time дата и время
     * @param  string $format Формат обрабатываемой даты
     * @return mixed
     */
    public static function get($time, $format = '')
    {
        if ($format)
            return date($format, strtotime($time));
        else
            return getdate(strtotime($time));
    }

    /**
     * Возращает разницу между датами
     * 
     * @param  string $date1 дата и время
     * @param  string $date2 дата и время
     * @return array
     */
    public static function diff($date1, $date2 ='')
    {
        $d1 = new DateTime($date1);
        if ($date2)
            $d2 = new DateTime($date2);
        else
            $d2 = new DateTime();
        // если версия PHP > 5.2
        if (method_exists($d1, 'diff')) {
            $diff = $d1->diff($d2);
            $diff->w = $diff->d % 7;
            return $diff;
        } else {
            $time = $d2->getTimestamp() - $d1->getTimestamp();
            $y = $m = $w = $d = $h = $i = $s = 0;
            // если больше года
            if ($time > 31536000) {
                $mod = $time % 31536000;
                $y = ($time - $mod) / 31536000;
                $time = $mod;
            }
            // если больше месяца
            if ($time > 2592000) {
                $mod = $time % 2592000;
                $m = ($time - $mod) / 2592000;
                $time = $mod;
            }
            // если больше недели
            if ($time > 604800) {
                $mod = $time % 604800;
                $w = ($time - $mod) / 604800;
                $time = $mod;
            }
            // если больше дня
            if ($time > 86400) {
                $mod = $time % 86400;
                $d = ($time - $mod) / 86400;
                $time = $mod;
            }
            // если больше часа
            if ($time > 3600) {
                $mod = $time % 3600;
                $h = ($time - $mod) / 3600;
                $time = $mod;
            }
            // если больше минуты
            if ($time > 60) {
                $mod = $time % 60;
                $i = ($time - $mod) / 60;
                $s = $mod;
            }
            $std = new stdClass();
            $std->y = $y;
            $std->d = $d;
            $std->m = $m;
            $std->w = $w;
            $std->h = $h;
            $std->i = $i;
            $std->s = $s;

            return $std;
        }
    }

    /**
     * Возращает минуты
     * 
     * @param  integer $minute минуты от 1 до 60
     * @param  boolean $digit выводить $minute в названии
     * @return string
     */
    public static function getMMinute($minute, $digit = true)
    {
        $str = (string)$minute;
        $part = (int)$str[strlen($str) - 1];
        if ($minute == 1)
            return GText::_('just', 'date');

        if (!($minute >= 10 && $minute <= 20)) {
            if ($part == 1)
                return ($digit ? $minute . ' ' : '') . GText::_('a minute ago', 'date');

            if ($part >= 2 && $part <= 4)
                return ($digit ? $minute . ' ' : '') . GText::_('minutes ago', 'date');
        }

        return ($digit ? $minute . ' ' : '') . GText::_('a minutes ago', 'date');
    }


    /**
     * Возращает час
     * 
     * @param  integer $hour день от 1 до 24
     * @param  boolean $digit выводить $hour в названии
     * @return string
     */
    public static function getMHour($hour, $digit = true)
    {
        $str = (string)$hour;
        $part = (int)$str[strlen($str) - 1];
        if ($part == 1)
            return GText::_('an hour ago', 'date');

        if ($part >= 2 && $part <= 4)
            return ($digit ? $hour . ' ' : '') . GText::_('hour ago', 'date');

        return ($digit ? $hour . ' ' : '') .  GText::_('hours ago', 'date');
    }

    /**
     * Возращает день
     * 
     * @param  integer $day день от 1 до ...
     * @param  boolean $digit выводить $day в названии
     * @return string
     */
    public static function getMDay($day, $digit = true)
    {
        $str = (string)$day;
        $part = (int)$str[strlen($str) - 1];
        if ($day == 1)
            return  GText::_('yesterday', 'date');

       if ($part == 1)
            return ($digit ? $day . ' ' : '') .  GText::_('day ago', 'date');

        if (!($day >= 10 && $day <= 20))
            if ($part >= 2 && $part <= 4)
                return ($digit ? $day . ' ' : '') . GText::_('a day ago', 'date');

        return ($digit ? $day . ' ' : '') . GText::_('days ago', 'date');
    }

    /**
     * Возращает название недели
     * 
     * @param  integer $week неделя
     * @param  boolean $digit выводить $week в названии
     * @return string
     */
    public static function getMWeek($week, $digit = true)
    {
        if ($week == 1)
            return GText::_('week ago', 'date');

        if ($week >= 2 && $week <= 4)
            return ($digit ? $week . ' ' : '') . GText::_('weeks ago', 'date');

        return ($digit ? $week . ' ' : '') . GText::_('last week', 'date');
    }

    /**
     * Возращает название месяца
     * 
     * @param  integer $month месяц от 1 до 12
     * @param  boolean $digit выводить $month в названии
     * @return string
     */
    public static function getMMonth($month, $digit = true)
    {
        if ($month == 1)
            return GText::_('a month ago', 'date');

        if ($month >= 2 && $month <= 4)
            return ($digit ? $month . ' ' : '') . GText::_('month ago', 'date');

        return ($digit ? $month . ' ' : '') . GText::_('months ago', 'date');
    }

    /**
     * Возращает год
     * 
     * @param  integer $year год
     * @param  boolean $digit выводить $year в названии
     * @return string
     */
    public static function getMYear($year, $digit = true)
    {
        $str = (string)$year;
        $part = (int)$str[strlen($str) - 1];
        if ($year == 1)
            return GText::_('last year', 'date');

        if (!($year >= 10 && $year <= 20)) {
            if ($part == 1)
                return ($digit ? $year . ' ' : '') . GText::_('last year', 'date');

            if ($part >= 2 && $part <= 4)
                return ($digit ? $year . ' ' : '') . GText::_('years ago', 'date');
        }

        return ($digit ? $year . ' ' : '') .  GText::_('a years ago', 'date');
    }

    /**
     * Возарщает дату для поста
     * 
     * @param  string $date дата и время "Y-m-d H:i:s"
     * @return string
     */
    public static function toMComment($date)
    {
        $diff = self::diff($date);

        // если больше года
        if ($diff->y > 0)
            return self::getMYear($diff->y);
        // если больше месяца
        if ($diff->m > 0)
            return self::getMMonth($diff->m);
        // если больше недели
        if ($diff->w > 0)
            return self::getMWeek($diff->w);
        // если больше дня
        if ($diff->d > 0)
            return self::getMDay($diff->d);
        // если больше часа
        if ($diff->h > 0)
            return self::getMHour($diff->h);
        // если больше минуты
        if ($diff->i > 0)
            return self::getMMinute($diff->i);
        // несколько секунд
        if ($diff->i == 0)
            return GText::_('just', 'date');

        $d = getdate(strtotime($date));
        $now = getdate();
        // сегодня
        if ($now['mday'] == $d['mday'] && $now['mon'] == $d['mon'] && $now['year'] == $d['year'])
            return GText::_('Today', 'date') . ', ' . $d['hours'] . ':' . $d['minutes'];
        // вчера
        $yesterday = getdate(mktime(0, 0, 0 , $now['mon'], $now['mday'] - 1, $d['year']));
        if ($yesterday['mday'] == $d['mday'] && $yesterday['mon'] == $d['mon'] && $yesterday['year'] == $d['year'])
            return GText::_('Yesterday', 'date');

        return $d['mday'] . ' ' . self::getMonth($d['mon'], false, true) . ', ' . $d['hours'] . ':' . $d['minutes'];
    }

    /**
     * Возвращет дату для поста
     * 
     * @param  string $timestamp время unix
     * @return string
     */
    public static function toMPost($timestamp)
    {
        $date = getdate($timestamp);
        // now date
        $now = getdate();
        // check today
        if ($now['mday'] == $date['mday'] && $now['mon'] == $date['mon'] && $now['year'] == $date['year'])
            return GText::_('Today', 'date') . ', ' . $date['hours'] . ':' . $date['minutes'];
        // check yesterday
        $yesterday = getdate(mktime(0, 0, 0 , $now['mon'], $now['mday'] - 1, $now['year']));
        if ($yesterday['mday'] == $date['mday'] && $yesterday['mon'] == $date['mon'] && $yesterday['year'] == $date['year'])
            return GText::_('Yesterday', 'date');

        return $date['mday'] . ' ' . self::getMonth($date['mon'], false, true) . ', ' . $date['hours'] . ':' . $date['minutes'];
    }

    /**
     * Вывод формата даты
     * 
     * @param  string $format формат даты
     * @param  string $timestamp время unix
     * @return string
     */
    public static function format($format, $timestamp)
    {
        if (!is_int($timestamp))
            $timestamp = strtotime($timestamp);
        $date = GText::getSection('date');

        return strtr(date($format, $timestamp), $date['format']);
    }
}
?>