<?php
/**
 * Gear CMS
 * 
 * Пакет баннеров
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Banners.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Баннера
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Banners.php 2016-01-01 12:00:00 Gear Magic $
 */
class GBanners
{
    /**
     * Шаблоны баннеров
     *
     * @var array
     */
    protected static $bannerTemplates = array();

    /**
     * Возращает шаблон баннера
     * 
     * @return string
     */
    protected static function getBannerTemplate($filename, $replace)
    {
        if (isset(self::$bannerTemplates[$filename]))
            $template = self::$bannerTemplates[$filename];
        else
            $template = Gear::content($filename);

        if (!$template || empty($replace)) return '';

        $replace = json_decode($replace, true);
        //
        if (empty($replace['banner1_blank']))
            $replace['banner1_blank'] = '';
        else
            $replace['banner1_blank'] = 'target="_blank"';
        if (empty($replace['banner2_blank']))
            $replace['banner2_blank'] = '';
        else
            $replace['banner2_blank'] = 'target="_blank"';

        return strtr($template, $replace);
    }

    /**
     * Подсчет визитов
     * 
     * @return void
     */
    public static function getItems($byId = false)
    {
        $items = array();
        $query = GFactory::getQuery();
        $sql = 'SELECT * FROM `site_banners` WHERE `banner_visible`=1';
        if ($query->execute($sql) !== true) return array();
        while (!$query->eof()) {
            $rec = $query->next();
            $type = (int) $rec['banner_type'];
            switch ($type) {
                // фоновая реклама
                case 1:
                    $style = 'background:';
                    if ($rec['banner_bg_color'])
                        $style .= $rec['banner_bg_color'] . ' ';
                    $style .= 'url("' . $rec['banner_image'] . '") ';
                    $style .= $rec['banner_bg_repeat'] . ' ';
                    if ($rec['banner_bg_fix'])
                        $style .= 'fixed ';
                    if ($rec['banner_bg_posx'])
                        $style .= $rec['banner_bg_posx'] . ' ';
                    else
                        $style .= $rec['banner_bg_posx_type'] . ' ';
                    if ($rec['banner_bg_posy'])
                        $style .= $rec['banner_bg_posy'] . ' ';
                    else
                        $style .= $rec['banner_bg_posy_type'] . ' ';
                    $banner = array(
                        'type'  => $type,
                        'url'   => $rec['banner_url'],
                        'style' => $style
                    );
                    break;

                // баннер
                case 2:
                    $banner = array(
                        'type' => $type,
                        'html' => '<a href="' . $rec['banner_url'] . '" title="' . $rec['banner_name'] . '"><img src="' . $rec['banner_image'] . '"></a>'
                    );
                    break;

                // кода баннера
                case 3:
                    $code = strtr($rec['banner_code'], array("\n" => '', "'" => "\'"));
                    $banner = array(
                        'type' => $type,
                        'code' => $code
                    );
                    break;

                // баннера по шаблону
                case 4:
                    $banner = array(
                        'type' => 3,
                        'code' => self::getBannerTemplate($rec['banner_template'], $rec['banner_template_data'])
                    );
                    break;
            }
            $banner['exceptions'] = empty($rec['banners_exceptions']) ? '' : $rec['banners_exceptions'];

            if (isset($items[$rec['banner_selector']]))
                $items[$rec['banner_selector']]['items'][] = $banner;
            else
                $items[$rec['banner_selector']] = array('items' => array($banner));
        }

        $res = array();
        foreach ($items as $id => $data) {
            if ($byId) {
                $res[$id] = $data;
            } else {
                $data['id'] = $id;
                $res[] = $data;
            }
        }

        return $res;
    }
}
?>