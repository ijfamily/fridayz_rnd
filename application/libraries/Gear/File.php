<?php
/**
 * Gear CMS
 * 
 * Пакет обработки файлов
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: File.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * Класс отбработки файлов
 * 
 * @category   Gear
 * @package    Libraries
 * @subpackage GFile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: File.php 2016-01-01 15:00:00 Gear Magic $
 */
class GFile
{
    /**
     * Возращает описание расширения файла
     * 
     * @param  string $ext расширения файла
     * @return string
     */
    public static function extName($ext)
    {
        $files = GFactory::getHelper('Files');

        if (isset($files[$ext]))
            return $files[$ext];
        else
            return '';
    }

    /**
     * Возращает права доступа к файлу ввиде цифр
     * 
     * @param  integer $perms права доступа к файлу
     * @return integer
     */
    public static function permsDigit($perms)
    {
        $p = substr(decoct($perms), 2, 6);
        if (strlen($p) == '3')
            return '0' . $p;
        else
            return $p;
    }

    /**
     * Возращает информацию о правах доступа к файлу
     * 
     * @param  integer $perms права доступа к файлу
     * @return string
     */
    public static function permsInfo($perms)
    {
        if (($perms & 0xC000) == 0xC000) {
            // Socket
            $info = 's';
        } elseif (($perms & 0xA000) == 0xA000) {
            // Symbolic Link
            $info = 'l';
        } elseif (($perms & 0x8000) == 0x8000) {
            // Regular
            $info = '-';
        } elseif (($perms & 0x6000) == 0x6000) {
            // Block special
            $info = 'b';
        } elseif (($perms & 0x4000) == 0x4000) {
            // Directory
            $info = 'd';
        } elseif (($perms & 0x2000) == 0x2000) {
            // Character special
            $info = 'c';
        } elseif (($perms & 0x1000) == 0x1000) {
            // FIFO pipe
            $info = 'p';
        } else {
            // Unknown
            $info = 'u';
        }
        // Owner
        $info .= (($perms & 0x0100) ? 'r' : '-');
        $info .= (($perms & 0x0080) ? 'w' : '-');
        $info .= (($perms & 0x0040) ?
                    (($perms & 0x0800) ? 's' : 'x' ) :
                    (($perms & 0x0800) ? 'S' : '-'));
        
        // Group
        $info .= (($perms & 0x0020) ? 'r' : '-');
        $info .= (($perms & 0x0010) ? 'w' : '-');
        $info .= (($perms & 0x0008) ?
                    (($perms & 0x0400) ? 's' : 'x' ) :
                    (($perms & 0x0400) ? 'S' : '-'));
        
        // World
        $info .= (($perms & 0x0004) ? 'r' : '-');
        $info .= (($perms & 0x0002) ? 'w' : '-');
        $info .= (($perms & 0x0001) ?
                    (($perms & 0x0200) ? 't' : 'x' ) :
                    (($perms & 0x0200) ? 'T' : '-'));

        return $info;
    }

    /**
     * Возращает размер файла
     * 
     * @param  integer $size размер файла в байтах
     * @return string
     */
    public static function sizeToStr($size)
    {
        $kb = 1024;
        $mb = 1024 * $kb;
        $gb = 1024 * $mb;
        $tb = 1024 * $gb;
        if ($size < $kb) {
          return $size .' bytes';
        } else if ($size < $mb) {
          return round($size / $kb, 2).' Kb';
        } else if ($size < $gb) {  
          return round($size / $mb, 2).' Mb';
        } else if ($size < $tb) {
          return round($size / $gb, 2).' Gb';
        } else {
          return round($size / $tb, 2).' Tb';
        }
    }

    /**
     * Возращает размер файла
     * 
     * @param  string $filename имя файла
     * @return string
     */
    public static function size($filename)
    {
        $size = filesize($filename);
        if ($size === false) return 0;

        return self::sizeToStr($size);
    }

    /**
     * Создание файла
     * 
     * @param  string $fileName название файла (с каталогом)
     * @param  string $mode режим работы с файлом
     * @param  mixed $data данные файла
     * @return mixed
     */
    public static function create($fileName, $mode, $data)
    {
        if (!$handle = fopen($fileName, $mode))
             return sprintf(L_FILE_ERR_OPEN, $fileName);
        if (fwrite($handle, $data) === false)
            return sprintf(L_FILE_ERR_WRITE, $fileName);

        return fclose($handle);
    }

    /**
     * Удаление файла
     * 
     * @param  string $filename название файла (с каталогом)
     * @param  boolean $checkExist проверка существования файла
     * @return mixed
     */
    public static function delete($filename, $checkExist = true)
    {
        try {
            if ($checkExist) {
                // если файл не существует
                if (!file_exists($filename))
                    throw new GException('Deleting data', 'Can`t perform file deletion "%s"', $filename);
                // если файл не удаляется
                if (@unlink($filename) === false)
                    throw new GException('Deleting data', 'Can not delete file "%s"', $filename);
            } else {
                // если файл не существует
                if (!file_exists($filename))
                    return true;
                // если файл не удаляется
                if (@unlink($filename) === false)
                    throw new GException('Deleting data', 'Can not delete file "%s"', $filename);
            }
        } catch (GException $e) {}

        return true;
    }

    /**
     * Копирование файла
     * 
     * @param  string $filename название файла (с каталогом)
     * @param  boolean $checkExist проверка существования файла
     * @return mixed
     */
    public static function copy($source , $dest)
    {
        try {
            // если файл не существует
            if (copy($source, $dest) === false)
                throw new GException('Deleting data', 'Can`t perform file deletion "%s"', $source);
        } catch (GException $e) {}

        return true;
    }

    /**
     * Перенос файла
     * 
     * @param  string $source исходное название файла (с каталогом)
     * @param  string $dest новое название файла (с каталогом)
     * @return void
     */
    public static function move($source , $dest)
    {
        self::copy($source , $dest);
        self::delete($source);
    }
}


/**
 * Класс отбработки каталогов
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: File.php 2016-01-01 15:00:00 Gear Magic $
 */
class CDir
{
    /**
     * Удаление файлов в каталоге
     * 
     * @param  string $path каталог
     * @return mixed
     */
    public static function clear($path, $exception = array())
    {
        try {
            $handle = @opendir($path);
            if ($handle === false)
                throw new GException('Deleting data', 'Can`t perform file deletion "%s"', $path);
            while(false !== ($file = readdir($handle))) {
                if ($exception)
                    if (in_array($file, $exception))
                        continue;
                if($file != '.' && $file != '..')
                    unlink($path . $file);
            }
        } catch (GException $e) {}
        closedir($handle);
    }

    /**
     * Удаляет часть пути к файлу
     * 
     * @param  string название файла (с каталогом)
     * @return string
     */
    public static function trimPath($fileName)
    {
        return str_replace(array('../', './'), '', $fileName);
    }
}
?>