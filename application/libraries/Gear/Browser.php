<?php
/**
 * Detect browser names and versions of Chrome, Firefox, Internet Explorer, Opera & Safari.
 * 
 * Returns array('name' => Browser name (as written here ^),
 * 'version' => array(major version, minor subversion, release, build)).
 * 
 * 'version' is array of integers. 
 * 
 * In case of no browser detected method returns array(null, array()).
 * 
 * All non-digital version flags (a, b, beta, pre and so on) are skipped.
 * If browser version is shorter then 4 parts separated by point (.),
 * undetected parts will be set in 'version' array as null.
 * 
 * This code written with great help of http://www.useragentstring.com/ website
 * 
 * The code is actual on 2009-09-15.
 * 
 * @author Leontyev Valera (feedbee@gmail.com)
 * @copyright 2009
 * @license BSD
 * 
 * @param string $userAgent
 * @return array
 */
class GBrowser
{
    /**
     * Сылка на экземпляр класса
     *
     * @var mixed
     */
    protected static $_instance = null;

    /**
     * Название браузера
     *
     * @var string
     */
    public $name = '';

    /**
     * Полная версия браузера
     *
     * @var string
     */
    public $fullName = '';

    /**
     * Версия браузера
     *
     * @var string
     */
    public $version = '';

    /**
     * Версия ОС
     *
     * @var string
     */
    public $os = '';

    /**
     * Определение версии браузера
     * 
     * @param  string $userAgent
     * @return array
     */
    public function detectBrowser($userAgent = null)
    {
        is_null($userAgent) && ($userAgent = $_SERVER['HTTP_USER_AGENT']);
        $name = null;
        $version = array(null, null, null, null);
        if (false !== strpos($userAgent, 'Opera/')) {
            //http://www.useragentstring.com/pages/Opera/
            $name = 'Opera';
            // http://dev.opera.com/articles/view/opera-ua-string-changes/
            if (false !== strpos($userAgent, 'Version/')) {
                preg_match('#Version/(\d{1,2})\.(\d{1,2})#i', $userAgent, $versionMatch);
                isset($versionMatch[1]) && $version[0] = (int)$versionMatch[1];
                isset($versionMatch[2]) && $version[1] = (int)$versionMatch[2];
            } else {
                preg_match('#Opera/(\d{1,2})\.(\d{1,2})#i', $userAgent, $versionMatch);
                isset($versionMatch[1]) && $version[0] = (int)$versionMatch[1];
                isset($versionMatch[2]) && $version[1] = (int)$versionMatch[2];
            }
        } else
        if (false !== strpos($userAgent, 'Opera ')) {
            //http://www.useragentstring.com/pages/Opera/
            $name = 'Opera';
            preg_match('#Opera (\d{1,2})\.(\d{1,2})#i', $userAgent, $versionMatch);
            isset($versionMatch[1]) && $version[0] = (int)$versionMatch[1];
            isset($versionMatch[2]) && $version[1] = (int)$versionMatch[2];
        } else
        if (false !== strpos($userAgent, 'Firefox/')) {
            // http://www.useragentstring.com/pages/Firefox/
            $name = 'Firefox';
            preg_match('#Firefox/(\d{1,2})\.(\d{1,2})(\.(\d{1,2})(\.(\d{1,2}))?)?#i', $userAgent, $versionMatch);
            isset($versionMatch[1]) && $version[0] = (int)$versionMatch[1];
            isset($versionMatch[2]) && $version[1] = (int)$versionMatch[2];
            isset($versionMatch[4]) && $version[2] = (int)$versionMatch[4];
            isset($versionMatch[6]) && $version[3] = (int)$versionMatch[6];
        } else
        if (false !== strpos($userAgent, 'MSIE ')) {
            //http://www.useragentstring.com/pages/Internet%20Explorer/
            $name = 'Internet Explorer';
            preg_match('#MSIE (\d{1,2})\.(\d{1,2})#i', $userAgent, $versionMatch);
            isset($versionMatch[1]) && $version[0] = (int)$versionMatch[1];
            isset($versionMatch[2]) && $version[1] = (int)$versionMatch[2];
        } else
        // Firefox in Debian
        if (false !== strpos($userAgent, 'Iceweasel/')) {
            // http://www.useragentstring.com/pages/Iceweasel/
            $name = 'Firefox'; //Iceweasel is identical to Firefox! no need to differt them
            preg_match('#Iceweasel/(\d{1,2})\.(\d{1,2})(\.(\d{1,2})(\.(\d{1,2}))?)?#i', $userAgent, $versionMatch);
            isset($versionMatch[1]) && $version[0] = (int)$versionMatch[1];
            isset($versionMatch[2]) && $version[1] = (int)$versionMatch[2];
            isset($versionMatch[4]) && $version[2] = (int)$versionMatch[4];
            isset($versionMatch[6]) && $version[3] = (int)$versionMatch[6];
        } else
        if (false !== strpos($userAgent, 'Chrome/')) {
            // http://www.useragentstring.com/pages/Chrome/
            $name = 'Chrome';
            preg_match('#Chrome/(\d{1,2})\.(\d{1,3})\.(\d{1,3}).(\d{1,3})#i', $userAgent, $versionMatch);
            isset($versionMatch[1]) && $version[0] = (int)$versionMatch[1];
            isset($versionMatch[2]) && $version[1] = (int)$versionMatch[2];
            isset($versionMatch[3]) && $version[2] = (int)$versionMatch[3];
            isset($versionMatch[4]) && $version[3] = (int)$versionMatch[4];
        } else
        if (false !== strpos($userAgent, 'Safari/')) {
            // http://www.useragentstring.com/pages/Safari/
            $name = 'Safari';
            preg_match('#Safari/(\d{1,3})\.(\d{1,2})(\.(\d{1,2}))?#i', $userAgent, $versionMatch);
            isset($versionMatch[1]) && $version[0] = (int)$versionMatch[1];
            isset($versionMatch[2]) && $version[1] = (int)$versionMatch[2];
            isset($versionMatch[4]) && $version[2] = (int)$versionMatch[4];
        }

        return array('name' => $name, 'version' => $version);
    }

	/**
	 * Compare browser versions.
	 * 
	 * Returns int(0)  if version is aqual to $conditions,
	 *         int(-1) if version is older than $conditions,
	 *         int(1)  if version is newer than $conditions.
	 * 
	 * Returns NULL in case of any error.
	 * 
	 * @author Leontyev Valera (feedbee@gmail.com)
	 * @copyright 2009
	 * @license BSD
	 * 
	 * @param array $browser -- result of self::detectBrowser() method
	 * @param $conditions    -- vetsions to compre array('Opera' => array(9, 4), 'Firefox' => array(3, 1, 1), ...)
	 * @return int
	 */
	protected function checkForBrowserVersion(array $browser, array $conditions)
	{
		if (!isset($browser['name']) || !isset($conditions[$browser['name']])
			|| !isset($browser['version']) || count($browser['version']) < 1)
		{
			return null;
		}
		
		$cnd = $conditions[$browser['name']]; // 0=>, 1=>, 2=>
		if (!is_array($cnd))
		{
			return null;
		}
		
		for ($i = 0; $i < count($cnd); $i++)
		{
			if ($browser['version'][$i] < $cnd[$i])
			{
				return -1;
			}
			else if ($browser['version'][$i] > $cnd[$i])
			{
				return 1;
			}
		}
		
		return 0;
	}

    /**
     * Конструктор
     *
     * @return void
     */
    public function __construct($detectBrowser = true, $detectOs = true)
    {
        if ($detectBrowser) {
            $browser = self::detectBrowser($_SERVER['HTTP_USER_AGENT']);
            if (is_null($browser['name']))
                $this->name = 'unknow';
            else {
                $this->name = $browser['name'];
                $this->version = join('.', array_filter($browser['version'], create_function('$x', 'return !is_null($x);')));
                $this->fullName = $this->name . ' ' .  $this->version;
            }
        }
        if ($detectOs) {
            $this->os = $this->getOs();
        }
    }

    /**
     * Если браузер IE
     * 
     * @return boolean
     */
    public function isIE()
    {
        return $this->name == 'Internet Explorer';
    }

    /**
     * Возращает версию ОС
     * 
     * @return string
     */
    public function getOs()
    {
        $oses = array(
            'Windows 3.11'   => 'Win16',
            'Windows 95'     => '(Windows 95)|(Win95)|(Windows_95)',
            'Windows 98'     => '(Windows 98)|(Win98)',
            'Windows 2000'   => '(Windows NT 5.0)|(Windows 2000)',
            'Windows XP'     => '(Windows NT 5.1)|(Windows XP)',
            'Windows 2003'   => '(Windows NT 5.2)',
            'Windows 7'      => 'Windows NT 6.1',
            'Windows 8.1'    => 'Windows NT 6.3',
            'Windows 8'      => 'Windows NT 6.2',
            'Windows Vista'  => 'Windows NT 6.0',
            'Windows NT 4.0' => '(Windows NT 4.0)|(WinNT4.0)|(WinNT)|(Windows NT)',
            'Windows ME'     => 'Windows ME',
            'Open BSD'       => 'OpenBSD',
            'Sun OS'         => 'SunOS',
            'Linux'          => '(Linux)|(X11)',
            'Macintosh'      => '(Mac_PowerPC)|(Macintosh)',
            'QNX'            => 'QNX',
            'BeOS'           => 'BeOS',
            'OS/2'           => 'OS\/2',
            'Mac OS'         => 'Mac OS',
            'Search Bot'     => '(nuhk)|(Googlebot)|(Yammybot)|(Openbot)|(Slurp\/cat)|(msnbot)|(ia_archiver)'
        );
        $userAgent = $_SERVER['HTTP_USER_AGENT'];
        foreach($oses as $os => $pattern) {
            if (preg_match('/' . $pattern . '/', $userAgent)) {
                return $os;
            }
        }

        return 'unknow';
    }

    /**
     * Возращает указатель на созданный экземпляр класса (если класс не создан, создаст его)
     * 
     * @return mixed
     */
    public static function getInstance($detectBrowser = true, $detectOs = true)
    {
        if (null === self::$_instance)
            self::$_instance = new self($detectBrowser, $detectOs);

        return self::$_instance;
    }
}