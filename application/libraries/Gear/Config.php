<?php
/**
 * Gear CMS
 *
 * Конфигурация системы Gear
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GConfig
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Config.php 2016-01-01 12:10:00 Gear Magic $
 */

/**
 * Конфигурация системы Gear
 * 
 * @category   Gear
 * @package    GConfig
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Config.php 2016-01-01 12:10:00 Gear Magic $
 */
class GConfig
{
   /**
     * Установленные параметры конфигурации
     *
     * @var array
     */
    protected $_install = array();

   /**
     * Параметры конфигурации по умолчанию
     *
     * @var array
     */
    protected $_default = array(
        // ключ для запросов к cms из админ-панели (для каждого сайта свой)
        'CMS/TOKEN'         => '',
        // маршрутизатор для запросов к админ-паенли
        'MANAGER/ROUTER'    => '/manager/router.php',
        // ограничивает действие куки сессии в пределах домена
        'COOKIE/DOMAIN'     => '',
        // ограничивает действие куки сессии в пределах URI
        'COOKIE/PATH'       => '/',
        // настройки модулей
        'CONFIG'            => array('Mimes' => 'MIMES', 'Robots' => 'ROBOTS/LIST', 'Router' => 'ROUTER'),
        // пароль для подключения к серверу базы данных
        'DB/PASSWORD'       => '',
        // порт подключения к серверу, по умолчанию (3306)
        'DB/PORT'           => 3306,
        // схема базы данных
        'DB/SCHEMA'         => '',
        // dns имя сервера базы данных или ip адрес (127.0.0.1 или localhost)
        'DB/SERVER'         => '127.0.0.1',
        // логин для подключения к серверу базы данных
        'DB/USERNAME'       => '',
        // кодировка базы данных
        'DB/CHARSET'        => 'utf8',
        // подключения к нескольким серверам базы данныз
        'DB/MULTIPLE'       => array(
            'localhost' => array(
                // dns имя сервера базы данных или ip адрес (127.0.0.1 или localhost)
                'SERVER'   => '127.0.0.1',
                // порт подключения к серверу, по умолчанию (3306)
                'PORT'     => 3306,
                // схема базы данных
                'SCHEMA'   => 'test',
                // логин для подключения к серверу базы данных
                'USERNAME' => 'root',
                // пароль для подключения к серверу базы данных
                'PASSWORD' => 'root',
                // кодировка базы данных
                'CHARSET'  => 'UTF-8'
            )
        ),
        // часовой пояс
        'TIMEZONE'       => 'Europe/Kiev',
        // язык по умолчанию
        'LANGUAGE'       => 'ru-RU',
        // подключать пути в настройках php
        'PATH/INCLUDE'   => true,
        // имя сессии
        'SESSION/NAME'   => 'cms',
        // жизнь сессии в минутах
        'SESSION/EXPIRE' => 15,
        // настройки PHP
        'PHP'            => array(
            // кодировка по умолчанию
            'DEFAULT_CHARSET'     => 'UTF8',
            // максимальный размер загрузки файлов
            'UPLOAD_MAX_FILESIZE' => '',
            // максимальный размер данных передаваемых через метод "POST"
            'POST_MAX_SIZE'       => '',
            // maximum amount of memory in bytes that a script is allowed to allocate
            'MEMORY_LIMIT'        => '100M',
            // maximum time in seconds a script is allowed to run before it is terminated by the parser
            'MAX_EXECUTION_TIME'  => 60
        ),
        // допустимые php расширения
        'PHP/EXTENSIONS'    => array('mysql', 'json', 'session', 'gd', 'mbstring'/*, 'bcmath'*/),
        // импорт модулей
        'IMPORT' => array()
    );

   /**
     * Текущие настройки
     *
     * @var array
     */
    protected $_params = array();

    /**
     * Файл конфигурации (по умолчанию)
     *
     * @var string
     */
    protected $_filename = 'System.php';

    /**
     * Сылка на экземпляр класса
     *
     * @var mixed
     */
    protected static $_instance = null;

    /**
     * Конструктор
     *
     * @return void
     */
    public function __construct($path = 'config')
    {
        // подключение установленных настроек
        if (($this->_install = require_once($path . '/' . $this->_filename)) === false) return;
        // настройки сайта
        $siteSettings = require_once($path . '/Site.php');
        // применении настроек
        $this->_params = array_merge($this->_default, $this->_install, $siteSettings);
        if (empty($this->_params['DOCUMENT_ROOT']))
            $this->_params['DOCUMENT_ROOT'] = $_SERVER['DOCUMENT_ROOT'];
        // возращает настройки модулей
        $this->additionalConfig($path);
        // импорт сценариев
        $this->loadImport($this->_params['IMPORT']);
    }

    /**
     * Возращает указатель на созданный экземпляр класса (если класс не создан, создаст его)
     * 
     * @return mixed
     */
    public static function getInstance($path = 'config')
    {
        if (null === self::$_instance)
            self::$_instance = new self($path);

        return self::$_instance;
    }

    /**
     * Возращает настройки модулей (указанных в параметре "CONFIG")
     *
     * @return array
     */
    public function additionalConfig($path = 'config')
    {
        foreach ($this->_params['CONFIG'] as $key => $value) {
            $filename = $path . '/' . $key . '.php';
            if (($arr = require_once($filename)) !== false)
                $this->_params[$value] = $arr;
        }
    }

    /**
     * Импорт сценариев из каталога "Gear/Imports"
     *
     * @param  mixed $arr массив сценариев (array("simple1", "simple2", ...) => array("Gear/Imports/simple1.php", "Gear/Imports/simple2.php", ....))
     * @return array
     */
    public function loadImport($arr)
    {
        if (is_array($arr)) {
            $size = sizeof($arr);
            for ($i = 0; $i < $size; $i++) {
                Gear::import($arr[$i]);
            }
        } else
            Gear::import($arr);
    }

    /**
     * Возращает установленные параметры по ключу (если он есть)
     *
     * @param  mixed $key ключ
     * @return mixed
     */
    public function getInstall($key = null)
    {
        if ($key)
            return isset($this->_install[$key]) ? $this->_install[$key] : false;

        return $this->_install;
    }

    /**
     * Возвращает текущие настройки системы по ключу (если он есть)
     *
     * @param  string $key ключ
     * @return mixed
     */
    public function getParams($key = '', $default = '')
    {
        if ($key)
            return isset($this->_params[$key]) ? $this->_params[$key] : $default;

        return $this->_params;
    }

    /**
     * Установка значения параметра настройки системы
     *
     * @param  string $key ключ
     * @param  string $value значение
     * @return void
     */
    public function setParams($key, $value)
    {
        $this->_params[$key] = $value;
    }

    /**
     * Возвращает значение параметра из файла конфигурации по ключу
     *
     * @param  string $name файл конфигурации
     * @param  string $key ключ
     * @return mixed
     */
    public function getFrom($name, $key = '', $default = false)
    {
        if ($key)
            return isset($this->_params[$name][$key]) ? $this->_params[$name][$key] : $default;

        return isset($this->_params[$name]) ? $this->_params[$name] : $default;
    }

    /**
     * Возвращает значение параметра из файла конфигурации по ключу
     *
     * @param  string $name файл конфигурации
     * @param  string $key ключ
     * @return mixed
     */
    public function load($basename)
    {
        $name = strtoupper($basename);
        if (($settings = require(PATH_CONFIG . $basename . '.php')) === false) {
            if (isset($this->_params[$name]))
                return $this->_params[$name];
            else
                return false;
        } else {
            if ($settings === true)
                return $this->_params[$name];
            else
                return $this->_params[$name] = $settings;
        }
    }

    /**
     * Возвращает текущие настройки системы по ключу (если он есть)
     *
     * @param  string $key ключ
     * @return mixed
     */
    public function get($key = '', $default = '')
    {
        return $this->getParams($key, $default = '');
    }

    /**
     * Установка параметра настройки системы
     *
     * @param  string $key ключ
     * @param  mixed $value значение
     * @return void
     */
    public function set($key, $value)
    {
        $this->setParams($key, $value);
    }

    /**
     * Возвращает настройки системы по умолчанию
     *
     * @param  string $key ключ
     * @return mixed
     */
    public function getDefault($key = '')
    {
        if ($key)
            return isset($this->_default[$key]) ? $this->_default[$key] : false;

        return $this->_default;
    }

    /**
     * Проверка существования расширения
     *
     * @var boolean
     */
    public function initExtensions()
    {
        $ext = $this->getParams('PHP/EXTENSIONS');
        $count = sizeof($ext);
        for ($i = 0; $i < $count; $i++)
            if (!extension_loaded($ext[$i]))
                return $ext[$i];

        return true;
    }

    /**
     * Проверка существования расширения PHP
     *
     * @return mixed
     */
    public function initPHP()
    {
        foreach($this->_params['PHP'] as $key => $value) {
            $key = strtolower($key);
            if (strlen($value) > 0)
                if (ini_set($key, $value) === false)
                    return $key;
        }

        return true;
    }
}
?>