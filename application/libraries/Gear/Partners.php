<?php
/**
 * Gear CMS
 * 
 * Пакет партнёров
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Partner.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Класс партнёров
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Partner.php 2016-01-01 12:00:00 Gear Magic $
 */
class GPartners
{
    public static $place = array();

    /**
     * Возращает информацию о партнере
     * 
     * @param string $value
     * @param string $type тип (login, e-mail, login_or_e-mail, id, code)
     * @return mixed
     */
    public static function getPlace($value = '', $type = 'uri')
    {
        if (empty($value) && !empty(self::$place)) return self::$place;

        $query = GFactory::getQuery();
        $value = $query->escapeStr($value);
        switch ($type) {
            case 'uri': $cond = "`place_uri`=$value"; break;

            case 'id': $cond = "`place_id`=$value"; break;

            default: $cond = ' 0';
        }

        // проверка существования пользователя
        $sql = 'SELECT * FROM `place_names` WHERE ' . $cond;
        $place = $query->getRecord($sql);
        if ($place === false) return false;
        if (empty($place)) return false;

        return self::$place = $place;
    }

    /**
     * Возращает список всех партнёров
     * 
     * @return mixed
     */
    public static function getAllPlaces($onMap = true)
    {
        $query = GFactory::getQuery();
        $dirImages = Gear::$app->config->get('DIR/IMAGES');
     
        // проверка существования пользователя
        $sql = 'SELECT * FROM `place_names`';
        if ($onMap)
            $sql .= ' WHERE `place_map`=1';
        if ($query->execute($sql) !== true) return false;
        $items = array();
        while (!$query->eof()) {
            $item = $query->next();
            if ($item['place_coord_ln'] && $item['place_coord_lt'])
                $coord = $item['place_coord_lt'] . ',' . $item['place_coord_ln'];
            else
                $coord = '';
            $items[] = array(
                'id'          => $item['place_id'],
                'name'        => $item['place_name'],
                'description' => $item['place_description'],
                'uri'         => $item['place_uri'],
                'folder'      => $item['place_folder'],
                'image'       => $item['place_image'] ? $item['place_image'] : $dirImages . 'place-none.jpg',
                'address'     => $item['place_address'],
                'phone'       => $item['place_phone'],
                'kitchen'     => $item['place_kitchen'],
                'schedule'    => $item['place_schedule'],
                'coord'       => $coord
            );
        }

        return $items;
    }

    /**
     * Возращает список всех категорий партнёров
     * 
     * @return mixed
     */
    public static function getCategories($parentId)
    {
        $query = GFactory::getQuery();

        // список всех категорий предка
        $sql = 'SELECT `c`.* FROM `site_categories` `c` JOIN (SELECT category_left, category_right  FROM `site_categories` WHERE `category_id`=' . $parentId . ') `p` '
             . 'ON `c`.`category_left`>`p`.`category_left` AND `c`.`category_right`<`p`.`category_right` AND `c`.`category_visible`=1';
        if ($query->execute($sql) !== true) return false;
        $items = array();
        while (!$query->eof()) {
            $item = $query->next();
            $items[] = array(
                'id'   => $item['category_id'],
                'name' => $item['category_name'],
                'uri'  => $item['category_uri']
            );
        }

        return $items;
    }

    /**
     * Возращает список всех категорий партнёров
     * 
     * @return mixed
     */
    public static function getImages($placeId)
    {
        $query = GFactory::getQuery();

        // список всех изображений заведения
        $sql = 'SELECT * FROM `place_images` WHERE `place_id`=' . $placeId . ' AND `image_visible`=1 ORDER BY `image_index` ASC';
        if ($query->execute($sql) !== true) return array();
        $items = array();
        while (!$query->eof()) {
            $item = $query->next();
            $items[] = array(
                'id' => $item['image_id'],
                'i' => array('file' => $item['image_entire_filename'], 'title' => $item['image_title']),
                't' => array('file' => $item['image_thumb_filename'], 'title' => $item['image_thumb_title']),
            );
        }

        return $items;
    }
}
?>