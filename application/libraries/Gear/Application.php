<?php
/**
 * Gear CMS
 * 
 * Пакет приложения
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Application.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Класс приложения
 * 
 * @category   Gear
 * @package    Core
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Application.php 2016-01-01 12:00:00 Gear Magic $
 */
class GApplication
{
    /**
     * Если страница сайта кэшируется
     *
     * @var boolean
     */
    public static $isCaching = false;

    /**
     * Обработчик SQL запросов
     *
     * @var object
     */
    protected $_query;

    /**
     * Компоненты приложения найденные парсером
     *
     * @var array
     */
    public static $components = array();

    /**
     * Указатель на экземпляр класса обработки запросов форм
     *
     * @var object
     */
    public $input = null;

    /**
     * Указатель на экземпляр класса конфигурации
     *
     * @var object
     */
    public $config = null;

    /**
     * Указатель экземпляра класса URL
     *
     * @var object
     */
    public $url = null;

    /**
     * Указатель экземпляра класса даты
     *
     * @var object
     */
    public $date = null;

    /**
     * Указатель экземпляра класса маршрутизатора
     *
     * @var object
     */
    public $router = null;

    /**
     * Указатель экземпляра класса документа
     *
     * @var object
     */
    public $document = null;

    /**
     * Указатель экземпляра класса языка
     *
     * @var object
     */
    public $language = null;

    /**
     * Указатель экземпляра класса сессии
     *
     * @var object
     */
    public $session = null;

    /**
     * Указатель экземпляра класса браузера
     *
     * @var object
     */
    public $browser = null;

    /**
     * Загрузить данные для страницы
     *
     * @var boolean
     */
    public $isLoadDataPage = true;

    /**
     * Данные страницы
     *
     * @var mixed
     */
    public $dataPage = false;

    /**
     * Данные лендинг страницы (блоки из "site_landing_pages")
     * блоки загружаются если сайт в режиме "конструктора" для правки через шаблон
     *
     * @var mixed
     */
    public $dataLandingPage = false;

    /**
     * Массив данных для обмена между компонентами
     *
     * @var array
     */
    public $data = array();

    /**
     * Режим работы приложения ("design" - отображение компонентов, "default" - режим работы)
     * 
     * @var string
     */
    public $designMode = false;

    /**
     * Категория статьи
     * 
     * @var mixed
     */
    public $category = false;

    /**
     * Работает ли ЧПУ
     * 
     * @var boolean
     */
    public $sef = false;

    /**
     * Использовать отладку системы
     * 
     * @var mixed
     */
    public $debug = false;

    /**
     * Если статья с ограниченым доступом, то появится класс аутентификации пользователя
     * 
     * @var mixed
     */
    public $auth = false;

    /**
     * Код
     * 
     * @var integer
     */
    public $domainId = 0;

    /**
     * Код
     * 
     * @var boolean
     */
    public $loadedFromCache = false;

    /**
     * Код
     * 
     * @var boolean
     */
    public $closeHtml = true;

    /**
     * Если еесть token для управления сайтом через админпанель
     * 
     * @var boolean
     */
    public $hasToken = false;

    /**
     * Инициализация приложения
     *
     * @return void
     */
    public function initialise()
    {
        // конфигурации системы
        $this->config = GFactory::getConfig(PATH_CONFIG);
        // если подключить сайты на других доменах
        if ($this->config->get('OUTCONTROL')) {
            $this->config->load('Domains');
            // идент. домена
            $this->domainId = $this->getSiteDomain();
        }
        // использовать отладку системы
        if ($this->config->get('DEBUG'))
            $this->debug = GFactory::getDg();
        // работает ли ЧПУ
        $this->sef = $this->config->get('SEF');
        // обработчик строки запроса
        $this->url = GFactory::getURL();
        // обработчик языков системы
        Gear::library('/Language');
        $this->language = GLanguage::getInstance($this->url->language, $this->config->get('LANGUAGE'), $this->config->get('LANGUAGES'));
        // если отладка
        if ($this->debug) $this->debug->info($this->url->path, GText::_('debugging the page', 'interface'));
        // версия приложения
        Gear::library('/Version');
        // сессия
        $this->session = GFactory::getSession();
        // обработчик дат системы
        $this->date = GFactory::getDate($this->config->get('TIMEZONE'));
        // обработчик статей
        Gear::library('/Article');
        // кэша приложения
        $this->cache = GFactory::getCache();
        // обработчик форм
        $this->input = GFactory::getClass('Input');
        // маршрутизатор приложения
        $this->router = GFactory::getRouter();
        // документ
        $this->document = GFactory::getDocument();

        // доступен ли сайт
        if ($this->config->get('INACCESSIBLE')) {
            // если отладка
            if ($this->debug) $this->debug->log('yeas', GText::_('site disabled in settings', 'interface'));
            Gear::content('site_inaccessible.tpl', true);
        }

        // определение роботов
        if ($this->config->get('ROBOTS'))
            $this->detectRobots();
        // определение браузера
        $this->browser = GFactory::getBrowser();
        // определение режима компонентов в приложении
        $token = isset($_COOKIE['cms-token']) ? $_COOKIE['cms-token'] : '';
        $this->hasToken = $token == $this->config->get('CMS/TOKEN');
        $mode = isset($_COOKIE['cms-mode']) ? $_COOKIE['cms-mode'] : '';
        // если режим конструктора
        $this->designMode = $mode == 'design' && $this->hasToken;
        if ($this->designMode) {
            // если отладка
            if ($this->debug) $this->debug->log('yes', GText::_('design view', 'interface'));
        } else {
            // если отладка
            if ($this->debug) $this->debug->log('no', GText::_('design view', 'interface'));
        }

        if ($theme = $this->defineThemeFromUrl())
            $this->config->set('THEME', $theme);
    }

    /**
     * Установка темы через URL
     *
     * @return bool
     */
    protected function defineThemeFromUrl()
    {
        if (isset($_GET['theme']))
            $theme = $_GET['theme'];
        else
        if (isset($_COOKIE['theme']))
            $theme = $_COOKIE['theme'];
        if (empty($theme)) return false;
        if ($theme == 'common' || $theme == 'system') return false;

        $theme = strtolower(preg_replace ("/[^a-zA-ZА-Яа-я0-9\s]/","", $theme));
        $path = './' . PATH_TEMPLATE . $theme;
        if (!file_exists($path))
            die('The parameter in the url string for the name of the topic is not specified correctly. The Theme is not found. If the message is continually displayed, RESET COOKIES!');

        if (isset($_GET['theme']))
            setcookie('theme', $theme, 0, '/', ltrim($_SERVER['SERVER_NAME'], 'www'));

        return $theme;
    }

    /**
     * Инициализация заголовков ответа
     *
     * @return void
     */
    public function initHeaders()
    {
        header('X-Powered-By: ' . GVersion::getPoweredBy());
        header('X-Cache: generated');
    }

    /**
     * Добавление объявления в заголовок HTTP
     *
     * @return void
     */
    public function addHeader($key, $value)
    {
        header($key . ': ' . $value);
    }

    /**
     * Определение роботов в запросе
     *
     * @return void
     */
    public function detectRobots()
    {
        // если отладка
        if ($this->debug) $this->debug->log('yes', GText::_('article category', 'interface'));
        $robots = $this->config->load('Robots');
        $limit = $robots['LIMIT'];
        $list = $robots['LIST'];
        $table = null;
        foreach($list as $key => $value) {
            // если бот есть в списке
            if (strstr($_SERVER['HTTP_USER_AGENT'], $value['name'])) {
                if (is_null($table)) {
                    // установить соединение с сервером
                    GFactory::getDb()->connect();
                    $table = new GDbTable('site_bots', 'bot_id');
                }
                $table->insert(
                    array('bot_name'      => $value['name'],
                          'bot_details'   => $_SERVER['HTTP_USER_AGENT'],
                          'bot_uri'       => $_SERVER['REQUEST_URI'],
                          'bot_date'      => date('Y-m-d H:i:s'),
                          'bot_ipaddress' => $_SERVER['REMOTE_ADDR'],
                          'bot_access'    => $value['access'] ? 1 : 0)
                );
                // если количество ботов привышают допустимый предел в базе данных
               if (($table->getLastInsertId() > $limit) && $limit > 0) {
                    // чистим таблицу
                    $table->clear();
                }
                // если доступ боту в настройках системы закрыт
                if (!$value['access']) {
                    header("HTTP/1.0 404 Not Found");
                   exit;
                }
                break;
            }
        }
    }

    /**
     * Проверка мобильной версии
     *
     * @return boolean
     */
    public function isMobile()
    {
          $pda_patterns = array(
            'MIDP','FLY-','MMP','Mobile','MOT-',
            'Nokia','Obigo','Panasonic','PPC',
            'ReqwirelessWeb','Samsung','SEC-SGH',
            'Smartphone','SonyEricsson','Symbian',
            'WAP Browser','j2me','BREW', 'iPod', 'iPhone'
          );
          $agent = $_SERVER['HTTP_USER_AGENT'];
          $user_agent = strtolower($agent);
          foreach($pda_patterns as $val){
            $val = strtolower($val);
            if(strpos($user_agent, $val) !== false) return true;
          }

          return false;
    }

    /**
     * Возращает информацию о подключенном домене
     *
     * @param  mixed $index индекс в массиве indexes доменов
     * @return void
     */
    public function getSiteDomain($index = false)
    {
        if ($index !== false) {
            $arr = $this->config->getFrom('DOMAINS', 'indexes');
            if (isset($arr[$this->domainId])) {
                if ($index == -1)
                    return $arr[$this->domainId];
                else
                    return $arr[$this->domainId][$index];
            } else
                return false;
        }

        $arr = $this->config->getFrom('DOMAINS', 'domains');
        if ($arr === false) return 0;

        if (isset($arr[$_SERVER['HTTP_HOST']]))
            return $arr[$_SERVER['HTTP_HOST']];
        else
            return 0;
    }

    /**
     * Загрузка категории
     *
     * @return void
     */
    public function loadCategory()
    {
        // если категория уже загружена через маршрутизатор (исключение, если категории имеют не древови-ю структуру)
        if ($this->category !== false) return;

        // категория статьи
        if (empty($this->category)) {
            // если работает ЧПУ
            if ($this->sef) {
                $this->category = GCategories::get($this->url->pathOnly, $this->domainId, $this->_query);
            } else
                $this->category = GCategories::getById($this->url->getVar('c'), $this->domainId, $this->_query);
            // если отладка
            if ($this->debug) $this->debug->log($this->category, GText::_('article category', 'interface'));
        }

        if (empty($this->category)) return;

        // если работает ЧПУ
        if ($this->sef)
            $useList = $this->category['list'] && empty($this->url->filename);
        else
            // выбрана список а не статья
            $useList = $this->category['list'] && !$this->url->hasVar('a');
        // если в категории указан список, а в ссылке не указан файл
        if ($useList) {
            $this->dataPage = GArticles::getDataTemplate($this->category);
            $this->dataPage['page_header'] = $this->category['category_name'];
           
			if (empty($this->dataPage['page_title'])){
				 $this->dataPage['page_title'] = $this->category['category_name'];
			}
            // добавление данных о статье в документ
            $this->document->setFromArticle($this->dataPage);
            // смена компонента статьи
            if (!empty($this->category['list_class'])) {
                // если отладка
                if ($this->debug) $this->debug->log($this->category['list_class'], GText::_('involved in the category list', 'interface'));
                $cpath = '';
                $types = $this->config->get('LIST/TYPES');
                if (isset($types[$this->category['list_class']]))
                    $cpath = $types[$this->category['list_class']]['path'];
                $this->document->component('Article', array(
                    'class'      => $this->category['list_class'],
                    'path'       => $cpath,
                    'template'   => $this->category['list_template'], // шаблон списка
                    'list-limit' => (int) $this->category['list_limit'], // количество записей на странице (по умолчанию 0 )
                    'list-order' => $this->category['list_order'], // порядок сортировки (d - по убыванию, a - по возрастанию)
                    'list-field' => $this->category['list_sort'], // критерий сортировки (header - по алфавиту, date - по дате публикации)
                    'list-subcategory' => $this->category['list_subcategory'], // выводить записи опубликованные в субкатегориях
                    'list-article-type' => $this->category['list_type'] // выводить статьи по виду (0 - все, 1 - статья, 2 - новость)
                ));
            }
            // кэширование категории
            $this->document->caching = $this->category['list_caching'];
            $this->cache->generatorId = $this->category['category_id'];
            $this->cache->generator = 'category';
        }
    }

    /**
     * Загрузка данных статьи
     *
     * @return void
     */
    public function loadData()
    {
        // инициализация статьи сайта (если данные страницы ранее были загружены каким-либо компонентом, нет смысла искать статью по запросу)
        if ($this->dataPage === false) {
            // если работает ЧПУ
            if ($this->sef) {
                $this->dataPage = GArticles::getByUrl($this->url, $this->language->get('id'), $this->category, $this->_query);
                // если статья не опубликована
                if (!empty($this->dataPage))
                if (!$this->dataPage['published']) {
                    // если открыта админпанель и есть привью статьи
                    if (!($this->hasToken && isset($_GET['preview'])))
                        $this->dataPage = array();
                }
            } else {
                // если в урл есть запрос на статью
                if ($this->url->hasVar('a')) {
                    $this->dataPage = GArticles::getById($this->url->getVar('a'), $this->language->get('id'), 0, $this->_query);
                    // для выборки категории т.к. без ЧПУ берется из статьи
                    if (!empty($this->dataPage['category_id']))
                        $this->url->setVar('c', $this->dataPage['category_id']);
                // если главная страница
                } else {
                    if ($this->url->isRoot())
                        $this->dataPage = GArticles::getIndex($this->language->get('id'), $this->_query);
                    // если в урл нет запроса на статью 
                    /*else
                        return;*/
                }
            }
            // если режим конструктора
            if ($this->designMode && $this->dataPage)
                $this->dataLandingPage = GArticles::getLandingBlocks($this->dataPage['article_id']);
        }

        // проверка существ-я статьи
        if (empty($this->dataPage['page_html']))
            $this->document->setText('');
        else
            $this->document->setText($this->dataPage['page_html']);
        $this->document->setFromArticle($this->dataPage);
        // если нет статьи
        if (empty($this->dataPage)) {
            $this->document->setCode(404);
            return;
        }
        //если нет доступа к статье
        if ($this->dataPage['access_id'] == 2) {
            $this->auth = GFactory::getAuth();
            if (!$this->auth->check()) {
                $this->document->setCode(401);
                return;
             }
        }
        // если статья есть
        if (!empty($this->dataPage['article_id'])) {
            $this->cache->generatorId = $this->dataPage['article_id'];
            $this->cache->note = $this->dataPage['article_note'];
        }
    }

    /**
     * Установка данных статьи если замена статьи дургим компонентом (используется в маршрутизаторе запросов)
     * 
     * @param array $data данные статьи
     * @return void
     */
    public function setDataPage($data)
    {
        $this->dataPage = GArticles::getDataTemplate($data);
        $this->document->setFromArticle($this->dataPage);
    }

    /**
     * Вывод данных приложения
     *
     * @return void
     */
    private function _render()
    {
        // инициализация заголовков ответа
        $this->initHeaders();
        // если AJAX запрос
        if ($this->url->isAjax()) {
            // если отладка
            if ($this->debug) $this->debug->log('yes', GText::_('ajax request', 'interface'));
            // проверка существования кэша, если кэш существует вывести его
            if ($this->cache->isExist() && $this->config->get('CACHE')) {
                // если запрос был черех AJAX нет смысла закрывать разметку html
                $this->closeHtml = false;
                $this->cache->render(false);
                return;
            }
            // вызов маршрутизатора на AJAX запросе
            $content = $this->router->call('ajaxInit');
            // кэшировать можно страницы не поступаемые от обработки форм
            if ($this->url->isCaching() && $this->config->get('CACHE')) {
                // если есть необходимость кэшировать весь контент
                // и если в настройках выставлено кэширование
                if (self::$isCaching) {
                    $this->addHeader('X-Cache', 'file');
                    $this->cache->update($content);
                }
            }
            echo $content;
            //return;
            exit;
        }
        // проверка существования кэша, если кэш существует вывести его и не в режиме конструктора
        if ($this->url->isCaching() && !$this->designMode && $this->config->get('CACHE')) {
            if ($this->cache->isExist()) {
                // если отладка
                if ($this->debug) $this->debug->log('yes', GText::_('load from cache', 'interface'));
                $this->addHeader('X-Cache', 'file');
                $this->cache->render();
                $this->loadedFromCache = true;
                return;
            }
        }
        // обработчик статей
        Gear::library('/String');

        // вызов маршрутизатора до инициализации данных приложения
        $this->router->call('beforeInit');

        // установить соединение с сервером
        GFactory::getDb()->connect();
        $this->_query = new GDbQuery();

        // если работает ЧПУ
        if ($this->sef)
            // категория статьи
            $this->loadCategory();

        // если в маршрутизаторе ранее не указывалось замена страницы компонентом
        if (!$this->document->schemaRouter && $this->isLoadDataPage) {
            // если компонент статьи не был заменен другим компонентом
            if ($this->document->isComponent('Article') === false) {
                $this->loadData();
            }
        }
        // если не работает ЧПУ
        if (!$this->sef)
            // категория статьи (указывается идент. категории в статье loadData)
            $this->loadCategory();

        // вызов маршрутизатора после инициализации данных приложения
        $this->router->call('afterInit');
        // исходный контент
        $this->page = GFactory::getPage();
        $content = $this->page->getContent();

        // вывод контента
        echo $content;
    }

    /**
     * Вывод контента приложения
     *
     * @return void
     */
    public function render()
    {
        // если используется GZIP сжатие
        if ($this->config->get('GZIP')) {
            header('Content-Type: text/html; charset=' . $this->config->get('CHARSET'));
            $res = ob_start('ob_gzhandler');
            // если отладка
            if ($this->debug) $this->debug->log($res ? 'yes' : 'no', GText::_('using Gzip', 'interface'));
        }

        // использовать глобальные теги
        if ($this->config->get('GLOBAL/TAGS'))
            ob_start(array($this->document, 'replaceAlias'));

        $this->_render();

        if ($this->config->get('GLOBAL/TAGS'))
            ob_end_flush();

        $content = ob_get_contents();

        // кэшировать можно страницы не поступаемые от обработки форм и не в режиме конструктора
        // и если в настройках выставлено кэширование
        if ($this->url->isCaching() && !$this->designMode && $this->config->get('CACHE')) {
            // если есть необходимость кэшировать весь контент
            if (self::$isCaching || $this->document->caching) {
                $this->cache->update($content);
            }
        }

        // если отладка
        if ($this->debug) {
            Gear::library('/File');
            if (function_exists('memory_get_peak_usage'))
                $this->debug->log(GFile::sizeToStr(memory_get_peak_usage()), GText::_('the peak value of the amount of memory allocated to the script', 'interface'));
            if (function_exists('memory_get_usage'))
                $this->debug->log(GFile::sizeToStr(memory_get_usage()), GText::_('the amount of memory allocated script', 'interface'));
        }
    }
}
?>