<?php
/**
 * Gear CMS
 * 
 * Пакет обработки URI запроса
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Uri.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Класс обработки URI запроса
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Uri.php 2016-01-01 12:00:00 Gear Magic $
 */
class GUrl
{
    /**
     * Идент. статьи (если указана в конце строки)
     *
     * @var integer
     */
    public $id = 0;

    /**
     * Путь + файл (полученный из URL)
     *
     * @var string
     */
    public $path = '/';

    /**
     * Путь (полученный из URL)
     *
     * @var string
     */
    public $pathOnly = '/';

    /**
     * Название файла (полученный из URL)
     *
     * @var string
     */
    public $filename = false;

    /**
     * Путь разбитый на сегменты
     *
     * @var array
     */
    protected $_segments = array();

    /**
     * URI полученный из $_SERVER['REQUEST_URI']
     *
     * @var string
     */
    public $request = '';

    /**
     * Сылка на экземпляр класса
     *
     * @var mixed
     */
    protected static $_instance = null;

    /**
     * Префикс языка получаемый из запроса (defineComponents, "en-GB", "uk-UA")
     *
     * @var string
     */
    public $language = '';

    /**
     * Название компонента для совершения дейтсвия (если ЧПУ не работает)
     * получено из GET запроса
     *
     * @var string
     */
    public $do = '';

    /**
     * Название хоста
     *
     * @var string
     */
    public $host = '';

    /**
     * Схема URL
     *
     * @var string
     */
    public $scheme = '';

    /**
     * Конструктор
     * 
     * @param string $store состояние сессии ($_state)
     * @return void
     */
    public function __construct()
    {
        // метода запроса
        $this->method = strtoupper($_SERVER['REQUEST_METHOD']);
        // переменные запроса
        $this->_vars = &$_GET;
        // определение запроса пользователя
        $this->defineRequest();
        // если работает ЧПУ
        if (!Gear::$app->sef)
            // действие для вызова компонентов
            $this->do = $this->getVar('do', '');
        // название хоста
        if (isset($_SERVER['REQUEST_SCHEME'])) {
            $this->scheme = $_SERVER['REQUEST_SCHEME'];
        } else {
            if ($this->isSSL())
                $this->scheme = 'https';
            else
                $this->scheme = 'http';
        }
        $this->host = $this->scheme . '://' . $_SERVER['SERVER_NAME'];
    }

    /**
     * Возращает указатель на созданный экземпляр класса (если класс не создан, создаст его)
     * 
     * @return mixed
     */
    public static function getInstance()
    {
        if (null === self::$_instance)
            self::$_instance = new self();

        return self::$_instance;
    }

    /**
     * Проверка на ssl
     * 
     * @return boolean
     */
    public function isSSL()
    {
        if (isset($_SERVER['HTTPS'])) {
            return !empty($_SERVER['HTTPS']) && stristr($_SERVER['HTTPS'], 'off') === false;
        } else
            if (isset($_SERVER['SERVER_PORT'])) {
                return '443' == $_SERVER['SERVER_PORT'];
            }

        return false;
    }

    /**
     * Определение запроса пользователя
     * 
     * @return void
     */
    protected function defineRequest()
    {
        $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        if ($path == '/') return; 

        $this->_segments = explode('/', $path);
        // убираем 1-й сегмент "/"
        array_shift($this->_segments);
        // убираем последний сегмент "/"
        if ($path[strlen($path) - 1] == '/')
            array_pop($this->_segments);
        // проверка сегмента на язык
        if ($this->_segments) {
            $langs = GFactory::getConfig()->get('LANGUAGES');
            if (isset($langs[$this->_segments[0]])) {
                $this->language = $this->_segments[0];
                array_shift($this->_segments);
            }
        }
        // проверка сегмента на файл
        if ($this->_segments) {
            $str = $this->_segments[sizeof($this->_segments) - 1];
            if (strpos($str, '.') !== false) {
                $this->filename = $this->_segments[sizeof($this->_segments) - 1];
                array_pop($this->_segments);
            }
        }
        // определяем путь для запросов
        $this->path = $this->pathOnly = '/';
        $str = implode('/', $this->_segments);
        if ($str)
             $this->path .= $str . '/';
        $this->pathOnly = $this->path;
        if ($this->filename)
            $this->path .= $this->filename;

        // идент. статьи если указан в строке
        $this->id = $this->getId();
        // BUG: не должен появляться, приходиться удалять
        $this->remove('REQUEST_PATH');
    }

    /**
     * Проверка URL регулярным выражением
     * 
     * @return integer
     */
    public function pregMatch($pattern, $subject = '')
    {
        if (empty($subject))
            $subject = $this->path;

        return preg_match($pattern, $subject);
    }

    /**
     * Возращает идент. записи в конце строки
     * 
     * @return integer
     */
    protected function getId($side = 'left')
    {
        switch ($side) {
            // правая сторона
            case 'right':
                // поиск разделителя в конце строки (//bla-bla-123.html)
                $pos = strrpos($this->filename, '-');
                if ($pos === false) return 0;
                $str = substr($this->filename, $pos + 1);
                $id = pathinfo($str, PATHINFO_FILENAME);
                // если id цифра
                if (is_numeric($id)) {
                    // нзвание файла без id
                    $this->filename = substr($this->filename, 0, $pos);

                    return $id;
                } else
                    return 0;

            // левая сторона
            case 'left':
                // поиск разделителя в конце строки (//123-bla-bla.html)
                $pos = strpos($this->filename, '-');
                if ($pos === false) return 0;
                $id = substr($this->filename, 0, $pos);
                // если id цифра
                if (is_numeric($id)) {
                    // нзвание файла без id
                    $this->filename = substr($this->filename, $pos + 1);

                    return $id;
                } else
                    return 0;
        }
    }

    /**
     * Возращает true если страница главная
     * 
     * @return boolean
     */
    public function isRoot()
    {
        // если ЧПУ
        if (Gear::$app->sef)
            return $this->path == '/';
        else
            return $this->path == '/' && !$this->hasVars();
    }

    /**
     * Если сегментов нет (есть только "/")
     * 
     * @return boolan
     */
    public function isRootSeg()
    {
        return empty($this->_segments);
    }

    /**
     * Если сегментов нет (есть только "/")
     * 
     * @return boolan
     */
    public function isOneSeg()
    {
        return empty($this->filename) && $this->getSegSize() == 1;
    }

    /**
     * Если запрос выполнен через AJAX
     * 
     * @return boolan
     */
    public function isAjax()
    {
        return $this->getSeg(0) == 'request' || !empty($_GET['request']);
    }

    /**
     * Возращает часть пути сегмента полученного из URI
     * 
     * @param  integer $index номер сегмента
     * @param  string $default значение по умолчанию если сегмента нет
     * @return mixed
     */
    public function getSeg($index = 0, $default = '')
    {
        // если есть выбранный сегмент
        if (isset($this->_segments[$index]))
            return $this->_segments[$index];

        return $default;
    }

    /**
     * Возращает идент. из пути сегмента
     * 
     * @param  integer $index номер сегмента
     * @param  string $default значение по умолчанию если идент. нет
     * @return integer
     */
    public function getSegId($index = 0, $default = 0)
    {
        return (int) $this->getSeg($index, $default);
    }

    /**
     * Возращает URL без параметров
     * 
     * @return string
     */
    public function getWithoutQuery()
    {
        return $this->host . $this->pathOnly;
    }

    /**
     * Возращает часть пути сегмента полученного из URI
     * 
     * @param  integer $index номер сегмента
     * @param  string $default значение по умолчанию если сегмента нет
     * @return mixed
     */
    public function segToStr($count = 0)
    {
        if (empty($count) || $this->getSegSize() < $count)
            $count = $this->getSegSize();
        $str = '';
        for ($i = 0; $i < $count; $i++)
            $str .= '/' . $this->_segments[$i];

        return $str . '/';
    }

    /**
     * Проверка сегмента URL на валидность
     * 
     * @param  integer $index номер сегмента (от 1 и до ...)
     * @param  string $path название сегмента
     * @param  integer $size кол-о всех сегментов
     * @return boolean
     */
    public function checkSeg($index, $path, $size = 0)
    {
        if ($size)
            return $this->getSeg($index) == $path && $this->getSegtSize() == $size;

        return $this->getSegment($index) == $path;
    }

    /**
     * Проверка сегмента URL на присутствие даты формата "dd/mm/yyyy, mm/yyyy, yyyy"
     * 
     * @param  string $path сегмент
     * @param  array $allow массив с допуском формата даты по сегменту
     * @return boolean
     */
    public function checkDate($allow = array(true, true, true), $subject = '')
    {
            // проверка на дату "dd/mm/yyyy"
            if ($this->pregMatch('/^\/([0-9]{2})\/([0-9]{2})\/([0-9]{4})\/$/', $subject) > 0 && $allow[0])
                return true;
            // проверка на дату "mm/yyyy"
            if ($this->pregMatch('/^\/([0-9]{2})\/([0-9]{4})\/$/', $subject) > 0 && $allow[1])
                return true;
            // проверка на дату "yyyy"
            if ($this->pregMatch('/^\/([0-9]{4})\/$/', $subject) > 0 && $allow[2])
                return true;

        return false;
    }

    /**
     * Проверка сегмента URL на валидность
     * 
     * @param  string $name название сегмента (/path, /path/path1, ...)
     * @param  integer $check тип проверки
     * @param  integer $size количество сегментов
     * @return boolean
     */
    public function match($name, $check = 'equal', $size = 0)
    {
        if ($size)
            return $this->is($name, $check) && $this->getSegSize() == $size;
        else
            return $this->is($name, $check);
    }

    /**
     * Проверка сегмента URL на валидность
     * 
     * @param  string $name название сегмента (/path, /path/path1, ...)
     * @param  integer $check тип проверки
     * @return boolean
     */
    public function is($name, $check = 'equal')
    {
        switch ($check) {
            // если сегмент это файл
            case 'one path': return empty($this->filename) && $this->getSegSize() == 1;

            // если указан путь и может быть файл
            case 'path&file': return $name == $this->pathOnly && $this->filename;

            // если сегмент это файл
            case 'file': return $name == $this->filename;

            // если сегмент равный части URL
            case 'equal': return $name == $this->path;

            // если сегмент вначале URL
            case 'first':
                if ($this->_segments)
                    return $this->_segments[0] == $name;
                else
                    return false;

            // если сегмент это часть URL
            case 'part':
                $p = strpos($this->path, $name);
                return $p !== false && $p > 0;

            // если сегмент вконце URL
            case 'last':
                if ($this->_segments)
                    return $this->_segments[size($this->_segments) - 1] == $name;
                else
                    return false;

            default:
                return false;
        }
    }

    /**
     * Можно ли кэшировать запрос пользователя
     * (кэширование только GET запросов)
     *
     * @return boolean
     */
    public function isCaching()
    {
        return empty($_POST);
    }

    /**
     * Возращает колич-о сегментов
     * 
     * @return integer
     */
    public function getSegSize()
    {
        return sizeof($this->_segments);
    }

    /**
     * Возращает все сегменты
     * 
     * @return array
     */
    public function getSegments()
    {
        return $this->_segments;
    }

    /**
     * Возращает название файла в запросе
     * 
     * @return mixed
     */
    public function getFilename($suffix = true)
    {
        if (!$this->filename) return false;

        if ($suffix)
            return $this->filename;
        else
            return pathinfo($this->filename, PATHINFO_FILENAME);
    }

     /**
     * Проверка существования переменной $name сессии
     * 
     * @param  string $name имя переменной
     * @return boolean
     */
    public function hasVar($name)
    { 
        return isset($this->_vars[$name]);
    }

     /**
     * Есть ли переменные в запросе
     * 
     * @return boolean
     */
    public function hasVars()
    { 
        return sizeof($this->_vars);
    }

     /**
     * Установка значения $value для ключа $name в GET
     * 
     * @param  string $name имя переменной
     * @param  mixed $value значение
     * @return void
     */
    public function setVar($name, $value = null)
    {
        $old = isset($this->_vars[$name]) ? $this->_vars[$name] : null;

        if ($value === null)
            unset($this->_vars[$name]);
        else
            $this->_vars[$name] = $value;
    }

     /**
     * Установка значений ключам в GET
     * 
     * @param  mixed $vars ассоц. массив
     * @return void
     */
    public function setVars($vars)
    {
        foreach($vars as $key => $value)
            $this->setVar($key, $value);
    }

    /**
     * Возращает значение ключа из метода GET, если ключ
     * не найден - возращает значение по умолчанию
     * 
     * @param  string $key ключ в массиве GET
     * @param  string $default значение по умолчанию
     * @return mixed
     */
    public function getVar($name, $default = '')
    {
        if (isset($this->_vars[$name]))
            return $this->_vars[$name];
        else
            return $default;
    }

    /**
     * Возращает переменные ввиде строки запроса
     * 
     * @return string
     */
    public function getQuery()
    {
        $str = '';
        $first = true;
        foreach($this->_vars as $key => $value) {
            if ($first)
                $first = false;
            else
                $str .= '&';
            $str .= $key . '=' . $value;
        }

        return $str;
    }

    /**
     * Возращает все из GET
     * 
     * @param  mixed $exception исключающий массив переменных
     * @param  string $query возратить переменные ввиде строки запроса
     * @return mixed
     */
    public function getVars($exception = false, $query = false)
    {
        if ($exception) {
            $this->remove($exception);
        }
        if ($query)
            $this->getQuery();

        return $this->_vars;
    }

    /**
     * Удаление ключа(ей) из запроса
     * 
     * @param  mixed $vars ключ или массив ключей в запросе
     * @return void
     */
    public function remove($vars)
    {
        if (is_array($vars)) {
            foreach($vars as $index => $value)
                unset($this->_vars[$value]);
        } else
            unset($this->_vars[$vars]);
    }

    /**
     * Проверка метода запроса
     * 
     * @return boolean
     */
    public function isMethod($method)
    {
        return $this->method == $method;
    }
}
?>