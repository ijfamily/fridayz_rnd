<?php
/**
 * Gear CMS
 * 
 * Пакет обработки сессий
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Session.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Класс обработки сессий
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Session.php 2016-01-01 12:00:00 Gear Magic $
 */
class GSession
{
   /**
     * Текущие состояние сессии
     * 'inactive'|'active'|'expired'|'destroyed'|'error'
     * 
     * @var string
     */
    protected $_state = 'inactive';

    /**
     * Конструктор
     * 
     * @return void
     */
    public function __construct()
    {
        // удалить сессиюю если она стартовала из session.auto_start
        if (session_id()) {
            session_unset();
            session_destroy();
        }

        // отключение прозрачной поддержки sid
        ini_set('session.use_trans_sid', '0');
        // позволить принимать Id сессии только из cookies
        ini_set('session.use_only_cookies', '1');

        $this->_state = 'inactive';
    }

    /**
     * Возращает состояние сессии
     * 
     * @return string 'inactive'|'active'|'expired'|'destroyed'|'error'
     */
    public function getState()
    {
        return $this->_state;
    }

    /**
     * Возращает жизнь сессии в минутах
     * 
     * @return integer
     */
    public function getExpire()
    {
        return GFactory::getConfig()->get('SESSION/EXPIRE');
    }
 
     /**
     * Старт сессии
     * 
     * @return mixed если сессия уже стартовала false, иначе true
     */
    public function start()
    {
        if ($this->_state === 'active')
            return false;

        $name = GFactory::getConfig()->getParams('SESSION/NAME');
        session_name($name);
        $this->_state = 'active';

        return session_start();
    }

     /**
     * Рестарт сессии
     * 
     * @return mixed если сессия уже уничтожена false, иначе true
     */
    public function reStart()
    {
        $this->destroy();
        if ($this->_state !== 'destroyed')
            return false;

        session_regenerate_id(true);
        $this->_start();
        $this->_state = 'active';

        return true;
    }

     /**
     * Возращает идентификатор сессии
     * 
     * @return string если сессия уничтожена false, иначе id
     */
    public function getId()
    {
        if ($this->_state === 'destroyed')
            return false;

        return session_id();
    }

     /**
     * Проверка активности сессии
     * 
     * @return string true если активна, иначе - false
     */
    public function isActive()
    {
        return $this->_state == 'active';
    }

     /**
     * Завершить текущий сеанс сессии и сохранить данные сеанса
     * 
     * @return void
     */
    public function close()
    {
        session_write_close();
    }

     /**
     * Удаление переменной $name в сессии
     * 
     * @param string $name имя переменной
     * @param string $namespace пространство имен чтобы не было конфликтов с переменными
     * @return mixed если сессия не активна - false, иначе возращает старое значение переменной
     */
    public function clear($name, $namespace = '')
    {
        if ($this->_state !== 'active')
            return false;

        $value = null;
        if ($namespace) {
            if (isset($_SESSION[$namespace][$name])) {
                $value = $_SESSION[$namespace][$name];
                unset($_SESSION[$namespace][$name]);
            }
        } else {
            if (isset($_SESSION[$name])) {
                $value = $_SESSION[$name];
                unset($_SESSION[$name]);
            }
        }

        return $value;
    }

     /**
     * Уничтожение сессии
     * 
     * @return mixed если сессия уже уничтожена - false, иначе true
     */
    public function destroy()
    {
        if ($this->_state === 'destroyed')
            return true;

        if (isset($_COOKIE[session_name()])) {
            $cfg = GFactory::getConfig();
            $$cfgDomain = $cfg->get('COOKIE/DOMAIN', '');
            $$cfgPath = $cfg->get('COOKIE/PATH', '/');
            setcookie(session_name(), '', time() - 42000, $$cfgPath, $$cfgDomain);
        }

        session_unset();
        session_destroy();
        $this->_state = 'destroyed';

        return true;
    }

     /**
     * Установка значения $value переменной $name сессии
     * 
     * @param  string $name имя переменной
     * @param  mixed $value значение
     * @param  string $namespace пространство имен чтобы не было конфликтов с переменными
     * @return mixed если сессия не активна - false, иначе void
     */
    public function set($name, $value = null, $namespace = '')
    {
        if ($this->_state !== 'active')
            return false;

        if ($namespace)
            $old = isset($_SESSION[$namespace][$name]) ? $_SESSION[$namespace][$name] : null;
        else
            $old = isset($_SESSION[$name]) ? $_SESSION[$name] : null;

        if ($value === null) {
            if ($namespace)
                unset($_SESSION[$namespace][$name]);
            else
                unset($_SESSION[$name]);
        } else {
            if ($namespace)
                $_SESSION[$namespace][$name] = $value;
            else
                $_SESSION[$name] = $value;
        }
    }

     /**
     * Возращает значения переменной $name сессии
     * 
     * @param  string $name имя переменной
     * @param  mixed $default значение поумолчанию если нет переменной $name в сессии
     * @param  string $namespace пространство имен чтобы не было конфликтов с переменными
     * @return mixed если сессия не активна - false, иначе значение переменной
     */
    public function get($name, $default = '', $namespace = '')
    {
        if ($this->_state !== 'active' && $this->_state !== 'expired')
            return null;

        if ($namespace) {
            if (isset($_SESSION[$namespace][$name]))
                return $_SESSION[$namespace][$name];
            else
                return $default;
        } else {
            //echo '========', $name,'==[', $_SESSION[$name], ']';
            if (isset($_SESSION[$name]))
                return $_SESSION[$name];
            else
                return $default;
        }
    }

     /**
     * Проверка существования переменной $name сессии
     * 
     * @param  string $name имя переменной
     * @param  string $namespace пространство имен чтобы не было конфликтов с переменными
     * @return mixed если сессия не активна - null
     */
    public function has($name, $namespace = '')
    {
        if ($this->_state !== 'active')
            return null;

        if ($namespace) 
            return isset($_SESSION[$namespace][$name]);
        else
            return isset($_SESSION[$name]);
    }

     /**
     * Возращает название сессии
     * 
     * @return mixed если сессия уже уничтожена - false, иначе название
     */
    public function getName()
    {
        if ($this->_state === 'destroyed')
            return false;

        return session_name();
    }
}
?>