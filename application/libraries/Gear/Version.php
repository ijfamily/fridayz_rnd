<?php
/**
 * Gear CMS
 * 
 * Пакет версии системы
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Version.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Класс версии системы
 * 
 * @category   Gear
 * @package    Libraries
 * @subpackage GVersion
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Version.php 2016-01-01 12:00:00 Gear Magic $
 */
class GVersion
{
    /**
     * Псевдоним
     */
    public static $alias = '';

    /**
     * Название приложения
     */
    public static $name = 'Gear CMS';

    /**
     * Версия
     */
    public static $version = '3.0';

    /**
     * Дата последнего обновления
     */
    public static $update = '20160101';

    /**
     * Сайт
     */
    public static $site = 'http://gearmagic.ru/';

    /**
     * Возращает номер версии c псевдонимом
     *
     * @return string
     */
    public static function getVersionAlias()
    {
        if (self::$alias)
            return self::$version . ' / ' . self::$alias;
        else
            return self::$version;
    }

    /**
     * Возращает развёрнутую версию системы
     *
     * @return string
     */
    public static function getFullVersion()
    {
        if (empty(self::$alias))
            return self::$version . ' (' . self::$update . ')';
        else
            return self::$version . ' / ' . self::$alias . ' (' . self::$update . ')';
    }

    /**
     * Возращает сокр. название приложения
     *
     * @return string
     */
    public static function getShortName()
    {
        return self::$name . ' ' . self::$version;
    }

    /**
     * Возращает сокр. название приложения
     *
     * @return string
     */
    public static function getPoweredBy()
    {
        return self::$name . '/' . self::$version . ' build/' . self::$update . ' (' . self::$site . ')';
    }
}
?>