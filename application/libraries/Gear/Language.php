<?php
/**
 * Gear CMS
 * 
 * Пакет языка
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Language.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Класс обработки запросов перехода между языками приложения
 * 
 * @category   Gear
 * @package    Libraries
 * @subpackage Language
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Language.php 2016-01-01 12:00:00 Gear Magic $
 */
class GLanguage
{
    /**
     * Сылка на экземпляр класса
     *
     * @var mixed
     */
    protected static $_instance = null;

    /**
     * Префикс языка полученный из 1-о сегмента uri ("en-GB", "ua-UK", ...)
     *
     * @var string
     */
    public $prefix = '';

    /**
     * Префикс языка по умолчанию (из конфига)
     *
     * @var string
     */
    public $prefixDef = '';

    /**
     * Данные о текущем языке (array("id", "alias", "prefix", "prefixDef", "title"))
     *
     * @var array
     */
    protected $_data = array();

    /**
     * Список досупных языков (из конфига)
     *
     * @var array
     */
    protected $_languages = array();

    /**
     * Конструктор
     * 
     * @param  string $prefix префикс языка (1-й сегмент в uri, "en-GB", "ua-UK", ...)
     * @param  string $prefixDef префикс языка по умолчанию (из конфига)
     * @param  array $languages список доступных языков (из конфига)
     * @return void
     */
    public function __construct($prefix = '', $prefixDef = '', $languages = array())
    {
        // префикс по умолчанию
        $this->prefixDef = $prefixDef;
        // если есть список языков
        if ($languages)
            $this->_languages = $languages;
        // устанавливаем язык
        $this->set($prefix);
    }

    /**
     * Возращает указатель на созданный экземпляр класса (если класс не создан, создаст его)
     * 
     * @return mixed
     */
    public static function getInstance($prefix = '', $prefixDef = '', $languages = array())
    {
        if (null === self::$_instance) 
            self::$_instance = new self($prefix, $prefixDef, $languages);

        return self::$_instance;
    }

    /**
     * Возращает значение атрибута $name языка
     * 
     * @param  string $name один из атрибутов выбранного языка
     * @return mixed
     */
    public function get($name)
    {
        if ($name == 'isDefault')
            return $this->_data['prefix'] == $this->_data['prefixDef'];

        if (isset($this->_data[$name]))
            return $this->_data[$name];
        else
            return false;
    }

    /**
     * Устанавливает язык
     * 
     * @param  string $prefix префикс языка ("en-GB", "ua-UK", ...)
     * @return mixed
     */
    public function set($prefix = '')
    {
        try {
            // если выбранный язык существует
            if (isset($this->_languages[$prefix]))
                $this->prefix = $prefix;
            else
                $this->prefix = $this->prefixDef;
            $this->_data = $this->_languages[$this->prefix];
            $this->_data['prefix'] = $prefix;
            $this->_data['prefixDef'] = $this->prefixDef;
            if ($prefix == $this->prefixDef)
                $this->_data['prefixUri'] = '';
            else
                $this->_data['prefixUri'] = '/' . $prefix;
            // путь к языкам
            $path = DOCUMENT_ROOT . PATH_APPLICATION . PATH_LANGUAGE . $this->_data['alias'];
            // если устанавлеваемый язык существует
            $fileName = $path . '/Exception.php';
            if (!file_exists($fileName))
                throw new GException('Сonfiguration error', 'Unable to mount language (%s)', $this->prefix);
            // подключаем языки
            GText::$path = $path . '/';
            GText::add('exception', 'Exception.php');
            GText::add('interface', 'Interface.php');
            GText::add('date', 'Date.php');
        } catch(GException $e) {
            $e->renderException(array(), PATH_TEMPLATE_SYS . 'exception.php');
        }
    }

    /**
     * Устанавливает языки доступные в системе
     * 
     * @param  array $languages список языков
     * @return void
     */
    public function setLanguages($languages)
    {
        $this->_languages = $languages;
    }
}


/**
 * Класс обработки текста языков
 * 
 * @category   Gear
 * @package    Libraries
 * @subpackage Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Language.php 2016-01-01 12:00:00 Gear Magic $
 */
class GText
{
    /**
     * Путь к файлам языков
     *
     * @var string
     */
    public static $path = '';

    /**
     * Массив для хранения текста
     *
     * @var array
     */
    protected static $_data = array();


    /**
     * Возвращает текст
     * 
     * @param  string $str текст ("abcb .. %s ...")
     * @param  array $args массив значений для подстановки вместо "%s"
     * @return string
     */
    public static function formatStr($str, $args)
    {
        if (!sizeof($args)) return $str;

        if (is_array($args))
            array_unshift($args, $str);
        
        if (is_string($args))
            $args = array($str, $args);

        return call_user_func_array('sprintf', $args);
    }

    /**
     * Добавление текст в раздел
     * 
     * @param  string $section название раздела
     * @param  string $filename имя файла
     * @return void
     */
    public static function add($section, $filename)
    {
        self::$_data[$section] = @require(self::$path . $filename);
    }

    /**
     * Добавление текст компонента в раздел
     * {component}/languages/{language}/Text.php
     * 
     * @param  string $section название раздела или каталога с компонентами ("forms", "sliders") 
     * @return void
     */
    public static function addText($section)
    {
        if (isset(self::$_data[$section]))
            return;

        $path = './' . PATH_COMPONENT .  $section . PATH_LANGUAGE . GFactory::getLanguage('alias') . '/Text.php';

        self::$_data[$section] = @require_once($path);
    }

    /**
     * Возращает текст по ключу
     * 
     * @param  string $name ключ
     * @param  string $section название раздела
     * @return mixed
     */
    public static function get($name, $section = '')
    {
        if ($section) {
            if (isset(self::$_data[$section][$name]))
                return self::$_data[$section][$name];
            else
                return false;
        } else {
            if (isset(self::$_data[$name]))
                return self::$_data[$name];
            else
                return false;
        }
    }

    /**
     * Возращает раздел
     * 
     * @param  string $name название раздела
     * @return mixed
     */
    public static function getSection($name)
    {
        if (isset(self::$_data[$name]))
            return self::$_data[$name];
        else
            return false;
    }

    /**
     * Возращает весь текст
     * 
     * @return array
     */
    public static function getAll()
    {
        return self::$_data;
    }

    /**
     * Устанавливает текст по ключу
     * 
     * @param  string $name ключ
     * @param  string $value текст
     * @return void
     */
    public static function set($name, $value)
    {
        self::$_data[$name] = $value;
    }

    /**
     * Проверка существования текста с ключем
     * 
     * @param  string $name ключ
     * @param  string $section название раздела
     * @return boolean
     */
    public static function has($name, $section = '')
    {
        if ($section)
            return isset(self::$_data[$section][$name]);
        else
            return isset(self::$_data[$name]);
    }

    /**
     * Возращает текст из указанного раздела и выполняет подстановку параметров
     * 
     * @param  string $name ключ
     * @param  string $section название раздела
     * @param  mixed $params параметры ("abcd" или array("a", "b", ...))
     * @return string
     */
    public static function _($name, $section = 'exception', $params = '')
    {
        if (($str = self::get($name, $section)) === false)
            $str = $name;

        if ($params)
            return self::formatStr($str, $params);
        else
            return $str;
    }
}


/**
 * Класс вывода сообщений в шаблоне компонента
 * 
 * @category   Gear
 * @package    Libraries
 * @subpackage Message
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Language.php 2016-01-01 12:00:00 Gear Magic $
 */
class GMessage
{
    /**
     * Сообщения компонентов
     *
     * @var array
     */
    public static $msg = array();

    /**
     * Добавление сообщения компонента в список
     * 
     * @param  string $to название компонента
     * @param  string $type тип сообщения ("error", "success")
     * @param  string $msg сообщение
     * @param  string $section название раздела в GText (если раздел не указан GText шаблон использоваться не будет)
     * @param  mixed $params параметры ("abcd" или array("a", "b", ...))
     * @return void
     */
    public static function to($to, $type, $msg, $section = '', $params = '')
    {
        if (!isset(self::$msg[$to]))
            self::$msg[$to] = array();

        if (!isset(self::$msg[$to][$type]))
            self::$msg[$to][$type] = array();

        if (empty($section))
            self::$msg[$to][$type][] = $msg;
        else
            self::$msg[$to][$type][] = GText::_($msg, $section, $params);
    }

    /**
     * Возращает сообщение компонента
     * 
     * @param  string $from название компонента
     * @param  string $type тип сообщения ("error", "success")
     * @param  integer $index порядок в списке (по умолчанию: 0)
     * @return mixed
     */
    public static function get($from, $type, $index = 0)
    {
        if (!isset(self::$msg[$from][$type][$index])) return false;

        return self::$msg[$from][$type][$index];
    }

    /**
     * Возращает все сообщения компонента
     * 
     * @param  string $from название компонента
     * @param  string $type тип сообщения ("error", "success")
     * @return mixed
     */
    public static function getAll($from, $type = '')
    {
        if ($type)
            if (isset(self::$msg[$from][$type]))
                return self::$msg[$from][$type];
            else
                return false;
        else
            if (isset(self::$msg[$from]))
                return self::$msg[$from];
            else
                return false;
    }

    /**
     * Проверка существования сообщения
     * 
     * @param  string $from название компонента
     * @param  string $type тип сообщения ("error", "success")
     * @param  integer $index порядок в списке (по умолчанию: 0)
     * @return boolean
     */
    public static function has($from, $type, $index = 0)
    {
        if (!isset(self::$msg[$from][$type])) return 0;

        return sizeof(self::$msg[$from][$type]) > 0;
    }

    /**
     * Удаления сообщений компонентов
     * 
     * @param  string $from название компонента
     * @param  string $type тип сообщения ("error", "success")
     * @return void
     */
    public static function remove($from, $type = '')
    {
        if ($type)
            if (isset(self::$msg[$from][$type]))
                unset(self::$msg[$from][$type]);
            else
                return false;
        else
            if (isset(self::$msg[$from]))
                unset(self::$msg[$from]);
            else
                return false;
    }

    /**
     * Удаления всех сообщений компонентов
     * 
     * @return void
     */
    public static function clear()
    {
        self::$msg = array();
    }
}
?>