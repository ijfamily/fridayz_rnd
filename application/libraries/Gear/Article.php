<?php
/**
 * Gear CMS
 * 
 * Пакет отбработки статей сайта
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Article.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Класс статьи
 * 
 * @category   Gear
 * @package    Libraries
 * @subpackage Articles
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Article.php 2016-01-01 12:00:00 Gear Magic $
 */
class GArticles
{
    /**
     * Получение URL статьи
     * 
     * @param  integer $typeId (0 - статья, 1 - новость)
     * @param  integer $artId идент. статьи
     * @param  string $artUri uri статьи
     * @param  integer $catId идент. категории
     * @param  string $catUri uri категории
     * @param  boolean $sef если работает ЧПУ
     * @param  boolean $scheme подстановка схемы
     * @return boolean
     */
    public static function genUrl($data, $ln = '', $sef = true, $scheme = true)
    {
        if ($scheme)
            $url = '{chost}' . $ln;
        else
            $url = $ln;
        // если работает ЧПУ
        if ($sef) {
            // если главная страница в категории
            if (self::isIndex($data['article_uri']))
                $artUri = '';
            else
                $artUri = $data['article_uri'];
            // если есть категория
            if (!empty($data['category_uri']))
                $url .= $data['category_uri'];
            // если статическая страница
            if ($data['type_id'] == 1) {
                $url .= $artUri;
            } else 
            // если тип не статическая страница
            if ($artUri)
                $url .= $data['article_id'] . '-' . $artUri;
        } else {
            $url .= '?a=' . $data['article_id'];
        }

        return $url;
    }

    /**
     * Является ли статья главной в категории
     * 
     * @return boolean
     */
    public static function isIndex($url)
    {
        return $url == 'index.html' || $url == 'index.htm' || $url == 'index';
    }

    /**
     * Возращает url статьи
     *
     * @param array $article данные статьи
     * @return string
     */
    public static function getUrl($article)
    {
        if ($article['article_uri'])
            return $article['article_uri'];

        switch ($article['category_id']) {
            // статьи
            case 1: return '/articles/' . $article['article_id'];

            // новости
            case 2: return '/news/' . $article['article_id'];

            default:
                return '';
        }
    }

    /**
     * Возращает статью по ее идент.
     *
     * @param integer $id идент. статьи
     * @param integer $languageId идент. языка
     * @param integer $typeId тип статьи (статья, новость)
     * @param object $query запрос
     * @return array
     */
    public static function getById($id, $languageId, $typeId = 0, $query = null)
    {
        GTimer::start('query article');
        if (is_null($query))
            $query = new GDbQuery();
        $sql = 'SELECT `t`.*, `a`.*, `p`.* '
             . 'FROM `site_pages` `p`, `site_articles` `a` '
             . 'LEFT JOIN `site_templates` `t` USING (`template_id`) '
             . 'WHERE `a`.`article_id`=`p`.`article_id` AND`p`.`language_id`=' . $languageId. ' AND `a`.`article_id`=' . $id;
        // `a`.`published`=1
        // если указан тип статьи
        if ($typeId)
            $sql .= ' AND `a`.`type_id`=' . $typeId;
        if (($data = $query->getRecord($sql)) === false)
            throw new GException('Query error (Internal Server Error)', 'Response from the database: %s', $query->getError(), __CLASS__ . '::' . __FUNCTION__);
        GTimer::stop('query article');

        return $data;
    }

    /**
     * Возращает главную страницу (если ЧПУ не работает)
     *
     * @param integer $languageId идент. языка
     * @param object $query запрос
     * @return array
     */
    public static function getIndex($languageId, $query = null)
    {
        GTimer::start('query article');
        if (is_null($query))
            $query = new GDbQuery();
        $sql = 'SELECT `t`.*, `a`.*, `p`.* '
             . 'FROM `site_pages` `p`, `site_articles` `a` '
             . 'LEFT JOIN `site_templates` `t` USING (`template_id`) '
             . 'WHERE `a`.`article_id`=`p`.`article_id` AND`p`.`language_id`=' . $languageId. " AND `a`.`article_uri`='index.html' AND `a`.`category_id` IS NULL";
        //AND `a`.`published`=1
        if (($data = $query->getRecord($sql)) === false)
            throw new GException('Query error (Internal Server Error)', 'Response from the database: %s', $query->getError(), __CLASS__ . '::' . __FUNCTION__);
        GTimer::stop('query article');

        return $data;
    }

    /**
     * Возращает статью по ее url
     *
     * @param integer $url ресурс статьи
     * @param integer $languageId идент. языка
     * @param object $query запрос
     * @return array
     */
    public static function getByUrl($url, $languageId, $category = 0, $query = null)
    {
        // если при запросе к категории она не была найдена
        if ($category === false) return array();

        if (is_object($url)) {
            $filename = $url->filename;
            $path = $url->pathOnly;
            $request = $url->path;
            $id = $url->id;
        } else {
            $filename = '';
            $path = $url;
            $request = $url;
            $id = 0;
        }
        if (empty($filename))
            $filename = 'index.html';
        else
            if ($filename == 'index.html')
                $filename = '';
        if ($request) {
            if ($request[0] == '/')
                $request = substr($request, 1);
        }
        $categoryId = 0;
        if (is_array($category) &&  !empty($category)) {
            $categoryId = $category['category_id'];
            $categoryUri = $category['category_uri']; // для проверки исключения "/"
        } else {
            $categoryId = $category;
            $categoryUri = '';  // для проверки исключения "/"
        }

        GTimer::start('query article');
        if (is_null($query))
            $query = new GDbQuery();
        $sql = 'SELECT `t`.*, `a`.*, `p`.* FROM `site_pages` `p`, `site_articles` `a` '
             . 'LEFT JOIN `site_templates` `t` USING (`template_id`) '
             . 'WHERE `a`.`article_id`=`p`.`article_id` AND `p`.`language_id`=' . $languageId;
        //AND `a`.`published`=1
        // если указан идент.статьи
        if ($id)
            $sql .= ' AND `a`.`article_id`=' . $id;
        else
            $sql .= ' AND `a`.`article_uri`=' . $query->escapeStr($filename);
        // если есть категория
        if ($categoryId) {
            // проверка исключения, если url категории = "/"
            if ($categoryUri == '/')
                $sql .= " AND (`a`.`category_id`='$categoryId' OR `a`.`category_id` IS NULL)";
            else
                $sql .= " AND `a`.`category_id`='$categoryId'";
        } else
            $sql .= " AND `a`.`category_id` IS NULL";
        $sql .= ' LIMIT 1';
        if (($data = $query->getRecord($sql)) === false) {
            throw new GException('Query error (Internal Server Error)', 'Response from the database: %s', $query->getError(), __CLASS__ . '::' . __FUNCTION__);
        }
        GTimer::stop('query article');
        if (empty($data))
            return array();

        return $data;
    }

    /**
     * Возращает шаблон по названию файла
     *
     * @param string $filename файл шаблона
     * @param object $query запрос
     * @return array
     */
    public static function getTemplate($filename, $query = null)
    {
        GTimer::start('query template');
        if (is_null($query))
            $query = new GDbQuery();
        $sql = 'SELECT * FROM `site_templates` WHERE `template_filename`=\'' . $filename . '\'';
        if (($data = $query->getRecord($sql)) == false)
            return array();
        GTimer::stop('query template');

        return $data;
    }

    /**
     * Возращает шаблон записи статьи
     *
     * @return array
     */
    public static function getDataTemplate($replace = array())
    {
        $tpl = array(
            'article_id'         => 0,
            'article_folder'     => '',
            'category_id'        => 0,
            'access_id'          => 1, 
            'template_id'        => 0,
            'type_id'            => 1,
            'article_form'       => '',
            'article_index'      => 1,
            'article_note'       => '',
            'article_uri'        => '',
            'article_caching'    => 0,
            'article_sitemap'    => 0,
            'article_search'     => 0,
            'article_archive'    => 0,
            'article_print'      => 0,
            'article_rss'        => 0,
            'article_map'        => 0,
            'article_header'     => '',
            'article_landing'    => 0,
            'sitemap_changefreq' => '',
            'sitemap_priority'   => '',
            'published'          => 1,
            'published_main'     => 1,
            'published_feed'     => 1,
            'published_date'     => '',
            'published_time'     => '',
            'published_user'     => '',
            'announce_date'      => '',
            'announce_time'      => ''
        );

        if ($replace)
            return array_merge($tpl, $replace);
        else
            return $tpl;
    }

    /**
     * Возращает блоки лендинга
     *
     * @param string $articleId идент. статьи
     * @param object $query запрос
     * @return array
     */
    public static function getLandingBlocks($articleId, $query = null)
    {
        if (is_null($query))
            $query = new GDbQuery();

        $sql = 'SELECT * FROM `site_landing_pages` WHERE `block_visible`=1 AND `article_id`=' . $articleId . ' ORDER BY `block_index` ASC';
        if ($query->execute($sql) === false) return array();
        $blocks = array();
        while (!$query->eof()) {
            $block = $query->next();
            if (empty($block['block_text'])) continue;
            $blocks[] = $block;
        }

        return $blocks;
    }
}


/**
 * Класс категории статьи
 * 
 * @category   Gear
 * @package    Libraries
 * @subpackage Categories
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Article.php 2016-01-01 12:00:00 Gear Magic $
 */
class GCategories
{
    /**
     * Категория статьи
     *
     * @var mixed (false - категория не была найдена в запросе, array - категория получена из запроса)
     */
    public static $category = false;

    /**
     * Возращает категорию статьи
     *
     * @param array $article данные статьи
     * @return string
     */
    public static function get($url = '', $domainId = 0, $query = null)
    {
        // только для главной страницы, т.к. категория не может быть для главной, но и sql запрос по ней не выполнялся,
        // но если домен на другом сайте и главная страница категория, то запрос выполняется
        if ($url == '/' && $domainId == 0) return array();

        // если возратить категорию созданую ранее по запросу
        if (empty($url)) return self::$category;
        // все катагории начинаются без "/"
        if ($url[0] == '/')
            $url = substr($url, 1);
        GTimer::start('query category');
        if (is_null($query))
            $query = new GDbQuery();
        $sql = 'SELECT * FROM `site_categories` WHERE `domain_id`=' . $domainId . ' AND `category_uri`=' . $query->escapeStr($url) . ' LIMIT 1';
        if ((self::$category = $query->getRecord($sql)) === false) {
            throw new GException('Query error (Internal Server Error)', 'Response from the database: %s', $query->getError(), __CLASS__ . '::' . __FUNCTION__);
        }
        GTimer::stop('query category');
        // запрос не удался
        if (empty(self::$category))
            return false;
        else
            return self::$category;
    }

    /**
     * Возращает категорию по ее id
     *
     * @param integer $id идент. категории
     * @param object $query запрос
     * @return string
     */
    public static function getById($id = 0, $domainId = 0, $query = null)
    {
        if (empty($id)) return self::$category;

        GTimer::start('query category');
        if (is_null($query))
            $query = new GDbQuery();
        $sql = 'SELECT * FROM `site_categories` WHERE `domain_id`=' . $domainId . ' AND `category_id`=' . $id;
        if ((self::$category = $query->getRecord($sql)) === false) {
            throw new GException('Query error (Internal Server Error)', 'Response from the database: %s', $query->getError(), __CLASS__ . '::' . __FUNCTION__);
        }
        GTimer::stop('query category');

        return self::$category;
    }
}
?>