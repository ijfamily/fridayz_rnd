<?php
/**
 * Gear CMS
 *
 * Капча
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Captcha
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru> w3box.ru
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Captcha.php 2016-01-01 16:00:00 Gear Magic $
 * 
 */

/**
 * Класс капчи
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  freeware
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Captcha.php 1.1. 2008-02-08 w3box.ru $
 */
class GCaptcha
{
    /**
     * Hash key
     *
     * @var string
     */
    public $_hashKey = 'my cool captcha';

    /**
     * Symbols count
     *
     * @var integer
     */
    public $count = 5;

    /**
     * Char
     *
     * @var array
     */
    public $char = array('angelMin'    => -10,
                         'angelMax'    => 10,
                         'angelShadow' => 5,
                         'align'       => 40);

    /**
     * Charset
     *
     * @var string
     */
    public $chars = '0123456789';

    /**
     * Image width
     *
     * @var integer
     */
    public $width = 120;

    /**
     * Image height
     *
     * @var integer
     */
    public $height = 55;

    /**
     * Noise
     *
     * @var integer
     */
    public $noise = 8;

    /**
     * Font
     *
     * @var array
     */
    public $font = array('sizeMin' => 42,
                         'sizeMax' => 42,
                         'file'    => 'application/templates/common/fonts/corbel.ttf');

    /**
     * Interval between the start character
     *
     * @var integer
     */
    public $interval = 16;

    /**
     * Validate captcha
     *
     * @return boolean
     */
    public function isValid($code)
    {
        if (empty($code))
            return false;
        if (!isset($_COOKIE['_CPT']))
            return false;

        return md5($code . $this->_hashKey) == $_COOKIE['_CPT'];
    }

    /**
     * Render image
     *
     * @return void
     */
    public function render()
    {
        // first character position in the horizontal
        $start = 5;
        $image = imagecreatetruecolor($this->width, $this->height);
        // background color
        $background_color = imagecolorallocate($image, 255, 255, 255);
        // shadow color
        $font_color = imagecolorallocate($image, 32, 64, 96);
        imagefill($image, 0, 0, $background_color);
        $str = '';
        $num_chars = strlen($this->chars);
        for ($i=0; $i < $this->count; $i++) {
            $char=$this->chars[rand(0, $num_chars-1)];
            $font_size=rand($this->font['sizeMin'], $this->font['sizeMax']);
            $char_angle=rand($this->char['angelMin'], $this->char['angelMax']);
            imagettftext($image, $font_size, $char_angle, $start, $this->char['align'], $font_color, $this->font['file'], $char);
            imagettftext($image, $font_size, $char_angle+$this->char['angelShadow']*(rand(0, 1)*2-1), $start, $this->char['align'], $background_color, $this->font['file'], $char);
            $start+=$this->interval;
            $str.=$char;
        }
        if ($this->noise) {
            for ($i=0; $i < $this->width; $i++) {
                for ($j=0; $j < $this->height; $j++) {
                    $rgb = imagecolorat($image, $i, $j);
                    $r = ($rgb>>16) & 0xFF;
                    $g = ($rgb>>8) & 0xFF;
                    $b = $rgb & 0xFF;
                    $k = rand(-$this->noise, $this->noise);
                    $rn = $r+255*$k/100;
                    $gn = $g+255*$k/100;
                    $bn = $b+255*$k/100;
                    if ($rn<0) $rn=0;
                    if ($gn<0) $gn=0;
                    if ($bn<0) $bn=0;
                    if ($rn>255) $rn=255;
                    if ($gn>255) $gn=255;
                    if ($bn>255) $bn=255;
                    $color = imagecolorallocate($image, $rn, $gn, $bn);
                    imagesetpixel($image, $i, $j , $color);
                }
            }
        }

        setcookie ('_CPT', md5($str . $this->_hashKey), time() + 60);

        if (function_exists("imagepng")) {
            header("Content-type: image/png");
            imagepng($image);
        } else
            if (function_exists("imagegif")) {
                header("Content-type: image/gif");
                imagegif($image);
            } else
                if (function_exists("imagejpeg")) {
                    header("Content-type: image/jpeg");
                    imagejpeg($image);
                }
        imagedestroy($image);
    }
}
?>
