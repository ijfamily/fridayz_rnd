<?php
/**
 * Gear CMS
 *
 * Компонент "Баннера"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Banners.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;


/**
 * Класс баннера (для AJAX)
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Banners.php 2016-01-01 12:00:00 Gear Magic $
 */
class CjBanners extends GComponentAjax
{
    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        // инициализация модели компонента
        $this->model = GFactory::getModel('/Banners', 'jBanners', $this->attributes);
    }

    /**
     * Вывод шаблона
     * 
     * @return void
     */
    public function template()
    {
        $items = $this->model->getItems();
        if (empty($items)) return;
?>
var gearBanners = [
<?php
        $j = 0;
        $jcount = sizeof($items);
        
        foreach ($items as $id => $data) {
            echo '{id: \'', $id, '\', items: [';
            $count = sizeof($data['items']);
            for ($i = 0; $i < $count; $i++) {
                $item = $data['items'][$i];
                switch ($item['type']) {
                    // фоновая реклама
                    case 1: echo "{type: {$item['type']}, exceptions: '{$item['exceptions']}', url: '{$item['url']}', style: '{$item['style']}'}"; break;

                    // баннер
                    case 2: echo "{type: {$item['type']}, exceptions: '{$item['exceptions']}', html: '{$item['html']}'}"; break;

                    // код баннера
                    case 3: echo "{type: {$item['type']}, exceptions: '{$item['exceptions']}', code: '{$item['code']}'}"; break;
                }
                
                if ($i < $count - 1) echo ',';
            }
            echo ']}';
            if ($j < $jcount - 1) echo ',' . _n;
            $j++;
        }
        echo _n;
?>
];
<?php
    }

    /**
     * Вывод данных
     * 
     * @return void
     */
    public function render()
    {
        header('Content-Type: text/javascript');

        $js = $this->getContent();
        // кэширование
        if (Gear::$app->config->get('CACHE')) {
            Gear::$app->cache->generator = 'banners';
            Gear::$app->cache->note = 'component "Banners JS"';
            Gear::$app->cache->update($js);
        }

        echo $js;
    }
}
?>