<?php
/**
 * Gear CMS
 *
 * Компонент "Лента видеозаписей"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2015 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Feed.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CpFeed
 */
Gear::library('/Components/Feed');

/**
 * Компонент ленты видеозаписей
 * 
 * @category   Gear
 * @package    Components
 * @subpackage Feed
 * @copyright  Copyright (c) 2013-2016 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Feed.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpVideoFeed extends CpFeed
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'video_feed.tpl.php';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'video-feed';

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->info(__CLASS__, L_COMPONENT);

        // инициализация модели компонента
        $this->model = GFactory::getModel('/Video/Feed', 'VideoFeed', $this->attributes);
    }

    /**
     * Возращает "open" тег компонента (для режима "конструктора")
     * 
     * @return string
     */
    protected function getDesignTagOpen()
    {
        $_ = GText::getSection($this->get('path'));

        return <<<HTML
        <section role="component" id="{$this->_data['attr']['id']}" class="gear-position-rt">
            <control id="ctrl-{$this->_data['attr']['id']}" title="{$_['setting component']}" role="component control"></control>
            <script type="text/javascript">
                (function(w, n, id) {
                    w[n] = w[n] || [];
                    w[n].push({
                        id: id,
                        className: "{$this->_data['attr']['class']}",
                        templateId: {$this->_data['template-id']},
                        dataId: {$this->_data['data-id']},
                        articleId: {$this->_data['article-id']},
                        pageId: {$this->_data['page-id']},
                        belongTo: "{$this->_data['belong-to']}",
                        menu: {
                            "add": {name: "{$_['add video']}", icon: "add-document", url: "site/video/video/~/profile/interface/"},
                            "list": {name: "{$_['videos']}", icon: "documents", url: "site/video/video/~/grid/interface/"},
                            "property": {name: "{$_['properties']}", icon: "property", url: "site/design/~/component/interface/"}
                        }
                    });
                })(window, "gearComponents", "{$this->_data['attr']['id']}");
            </script>
            <content>
HTML;
    }

    /**
     * Возращает тег управления элементами компонента (для режима "конструктора")
     * 
     * @return string
     */
    protected function getDesignTagControl()
    {
        $_ = GText::getSection($this->get('path'));

        return <<<HTML
            <control id="ctrl-{$this->_data['attr']['id']}-item-%s" title="{$_['setting element']}" role="item control"></control>
            <script type="text/javascript">
                (function(w, n, id) {
                    w[n] = w[n] || [];
                    w[n].push({ id: id, dataId: '%s', menu: { "edit": {name: "{$_['edit']}", icon: "edit", url: "site/albums/albums/~/profile/interface/"} }});
                })(window, "gearComponents", "{$this->_data['attr']['id']}-item-%s");
            </script>
HTML;
    }
}
?>