<?php
/**
 * Gear CMS
 *
 * Компонент "Галерея"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Gallery.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Базовый класс компонента галереи
 * 
 * атрибуты компонента:
 * data-id - идентификатор галереи в бд (1, 2, ...)
 * list-limit- количество записей на странице (5, 10, ...)
 * use-ajax- использование ajax
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Gallery.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpBaseGallery extends GComponentLight
{
    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'gallery';

    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'gallery.tpl.php';

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        Gear::$app->config->load('Galleries');
    }

    /**
     * Добавление JS и CSS объявлений компонента в документ
     *
     * @params object $doc документ
     * @return void
     */
    public function scripts($doc)
    {
        if ($this->useAjax)
            $doc->addScript('jquery.loader.js');
    }

    /**
     * Возращает "open" тег компонента (для режима "конструктора")
     * 
     * @return string
     */
    protected function getDesignTagOpen()
    {
        return <<<HTML
        <section role="component" id="{$this->_data['attr']['id']}" class="gear-position-rt">
            <control id="ctrl-{$this->_data['attr']['id']}" title="{$this->_['setting component']}" role="component control"></control>
            <script type="text/javascript">
                (function(w, n, id) {
                    w[n] = w[n] || [];
                    w[n].push({
                        id: id,
                        className: "{$this->_data['attr']['class']}",
                        templateId: {$this->_data['template-id']},
                        dataId: {$this->_data['data-id']},
                        articleId: {$this->_data['article-id']},
                        pageId: {$this->_data['page-id']},
                        belongTo: "{$this->_data['belong-to']}",
                        menu: {
                            "add": {name: "{$this->_['add image']}", icon: "add-image", url: "site/galleries/images/~/profile/interface/"},
                            "edit": {name: "{$this->_['edit']}", icon: "edit", url: "site/galleries/galleries/~/profile/interface/"},
                            "list": {name: "{$this->_['gallery']}", icon: "galleries", url: "site/galleries/galleries/~/grid/interface/"},
                            "property": {name: "{$this->_['property']}", icon: "property", url: "site/design/~/component/interface/"}
                        }
                    });
                })(window, "gearComponents", "{$this->_data['attr']['id']}");
            </script>
            <content>
HTML;
    }

    /**
     * Возращает тег управления элементами компонента (для режима "конструктора")
     * 
     * @return string
     */
    protected function getDesignTagControl()
    {
        return <<<HTML
            <control id="ctrl-{$this->_data['attr']['id']}-item-%s" title="{$this->_['setting item']}" role="item control"></control>
            <script type="text/javascript">
                (function(w, n, id) {
                    w[n] = w[n] || [];
                    w[n].push({ id: id, dataId: '%s', menu: { "edit": {name: "{$this->_['edit']}", icon: "edit-image", url: "site/galleries/images/~/profile/interface/"} }});
                })(window, "gearComponents", "{$this->_data['attr']['id']}-item-%s");
            </script>
HTML;
    }

    /**
     * Инициализация компонента
     * 
     * @return void
     */
    protected function initialise()
    {
        parent::initialise();
 
        // каталог изображений
        $this->_data['dir'] = Gear::$app->config->getFrom('GALLERIES', 'DIR');
        // элементы списка
        $this->_data['items'] = $this->model->getItems();
        // изображений в списке
        $this->_data['pages'] = $this->model->countPages;
        // количество элементов списка
        $this->_data['count'] = sizeof($this->_data['items']);
        // скрипт списка
        $this->_data['vars'] = $this->model->vars;
        // если используется AJAX, но записей всего на 1-у страницу, то нет смысла его использовать
        $isOnePage = $this->model->countRecords <= $this->model->countItems;
        if ($this->useAjax) {
            $this->useAjax = !$isOnePage;
        }
        // пагинация
        $this->_data['pagination'] = '';
        // положение в списке
        $this->_data['pagination-pos'] = Gear::$app->config->get('LIST/NAVIGATION');
        $this->_data['navigation-title'] = GText::_('Showing records %s to %s of %s', $this->path, 
                                                    array($this->model->recordFrom, $this->model->recordTo, $this->model->countRecords));
        // панель навигации списка
        if ($this->useAjax)
            $this->_data['pagination'] = '';
        else
            if (!$isOnePage)
                $this->_data['pagination'] = $this->model->getPagination();
    }

    /**
     * Валидация данных компонента после их инициализации
     * если false - компонент не выводится
     * 
     * @return boolean
     */
    public function isValidInitialise()
    {
        return !($this->_data['count'] == 0 && !$tpl['design-mode']);
    }
}


/**
 * Базовый класс компонента галереи (для AJAX)
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Gallery.php 2013-12-01 12:00:00 Gear Magic $
 */
class CjGallery extends GComponentAjax
{
    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'gallery';

    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'gallery.tpl.php';

    /**
     * Инициализация компонента
     * 
     * @return void
     */
    protected function initialise()
    {
        parent::initialise();

        // альбомы
        $this->_data['items'] = $this->model->getItems();
        // количество записей в списке
        $this->_data['count'] = sizeof($this->_data['items']);
        // количество элементов списка
        $this->_response['count'] = (int) $this->model->countItems;
        // количество элементов списка
        $this->_response['total'] = (int) $this->model->countRecords;
    }
}
?>