<?php
/**
 * Gear CMS
 *
 * Компонент "Форма"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Form.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Базовый класс формы
 * 
 * атрибуты компонента:
 * waiting-time : время ожидания между запросами в сек. (0 / ...)
 * max-messages : количество сообщений которые может отправить клиент (0 / ...)
 * check-spam : выполнять проверку запроса на SPAM (true / false)
 * send-mail : отправлять на email сообщение после обработки формы (true / false)
 * location : переход по адресу если запрос был успешен
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Form.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpForm extends GComponentLight
{
    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'form';

    /**
     * Подключать скрипты (свойство компонента)
     *
     * @var boolean
     */
    public $useScripts = true;

    /**
     * Атрибуты компонента по умолчанию (если атрибуты не "пришли" из редактора в админке или из 
     * тега компонента)
     *
     * @var array
     */
    public $defAttributes = array(
        'form-name'     => '', // название отображается в столбце "Форма" компонента "Обратная связь"
        'use-ajax'      => false, // Использовать AJAX
        'check-spam'    => false, // Проверка ip адреса пользователя в базе данных на СПАМ
        'check-captcha' => false, // Проверка капчи
        'waiting-time'  => 0, // Время в течении которого будет отправлены сообщения, будет считаться СПАМом
        'max-messages'  => 0, // Количество сообщений которые может отправить пользователь в течении сессии сайта
        'send-sms'      => false, // Отправить SMS сообщение на телефон
        'sms-url'       => '', // Ресурс для отправки sms сообщений
        'send-mail'     => false, // Отправить сообщение на e-mail
        'mail-subject'  => '', // Тема письма приходящего от клиента
        'emails'        => '', // E-mail адрес для отправленных сообщений
        'location'      => '', // При успешной обработке сообщения перейти по указанному адресу
        'msg-success'   => '', // Сообщение пользователю после успешной обработки формы
        'use-scripts'   => false, // Подключить скрипты
        'hidden'        => false, // Скрыть компонент
        'template'      => '' // Шаблон компонента
    );

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // подключать скрипты
        $this->useScripts = $this->get('use-scripts', $this->useScripts);
    }

    /**
     * Добавление JS и CSS объявлений компонента в документ
     *
     * @params object $doc документ
     * @return void
     */
    public function scripts($doc)
    {
        // Подключает Gear JS
        /*
        if ($this->useAjax)
            $doc->addJQueryDeclaration("$('#{$this->id}').validationEngine({ajaxFormValidation: true});");
        else
            $doc->addJQueryDeclaration("$('#{$this->id}').validationEngine();");
        if ($this->useScripts) {
            $doc->addStyleSheet('jquery.validationEngine.css');
            $doc->addScript('validationEngine/jquery.validationEngine-' . GFactory::getLanguage('alias') . '.js');
            $doc->addScript('jquery.validationEngine.min.js');
        }
        */
    }

    /**
     * Возращает "open" тег компонента (для режима "конструктора")
     * 
     * @return string
     */
    protected function getDesignTagOpen()
    {
        return <<<HTML
        <section role="component" id="{$this->_data['attr']['id']}" class="gear-position-rt">
            <control id="ctrl-{$this->_data['attr']['id']}" title="{$this->_['setting component']}" role="component control"></control>
            <script type="text/javascript">
                (function(w, n, id) {
                    w[n] = w[n] || [];
                    w[n].push({
                        id: id,
                        className: "{$this->_data['attr']['class']}",
                        templateId: {$this->_data['template-id']},
                        dataId: {$this->_data['data-id']},
                        articleId: {$this->_data['article-id']},
                        pageId: {$this->_data['page-id']},
                        belongTo: "{$this->_data['belong-to']}",
                        menu: {
                            "property": {name: "{$this->_['properties']}", icon: "property", url: "site/design/~/component/interface/"}
                        }
                    });
                })(window, "gearComponents", "{$this->_data['attr']['id']}");
            </script>
            <content>
HTML;
    }

    /**
     * Инициализация компонента
     * 
     * @return void
     */
    protected function initialise()
    {
        parent::initialise();

        // сообщение
        //$this->_data['message'] = $this->model->getMessage();
        // список полей
        $this->_data['fields'] = $this->model->getFields();
        // форма от которой идет запрос
        $this->_data['target'] = $this->model->target;
        $this->_data['message'] = '';
        // если есть ошибки
        $this->_data['is-success'] = GMessage::has('form', 'success');
        if ($this->_data['is-success'])
            $this->_data['message'] = GMessage::get('form', 'success');
        $this->_data['is-error'] = GMessage::has('form', 'error');
        if ($this->_data['is-error'])
            $this->_data['message'] = GMessage::get('form', 'error');
        // был ли сабмит формы
        $this->_data['is-submit'] = $this->model->isSubmit();
        // url запрос формы
        $this->_data['form-action'] = '';
        // адрес капчи
        if ($this->sef) {
            $router = Gear::$app->config->load('Router');
            if (isset($router['route']['captcha']))
                $this->_data['url-captcha'] = $router['route']['captcha']['value'];
            else
                $this->_data['url-captcha'] = '';
        } else
            $this->_data['url-captcha'] = '?do=captcha';
        // данные формы после сабмита
        $this->_data['form-data'] = array();
        if ($this->_data['is-submit'])
            $this->_data['form-data'] = $this->model->getData();
    }
}
?>