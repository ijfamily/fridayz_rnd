<?php
/**
 * Gear CMS
 *
 * Компонент "Баннер"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2017 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Banner.php 2017-04-21 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see GBanners
 */
Gear::library('/Banners');

/**
 * Компонента списка статей по выбранному тегу
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2017 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Banner.php 2017-04-21 12:00:00 Gear Magic $
 */
class CpBanner extends GComponentLight
{
    /**
     * Вывод данных
     * 
     * @return void
     */
    public function render()
    {
        $id = $this->get('id');
        if (empty($id)) return;

        $items = Gear::from('banners', 'items');
        if (empty($items)) {
            $items = GBanners::getItems(true);
            Gear::to('banners', 'items', $items);
        }

        if (isset($items[$id])) {
            if (isset($items[$id]['items'][0])) {
                $banner = $items[$id]['items'][0];
                echo $banner['code'];
            }
        }
    }
}
?>