<?php
/**
 * Gear CMS
 *
 * Компонент "Форма аторизации пользователя"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: SignIn.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see GUser
 */
Gear::library('/User');

/**
 * @see CpForm
 */
Gear::library('/Components/Form');

/**
 * Форма аторизации пользователя
 * 
 * @category   Gear
 * @package    Components
 * @subpackage Form
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: SignIn.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpFormSignIn extends CpForm
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'form_signin.tpl.php';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'form-signin';

    /**
     * Адаптер соц.сети
     *
     * @var object
     */
    public $social;

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        Gear::$app->config->load('Users');

        $this->set('check-captcha', Gear::$app->config->getFrom('USERS', 'SIGNIN/CAPTCHA'));

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->info(__CLASS__, GText::_('component', 'interface'));

        // инициализация модели компонента
        $this->model = GFactory::getModel('/Form/SignIn', 'FormSignIn', $this->attributes);
        // инициализация модели соц.сети
        if (Gear::$app->config->getFrom('USERS', 'SIGNIN/SOCIAL')) {
            $adapter = Gear::$app->config->getFrom('USERS', 'SOCIAL/ADAPTER');
            $this->social = GFactory::getModel('/Social/' . $adapter, 'Social' . $adapter, $this->attributes);
            $this->social->initialize();
        }
    }

    /**
     * Инициализация компонента
     * 
     * @return void
     */
    protected function initialise()
    {
        parent::initialise();

        // тип авториазции
        $this->_data['signin-type'] = Gear::$app->config->getFrom('USERS', 'SIGNIN/TYPE');
        // разрешить восстановление аккаунта
        $this->_data['remind-password'] = Gear::$app->config->getFrom('USERS', 'RESTORE');
        // социальные сети
        if ($this->social)
            $this->_data['social-icons'] = $this->social->getIcons();
        else
            $this->_data['social-icons'] = array();
    }
}


/**
 * Форма аторизации пользователя (для AJAX)
 * 
 * @category   Gear
 * @package    Components
 * @subpackage Form
 * @copyright  Copyright (c) 2014-2015 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: SignIn.php 2016-01-01 12:00:00 Gear Magic $
 */
class CjFormSignIn extends GComponentAjax
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'form_signin_popup.tpl.php';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'form-signin';

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        Gear::$app->config->load('Users');

        $this->set('check-captcha', Gear::$app->config->getFrom('USERS', 'SIGNIN/CAPTCHA'));

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->info(__CLASS__, GText::_('component', 'interface'));

        // инициализация модели компонента
        $this->model = GFactory::getModel('/Form/SignIn', 'jFormSignIn', $attr);
    }

    /**
     * Вывод данных
     * 
     * @return void
     */
    public function render()
    {
        // если ошибка в сабмите формы
        if (GMessage::has('form', 'error')) {
            $this->_response['success'] = false;
            $this->_response['message'] = GMessage::get('form', 'error');
        } else {
            $this->initialise();
            /*
            // если контент компонента прошел валидацию
            if (($content = $this->getContent()) === false)
                $this->_response['success'] = false;
            else
                $this->_response['html'] = $content;
            */
            if (GMessage::has('form', 'success')) {
                $this->_response['success'] = true;
                $this->_response['message'] = GMessage::get('form', 'success');
            } else
                $this->_response['redirect'] = '/';
        }
        // вывод данных компонента
        die(json_encode($this->_response));
    }
}
?>