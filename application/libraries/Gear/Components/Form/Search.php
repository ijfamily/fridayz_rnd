<?php
/**
 * Gear CMS
 *
 * Компонент "Форма поиска статей"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Search.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CpForm
 */
Gear::library('/Components/Form');

/**
 * Форма поиска статей
 * 
 * @category   Gear
 * @package    Components
 * @subpackage Form
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Search.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpFormSearchArticle extends CpForm
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'form_search_article.tpl.php';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'form-search-article';

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->info(__CLASS__, GText::_('component', 'interface'));

        // инициализация модели компонента
        $this->model = GFactory::getModel('/Form/Search', 'FormSearchArticle', $this->attributes);
        
    }

    /**
     * Инициализация компонента
     * 
     * @return void
     */
    protected function initialise()
    {
        parent::initialise();

        // что искали
        $this->_data['search'] = '';
        // элементы списка
        $this->_data['items'] = array();
        // страниц в списке
        $this->_data['pages'] = 0;
        // количество элементов списка
        $this->_data['count'] = 0;
        // пагинация
        $this->_data['pagination'] = $this->_data['navigation-title'] = '';

        // если есть строка запроса
        if ($search = $this->model->getSearch()) {
            $list = GFactory::getModel('/Form/Search', 'ListSearchArticle', $this->attributes);
            $list->setSearch($search);

            // что искали
            $this->_data['search'] = $search;
            // элементы списка
            $this->_data['items'] = $list->getItems();
            // страниц в списке
            $this->_data['pages'] = $list->countPages;
            // количество элементов списка
            $this->_data['count'] = sizeof($this->_data['items']);
            // положение в списке
            $this->_data['pagination-pos'] = Gear::$app->config->get('LIST/NAVIGATION');
            $this->_data['navigation-title'] = GText::_('Showing records %s to %s of %s', 'list', array($list->recordFrom, $list->recordTo, $list->countRecords));
        }
    }
}
?>