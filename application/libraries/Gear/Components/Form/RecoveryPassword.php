<?php
/**
 * Gear CMS
 *
 * Компонент "Форма восcтановления аккаунта пользователя"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: RecoveryPassword.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CpForm
 */
Gear::library('/Components/Form');

/**
 * Форма восcтановления аккаунта пользователя
 * 
 * @category   Gear
 * @package    Components
 * @subpackage Form
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: RecoveryPassword.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpFormRecoveryPassword extends CpForm
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'form_recovery_password.tpl.php';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'form-recoverypassword';

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->info(__CLASS__, GText::_('component', 'interface'));

        // инициализация модели компонента
        $this->model = GFactory::getModel('/Form/RecoveryPassword', 'FormRecoveryPassword', $this->attributes);
    }

    /**
     * Инициализация компонента
     * 
     * @return void
     */
    protected function initialise()
    {
        parent::initialise();

        $this->_data['hash'] = $this->model->hash;
    }
}
?>