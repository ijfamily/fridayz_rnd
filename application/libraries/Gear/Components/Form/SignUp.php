<?php
/**
 * Gear CMS
 *
 * Компонент "Форма регистрации пользователя"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: SignUp.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CpForm
 */
Gear::library('/Components/Form');

/**
 * Форма регистрации пользователя
 * 
 * @category   Gear
 * @package    Components
 * @subpackage Form
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: SignUp.php  2016-01-01 12:00:00 Gear Magic $
 */
class CpFormSignUp extends CpForm
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'form_signup.tpl.php';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'form-signup';

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->info(__CLASS__, GText::_('component', 'interface'));

        // инициализация модели компонента
        $this->model = GFactory::getModel('/Form/SignUp', 'FormSignUp', $this->attributes);
    }

    /**
     * Инициализация компонента
     * 
     * @return void
     */
    protected function initialise()
    {
        parent::initialise();

        // выводить правила сайта при регистрации
        if (Gear::$app->config->getFrom('USERS', 'SIGNUP/RULES'))
            $this->_data['rules'] = Gear::content('site_rules.tpl');
        else
            $this->_data['rules'] = '';
        $this->_data['is-confirm'] = Gear::$app->config->getFrom('USERS', 'SIGNUP/CONFIRM');
    }
}


/**
 * Форма регистрации пользователя
 * 
 * @category   Gear
 * @package    Components
 * @subpackage Form
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: SignUp.php  2016-01-01 12:00:00 Gear Magic $
 */
class CpFormSignUpConfirm extends GComponentLight
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'form_signup_confirm.tpl.php';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'form-signup-confirm';

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->info(__CLASS__, GText::_('component', 'interface'));

        // инициализация модели компонента
        $this->model = GFactory::getModel('/Form/SignUp', 'FormSignUpConfirm', $this->attributes);
    }

    /**
     * Инициализация компонента
     * 
     * @return void
     */
    protected function initialise()
    {
        parent::initialise();

        // сообщение
        $this->_data['is-confirm'] = $this->model->checkConfirm();
    }
}
?>