<?php
/**
 * Gear CMS
 *
 * Компонент "Форма обратной связи"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Feedback.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CpForm
 */
Gear::library('/Components/Form');

/**
 * Форма обратной связи
 * 
 * @category   Gear
 * @package    Components
 * @subpackage Form
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Feedback.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpFormFeedback extends CpForm
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'form_feedback.tpl.php';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'form-feedback';

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->info(__CLASS__, GText::_('component', 'interface'));

        // инициализация модели компонента
        $this->model = GFactory::getModel('/Form/Feedback', 'FormFeedback', $this->attributes);
    }

    /**
     * Инициализация компонента
     * 
     * @return void
     */
    protected function initialise()
    {
        parent::initialise();

        // работает ли ЧПУ
        if ($this->sef) {
            if ($this->useAjax)
                $this->_data['form-action'] = '/request/feedback/';
        } else
            if ($this->useAjax)
                $this->_data['form-action'] = '?request=feedback';

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->log($this->_data, GText::_('at component template', 'interface'));
    }
}


/**
 * Форма обратной связи (для AJAX)
 * 
 * @category   Gear
 * @package    Components
 * @subpackage Form
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Feedback.php 2016-01-01 12:00:00 Gear Magic $
 */
class CjFormFeedback extends GComponentAjax
{
    /**
     * Конструктор
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->info(__CLASS__, GText::_('component', 'interface'));

        // инициализация модели компонента
        $this->model = GFactory::getModel('/Form/Feedback', 'jFormFeedback', $this->attributes);
    }
}
?>