<?php
/**
 * Gear CMS
 *
 * Компонент "Виджет карты Yandex"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: YandexMap.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CpWidget
 */
Gear::library('/Components/Widget');

/**
 * Компонент виджета карты Yandex
 * 
 * @category   Gear
 * @package    Components
 * @subpackage Widget
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: YandexMap.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpWidgetYandexMap extends CpWidget
{
    /**
     * Ширина карты
     *
     * @var integer
     */
    public $width = 400;

    /**
     * Высота карты
     *
     * @var integer
     */
    public $height = 400;

    /**
     * Сид карты
     *
     * @var string
     */
    public $sid = '';

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->info(__CLASS__, GText::_('component', 'interface'));

        // ширина карты
        $this->width = $this->get('width', $this->width);
        // высота карты
        $this->height = $this->get('height', $this->height);
        // сид карты
        $this->sid = $this->get('sid', $this->sid);
    }

    /**
     * Вывод данных компонента
     * 
     * @return void
     */
    protected function content()
    {
        echo '<script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=', $this->sid , '&amp;width=', $this->width, '&amp;height=', $this->height, '"></script>';
    }
}
?>