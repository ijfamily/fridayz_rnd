<?php
/**
 * Gear CMS
 *
 * Компонент "Виджет социальной сети Facebook"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Facebook.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CpWidget
 */
Gear::library('/Components/Widget');

/**
 * Компонент виджета социальной сети Facebook
 * 
 * @category   Gear
 * @package    Components
 * @subpackage Widget
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Facebook.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpWidgetFB extends CpWidget
{
    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'widget-fb';

    /**
     * Вывод данных
     * 
     * @return void
     */
    public function render()
    {
?>
<div id="fb-root"></div>
<script>(function(d, s, id){var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) return;js = d.createElement(s); js.id = id;js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script>
<div class="fb-like-box" <?print $this->getOptions();?>></div>
<?php
    }
}
?>