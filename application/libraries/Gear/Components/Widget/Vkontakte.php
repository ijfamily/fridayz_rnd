<?php
/**
 * Gear CMS
 *
 * Компонент "Виджет социальной сети ВКонтакте"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Vkontakte.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CpWidget
 */
Gear::library('/Components/Widget');

/**
 * Компонент виджета социальной сети ВКонтакте
 * 
 * @category   Gear
 * @package    Components
 * @subpackage Widget
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Vkontakte.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpWidgetVK extends CpWidget
{
    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'widget-vk';

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента (включая $params)
     * @params array $params атрибуты компонента ("params{...}") установленные в редакторе
     * @return void
     */
    public function __construct($attr = array(), $params = array())
    {
        parent::__construct($attr, $params);

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->info(__CLASS__, GText::_('component', 'interface'));

        unset($this->params['group']);
        unset($this->params['container']);
        // добавление JS объявления
        Gear::$app->document->addScript('http://vk.com/js/api/openapi.js', 'text/javascript', true);
    }

    /**
     * Вывод данных
     * 
     * @return void
     */
    public function render()
    {
        print '<div id="' . $this->get('container') . '"></div>' . "";
        print '<script type="text/javascript">';
        print 'VK.Widgets.Group("' . $this->get('container') . '", ' . json_encode($this->params) . ', ' . $this->get('group') . ');';
        print '</script>';
    }
}
?>