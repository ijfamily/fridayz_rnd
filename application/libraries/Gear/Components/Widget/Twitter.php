<?php
/**
 * Gear CMS
 *
 * Компонент "Виджет социальной сети Twitter"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Twitter.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CpWidget
 */
Gear::library('/Components/Widget');

/**
 * Компонент виджета социальной сети Twitter
 * 
 * @category   Gear
 * @package    Components
 * @subpackage Widget
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Twitter.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpWidgetTwitter extends CpWidget
{
    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'widget-twitter';

    /**
     * Вывод данных
     * 
     * @return void
     */
    public function render()
    {
?>
<a class="twitter-timeline" <?php print $this->getOptions();?> href="https://twitter.com/twitterapi">Твиты пользователя @twitter</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
<?php
    }
}
?>