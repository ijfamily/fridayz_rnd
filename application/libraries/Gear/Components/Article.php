<?php
/**
 * Gear CMS
 *
 * Компонент "Статья"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Article.php 2016-01-01 21:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Компонент статьи
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Article.php 2016-01-01 21:00:00 Gear Magic $
 */
class CpArticle extends GComponentLight
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'article.tpl.php';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'article';

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента 
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->info(__CLASS__, GText::_('component', 'interface'));
    }

    /**
     * Возращает "open" тег компонента (для режима "конструктора")
     * 
     * @return string
     */
    protected function getDesignTagOpen()
    {
        return <<<HTML
        <section role="component" id="{$this->_data['attr']['id']}" class="gear-position-lt">
            <control id="ctrl-{$this->_data['attr']['id']}" title="{$this->_['setting component']}" role="component control"></control>
            <script type="text/javascript">
                (function(w, n, id) {
                    w[n] = w[n] || [];
                    w[n].push({
                        id: id,
                        className: "{$this->_data['attr']['class']}",
                        templateId: {$this->_data['template-id']},
                        dataId: {$this->_data['article-id']},
                        articleId: {$this->_data['article-id']},
                        pageId: {$this->_data['page-id']},
                        belongTo: "{$this->_data['belong-to']}",
                        menu: {
                            "add": {name: "{$this->_['add article']}", icon: "add-document", url: "site/articles/articles/~/profile/interface/"},
                            "edit": {name: "{$this->_['edit']}", icon: "edit", url: "site/articles/articles/~/profile/interface/"},
                            "list": {name: "{$this->_['articles']}", icon: "documents", url: "site/articles/articles/~/grid/interface/"},
                            "property": {name: "{$this->_['property']}", icon: "property", url: "site/design/~/component/interface/"}
                        }
                    });
                })(window, "gearComponents", "{$this->_data['attr']['id']}");
            </script>
            <content>
HTML;
    }

    /**
     * Инициализация компонента
     * 
     * @return void
     */
    protected function initialise()
    {
        parent::initialise();

        $pdata = Gear::$app->document->getData();

        // если есть шаблон в статье
        if (Gear::$app->document->template)
            $this->template = Gear::$app->document->template;
        // дата публикации
        $this->_data['published.date'] = $pdata['published_date'];
        $this->_data['published.time'] = $pdata['published_time'];
        // изображения загаловка статьи
        $this->_data['image'] = $pdata['page_image'];
        // показывать загаловок статьи
        $this->_data['show-header'] = Gear::$app->document->showHeader;
        // загаловок статьи
        $this->_data['header'] = Gear::$app->document->header;
        // текст статьи
        $this->_data['html'] = Gear::$app->document->getRenderText();
        $this->_data['html-widgets'] = Gear::$app->document->getData('page_html_widgets');
        // текст статьи разбитый тегом [break]
        $this->_data['break'] = Gear::$app->document->breakText();
        // доступна статья для печати
        $this->_data['print'] = Gear::$app->document->print;
        // дата правки статьи
        $this->_data['date'] = Gear::$app->document->date;
        // идент. статьи
        $this->_data['id'] = Gear::$app->document->id;
        // если страница пуста
        $this->_data['is-empty'] = Gear::$app->document->isEmpty();
        // если страница лендинга
        $this->_data['is-landing'] = Gear::$app->document->isLanding;
        // блоки на странице лендинга (для правки в режиме конструктора)
        $this->_data['landing-page'] = Gear::$app->dataLandingPage;

        // если состояние страницы не "печать"
        if (GDocument::$state != 'print')
            // если стятья имеет текст
            if (!Gear::$app->document->isEmpty())
                // если в админке выставлено "печать"
                if (Gear::$app->document->print)
                    $this->_data['print'] = GFactory::getLanguage('prefixUri') . '/print/?url=' . Gear::$app->url->path;

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->log($this->_data, GText::_('at component template', 'interface'));
    }

    /**
     * Валидация компонента перед выводом данных, если компонент не прошел
     * валидацию - данные компонента не выводятся
     * 
     * @return boolean
     */
    public function isValid()
    {
        if (Gear::$app->document->isOk()) {
            // если главная страница и нет флажка "Опубликовать статью на главной"
            if (Gear::$app->url->isRoot() && !Gear::$app->dataPage['published_main'])
                return false;
        } else
            return false;

        return parent::isValid();
    }
}


/**
 * Компонент схема расположения страниц
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Article.php 2015-07-01 21:00:00 Gear Magic $
 */
class CpArticleSchema extends GComponent
{
    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        $this->template = Gear::$app->document->schema;
    }
}
?>