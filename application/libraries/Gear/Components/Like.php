<?php
/**
 * Gear CMS
 *
 * Компонент "Лайк материала"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Like.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Компонент лайк материала (для AJAX запроса)
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Like.php 2016-01-01 12:00:00 Gear Magic $
 */
class CjLike extends GComponentAjax
{
    protected $votes = 0;
    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // инициализация модели компонента
        $this->model = GFactory::getModel('/Like', 'jLike', $attr);
    }

    /**
     * Валидация компонента перед выводом данных, если компонент не прошел
     * валидацию - данные компонента не выводятся
     * 
     * @return boolean
     */
    public function isValid()
    {
        // вид запроса
        $type = $this->input->get('type', '');
        if (!$this->model->isExistType($type))
            return $this->_['Wrong kind of request is addressed'];
        // идент. записи лайка
        $id = $this->model->formatId($this->input->get('id', 0));
        if (empty($id))
            return $this->_['Incorrect request id'];

        $sess = GFactory::getSession();
        $sess->start();
        $slikes = $sess->get('like', false);
        if ($slikes) {
            if (isset($slikes[$type . $id]))
                return $this->_['You have already voted'];
        }

        // проверка материала лайка
        $likes = $this->model->getLike($type, $id);
        if ($likes === false)
            return /*$this->_['Unable to determine the rating']*/true;
        $this->votes = (int) $likes['votes'] + 1;
        $data = array(
            'likes_votes' => $this->votes
        );
        // обновить лайк
        if (!$this->model->updateLike($type, $data, $id))
            return $this->_['Unable to update the like'];
        // если лайка еще нет
        if ($slikes)
           $slikes[$type . $id] = true;
        else
             $slikes = array($type . $id => true);
        // обновить лайки в сессии
        $sess->set('like', $slikes);

        return true;
    }

    /**
     * Вывод данных
     * 
     * @return void
     */
    public function render()
    {
        // валидация компонента
        if (($res = $this->isValid()) !== true) {
            $this->_response['success'] = false;
            $this->_response['html'] = $res;
        } else
            $this->_response['votes'] = $this->votes;
        // вывод данных компонента
        die(json_encode($this->_response));
    }
}
?>