<?php
/**
 * Gear CMS
 *
 * Компонент "Карта сайта"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Sitemap.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Компонент карты сайта
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Sitemap.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpSitemap extends GComponentRest
{
    /**
     * Конструктор
     *
     * @return void
     */
    public function __construct()
    {
        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->info(__CLASS__, GText::_('component', 'interface'));

        // инициализация модели компонента
        $this->model = GFactory::getModel('/Sitemap', 'Sitemap');
    }

    /**
     * Вывод данных
     * 
     * @return void
     */
    public function render()
    {
        header("Content-Type:text/xml");

        ob_start();
        $this->model->createXML();
        $xml = ob_get_contents();
        ob_clean();

        // кэширование
        if (Gear::$app->config->get('SITEMAP/CACHING')) {
            Gear::$app->cache->generator = 'sitemap';
            Gear::$app->cache->note = 'component "Sitemap"';
            Gear::$app->cache->update($xml);
        }

        echo $xml;
    }
}
?>