<?php
/**
 * Gear CMS
 *
 * Компонент "Лента"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2015 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Feed.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Базовый класс компонента ленты
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Feed.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpFeed extends GComponentLight
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'feed.tpl.php';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'feed';

    /**
     * Загаловок ленты
     *
     * @var string
     */
    public $title = '';

    /**
     * Возращает "open" тег компонента (для режима "конструктора")
     * 
     * @return string
     */
    protected function getDesignTagOpen()
    {
        return <<<HTML
        <section role="component" id="{$this->_data['attr']['id']}" class="gear-position-rt">
            <control id="ctrl-{$this->_data['attr']['id']}" title="{$this->_['setting component']}" role="component control"></control>
            <script type="text/javascript">
                (function(w, n, id) {
                    w[n] = w[n] || [];
                    w[n].push({
                        id: id,
                        className: "{$this->_data['attr']['class']}",
                        templateId: {$this->_data['template-id']},
                        dataId: {$this->_data['data-id']},
                        articleId: {$this->_data['article-id']},
                        pageId: {$this->_data['page-id']},
                        belongTo: "{$this->_data['belong-to']}",
                        menu: {
                            "add": {name: "{$this->_['add article']}", icon: "add-document", url: "site/articles/articles/~/profile/interface/"},
                            "edit": {name: "{$this->_['edit']}", icon: "edit", url: "site/articles/articles/~/profile/interface/"},
                            "list": {name: "{$this->_['articles']}", icon: "documents", url: "site/articles/articles/~/grid/interface/"},
                            "property": {name: "{$this->_['properties']}", icon: "property", url: "site/design/~/component/interface/"}
                        }
                    });
                })(window, "gearComponents", "{$this->_data['attr']['id']}");
            </script>
            <content>
HTML;
    }

    /**
     * Возращает тег управления элементами компонента (для режима "конструктора")
     * 
     * @return string
     */
    protected function getDesignTagControl()
    {
        return <<<HTML
            <control id="ctrl-{$this->_data['attr']['id']}-item-%s" title="{$this->_['setting element']}" role="item control"></control>
            <script type="text/javascript">
                (function(w, n, id) {
                    w[n] = w[n] || [];
                    w[n].push({ id: id, dataId: '%s', menu: { "edit": {name: "{$this->_['edit']}", icon: "edit", url: "site/articles/articles/~/profile/interface/"} }});
                })(window, "gearComponents", "{$this->_data['attr']['id']}-item-%s");
            </script>
HTML;
    }

    /**
     * Инициализация компонента
     * 
     * @return void
     */
    protected function initialise()
    {
        parent::initialise();

        // элементы ленты
        $this->_data['items'] = $this->model->getItems();
        // количество записей в списке
        $this->_data['count'] = sizeof($this->_data['items']);
        // загаловок ленты
        $this->_data['title'] = $this->get('title', $this->title);

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->log($this->_data, GText::_('at component template', 'interface'));
    }

    /**
     * Валидация данных компонента после их инициализации
     * если false - компонент не выводится
     * 
     * @return boolean
     */
    public function isValidInitialise()
    {
        return !($this->_data['count'] == 0 && !$this->_data['design-mode']);
    }
}
?>