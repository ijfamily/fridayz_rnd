<?php
/**
 * Gear CMS
 *
 * Компонент "Список"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: List.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Базовый класс компонента списка
 * 
 * атрибуты компонента:
 * list-page : текущая страница списка (1, 2, ...)
 * list-limit : количество записей на странице (5, 10, ...)
 * list-field : псевдоним сортируемого поля ("field1", ...)
 * list-order : вид сортировик ("a" -> "по возрастанию" или "d" -> "по убыванию")
 * use-ajax : использовать AJAX (true / false)
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: List.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpList extends GComponentLight
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'list.tpl.php';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'list';

    /**
     * Возращает "open" тег компонента (для режима "конструктора")
     * 
     * @return string
     */
    protected function getDesignTagOpen()
    {
        return <<<HTML
        <section role="component" id="{$this->_data['attr']['id']}" class="gear-position-rt">
            <control id="ctrl-{$this->_data['attr']['id']}" title="{$this->_['setting component']}" role="component control"></control>
            <script type="text/javascript">
                (function(w, n, id) {
                    w[n] = w[n] || [];
                    w[n].push({
                        id: id,
                        className: "{$this->_data['attr']['class']}",
                        templateId: {$this->_data['template-id']},
                        dataId: {$this->_data['data-id']},
                        articleId: {$this->_data['article-id']},
                        pageId: {$this->_data['page-id']},
                        belongTo: "{$this->_data['belong-to']}",
                        menu: {
                            "add": {name: "{$this->_['add article']}", icon: "add-document", url: "site/articles/articles/~/profile/interface/"},
                            "edit": {name: "{$this->_['edit']}", icon: "edit", url: "site/articles/articles/~/profile/interface/"},
                            "list": {name: "{$this->_['articles']}", icon: "documents", url: "site/articles/articles/~/grid/interface/"},
                            "property": {name: "{$this->_['property']}", icon: "property", url: "site/design/~/component/interface/"}
                        }
                    });
                })(window, "gearComponents", "{$this->_data['attr']['id']}");
            </script>
            <content>
HTML;
    }

    /**
     * Возращает тег управления элементами компонента (для режима "конструктора")
     * 
     * @return string
     */
    protected function getDesignTagControl()
    {
        return <<<HTML
            <control id="ctrl-{$this->_data['attr']['id']}-item-%s" title="{$this->_['setting item']}" role="item control"></control>
            <script type="text/javascript">
                (function(w, n, id) {
                    w[n] = w[n] || [];
                    w[n].push({ id: id, dataId: '%s', menu: { "edit": {name: "{$this->_['edit']}", icon: "edit-image", url: "site/articles/articles/~/profile/interface/"} }});
                })(window, "gearComponents", "{$this->_data['attr']['id']}-item-%s");
            </script>
HTML;
    }

    /**
     * Инициализация компонента
     * 
     * @return void
     */
    protected function initialise()
    {
        parent::initialise();

        // если список в статье с категорией
        if (isset(Gear::$app->category['list_calendar'])) {
            // если в настройках и в категории выставлено показать календарь
            $this->_data['use-calendar'] = Gear::$app->config->get('CALENDAR') && Gear::$app->category['list_calendar'];
        } else
            // включить календарь
            $this->_data['use-calendar'] = Gear::$app->config->get('CALENDAR');
        // подсветка дней в календаре
        $this->_data['calendar-highlight'] = Gear::$app->config->get('CALENDAR/HIGHLIGHT');
        // параметр фильтрации списка через календарь
        $this->_data['date-on'] = $this->model->dateOn;
        // элементы списка
        $this->_data['items'] = $this->model->getItems();
        // страниц в списке
        $this->_data['pages'] = $this->model->countPages;
        // количество элементов списка
        $this->_data['count'] = sizeof($this->_data['items']);
        // категория статьи
        if (empty(Gear::$app->category)) {
            $this->_data['category-name'] = '';
            $this->_data['category-id'] = '';
        } else {
            $this->_data['category-name'] = Gear::$app->category['category_name'];
            $this->_data['category-id'] = Gear::$app->category['category_id'];
        }
        // если используется AJAX, но записей всего на 1-у страницу, то нет смысла его использовать
        $isOnePage = $this->model->countRecords <= $this->model->countItems;
        if ($this->useAjax) {
            $this->useAjax = !$isOnePage;
        }
        // пагинация
        $this->_data['pagination'] = '';
        // положение в списке
        $this->_data['pagination-pos'] = Gear::$app->config->get('LIST/NAVIGATION');
        $this->_data['navigation-title'] = GText::_('Showing records %s to %s of %s', $this->get('path'), 
                                                    array($this->model->recordFrom, $this->model->recordTo, $this->model->countRecords));
        // панель навигации списка
        if ($this->useAjax)
            $this->_data['pagination'] = '';
        else
            if (!$isOnePage) {
                $this->_data['pagination'] = $this->model->getPagination();
            }
        // переменные модели компонента
        $this->_data['vars'] = $this->model->vars;

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->log($this->_data, GText::_('at component template', 'interface'));
    }
}


/**
 * Базовый класс компонента списка (для AJAX)
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: List.php 2013-11-10 12:00:00 Gear Magic $
 */
class CjList extends GComponentAjax
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'list.tpl.php';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'list';

    /**
     * Инициализация компонента
     * 
     * @return void
     */
    protected function initialise()
    {
        // элементы списка
        $this->_data['items'] = $this->model->getItems();
        // количество элементов списка
        $this->_data['count'] = (int) $this->model->countItems;
        // количество элементов списка
        $this->_response['count'] = (int) $this->model->countItems;
        // количество записей
        $this->_response['total'] = (int) $this->model->countRecords;
    }
}
?>