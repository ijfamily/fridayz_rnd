<?php
/**
 * Gear CMS
 *
 * Компонент "RSS лента"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Rss.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Компонент RSS ленты
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Rss.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpRss extends GComponentLight
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'rss.tpl.php';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'rss';

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->info(__CLASS__, GText::_('component', 'interface'));

        // инициализация модели компонента
        $this->model = GFactory::getModel('/Rss', 'Rss', $this->attributes);
    }

    /**
     * Инициализация компонента
     * 
     * @return void
     */
    protected function initialise()
    {
        // тема
        $this->_data['theme'] = Gear::$app->config->get('THEME');
        // генератор
        $this->_data['generator'] = GVersion::getPoweredBy();
        // статьи
        $this->_data['articles'] = $this->model->getArticles();

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->log($this->_data, GText::_('at component template', 'interface'));
    }

    /**
     * Вывод данных
     * 
     * @return void
     */
    public function render()
    {
        header("Content-Type: text/xml");

        $xml = $this->getContent();
        // кэширование
        if (Gear::$app->config->get('RSS/CACHING')) {
            Gear::$app->cache->generator = 'rss';
            Gear::$app->cache->note = 'component "Rss"';
            Gear::$app->cache->update($xml);
        }

        echo $xml;
    }
}
?>