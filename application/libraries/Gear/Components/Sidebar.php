<?php
/**
 * Gear CMS
 *
 * Компонент "Боковое меню"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2014-2015 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Sidebar.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Базовый класс компонента бокового меню
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2014-2015 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Sidebar.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpSidebar extends GComponentLight
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'sidebar.tpl.php';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'sidebar';

    /**
     * Инициализация компонента
     * 
     * @return void
     */
    protected function initialise()
    {
        parent::initialise();

        // список элементов
        $this->_data['items'] = $this->model->getItems();

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->log($this->_data, GText::_('at component template', 'interface'));
    }
}
?>