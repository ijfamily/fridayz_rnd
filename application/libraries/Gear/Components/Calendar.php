<?php
/**
 * Gear CMS
 *
 * Компонент "Календарь"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Calendar.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Класс компонента "Календарь" (для AJAX)
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Calendar.php 2016-01-01 12:00:00 Gear Magic $
 */
class CjCalendar extends GComponentAjax
{
    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'calendar';

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        // инициализация модели компонента
        $this->model = GFactory::getModel('/Calendar', 'jCalendar', $this->attributes);
    }

    /**
     * Возращает контент компонента
     * 
     * @return string
     */
    public function getContent()
    {
        return $this->model->getDates();
    }
}
?>