<?php
/**
 * Gear CMS
 *
 * Компонент "Страница сайта"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Page.php 2016-01-01 21:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Компонент страницы сайта
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Page.php 2016-01-01 21:00:00 Gear Magic $
 */
class CpPage extends GComponent
{
    /**
     * Поиск тега в ресурсе (шаблон, текст, файл)
     *
     * @var string
     */
    public $tagSearch = 'component';

    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'index.tpl';

    /**
     * Идинд. шаблона cстраницы (template_id)
     *
     * @var integer
     */
    public $templateId = 0;

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'page';

    /**
     * Атрибуты компонентов полученные из страницы (component_attributes)
     *
     * @var array
     */
    public $componentAttrs = array();

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr = array());

        // установить соединение с сервером
        GFactory::getDb()->connect();

        $tpl = GArticles::getTemplate($this->template);
        // если шаблон существует
        if ($tpl) {
            // определение атрибутов для компонентов в главном шаблоне
            if (!empty($tpl['component_attributes'])) {
                $this->componentAttrs = json_decode($tpl['component_attributes'], true);
            }
            $this->templateId = $tpl['template_id'];
        }
    }

    /**
     * Возращает текст шаблона с определением модели DOM
     * 
     * @return string
     */
    protected function getTemplate()
    {
        $filename = './' . PATH_TEMPLATE . Gear::$app->config->get('THEME') . '/' . GFactory::getLanguage('alias') . '/' . $this->template;
        try {
            if (file_exists($filename)) {
                $this->dom = file_get_html_template($filename, array());
                $this->updateTags();
                return $this->dom;
            } else
                throw new GException('Component error', 'Unable to load template "%s"', $filename);
        } catch(GException $e) {
             $e->renderDialog();
        }
    }

    /**
     * Возращает атрибуты компонента полученные из страницы статьи
     *
     * @return array
     */
    public function getComponentAttrs($componentId, $attr = array())
    {
        if (isset($this->componentAttrs[$componentId])) {
            if ($attr)
                return array_merge($this->componentAttrs[$componentId], $attr);
            else
                return $this->componentAttrs[$componentId];
        }

        return false;
    }

    /**
     * Создание компонента на основе DOM модели
     * 
     * @param  object $tag один из узлов DOM модели
     * @return void
     */
    protected function updateTag($tag)
    {
        // если атрибуты компонента была ранее заменены другим компонентом
        if (($attr = Gear::$app->document->isComponent($tag->attr['class'])) !== false) {
            $tag->attr = $attr;
            // если компонент необходимо скрыть
            if ($attr['hidden']) return;
        }

        try {
            if ($cmp = GComponent::factory($tag->attr, $tag->innertext, 'template')) {
                if (is_object($cmp)) {
                    // инициализация, валидация (если нет данных у компонента здесь указывается 404 ошибка), рендеринг
                    $cmp->render();
                    // если компонент передал документу что нет данных для отображения (возможно в компоненте Article)
                    // и если компонент не статья, а то и она выведит ошибку
                    if ($tag->attr['class'] != 'Article')
                        if (!Gear::$app->document->isOk())
                            Gear::$app->document->renderError();
                } else
                    throw new GException('Component error', $cmp);
            } else
                throw new GException('Component error', 'Can`t connect the component "%s", incorrectly specified path "%s"',
                                     array((isset($tag->attr['class']) ? $tag->attr['class'] : '') . ',' .
                                     (isset($tag->attr['path']) ? $tag->attr['path'] : '')), $this);
        } catch(GException $e) {}
    }

    /**
     * Обработка тега "condition"
     * 
     * @return void
     */
    protected function updateTagCondition($tag)
    {
        $tagName = isset($tag->attr['tag']) ? $tag->attr['tag'] : 'span';
        // атрибут "for" используется для валидации компонента по условию запроса
        $for = isset($tag->attr['for']) ? $tag->attr['for'] : false;
        if ($for) {
            // для сравнения
            $forData = isset($tag->attr['for-data']) ? $tag->attr['for-data'] : false;
            // для отрицания
            $forDataNo = isset($tag->attr['for-data-no']) ? $tag->attr['for-data-no'] : false;
            $show = false;
            if (!empty($forData) || $forDataNo) {
                switch ($for) {
                    // проверка авторизации пользователя
                    case 'user':
                        if ($forData) {
                            if ($forData == 'signin') {
                                $show = GFactory::getAuth()->check();
                                if ($show)
                                    $tag->outertext = GFactory::getAuth()->getUserTemplate($tag->innertext);
                            } else
                            if ($forData == 'no') {
                                $show = !GFactory::getAuth()->check();
                                $tag->outertext = $tag->innertext;
                            }
                        }
                        break;

                    // если указано имя домена или код домена (из настроеек подключения доменов)
                    case 'domain':
                        if ($forData) {
                            if ($forData == 'main')
                                $show = Gear::$app->domainId == 0;
                            else
                            if (is_numeric($forData))
                                $show = Gear::$app->domainId == (int) $forData;
                            else
                                $show = $_SERVER['SERVER_NAME'] == $forData || $forData == 'all';
                        }
                        break;

                    // если указан url ("/folder1/folder2/file.html")
                    case 'url': 
                        if ($forData)
                            $show = Gear::$app->url->path == $forData;
                        else
                        if ($forDataNo)
                            $show = Gear::$app->url->path != $forDataNo;
                        break;

                    // если главная страница
                    case 'page':
                        if ($forData)
                            $show = $forData == 'index' && Gear::$app->url->isRoot();
                        else
                        if ($forDataNo)
                            $show = $forDataNo == 'index' && !Gear::$app->url->isRoot();
                        break;

                    // если дата совпадает
                    case 'date':
                        if ($forData)
                            $show = $forData == date('d-m-Y');
                        else
                        if ($forDataNo)
                            $show = $forDataNo != date('d-m-Y');
                        break;

                    // если главная страница
                    case 'category':
                        if (!empty(Gear::$app->category)) {
                            if ($forData)
                                $show = $forData == Gear::$app->category['category_id'];
                            else
                            if ($forDataNo)
                                $show = $forDataNo != Gear::$app->category['category_id'];
                        } else
                            $show = false;
                        break;
                }
            }
        }

        if ($show) {
            $tag->tag = $tagName;
            $tag->attr = array();
        } else {
            $tag->outertext = '';
        }

        return $show;
    }

    /**
     * Обновление DOM модели на основе созданных в ней компонентов
     * 
     * @return void
     */
    protected function updateTags()
    {
        GTimer::start('page parsing');
        $commonTags = array('Counter' => false, 'Meta' => false, 'StyleSheet' => false, 'StyleDeclaration' => false, 'Script' => false, 'ScriptDeclaration' => false);

        //$tStyleSheet = $tStyleDeclaration = $tScript = $tScriptDeclaration = '';
        if ($this->dom) {
            // поиск тега "condition" (выводит содержимое по условию)
            foreach ($this->dom->find('condition') as $tag) {
                if (!$this->updateTagCondition($tag)) {
                    // удаление тегов компонентов внутри
                    foreach ($tag->find('component') as $e) {
                        $e->tag = 'none';
                        $e->outertext = '';
                    }
                }
            }

            // поиск тега "component" (вывод компонентов страницы)
            $tagMeta = $tagMetaTypes = array();
            foreach ($this->dom->find($this->tagSearch) as $tag) {
                GApplication::$components[] = $tag->class;
 
                if ($tag->parent->tag == 'if')
                    $tag->parent->outertext = '';

                if (isset($commonTags[$tag->attr['class']])) {
                    $commonTags[$tag->attr['class']] = $tag;
                } else {
                        // включение буферизации вывода
                        ob_start();
                        // создание компонента на основе DOM модели
                        $this->updateTag($tag);
                        // замена текста DOM объекта данными компонента
                        $tag->outertext = ob_get_contents();
                        // удаление выходного буфера и выключение буферизации вывода
                        ob_end_clean();
                }
            }

            // если отладка
            if (Gear::$app->debug) Gear::$app->debug->info(GApplication::$components, GText::_('Components on the home page template', 'interface'));

            // замена текста DOM объекта данными компонента
            if ($commonTags['Meta'])
                $commonTags['Meta']->outertext = Gear::$app->document->getMetaTags();
            if ($commonTags['StyleSheet'])
                $commonTags['StyleSheet']->outertext = Gear::$app->document->getStyleSheetTags();
            if ($commonTags['Script'])
                $commonTags['Script']->outertext = Gear::$app->document->getScriptTags();
            if ($commonTags['StyleDeclaration'])
                $commonTags['StyleDeclaration']->outertext = Gear::$app->document->getStyleDeclarationTags();
            if ($commonTags['ScriptDeclaration'])
                $commonTags['ScriptDeclaration']->outertext = Gear::$app->document->getScriptDeclarationTags();
            if ($commonTags['Counter'])
                $commonTags['Counter']->outertext = Gear::$app->document->getCounter();
        }
        GTimer::stop('page parsing');
    }
}
?>