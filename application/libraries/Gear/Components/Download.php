<?php
/**
 * Gear CMS
 *
 * Компонент "Загрузка файла"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Download.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Компонент загрузки файла
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Download.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpDownload extends GComponentRest
{
    /**
     * Конструктор
     *
     * @return void
     */
    public function __construct()
    {
        // инициализация модели компонента
        $this->model = GFactory::getModel('/Download');
    }

    /**
     * Вывод данных
     * 
     * @return void
     */
    public function render()
    {
        return $this->model->viewFile();
    }
}
?>