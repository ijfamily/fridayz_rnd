<?php
/**
 * Gear CMS
 *
 * Компонент "Слайдер Nivo"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Nivo.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CpSlider
 */
Gear::library('/Components/Slider');

/**
 * Компонент Nivo слайдер
 * jQuery Nivo Slider v2.7.1
 * http://nivo.dev7studios.com
 * 
 * @category   Gear
 * @package    Components
 * @subpackage Slider
 * @copyright  Copyright (c) 2013-2016 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Nivo.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpSliderNivo extends CpSlider
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'slider_nivo.tpl.php';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'slider-nivo';

    /**
     * Добавление JS и CSS объявлений компонента в документ
     *
     * @params object $doc документ
     * @return void
     */
    public function scripts($doc)
    {
        $doc->addStyleSheet('jquery.nivo-slider.css', 'text/css', 'screen');
        $doc->addStyleSheet('nivo-slider/themes/default/default.css', 'text/css', 'screen');
        $doc->addStyleSheet('nivo-slider/themes/light/light.css', 'text/css', 'screen');
        $doc->addScript('jquery.nivo.slider.min.js');
        $s = '$(\'#' . $this->id . '\').nivoSlider(' . json_encode($this->attributes) . ');';
        $doc->addJQueryDeclaration($s);
    }
}
?>