<?php
/**
 * Gear CMS
 *
 * Компонент "Слайдер Slit"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Slit.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CpSlider
 */
Gear::library('/Components/Slider');

/**
 * Компонент Slit слайдер
 * jQuery Slider Slit v1.1.0
 * http://www.codrops.com
 * 
 * @category   Gear
 * @package    Components
 * @subpackage Slider
 * @copyright  Copyright (c) 2013-2016 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Slit.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpSliderSlit extends CpSlider
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'slider_slit.tpl.php';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'slider-slit';

    /**
     * Добавление JS и CSS объявлений компонента в документ
     *
     * @params object $doc документ
     * @return void
     */
    public function scripts($doc)
    {
        $doc->addScript('jquery.slitslider.js');
        $doc->addStyleSheet('jquery.slitslider.css', 'text/css', 'screen');
    }
}
?>