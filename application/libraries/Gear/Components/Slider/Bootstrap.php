<?php
/**
 * Gear CMS
 *
 * Компонент "Слайдер Bootstrap"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Bootstrap.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CpSlider
 */
Gear::library('/Components/Slider');

/**
 * Компонент слайдер Bootstrap
 * 
 * @category   Gear
 * @package    Components
 * @subpackage Slider
 * @copyright  Copyright (c) 2013-2016 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Bootstrap.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpSliderBootstrap extends CpSlider
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'slider_bootstrap.tpl.php';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'slider-bootstrap';

    /**
     * Добавление JS и CSS объявлений компонента в документ
     *
     * @params object $doc документ
     * @return void
     */
    public function scripts($doc)
    {
        //$('#slider').carousel({ interval: 5000 });
        //$doc->addScript('jquery.slitslider.js');
        //$doc->addStyleSheet('jquery.slitslider.css', 'text/css', 'screen');
        $s = '$(\'#' . $this->id . '\').carousel({ interval: 5000 });';
        $doc->addJQueryDeclaration($s);

    }
}
?>