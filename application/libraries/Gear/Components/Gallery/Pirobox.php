<?php
/**
 * Gear CMS
 *
 * Компонент "Галерея Pirobox"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Pirobox.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CpGallery
 */
Gear::library('/Components/Gallery');

/**
 * Компонент галереи для jQuery Pirobox
 * http://jquery.pirolab.it/pirobox/
 * 
 * @category   Gear
 * @package    Components
 * @subpackage Gallery
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Pirobox.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpGalleryPirobox extends CpBaseGallery
{
    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'gallery-pirobox';

    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'gallery_pirobox.tpl.php';

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->info(__CLASS__, GText::_('component', 'interface'));

        // инициализация модели компонента
        $this->model = GFactory::getModel('/List/Gallery', 'GalleryImages', $this->attributes);
    }

    /**
     * Добавление JS и CSS объявлений компонента в документ
     *
     * @params object $doc документ
     * @return void
     */
    public function scripts($doc)
    {
        // Подключает Gear JS
        /*
        $doc->addStyleSheet('pirobox/style2/style.css', 'text/css', '', '', false, 'Pirobox gallery');
        $doc->addScript('jquery-ui.custom.min.js?v=1.8.2', 'text/javascript', false, false, 'jQuery UI');
        $doc->addScript('jquery.pirobox.ext.min.js?v=1.0', 'text/javascript', false, false, 'Pirobox gallery');
        $s = "$().piroBox_ext({ piro_speed : 700, bg_alpha : 0.5, piro_scroll : true });";
        $doc->addJQueryDeclaration($s);
        */
    }
}


/**
 * Компонент галереи для jQuery Pirobox (для AJAX)
 * http://jquery.pirolab.it/pirobox/
 * 
 * @category   Gear
 * @package    Components
 * @subpackage Gallery
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Pirobox.php 2016-01-01 12:00:00 Gear Magic $
 */
class CjGalleryPirobox extends CjGallery
{
    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'gallery';

    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'gallery_pirobox_j.tpl.php';

    /**
     * Конструктор
     *
     * @return void
     */
    public function __construct()
    {
        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->info(__CLASS__, GText::_('component', 'interface'));

        // инициализация модели компонента
        $this->model = GFactory::getModel('/List/Gallery', 'jGalleryImages', $this->attributes);
    }
}
?>