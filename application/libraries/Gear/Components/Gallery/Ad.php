<?php
/**
 * Gear CMS
 *
 * Компонент "Галерея AD"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Ad.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CpGallery
 */
Gear::library('/Components/Gallery');

/**
 * Компонент галереи для jQuery AD Gallery
 * http://coffeescripter.com/code/ad-gallery/
 * 
 * @category   Gear
 * @package    Components
 * @subpackage Gallery
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Ad.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpGalleryAd extends CpBaseGallery
{
    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'gallery-ad';

    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'gallery_ad.tpl.php';

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->info(__CLASS__, GText::_('component', 'interface'));

        // инициализация модели компонента
        $this->model = GFactory::getModel('/List/Gallery', 'GalleryImages', $this->attributes);
    }

    /**
     * Добавление JS и CSS объявлений компонента в документ
     *
     * @params object $doc документ
     * @return void
     */
    public function scripts($doc)
    {
        $doc->addStyleSheet('jquery.ad-gallery.css');
        $doc->addScript('jquery.ad-gallery.min.js');
        $s = "$('#" . $this->id . "').adGallery();";
        $doc->addJQueryDeclaration($s);
    }
}


/**
 * Компонент галереи для jQuery AD Gallery (для AJAX)
 * http://coffeescripter.com/code/ad-gallery/
 * 
 * @category   Gear
 * @package    Components
 * @subpackage Gallery
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Gallery.php 2016-01-01 12:00:00 Gear Magic $
 */
class CjGalleryAd extends CjGallery
{
    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'gallery';

    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'gallery_ad_j.tpl.php';

    /**
     * Конструктор
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->info(__CLASS__, GText::_('component', 'interface'));

        // инициализация модели компонента
        $this->model = GFactory::getModel('/List/Gallery', 'jGalleryImages', $this->attributes);
    }
}
?>