<?php
/**
 * Gear CMS
 *
 * Компонент "Лента фотоальбомов"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2015 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Albums.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CpFeed
 */
Gear::library('/Components/Feed');

/**
 * Компонент ленты фотоальбомов
 * 
 * @category   Gear
 * @package    Components
 * @subpackage Feed
 * @copyright  Copyright (c) 2013-2016 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Albums.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpFeedAlbums extends CpFeed
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'feed_albums.tpl.php';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'feed-albums';

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->info(__CLASS__, L_COMPONENT);

        // инициализация модели компонента
        $this->model = GFactory::getModel('/Feed/Albums', 'FeedAlbums', $this->attributes);
    }

    /**
     * Возращает "open" тег компонента (для режима "конструктора")
     * 
     * @return string
     */
    protected function getDesignTagOpen()
    {
        $_ = GText::getSection($this->get('path'));

        return <<<HTML
        <section role="component" id="{$this->_data['attr']['id']}" class="gear-position-rt">
            <control id="ctrl-{$this->_data['attr']['id']}" title="{$_['setting component']}" role="component control"></control>
            <script type="text/javascript">
                (function(w, n, id) {
                    w[n] = w[n] || [];
                    w[n].push({
                        id: id,
                        className: "{$this->_data['attr']['class']}",
                        templateId: {$this->_data['template-id']},
                        dataId: {$this->_data['data-id']},
                        articleId: {$this->_data['article-id']},
                        pageId: {$this->_data['page-id']},
                        belongTo: "{$this->_data['belong-to']}",
                        menu: {
                            "add": {name: "{$_['add album']}", icon: "add-document", url: "site/albums/albums/~/profile/interface/"},
                            "list": {name: "{$_['albums']}", icon: "documents", url: "site/albums/albums/~/grid/interface/"},
                            "property": {name: "{$_['properties']}", icon: "property", url: "site/design/~/component/interface/"}
                        }
                    });
                })(window, "gearComponents", "{$this->_data['attr']['id']}");
            </script>
            <content>
HTML;
    }

    /**
     * Возращает тег управления элементами компонента (для режима "конструктора")
     * 
     * @return string
     */
    protected function getDesignTagControl()
    {
        $_ = GText::getSection($this->get('path'));

        return <<<HTML
            <control id="ctrl-{$this->_data['attr']['id']}-item-%s" title="{$_['setting element']}" role="item control"></control>
            <script type="text/javascript">
                (function(w, n, id) {
                    w[n] = w[n] || [];
                    w[n].push({ id: id, dataId: '%s', menu: { "edit": {name: "{$_['edit']}", icon: "edit", url: "site/albums/albums/~/profile/interface/"} }});
                })(window, "gearComponents", "{$this->_data['attr']['id']}-item-%s");
            </script>
HTML;
    }
}
?>