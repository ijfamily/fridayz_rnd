<?php
/**
 * Gear CMS
 *
 * Компонент "Лента архива статей"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2015 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Archive.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CpFeed
 */
Gear::library('/Components/Feed');

/**
 * Компонент ленты архива статей
 * 
 * @category   Gear
 * @package    Components
 * @subpackage Feed
 * @copyright  Copyright (c) 2013-2016 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Archive.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpFeedArchive extends CpFeed
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'feed_archive.tpl.php';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'feed-archive';

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->info(__CLASS__, 'Компонент');

        // инициализация модели компонента
        $this->model = GFactory::getModel('/Feed/Archive', 'FeedArchive', $this->attributes);
    }

    /**
     * Инициализация компонента
     * 
     * @return void
     */
    protected function initialise()
    {
        parent::initialise();

        // группировка
        $this->_data['group-by'] = $this->model->groupBy;
    }
}
?>