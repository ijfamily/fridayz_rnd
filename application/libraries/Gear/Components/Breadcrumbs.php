<?php
/**
 * Gear
 *
 * Компонент "Хлебные крошки"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Breadcrumbs.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Компонент вывода хлебных крошек
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Breadcrumbs.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpBreadcrumbs extends GComponentLight
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'breadcrumbs.tpl.php';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'breadcrumbs';

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента 
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->info(__CLASS__, GText::_('component', 'interface'));

        // инициализация модели компонента
        $this->model = GFactory::getModel('/Breadcrumbs', $this->attributes);
        // если страница отображаема
        if (!Gear::$app->document->isOk()) return;
    }

    /**
     * Возращает "open" тег компонента (для режима "конструктора")
     * 
     * @return string
     */
    protected function getDesignTagOpen()
    {
        return <<<HTML
        <section role="component" id="{$this->_data['attr']['id']}" class="gear-position-rt">
            <control id="ctrl-{$this->_data['attr']['id']}" title="{$this->_['setting component']}" role="component control"></control>
            <script type="text/javascript">
                (function(w, n, id) {
                    w[n] = w[n] || [];
                    w[n].push({
                        id: id,
                        className: "{$this->_data['attr']['class']}",
                        templateId: {$this->_data['template-id']},
                        dataId: {$this->_data['data-id']},
                        articleId: {$this->_data['article-id']},
                        pageId: {$this->_data['page-id']},
                        belongTo: "{$this->_data['belong-to']}",
                        menu: {
                            "add": {name: "{$this->_['add category']}", icon: "add-category", url: "site/references/acategories/~/profile/interface/"},
                            "edit": {name: "{$this->_['edit']}", icon: "edit", url: "site/references/acategories/~/profile/interface/"},
                            "list": {name: "{$this->_['categories']}", icon: "categories", url: "site/references/acategories/~/grid/interface/"},
                            "property": {name: "{$this->_['property']}", icon: "property", url: "site/design/~/component/interface/"}
                        }
                    });
                })(window, "gearComponents", "{$this->_data['attr']['id']}");
            </script>
            <content>
HTML;
    }

    /**
     * Возращает тег управления элементами компонента (для режима "конструктора")
     * 
     * @return string
     */
    protected function getDesignTagControl()
    {
        return <<<HTML
            <control id="ctrl-{$this->_data['attr']['id']}-item-%s" title="{$this->_['setting element']}" role="item control"></control>
            <script type="text/javascript">
                (function(w, n, id) {
                    w[n] = w[n] || [];
                    w[n].push({ id: id, dataId: '%s', menu: { "edit": {name: "{$this->_['edit']}", icon: "edit-category", url: "site/references/acategories/~/profile/interface/"} }});
                })(window, "gearComponents", "{$this->_data['attr']['id']}-item-%s");
            </script>
HTML;
    }

    /**
     * Валидация компонента перед выводом данных, если компонент не прошел
     * валидацию - данные компонента не выводятся
     * 
     * @return boolean
     */
    public function isValid()
    {
        if (!Gear::$app->config->get('BREADCRUMBS', false)) return false;

        return parent::isValid();
    }

    /**
     * Инициализация компонента
     * 
     * @return void
     */
    protected function initialise()
    {
        parent::initialise();

        // элементы списка
        $this->_data['items'] = $this->model->getItems();
        // количество элементов списка
        $this->_data['count'] = sizeof($this->_data['items']);

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->log($this->_data, GText::_('at component template', 'interface'));
    }

    /**
     * Валидация данных компонента после их инициализации
     * если false - компонент не выводится
     * 
     * @return boolean
     */
    public function isValidInitialise()
    {
        return !$this->_data['is-root'];
    }
}
?>