<?php
/**
 * Gear CMS
 *
 * Компонент "Информация о пользователе"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Page.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see GUser
 */
Gear::library('/User');

/**
 * Информация о пользователе
 * 
 * @category   Gear
 * @package    Components
 * @subpackage User
 * @copyright  Copyright (c) 2014-2015 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Page.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpUserPage extends GComponentLight
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'user_page.tpl.php';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'user-page';

    /**
     * Класс пользователя
     *
     * @var object
     */
    public $user;

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->info(__CLASS__, GText::_('component', 'interface'));

        // инициализация модели компонента
        $this->action = Gear::$app->url->getVar('action', false);
        // пользователь
        $this->auth = GFactory::getAuth();
    }

    /**
     * Инициализация компонента
     * 
     * @return void
     */
    protected function initialise()
    {
        parent::initialise();

        // данные о пользователе
        $this->_data['user'] = false;
        // если аккаунт был удален
        $this->_data['is-deleted'] = false;
        // если пользователь авторизирован
        if ($this->auth->check()) {
            $this->_data['user'] = $this->auth->getUser();
            // действие над аккаунтом
            switch ($this->action) {
                // выйти из аккаунта
                case 'signout':
                    // добавление в журнал
                    GUserLog::add(array(
                        'user_id'     => $this->auth->getId(),
                        'log_action'  => 'signout',
                        'log_text'    => 'Success',
                        'log_email'   => $this->auth->getEmail(),
                        'log_network' => $this->auth->getNetworkName()
                    ));
                    $this->auth->signOut();
                    //header('location: /');
                    //break;
                    Gear::redirect('/');

                // удалить аккаунт
                case 'delete':
                    GUser::delete($this->auth->getId());
                    GUserNetworks::deleteByUser($this->auth->getId());
                    // добавление в журнал
                    GUserLog::add(array(
                        'user_id'     => $this->auth->getId(),
                        'log_action'  => 'delete',
                        'log_text'    => 'Success',
                        'log_email'   => $this->auth->getEmail(),
                        'log_network' => $this->auth->getNetworkName()
                    ));
                    $this->auth->signOut();
                    $this->_data['is-deleted'] = true;
            }
        }

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->log($this->_data, GText::_('at component template', 'interface'));
    }
}
?>