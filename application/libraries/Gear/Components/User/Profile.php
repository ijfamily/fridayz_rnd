<?php
/**
 * Gear CMS
 *
 * Компонент "Форма профиля пользователя"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Profile.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CpForm
 */
Gear::library('/Components/Form');

/**
 * Форма профиля пользователя
 * 
 * @category   Gear
 * @package    Components
 * @subpackage User
 * @copyright  Copyright (c) 2014-2015 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Profile.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpUserProfile extends CpForm
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'user_profile.tpl.php';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'user-profile';

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // если отладка 
        if (Gear::$app->debug) Gear::$app->debug->info(__CLASS__, GText::_('component', 'interface'));

        // инициализация модели компонента
        $this->model = GFactory::getModel('/User/Profile', 'UserProfile', $this->attributes);
    }
}
?>