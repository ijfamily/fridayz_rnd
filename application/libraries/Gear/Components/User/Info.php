<?php
/**
 * Gear CMS
 *
 * Компонент "Информация о пользователе"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Page.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see GUser
 */
Gear::library('/User');

/**
 * Информация о пользователе
 * 
 * @category   Gear
 * @package    Components
 * @subpackage User
 * @copyright  Copyright (c) 2014-2015 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Page.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpUserInfo extends GComponentLight
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'user_info.tpl.php';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'user-info';

    /**
     * Класс пользователя
     *
     * @var object
     */
    public $user;

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->info(__CLASS__, GText::_('component', 'interface'));

        // пользователь
        $this->auth = GFactory::getAuth();
    }

    /**
     * Инициализация компонента
     * 
     * @return void
     */
    protected function initialise()
    {
        parent::initialise();

        // если пользователь авторизирован
        //if (!$this->auth->check()) return;
        $this->_data['user'] = $this->auth->getUser();

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->log($this->_data, GText::_('at component template', 'interface'));
    }
}
?>