<?php
/**
 * Gear CMS
 *
 * Компонент "Меню сайта"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Menu.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Базовый класс компонента меню
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Menu.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpMenu extends GComponentLight
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'menu.tpl.php';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'menu';

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->info(__CLASS__, GText::_('component', 'interface'));

        // инициализация модели компонента
        $this->model = GFactory::getModel('/Menu', 'Menu', $this->attributes);
    }

    /**
     * Возращает "open" тег компонента (для режима "конструктора")
     * 
     * @return string
     */
    protected function getDesignTagOpen()
    {
        return <<<HTML
        <section role="component" id="{$this->_data['attr']['id']}" class="gear-position-rt">
            <control id="ctrl-{$this->_data['attr']['id']}" title="{$this->_['setting component']}" role="component control"></control>
            <script type="text/javascript">
                (function(w, n, id) {
                    w[n] = w[n] || [];
                    w[n].push({
                        id: id,
                        className: "{$this->_data['attr']['class']}",
                        templateId: {$this->_data['template-id']},
                        dataId: {$this->_data['data-id']},
                        articleId: {$this->_data['article-id']},
                        pageId: {$this->_data['page-id']},
                        belongTo: "{$this->_data['belong-to']}",
                        menu: {
                            "add": {name: "{$this->_['add item']}", icon: "add-document", url: "site/menu/items/~/profile/interface/"},
                            "edit": {name: "{$this->_['edit']}", icon: "edit", url: "site/menu/menu/~/profile/interface/"},
                            "list": {name: "{$this->_['menu']}", icon: "documents", url: "site/menu/items/~/grid/interface/"},
                            "property": {name: "{$this->_['property']}", icon: "property", url: "site/design/~/component/interface/"}
                        }
                    });
                })(window, "gearComponents", "{$this->_data['attr']['id']}");
            </script>
            <content>
HTML;
    }

    /**
     * Возращает тег управления элементами компонента (для режима "конструктора")
     * 
     * @return string
     */
    protected function getDesignTagControl()
    {
        return <<<HTML
            <control id="ctrl-{$this->_data['attr']['id']}-item-%s" title="{$this->_['setting item']}" role="item control"></control>
            <script type="text/javascript">
                (function(w, n, id) {
                    w[n] = w[n] || [];
                    w[n].push({ id: id, dataId: '%s', menu: { "edit": {name: "{$this->_['edit']}", icon: "edit", url: "site/menu/items/~/profile/interface/"} }});
                })(window, "gearComponents", "{$this->_data['attr']['id']}-item-%s");
            </script>
HTML;
    }

    /**
     * Инициализация компонента
     * 
     * @return void
     */
    protected function initialise()
    {
        parent::initialise();

        // элементы меню
        $this->_data['items'] = $this->model->getItems();

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->log($this->_data, GText::_('at component template', 'interface'));
    }
}


/**
 * Компонент каскадного меню
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Menu.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpMenuCascade extends CpMenu
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'menu_cascade.tpl.php';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'menu-cascade';

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->info(__CLASS__, GText::_('component', 'interface'));

        // инициализация модели компонента
        $this->model = GFactory::getModel('/Menu', 'MenuCascade', $attr);
    }
}
?>