<?php
/**
 * Gear CMS
 *
 * Компонент "Виджеты социальных сетей"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Widget.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Базовый класс компонент виджета
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Widget.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpWidget extends GComponentWidget
{
    /**
     * Возращает теги виджета
     * 
     * @return string
     */
    protected function getAttrTags()
    {
        $str = '';
        foreach ($this->attributes as $key => $value) {
            if ($key != 'id' && $key != 'path' && $key != 'template')
            if (!is_null($value))
                if ($value)
                    $str .= $key . '="' . $value . '" ';
        }

        return $str;
    }
}
?>