<?php
/**
 * Gear CMS
 *
 * Компонент "Список фотоальбомов"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Albums.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CpList
 */
Gear::library('/Components/List');

/**
 * Компонент списка фотоальбомов
 * 
 * @category   Gear
 * @package    Components
 * @subpackage List
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Albums.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpAlbums extends CpList
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'albums.tpl.php';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'albums';

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->info(__CLASS__, GText::_('component', 'interface'));

        // инициализация модели компонента
        $this->model = GFactory::getModel('/List/Albums', 'Albums', $this->attributes);
    }

    /**
     * Возращает "open" тег компонента (для режима "конструктора")
     * 
     * @return string
     */
    protected function getDesignTagOpen()
    {
        return <<<HTML
        <section role="component" id="{$this->_data['attr']['id']}" class="gear-position-rt">
            <control id="ctrl-{$this->_data['attr']['id']}" title="{$this->_['setting component']}" role="component control"></control>
            <script type="text/javascript">
                (function(w, n, id) {
                    w[n] = w[n] || [];
                    w[n].push({
                        id: id,
                        className: "{$this->_data['attr']['class']}",
                        templateId: {$this->_data['template-id']},
                        dataId: {$this->_data['data-id']},
                        articleId: {$this->_data['article-id']},
                        pageId: {$this->_data['page-id']},
                        belongTo: "{$this->_data['belong-to']}",
                        menu: {
                            "add": {name: "{$this->_['add album']}", icon: "add-document", url: "site/albums/albums/~/profile/interface/"},
                            "list": {name: "{$this->_['albums']}", icon: "documents", url: "site/albums/albums/~/grid/interface/"}
                        }
                    });
                })(window, "gearComponents", "{$this->_data['attr']['id']}");
            </script>
            <content>
HTML;
    }

    /**
     * Возращает тег управления элементами компонента (для режима "конструктора")
     * 
     * @return string
     */
    protected function getDesignTagControl()
    {
        return <<<HTML
            <control id="ctrl-{$this->_data['attr']['id']}-item-%s" title="{$this->_['setting item']}" role="item control"></control>
            <script type="text/javascript">
                (function(w, n, id) {
                    w[n] = w[n] || [];
                    w[n].push({ id: id, dataId: '%s', menu: { "edit": {name: "{$this->_['edit']}", icon: "edit-image", url: "site/albums/albums/~/profile/interface/"} }});
                })(window, "gearComponents", "{$this->_data['attr']['id']}-item-%s");
            </script>
HTML;
    }
}


/**
 * Компонент списка изображений фотоальбомов
 * 
 * @category   Gear
 * @package    Components
 * @subpackage List
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Albums.php 2016-01-01 12:00:00 Gear Magic $
 */
class CpAlbumImages extends CpList
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'album_images.tpl.php';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'album-images';

    /**
     * Фотоальбом
     *
     * @var mixed
     */
    public $album = false;

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->info(__CLASS__, GText::_('component', 'interface'));

        // инициализация модели компонента
        $this->model = GFactory::getModel('/List/Albums', 'AlbumImages', $this->attributes);
    }

    /**
     * Возращает "open" тег компонента (для режима "конструктора")
     * 
     * @return string
     */
    protected function getDesignTagOpen()
    {
        $d = $this->model->albumId;
        return <<<HTML
        <section role="component" id="{$this->_data['attr']['id']}" class="gear-position-rt">
            <control id="ctrl-{$this->_data['attr']['id']}" title="{$this->_['setting component']}" role="component control"></control>
            <script type="text/javascript">
                (function(w, n, id) {
                    w[n] = w[n] || [];
                    w[n].push({
                        id: id,
                        className: "{$this->_data['attr']['class']}",
                        templateId: {$this->_data['template-id']},
                        dataId: {$this->_data['data-id']},
                        articleId: {$this->_data['article-id']},
                        pageId: {$this->_data['page-id']},
                        belongTo: "{$this->_data['belong-to']}",
                        menu: {
                            "add": {name: "{$this->_['add image']}", icon: "add-image", url: "site/albums/images/~/profile/interface/"},
                            "list": {name: "{$this->_['images']}", icon: "documents", url: "site/albums/images/~/grid/interface/{$d}/"}
                        }
                    });
                })(window, "gearComponents", "{$this->_data['attr']['id']}");
            </script>
            <content>
HTML;
    }

    /**
     * Возращает тег управления элементами компонента (для режима "конструктора")
     * 
     * @return string
     */
    protected function getDesignTagControl()
    {
        return <<<HTML
            <control id="ctrl-{$this->_data['attr']['id']}-item-%s" title="{$this->_['setting item']}" role="item control"></control>
            <script type="text/javascript">
                (function(w, n, id) {
                    w[n] = w[n] || [];
                    w[n].push({ id: id, dataId: '%s', menu: { "edit": {name: "{$this->_['edit']}", icon: "edit-image", url: "site/albums/images/~/profile/interface/"} }});
                })(window, "gearComponents", "{$this->_data['attr']['id']}-item-%s");
            </script>
HTML;
    }

    /**
     * Инициализация компонента
     * 
     * @return void
     */
    protected function initialise()
    {
        parent::initialise();

        $this->album = $this->model->getAlbum();
        if ($this->album) {
            $this->_data['album-date'] = $this->album['published_date'];
            $this->_data['album-title'] = $this->album['album_name'];
            $this->_data['album-desc'] = $this->album['album_description'];
            Gear::$app->document->title = $this->album['album_name'];
        }
    }

    /**
     * Валидация данных компонента после их инициализации
     * если false - компонент не выводится
     * 
     * @return boolean
     */
    public function isValidInitialise()
    {
        if (empty($this->album)) {
            Gear::$app->document->setCode(404);
            return false;
        }

        return true;
    }

    /**
     * Добавление JS и CSS объявлений компонента в документ
     *
     * @params object $doc документ
     * @return void
     */
    public function scripts($doc)
    {}
}
?>