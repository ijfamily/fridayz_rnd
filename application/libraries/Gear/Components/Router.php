<?php
/**
 * Gear CMS
 *
 * Компонент "Маршрутизатор"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Router.php 2016-01-01 21:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Маршрутизатор компонентов
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Router.php 2016-01-01 21:00:00 Gear Magic $
 */
class CpRouter extends GComponentRouter
{
    /**
     * Если ЧПУ не включено, в запросе:
     * a - идент. статьи, c - идент. категории, do - действие
     * 
     * /

    /**
     * Вызывается при AJAX запросе
     * 
     * @return void
     */
    protected function ajaxInit()
    {
        // если не зайдествованы AJAX запросы
        if (!Gear::$app->config->get('AJAX')) {
            return json_encode(array('success' => false, 'message' => GText::_('Sorry, but your request is not defined')));
        }

        $request = '';
        // если ЧПУ работает
        if (Gear::$app->sef) {
            $isRequest = $this->url->getSeg(0, false);
            if ($isRequest == 'request') {
                $request = $this->url->getSeg(1, false);
            }
        }
        if (empty($request))
            $request = $this->url->getVar('request', false);
        // если AJAX запрос
        if ($request) {
            $router = $this->list['request'];
            if (isset($router[$request])) {
                if ($router[$request]['enabled']) {
                    $cmp = $this->requestTo($router[$request], $request);
                }
            }
        }

        // если компонент создан
        if (isset($cmp)) {
            // заставляем приложение кэшировать компонент
            if ($cmp)
                return $cmp->render();
        }

        return json_encode(array('success' => false, 'message' => GText::_('Sorry, but your request is not defined')));
    }

    /**
     * Вызывается до инициализации данных приложения
     * 
     * @return void
     */
    protected function beforeInit()
    {
        // список маршрутизации
        foreach ($this->list['route'] as $alias => $settings) {
            if ($settings['type'] == 'none' || empty($settings['type']) && $settings['enabled']) {
                if ($this->routeTo($settings)) break;
            } else
                // если ЧПУ работает
                if (Gear::$app->sef) {
                    if ($this->url->match($settings['value'], $settings['type'], $settings['size'])) {
                        if ($settings['enabled'])
                            if ($this->routeTo($settings)) break;
                    }
                // если ЧПУ не работает
                } else {
                    if ($this->url->do == $settings['alias'] && $settings['enabled'])
                        if ($this->routeTo($settings)) break;
                }
        }
    }

    /**
     * Вызывается после инициализации данных приложения
     * 
     * @return void
     */
    protected function afterInit()
    {
        // если страница не найдена, нет смысла создавать конструкции
        if ($this->document->code == 404) return;
    }
}
?>