<?php
/**
 * Gear CMS
 *
 * Компонент "Подсчет пользователей"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Visits.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Клас подсчета пользователей (для AJAX)
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Visits.php 2016-01-01 12:00:00 Gear Magic $
 */
class CjVisor extends GComponentAjax
{
    /**
     * Данные возращаемые клиенту
     *
     * @var array
     */
    protected $_response = array('success' => false, 'message' => 'unknow', '_st' => '');

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        $this->get = $this->input->get('get', array());
        $this->get = array_merge(array(
            'article' => false,
            'banners' => false
        ), $this->get);
        
        // тип идентификатор
        $this->cls = $this->input->get('cls', '');
        // строка запроса
        $this->url = $this->input->get('url', '');
        // идентификатор статьи и т.д.
        $id = $this->input->get('id', '');
        if ($id) {
            $str = explode('-', $id);
            $this->type = $str[0];
            if (sizeof($str) > 0)
                $this->id = (int) $str[1];
        }

        // установить соединение с сервером
        GFactory::getDb()->connect();
    }

    /**
     * Подсчет визитов
     * 
     * @return void
     */
    protected function considerVisits()
    {
        // если не указан идентификатор
        if (empty($this->id) || !$this->get['article']) return;

        Gear::$app->session->start();
        switch ($this->type) {
            case 'article':
                $id = 'article-visit-' . $this->id;
                // если уже отметили что визит состоялся
                if (Gear::$app->session->get($id, false)) {
                     $this->_response['success'] = true;
                    $this->_response['message'] = 'ok';
                    break;
                }
                Gear::$app->session->set($id, true);
                $query = GFactory::getQuery();
                $sql = 'UPDATE `site_articles` SET `article_visits`=`article_visits` + 1 WHERE `article_id`=' . $this->id;
                if (($this->_response['success'] = $query->execute($sql)) !== true) {
                    $this->_response['message'] = 'error request';
                } else {
                    $this->_response['success'] = true;
                    $this->_response['message'] = 'ok';
                }
                break;
        }
    }

    /**
     * Подсчет визитов
     * 
     * @return void
     */
    protected function banners()
    {
        //
        if (!Gear::$app->config->get('BANNERS') || !$this->get['banners']) return;

        /**
         * @see GBanners
         */
        Gear::library('/Banners');

        $this->_response['banners'] = GBanners::getItems();
        $this->_response['success'] = true;
    }

    /**
     * Вывод данных
     * 
     * @return void
     */
    public function render()
    {
        // подсчет визитов
        $this->considerVisits();
        // вывод баннеров
        $this->banners();
        // вывод данных компонента
        die(json_encode($this->_response));
    }
}
?>