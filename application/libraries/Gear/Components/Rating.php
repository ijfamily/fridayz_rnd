<?php
/**
 * Gear CMS
 *
 * Компонент "Рейтинг материала"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Rating.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Компонент рейтинга материала (для AJAX запроса)
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Rating.php 2016-01-01 12:00:00 Gear Magic $
 */
class CjRating extends GComponentAjax
{
    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // инициализация модели компонента
        $this->model = GFactory::getModel('/Rating', 'jRating', $attr);
    }

    /**
     * Валидация компонента перед выводом данных, если компонент не прошел
     * валидацию - данные компонента не выводятся
     * 
     * @return boolean
     */
    public function isValid()
    {
        // вид запроса
        $type = $this->input->get('type', '');
        if (!$this->model->isExistType($type))
            return $this->_['Wrong kind of request is addressed'];
        // идент. записи рейтинга
        $id = $this->model->formatId($this->input->get('id', 0));
        if (empty($id))
            return $this->_['Incorrect request id'];
        // значение рейтинга
        $value = $this->input->getInt('value', 0);
        if (empty($value) || !($value >= 1 && $value <= 5))
            return $this->_['Not specified rating value'];

        $sess = GFactory::getSession();
        $sess->start();
        $srating = $sess->get('rating', false);
        if ($srating) {
            if (isset($srating[$type . $id]))
                return $this->_['You have already voted'];
        }

        // проверка материала рейтинга
        $rating = $this->model->getRating($type, $id);
        if ($rating === false)
            return /*$this->_['Unable to determine the rating']*/true;
        $data = array(
            'rating_votes' => $rating['votes'] + 1,
            'rating_value' => $rating['value'] + $value
        );
        // обновить рейтинг
        if (!$this->model->updateRating($type, $data, $id))
            return $this->_['Unable to update the rating'];
        // если рейтинга материала еще нет
        if ($srating)
           $srating[$type . $id] = true;
        else
             $srating = array($type . $id => true);
        // обновить рейтинг в сессии
        $sess->set('rating', $srating);

        return true;
    }

    /**
     * Вывод данных
     * 
     * @return void
     */
    public function render()
    {
        // валидация компонента
        if (($res = $this->isValid()) !== true) {
            $this->_response['success'] = false;
            $this->_response['html'] = $res;
        }
        // вывод данных компонента
        die(json_encode($this->_response));
    }
}
?>