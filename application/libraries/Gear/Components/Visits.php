<?php
/**
 * Gear CMS
 *
 * Компонент "Подсчет пользователей"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Visits.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;


/**
 * Клас подсчета пользователей (для AJAX)
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Visits.php 2016-01-01 12:00:00 Gear Magic $
 */
class CjVisits extends GComponentAjax
{
    /**
     * Данные возращаемые клиенту
     *
     * @var array
     */
    protected $_response = array('success' => false, 'message' => 'unknow', '_st' => '');

    /**
     * Конструктор
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        // идентификатор статьи и т.д.
        $this->dataId = $this->input->getInt('id', 0);
        // тип идентификатор
        $this->dataType = $this->input->get('type', false);
    }

    /**
     * Подсчет визитов
     * 
     * @return void
     */
    protected function considerVisits()
    {
        // если не указан идентификатор
        if (empty($this->dataId)) return;

        Gear::$app->session->start();

        switch ($this->dataType) {
            case 'article':
                $id = 'article-visit-' . $this->dataId;
                // если уже отметили что визит состоялся
                if (Gear::$app->session->get($id, false)) {
                    $this->_response['message'] = 'ok';
                    break;
                }
                Gear::$app->session->set($id, true);
                $query = new GDbQuery();
                $sql = 'UPDATE `site_articles` SET `article_visits`=`article_visits` + 1 WHERE `article_id`=' . $this->dataId;
                if (($this->_response['success'] = $query->execute($sql)) !== true)
                    $this->_response['message'] = 'error request';
                else
                    $this->_response['message'] = 'ok';
                break;
        }
    }

    /**
     * Вывод данных
     * 
     * @return void
     */
    public function render()
    {
        // подсчет визитов
        $this->considerVisits();
        // вывод данных компонента
        die(json_encode($this->_response));
    }
}
?>