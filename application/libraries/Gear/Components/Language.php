<?php
/**
 * Gear CMS
 *
 * Компонент "Языки сайта"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Language.php 2016-01-01 21:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * Компонент языка сайта
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Language.php 2016-01-01 21:00:00 Gear Magic $
 */
class CpLanguage extends GComponentLight
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'language.tpl.php';

    /**
     * Поиск тега в ресурсе (шаблон, текст, файл)
     *
     * @var string
     */
    public $tagSearch = 'language';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'language';

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // инициализация модели компонента
        $this->model = GFactory::getModel('/Language', $this->attributes);
    }

    /**
     * Инициализация компонента
     * 
     * @return void
     */
    protected function initialise()
    {
        parent::initialise();

        // язык по умолчанию
        $this->_data['def'] = GFactory::getLanguage('prefixDef');
        // элементы списка
        $this->_data['items'] = $this->model->getItems();
        // переход пj url
        $this->_data['url'] = Gear::$app->url->path;
    }
}
?>