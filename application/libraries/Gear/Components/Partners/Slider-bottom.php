<?php
/**
 * Gear CMS
 *
 * Компонент "Слайдер партнеров"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2017 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Slider.php 2017-08-24 12:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CpSlider
 */
Gear::library('/Components/Slider');

/**
 * Компонент слайдер партнеров
 * 
 * @category   Gear
 * @package    Components
 * @subpackage Slider
 * @copyright  Copyright (c) 2013-2017 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Slider.php 2017-08-24 12:00:00 Gear Magic $
 */
class CpPartnersSliderBottom extends CpSlider
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'partners_bottom_slider.tpl.php';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'partners-bottom-slider';

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
                // инициализация модели компонента
        $this->model = GFactory::getModel('/Partners/Slider-bottom', 'PartnersSliderBottom', $this->attributes);

        parent::__construct($attr);

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->info(__CLASS__, GText::_('component', 'interface'));
    }
}
?>