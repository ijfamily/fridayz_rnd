<?php
/**
 * Gear CMS
 *
 * Компонент "Страница заведения"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Partner.php 2016-01-01 21:00:00 Gear Magic $
 */

defined('_INC') or die;

/**
 * @see CpArticle
 */
Gear::library('/Components/Article');

/**
 * Компонент страницы заведения
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Partner.php 2016-01-01 21:00:00 Gear Magic $
 */
class CpPartner extends CpArticle
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'partner.tpl.php';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'partner';

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента 
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        $place = GPartners::getPlace();
        Gear::$app->setDataPage(array(
            'page_header'           => $place['place_name'],
            'page_title'            => $place['place_name'],
            'page_meta_keywords'    => $place['page_meta_keywords'],
            'page_meta_description' => $place['page_meta_description'],
            'page_meta_robots'      => $place['page_meta_robots'],
            'page_meta_author'      => $place['page_meta_author'],
            'page_meta_copyright'   => $place['page_meta_copyright'],
            'page_meta_revisit'     => $place['page_meta_revisit'],
            'page_meta_document'    => $place['page_meta_document'],
        ));
    }

    /**
     * Инициализация компонента
     * 
     * @return void
     */
    protected function initialise()
    {
        $place = GPartners::getPlace();

        if (!empty($place['rating_value']))
            $average = round ($place['rating_value'] / $place['rating_votes'], 2);
        else
            $average = 0;
        $state = round($average);
        $images = array();
        if ($place['place_extended'])
            $images = GPartners::getImages($place['place_id']);

        $this->_data['info'] = array(
            'id'       => $place['place_id'],
            'text'     => $place['place_text'],
            'uri'      => $place['place_uri'],
            'name'     => $place['place_name'],
            'folder'   => $place['place_folder'],
            'image'    => $place['place_image'] ? $place['place_image'] : '',
            'head'    => $place['place_head'] ? $place['place_head'] : '',
            'address'  => $place['place_address'],
            'delivery'      => (int) $place['place_delivery'],
            'delivery-note' => $place['place_delivery_note'],
            'phone'    => $place['place_phone'],
            'site'     => trim($place['place_site'], '!'),
            'coord'    => '',
            'coord-other' => array(),
            'schedule' => $place['place_schedule'],
            'kitchen'  => $place['place_kitchen'],
            'extended' => $place['place_extended'],
            'rating-average' => $average,
            'rating-state'   => round($average),
            'rating-votes'   => $place['rating_votes'],
			'like'         => $place['likes'] ,
			'like-votes'   => $place['likes_votes'],
            'images'   => $images
        );
        
        if (empty($place['place_attributes']))
            $this->_data['features'] = array();
        else
            $this->_data['features'] = json_decode($place['place_attributes'], true);
        if ($place['place_coord_ln'] && $place['place_coord_lt'])
            $this->_data['info']['coord'] = $place['place_coord_lt'] . ',' . $place['place_coord_ln'];
        if ($place['place_coord_other']) {
            $this->_data['info']['coord-other'] = explode(';', $place['place_coord_other']);
        }

        parent::initialise();
    }

    /**
     * Валидация компонента перед выводом данных, если компонент не прошел
     * валидацию - данные компонента не выводятся
     * 
     * @return boolean
     */
    public function isValid()
    {
        return true;
    }
}
?>