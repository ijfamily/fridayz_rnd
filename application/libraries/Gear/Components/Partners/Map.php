<?php
/**
 * Gear CMS
 *
 * Компонент "Карта партнёров"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Map.php 2016-01-01 21:00:00 Gear Magic $
 */

defined('_INC') or die;

Gear::library('/Partners');

/**
 * Компонент карты партнёров
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Map.php 2016-01-01 21:00:00 Gear Magic $
 */
class CpPartnersMap extends GComponentLight
{
    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = 'partners_map.tpl.php';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = 'partners-map';

    /**
     * Инициализация компонента
     * 
     * @return void
     */
    protected function initialise()
    {
        // список партнёров
        $this->_data['list'] = GPartners::getAllPlaces(true);

        // если отладка
        if (Gear::$app->debug) Gear::$app->debug->log($this->_data, GText::_('at component template', 'interface'));
    }
}
?>