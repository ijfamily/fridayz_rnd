<?php
/**
 * Gear CMS
 *
 * Блокировка IP адресов (создан: 2016-08-25 15:35:12)
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 *
 * @category   Gear
 * @package    Config
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Locking.php 2016-08-25 15:35:12 Gear Magic $
 */

return array(
    // блокировать IP адрес
    'ACTIVE'   => false,
    // вывод шаблона с ошибкой заблокированному IP адресу
    'TEMPLATE' => true,
    // список IP адресов
    'IP'       => array(
        '127.0.0.1' => true
    )
);
?>