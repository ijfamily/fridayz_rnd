<?php
/**
 * Gear CMS
 *
 * Настройки пользователей сайта (создан: 2016-11-04 11:44:25)
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 *
 * @category   Gear
 * @package    Config
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Users.php 2016-11-04 11:44:25 Gear Magic $
 */

return array(
    // разрешить восстановление аккаунта
    'client.id'     => 'cfd1200fdffa468496e17eff5aea4d74',
    // резрешить авторизацию и регистрацию
    'client.secret' => 'fd2530da63774b91bdc207d5a821f844',
    // метод авторизации на сайте
    'token'         => 'AQAAAAAXr301AAPukg39YN9LHEa0txyL-yklTcY',
    'counter.id'    => '38248495'
    //https://oauth.yandex.ru/authorize?response_type=token&client_id=
);
?>