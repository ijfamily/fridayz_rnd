<?php
/**
 * Gear CMS
 *
 * Настройки альбомов (создан: 2016-08-31 15:24:03)
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 *
 * @category   Gear
 * @package    Config
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Partners.php 2016-08-31 15:24:03 Gear Magic $
 */

return array(
    // генерировать название файла
    'GENERATE/FILE/NAME' => true,
    // количество изображений в списке
    'LIST/LIMIT'         => 0,

    // Изображения
    // размеры оригинального изображения
    'IMAGE/ORIGINAL/WIDTH'  => 640,
    'IMAGE/ORIGINAL/HEIGHT' => 640,
    // размеры миниатюры изображения
    'IMAGE/THUMB/WIDTH'     => 400,
    'IMAGE/THUMB/HEIGHT'    => 0,
    // качество изображения
    'IMAGE/QUALITY'         => 85,

    // Водяной знак
    // использовать
    'WATERMARK'          => true,
    // положение штампа
    'WATERMARK/POSITION' => 'bottom-right'
);
?>