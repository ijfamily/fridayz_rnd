<?php
/**
 * Gear CMS
 *
 * Настройка списка MIMES (создан: 2016-05-13 17:50:25)
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 *
 * @category   Gear
 * @package    Config
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Mimes.php 2016-05-13 17:50:25 Gear Magic $
 */

defined('_INC') or die;

return array(
    'csv' => array('text/x-comma-separated-values','text/comma-separated-values','application/octet-stream','application/vnd.ms-excel','application/x-csv','text/x-csv','text/csv','application/csv','application/excel','application/vnd.msexcel'),
    'psd' => 'application/x-photoshop',
    'pdf' => array('application/pdf','application/x-download'),
    'ai' => 'application/postscript',
    'eps' => 'application/postscript',
    'xls' => array('application/excel','application/vnd.ms-excel','application/msexcel'),
    'ppt' => array('application/powerpoint','application/vnd.ms-powerpoint'),
    'gtar' => 'application/x-gtar',
    'gz' => 'application/x-gzip',
    'js' => 'application/x-javascript',
    'swf' => 'application/x-shockwave-flash',
    'tar' => 'application/x-tar',
    'tgz' => array('application/x-tar','application/x-gzip-compressed'),
    'xhtml' => 'application/xhtml+xml',
    'zip' => array('application/x-zip','application/zip','application/x-zip-compressed'),
    'mid' => 'audio/midi',
    'midi' => 'audio/midi',
    'mpga' => 'audio/mpeg',
    'mp2' => 'audio/mpeg',
    'mp3' => array('audio/mpeg','audio/mpg','audio/mpeg3','audio/mp3'),
    'aif' => 'audio/x-aiff',
    'aiff' => 'audio/x-aiff',
    'aifc' => 'audio/x-aiff',
    'wav' => array('audio/x-wav','audio/wave','audio/wav'),
    'bmp' => array('image/bmp','image/x-windows-bmp'),
    'gif' => 'image/gif',
    'jpeg' => array('image/jpeg','image/pjpeg'),
    'jpg' => array('image/jpeg','image/pjpeg'),
    'jpe' => array('image/jpeg','image/pjpeg'),
    'png' => array('image/png','image/x-png'),
    'tiff' => 'image/tiff',
    'tif' => 'image/tiff',
    'css' => 'text/css',
    'html' => 'text/html',
    'htm' => 'text/html',
    'shtml' => 'text/html',
    'txt' => 'text/plain',
    'text' => 'text/plain',
    'log' => array('text/plain','text/x-log'),
    'rtx' => 'text/richtext',
    'rtf' => 'text/rtf',
    'xml' => 'text/xml',
    'xsl' => 'text/xml',
    'mpeg' => 'video/mpeg',
    'mpg' => 'video/mpeg',
    'mpe' => 'video/mpeg',
    'qt' => 'video/quicktime',
    'mov' => 'video/quicktime',
    'avi' => 'video/x-msvideo',
    'movie' => 'video/x-sgi-movie',
    'doc' => 'application/msword',
    'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'word' => array('application/msword','application/octet-stream'),
    'xl' => 'application/excel',
    'json' => array('application/json','text/json')
);
?>