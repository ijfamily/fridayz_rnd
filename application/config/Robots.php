<?php
/**
 * Gear CMS
 *
 * Настройки посещаемости сайта роботами (создан: 2016-05-14 13:15:40)
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 *
 * @category   Gear
 * @package    Config
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Robots.php 2016-05-14 13:15:40 Gear Magic $
 */

return array(
    // максимально количество записей в журнале
    'LIMIT' => 10000,
    // список роботов
    'LIST'  => array(
        'Yandex' => array('name' => 'Yandex', 'access' => true),
        'Googlebot' => array('name' => 'Googlebot', 'access' => true),
        'Mediapartners-Google' => array('name' => 'Mediapartners-Google (Adsense)', 'access' => true),
        'Slurp' => array('name' => 'Hot;Bot;search', 'access' => true),
        'WebCrawler' => array('name' => 'WebCrawler;search', 'access' => true),
        'ZyBorg' => array('name' => 'Wisenut;search', 'access' => true),
        'scooter' => array('name' => 'AltaVista', 'access' => true),
        'StackRambler' => array('name' => 'Rambler', 'access' => true),
        'Aport' => array('name' => 'Aport', 'access' => true),
        'lycos' => array('name' => 'Lycos', 'access' => true),
        'WebAlta' => array('name' => 'WebAlta', 'access' => true),
        'yahoo' => array('name' => 'Yahoo', 'access' => true),
        'msnbot' => array('name' => 'msnbot/1.0', 'access' => true),
        'ia_archiver' => array('name' => 'Alexa search engine', 'access' => true),
        'FAST' => array('name' => 'AllTheWeb', 'access' => true)
    )
);
?>