<?php
/**
 * Gear CMS
 *
 * Настройка галереи изображений (создан: 2019-11-23 10:15:43)
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 *
 * @category   Gear
 * @package    Config
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Galleries.php 2019-11-23 10:15:43 Gear Magic $
 */

return array(
    // каталог файлов
    'DIR'                => 'data/galleries/',
    // генерировать название файла
    'GENERATE/FILE/NAME' => true,
    // количество изображений в списке
    'LIST/LIMIT'         => 2,

    // Изображения
    // размеры оригинального изображения
    'IMAGE/ORIGINAL/WIDTH'  => 0,
    'IMAGE/ORIGINAL/HEIGHT' => 0,
    'IMAGE/ORIGINAL/MACRO'  => 'resize{\"width\": 0, \"height\": 600}',
    // размеры миниатюры изображения
    'IMAGE/THUMB/WIDTH'     => 0,
    'IMAGE/THUMB/HEIGHT'    => 0,
    'IMAGE/THUMB/MACRO'     => 'resize{\"width\": 0, \"height\": 300}',
    // качество изображения
    'IMAGE/QUALITY'         => 100,

    // Водяной знак
    // использовать
    'WATERMARK'          => false,
    // положение штампа
    'WATERMARK/POSITION' => 'bottom-right'
);
?>