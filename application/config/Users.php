<?php
/**
 * Gear CMS
 *
 * Настройки пользователей сайта (создан: 2016-11-04 11:44:25)
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 *
 * @category   Gear
 * @package    Config
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Users.php 2016-11-04 11:44:25 Gear Magic $
 */

defined('_INC') or die;

return array(
    // разрешить восстановление аккаунта
    'RESTORE'        => false,
    // резрешить авторизацию и регистрацию
    'SIGNIN/ACCESS'  => true,
    // метод авторизации на сайте
    'SIGNIN/TYPE'    => 'login_or_e-mail',
    // подтверждение регистрации пользователя
    'SIGNUP/CONFIRM' => false,
    // уведомление администратора
    'SIGNUP/NOTIFICATION' => false,
    // авторизацию на сайте через социальные сети
    'SIGNIN/SOCIAL'  => true,
    // адаптер социальной сети
    'SOCIAL/ADAPTER' => 'uLogin',
    // регистрация нескольких пользователей с одного IP адреса
    'SIGNUP/ONE-IP'  => true,
    // выводить правила сайта при регистрации
    'SIGNUP/RULES'   => true,
    // код безопасности при авторизации
    'SIGNIN/CAPTCHA' => true,
    // код безопасности при регистрации
    'SIGNUP/CAPTCHA' => true,
    // максимальное количество зарегистрированных пользователей
    'SIGNUP/MAX'     => 0
);
?>