<?php
/**
 * Gear CMS
 *
 * Настройки альбомов (создан: 2020-06-29 21:48:50)
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 *
 * @category   Gear
 * @package    Config
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Albums.php 2020-06-29 21:48:50 Gear Magic $
 */

return array(
    // каталог файлов
    'DIR'                => 'data/albums/',
    // генерировать название файла
    'GENERATE/FILE/NAME' => true,
    // количество изображений в списке
    'LIST/LIMIT'         => 150,

    // Изображения
    // размеры оригинального изображения
    'IMAGE/ORIGINAL/WIDTH'  => 1920,
    'IMAGE/ORIGINAL/HEIGHT' => 1280,
    // размеры миниатюры изображения
    'IMAGE/THUMB/WIDTH'     => 330,
    'IMAGE/THUMB/HEIGHT'    => 400,
    // качество изображения
    'IMAGE/QUALITY'         => 80,

    // Водяной знак
    // использовать
    'WATERMARK'          => false,
    // положение штампа
    'WATERMARK/POSITION' => 'bottom-right'
);
?>