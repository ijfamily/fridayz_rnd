<?php
/**
 * Gear CMS
 *
 * Настройка маршрутизации запросов (создан: 2017-10-30 12:40:47)
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 *
 * @category   Gear
 * @package    Config
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Router.php 2017-10-30 12:40:47 Gear Magic $
 */

return array(
    // маршрутизация при AJAX запросах
    'request' => array(
        // Like
        'like' => array('desc' => 'Like','enabled' => true),
        // рейтинг материала
        'rating' => array('desc' => 'рейтинг материала','enabled' => true),
        // авторизация пользователей
        'signin' => array('desc' => 'авторизация пользователей','enabled' => true),
        // подсчёт визитов
        'visor' => array('desc' => 'подсчёт визитов','enabled' => true),
        // календарь
        'calendar' => array('desc' => 'календарь','enabled' => true)
    ),
    // маршрутизация запросов
    'route'   => array(
        // обратная связь
        'feedback' => array('alias' => 'feedback','value' => '/feedback/','size' => '0','type' => 'equal','desc' => 'обратная связь','enabled' => true),
        // страница партнёра
        'partner' => array('alias' => 'partner','value' => '','size' => '0','type' => 'none','desc' => 'страница партнёра','enabled' => true),
        // подтверждении регистрации
        'signup_confirm' => array('alias' => 'signup_confirm','value' => '/signup/confirm/','size' => '0','type' => 'equal','desc' => 'подтверждении регистрации','enabled' => true),
        // восстановление аккаунта
        'recovery_password' => array('alias' => 'recovery_password','value' => '/recovery-password/','size' => '0','type' => 'equal','desc' => 'восстановление аккаунта','enabled' => true),
        // восстановление аккаунта
        'remind_password' => array('alias' => 'remind_password','value' => '/remind-password/','size' => '0','type' => 'equal','desc' => 'восстановление аккаунта','enabled' => true),
        // профиль пользователя
        'user_profile' => array('alias' => 'user_profile','value' => '/user/profile/','size' => '0','type' => 'equal','desc' => 'профиль пользователя','enabled' => true),
        // информация о пользователе
        'user' => array('alias' => 'user','value' => '/user/','size' => '0','type' => 'equal','desc' => 'информация о пользователе','enabled' => true),
        // регистрация пользователей
        'signup' => array('alias' => 'signup','value' => '/signup/','size' => '0','type' => 'equal','desc' => 'регистрация пользователей','enabled' => true),
        // авторизация пользователей
        'signin' => array('alias' => 'signin','value' => '/signin/','size' => '0','type' => 'equal','desc' => 'авторизация пользователей','enabled' => false),
        // список статей в архиве
        'archive' => array('alias' => 'archive','value' => '','size' => '0','type' => 'none','desc' => 'список статей в архиве','enabled' => true),
        // капча
        'captcha' => array('alias' => 'captcha','value' => '/captcha/','size' => '0','type' => 'equal','desc' => 'капча','enabled' => true),
        // лента новостей
        'rss' => array('alias' => 'rss','value' => '/rss.xml','size' => '0','type' => 'equal','desc' => 'лента новостей','enabled' => true),
        // карта сайта для Yandex, Google
        'sitemap' => array('alias' => 'sitemap','value' => '/sitemap.xml','size' => '0','type' => 'equal','desc' => 'карта сайта для Yandex, Google','enabled' => true),
        // статья по идентификатору
        'articles' => array('alias' => 'articles','value' => 'articles','size' => '2','type' => 'first','desc' => 'статья по идентификатору','enabled' => true),
        // новость по идентификатору
        'news' => array('alias' => 'news','value' => 'news','size' => '2','type' => 'first','desc' => 'новость по идентификатору','enabled' => true),
        // список статей по короткому тегу
        'tag' => array('alias' => 'tag','value' => 'tag','size' => '2','type' => 'first','desc' => 'список статей по короткому тегу','enabled' => true),
        // альбом изображений
        'albums' => array('alias' => 'albums','value' => 'albums','size' => '2','type' => 'first','desc' => 'альбом изображений','enabled' => true),
        // поиск статей
        'search' => array('alias' => 'search','value' => '/search/','size' => '0','type' => 'equal','desc' => 'поиск статей','enabled' => true)
    )
);
?>