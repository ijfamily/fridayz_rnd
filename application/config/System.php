<?php
/**
 * Gear CMS
 *
 * Конфигурация системы
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 *
 * @category   Gear
 * @package    Config
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: System.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

return array(
    // dns имя сервера базы данных или ip адрес (127.0.0.1 или localhost)
    'DB/SERVER'   => 'localhost',
    // схема базы данных
    'DB/SCHEMA'   => 'fridayz_rnd',
    // логин для подключения к серверу базы данных
    'DB/USERNAME' => 'fridayz_rnd',
    // пароль для подключения к серверу базы данных
    'DB/PASSWORD' => 'gZF65c^D#Rsd',
    // порт подключения к серверу, по умолчанию (3306)
    'DB/PORT'     => 3306
);
?>