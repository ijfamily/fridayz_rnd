<?php
/**
 * Gear CMS
 *
 * Настройка списка Социальных сетей (создан: 2016-08-14 20:51:13)
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 *
 * @category   Gear
 * @package    Config
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Social.php 2016-08-14 20:51:13 Gear Magic $
 */

defined('_INC') or die;

return array(
    'Vkontakte' => array(
        'enabled' => true,
        'name'    => 'Вконтакте',
        'keys'    => array('id' => '5587232', 'secret' => 'APgwk6qgnXK92l64ODDJ')
    ),
    'OpenID' => array(
        'enabled' => false,
        'name'    => 'OpenID',
        'keys'    => array()
    ),
    'Yahoo' => array(
        'enabled' => false,
        'name'    => 'Yahoo',
        'keys'    => array('key' => '', 'secret' => '')
    ),
    'AOL' => array(
        'enabled' => false,
        'name'    => 'AOL',
        'keys'    => array()
    ),
    'Google' => array(
        'enabled' => false,
        'name'    => 'Google',
        'keys'    => array('id' => '', 'secret' => '')
    ),
    'Facebook' => array(
        'enabled' => false,
        'name'    => 'Facebook',
        'keys'    => array('id' => '', 'secret' => '')
    ),
    'Twitter' => array(
        'enabled' => false,
        'name'    => 'Twitter',
        'keys'    => array('key' => '', 'secret' => '')
    ),
    'Live' => array(
        'enabled' => false,
        'name'    => 'Live',
        'keys'    => array('id' => '', 'secret' => '')
    ),
    'LinkedIn' => array(
        'enabled' => false,
        'name'    => 'LinkedIn',
        'keys'    => array('key' => '', 'secret' => '')
    ),
    'Foursquare' => array(
        'enabled' => false,
        'name'    => 'Foursquare',
        'keys'    => array('id' => '', 'secret' => '')
    ),
    
);
?>