<?php
/**
 * Gear CMS
 *
 * Настройки подключения нескольких доменов (создан: 2016-06-09 15:41:24)
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 *
 * @category   Gear
 * @package    Config
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Domains.php 2016-06-09 15:41:24 Gear Magic $
 */

return array(
    'indexes' => array(),
    'domains' => array()
);
?>