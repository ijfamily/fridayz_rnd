<?php
/**
 * Gear CMS
 *
 * Общие настройки форм (создан: 2016-08-15 15:14:52)
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 *
 * @category   Gear
 * @package    Config
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Forms.php 2016-08-15 15:14:52 Gear Magic $
 */

return array(
    // Проверка на СПАМ
    // проверка ip адреса
    'CHECK_SPAM'    => false,
    // проверка "капчи"
    'CHECK_CAPTCHA' => true,
    // время простоя (сек.)
    'WAITING_TIME'  => 0,
    // количество сообщений
    'MAX_MESSAGES'  => 0,

    // SMS сообщение на телефон
    // отправить SMS сообщение на телефон
    'SEND_SMS' => false,
    // ресурс для отправки sms сообщений
    'SMS_URL'  => '',

    // Сообщение на e-mail
    // Отправить сообщение на e-mail
    'SEND_MAIL'    => true,
    // Тема письма приходящего от клиента
    'MAIL_SUBJECT' => 'Обратная связь',
    // E-mail адрес для отправленных сообщений
    'EMAILS'       => 'fridayz@gmail.com',

    // После успешной обработки формы
    // переход по адресу
    'LOCATION'    => '',
    // сообщение пользователю после успешной обработки формы
    'MSG_SUCCESS' => ''
);
?>