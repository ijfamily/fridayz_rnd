<?php
/**
 * Gear CMS
 *
 * Настройки сайта (создан: 29-06-2020 22:07:09)
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 *
 * @category   Gear
 * @package    Config
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Site.php 29-06-2020 22:07:09 Gear Magic $
 */

return array(
    // Общие настройки
    // название сайта
    'NAME'      => 'Fridayz',
    // домашняя страница сайта
    'HOME'      => 'http://www.rnd.fridayz.ru/',
    // название домена
    'HOST'      => 'www.rnd.fridayz.ru',
    // IP адрес вашего сервера
    'IP_ADDRESS'=> '127.0.0.1',
    // шаблон загаловка страницы
    'TITLE'     => '%s - #Fridayz',
    // кодировка страниц сайта
    'CHARSET'   => 'UTF-8',
    // часовой пояс
    'TIMEZONE'  => 'Europe/Kiev',
    // язык по умолчанию
    'LANGUAGE'       => 'ru-RU',
    'LANGUAGE/ID'    => 1,
    'LANGUAGE/ALIAS' => 'ru',
    // языки на сайте
    'LANGUAGES' => array(
        'ru-RU' => array('title' => 'Русский', 'id' => 1, 'alias' => 'ru')
    ),
    // шаблон сайта по умолчанию
    'THEME'     => 'light',
    // использовать ЧПУ
    'SEF'       => true,
    // тип ЧПУ
    'SEF/TYPE'  => 2,
    // если сайт не доступный - false
    'INACCESSIBLE'  => false,
    // ключевые слова (глобальные)
    'META/KEYWORDS'    => 'первый медийный онлайн журнал, журнал, медийный журнал,  развлечения, мероприятия, организации, 
организации л учреждения, новости, фотографии, профессиональные фото-галереи',
    // описание (глобальные)
    'META/DESCRIPTION' => 'первый медийный онлайн журнал  посвященный культурной и развлекательной жизни города, призванный предоставить всестороннюю и современную информации о жизни общества. Проект посвящен светским мероприятиям, выставкам и концертам, премьерам и презентациям, а также другим культурным событиям, заслуживающие внимание СМИи освещения в целях информирования молодой, активной части общества. На сайте можно найти широкий каталог заведений, со своей необходимой информацией о них. Проект Fridayz создан для максимального полного освещения столичной жизни не только посредством профессиональных фото-галерей, но и текстовых публикаций информационных материалов.',
    // вывод ошибок в шаблоне статьи
    'ERROR/ARTICLE'    => true,
    // вид редактора статьи
    'EDITOR/ARTICLE'   => 'tinymce',
    // вид редактора шаблона
    'EDITOR/TEMPLATE'  => 'tinymce',
    // вид редактора landing страницы
    'EDITOR/LANDING'   => 'text',
    // управление несколькими доменами:
    'OUTCONTROL'       => false,
    // отключить индексацию
    'NOTINDEX'         => false,

    // RSS лента
    // использовать RSS экспорт новостей
    'RSS'               => true,
    // тип экспорта RSS ленты
    'RSS/EXPORT/TYPE'   => 1,
    // количество экспортируемых статей
    'RSS/EXPORT/COUNT'  => 0,
    // Формат экспорта RSS ленты
    'RSS/EXPORT/FORMAT' => 1,
    // кэширование RSS ленты
    'RSS/CACHING'       => false,

    // Карта сайта для Google, Yandex, ...
    // использовать карту сайта
    'SITEMAP'                       => true,
    // количество записей
    'SITEMAP/COUNT'                 => 0,
    // кэширование карты сайта
    'SITEMAP/CACHING'               => true,
    // приоритетность статьи
    'SITEMAP/ARTICLES/PRIORITY'     => '0.5',
    // частота сканирования статьи
    'SITEMAP/ARTICLES/CHANGEFREQ'   => '',
    // приоритетность новости
    'SITEMAP/NEWS/PRIORITY'         => '0.5',
    // частота сканирования новости
    'SITEMAP/NEWS/CHANGEFREQ'       => '',
    // приоритетность категории
    'SITEMAP/CATEGORIES/PRIORITY'   => '0.5',
    // частота сканирования категории
    'SITEMAP/CATEGORIES/CHANGEFREQ' => '',

    // Безопасность
    // ключ безопасности
    'CMS/TOKEN'    => '1ac25c8baedb4e25def56c4334e25788',
    // разрешить AJAX запросы
    'AJAX'         => true,
    // задействовать капчу
    'CAPTCHA'      => true,
    // тип кода безопасности капчи
    'CAPTCHA/TYPE' => 'CAPTCHA',
    // настройка reCAPTCHA
    'reCAPTCHA'    => array(
        // публичный ключ сервиса reCAPTCHA
        'public_key'  => '',
        // секретный ключ сервиса reCAPTCHA
        'private_key' => '',
        // оформление reCAPTCHA
        'theme'       => '',
    ),
    // настройка kCAPTCHA
    'kCAPTCHA'         => array(
        // JPEG quality of CAPTCHA image (bigger is better quality, but larger file size)
        'jpegQuality'          => 90,
        // CAPTCHA image colors (RGB, 0-255)
        'foregroundColor'      => array(mt_rand(0,80), mt_rand(0,80), mt_rand(0,80)),
        'backgroundColor'      => array(228,234,237),
        //  set to false to remove credits line. Credits adds 12 pixels to image height
        'showСredits'          => false,
        'credits'              => 'www.fridayz.ru',
        // increase safety by prevention of spaces between symbols
        'noSpaces'             => false,
        // folder with fonts
        'fontsDir'             => 'fonts',
        // CAPTCHA image size (you do not need to change it, this parameters is optimal)
        'width'                => 150,
        'height'               => 71,
        // CAPTCHA string length random 5 or 6 or 7
        'length'               => mt_rand(5,7),
        '_length'              => '5,7',
        // KCAPTCHA configuration file (do not change without changing font files!)
        'alphabet'             => "0123456789abcdefghijklmnopqrstuvwxyz",
        // symbols used to draw CAPTCHA (alphabet without similar symbols (o=0, 1=l, i=j, t=f))
        'allowedSymbols'       => "23456789abcdegikpqsvxyz",
        // symbol's vertical fluctuation amplitude
        'fluctuationAmplitude' => 8,
        // noise no white noise
        'whiteNoiseDensity'    => 0.16,
        // noise no white noise no black noise
        'blackNoiseDensity'    => 0.033,
        // type file output
        'type'                 => 'PNG'
    ),

    // Список статей
    // список архива статей
    // вывод архива статей за указанный день
    'LIST/ARCHIVE/DAY'      => true,
    // вывод архива статей за указанный месяц
    'LIST/ARCHIVE/MONTH'    => true,
    // вывод архива статей за указанный год
    'LIST/ARCHIVE/YEAR'     => true,
    // шаблон
    'LIST/ARCHIVE/TEMPLATE' => 'list_archive.tpl.php',
    // список статей
    // записей на странице
    'LIST/LIMIT'    => 12,
    // порядок сортировки
    'LIST/ORDER'    => 'd',
    // критерий сортировки
    'LIST/SORT'     => 'date',
    // шаблон
    'LIST/TEMPLATE' => 'list_articles.tpl.php',
    // кэширование
    'LIST/CACHING'  => true,
    // формат даты
    'LIST/DATE/FORMAT' => 'j-m-Y, H:i',
    // навигация в списке
    'LIST/NAVIGATION'  => 'bottom',
    // вид списка
    'LIST/TYPES'       => array(
        'ListArticles'   => array('name' => 'Статьи категории', 'path' => 'list/articles/'),
        'Albums'         => array('name' => 'Фотоальбомы', 'path' => 'list/albums/'),
        'PartnersList'   => array('name' => 'Заведения', 'path' => 'partners/list/'),
        'VideoList'      => array('name' => 'Видеозаписи', 'path' => 'video/list/'),
        'ListCategories' => array('name' => 'Подкатегории в категории', 'path' => 'list/categories/')
    ),

    // Настройки E-Mail
    // e-mail адрес администратора
    'MAIL/ADMIN'         => 'noreply@fridayz.ru',
    // e-mail адрес уведомителя
    'MAIL/NOTIFICATION'  => 'svetlana.ageeva@gmail.com',
    // Заголовок отправителя писем, при отправке писем
    'MAIL/HEADER'        => '- медийный он-лайн журнал Fridayz.ru',
    // метод отправки почты
    'MAIL/TYPE'          => 'PHP mail',
    // SMTP хост
    'MAIL/SMTP/HOST'     => 'localhost',
    // SMTP порт
    'MAIL/SMTP/PORT'     => '25',
    // SMTP имя пользователя
    'MAIL/SMTP/LOGIN'    => '',
    // SMTP пароль
    'MAIL/SMTP/PASSWORD' => '',
    // использовать защищенный протокол
    'MAIL/SMTP/SECURE'   => '0',
    // SMTP e-mail для авторизации на SMTP сервере
    'MAIL/SMTP/MAIL'     => '',

    // Допустимые расширения файлов
    // для изображений
    'FILES/EXT/IMAGES' => 'JPG,JPEG,PNG,GIF,ICO',
    // для документов
    'FILES/EXT/DOCS'   => 'DOC,DOCX,XLS,XLSX,PDF,RTF,TXT,ZIP,RAR,TTF,OTF,EOT,SVG,WOFF',
    // для аудио
    'FILES/EXT/AUDIO'  => 'MP3,WAV,OGG',
    // для видео
    'FILES/EXT/VIDEO'  => 'FLV,AVI,MP4',

    // Каталоги файлов
    // каталог тем
    'DIR/THEMES'     => 'themes/',
    // каталог данных сайта
    'DIR/DATA'       => 'data/',
    // аудио
    'DIR/AUDIO'      => 'data/audio/',
    // видео
    'DIR/VIDEO'      => 'data/video/',
    // документы
    'DIR/DOCS'       => 'data/docs/',
    // изображения банеров
    'DIR/BANNERS'    => 'data/banners/',
    // изображения слайдеров
    'DIR/SLIDERS'    => 'data/sliders/',
    // изображения в статьях
    'DIR/IMAGES'     => 'data/images/',
    // изображения категорий
    'DIR/CATEGORIES' => 'data/categories/',
    // временный каталог
    'DIR/TEMP'       => 'data/temp/',

    // Изображения
    // размеры оригинального изображения
    'IMAGE/ORIGINAL/SIZE' => '890x0',
    // размеры средний копии изображения
    'IMAGE/MEDIUM/SIZE'   => '420x0',
    // размеры миниатюры изображения
    'IMAGE/THUMB/SIZE'    => '300x0',
    // качество изображения
    'IMAGE/QUALITY'       => 100,

    // Оптимизация
    // включить кэширование
    'CACHE'                 => true,
    // тип кеширования
    'CACHE/TYPE'            => 'filecache',
    // настройки подключения к серверу Memcache
    'CACHE/MEMCACHE/SERVER' => 'localhost',
    // каталог файлов кэша
    'CACHE/DIR'             => 'cache/',
    // включить Gzip сжатие HTML страниц
    'GZIP'                  => true,
    // использовать отладчик
    'DEBUG'                 => true,
    // включить разметку Open Graph
    'OPENGRAPH'             => true,
    // одна картинка для всех статей в Open Graph разметке
    'OPENGRAPH/IMAGE'       => '',
    // навигация по страницам
    'BREADCRUMBS'           => true,
    // выводить нулевой уровень навигации
    'BREADCRUMBS/ZERO'      => true,
    // работа с глобальными тегами
    'GLOBAL/TAGS'           => true,
    // включить календарь
    'CALENDAR'              => true,
    // подсветка дней в календаре
    'CALENDAR/HIGHLIGHT'    => false,
    // вести статистику посещения сайта роботами
    'ROBOTS'                => true,
    // вести подсчет посещаемости страниц сайта
    'CONSIDER/VISITS'       => true,
    // вести подсчет посещаемости страниц сайта
    'BANNERS'               => true,
    // поиск статей по коротким тегам
    'ARTICLE/TAGS'          => true,
    // использовать канонические ссылки
    'CANONICAL'             => true,

    // Водяной знак
    // файл штампа
    'WATERMARK/STAMP'    => 'data/images/stamp.png',
    // положение штампа
    'WATERMARK/POSITION' => 'bottom-right',

    // Справка
    // ресурс справки
    'GUIDE/RESOURCE' => 'http://cms.gearmagic.ru/guide/v3_0/',
    // каталог разделов
    'GUIDE/TREE'     => 'http://cms.gearmagic.ru/guide/v3_0/output/tree.json'
);
?>