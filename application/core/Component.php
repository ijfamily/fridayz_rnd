<?php
/**
 * Gear CMS
 *
 * Компонент
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Core
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Component.php 2013-05-01 16:00:00 Gear Magic $
 */

defined('_INC') or die('Restricted access');

/**
 * Базовый класс компонента
 * 
 * так выглядит тег компонента в статье:
 * <component
 *  id = string
 *  class = string
 *  path = string
 *  template = string
 * >
 * 
 * @category   Gear
 * @package    Core
 * @copyright  Copyright (c) 2012-2015 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Component.php 2015-07-01 16:00:00 Gear Magic $
 */
class GComponent
{
    /**
     * Все атрибуты компонента
     *
     * @var array
     */
    public $attributes = array();

    /**
     * Поиск тега в ресурсе (шаблон, текст, файл)
     *
     * @var string
     */
    public $tagSearch = 'component';

    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = '';

    /**
     * Обработчик DOM полученной из шаблона
     *
     * @var object
     */
    public $dom = null;

    /**
     * Шаблон (текст html)
     *
     * @var string
     */
    public $html = '';

    /**
     * Идентификатор компонента
     *
     * @var string
     */
    public $id = '';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = '';

    /**
     * Класс компонента
     *
     * @var string
     */
    public $class = '';

    /**
     * Принадлежность компонента (article, scheme, template)
     *
     * @var string
     */
    public $belongTo = '';

    /**
     * Компонент в режиме "демонстрации"
     * 
     * @var boolean
     */
    public $designMode = false;

    /**
     * Интерфейс управления компонентом в режиме конструктора
     *
     * @var array
     */
    public $designControl = array();

    /**
     * Whether to use the design mode for the component
     * 
     * @var boolean
     */
    public $useDesignMode = false;

    /**
     * Экземпляр класса  модели данных компонента
     * 
     * @var object
     */
    public $model;

    /**
     * Использовать AJAX запросы
     *
     * @var boolean
     */
    public $useAjax = false;

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        // атрибуты компонента
        $this->attributes = $attr;
        // шаблон компонента
        $template = $this->get('template');
        if ($template)
            $this->template = $template;
        // идентификатор компонента
        $this->id = $this->get('id', $this->id);
        // название компонента
        $this->name = $this->get('name', $this->name);
        // использовать AJAX запросы
        $this->useAjax = $this->get('use-ajax', $this->useAjax);
    }

    /**
     * Устанавливает атрибут компонента ($attributes)
     *
     * @params array $name название атрибута
     * @params array $value значение, если null удаляет атрибут
     * @return mixed
     */
    public function set($name, $value = null)
    {
        if ($value == null) {
            unset($this->attributes[$name]);
        } else
            $this->attributes[$name] = $value;
    }

    /**
     * Возращает атрибуты компонента (из $attributes)
     *
     * @params array $name название атрибута
     * @params array $default значение по умолчанию, если нет $name
     * @return mixed
     */
    public function get($name, $default = '')
    {
        if (isset($this->attributes[$name]))
            return $this->attributes[$name];
        else
            return $default;
    }

    /**
     * Возвращает экземпляр класса по атрибутам тега компонента
     *
     * @param  array $attr массив атрибутов тега компонента
     * @param  string $content внутрений текст тега компонента
     * @return mixed
     */
    public static function factory($attr = array(), $content = '', $belongTo = '')
    {
        // если существует атрибут тега "class"
        try {
            if (empty($attr['class']))
                throw new GException('Component error', 'The component class is empty');
            // название класса компонента
            $className = $attr['class'];
            // если существует атрибут тега "path" (пут к компоненту)
            if (empty($attr['path']))
                throw new GException('Component error', 'The component path is empty', $className);
            $path = './' . PATH_COMPONENT .  $attr['path'];
            // if file of component exist
            $fileName = $path . 'component.php';
            // если существует файл компонента
            if (!file_exists($fileName)) {
                throw new GException('Component error', 'The component "%s" is not found in the component list', $className . ' (path: ' . $fileName . ')');
            }
            // подключение языка - {component}/languages/{language}/Translator.php
            // WARNING: 
            GText::addText($attr['path']);
            // подключение компонента - {component}/component.php
            require_once($fileName);
            // если класс компонента не существует
            if (!class_exists($className))
                throw new GException('Component error', 'The component class "%s" does not exist', $className, $this);
            // если документ или страница содержит атрибуты компонентов
            if (!isset($attr['id']))
                $attr['id'] = '';
            if ($belongTo == 'template') {
                if (($сattr = Gear::$app->page->getComponentAttrs($attr['id'], $attr)) !== false)
                    $attr = $сattr;
            } else {
                if (($сattr = Gear::$app->document->getComponentAttrs($attr['id'], $attr)) !== false) {
                    $attr = $сattr;
                }
            }
            
        } catch(GException $e) {}

        // создаём класс компонента
        $cmp = new $className($attr);
        $cmp->belongTo = $belongTo;
        $cmp->designMode = Gear::$app->designMode;
        if (isset($cmp->model)) {
            // если есть модель, делаем инициализацию модели по атрибутам
            if ($cmp->model) {
                $cmp->model->construct();
            }
        }

        return $cmp;
    }

    /**
     * Валидация компонента перед выводом данных, если компонент не прошел
     * валидацию - данные компонента не выводятся
     * 
     * @return boolean
     */
    public function isValid()
    {
        return true;
    }

    /**
     * Инициализация компонента
     * 
     * @return void
     */
    protected function initialise()
    {
        // использовать ajax запросы
        $this->_data['use-ajax'] = $this->useAjax;
        // если главная страница
        $this->_data['is-root'] = Gear::$app->url->isRoot();
        // идентификатор записи
        $this->_data['data-id'] = $this->get('data-id');
        // идентификатор шаблона статьи или страницы
        if ($this->belongTo == 'template')
            $this->_data['template-id'] = Gear::$app->page->templateId;
        else
            $this->_data['template-id'] = Gear::$app->document->templateId;
        // идентификатор страницы
        $this->_data['page-id'] = Gear::$app->document->pageId;
        // идентификатор статьи
        $this->_data['article-id'] = Gear::$app->document->id;
        // принадлежность компонента
        $this->_data['belong-to'] = $this->belongTo;
        // компонент в режиме "демонстрации"
        $this->_data['design-mode'] = $this->useDesignMode && $this->designMode;
        if ($this->_data['design-mode'])
            $this->_data['design-control'] = $this->designControl;
    }

    /**
     * Возращает текст шаблона с определением модели DOM
     * 
     * @return string
     */
    public function getHtml()
    {
        $this->dom = str_get_html($this->html);
        $this->updateTags();

        return $this->dom;
    }

    /**
     * Возращает текст шаблона с определением модели DOM
     * 
     * @return string
     */
    protected function getTemplate()
    {
        $filename = './' . PATH_TEMPLATE . Gear::$app->config->get('THEME') . '/' . GFactory::getLanguage('alias') . '/' . $this->template;
        try {
            if (file_exists($filename)) {
                $this->dom = file_get_html($filename);
                $this->updateTags();
                return $this->dom;
            } else
                throw new GException('Component error', 'Unable to load template "%s"', $filename);
        } catch(GException $e) {}
    }

    /**
     * Вывод данных компонента в буфер (без участия шаблонов) перед его рендерингом.
     * Оптимально для компонентов, которым нет необходимости использовать шаблоны.
     * (для изъятия из буфера используется "getContent" -> "render")
     * 
     * @return void
     */
    protected function content()
    {}

    /**
     * Добавление JS и CSS объявлений компонента в документ
     *
     * @params object $doc документ
     * @return void
     */
    public function scripts($doc)
    {}

    /**
     * Возращает контент компонента
     * 
     * @return string
     */
    public function getContent()
    {
        // валидация компонента
        if (!$this->isValid()) return false;
        // добавление JS и CSS объявлений компонента в документ
        $this->scripts(Gear::$app->document);
        // возращает текст шаблона с определением модели DOM
        if ($this->html)
            return $this->getHtml();
        if ($this->template)
            return $this->getTemplate();
        // включение буферизации вывода
        ob_start();
        $this->content();
        $html = ob_get_contents();
        // удаление выходного буфера и выключение буферизации вывода
        ob_end_clean();

        return $html;
    }

    /**
     * Вывод данных
     * 
     * @return void
     */
    public function render()
    {
        // если компонент не в режиме "конструктора"
        if (!($this->useDesignMode && $this->designMode))
            // если компонент скрыт
            if ($this->get('hidden')) return;

        // инициализация данных
        $this->initialise();
        // если контент компонента прошел валидацию
        if (($content = $this->getContent()) === false) return;
        echo $content;
    }

    /**
     * Создание компонента на основе DOM модели
     * 
     * @param  object $tag один из узлов DOM модели
     * @return void
     */
    protected function updateTag($tag)
    {
        if ($cmp = self::factory($tag->attr, $tag->innertext, 'article'))
            if (is_object($cmp)) {
                $cmp->render();
            } else
                try {
                    throw new GException('Component error', $cmp);
                } catch(GException $e) {}
        else
            try {
                throw new GException('Component error', 'Can`t connect the component "%s", incorrectly specified path "%s"',
                                     array((isset($tag->attr['class']) ? $tag->attr['class'] : ''), 
                                     (isset($tag->attr['path']) ? $tag->attr['path'] : '')), $this);
            } catch(GException $e) {}
    }

    /**
     * Обновление DOM модели на основе созданных в ней компонентов
     * 
     * @return void
     */
    protected function updateTags()
    {
        if ($this->dom) {
            foreach ($this->dom->find($this->tagSearch) as $tag) {
                GApplication::$components[] = $tag->class;
                // включение буферизации вывода
                ob_start();
                // создание компонента на основе DOM модели
                $this->updateTag($tag);
                // замена текста DOM объекта данными компонента
                $tag->outertext = ob_get_contents();
                // удаление выходного буфера и выключение буферизации вывода
                ob_end_clean();
            }
        }
    }
}


/**
 * Самостоятельно вызываемый компонент (используется для построения сайта основанных не на статье,
 * а на конструкции с преобразованными данными, где данные будут представлены в виде статьи)
 * 
 * @category   Gear
 * @package    Core
 * @copyright  Copyright (c) 2011-2013 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Component.php 2013-05-01 16:00:00 Gear Magic $
 */
class GComponentOverlay extends GComponent
{
    /**
     * Using article template for output component data (default "true")
     *
     * @var boolean
     */
    protected $_isUseArticleTemplate = true;

    /**
     * Вывод данных
     * 
     * @return void
     */
    public function render()
    {
        // инициализация данных
        $this->initialise();
        // если контент компонента прошел валидацию
        if (($content = $this->getContent()) === false) return;
        // включение буферизации вывода
        ob_start();
        echo $content;
        $doc = GFactory::getDocument();
        $doc->isUseTemplate = $this->_isUseArticleTemplate;
        $doc->substitute = true;
        $doc->setText(ob_get_contents());
        // удаление выходного буфера и выключение буферизации вывода
        ob_end_clean();
    }
}


/**
 * Маршрутизатор компонентов
 * 
 * @category   Gear
 * @package    Core
 * @copyright  Copyright (c) 2011-2013 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Component.php 2013-05-01 16:00:00 Gear Magic $
 */
class GComponentRouter
{
    /**
     * Указатель экземпляра класса URL
     *
     * @return object
     */
    public $url = null;

    /**
     * Указатель экземпляра класса документа
     *
     * @return object
     */
    public $document = null;

    /**
     * Указатель экземпляра класса настроеек
     *
     * @return object
     */
    public $config = null;

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента (включая $params)
     * @params array $params атрибуты компонента ("params{...}") установленные в редакторе
     * @return void
     */
    public function __construct()
    {
        // документ
        $this->document = GFactory::getDocument();
        // обработчик строки запроса
        $this->url = GFactory::getURL();
        // настройки
        $this->config = Gear::$app->config;
        $this->list = Gear::$app->config->getFrom('ROUTER');
    }

    /**
     * Маршрут по запросу
     * 
     * @param  string $settings настройки маршрута
     * @return boolean если маршрут выбран - true
     */
    public function routeTo($settings)
    {
        $filename = './' . PATH_ROUTE . $settings['alias'] . '.php';
        if (!file_exists($filename))
            return false;

        require_once($filename);

        return call_user_func('route_to_' . $settings['alias'], $settings, $this->document, $this->config);
    }

    /**
     * Маршрут по AJAX запросу
     * 
     * @param  string $settings настройки маршрута
     * @param  string $alias название запроса
     * @return boolean если маршрут выбран - true
     */
    public function requestTo($settings, $alias)
    {
        $filename = './' . PATH_ROUTE . $alias . '.php';
        if (!file_exists($filename))
            return false;

        require_once($filename);

        return call_user_func('request_to_' . $alias, $settings, $this->document, $this->config);
    }

    /**
     * Вызывает один из методов маршрутизатора
     * 
     * @param  string $router название метода
     * @return void
     */
    public function call($router)
    {
        return $this->$router();
    }

    /**
     * Вызывается при AJAX запросе
     * 
     * @return void
     */
    protected function ajaxInit()
    {}

    /**
     * Вызывается до инициализации данных приложения
     * 
     * @return void
     */
    protected function breforeInit()
    {}

    /**
     * Вызывается после инициализации данных приложения
     * 
     * @return void
     */
    protected function afterInit()
    {}
}


/**
 * Маршрутизатор компонентов
 * 
 * @category   Gear
 * @package    Core
 * @copyright  Copyright (c) 2011-2013 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Component.php 2013-05-01 16:00:00 Gear Magic $
 */
class GComponentRest
{
    /**
     * Экземпляр класса модели данных компонента
     * 
     * @var object
     */
    public $model;

    /**
     * Вывод данных
     * 
     * @return void
     */
    public function render()
    {}
}


/**
 * Компонент для AJAX запросов
 * 
 * 
 * @category   Gear
 * @package    Core
 * @copyright  Copyright (c) 2011-2013 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Component.php 2013-05-01 16:00:00 Gear Magic $
 */
class GComponentAjax
{
    /**
     * Атрибуты компонента
     *
     * @var array
     */
    public $attributes = array();

    /**
     * Атрибуты компонента по умолчанию (если атрибуты не "пришли" из редактора в админке или из 
     * тега компонента)
     *
     * @var array
     */
    public $defAttributes = array();

    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = '';

    /**
     * Идентификатор компонента
     *
     * @var string
     */
    public $id = '';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = '';

    /**
     * Экземпляр класса  модели данных компонента
     * 
     * @var object
     */
    public $model = null;

    /**
     * Данные используемые в шаблоне $template
     *
     * @var mixed
     */
    protected $_data = array();

    /**
     * Путь к компоненту
     *
     * @var string
     */
    public $path = '';

    /**
     * Локализация языка
     *
     * @var array
     */
    public $_ = array();

    /**
     * Данные возращаемые клиенту
     *
     * @var array
     */
    protected $_response = array('success' => true, 'message' => '', 'html' => '', '_st' => '');

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        // если есть атрибуты по умолчанию
        if ($this->defAttributes)
            $this->attributes = array_merge($this->defAttributes, $attr);
        else
            // атрибуты компонента
            $this->attributes = $attr;

        // обработчик запросов к форме
        $this->input = GFactory::getApplication()->input;

        $this->articleId = $this->input->get('aid', 0);
        $this->showContent = $this->input->get('get', '') == 'content';
        // идентификатор компонента
        $this->id = $this->input->get('id', '');
        // атрибуты компонента
        //$this->attributes = $this->loadAttributes($this->articleId, $this->id);
        // путь к компоненту
        $this->path = $this->get('path', $this->path);
        // шаблон компонента
        $template = $this->get('template');
        if ($template)
            $this->template = $template;
        // название компонента
        $this->name = $this->get('name', $this->name);

        $this->_ = GText::getSection($this->path);
    }

    /**
     * Возращает атрибуты компонента из компонентов статьи
     *
     * @params integer $articlId идент. статьи
     * @params integer $componentId идент. компонента
     * @return mixed
     */
    protected function loadAttributes($articlId, $componentId)
    {
        $data = GArticles::getById($articlId, Gear::$app->language->get('id'), 0, null, false);
        if ($data) {
            $componentAttrs = json_decode($data['component_attributes'], true);
            if (isset($componentAttrs[$componentId])) {
                $attr = $componentAttrs[$componentId];
                $attr['id'] = $componentId;
                return $attr;
            }
        }

        return array();
    }

    /**
     * Возращает атрибуты компонента (из $attributes)
     *
     * @params array $name название атрибута
     * @params array $default значение по умолчанию, если нет $name
     * @return mixed
     */
    public function get($name, $default = '')
    {
        if (isset($this->attributes[$name]))
            return $this->attributes[$name];
        else
            return $default;
    }

    /**
     * Устанавливает атрибуты компонента ($attributes)
     *
     * @params array $name название атрибута
     * @params array $value значение по умолчанию, если нет $name
     * @return mixed указатель на объект
     */
    public function set($name, $value)
    {
        $this->attributes[$name] = $value;

        return $this;
    }

    /**
     * Валидация компонента перед выводом данных, если компонент не прошел
     * валидацию - данные компонента не выводятся
     * 
     * @return boolean
     */
    public function isValid()
    {
        return true;
    }

    /**
     * Инициализация компонента
     * 
     * @return void
     */
    protected function initialise()
    {
        // идентификатор статьи
        $this->_data['article-id'] = $this->articleId;
        // атрибуты компонента
        $this->_data['attr'] = $this->attributes;
    }

    /**
     * Вывод шаблона
     * 
     * @return void
     */
    public function template()
    {
        GFactory::getTemplate($this->name, $this->_data, $this->template);
    }

    /**
     * Возращает контент компонента
     * 
     * @return string
     */
    public function getContent()
    {
        // валидация компонента
        if (!$this->isValid()) return false;
        // инициализация данных
        $this->initialise();
        // включение буферизации вывода
        ob_start();
        $this->template();
        $html = ob_get_contents();
        // удаление выходного буфера и выключение буферизации вывода
        ob_end_clean();

        return $html;
    }

    /**
     * Вывод данных
     * 
     * @return void
     */
    public function render()
    {
        // если контент компонента прошел валидацию
        if (($content = $this->getContent()) === false)
            $this->_response['success'] = false;
        else
            $this->_response['html'] = $content;
        // вывод данных компонента
        die(json_encode($this->_response));
    }
}


/**
 * Базовый класс "лёгкого" компонента
 * 
 * @category   Gear
 * @package    Core
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Component.php 2013-11-10 16:00:00 Gear Magic $
 */
class GComponentLight
{
    /**
     * Атрибуты компонента ("приходят" из редактора в админке или из тега компонента)
     *
     * @var array
     */
    public $attributes = array();

    /**
     * Атрибуты компонента по умолчанию (если атрибуты не "пришли" из редактора в админке или из 
     * тега компонента)
     *
     * @var array
     */
    public $defAttributes = array();

    /**
     * Файл шаблона (example.tpl)
     *
     * @var string
     */
    public $template = '';

    /**
     * Идентификатор компонента
     *
     * @var string
     */
    public $id = '';

    /**
     * Название компонента (для идентификации в шаблоне)
     *
     * @var string
     */
    public $name = '';

    /**
     * Класс компонента
     *
     * @var string
     */
    public $class = '';

    /**
     * Принадлежность компонента (article, scheme, template)
     *
     * @var string
     */
    public $belongTo = '';

    /**
     * Компонент в режиме "демонстрации"
     * 
     * @var boolean
     */
    public $designMode = false;

    /**
     * Whether to use the design mode for the component
     * 
     * @var boolean
     */
    public $useDesignMode = true;

    /**
     * Экземпляр класса  модели данных компонента
     * 
     * @var object
     */
    public $model = null;

    /**
     * Данные используемые в шаблоне $template
     *
     * @var mixed
     */
    protected $_data = array();

    /**
     * Использовать AJAX запросы
     *
     * @var boolean
     */
    public $useAjax = false;

    /**
     * Работа ЧПУ
     *
     * @var boolean
     */
    public $sef = false;

    /**
     * Путь к компоненту
     *
     * @var string
     */
    public $path = '';

    /**
     * Локализация языка
     *
     * @var array
     */
    public $_ = array();

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        // если есть атрибуты по умолчанию
        if ($this->defAttributes)
            $this->attributes = array_merge($this->defAttributes, $attr);
        else
            // атрибуты компонента
            $this->attributes = $attr;
        // шаблон компонента
        $template = $this->get('template');
        if ($template)
            $this->template = $template;
        // идентификатор компонента
        $this->id = $this->get('id', $this->id);
        // название компонента
        $this->name = $this->get('name', $this->name);
        // использовать ajax запросы
        $this->useAjax = $this->get('use-ajax', $this->useAjax);
        // путь к компоненту
        $this->path = $this->get('path', $this->path);
        // работает ли ЧПУ
        $this->sef = Gear::$app->config->get('SEF');
        // псевдоним языка (ru, en, uk)
        $this->_data['language'] = Gear::$app->language->get('alias');

        // шаблонные данные
        //работает ли ЧПУ
        $this->_data['sef'] = Gear::$app->config->get('SEF');
        // переменные компонента
        $this->_data['vars'] = array();
        // использовать ajax
        $this->_data['use-ajax'] = $this->useAjax;
        $this->_ = GText::getSection($this->path);
    }

    /**
     * Устанавливает атрибут компонента ($attributes)
     *
     * @params array $name название атрибута
     * @params array $value значение, если null удаляет атрибут
     * @return mixed
     */
    public function set($name, $value = null)
    {
        if ($value == null) {
            unset($this->attributes[$name]);
        } else
            $this->attributes[$name] = $value;
    }

    /**
     * Возращает атрибуты компонента (из $attributes)
     *
     * @params array $name название атрибута
     * @params array $default значение по умолчанию, если нет $name
     * @return mixed
     */
    public function get($name, $default = '')
    {
        if (isset($this->attributes[$name]))
            return $this->attributes[$name];
        else
            return $default;
    }

    /**
     * Возвращает экземпляр класса по атрибутам тега компонента
     *
     * @param  array $attr массив атрибутов тега компонента
     * @param  string $content внутрений текст тега компонента
     * @return mixed
     */
    public static function factory($attr = array(), $content = '', $belongTo = '')
    {
        // если существует атрибут тега "class"
        try {
            if (empty($attr['class']))
                throw new GException('Component error', 'The component class is empty');
            // название класса компонента
            $className = $attr['class'];
            // если существует атрибут тега "path" (пут к компоненту)
            if (empty($attr['path']))
                throw new GException('Component error', 'The component path is empty', $className);
            $path = './' . PATH_COMPONENT .  $attr['path'];
            // if file of component exist
            $fileName = $path . 'component.php';
            // если существует файл компонента
            if (!file_exists($fileName)) {
                throw new GException('Component error', 'The component "%s" is not found in the component list', $className, $this);
            }
            // подключение языка - {component}/languages/{language}/Text.php
            // WARNING:
            GText::addText($attr['path']);
            //$translator = require($path . '/languages/' . GFactory::getLanguage('alias') . '/Text.php');
            // подключение компонента - {component}/component.php
            require_once($fileName);
            // если класс компонента не существует
                if (!class_exists($className))
                    throw new GException('Component error', 'The component class "%s" does not exist', $className, $this);
            // если документ или страница содержит атрибуты компонентов
            if ($belongTo == 'template') {
                if (($сattr = Gear::$app->page->getComponentAttrs($attr['id'], $attr)) !== false)
                    $attr = $сattr;
            } else {
                if (($сattr = Gear::$app->document->getComponentAttrs($attr['id'], $attr)) !== false) {
                    $attr = $сattr;
                }
            }
        } catch(GException $e) {}

        // создаём класс компонента
        $cmp = new $className($attr);
        $cmp->belongTo = $belongTo;
        // если есть модель, делаем инициализацию модели по атрибутам
        if ($cmp->model) {
            $cmp->model->construct();
        }

        return $cmp;
    }

    /**
     * Валидация компонента перед выводом данных, если компонент не прошел
     * валидацию - данные компонента не выводятся
     * 
     * @return boolean
     */
    public function isValid()
    {
        // атрибут "for" используется для валидации компонента по условию запроса
        $for = $this->get('for', false);
        if ($for) {
            // для сравнения
            $forData = $this->get('for-data');
            // для отрицания
            $forDataNo = $this->get('for-data-no');
            if ($forData || $forDataNo) {
                switch ($for) {
                    // если указан url ("/folder1/folder2/file.html")
                    case 'url': 
                        if ($forData)
                            return Gear::$app->url->path == $forData;
                        else
                        if ($forDataNo)
                            return Gear::$app->url->path != $forData;

                    // если главная страница
                    case 'page':
                        if ($forData)
                            return $forData == 'index' && Gear::$app->url->isRoot();
                        else
                        if ($forDataNo)
                            return $forDataNo == 'index' && !Gear::$app->url->isRoot();

                    // если дата совпадает
                    case 'date':
                        if ($forData)
                            return $forData == date('d-m-Y');
                        else
                        if ($forDataNo)
                            return $forDataNo != date('d-m-Y');

                    // если главная страница
                    case 'category':
                        if (!empty(Gear::$app->category)) {
                            if ($forData)
                                return $forData == Gear::$app->category['category_id'];
                            else
                            if ($forDataNo)
                                return $forDataNo != Gear::$app->category['category_id'];
                        } else
                            return false;
                }
            }
        }

        return true;
    }

    /**
     * Валидация данных компонента после их инициализации
     * если false - компонент не выводится
     * 
     * @return boolean
     */
    public function isValidInitialise()
    {
        return true;
    }

    /**
     * Возращает "open" тег компонента (для режима "конструктора")
     * 
     * @return string
     */
    protected function getDesignTagOpen()
    {
        return <<<HTML
        <section role="component" id="{$this->_data['attr']['id']}" class="c-position-rt">
            <control id="ctrl-{$this->_data['attr']['id']}" title="Настройка компонента" type="control"></control>
            <script type="text/javascript">
                (function(w, n, id) {
                    w[n] = w[n] || [];
                    w[n].push({
                        id: id,
                        className: "{$this->_data['attr']['class']}",
                        templateId: {$this->_data['template-id']},
                        dataId: {$this->_data['data-id']},
                        articleId: {$this->_data['article-id']},
                        pageId: {$this->_data['page-id']},
                        belongTo: "{$this->_data['belong-to']}",
                        menu: {
                            "add": {name: "Добавить статью", icon: "add-document", url: "site/articles/articles/~/profile/interface/"},
                            "edit": {name: "Редактировать", icon: "edit", url: "site/articles/articles/~/profile/interface/"},
                            "list": {name: "Статьи", icon: "documents", url: "site/articles/articles/~/grid/interface/"},
                            "property": {name: "Свойства", icon: "property", url: "site/design/~/component/interface/"}
                        }
                    });
                })(window, "gearComponents", "{$this->_data['attr']['id']}");
            </script>
            <content>
HTML;
    }

    /**
     * Возращает "close" тег компонента (для режима "конструктора")
     * 
     * @return string
     */
    protected function getDesignTagClose()
    {
        return '</content>' . _n . '</section>';
    }

    /**
     * Возращает тег управления элементами компонента (для режима "конструктора")
     * 
     * @return string
     */
    protected function getDesignTagControl()
    {
        return <<<HTML
            <control id="ctrl-{$this->_data['attr']['id']}-item-%s" class="c-control-i c-position-none" title="Настройка элемента" type="item"></control>
            <script type="text/javascript">
                (function(w, n, id) {
                    w[n] = w[n] || [];
                    w[n].push({ id: id, dataId: '%s', menu: { "edit": {name: "Редактировать", icon: "edit", url: "site/articles/articles/~/profile/interface/"} }});
                })(window, "gearComponents", "{$this->_data['attr']['id']}-item-%s");
            </script>
HTML;
    }

    /**
     * Инициализация компонента
     * 
     * @return void
     */
    protected function initialise()
    {
        // атрибуты компонента
        $this->_data['attr'] = $this->attributes;
        // категория статей
        $this->_data['category-id'] = 0;
        $this->_data['category-image'] = '';
        
        if (Gear::$app->category) {
            $this->_data['category-id'] = Gear::$app->category['category_id'];
            $this->_data['category-image'] = Gear::$app->category['category_image'];
        }
        // использовать ajax запросы
        $this->_data['use-ajax'] = $this->useAjax;
        // если главная страница
        $this->_data['is-root'] = Gear::$app->url->isRoot();
        // идентификатор записи
        $this->_data['data-id'] = $this->get('data-id', 0);
        // идентификатор шаблона статьи или страницы
        if ($this->belongTo == 'template')
            $this->_data['template-id'] = Gear::$app->page->templateId;
        else
            $this->_data['template-id'] = Gear::$app->document->templateId;
        // идентификатор страницы
        $this->_data['page-id'] = Gear::$app->document->pageId;
        // идентификатор статьи
        $this->_data['article-id'] = Gear::$app->document->id;
        // принадлежность компонента
        $this->_data['belong-to'] = $this->belongTo;
        // компонент в режиме "конструктора"
        $this->_data['design-mode'] = $this->useDesignMode && $this->designMode;
        // название хоста
        $this->_data['host'] = Gear::$app->url->host;
        // тема
        $this->_data['theme'] = Gear::$app->config->get('THEME');
        // путь к теме 
        $this->_data['theme/'] = $this->_data['host'] . '/themes/' . $this->_data['theme'];
        // путь к изображениям в теме
        $this->_data['theme/images/'] = $this->_data['host'] . '/themes/' . $this->_data['theme'] . '/images/';
        // путь к изображениям статьи
        $this->_data['data/images/'] = $this->_data['host'] . Gear::$app->config->get('DIR/IMAGES');
        // создание экрана для тега компонента (для режима "конструктора")
        if ($this->_data['design-mode']) {
            //$this->_data['design-control'] = $this->designControl;
            $this->_data['design-tag-open'] = $this->getDesignTagOpen();
            $this->_data['design-tag-close'] = $this->getDesignTagClose();
            $this->_data['design-tag-control'] = $this->getDesignTagControl();
        } else {
            $this->_data['design-tag-open'] = '';
            $this->_data['design-tag-close'] = '';
            $this->_data['design-tag-control'] = '';
        }
    }

    /**
     * Вывод шаблона
     * 
     * @return void
     */
    public function template()
    {
        GFactory::getTemplate($this->name, $this->_data, $this->template);
    }

    /**
     * Добавление JS и CSS объявлений компонента в документ
     *
     * @params object $doc документ
     * @return void
     */
    public function scripts($doc)
    {}

    /**
     * Возращает контент компонента
     * 
     * @return string
     */
    public function getContent()
    {
        // валидация компонента
        if (!$this->isValid()) return false;
        // добавление JS и CSS объявлений компонента в документ
        $this->scripts(Gear::$app->document);
        // инициализация данных
        $this->initialise();
        // валидация инициализация данных компонента
        if (!$this->isValidInitialise()) return false;
        // включение буферизации вывода
        ob_start();
        $this->template();
        $html = ob_get_contents();
        // удаление выходного буфера и выключение буферизации вывода
        ob_end_clean();

        return $html;
    }

    /**
     * Вывод данных
     * 
     * @return void
     */
    public function render()
    {
        // если компонент не в режиме "конструктора"
        if (!($this->useDesignMode && $this->designMode))
            // если компонент скрыт
            if ($this->get('hidden')) return false;

        // если контент компонента прошел валидацию
        if (($content = $this->getContent()) === false) return;
        echo $content;
    }
}


/**
 * Базовый класс "лёгкого" компонента
 * 
 * @category   Gear
 * @package    Core
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Component.php 2013-11-10 16:00:00 Gear Magic $
 */
class GComponentWidget
{
    /**
     * Атрибуты компонента
     *
     * @var array
     */
    public $attributes = array();

    /**
     * Идентификатор компонента
     *
     * @var string
     */
    public $id = '';

    /**
     * Класс компонента
     *
     * @var string
     */
    public $class = '';

    /**
     * Принадлежность компонента (article, scheme, template)
     *
     * @var string
     */
    public $belongTo = '';

    /**
     * Компонент в режиме "демонстрации"
     * 
     * @var boolean
     */
    public $designMode = false;

    /**
     * Интерфейс управления компонентом в режиме конструктора
     *
     * @var array
     */
    public $designControl = array();

    /**
     * Whether to use the design mode for the component
     * 
     * @var boolean
     */
    public $useDesignMode = true;

    /**
     * Конструктор
     *
     * @params array $attr все атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        // атрибуты компонента
        $this->attributes = $attr;
        // идентификатор компонента
        $this->id = $this->get('id', $this->id);
        // класс компонента
        $this->class = $this->get('class', $this->class);
    }

    /**
     * Возращает "open" тег компонента (для режима "конструктора")
     * 
     * @return string
     */
    protected function getDesignTagOpen()
    {
        $id = $this->get('id');
        $pageId = Gear::$app->document->pageId;
        $articleId = Gear::$app->document->id;
        // идентификатор записи
        $dataId = $this->get('data-id', 0);
        // идентификатор шаблона статьи или страницы
        if ($this->belongTo == 'template')
            $templateId = Gear::$app->page->templateId;
        else
            $templateId = Gear::$app->document->templateId;

        return <<<HTML
        <section role="component" id="{$this->id}" class="gear-position-lt">
            <control id="ctrl-{$this->id}" title="Настройка компонента" role="component control"></control>
            <script type="text/javascript">
                (function(w, n, id) {
                    w[n] = w[n] || [];
                    w[n].push({
                        id: id,
                        className: "{$this->class}",
                        templateId: {$templateId},
                        dataId: {$dataId},
                        articleId: {$articleId},
                        pageId: {$pageId},
                        belongTo: "{$this->belongTo}",
                        menu: {
                            "property": {name: "Свойства", icon: "property", url: "site/design/~/component/interface/"}
                        }
                    });
                })(window, "gearComponents", "{$this->id}");
            </script>
            <content>
HTML;
    }

    /**
     * Возращает "close" тег компонента (для режима "конструктора")
     * 
     * @return string
     */
    protected function getDesignTagClose()
    {
        return '</content>' . _n . '</section>';
    }

    /**
     * Возращает атрибуты компонента (из $attributes)
     *
     * @params array $name название атрибута
     * @params array $default значение по умолчанию, если нет $name
     * @return mixed
     */
    public function get($name, $default = '')
    {
        if (isset($this->attributes[$name]))
            return $this->attributes[$name];
        else
            return $default;
    }

    /**
     * Валидация компонента перед выводом данных, если компонент не прошел
     * валидацию - данные компонента не выводятся
     * 
     * @return boolean
     */
    public function isValid()
    {
        return true;
    }

    /**
     * Вывод данных компонента
     * 
     * @return void
     */
    protected function content()
    {}

    /**
     * Вывод данных
     * 
     * @return void
     */
    public function render()
    {
        // если компонент не в режиме "конструктора"
        if (!($this->useDesignMode && $this->designMode))
            // если компонент скрыт
            if ($this->get('hidden')) return false;

        // валидация компонента
        if (!$this->isValid()) return false;
        /*
        // компонент в режиме "конструктора"
        if ($this->useDesignMode && $this->designMode)
            echo '<component id="', $this->id, '" class="', $this->class, 
                 '" template-id="', Gear::$app->document->templateId, '" data-id="0" article-id="', Gear::$app->document->id, 
                 '" page-id="', Gear::$app->document->pageId, '" belong-to="', $this->belongTo, '">',
                 '<control class="c-position-lt" title="Настройка компонента" type="component"><menu>', json_encode($this->designControl['menu']), '</menu></control>',
                 '<content class="c-position-lt">';
        */
        echo $this->getDesignTagOpen();
        // вывод данных компонента
        $this->content();
        echo $this->getDesignTagClose();
        /*
        // компонент в режиме "конструктора"
        if ($this->useDesignMode && $this->designMode)
            echo '</content></component>';
        */
    }
}
?>