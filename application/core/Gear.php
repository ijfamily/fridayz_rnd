<?php
/**
 * Gear CMS
 *
 * Базовые настройки системы
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Core
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Gear.php 2015-07-01 12:00:00 Gear Magic $
 */

defined('_INC') or die('Restricted access');

/**
 * Константа для подключаемых файлов, чтобы предотвратить прямой доступ
 */
define('_TPL', 1);

/**
 * Спецсимволы
 */
define('_t', '    ');
define('_2t', '        ');
define('_3t', '            ');
define('_n', "\n");
define('_n_t', "\n    ");

/**
 * Определение уровней вывода ошибок
 */
switch (REPORTING) {
    case 'development': error_reporting(0); break;
    case 'production': error_reporting(0); break;
    default:
        exit('Неправильно выбран уровень ошибок.');
}

/**
 * Определение путей к библиотекам и шаблонам системы
 */
// название ядра (как каталог "/application/libraries/{Gear}/...")
define('CORE_NAME', 'Gear');
// путь к шаблонам приложения
define('PATH_TEMPLATE', PATH_APPLICATION . 'templates/');
// путь к шаблонам приложения
define('PATH_TEMPLATE_SYS', PATH_TEMPLATE . 'system/');
// путь к шаблонам для всех компонентов
define('PATH_TEMPLATE_COMMON', PATH_TEMPLATE . 'common/');
// путь к ресурсам шаблона
define('PATH_THEME', 'themes/');
// путь к языкам
define('PATH_LANGUAGE', 'languages/');
// путь к компонентам системы
define('PATH_COMPONENT', PATH_APPLICATION . 'components/');
// путь к кэшу системы
define('PATH_CACHE', 'cache/');
// путь к каталогу настроек системы
define('PATH_CONFIG', PATH_APPLICATION . 'config/');
// путь к маршрутам
define('PATH_ROUTE', PATH_APPLICATION . 'route/');

/**
 * Gear
 * 
 * @category   Gear
 * @package    Core
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Gear.php 2015-06-01 12:00:00 Gear Magic $
 */
final class Gear
{
    /**
     * Указатель на экземпляр класса приложения
     * 
     * @var object
     */
    public static $app;

    /**
     * Массив хранения данных для передачи между объектами
     * 
     * @var array
     */
    public static $data = array();

    /**
     * Перенаправление на нужный URL с заголовком PHP или использование JavaScript location.href
     *
     * @param string $url  URL куда перенаправить
     * @param string $mode PHP|JS
     */
    public static function redirect($url, $mode = "PHP") {
        if ((PHP_VERSION_ID >= 50400 && PHP_SESSION_ACTIVE === session_status()) || (PHP_VERSION_ID < 50400 && isset($_SESSION) && session_id())) {
            session_write_close();
        }

        if ($mode == "PHP") {
            header("Location: $url");
        } elseif ($mode == "JS") {
            echo '<html><head><script type="text/javascript">';
            echo 'function redirect(){ window.top.location.href="' . $url . '"; }';
            echo '</script></head>';
            echo '<body onload="redirect()">Redirecting, please wait...</body></html>';
        }

        die();
    }

    /**
     * Передать данные компоненту
     *
     * @param  string $name название компонента
     * @param  mixed $data данные для компонента
     * @return void
     */
    public static function to($name, $key, $value)
    {
        if (is_null($value))
            unset(self::$data[$name][$key]);
        else {
            if (isset(self::$data[$name]))
                self::$data[$name][$key] = $value;
            else
                self::$data[$name] = array($key => $value);
        }
    }

    /**
     * Забрать данные для компонента
     *
     * @param  string $name название компонента
     * @param  mixed $default значение по умолчанию если нет данных для компонента
     * @return void
     */
    public static function from($name, $key, $default = false)
    {
        if (isset(self::$data[$name][$key]))
            return self::$data[$name][$key];
        else
            return $default;
    }

    /**
     * Подключение сценарий vendor
     * (путь "Vendor/{$name}")
     * 
     * @param  string $name название файла
     * @return void
     */
    public static function vendor($name)
    {
        require_once('Vendor' . $name . '.php');
    }

    /**
     * Подключение сценария библиотеки
     * (путь "Gear/{$name}")
     * 
     * @param  string $name название файла
     * @param  boolean $once подлючить сценарий только 1-н раз
     * @param  boolean $library если true - подключить сценарий из Gear/{$name}", если false - Gear/Imports/{$name}"
     * @return mixed
     */
    public static function library($name)
    {
        require_once(CORE_NAME . $name . '.php');
    }

    /**
     * Загрузка шаблона
     * 
     * @param  string $template название файла шаблона
     * @param  boolean $exit вывод шаблона и обрыв сценария если true
     * @param  boolean $common true - без учета шаблона
     * @return mixed
     */
    public static function content($template, $exit = false, $common = false)
    {
        // путь к шаблону
        if ($common)
            $filename = './' . PATH_TEMPLATE_COMMON . '/' . $template;
        else
            $filename = './' . PATH_TEMPLATE . Gear::$app->config->get('THEME') . '/' . GFactory::getLanguage('alias') . '/' . $template;

        if ($exit)
            die(file_get_contents($filename));
        else
            return file_get_contents($filename);
    }

    /**
     * Рендер компонента
     * (путь "application/components/{$path}/component.php" и имя класса "C{$class}")
     * 
     * @param  string $path путь к компоненту
     * @param  string $class название файла и класса
     * @param  array $attr атрибуты компонента
     * @return mixed
     */
    public static function component($class, $path, $attr = array())
    {
        $fileName = './' . PATH_COMPONENT .  $path . 'component.php';
        // если существует файл компонента
        try {
            if (!file_exists($fileName))
                throw new GException('Component error', 'The component "%s" is not found in the component list', $class . ' (path: ' . $fileName . ')');
        } catch(GException $e) {}
        // подключение языка - {component}/languages/{language}/Text.php
        // WARNING: 
        GText::addText($path);
        // подключение компонента - {component}/component.php
        require_once($fileName);
        // если класс компонента не существует
        try {
            if (!class_exists($class))
                throw new GException('Component error', 'The component class "%s" does not exist', $class);
        } catch(GException $e) {}

        // создаём класс компонента
        $cmp = new $class($attr);
        // если есть модель, делаем инициализацию модели по атрибутам
        if ($cmp->model) {
            $cmp->model->construct();
        }

        $cmp->render();
    }

    /**
     * Возращает переменные для шаблона
     * 
     * @return mixed
     */
    public static function &tpl($name)
    {
        if (empty($GLOBALS[$name]))
            return $GLOBALS[$name];
        else
            return $GLOBALS[$name];
    }

    /**
     * Настройки документа через шаблон
     * 
     * @return void
     */
    public static function doc($data)
    {
        GFactory::getDocument()->set($data);
    }

    /**
     * Вывод экранированного кода
     * 
     * @return void
     */
    public static function code($vars, $comment = '')
    {
        echo '<pre>';
        if ($comment)
            echo 'Code for: <strong>', $comment, '</strong><br><br>';
        print_r($vars);
        echo '</pre>';
    }

    /**
     * Вывод лога
     * 
     * @return void
     */
    public static function log($vars, $comment = '')
    {
        GFactory::getDg()->log($vars, $comment);
    }
}


/**
 * Timer
 * 
 * @category   Gear
 * @package    Core
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Gear.php 2015-06-01 12:00:00 Gear Magic $
 */
final class GTimer
{
    /**
     * Временные интервалы
     * 
     * @var array
     */
    public static $times = array();

    /**
     * Возращает время в микросек.
     * 
     * @return float
     */
    public static function getTime()
    {
        list($usec, $sec) = explode(' ', microtime());

        return ((float)$usec + (float)$sec);
    }

    /**
     * Начала замера интервала
     * 
     * @param string $name название интервала
     * @param string $comment коментарий
     * @return float возращает время в микросек.
     */
    public static function start($name, $comment = '')
    {
        $time = self::getTime();
        if (empty($comment))
            $comment = $name;
        self::$times[$name] = array($time, $time, $comment);

        return $time;
    }

    /**
     * Конец замера интервала
     * 
     * @param string $name название интервала
     * @param boolean $result true - последний замер, false - интервал 
     * @param integer $precision округление
     * @return mixed возращает время в микросек.
     */
    public static function stop($name, $result = true, $precision = 0)
    {
        $time = self::getTime();
        if (isset(self::$times[$name]))
            self::$times[$name][1] = $time;
        else
            self::$times[$name] = array($time, $time, '');

        if ($result)
            $value = self::$times[$name][1] - self::$times[$name][0];
        else
            $value = $time;
        if ($precision)
            return round($value, $precision);
        else
            return $value;
    }

    /**
     * Возращает результат замера или инфо. интервала
     * 
     * @param string $name название интервала
     * @param boolean $result true - выводить результат, false - инфо. интервала 
     * @param integer $precision округление
     * @return mixed возращает время в микросек.
     */
    public static function get($name, $result = true, $precision = 0)
    {
        if (!$result)
            if (isset(self::$times[$name]))
                return self::$times[$name];
            else
                return false;

        if (isset(self::$times[$name]))
            $value = self::$times[$name][1] - self::$times[$name][0];
        else
            return false;;

        if ($precision)
            return round($value, $precision);
        else
            return $value;
    }

    /**
     * Возращает коментарий и интервал
     * 
     * @param string $name название интервала
     * @param boolean $result true - выводить результат, false - инфо. интервала 
     * @param integer $precision округление
     * @return mixed
     */
     public static function getReport($name, $result = true, $precision = 0)
     {
        if (($time = self::get($name, $result, $precision)) !== false)
            return array($time, self::$times[$name][2]);
     }
}


// инициализация путей
try {
    // путь к каталогу ".../application/libraries"
    $pathL = DOCUMENT_ROOT . PATH_APPLICATION . '/libraries';
    // путь к каталогу ".../application"
    $pathA = DOCUMENT_ROOT . PATH_APPLICATION;
    if (!file_exists($pathL))
        throw new GException('Сonfiguration error', 'Can not set the local path "%s"', $pathL);

    $path = $pathL . PATH_SEPARATOR . $pathA . PATH_SEPARATOR . get_include_path();
    if (set_include_path($path) === false)
        throw new GException('Сonfiguration error', 'Can not set the local path "%s"', $path);

} catch(GException $e) {
    $e->renderException(array(), PATH_TEMPLATE_SYS . '/exception.php');
}

/**
 * @see GException
 */
require('Exception.php');

/**
 * @see GFactory
 */
require('Factory.php');
?>