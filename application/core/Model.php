<?php
/**
 * Gear CMS
 *
 * Модель данных компонента
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Core
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Model.php 2013-07-01 12:00:00 Gear Magic $
 */

/**
 * Модель данных компонента (необходима для отображения данных в компоненте)
 * 
 * @category   Gear
 * @package    Core
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Model.php 2013-07-01 12:00:00 Gear Magic $
 */
class GModel
{
    /**
     * Атрибуты компонента
     *
     * @var array
     */
    public $attributes = array();

    /**
     * Идентификатор записи
     *
     * @var integer
     */
    public $dataId = 0;

    /**
     * Идентификатор для подстановки в sql запрос
     *
     * @var string
     */
    public $uri;

    /**
     * Идентификатор компонента
     *
     * @var string
     */
    public $id = '';

    /**
     * Псевдоним языка
     *
     * @var string
     */
    public $ln = '';

    /**
     * Работа ЧПУ
     *
     * @var boolean
     */
    public $sef = false;

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        $this->attributes = $attr;

        $this->uri = GFactory::getURL();
        $this->url = $this->uri;
        // идентификатор записи
        $this->dataId = $this->get('data-id', 0);
        // идентификатор компонента
        $this->id = $this->get('id', $this->id);
        // работает ли ЧПУ
        $this->sef = Gear::$app->config->get('SEF');
        // язык
        $this->ln = GFactory::getLanguage('prefixUri');
        $this->path = $this->get('path');
        $this->_ = GText::getSection($this->path);
    }

    /**
     * Устанавливает атрибут ($attributes)
     *
     * @params array $name название атрибута
     * @params array $value значение, если null удаляет атрибут
     * @return mixed
     */
    public function set($name, $value = null)
    {
        if ($value == null) {
            unset($this->attributes[$name]);
        } else
            $this->attributes[$name] = $value;
    }

    /**
     * Возращает атрибуты (из $attributes)
     *
     * @params array $name название атрибута
     * @params array $default значение по умолчанию, если нет $name
     * @return mixed
     */
    public function get($name, $default = '')
    {
        if (isset($this->attributes[$name]))
            return $this->attributes[$name];
        else
            return $default;
    }

    /**
     * Инициализация модели
     * 
     * @return void
     */
    public function initialise()
    {}

    /**
     * Этот метод вызывается сразу после создания компонента модели
     * Используется для инициализации атрибутов модели не переданных через конструктор
     * 
     * @return void
     */
    public function construct()
    {}
}


/**
 * Модель данных компонента для отображение элементов списка (слайдеры, галереи и т.д.)
 * 
 * @category   Gear
 * @package    Core
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Model.php 2013-07-01 12:00:00 Gear Magic $
 */
class GModelItems extends GModel
{
    /**
     * Обработчик SQL запроса
     *
     * @var array
     */
    protected $_query;

   /**
     * SQL запрос
     * 
     * @var string
     */
    protected $_sql = '';

    /**
     * Конструктор
     *
     * @params array $attr атрибуты компонента
     * @return void
     */
    public function __construct($attr = array())
    {
        parent::__construct($attr);

        // установить соединение с сервером
        GFactory::getDb()->connect();
        // обработчик SQL запросов
        $this->_query = new GDbQuery();
    }

    /**
     * Конструктор запроса
     *
     * @return void
     */
    protected function query()
    {
        return '';
    }

    /**
     * Возращает список элементов
     * 
     * @return array
     */
    public function getItems()
    {
        // конструктор запроса
        $this->_sql = $this->query();
        $this->_query->execute($this->_sql);
        $items = array();
        $index = 0;
        while (!$this->_query->eof()) {
            $items[] = $this->getItem($this->_query->next(), $index);
            $index++;
        }

        return $items;
    }

    public function getCountRecords()
    {
        return (int) $this->_query->getFoundRows();
    }

    /**
     * Вывод элемента списка
     * 
     * @param  array $item элемент списка из базы данных
     * @param  integer $index порядковый номер
     * @return void
     */
    public function getItem($item, $index)
    {
        return $item;
    }
}
?>