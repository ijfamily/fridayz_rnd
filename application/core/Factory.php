<?php
/**
 * Gear CMS
 *
 * Фабрика классов
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Core
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Factory.php 2013-09-04 12:00:00 Gear Magic $
 */

defined('_INC') or die('Restricted access');

/**
 * @see GModel
 */
require('Model.php');

/**
 * @see GComponent
 */
require('Component.php');

/**
 * @see Dom
 */
require('Dom.php');

/**
 * Фабрика классов
 * 
 * @category   Gear
 * @package    Core
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Factory.php 2013-05-01 12:00:00 Gear Magic $
 */
final class GFactory
{
    /**
     * Указатель на экземпляр класса компонента страницы
     *
     * @return object
     */
    protected static $_page = null;

    /**
     * Указатель на экземпляр класса базы данных
     *
     * @return object
     */
    protected static $_db = null;

    /**
     * Указатель на экземпляр класса кэша
     *
     * @return object
     */
    protected static $_cache = null;

    /**
     * Указатель на экземпляр класса конфигурации
     *
     * @return object
     */
    protected static $_config = null;

    /**
     * Указатель на экземпляр класса документа
     *
     * @return object
     */
    protected static $_document = null;

    /**
     * Указатель на экземпляр класса маршрутизатора
     *
     * @return object
     */
    protected static $_router = null;

    /**
     * Указатель на экземпляр класса запроса пользователя
     *
     * @return object
     */
    protected static $_request = null;

    /**
     * Указатель на экземпляр класса приложения
     *
     * @return object
     */
    protected static $_application = null;

    /**
     * Указатель на экземпляр класса отладчика
     *
     * @return object
     */
    protected static $_debugger = null;

    /**
     * Указатель на экземпляр класса сессии
     *
     * @return object
     */
    protected static $_session = null;

    /**
     * Указатель на экземпляр класса обработки ввода данных
     *
     * @return object
     */
    protected static $_input = null;

    /**
     * Указатель на экземпляр класса URI
     *
     * @return object
     */
    protected static $_url = null;

    /**
     * Указатель на экземпляр класса даты
     *
     * @return object
     */
    protected static $_date = null;

    /**
     * Указатель на экземпляр класса почты
     *
     * @return object
     */
    protected static $_mail = null;

    /**
     * Указатель на экземпляр класса браузера
     *
     * @return object
     */
    protected static $_browser = null;

    /**
     * Указатель на экземпляр класса пользователя
     *
     * @return object
     */
    protected static $_user = null;

    /**
     * Указатель на экземпляр класса запроса к базе данных
     *
     * @return object
     */
    protected static $_query = null;

    /**
     * Указатель на экземпляр класса аутентификации пользователя
     *
     * @return object
     */
    protected static $_auth = null;

    /**
     * Возращает указатель на экземпляр класса запроса к базе данных
     * 
     * @return mixed
     */
    public static function getQuery()
    {
        if (self::$_query === null) {
            self::$_query = new GDbQuery();
        }

        return self::$_query;
    }

    /**
     * Возращает указатель на экземпляр класса аутентификации пользователя
     * 
     * @return mixed
     */
    public static function getAuth()
    {
        if (self::$_auth === null) {
            self::$_auth = self::getClass('Auth');
        }

        return self::$_auth;
    }

    /**
     * Возращает указатель на класс пользователя
     * 
     * @return mixed
     */
    public static function getUser()
    {
        if (self::$_user === null) {
            self::$_user = self::getClass('User');
        }

        return self::$_user;
    }

    /**
     * Возращает URL 
     * @return string
     */
    public static function getUrl()
    {
        if (self::$_url === null)
            self::$_url = self::getIClass('Url');

        return self::$_url;
    }

    /**
     * Возращает язык из запроса пользователя
     * 
     * @return void
     */
    public static function getLanguage($key = '')
    {
        return GLanguage::getInstance()->get($key);
    }

    /**
     * Возращает указатель на класс приложения
     * 
     * @return mixed
     */
    public static function getApplication()
    {
        if (self::$_application === null)
            self::$_application = GFactory::getClass('Application');

        return self::$_application;
    }

    /**
     * Возращает указатель на класс приложения
     * 
     * @return mixed
     */
    public static function getApp()
    {
        if (self::$_application === null)
            self::$_application = GFactory::getClass('Application');

        return self::$_application;
    }

    /**
     * Возращает указатель на класс браузера
     * 
     * @return mixed
     */
    public static function getBrowser()
    {
        if (self::$_browser === null)
            self::$_browser = GFactory::getClass('Browser');

        return self::$_browser;
    }

    /**
     * Возращает указатель на класс даты
     * 
     * @return mixed
     */
    public static function getDate($timezone = '')
    {
        if (self::$_date === null)
            self::$_date = GFactory::getClass('Date', '', $timezone);

        return self::$_date;
    }

    /**
     * Возращает указатель на класс конфигурации
     * 
     * @return mixed
     */
    public static function getConfig($path = 'config')
    {
        if (self::$_config === null) {
            self::$_config = self::getClass('Config', '', $path);
        }

        return self::$_config;
    }

    /**
     * Возращает указатель на класс базы данных
     * 
     * @return mixed
     */
    public static function getDb()
    {
        if (self::$_db === null) {
            Gear::library('/Db/Driver');
            self::$_db = GDbDriver::get(self::getConfig()->getParams());
        }

        return self::$_db;
    }

    /**
     * Возращает указатель на класс отладчика
     * 
     * @return mixed
     */
    public static function getDg()
    {
        if (self::$_debugger === null) {
            Gear::library('/Debugger');
            self::$_debugger = FirePHP::getInstance(true);
        }

        return self::$_debugger;
    }

    /**
     * Возращает указатель на класс почты
     * 
     * @return mixed
     */
    public static function getMail()
    {
        if (self::$_mail === null)
            self::$_mail = GFactory::getClass('Mail');

        return self::$_mail;
    }

    /**
     * Возращает указатель на класс кэша
     * 
     * @return mixed
     */
    public static function getCache()
    {
        if (self::$_cache === null)
            self::$_cache = GFactory::getClass('Cache');

        return self::$_cache;
    }

    /**
     * Возращает указатель на класс документа
     * 
     * @return mixed
     */
    public static function getDocument($name = '')
    {
        if (self::$_document === null)
            self::$_document = GFactory::getClass('Document');

        if ($name)
            return self::$_document->get($name);
        else
            return self::$_document;
    }

    /**
     * Возращает указатель на класс маршрутизатора
     * 
     * @return mixed
     */
    public static function getRouter()
    {
        if (self::$_router === null)
            self::$_router = self::getComponent('Router');

        return self::$_router;
    }

    /**
     * Возращает указатель на класс компонента страницы
     * 
     * @return mixed
     */
    public static function getPage()
    {
        if (self::$_page === null)
            self::$_page = self::getComponent('Page');

        return self::$_page;
    }

    /**
     * Возращает указатель на класс сессии
     * 
     * @return mixed
     */
    public static function getSession()
    {
        if (self::$_session === null) {
            self::$_session = self::getClass('Session');
        }

        return self::$_session;
    }

    /**
     * Возращает указатель на класс сессии
     * 
     * @return mixed
     */
    public static function getInput()
    {
        if (self::$_input === null) {
            self::$_input = self::getClass('Input');
        }

        return self::$_input;
    }

    /**
     * Загрузка модели компонента с возвращением указателя на экземпляр вызывающего класса
     * (путь "Gear/Models/{$name}" и имя класса "GModel{$name}")
     * 
     * @param  string $name название файла и класса (если $cname не указан)
     * @param  string $cname название класса
     * @params array $attr атрибуты компонента
     * @return mixed
     */
    public static function getModel($name, $cname = '', $attr = array())
    {
        if (is_array($cname)) {
            $attr = $cname;
            $cname = '';
        }
        // подключить
        require_once(CORE_NAME . '/Models' . $name . '.php');
        // название класса
        if ($cname)
            $class = 'Cm' . $cname;
        else
            $class = 'Cm' . $name;

        return new $class($attr);
    }

    /**
     * Загрузка компонента с возвращением указателя на экземпляр вызывающего класса
     * (путь "application/components/{$path}/component.php" и имя класса "C{$class}")
     * 
     * @param  string $path путь к компоненту
     * @param  string $class название файла и класса
     * @params array $attr атрибуты компонента (если они есть)
     * @return mixed
     */
    public static function getRelease($class, $path, $attr = array())
    {
        $fileName = './' . PATH_COMPONENT . $path . '/component.php';
        // если существует файл компонента
        try {
            if (!file_exists($fileName))
                throw new GException('Component error', 'The component "%s" is not found in the component list', $class);
        } catch(GException $e) {}
        // подключение языка - {component}/languages/{language}/Text.php
        // WARNING: 
        GText::addText($path);
        // подключение компонента - {component}/component.php
        require_once($fileName);
        // если класс компонента не существует
        try {
            if (!class_exists($class))
                throw new GException('Component error', 'The component class "%s" does not exist', $class);
        } catch(GException $e) {}

        // создаём класс компонента
        $attr['path'] = $path;
        $cmp = new $class($attr);
        // если есть модель, делаем инициализацию модели по атрибутам
        if ($cmp->model) {
            $cmp->model->construct();
        }

        return $cmp;
    }

    /**
     * Загрузка шаблона для отображения данных модели компонента
     * (путь "application/templates/{language}/{template}
     * 
     * @param  string $name название шаблона в $GLOBALS
     * @param  mixed $var данные для вставки в шаблон
     * @param  string $template название файла шаблона
     * @param  boolean $getContent возращать контент шаблона
     * @return void
     */
    public static function getTemplate($name, $var, $template, $getContent = false)
    {
        $GLOBALS[$name] = $var;
        // путь к шаблону
        $fileName = './' . PATH_TEMPLATE  . Gear::$app->config->get('THEME') . '/' . GFactory::getLanguage('alias') . '/' . $template;
        // если есть необходимость возвратить контент шаблона
        if ($getContent) {
            ob_start();
        }
        // подключение шаблона
        // BUG: необходимо постоянно подключать
        require($fileName);
        // если есть необходимость возвратить контент шаблона
        if ($getContent) {
            $tpl = ob_get_contents();
            ob_clean();

            return $tpl;
        }
    }

    /**
     * Загрузка компонента с возвращением указателя на экземпляр вызывающего класса
     * (путь "Gear/Components/{$name}" и имя класса "C{$name}")
     * 
     * @param  string $name название файла и класса
     * @return mixed
     */
    public static function getComponent($name)
    {
        // подключить
        require(CORE_NAME . '/Components/' . $name . '.php');
        // название класса
        $class = 'Cp' . $name;

        return new $class();
    }

    /**
     * Загрузка хелпера
     * (путь "Gear/Helpers/{$name}")
     * 
     * @param  string $name название файла
     * @return mixed
     */
    public static function getHelper($name)
    {
        if (!isset($GLOBALS['helper']))
            $GLOBALS['helper'] = array();
        // если существует хелпер
        if (isset($GLOBALS['helper'][$name]))
            return $GLOBALS['helper'][$name];
        // подключить
        $helper = @require(CORE_NAME . '/Helpers/' . $name . '.php');

        return $GLOBALS['helper'][$name] = $helper;
    }

    /**
     * Подключение сценария библиотеки
     * (путь "Gear/{$name}")
     * 
     * @param  string $name название файла
     * @param  string $class название класса
     * @param  boolean $attr атрибуты класса
     * @return mixed
     */
    public static function getClass($class, $filename = '', $attr = null)
    {
        if (empty($filename))
            $filename = $class;

        require_once(CORE_NAME . '/' . $filename . '.php');

        if ($class) { 
            $class = 'G' . $class;
            if ($attr)
                return new $class($attr);
            else
                return new $class();
        }
    }

    /**
     * Подключение сценария библиотеки
     * (путь "Gear/{$name}")
     * 
     * @param  string $name название файла
     * @param  string $class название класса
     * @param  boolean $once подлючить сценарий только 1-н раз
     * @return mixed
     */
    public static function getIClass($class, $filename = '', $attr = null)
    {
        if (empty($filename))
            $filename = $class;

        @require_once(CORE_NAME . '/' . $filename . '.php');

        if ($class) {
            $class = 'G' . $class;
            if ($attr)
                return $class::getInstance($attr);
            else
                return $class::getInstance();
        }
    }
}
?>