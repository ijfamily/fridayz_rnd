<?php
/**
 * Gear CMS
 *
 * Обработка ошибок
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Core
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Exception.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Класс обработки ошибок
 * 
 * @category   Gear
 * @package    Core
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Exception.php 2016-01-01 12:00:00 Gear Magic $
 */
class GException extends Exception
{
    /**
     * Ошибка
     */
    const TYPE_ERROR     = 0;

    /**
     * Исключение
     */
    const TYPE_EXCEPTION = 1;

    /**
     * Типы ошибок
     *
     * @var array
     */
    protected static $_typesErrors = array(
        E_ERROR             => 'Error',
        E_WARNING           => 'Warning',
        E_PARSE             => 'Parsing Error',
        E_NOTICE            => 'Notice',
        E_CORE_ERROR        => 'Core Error',
        E_CORE_WARNING      => 'Core Warning',
        E_COMPILE_ERROR     => 'Compile Error',
        E_COMPILE_WARNING   => 'Compile Warning',
        E_USER_ERROR        => 'User Error',
        E_USER_WARNING      => 'User Warning',
        E_USER_NOTICE       => 'User Notice',
        E_STRICT            => 'Runtime Notice',
        E_RECOVERABLE_ERROR => 'Catchable Fatal Error');

    /**
     * Фатальные ошибки восстанавливающий сценарий
     *
     * @var array
     */
    protected static $_fatalErrors = array(E_CORE_ERROR, E_COMPILE_ERROR, E_USER_ERROR, E_PARSE, E_STRICT);

    /**
     * Стек ошибок
     *
     * @var array
     */
    public static $_stack = array();

    /**
     * Последнее пойманое исключение
     *
     * @var array
     */
    protected static $_lastException = array();

    /**
     * Конструктор
     * 
     * @param  string $message сообщение
     * @param  string $status статус
     * @param  string $class класс вызвавший ошибку
     * @return void
     */
    public function __construct($status, $message, $params, $class = null)
    {
        // поймать исключение
        $this->catchException($status, $message, $params, $class);
    }

    /**
     * Поймать исключение
     * 
     * @param  string $message сообщение
     * @param  string $status статус
     * @param  string $class класс вызвавший ошибку
     * @return void
     */
    public static function catchException($status, $message, $params, $class = null)
    {
        if (REPORTING != 'development') return;

        // перевести статус
        $status = GText::_($status);
        // перевести сообщение
        $message = GText::_($message, 'exception', $params);
        // последнее пойманое исключение
        self::$_lastException = array('status' => $status, 'message' => $message, 'class' => $class);
        // добавление ошибки в стек
        self::addStack(self::$_lastException, self::TYPE_EXCEPTION);
    }

    /**
     * Преобразование строки файла
     * 
     * @param  string $str строка с укзанным файлов
     * @return string
     */
    public static function errFileStrip($str)
    {
        if (($p = strpos($str, 'application')) !== false)
            $str = substr($str, $p + 12, strlen($str) - $p - 12);

        return str_replace(array(chr(92), '/', '.php'), array(' &raquo; ', ' &raquo; ', ''), $str);
    }

    /**
     * Избавиться от полного пути в собщении
     * 
     * @param  string $str текст
     * @return string
     */
    public static function errMsgStrip($str)
    {
        if (($pAt = strpos($str, ' at ')) === false)
            return $str;
        if (($p = strpos($str, 'application')) !== false) {
            $r = substr($str, $pAt + 4, strlen($str) - $pAt - $p - 8);
            return str_replace($r, '', $str);
        } else
            return $str;
    }

    /**
     * Изменяет параметры ошибки (скрывает ненужное для пользователя)
     * 
     * @param  string $error ассоц-й массив параметров ошибки
     * @return array
     */
    public static function update($error)
    {
        // если указан файл
        if (isset($error['file'])) {
            $error['file'] = self::errFileStrip($error['file']);
        }
        $error['message'] = self::errMsgStrip($error['message']);
        // если указан уровень (тип) ошибки
        if (isset($error['level']))
            $error['level'] = isset(self::$_typesErrors[$error['level']]) ? self::$_typesErrors[$error['level']] : $error['level'];
        else
            if (isset($error['type']))
                $error['level'] = isset(self::$_typesErrors[$error['type']]) ? self::$_typesErrors[$error['type']] : $error['level'];

        return $error;
    }

    /**
     * Добавление ошибки в стек
     * 
     * @param  array $error ассоц-й массив параметров ошибки
     * @param  integer $type тип (исключение, ошибка)
     * @return array
     */
    public static function addStack($error, $type = GException::TYPE_ERROR)
    {
        self::$_stack[] = array('type' => $type, 'error' => self::update($error));
    }

    /**
     * Обработчик всех ошибок кроме фатальных
     * 
     * @return void
     */
    public static function catchError($level = 0, $message, $file = '', $line = 0)
    {
        // добавление ошибки в стек
        self::addStack(array('level' => $level, 'message' => $message, 'file' => $file, 'line' => $line), self::TYPE_ERROR);
    }

    /**
     * Обработчик фатальных ошибок
     * 
     * @return void
     */
    public static function catchFatalError()
    {
        // последняя ошибка
        $error = error_get_last();
        if (is_null($error)) return;

        // если фатальная ошибка, но исключение вызвали мы сами
        if (self::$_lastException) {
            self::renderException(self::$_lastException, true);
            return;
        }
        // если ошибка особо критическая и остоновка скрипта
        if ($error['type'] == E_ERROR) {
            // вывод ошибки в шаблон
            self::renderTplError(self::update($error));
        } else {
            // если ошибка фатальная
            if (in_array($error['type'], self::$_fatalErrors))
                // т.к. выводится после всего текста на выходе
                //self::renderFatalError(self::update($error));
                self::renderTplError(self::update($error));
        }
    }

    /**
     * Вывод ошибки в шаблон
     * 
     * @param  array $error ассоц-й массив параметров ошибки
     * @return void
     */
    public static function renderTplError($error)
    {
        $GLOBALS['fatalError'] = $error;
        // подключение шаблона
        $dir = dirname(__FILE__) . '/../templates/system/error.php';
        include($dir);
    }

    /**
     * Вывод критичиской ошибки из шаблона
     * 
     * @param  array $error ассоц-й массив параметров ошибки
     * @return void
     */
    public static function renderFatalError($error)
    {
        self::renderError($error);

        echo "\n</body>\n</html>";
    }

    /**
     * Вывод ошибки
     * 
     * @param  array $error ассоц-й массив параметров ошибки
     * @return void
     */
    public static function renderError($error)
    {
        echo '<div class="gear-err ', strtolower($error['level']), '"><div>',
             '<div class="gear-err-status">Gear: ', $error['level'], '</div>',
             '<div class="gear-err-file">', $error['file'], ' <b>(', $error['line'], ')</b></div>',
             '<div class="gear-err-msg">', $error['message'], '</div>', '</div></div>';
    }

    /**
     * Вывод пойманого исключения
     * 
     * @param  array $error ассоц-й массив параметров ошибки
     * @return void
     */
    public static function renderException($error = array(), $template = false)
    {
        // если не установлено исключение, то берется последнее
        if (empty($error))
            $error = self::$_lastException;
        // если есть необходимость использовать шаблон
        if (is_string($template)) {
            $GLOBALS['fatalError'] = $error;
            // подключение шаблона
            require($template);
            exit;
        }
        if ($template) {
            $GLOBALS['fatalError'] = $error;
            // подключение шаблона
            include(dirname(__FILE__) . '/../templates/system/exception.php');
            exit;
        }

        echo '<div class="gear-err ', $error['status'], '"><div>';
        // статус
        if (isset($error['status']))
            echo '<div class="gear-err-status">Gear: ', $error['status'], '</div>';
        // класс
        if (isset($error['class']))
            echo '<div class="gear-err-class">', $error['class'], '</div>';
        echo '<div class="gear-err-msg">', $error['message'], '</div>',
             '</div></div>';
    }

    /**
     * Вывод пойманого исключения
     * 
     * @param  array $error ассоц-й массив параметров ошибки
     * @return void
     */
    public static function renderDialog()
    {
        // берется последнее исключение
        $error = self::$_lastException;
        $GLOBALS['fatalError'] = $error;
        // подключение шаблона
        require(PATH_TEMPLATE_SYS . 'exception.php');
        exit;
    }

    /**
     * Вывод ошибок
     * 
     * @return void
     */
    public static function render()
    {
        // если html ранее не было вызываем шаблон
        if (!headers_sent() && sizeof(self::$_stack) > 0) {
            $GLOBALS['stackErrors'] = self::$_stack;
            // подключение шаблона
            require(PATH_TEMPLATE_SYS . 'stack.php');
            return;
        }
        $count = sizeof(self::$_stack);
        for ($i = 0; $i < $count; $i++) {
            if (self::$_stack[$i]['type'] == self::TYPE_EXCEPTION) {
                self::renderException(self::$_stack[$i]['error'], false);
            } else {
                self::renderError(self::$_stack[$i]['error']);
                
            }
        }

        // т.к. не всегда выводится html
        if (Gear::$app->closeHtml)
            echo "\n</body>\n</html>";
    }

    /**
     * Возвращает последнюю пойманную ошибку
     * 
     * @return array
     */
    public static function getLastException()
    {
        return self::$_lastException;
    }

    /**
     * Инициализация обработчика ошибок
     * 
     * @return void
     */
    public static function initialise()
    {
        if (REPORTING != 'development') return;

        // устанавливаем свой обработчик фатальных ошибок
        register_shutdown_function('GException::catchFatalError');
        // устанавливаем свой обработчик ошибок
        set_error_handler('GException::catchError', REPORTING_ERRORS);
    }
}
?>