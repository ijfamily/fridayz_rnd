<?php
/**
 * Gear CMS
 * 
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Languages
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Date.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

return array(
    'format' => array(
        // месяц
        'January'   => 'Января',
        'February'  => 'Февраля',
        'March'     => 'Марта',
        'April'     => 'Апреля',
        'May'       => 'Мая',
        'June'      => 'Июня',
        'July'      => 'Июля',
        'August'    => 'Августа',
        'September' => 'Сентября',
        'October'   => 'Октября',
        'November'  => 'Ноября',
        'December'  => 'Декабря',
        // месяц (сокр)
        'Jan' => 'Янв',
        'Feb' => 'Фев',
        'Mar' => 'Мар',
        'Apr' => 'Апр',
        'May' => 'Май',
        'Jun' => 'Июн',
        'Jul' => 'Июл',
        'Aug' => 'Авг',
        'Sep' => 'Сен',
        'Oct' => 'Окт',
        'Nov' => 'Ноя',
        'Dec' => 'Дек',
        // дни недели
        'Sunday'    => 'Воскресенье',
        'Monday'    => 'Понедельник',
        'Tuesday'   => 'Вторник',
        'Wednesday' => 'Среда',
        'Thursday'  => 'Четверг',
        'Friday'    => 'Пятница',
        'Saturday'  => 'Суббота',
        // дни недели (сокр)
        'Sun' => 'Вс',
        'Mon' => 'Пн',
        'Tue' => 'Вт',
        'Wed' => 'Ср',
        'Thu' => 'Чт',
        'Fri' => 'Пт',
        'Sat' => 'Сб'
    ),
    'month'   => array('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'),
    'monthBy' => array('Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'),
    // месяца без склонения
    'January'   => 'Январь',
    'February'  => 'Февраль',
    'March'     => 'Март',
    'April'     => 'Апрель',
    'May'       => 'Май',
    'June'      => 'Июнь',
    'July'      => 'Июль',
    'August'    => 'Август',
    'September' => 'Сентябрь',
    'October'   => 'Октябрь',
    'November'  => 'Ноябрь',
    'December'  => 'Декабрь',
    // для блога
    'an hour ago' => 'час назад',
    'hour ago'    => 'часа назад',
    'hours ago'   => 'часов назад',
    'just'          => 'только что',
    'minutes ago'   => 'минуты назад',
    'a minutes ago' => 'минут назад',
    'a minute ago'  => 'минуту назад',
    'yesterday' => 'вчера',
    'day ago'   => 'день назад',
    'a day ago' => 'дня назад',
    'days ago'  => 'дней назад',
    'Yesterday' => 'Вчера',
    'Today'     => 'Сегодня',
    'week ago'  => 'неделю назад',
    'weeks ago' => 'недели назад',
    'last week' => 'недель назад',
    'a month ago' => 'месяц назад',
    'month ago' => 'месяца назад',
    'months ago'  => 'месяцев назад',
    'last year'   => 'год назад',
    'years ago'   => 'года назад',
    'a years ago' => 'лет назад',
    'th' => '-е'
);
?>