<?php
/**
 * Gear CMS
 * 
 * Пакет английской (британской) локализации
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Languages
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Date.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

return array(
    'format' => array(
        // месяц
        'January'   => 'January',
        'February'  => 'February',
        'March'     => 'March',
        'April'     => 'April',
        'May'       => 'May',
        'June'      => 'June',
        'July'      => 'July',
        'August'    => 'August',
        'September' => 'September',
        'October'   => 'October',
        'November'  => 'November',
        'December'  => 'December',
        // месяц (сокр)
        'Jan' => 'Jan',
        'Feb' => 'Feb',
        'Mar' => 'Mar',
        'Apr' => 'Apr',
        'May' => 'May',
        'Jun' => 'Jun',
        'Jul' => 'Jul',
        'Aug' => 'Aug',
        'Sep' => 'Sep',
        'Oct' => 'Oct',
        'Nov' => 'Nov',
        'Dec' => 'Dec',
        // дни недели
        'Sunday'    => 'Sunday',
        'Monday'    => 'Monday',
        'Tuesday'   => 'Tuesday',
        'Wednesday' => 'Wednesday',
        'Thursday'  => 'Thursday',
        'Friday'    => 'Friday',
        'Saturday'  => 'Saturday',
        // дни недели (сокр)
        'Sun' => 'Sun',
        'Mon' => 'Mon',
        'Tue' => 'Tue',
        'Wed' => 'Wed',
        'Thu' => 'Thu',
        'Fri' => 'Fri',
        'Sat' => 'Sat'
    ),
    'month'   => array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'),
    'monthBy' => array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'),
    // месяца без склонения
    'January'   => 'January',
    'February'  => 'February',
    'March'     => 'March',
    'April'     => 'April',
    'May'       => 'May',
    'June'      => 'June',
    'July'      => 'July',
    'August'    => 'August',
    'September' => 'September',
    'October'   => 'October',
    'November'  => 'November',
    'December'  => 'December',
    // для блога
    'an hour ago' => 'an hour ago',
    'hour ago'    => 'hours ago',
    'hours ago'   => 'hours ago',
    'just'          => 'just',
    'minutes ago'   => 'minutes ago',
    'a minutes ago' => 'a minutes ago',
    'a minute ago'  => 'a minutes ago',
    'yesterday' => 'yesterday',
    'day ago'   => 'day ago',
    'a day ago' => 'a day ago',
    'days ago'  => 'days ago',
    'Yesterday' => 'Yesterday',
    'Today'     => 'Today',
    'week ago'  => 'week ago',
    'weeks ago' => 'weeks ago',
    'last week' => 'last week',
    'a month ago' => 'a month ago',
    'month ago'   => 'months ago',
    'months ago'  => 'months ago',
    'last year'   => 'last year',
    'years ago'   => 'years ago',
    'a years ago' => 'a years ago',
);
?>