<?php
/**
 * Gear CMS
 * 
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Languages
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

return array(
    // GArticle
    'download' => 'скачиваний',
    'anchor'   => 'cкачать',
    // GBreadcrumbs
    'archive for year' => 'Материалы за %s год',
    'archive for month,year' => 'Материалы за %s %s год',
    'archive for day,month,year' => 'Материалы за %s %s %s',
    // Debug
    'Components on the home page template' => 'Компоненты на главной странице шаблона',
    'the peak value of the amount of memory allocated to the script' => 'пиковое значение объема памяти выделенное скрипту',
    'the amount of memory allocated script' => 'количество памяти выделенной скрипт',
    'using Gzip' => 'используется Gzip',
    'load from cache' => 'загрузка из кэша',
    'aja request' => 'AJAX запрос',
    'involved in the category list' => 'в категории задействован список',
    'article category' => 'категория статьи',
    'robot statistics' => 'статистика роботов',
    'design view' => 'режим конструктора',
    'site disabled in settings' => 'сайт отключен в настройках',
    'debugging the page' => 'Отладка для страницы',
    'component' => 'Компонент',
    'at component template' => 'в шаблоне компонента'
);
?>