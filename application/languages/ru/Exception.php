<?php
/**
 * Gear CMS
 * 
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Languages
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Exception.php 2016-01-01 12:00:00 Gear Magic $
 */

defined('_INC') or die;

return array(
    // GDb
    // statuses
    'Connection error' => 'Ошибка подключения',
    // messages
    'Error when making a connection to the server' => 'Ошибка при выполнении соединения с сервером базы данных "%s"<br>Пожайлуста, проверьте параметры соединения.',

    // GDbQuery
    // statuses
    'Data processing error' => 'Ошибка обработки данных',
    // messages
    'Query error (Internal Server Error)' => 'Ошибка выполнения запроса к базе данных (внутренняя ошибка сервера) !',
    'Response from the database: %s' => 'Ответ от базы данных: <b>%s</b>',
    'To execute a query, you must change data!' => 'Для выполнения запроса, необходимо выполнить изменение данных!',

    // GInputFile
    // statuses
    'Loading data' => 'Загрузка данных',
    // messages
    'Upload file size exceeded the UPLOAD_MAX_FILESIZE' => 'Размер загружаемого файла превысил значение "UPLOAD_MAX_FILESIZE"!',
    'Can not open file "%s" for writing' => 'Невозможно открыть файл "%s" для записи!',
    'Unable to write data to a file "%s"' => 'Невозможно записать данные в файл "%s"!',
    'Upload file size exceeded the MAX_FILE_SIZE' => 'Размер загружаемого файла превысил значение "MAX_FILE_SIZE", который был указан в виде HTML',
    'The uploaded file was only partially loaded' => 'Загруженный файл был загружен лишь частично!',
    'File was not loaded' => 'Файл не был загружен!',
    'Could not write the file to disk' => 'Не удалось записать файл на диск!',
    'The download does not match the expansion' => 'Загружаемый файл не соответствует расширению!',
    'Can not delete file "%s"' => 'Невозможно удалить файл "%s"!',
    'Unable to move file' => 'Невозможно переместить файл!',
    'Error loading file "%s" on the server' => 'Ошибка загрузки файла "%s" на сервер!',
    'File successfully downloaded!' => 'Файл успешно загружен!',
    'Can`t perform file deletion "%s"' => 'Невозможно выполнить удаление файла "%s", файл не существует!',

    // GInput
    // messages
    'You must fill the field "%s"' => 'Необходимо заполнить поле "%s"',
    'You did not fill the field "%s"' => 'Вы неправильно заполнили поле "%s"',
    'The length of the field "%s" must be from %s characters' => 'Длина значения поля "%s", должна быть от %s-и символов!',
    'The max length of the field "%s" must be no more than %s characters' => 'Максимальная длина поля "%s", не должна превышить %s-и символов!',

    // GDocument
    // messages
    'Page in the publication process' => '<div class="gear-page-public"> Приносим свои извинения, страница в стадии наполнения</div>',
    'Error' => 'Ошибка',
    'Error 404' => 'Ошибка 404',
    'Error 500' => 'Ошибка 500',
    'Access error' => 'Ошибка доступа',
    'Internal server error' => 'Внутренняя ошибка сервера',
    'To view this page you need to login' => '<div class="content-none">Для просмотра этой страницы вам необходимо пройти <a href="/signin/">авторизацию</a></div>',
    'The requested document to the address "%s" not found' => 'Запрашиваемый документ (файл, директория) по указанному адресу <b>"http://%s"</b> не найден!',
    'Sorry, but your request is not defined' => 'Извините, но Ваш запрос не определён!',

    // GComponent
    // statuses
    'Component error' => 'Ошибка компонента',
    // messages
    'Can`t connect the component "%s", incorrectly specified path "%s"' => 'Невозможно подключить компонент "%s", неправильно указан путь "%s"',
    'The component "%s" is not found in the component list' => 'Компонент "%s" не найден в списке компонентов',
    'The component class is empty' => 'Не указан класс компонента',
    'The component path is empty' => 'Не указан путь к компоненту <b>"%s"</b>',
    'The component class "%s" does not exist' => 'Класс компонента <b>"%s"</b> не существует',
    'Unable to load template "%s"' => 'Невозможно загрузить шаблон "%s"',
    'Unable to load page "%s"' => 'Невозможно загрузить страницу "%s"',
    'No write access to the cache' => 'Нет доступа для записи данных в кэш',

    // GConfig
    // messages
    'Can not set the time zone "%s"' => 'Невозможно установить временную зону "%s"',
    'Can not set the local path "%s"' => 'Невозможно установить локальный путь "%s"',
    'You can not use the session (%s)' => 'Невозможно использовать сессии (%s)',
    'Unable to mount language (%s)'=> 'Невозможно подключить язык (%s)'
);
?>