<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Список фотоальбомов"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('albums'))) return;

// пагинация
$pagination = '';
$paginationTop = '';
$paginationBottom = '';
if ($tpl['pagination']) {
    $pagination .= '<ul class="pagination pagination-sm">';
    foreach($tpl['pagination'] as $index => $item) {
        switch ($item['type']) {
            case 'first': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Первая страница"><span aria-hidden="true">&laquo;</span></a></li>'; break;
            case 'prev': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Предыдущая страница"><span aria-hidden="true">&lsaquo;</span></a></li>'; break;
            case 'more': $pagination .= '<li class="disabled"><a href="#">▪▪▪</a></li>'; break;
            case 'page': $pagination .= '<li><a href="' . $item['url'] . '">' . $item['page'] . '</a></li>'; break;
            case 'active': $pagination .= '<li class="active"><a href="#">' . $item['page'] . '</a></li>'; break;
            case 'next': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Следущая страница"><span aria-hidden="true">&rsaquo;</span></a></li>'; break;
            case 'last': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Последняя страница"><span aria-hidden="true">&raquo;</span></a></li>'; break;
        }
    }
    $pagination .= '</ul>';
    if ($tpl['pagination-pos'] == 'top' || $tpl['pagination-pos'] == 'both')
        $paginationTop = $pagination;
    if ($tpl['pagination-pos'] == 'bottom' || $tpl['pagination-pos'] == 'both')
        $paginationBottom = $pagination;
}
// режим конструктора
echo $tpl['design-tag-open'];

?>
<div id="bg-banner-albums-long" class="banner"></div>

<!-- list albums -->
<section class="s-albums list">
    <div class="s-albums-body">
    <div id="bg-banner"></div>
<?php
/*
if ($tpl['category-name'])
    echo '<h2 class="title tc">', $tpl['category-name'], '</h2>';
*/
// включить календарь
if ($tpl['use-calendar']) :
?>
    <div class="calendar">
        <span class="calendar-date"><?php echo $tpl['date-on'] ? GDate::format('d F', $tpl['date-on']['timestamp']) : 'Календарь';?></span>
        <a class="calendar-btn" data-date-format="yyyy-mm-dd" href="#"></a>
        <?php if ($tpl['date-on']) : ?>
        <a class="calendar-all" href="{chost}/albums/">все записи</a>
        <?php endif;?>
    </div>
<?php
 endif;
 
// пагинация
echo $paginationTop;
?>
        <div class="row">
<?php
if (!($count = $tpl['count']))
    echo '<div class="gear-page-public">Мы приносим свои извинения, но по вашему запросу нет записей на странице!</div>';
foreach ($tpl['items'] as $t) : $date = GDate::format('d F / l', $t['date']); ?>
        <div class="s-albums-i">
            <a href="{chost}/albums/<?=$t['id'];?>">
                <img src="{chost}/data/albums/<?=$t['folder'], '/', $t['cover'];?>" title="Фотоотчёт / <?=$date, ' / ', $t['name'];?>" alt="Фотоотчёт / <?=$date, ' / ', $t['name'];?>" />
                <span class="s-albums-i-overlay">
<?php
    // режим конструктора
    if ($tpl['design-tag-control'])
        echo str_replace('%s', $t['id'], $tpl['design-tag-control']);
?>
                    <span class="s-albums-i-text">
                        <div class="s-albums-i-date"><?=$date;?></div>
                        <div class="s-albums-i-title"><?=$t['name'];?></div>
                    </span>
                </span>
            </a>
        </div>
<?php endforeach; ?>
        </div>
<?php
// пагинация
echo $paginationBottom;
?>
    </div>
</section>
<!-- /list albums -->
<?php
// режим конструктора
echo $tpl['design-tag-close'];

// включить календарь
if ($tpl['use-calendar']) :
?>
<script type="text/javascript">
    $(document).ready(function(){
        gear.calendarBs({
            selector: '.calendar-btn',
            language: '<?php echo $tpl['language'];?>',
            category: <?php echo $tpl['category-id'];?>,
            highlight: <?php echo $tpl['calendar-highlight'] ? 'true' : 'false';?>,
            dateOn: <?php echo $tpl['date-on'] ? '{year:' . $tpl['date-on']['year'] . ',month:' . ($tpl['date-on']['month'] - 1) . ',day:' . $tpl['date-on']['day'] . '}' : 'false'; ?>
        });
    });
</script>
<?php endif; ?>