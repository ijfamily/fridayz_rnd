<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Лента видеозаписей"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('video-feed'))) return;

if (!($count = $tpl['count'])) return;

// режим конструктора
echo $tpl['design-tag-open'];
$first = true;
?>
<!-- feed video -->
<section class="s-video feed">
    <div class="s-video-title"><img src="{host}/data/images/fridayz-tv-white.png" title="Fridayz TV" alt="Fridayz TV"></div>
    <div class="s-video-body">
        <div class="row">
<?php foreach ($tpl['items'] as $t) : ?>
            <div class="col-md-4 col-sm-4 <?php echo $first ? '' : ' s-video-i-slide'; $first = false; ?>">
                <div class="s-video-i">
                    <img src="<?php echo $t['thumbnail'];?>" alt="<?php echo $t['title'];?>" title="<?php echo $t['title'];?>" >
                    <div id="video-<?php echo $t['code'];?>" class="video-footer" style="display: block;" data-id="<?php echo $t['code'];?>">
                        <span class="video-footer-plays">
                            <span class="icon"></span><span class="video-footer-count"></span>
                        </span>
                    </div>
                    <a class="s-video-play" href="#" data-code="<?php echo $t['code'];?>" title="<?php echo $t['title'];?>"></a>
                </div>
            </div>
<?php endforeach; ?>
        </div>
    </div>
    <div class="s-video-footer">
        <span class="s-video-more"></span>
    </div>
    <div class="s-video-btns"><a class="btn btn-white" href="{chost}/fridayz-tv/">Показать еще</a></div>
</section>
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>
