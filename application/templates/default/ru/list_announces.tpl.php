<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Список статей" (анонсы, объявления)
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('list-articles'))) return;

// пагинация
$pagination = '';
$paginationTop = '';
$paginationBottom = '';
if ($tpl['pagination']) {
    $pagination .= '<ul class="pagination pagination-sm">';
    foreach($tpl['pagination'] as $index => $item) {
        switch ($item['type']) {
            case 'first': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Первая страница"><span aria-hidden="true">&laquo;</span></a></li>'; break;
            case 'prev': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Предыдущая страница"><span aria-hidden="true">&lsaquo;</span></a></li>'; break;
            case 'more': $pagination .= '<li class="disabled"><a href="#">▪▪▪</a></li>'; break;
            case 'page': $pagination .= '<li><a href="' . $item['url'] . '">' . $item['page'] . '</a></li>'; break;
            case 'active': $pagination .= '<li class="active"><a href="#">' . $item['page'] . '</a></li>'; break;
            case 'next': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Следущая страница"><span aria-hidden="true">&rsaquo;</span></a></li>'; break;
            case 'last': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Последняя страница"><span aria-hidden="true">&raquo;</span></a></li>'; break;
        }
    }
    $pagination .= '</ul>';
    if ($tpl['pagination-pos'] == 'top' || $tpl['pagination-pos'] == 'both')
        $paginationTop = $pagination;
    if ($tpl['pagination-pos'] == 'bottom' || $tpl['pagination-pos'] == 'both')
        $paginationBottom = $pagination;
}
// режим конструктора
echo $tpl['design-tag-open'];

?>
<!-- list announces -->
<div class="container article">
<?php
if ($tpl['category-name'])
    echo '<h2><span>', $tpl['category-name'], '</span> </h2>';
if (!($count = $tpl['count'])) :
?>
    <div class="gear-page-public">Мы приносим свои извинения, но по вашему запросу нет записей на странице!</div>
</div>
<?php
return; endif;

// пагинация
echo $paginationTop;
?>
    <div class="items-ann">
<?php
for ($i = 0; $i < $count; $i++) :
    $t = $tpl['items'][$i];
?>
        <div class="items-ann-i clearfix">
<?php
    // режим конструктора
    if ($tpl['design-tag-control'])
        echo str_replace('%s', $t['id'], $tpl['design-tag-control']);

?>
            <div class="date"><?php echo '<span>', date('d', strtotime($t['announce-date'])), '</span> ', GDate::format('M', $t['announce-date']), '</span><span class="time">', date('H-s', strtotime($t['announce-time'])), '</span>';?></div>
            <div class="text">
               <a href="<?php echo $tpl['host'], $t['url'];?>"><?php echo $t['title'];?></a>
            </div>
        </div>
<?php endfor; ?>
    </div>
<?php
// пагинация
echo $paginationBottom;
?>
</div>
<!-- /list announces -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>