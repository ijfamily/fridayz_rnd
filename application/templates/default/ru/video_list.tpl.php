<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Список видеозаписей"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('video-list'))) return;

// пагинация
$pagination = '';
$paginationTop = '';
$paginationBottom = '';
if ($tpl['pagination']) {
    $pagination .= '<ul class="pagination pagination-sm">';
    foreach($tpl['pagination'] as $index => $item) {
        switch ($item['type']) {
            case 'first': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Первая страница"><span aria-hidden="true">&laquo;</span></a></li>'; break;
            case 'prev': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Предыдущая страница"><span aria-hidden="true">&lsaquo;</span></a></li>'; break;
            case 'more': $pagination .= '<li class="disabled"><a href="#">▪▪▪</a></li>'; break;
            case 'page': $pagination .= '<li><a href="' . $item['url'] . '">' . $item['page'] . '</a></li>'; break;
            case 'active': $pagination .= '<li class="active"><a href="#">' . $item['page'] . '</a></li>'; break;
            case 'next': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Следущая страница"><span aria-hidden="true">&rsaquo;</span></a></li>'; break;
            case 'last': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Последняя страница"><span aria-hidden="true">&raquo;</span></a></li>'; break;
        }
    }
    $pagination .= '</ul>';
    if ($tpl['pagination-pos'] == 'top' || $tpl['pagination-pos'] == 'both')
        $paginationTop = $pagination;
    if ($tpl['pagination-pos'] == 'bottom' || $tpl['pagination-pos'] == 'both')
        $paginationBottom = $pagination;
}
// режим конструктора
echo $tpl['design-tag-open'];

?>
<div id="bg-banner1-fridayztv" class="banner"></div>

<!-- list video -->
<div class="s-video list">
    <div class="s-video-title"><img src="{host}/data/images/fridayz-tv.png" title="Fridayz TV" alt="Fridayz TV"></div>
    <div class="s-video-body">
<?php
// включить календарь
if ($tpl['use-calendar']) :
?>
    <div class="calendar">
        <span class="calendar-date"><?php echo $tpl['date-on'] ? GDate::format('d F', $tpl['date-on']['timestamp']) : GDate::format('d F', time());?></span>
        <a class="calendar-btn" data-date-format="yyyy-mm-dd" href="#"></a>
        <?php if ($tpl['date-on']) : ?>
        <a class="calendar-all" href="{chost}/fridayz-tv/">все записи</a>
        <?php endif;?>
    </div>
<?php
 endif;

// пагинация
echo $paginationTop;
?>

<?php
if (!($count = $tpl['count']))
    echo  '<div class="gear-page-public">Мы приносим свои извинения, но по вашему запросу нет записей на странице!</div>';

$index = 0;
foreach($tpl['items'] as $item) {
    if ($index == 0)
        echo _n, '<div class="row">', _n;
?>
    <div class="col-md-6">
        <iframe data-id="<?php echo $item['code'];?>" width="100%" height="360" src="https://player.vimeo.com/video/<?php echo $item['code'];?>" frameborder="0" webkitallowfullscreen="webkitallowfullscreen" mozallowfullscreen="mozallowfullscreen" allowfullscreen="allowfullscreen"></iframe>
        <div id="video-<?php echo $item['code'];?>" class="video-footer" data-id="<?php echo $item['code'];?>">
            <span class="video-footer-plays">
<?php
    // режим конструктора
    if ($tpl['design-tag-control'])
        echo str_replace('%s', $item['id'], $tpl['design-tag-control']);
?>
                <span class="icon"></span><span class="video-footer-count">0</span>
            </span>
        </div>
    </div>
<?php
    if ($index == 1) {
        echo '</div>';
        $index = -1;
    }
    $index++;
}
    echo '</div>';

// пагинация
echo $paginationBottom;
?>
</div>
<!-- /list video -->
<?php
// режим конструктора
echo $tpl['design-tag-close'];

// включить календарь
if ($tpl['use-calendar']) :
?>
<script type="text/javascript">
    $(document).ready(function(){
        gear.calendarBs({
            selector: '.calendar-btn',
            language: '<?php echo $tpl['language'];?>',
            category: <?php echo $tpl['category-id'];?>,
            highlight: <?php echo $tpl['calendar-highlight'] ? 'true' : 'false';?>,
            dateOn: <?php echo $tpl['date-on'] ? '{year:' . $tpl['date-on']['year'] . ',month:' . ($tpl['date-on']['month'] - 1) . ',day:' . $tpl['date-on']['day'] . '}' : 'false'; ?>
        });
    });
</script>
<?php endif; ?>