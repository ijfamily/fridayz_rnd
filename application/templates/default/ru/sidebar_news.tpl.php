<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Сайдбар статей"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('feed-news'))) return;

if (!($count = $tpl['count'])) return;

// режим конструктора
echo $tpl['design-tag-open'];

?>
<!-- sidebar news -->
<div class="sb-news">
<?php
for ($i = 0; $i < $count; $i++) :
    $t = $tpl['items'][$i];
?>
    <div class="sb-news-i">
            <a class="title" href="{chost}<?php echo $t['url'];?>"><?php echo $t['title'];?></a>
            <div class="desc"><?php echo $t['text'];?></div>
<?php
    // режим конструктора
    if ($tpl['design-tag-control'])
        echo str_replace('%s', $t['id'], $tpl['design-tag-control']);
?>
    </div>
<?php endfor; ?>
</div>
<!-- /sidebar news -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>