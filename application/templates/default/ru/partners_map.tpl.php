<?php
/**
 * Gear CMS
 *
 * Шаблон компонента "Карта партнёров"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('partners-map'))) return;

// если нет партнёров
if (empty($tpl['list'])) return;

$arr = array();
$places = '';
foreach ($tpl['list'] as $index => $item) {
    /*
    $places .= "mapPlaces.geoObjects.add(new ymaps.Placemark([" . $item['coord'] 
            . "], { balloonContent: '" . $item['name'] 
            . "'}, { iconImageHref: '/data/images/icon-bar.png', iconImageSize: [36, 40], iconImageOffset: [10, -38] }));\r\n";
    */
    $arr[] = "[ [" . $item['coord'] 
            . "], { balloonContent: '" . $item['name'] 
            . "'}, { iconImageHref: '/data/images/icon-bar.png', iconImageSize: [36, 40], iconImageOffset: [10, -38] } ]  \r\n";
}
$places = implode(',', $arr);
?>
<section class="s-map">
    <h2 class="title1">#Рядом</h2>
    <script type="text/javascript">
        var partnerPlaces = [<?php echo $places; ?>];
    </script>
    <div id="map-places" style="width:100%; height:400px"></div>
</section>