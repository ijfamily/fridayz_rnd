<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Список статей" (новости города)
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('list-articles'))) return;

// пагинация
$pagination = '';
$paginationTop = '';
$paginationBottom = '';
if ($tpl['pagination']) {
    $pagination .= '<ul class="pagination pagination-sm">';
    foreach($tpl['pagination'] as $index => $item) {
        switch ($item['type']) {
            case 'first': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Первая страница"><span aria-hidden="true">&laquo;</span></a></li>'; break;
            case 'prev': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Предыдущая страница"><span aria-hidden="true">&lsaquo;</span></a></li>'; break;
            case 'more': $pagination .= '<li class="disabled"><a href="#">▪▪▪</a></li>'; break;
            case 'page': $pagination .= '<li><a href="' . $item['url'] . '">' . $item['page'] . '</a></li>'; break;
            case 'active': $pagination .= '<li class="active"><a href="#">' . $item['page'] . '</a></li>'; break;
            case 'next': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Следущая страница"><span aria-hidden="true">&rsaquo;</span></a></li>'; break;
            case 'last': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Последняя страница"><span aria-hidden="true">&raquo;</span></a></li>'; break;
        }
    }
    $pagination .= '</ul>';
    if ($tpl['pagination-pos'] == 'top' || $tpl['pagination-pos'] == 'both')
        $paginationTop = $pagination;
    if ($tpl['pagination-pos'] == 'bottom' || $tpl['pagination-pos'] == 'both')
        $paginationBottom = $pagination;
}
// режим конструктора
echo $tpl['design-tag-open'];

?>
<div id="bg-banner-top-city" class="banner"></div>

<!-- list news -->
<div class="s-news list">
<?php
/*
if ($tpl['category-name'])
    echo '<h2 class="title tc">', $tpl['category-name'], '</h2>';
*/
// включить календарь
if ($tpl['use-calendar']) :
?>
    <div class="calendar">
        <span class="calendar-date"><?php echo $tpl['date-on'] ? GDate::format('d F', $tpl['date-on']['timestamp']) : GDate::format('d F', time());?></span>
        <a class="calendar-btn" data-date-format="yyyy-mm-dd" href="#"></a>
        <?php if ($tpl['date-on']) : ?>
        <a class="calendar-all" href="{chost}/news/">все записи</a>
        <?php endif;?>
    </div>
<?php
 endif;

// пагинация
echo $paginationTop;
?>
    <div class="s-news-body">
<?php
if (!($count = $tpl['count']))
    echo  '<div class="gear-page-public">Мы приносим свои извинения, но по вашему запросу нет записей на странице!</div>';

for ($i = 0; $i < $count; $i++) :
    $t = $tpl['items'][$i];
?>
    <div class="s-news-i<?php echo $i == 0 ? ' first' : '';?>">
        <a href="{chost}<?php echo $t['url'];?>"><img src="<?php echo $tpl['host'], (empty($t['img']) ? 'no_image.jpg' : $t['img']);?>" /></a>
        <div class="info">
            <a href="{chost}<?php echo $t['url'];?>">
            <div class="date">
<?php
    // режим конструктора
    if ($tpl['design-tag-control'])
        echo str_replace('%s', $t['id'], $tpl['design-tag-control']);
?>
            <?php echo GDate::format('l, d F', $t['date']);?>
            </div>
            <div class="title"><?php echo $t['title'];?></div>
            <div class="desc"><?php echo $t['text'];?></div>
            </a>
        </div>
<?php if ($t['tags']) : ?>
            <div class="tags"><?php echo $t['tags'];?></div>
<?php endif; ?>
    </div>
<?php endfor; ?>
    </div>
<?php
// пагинация
echo $paginationBottom;
?>
</div>
<!-- /list news -->
<?php
// режим конструктора
echo $tpl['design-tag-close'];

// включить календарь
if ($tpl['use-calendar']) :
?>
<script type="text/javascript">
    $(document).ready(function(){
        gear.calendarBs({
            selector: '.calendar-btn',
            language: '<?php echo $tpl['language'];?>',
            category: <?php echo $tpl['category-id'];?>,
            highlight: <?php echo $tpl['calendar-highlight'] ? 'true' : 'false';?>,
            dateOn: <?php echo $tpl['date-on'] ? '{year:' . $tpl['date-on']['year'] . ',month:' . ($tpl['date-on']['month'] - 1) . ',day:' . $tpl['date-on']['day'] . '}' : 'false'; ?>
        });
    });
</script>
<?php endif; ?>