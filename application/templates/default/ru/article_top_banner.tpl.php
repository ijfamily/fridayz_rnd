<?php
/**
 * Gear CMS
 *
 * Шаблон компонента "Статья"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('article'))) return;
?>
<article id="article-<?php echo $tpl['id'];?>" data-id="<?php echo $tpl['id'];?>" class="article">
<?php
// режим конструктора
echo $tpl['design-tag-open'];
?>
<div id="bg-banner"></div>

<?php
if (!empty($tpl['header']) && $tpl['show-header'])
    echo '<h2 class="title tc">', $tpl['header'], '</h2>';
echo $tpl['html'];

// режим конструктора
echo $tpl['design-tag-close'];
?>
<br />
</article>