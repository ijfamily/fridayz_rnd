<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Форма обратного вызова"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('user-profile'))) return;

Gear::doc(array('title' => 'Профиль пользователя'));

?>
<section class="s-form">
<h2 class="title tc">Профиль пользователя</h2>
<!-- form user profile -->
<div class="s-form-wrap">
<form  id="<?php echo $tpl['attr']['id'];?>" class="form-horizontal form-signup" role="form" method="post" action="<?php echo $tpl['use-ajax'] ? $tpl['form-action'] : '#' . $tpl['attr']['id'];?>">
<?php
if (!$tpl['use-ajax']) {
    if ($tpl['message']) {
        if ($tpl['is-success'])
            echo '<div class="alert alert-success">Данные успешно изменены!</div>';
        else
            echo '<div class="alert alert-danger"><strong>Ошибка!</strong> ', $tpl['message'], '</div>';
    }
}
if ($tpl['use-ajax']) {
?>
<?php
}
?>
    <input type="hidden" name="target" value="<?php echo $tpl['target'];?>"/>
    <input type="hidden" name="token" value="<?php echo $tpl['fields']['token'];?>"/>
    <div class="form-wrap">
        <div class="form-group">
             <label class="control-label col-sm-4" for="fname">Имя:</label>
             <div class="col-sm-8">
                <input type="text" class="form-control" id="fname" name="fname" maxlength="50" value="<?php echo $tpl['fields']['fname'];?>" placeholder="Введите ваше имя" required />
             </div>
        </div>
        <div class="form-group">
             <label class="control-label col-sm-4" for="lname">Фамилия:</label>
             <div class="col-sm-8">
                <input type="text" class="form-control" id="lname" name="lname" maxlength="50" value="<?php echo $tpl['fields']['lname'];?>" placeholder="Введите вашу фамилию" required />
             </div>
        </div>
        <div class="form-group">
             <label class="control-label col-sm-4" for="email">E-mail:</label>
             <div class="col-sm-8">
                <input type="text" class="form-control" id="email" name="email" maxlength="50" value="<?php echo $tpl['fields']['email'];?>" placeholder="Введите ваш e-mail" required />
             </div>
        </div>
        <div class="form-group">
             <label class="control-label col-sm-4" for="password">Пароль:</label>
             <div class="col-sm-8">
                <input type="password" class="form-control" id="password" name="password" maxlength="20" value="" required />
             </div>
        </div>
        <div class="form-group">
             <label class="control-label col-sm-4" for="password-cnf">Пароль <small>(подтв.)</small>:</label>
             <div class="col-sm-8">
                <input type="password" class="form-control" id="password-cnf" name="password-cnf" maxlength="20" value="" required />
             </div>
        </div>
    </div>
    <div class="form-wrap">
        <div class="row" style="text-align: center;">
            <a type="button" class="btn btn-primary" title="В личный кабинет" href="{chost}/user/">&laquo; Назад</a>
            <button type="submit" class="btn btn-success">Изменить</button>
            <a type="button" class="btn btn-danger" title="Удалить аккаунт пользователя" href="{chost}/user/?action=delete">Удалить аккаунт</a>
        </div>
    </div>
</form>
<?php
// использовать проверку
if ($tpl['attr']['use-scripts']) :
?>
<script type="text/javascript">
    (function(w, n, t, id) {
        w[n] = w[n] || {}; w[n][id] = { id: id, ajax: <?php echo $tpl['use-ajax'] ? 'true' : 'false';?>, data: {}, valid: { name: t, css: true } };
    })(window, "gearForms", "formValidation", "<?php echo $tpl['attr']['id'];?>");
</script>
<?php endif; ?>
<!-- /form user profile -->
</div>
</section>