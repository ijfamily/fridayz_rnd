<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Форма восстановления пароля (письмо)"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('data-mail'))) return;

?>
<html>
<body>
    <p>Для восстановления вашего аккаунта на сайте <b><?php echo $tpl['domain'];?></b>, перейдите по ссылке 
    <a href="<?php echo $tpl['host'];?>recovery-password/?<?php echo 'hash=', $tpl['hash'];?>"><?php echo $tpl['host'];?>recovery-password/?<?php echo 'hash=', $tpl['hash'];?></a> </p>
    <p style="padding-left: 20px;">Код восстановления: <?php echo $tpl['code'];?></p>
    <br />
    <p>---</p>
    <br />
    <p>Вы получили это сообщение, потому что Ваш email-адрес был указан для восcтановления аккаунта на сайте <b><?php echo $tpl['domain'];?></b>.</p>
    <p>Если данное письмо попало к Вам по ошибке, пожалуйста, проигнорируйте его и примите наши извинения.</p><br />
    <p>С наилучшими пожеланиями,<br />
    Администрация сайта <?php echo $tpl['domain'];?></p>
</body>
</html>