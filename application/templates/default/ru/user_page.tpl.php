<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Информация о пользователе"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('user-page'))) return;

Gear::doc(array('title' => 'Личный кабинет'));

// режим конструктора
echo $tpl['design-tag-open'];

if ($tpl['is-deleted']) :
?>
<section class="s-form">
    <h2 class="title tc">Личный кабинет</h2>
    <div class="alert alert-success">Ваш аккаунт успешно удален. Всего хорошего!</div>
</section>
<?php
return;
endif;

if (empty($tpl['user'])) :
?>
<section class="s-form">
    <h2 class="title tc">Личный кабинет</h2>
    <div class="alert alert-warning"><strong>Предупреждение!</strong> Личный кабинет доступен только для зарегистрированных пользователей. 
    Пройдите <a href="{chost}/signup/">регистрацию</a> или авторизацию!</div>
</section>
<?php
return;
endif;
?>
<?php
//Gear::code($tpl);
?>
<section class="s-form">
    <h2 class="title tc">Личный кабинет</h2>
    <ul class="list-group">
        <li class="list-group-item"><strong>Фамилия</strong>: <?php echo $tpl['user']['profile_last_name'];?></li>
        <li class="list-group-item"><strong>Имя</strong>: <?php echo $tpl['user']['profile_first_name'];?></li>
        <li class="list-group-item"><strong>E-mail</strong>: <?php echo $tpl['user']['contact_email'];?></li>
    </ul>
    <div>
        <a type="button" class="btn btn-danger" title="Удалить аккаунт пользователя" href="{chost}/user/?action=delete">Удалить аккаунт</a> 
        <a type="button" class="btn btn-primary" title="Выйти из личного кабинета" href="{chost}/user/?action=signout">Выйти</a>
        <a type="button" class="btn btn-primary" title="Изминить данные пользователя" href="{chost}/user/profile/">Изменить</a>
    </div>
</section>
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>