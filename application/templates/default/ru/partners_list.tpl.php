<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Список партнёров"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('partners-list'))) return;

// пагинация
$pagination = '';
$paginationTop = '';
$paginationBottom = '';
if ($tpl['pagination']) {
    $pagination .= '<ul class="pagination pagination-sm">';
    foreach($tpl['pagination'] as $index => $item) {
        switch ($item['type']) {
            case 'first': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Первая страница"><span aria-hidden="true">&laquo;</span></a></li>'; break;
            case 'prev': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Предыдущая страница"><span aria-hidden="true">&lsaquo;</span></a></li>'; break;
            case 'more': $pagination .= '<li class="disabled"><a href="#">▪▪▪</a></li>'; break;
            case 'page': $pagination .= '<li><a href="' . $item['url'] . '">' . $item['page'] . '</a></li>'; break;
            case 'active': $pagination .= '<li class="active"><a href="#">' . $item['page'] . '</a></li>'; break;
            case 'next': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Следущая страница"><span aria-hidden="true">&rsaquo;</span></a></li>'; break;
            case 'last': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Последняя страница"><span aria-hidden="true">&raquo;</span></a></li>'; break;
        }
    }
    $pagination .= '</ul>';
    if ($tpl['pagination-pos'] == 'top' || $tpl['pagination-pos'] == 'both')
        $paginationTop = $pagination;
    if ($tpl['pagination-pos'] == 'bottom' || $tpl['pagination-pos'] == 'both')
        $paginationBottom = $pagination;
}
// режим конструктора
echo $tpl['design-tag-open'];

?>
<div id="bg-banner-top-long" class="banner"></div>

<!-- list partners -->
<section class="s-places thumbs">
    <div class="s-places-body">
<?php
if ($tpl['categories']) {
    echo '<div class="types">';
    foreach ($tpl['categories'] as $index => $item)
        echo '<a ', ($tpl['category-id'] == $item['id'] ? ' class="active" ' : ''), 'href="{host}/', $item['uri'] ,'">', $item['name'], '</a>';
    echo '</div>';
}

// пагинация
echo $paginationTop;
?>

<?php
if (!($count = $tpl['count']))
    echo '<div class="gear-page-public">Мы приносим свои извинения, но по вашему запросу нет записей на странице!</div>';
$j = $icolor = 0;
$colors = array('pink', 'blue', 'green', 'orange');
for ($i = 0; $i < $count; $i++) :
    $t = $tpl['items'][$i];
    if ($j == 0) {
        echo '<div class="row">';
    }
    if (empty($t['cover'])) {
        $class = ' ' . $colors[$icolor];
        $image = /*$t['cat-image']*/'';
        $icolor++;
        if ($icolor > 3) $icolor = 0;
    } else {
        $class = '';
        $image = ' style="background-image:  url(' . $t['cover'] . ')"';
    }
    if ($t['phone']) {
        $phone = explode(',', $t['phone']);
        $phone = $phone[0];
    } else
        $phone = '';
    if ($t['address']) {
        $address = explode(';', $t['address']);
        $address = $address[0];
    } else
        $address = '';
    //<?php echo $colors[$j];?>

        <div class="col-md-3 col-sm-6 col-place">
        <div class="s-places-i s-places-i-general">
            <div class="s-places-i-front s-places-i-general-front<?=$class;?>" <?=$image;?>>
                <?php if (empty($t['image'])) : ?>
                <div class="place-logo">
                    <div class="place-frame-t"></div>
                    <h2 class="place-frame-title"><?php echo $t['name'];?></h2>
                    <div class="place-frame-b"></div>
                </div>
                <?php else : ?>
                <img src="<?php echo $t['image'];?>" />
                <?php endif; ?>
            </div>
            <div class="s-places-i-back s-places-i-general-back">
                <?php
                // режим конструктора
                if ($tpl['design-tag-control'])
                    echo str_replace('%s', $t['id'], $tpl['design-tag-control']);
                ?>
                <div class="s-places-info">
                    <?php if ($address) : ?>
                    <div class="address"><b>Адрес</b>: <?php echo $address;?></div>
                    <?php endif; ?>
                    <?php if ($phone) : ?>
                    <div class="phone"><b>Телефон</b>: <?php echo $phone;?></div>
                    <?php endif; ?>
                </div>
                <div class="s-places-brand">
                    <div class="rating">
                        <select id="rating-<?php echo $t['id'];?>" name="rating" class="barrating" autocomplete="off">
                          <option value="1"<?php echo $t['rating-state'] == 1 ? ' selected' : '';?>>1</option>
                          <option value="2"<?php echo $t['rating-state'] == 2 ? ' selected' : '';?>>2</option>
                          <option value="3"<?php echo $t['rating-state'] == 3 ? ' selected' : '';?>>3</option>
                          <option value="4"<?php echo $t['rating-state'] == 4 ? ' selected' : '';?>>4</option>
                          <option value="5"<?php echo $t['rating-state'] == 5 ? ' selected' : '';?>>5</option>
                        </select>
                        <div class="rating-msg"></div>
                        <div class="rating-title"><?php echo $t['rating-average'];?><span>рейтинг</span></div>
                    </div>
                   <?php if ($t['extended']) { ?>
                    <a class="btn btn-small btn-primary" href="{chost}/<?php echo $t['uri'];?>/">подробнее</a>
                    <?php } else { ?>
                    <a class="btn btn-primary btn-disabled" href="#">подробнее</a>
                    <?php } ?>
                </div>
            </div>
        </div>
        </div>
<?php
    if ($j == 3) {
        echo '</div>';
        $j = -1;
    }
    $j++;
endfor;
?>

        
<?php
// пагинация
echo $paginationBottom;
?>
    </div>
</section>
<!-- /.list partners -->
<?php
// режим конструктора
echo $tpl['design-tag-close'];
?>