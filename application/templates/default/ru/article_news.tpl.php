<?php
/**
 * Gear CMS
 *
 * Шаблон компонента "Статья"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('article'))) return;
?>
<div class="row row-article">
    <div class="col-md-9 col-article">
<?php

?>
        <article id="article-<?php echo $tpl['id'];?>" data-id="<?php echo $tpl['id'];?>" class="article">
        <?php
        // режим конструктора
        echo $tpl['design-tag-open'];

        if (!empty($tpl['header']) && $tpl['show-header'])
            echo '<h2 class="title">', $tpl['header'], '</h2>';

        echo $tpl['html'];
        
        // режим конструктора
        echo $tpl['design-tag-close'];
        ?>
        </article>
    </div>
    <div class="col-md-3 col-sidebar">
        <div class="sidebar-right">
<?php
Gear::component('FeedNews', 'feed/news/', array('limit' => 5, 'category-id' => 2, 'template' => 'sidebar_news.tpl.php'));
?>
        </div>
    </div>
</div>