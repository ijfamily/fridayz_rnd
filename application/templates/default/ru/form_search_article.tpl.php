<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Форма обратного вызова"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('form-search-article'))) return;

// пагинация
$pagination = '';
$paginationTop = '';
$paginationBottom = '';
if ($tpl['pagination']) {
    $pagination .= '<ul class="pagination pagination-sm">';
    foreach($tpl['pagination'] as $index => $item) {
        switch ($item['type']) {
            case 'first': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Первая страница"><span aria-hidden="true">&laquo;</span></a></li>'; break;
            case 'prev': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Предыдущая страница"><span aria-hidden="true">&lsaquo;</span></a></li>'; break;
            case 'more': $pagination .= '<li class="disabled"><a href="#">▪▪▪</a></li>'; break;
            case 'page': $pagination .= '<li><a href="' . $item['url'] . '">' . $item['page'] . '</a></li>'; break;
            case 'active': $pagination .= '<li class="active"><a href="#">' . $item['page'] . '</a></li>'; break;
            case 'next': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Следущая страница"><span aria-hidden="true">&rsaquo;</span></a></li>'; break;
            case 'last': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Последняя страница"><span aria-hidden="true">&raquo;</span></a></li>'; break;
        }
    }
    $pagination .= '</ul>';
    if ($tpl['pagination-pos'] == 'top' || $tpl['pagination-pos'] == 'both')
        $paginationTop = $pagination;
    if ($tpl['pagination-pos'] == 'bottom' || $tpl['pagination-pos'] == 'both')
        $paginationBottom = $pagination;
}
?>
<section class="s-search">
    <h2 class="title tc">Поиск по сайту</h2>
<!-- form search -->
<form id="form-search" class="form-inline form-search" role="form" method="get" action="/search/">
    <div class="form-group">
        <input type="text" class="form-control input-search" id="q" name="q" maxlength="64" value="<?php echo $tpl['fields']['q'];?>" placeholder="Введите слова для поиска" required />
    </div>
    <button type="submit" class="btn btn-search">Найти</button>
</form>
<script type="text/javascript">
    (function(w, n, t, id) {
        w[n] = w[n] || {}; w[n][id] = { id: id, ajax: false, data: {}, valid: { name: t, css: true } };
    })(window, "gearForms", "formValidation", "form-search");
</script>
<!-- /form search -->
<?php
if (!($count = $tpl['count']) && !empty($tpl['search'])) :
?>
<div style="text-align: center;">
    <div class="search-none">
        <p>Страницы, содержащие все слова запроса, не найдены.</p>
        <p>По запросу <span>"<?php echo $tpl['search'];?>"</span> ничего не найдено.</p>
        <p>Рекомендации:</p>
        <ul>
            <li>Убедитесь, что все слова написаны без ошибок.</li>
            <li>Попробуйте использовать другие ключевые слова.</li>
            <li>Попробуйте использовать более популярные ключевые слова.</li>
        </ul>
    </div>
<?php
return;
endif;
// если по запросу что-то нашли
if ($tpl['count'] > 0) :
?>
<div class="search-info">По Вашему запросу найдено <span><?php echo $tpl['count']; ?></span> запис(ь)ей.</div>
<?php
endif;
// если
if (!$tpl['is-success']) :
?>
<div class="search-info"><?php echo $tpl['message']; ?></div>
<?php
endif;

// пагинация
echo $paginationTop;

for ($i = 0; $i < $count; $i++) :
    $t = $tpl['items'][$i];
?>
    <div class="s-news-i" href="{chost}/<?php echo $t['url'];?>">
        <a href="{chost}<?php echo $t['url'];?>"><img src="<?php echo $tpl['host'], (empty($t['img']) ? 'no_image.jpg' : $t['img']);?>" /></a>
        <div class="info">
            <a href="{chost}<?php echo $t['url'];?>">
            <div class="date"><?php echo GDate::format('l, d F', $t['date']);?></div>
            <div class="title"><?php echo $t['title'];?></div>
            <div class="desc"><?php echo $t['text'];?></div>
            </a>
        </div>
<?php if ($t['tags']) : ?>
            <div class="tags"><?php echo $t['tags'];?></div>
<?php endif; ?>
<?php
    // режим конструктора
    if ($tpl['design-tag-control'])
        echo str_replace('%s', $t['id'], $tpl['design-tag-control']);
?>
    </div>
<?php
endfor;

// пагинация
echo $paginationBottom;
?>

</section>