<?php
/**
 * Gear CMS
 *
 * Шаблон компонента "Статья (резиденты)"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('article'))) return;
?>
<div id="bg-banner-residents" class="banner"></div>

<article id="article-<?php echo $tpl['id'];?>" data-id="<?php echo $tpl['id'];?>" class="article article-residents">
<?=$tpl['design-tag-open'];?>
    
<?php
echo $tpl['html'];

// режим конструктора
echo $tpl['design-tag-close'];
?>
</article>