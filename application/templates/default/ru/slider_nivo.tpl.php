<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Nivo слайдер"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('slider-nivo'))) return;

// режим конструктора
echo $tpl['design-tag-open'];

?>
<!-- slider -->
<div id="<?php echo $tpl['attr']['id'];?>" class="nivoSlider">
<?php
$count = $tpl['count'];
for ($i = 0; $i < $count; $i++) :
    $t = $tpl['items'][$i];
?>
    <img src="<?php echo $tpl['dir'], $t['src'];?>" data-thumb="<?php echo $tpl['dir'], $t['src'];?>" title="<?php echo $t['title'];?>"/>
<?php
    // режим конструктора
    if ($tpl['design-tag-control'])
        echo '<div align="center"> &nbsp;', strtr($tpl['design-tag-control'], array('%s' => $t['id'])) . '</div>';
endfor;
?>
</div>
<!-- /slider -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>