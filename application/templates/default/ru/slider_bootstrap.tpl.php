<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Слайдер Bootstrap"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('slider-bootstrap'))) return;

$count = $tpl['count'];
// режим конструктора
echo $tpl['design-tag-open'];

?>
<!-- slider -->
<div id="<?php echo $tpl['attr']['id'];?>" class="carousel slide">
    <!-- indicators -->
    <div class="indicators-ct">
        <ol class="carousel-indicators">
<?php
for ($i = 0; $i < $count; $i++) :
    echo '<li data-target="#' . $tpl['attr']['id'] . '" data-slide-to="' . $i . '"' . ($i ==0 ? ' class="active"' : '') . '></li>' . _n_t;
endfor;
?>
        </ol>
    </div>
    <!-- wrapper for slides -->
    <div class="carousel-inner">
<?php
$wrapper = '';
$indicators = '';
$count = $tpl['count'];
for ($i = 0; $i < $count; $i++) :
    $t = $tpl['items'][$i];
    echo '<div class="item' . ($i ==0 ? ' active' : '') . '"><img src="' . $tpl['dir'] . $t['src'] . '" title="' . $t['title'] . '" alt="' . $t['title'] . '">' . _n_t;

    echo '<blockquote>';
    if (!empty($t['title'])) echo '<h2>', $t['title'], '</h2>';
    if (!empty($t['title-1'])) echo '<h3>', $t['title-1'], '</h3>';
    if (!empty($t['text'])) echo $t['text'];
    echo '</blockquote>';

    // режим конструктора
    if ($tpl['design-tag-control'])
        echo '<div align="center"> &nbsp;' . str_replace('%s', $t['id'], $tpl['design-tag-control']) . '</div>';

    echo'</div>';
endfor;
?>
    </div>
    <!-- controls -->
    <a class="left carousel-control" data-slide="prev" href="#<?php echo $tpl['attr']['id'];?>"></a>
    <a class="right carousel-control" data-slide="next" href="#<?php echo $tpl['attr']['id'];?>"></a>
</div>
<!-- /slider -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>