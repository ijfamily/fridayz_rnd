<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Главное меню сайта"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('menu'))) return;

// режим конструктора
echo $tpl['design-tag-open'];

?>
<!-- main menu -->
<div class="collapse navbar-collapse" id="navigation-example">
    <ul class="nav navbar-nav navbar-right">
<?php
foreach ($tpl['items'] as $id => $item) :
    echo _2t, '<li><a href="{scheme}', $item['url'], '" class="btn" title="', $item['name'], '">', $item['name'];

    // режим конструктора
    if ($tpl['design-tag-control'])
        echo str_replace('%s', $item['id'], $tpl['design-tag-control']);

    echo '</a></li>', _n;
endforeach;
?>
    </ul>
</div>
<!-- /main menu -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>