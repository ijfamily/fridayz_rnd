<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Форма обратного вызова"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('form-callback'))) return;

// режим конструктора
echo $tpl['design-tag-open'];

?>
<!-- form callback -->
<form  id="<?php echo $tpl['attr']['id'];?>" class="form-horizontal form-callback" role="form" method="post" action="<?php echo $tpl['use-ajax'] ? $tpl['form-action'] : '#' . $tpl['attr']['id'];?>">
<?php
if (!$tpl['use-ajax']) {
    if ($tpl['message']) {
        if ($tpl['is-success'])
            echo '<div class="alert alert-success">Спасибо за Ваше сообщение!</div>';
        else
            echo '<div class="alert alert-danger"><strong>Ошибка!</strong> ', $tpl['message'], '</div>';
    }
}
if ($tpl['use-ajax']) {
?>
    <input type="hidden" name="id" value="<?php echo $tpl['attr']['id'];?>"/>
    <input type="hidden" name="aid" value="<?php echo $tpl['article-id'];?>"/>
<?php
}
?>
    <input type="hidden" name="target" value="<?php echo $tpl['target'];?>"/>
    <input type="hidden" name="token" value="<?php echo $tpl['fields']['token'];?>"/>
    <div class="form-group">
         <label class="control-label col-sm-2" for="name">Имя:</label>
         <div class="col-sm-10">
            <input type="text" class="form-control" id="name" name="name" maxlength="50" value="<?php echo $tpl['fields']['name'];?>" placeholder="Введите ваше имя" required />
         </div>
    </div>
    <div class="form-group">
         <label class="control-label col-sm-2" for="phone">Телефон:</label>
         <div class="col-sm-10">
            <input type="text" class="form-control" id="phone" name="phone" maxlength="20" value="" placeholder="Введите ваш телефон" required />
         </div>
    </div>
<?php
// вывод капчи
if ($tpl['attr']['check-captcha']) :
?>
    <div class="form-group">
        <div class="col-sm-2"><img src="<?php echo $tpl['url-captcha'];?>" /></div>
        <div class="col-sm-10"><input type="text" class="form-control" name="code" required /></div>
    </div>
<?php endif; ?>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-10"><button type="submit" class="btn  btn-primary">Отправить</button></div>
    </div>
</form>
<?php
// использовать проверку
if ($tpl['attr']['use-scripts']) :
?>
<script type="text/javascript">
    (function(w, n, t, id) {
        w[n] = w[n] || {}; w[n][id] = { id: id, ajax: <?php echo $tpl['use-ajax'] ? 'true' : 'false';?>, data: {}, valid: { name: t, css: true } };
    })(window, "gearForms", "formValidation", "<?php echo $tpl['attr']['id'];?>");
</script>
<?php endif; ?>
<!-- /form callback -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>