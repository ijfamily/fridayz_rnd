<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Лента архива статей"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('feed-archive'))) return;

// режим конструктора
echo $tpl['design-tag-open'];

?>
<h2>Архив новостей</h2>
<!-- feed archive -->
<div class="items-arc">
<?php
$gb = $tpl['group-by'];
$count = $tpl['count'];
for ($i = 0; $i < $count; $i++) :
    $t = $tpl['items'][$i];
?>
    <div class="items-arc-i clearfix">
        <a href="http://<?php echo $_SERVER['HTTP_HOST'] . $t['url'];?>">
<?
    switch ($gb) {
        case 'day': echo '<span class="month">', $t['day'], ' ', GDate::format('F', $t['date']), '</span> <span class="year">', $t['year'], '</span> <span class="count">(', $t['count'], ')</span>'; break;
        case 'month': echo '<span class="month">', GDate::format('F', $t['date']), '</span> <span class="year">', $t['year'], '</span> <span class="count">(', $t['count'], ')</span>'; break;
        case 'year': echo '<span class="year">', $t['year'], '</span> <span class="count">(', $t['count'], ')</span>'; break;
    }
?>
        </a>
    </div>
<?php endfor; ?>
<div class="goto"><a href="/news/">Все новости</a></div>
</div>
<!-- /feed archive -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>