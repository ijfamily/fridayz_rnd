<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Лента новостей"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('gallery'))) return;

// режим конструктора
echo $tpl['design-tag-open'];

?>
<!-- feed afisha -->
<div class="s-afisha feed" style="display: none;">
    <div class="s-afisha-wrap">
    <div class="s-afisha-body">
        <div id="carousel-afisha" class="slider responsive">
<?php
foreach ($tpl['items'] as $index => $t) :
?>
    <div>
    <a class="s-afisha-i pirobox_gall" href="{host}/data/galleries/<?php echo $t['folder'], '/', $t['i']['file'];?>" title="" rel="gallery">
        <div class="img"><img src="{host}/data/galleries/<?php echo $t['folder'], '/', $t['t']['file'];?>" /></div>
    </a>
    </div>
<?php endforeach; ?>
        </div>
    </div>
    <div class="btn-controls" style="margin-top: 45px;"><a class="btn btn-white" href="{chost}/afisha/">Показать еще</a></div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="{host}/themes/gear/css/pirobox/style2/style.css" />
<script type="text/javascript" src="{host}/themes/gear/js/jquery-ui.custom.min.js?v=1.8.2"></script>
<script type="text/javascript" src="{host}/themes/gear/js/jquery.pirobox.ext.min.js?v=1.0"></script>
<!-- /feed afisha -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>