<!doctype html>

<html lang="{lang}">
<head>
    <component class="Meta"></component>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="yandex-verification" content="42ade3dcd327b719" />
    <meta name="google-site-verification" content="yYVv6Fs95sU_xmmECQ8L18v-gpaAzyMbGngYrloqVdw" />

    <link rel="shortcut icon" type="image/x-icon" href="{theme}/favicon.ico" />
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" type="text/css" href="{gear}/css/bootstrap.min.css" /><component class="StyleSheet"></component>
    <!-- Plugins CSS -->
    <link rel="stylesheet" type="text/css" href="{theme}/plugins/simplyscroll/css/jquery.simplyscroll.css" />
    <link rel="stylesheet" type="text/css" href="{theme}/plugins/bootstrap/css/bootstrap.touch-carousel.css" />
    <link rel="stylesheet" type="text/css" href="{theme}/plugins/barrating/themes/css-stars.css" />
    <link rel="stylesheet" type="text/css" href="{theme}/plugins/slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="{theme}/plugins/slick/slick-theme.css" />

    <link rel="stylesheet" type="text/css" href="{theme}/css/style.css" />
    <link rel="stylesheet" type="text/css" href="{gear}/css/gear.css" />

    <!-- jQuery -->
    <script type="text/javascript" src="{gear}/js/jquery.min.js?v1.10.1"></script>

    <!--[if lt IE 9]>
        <script src="{gear}/js/html5shiv.js"></script>
        <script src="{gear}/js/respond.min.js"></script>
        <link href="{theme}/css/style-ie.css" rel="stylesheet" />
    <![endif]-->
</head>

<body id="body">
<!--[if lt IE 9]>
    <div class="brw-ie">Вы пользуетесь устаревшей версией браузера Internet Explorer.
        <a href="http://www.microsoft.com/rus/windows/internet-explorer/">Перейти к загрузке Internet Explorer</a>
    </div>
<![endif]-->

    <component class="Counter"></component>

    <component class="StyleDeclaration"></component>

    <!-- Navigation -->
    <nav class="navbar navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <!-- Toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-menu">
                    <span class="sr-only">Свернуть</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{host}"><img src="{theme}/images/logo.png" /></a>
            </div>
            <!-- Nav search -->
            <div class="navbar-search">
                <form action="/search/" method="get" role="form">
                    <input type="text" class="form-control input-search" name="q" required="" maxlength="64"  placeholder="Поиск" />
                    <span href="#" class="navbar-search-close">×</span>
                </form>
            </div>
            <!-- Nav buttons -->
            <div class="navbar-buttons">
                <condition for="user" for-data="no"><button class="button button-user" title="Вход в личный кабинет"></button></condition>
                <condition for="user" for-data="signin">
                <a class="button button-profile dropdown-toggle" href="{host}/user/" title="Личный кабинет" data-toggle="dropdown"><img src="{avatar}" /></a>
                <div class="dropdown-menu dropdown-profile" style="padding: 15px; padding-bottom: 0px;">
                    <a href="{host}/user/" class="dropdown-profile-icon"><img src="{avatar}" /></a>
                    <div class="dropdown-profile-content">
                        <div class="name" href="{host}/user/">{fname} {lname}</div>
                        <div class="controls">
                            <a href="{host}/user/" class="control">изменить профиль</a> <a href="{host}/user/?action=signout" class="control-out">выйти</a>
                        </div>
                    </div>
                </div>
                </condition>
                <button class="button button-search"></button>
            </div>
            <!-- Nav links for toggling -->
            <div class="collapse navbar-collapse" id="navbar-collapse-menu">
                <ul class="nav navbar-nav navbar-right navbar-menu">
                    <li class="navbar-menu-item"><a href="{host}/afisha/" class="color1">#Афиша</a></li>
                    <li class="navbar-menu-item"><a href="{host}/gorod/" class="color2">#Город</a></li>
                    <li class="navbar-menu-item"><a href="{host}/news/" class="color3">#Статьи</a></li>
                    <li class="navbar-menu-item"><a href="{host}/places/bars-and-restaurants/" class="color4">#Каталог организаций</a></li>
                    <li class="navbar-menu-item"><a href="{host}/residents.html" class="color5">#Резиденты</a></li>
                    <li class="navbar-menu-item"><a href="{host}/albums/" class="color5">#Фотоотчеты</a></li>
                    <li class="btn-tv"><a href="{host}/fridayz-tv/">#FRIDAYZ TV</a></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <div class="sidenav-overlay"></div>
    <div id="sidenav" class="sidenav">
        <div class="sidenav-menu-header">
            <a href="/"><div class="sidenav-logo"></div></a>
            <div class="sidenav-header-close"></div>
        </div>
        <a class="sidenav-btn-tv" href="{host}/fridayz-tv/">#FRIDAYZ TV</a>
        <ul class="sidenav-menu">
            <li><a href="{host}/afisha/">#Афиша</a></li>
            <li><a href="{host}/gorod/">#Город</a></li>
            <li><a href="{host}/news/">#Статьи</a></li>
            <li><a href="{host}/places/bars-and-restaurants/">#Каталог организаций</a></li>
            <li><a href="{host}/residents.html">#Резиденты</a></li>
            <li><a href="{host}/albums/">#Фотоотчеты</a></li>
        </ul>
    </div>

    <!-- Wrapper -->
    <div class="wrapper">
    <!--
    <div class="scroll-bar">
        <a class="icon-bar icon-bar-mail" href="mailto:info@fridayz.ru"></a>
        <a class="icon-bar icon-bar-up" href="#"></a>
    </div>
    -->
        <!-- Page Content -->
        <component id="Article" class="Article" path="article/" for="page" for-data-no="index"></component>

        <condition for="page" for-data="index">
            <component id="bg-banner-main-1" class="Banner" path="banner/"></component>
            <!--
            <div id="bg-banner-main-1" class="banner"></div>
            -->
            <component class="Gallery" path="gallery/gallery/" template="feed_afisha.tpl.php" data-id="6" list-limit="7"></component>
            <component id="bg-banner-main-2" class="Banner" path="banner/"></component>
            <!--
            <div id="bg-banner-main-2" class="banner"></div>
            -->
            <component class="FeedNews" path="feed/news/" template="feed_city_news.tpl.php" category-id="3" limit="6" title="#Город"></component>
            
            <component id="bg-banner-main-3" class="Banner" path="banner/"></component>
            <!--
            <div id="bg-banner-main-3" class="banner"></div>
            -->
            <component class="FeedNews" path="feed/news/" template="feed_article_news.tpl.php" category-id="2" limit="6" title="#Статьи"></component>
            <component id="bg-banner-main-4" class="Banner" path="banner/"></component>
            <!--
            <div id="bg-banner-main-4" class="banner"></div>
            -->
            <component class="FeedAlbums" path="feed/albums/" template="feed_albums.tpl.php" category-id="5" limit="6"></component>
            <component id="bg-banner-main-5" class="Banner" path="banner/"></component>
            <!--
            <div id="bg-banner-main-5" class="banner"></div>
            -->
            <component class="VideoFeed" path="video/feed/" template="video_feed.tpl.php" category-id="18" limit="3"></component>
            <component id="bg-banner-main-6" class="Banner" path="banner/"></component>
            <!--
            <div id="bg-banner-main-6" class="banner"></div>
            -->
            <component class="PartnersMap" path="partners/map/"></component>
            
            <component id="bg-banner-main-7" class="Banner" path="banner/"></component>
            <!--
             <div id="bg-banner-main-7" class="banner"></div>
             -->
        </condition>
        <!-- /.content -->

        <footer>
            <div class="container-fluid">
                <div class="row">
                <div class="col-md-8">
                    <div class="copyright">© 2017 Fridayz.ru - первый медийный онлайн журнал.<br />
                    Все материалы сайта защищены.<br />
                    При использовании любых материалов ссылка с указанием адреса обязательна.
                    </div>
                </div>
                <div class="col-md-4 col-logo"><a class="logo" href="{host}"><img src="{theme}/images/logo-bottom.png" /></a></div>
                </div>
            </div>
        </footer>
    </div>
    <!-- /.wrapper -->

    <!-- Video modal -->
    <div id="modal-video" class="modal modal-video fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body"><iframe></iframe><div class="modal-body-footer"></div></div>
                <div class="modal-footer" style="text-align: center;">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal -->

    <!-- Video modal -->
    <div id="modal-vk" class="modal modal-vk fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-vk">
            <div class="modal-content">
                <div class="modal-header" style="border-bottom:0">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Мы ВКонтакте</h4>
                </div>
                <div class="modal-body" id="modal-body-vk" style="padding:0;"></div>
                <div class="modal-footer" style="text-align:center;border-top:0;">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal -->
    <div class="scroll-to"><a href="#"></a></div>

    <component class="Script"></component>
    <!-- Bootstrap Core JavaScript -->
    <script type="text/javascript" src="{gear}/js/bootstrap.min.js?v3.3.4"></script>

    <script type="text/javascript" src="{gear}/js/jquery.blockUI.js"></script>
    <!-- Plugins JS -->
    <script type="text/javascript" src="{theme}/plugins/simplyscroll/js/jquery.simplyscroll.js"></script>
    <script type="text/javascript" src="{theme}/plugins/barrating/jquery.barrating.min.js"></script>
    <script type="text/javascript" src="{theme}/plugins/slick/slick.min.js"></script>
    <script type="text/javascript" src="{theme}/js/jquery.mojo.js"></script>
    <script type="text/javascript" src="{theme}/js/script.js?v1.0"></script>
    <!-- Gear JS -->
    <script type="text/javascript" src="{gear}/js/gear.js?v1.0"></script>
    <component class="ScriptDeclaration"></component>