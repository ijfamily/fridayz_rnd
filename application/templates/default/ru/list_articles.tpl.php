<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Список статей"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('list-articles'))) return;

// пагинация
$pagination = '';
$paginationTop = '';
$paginationBottom = '';
if ($tpl['pagination']) {
    $pagination .= '<ul class="pagination pagination-sm">';
    foreach($tpl['pagination'] as $index => $item) {
        switch ($item['type']) {
            case 'first': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Первая страница"><span aria-hidden="true">&laquo;</span></a></li>'; break;
            case 'prev': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Предыдущая страница"><span aria-hidden="true">&lsaquo;</span></a></li>'; break;
            case 'more': $pagination .= '<li class="disabled"><a href="#">▪▪▪</a></li>'; break;
            case 'page': $pagination .= '<li><a href="' . $item['url'] . '">' . $item['page'] . '</a></li>'; break;
            case 'active': $pagination .= '<li class="active"><a href="#">' . $item['page'] . '</a></li>'; break;
            case 'next': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Следущая страница"><span aria-hidden="true">&rsaquo;</span></a></li>'; break;
            case 'last': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Последняя страница"><span aria-hidden="true">&raquo;</span></a></li>'; break;
        }
    }
    $pagination .= '</ul>';
    if ($tpl['pagination-pos'] == 'top' || $tpl['pagination-pos'] == 'both')
        $paginationTop = $pagination;
    if ($tpl['pagination-pos'] == 'bottom' || $tpl['pagination-pos'] == 'both')
        $paginationBottom = $pagination;
}
// режим конструктора
echo $tpl['design-tag-open'];

?>
<!-- list articles -->
<div class="container article">
<?php
if ($tpl['category-name'])
    echo '<h2>', $tpl['category-name'], '</h2>';
if (!($count = $tpl['count'])) {
?>
    <div class="col-md-8"><div class="gear-page-public">Мы приносим свои извинения, но по вашему запросу нет записей на странице!</div></div>
    <div class="col-md-4"><div class="gear-calendar"></div></div>
</div>
<?php
}

if ($count) {
// пагинация
echo $paginationTop;
?>
<div class="row">
<div class="col-md-8">
    <div class="row-articles">
<?php
for ($i = 0; $i < $count; $i++) :
    $t = $tpl['items'][$i];
?>
    <div class="row-articles-i<?php echo $t['img'] ? ' img': '';?>">
        <?php if (!empty($t['img'])) { ?>
        <a class="img" href="{host}<?php echo $t['url'];?>"><img src="<?php echo $t['img'];?>"></a>
        <?php } ?>
        <div class="text">
            <h3><a href="{host}<?php echo $t['url'];?>"><?php echo $t['title'];?></a></h3>
            <div class="date"><span><?php echo GDate::format('j', $t['date']);?></span> <?php echo GDate::format('F', $t['date']);?></div>
            <p><?php echo $t['text'];?><br /></p>
            <div align="right"><a class="link" href="{host}<?php echo $t['url'];?>">подробнее</a></div>
<?php
    // режим конструктора
    if ($tpl['design-tag-control']) {
        echo str_replace('%s', $t['id'], $tpl['design-tag-control']);
    }
?>
        </div>
    </div>
<?php endfor; ?>
    </div>
</div>
<div class="col-md-4"><div class="gear-calendar"></div></div>
</div>
<?php
// пагинация
echo $paginationBottom;
}
?>
</div>
<!-- /list articles -->
<?php
// режим конструктора
echo $tpl['design-tag-close'];

// включить календарь
if ($tpl['use-calendar']) :
?>
<script type="text/javascript">
    $(document).ready(function(){
        // list calendar
        gear.calendarBs({
            selector: '.gear-calendar',
            language: '<?php echo $tpl['language'];?>',
            category: <?php echo $tpl['category-id'];?>,
            highlight: <?php echo $tpl['calendar-highlight'] ? 'true' : 'false';?>,
            dateOn: <?php echo $tpl['date-on'] ? '{year:' . $tpl['date-on']['year'] . ',month:' . ($tpl['date-on']['month'] - 1) . ',day:' . $tpl['date-on']['day'] . '}' : 'false'; ?>
            
        });
    });
</script>
<?php endif; ?>