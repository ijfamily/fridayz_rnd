<?php
/**
 * Gear CMS
 *
 * Шаблон компонента "Заведение партнёра"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('partner'))) return;

if (empty($tpl['info'])) return;
$inf = $tpl['info'];
$ftr = $tpl['features'];
?>
<article id="place-<?php echo $inf['id'];?>" data-id="<?php echo $inf['id'];?>" class="place">
    <div class="place-header"<?php echo $tpl['category-image'] ? ' style="background-image: url(' . $tpl['category-image'] . ')"' : '';?>>
        <ul class="place-net">
            <?php if ($inf['site']) : ?>
            <li><a class="place-net-site" href="http://<?php echo $inf['site'];?>" target="_blank" title="Сайт"></a></li>
            <?php else : ?>
            <li><span class="place-net-site"></span></li>
            <?php endif; ?>
            <?php if (!empty($ftr['vk'])) : ?>
            <li><a class="place-net-vk" href="<?php echo $ftr['vk'];?>" target="_blank" title="ВКонтакте"></a></li>
            <?php else : ?>
            <li><span class="place-net-vk"></span></li>
            <?php endif; ?>
            <?php if (!empty($ftr['ok'])) : ?>
            <li><a class="place-net-ok" href="<?php echo $ftr['ok'];?>" target="_blank" title="Одноклассники"></a></li>
            <?php else : ?>
            <li><span class="place-net-ok"></span></li>
            <?php endif; ?>
            <?php if (!empty($ftr['instagram'])) : ?>
            <li><a class="place-net-ins" href="<?php echo $ftr['instagram'];?>" target="_blank" title="Instagram"></a></li>
            <?php else : ?>
            <li><span class="place-net-ins"></span></li>
            <?php endif; ?>
        </ul>
        <div class="place-logo">
            <div class="place-frame-t"></div>
            <div class="place-title"><h2><?php echo $inf['name'];?></h2></div>
            <div class="place-rating">
                <?php for ($i = 0; $i < $inf['rating-state']; $i++) : ?>
                <span class="place-rating-star"></span>
                <?php endfor; ?>
            </div>
            <?php if ($inf['schedule']) : ?>
            <div class="place-schedule"><?php echo $inf['schedule'];?></div>
            <?php endif; ?>
            <div class="place-frame-b"></div>
        </div>
        <div class="place-header-wrapper">
            <div class="place-info">
                <?php if ($inf['kitchen']) : ?>
                <div class="place-kitchen">Кухня: <?php echo $inf['kitchen'];?></div>
                <?php endif; ?>
                <div class="place-address"><?php echo $inf['address'];?></div>
                <?php if ($inf['phone']) : ?>
                <div class="place-phone"><?php echo str_replace(',', ', ', $inf['phone']);?></div>
                <?php endif; ?>
            </div>
            <ul class="place-features">
                <?php if (!empty($ftr['karaoke'])) : ?>
                <li><span class="place-feature-karaoke"></span></li>
                 <?php endif; ?>
                 <?php if (!empty($ftr['hookah'])) : ?>
                <li><span class="place-feature-hookah"></span></li>
                <?php endif; ?>
                <?php if ($inf['delivery']) : ?>
                <li><span class="place-feature-delivery"></span></li>
                <?php endif; ?>
                <?php if (!empty($ftr['wifi'])) : ?>
                <li><span class="place-feature-wifi"></span></li>
                <?php endif; ?>
            </ul>
        </div>
<?php
        if ($inf['text'] && $inf['extended'])
            echo '<div class="place-text">', $inf['text'], '</div>';
?>
<?php if ($inf['images']) : ?>
        <ul class="place-photos" >
        <?php foreach ($inf['images'] as $t) : 
            echo '<li class="photo-i" data-src="{chost}/data/images/', $inf['folder'], '/', $t['i']['file'], '" data-sub-html="">',
                    '<a href="#"><img class="img-responsive" src="{chost}/data/images/', $inf['folder'], '/', $t['t']['file'], '" alt="', $t['t']['title'], '" title="', $t['t']['title'], '"></a>', 
                 '</li>';
        endforeach;?>
        </ul>
<?php endif; ?>
    </div>
<?php
// режим конструктора
echo $tpl['design-tag-open'];

// режим конструктора
echo $tpl['design-tag-close'];
?>
</article>

<?php if ($inf['coord']) : ?>
    <script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>
    <script type="text/javascript">
        ymaps.ready(function init(){
            var mapPlace = new ymaps.Map('map-place', { center: [<?=$inf['coord-other'] ? '48.5689472,39.3154929' : $inf['coord'];?>], behaviors: ['default', 'scrollZoom'], zoom: <?=$inf['coord-other'] ? 11: 13;?> }),
                placemark = new ymaps.Placemark([<?=$inf['coord'];?>], { balloonContent: '<img width="200px" src="{host}/<?=$inf['image'];?>" />', iconContent: "<?=$inf['name'];?>" }, { preset: "twirl#blueStretchyIcon", balloonCloseButton: false, hideIconOnBalloonOpen: false });
            mapPlace.geoObjects.add(placemark);
<?php
if ($inf['coord-other']) :
    foreach($inf['coord-other'] as $index => $item) : ?>
            placemark<?=$index + 1;?> = new ymaps.Placemark([<?=$item;?>], { balloonContent: '<img width="200px" src="{host}/<?=$inf['image'];?>" />', iconContent: "<?=$inf['name'];?>" }, { preset: "twirl#blueStretchyIcon", balloonCloseButton: false, hideIconOnBalloonOpen: false });
            mapPlace.geoObjects.add(placemark<?=$index + 1;?>);
<?php endforeach; endif; ?>
       });
    </script>
    <div id="map-place" style="width:100%; height:300px" class="s-map-place"></div>
<?php endif; ?>

<?php if ($inf['images']) : ?>
    <!--LightGallery -->
    <link href="{theme}/plugins/lightgallery/css/lightgallery.css" rel="stylesheet" />
    <script type="text/javascript" src="{theme}/plugins/lightgallery/js/lightgallery.js"></script>
    <!--LightGallery widgets -->
    <script type="text/javascript" src="{theme}/plugins/lightgallery/js/lg-hash.js"></script>
    <script type="text/javascript" src="{theme}/plugins/lightgallery/js/lg-social.js"></script>
<?php endif; ?>