<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Галерея Ad"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('gallery-ad'))) return;

// режим конструктора
echo $tpl['design-tag-open'];

?>
<!-- gallery ad -->
<div id="<?php echo $tpl['attr']['id'];?>" class="ad-gallery">
    <div class="ad-image-wrapper"></div>
    <div class="ad-controls"></div>
    <div class="ad-nav">
        <div class="ad-thumbs">
            <ul class="ad-thumb-list">
<?php
$count = $tpl['count'];
for ($i = 0; $i < $count; $i++) :
    $t = $tpl['items'][$i];
    echo '<li><a href="{chost}/', $tpl['dir'], $t['folder'], '/', $t['i']['file'], '">', '<img src="{chost}/', $tpl['dir'], $t['folder'], '/', $t['t']['file'], '" title="', $t['t']['title'], '" alt="', $t['alt'], '">';
    // режим конструктора
    if ($tpl['design-tag-control'])
        echo '<div align="center">', str_replace('%s', $t['id'], $tpl['design-tag-control']), '</div>';
    echo '</a></li>', _n_t;
endfor;
?>
            </ul>
        </div>
    </div>
</div>
<!-- /gallery ad -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>