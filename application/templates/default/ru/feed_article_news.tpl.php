<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Лента новостей"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('feed-news'))) return;

if (!($count = $tpl['count'])) return;

// режим конструктора
echo $tpl['design-tag-open'];

$first = $tpl['items'][0];
?>
<!-- feed news -->
<section class="s-news feed feed-short">
    <h2 class="title1">#Статьи</h2>
    <div class="s-news-body insta">
        <a class="s-news-feature" href="{host}<?=$first['url'];?>">
            <div class="s-news-feature-label">#Статьи</div>
            <img src="{host}<?=empty($first['img']) ? '/data/images/place-none.jpg' : $first['img'];?>" />
            <div class="s-news-feature-text">
                <div class="title"><?=$first['title'];?></div>
                <div class="desc"><?=$first['text'];?></div>
            </div>
        </a>
        <div class="s-news-l">
            <ul>
                <?php for($i = 1; $i < $tpl['count']; $i++) : $t = $tpl['items'][$i];?>
                <li><a href="{chost}<?=$t['url'];?>">
                    <span class="s-news-l-title"><?=$t['title'];?> </span>
                    <time class="s-news-l-time">
                        <div class="s-news-l-time-days"> <?=GDate::format('d/m', $t['date']);?></div>
                    </time>
                </a></li>
                <?php endfor; ?>
            </ul>
        </div>
        <div class="s-news-soc">
        <iframe src="//widget.stapico.ru/?q=fridayz_ru&s=60&w=3&h=5&b=8&p=4&header=yes&profile=no&effect=0" allowtransparency="true" frameborder="0" scrolling="no" style="border:none;overflow:hidden;width:300px;height:380px;" ></iframe> <!-- stapico - stapico.ru -->
        </div>
    </div>
</section>
<!-- /feed news -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>