<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Изображения фотоальбомов"
 */

defined('_TPL') or die('Restricted access');


if (!($tpl = &Gear::tpl('album-images'))) return;

// пагинация
$pagination = '';
$paginationTop = '';
$paginationBottom = '';
if ($tpl['pagination']) {
    $pagination .= '<ul class="pagination pagination-sm">';
    foreach($tpl['pagination'] as $index => $item) {
        switch ($item['type']) {
            case 'first': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Первая страница"><span aria-hidden="true">&laquo;</span></a></li>'; break;
            case 'prev': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Предыдущая страница"><span aria-hidden="true">&lsaquo;</span></a></li>'; break;
            case 'more': $pagination .= '<li class="disabled"><a href="#">▪▪▪</a></li>'; break;
            case 'page': $pagination .= '<li><a href="' . $item['url'] . '">' . $item['page'] . '</a></li>'; break;
            case 'active': $pagination .= '<li class="active"><a href="#">' . $item['page'] . '</a></li>'; break;
            case 'next': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Следущая страница"><span aria-hidden="true">&rsaquo;</span></a></li>'; break;
            case 'last': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Последняя страница"><span aria-hidden="true">&raquo;</span></a></li>'; break;
        }
    }
    $pagination .= '</ul>';
    if ($tpl['pagination-pos'] == 'top' || $tpl['pagination-pos'] == 'both')
        $paginationTop = $pagination;
    if ($tpl['pagination-pos'] == 'bottom' || $tpl['pagination-pos'] == 'both')
        $paginationBottom = $pagination;
}

$count = sizeof($tpl['items']);

// режим конструктора
echo $tpl['design-tag-open'];
?>
<!-- photos -->
<?php
// пагинация
echo $paginationTop;
?>
<div class="ct-photos">
<?php
echo '<h2 class="title tc">', GDate::format('d.m.Y', $tpl['album-date']), ' / ', $tpl['album-title'], ' / #Fridayz.ru', '</h2>';
?>

    <div class="row">
        <ul id="album-photos" >
<?php
for ($i = 0; $i < $count; $i++) :
    $t = $tpl['items'][$i];
    echo '<li class="photo-i" data-src="{chost}/data/albums/', $t['folder'], '/', $t['i']['file'], '" data-sub-html="">';
    // режим конструктора
    if ($tpl['design-tag-control'])
        echo str_replace('%s', $t['id'], $tpl['design-tag-control']);
    echo '<a href="#"><img class="img-responsive" src="{chost}/data/albums/', $t['folder'], '/', $t['t']['file'], '" alt="', $t['alt'], '" title="', $t['title'], '"></a>';
    echo '</li>';
endfor;
?>
        </ul>
    </div>
</div>
<?php
// пагинация
echo $paginationBottom;
?>
<!-- /photos -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>
<!--LightGallery -->
<link href="{theme}/plugins/lightgallery/css/lightgallery.css" rel="stylesheet" />
<script type="text/javascript" src="{theme}/plugins/lightgallery/js/lightgallery.js"></script>
<!--LightGallery widgets -->
<script type="text/javascript" src="{theme}/plugins/lightgallery/js/lg-hash.js"></script>
<script type="text/javascript" src="{theme}/plugins/lightgallery/js/lg-social.js"></script>