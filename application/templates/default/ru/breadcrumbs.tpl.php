<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Хлебные крошки"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('breadcrumbs'))) return;

// режим конструктора
echo $tpl['design-tag-open'];

?>
<!-- breadcrumbs -->
<div class="breadcrumbs"><?php
$count = $tpl['count'];
for($i = 0; $i < $count; $i++) :
    $t = $tpl['items'][$i];
    if ($i == 0)
        echo '<a href="', $tpl['host'], '" title="Главная страница">Главная </a> &raquo; ';
    else
    if ($i == $count - 1)
        echo '<span>', $t['title'], '</span>';
    else {
        if ($t['url']) {
            // режим конструктора
            if ($tpl['design-tag-control'])
                echo '<span>', str_replace('%s', $t['id'], $tpl['design-tag-control']), '</span>';
            echo '<a title="', $t['title'], '" href="', $tpl['host'], $t['url'], '">', $t['title'], '</a> &raquo; ';
        } else
            echo '<span>', $t['title'], '</span>';
    }
endfor;
?>
</div>
<!-- /breadcrumbs -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>