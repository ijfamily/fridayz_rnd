<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Список статей" (новости)
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('list-tag'))) return;

Gear::doc(array('title' => '#' . $tpl['tag']));

// пагинация
$pagination = '';
$paginationTop = '';
$paginationBottom = '';
if ($tpl['pagination']) {
    $pagination .= '<ul class="pagination pagination-sm">';
    foreach($tpl['pagination'] as $index => $item) {
        switch ($item['type']) {
            case 'first': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Первая страница"><span aria-hidden="true">&laquo;</span></a></li>'; break;
            case 'prev': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Предыдущая страница"><span aria-hidden="true">&lsaquo;</span></a></li>'; break;
            case 'more': $pagination .= '<li class="disabled"><a href="#">▪▪▪</a></li>'; break;
            case 'page': $pagination .= '<li><a href="' . $item['url'] . '">' . $item['page'] . '</a></li>'; break;
            case 'active': $pagination .= '<li class="active"><a href="#">' . $item['page'] . '</a></li>'; break;
            case 'next': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Следущая страница"><span aria-hidden="true">&rsaquo;</span></a></li>'; break;
            case 'last': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Последняя страница"><span aria-hidden="true">&raquo;</span></a></li>'; break;
        }
    }
    $pagination .= '</ul>';
    if ($tpl['pagination-pos'] == 'top' || $tpl['pagination-pos'] == 'both')
        $paginationTop = $pagination;
    if ($tpl['pagination-pos'] == 'bottom' || $tpl['pagination-pos'] == 'both')
        $paginationBottom = $pagination;
}
// режим конструктора
echo $tpl['design-tag-open'];
?>

<!-- list tag -->
<div class="s-news list">
<?php
echo '<h2 class="title tc">#', $tpl['tag'], '</h2>';
Gear::component('CloudeTags', 'cloude/', array('select' => $tpl['tag']));
// пагинация
echo $paginationTop;
?>
    <div class="s-news-body">
<?php
if (!($count = $tpl['count']))
    echo  '<div class="gear-page-public">Мы приносим свои извинения, но по вашему запросу нет записей на странице!</div>';

for ($i = 0; $i < $count; $i++) :
    $t = $tpl['items'][$i];
?>
    <div class="s-news-i">
        <a href="{chost}<?php echo $t['url'];?>"><img src="<?php echo $tpl['host'], (empty($t['img']) ? 'no_image.jpg' : $t['img']);?>" /></a>
        <div class="info">
            <a href="{chost}<?php echo $t['url'];?>">
            <div class="date">
<?php
    // режим конструктора
    if ($tpl['design-tag-control'])
        echo str_replace('%s', $t['id'], $tpl['design-tag-control']);
?>
            <?php echo GDate::format('l, d F', $t['date']);?>
            </div>
            <div class="title"><?php echo $t['title'];?></div>
            <div class="desc"><?php echo $t['text'];?></div>
            </a>
    </div>
<?php if ($t['tags']) : ?>
            <div class="tags"><?php echo $t['tags'];?></div>
<?php endif; ?>
        </div>
<?php endfor; ?>
    </div>
<?php
// пагинация
echo $paginationBottom;
?>
</div>
<!-- /list tag -->
<?php
// режим конструктора
echo $tpl['design-tag-close'];
?>