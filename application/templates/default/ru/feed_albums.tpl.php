<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Лента фотоальбомов"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('feed-albums'))) return;

if (!($count = $tpl['count'])) return;

// режим конструктора
echo $tpl['design-tag-open'];
?>
<!-- feed albums -->
<section class="s-albums feed">
    <div class="s-albums-body">
    <h2 class="title1" style="padding-left:10px;">#Фотоотчеты</h2>
        <div class="row">
<?php foreach ($tpl['items'] as $t) : $date = GDate::format('d F / l', $t['date']); ?>
        <div class="s-albums-i">
            <a href="{chost}/albums/<?=$t['id'];?>">
                <img src="{chost}/data/albums/<?=$t['folder'], '/', $t['cover'];?>" title="Фотоотчёт / <?=$date, ' / ', $t['name'];?>" alt="Фотоотчёт / <?=$date, ' / ', $t['name'];?>" />
                <span class="s-albums-i-overlay">
<?php
    // режим конструктора
    if ($tpl['design-tag-control'])
        echo str_replace('%s', $t['id'], $tpl['design-tag-control']);
?>
                    <span class="s-albums-i-text">
                        <div class="s-albums-i-date"><?=$date;?></div>
                        <div class="s-albums-i-title"><?=$t['name'];?></div>
                    </span>
                </span>
            </a>
        </div>
<?php endforeach; ?>
        </div>
        <div class="btn-controls"><a class="btn btn-primary" href="{chost}/albums/">Показать еще</a></div>
    </div>

</section>
<!-- /feed albums -->

<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>