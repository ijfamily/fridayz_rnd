<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Облако тегов"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('cloude-tags'))) return;

if (!($count = $tpl['count'])) return;

// режим конструктора
echo $tpl['design-tag-open'];

?>
<!-- cloude tags -->
<div class="s-tags">
<div>
<?php
$limit = 7;
$j = 0;
for ($i = 0; $i < $count; $i++) :
    $j++;
    $t = $tpl['items'][$i];
    if ($j > $limit) {
        $j = 0;
        echo '</div><div>';
    }
?>
        <a href="{chost}<?php echo $t['url'];?>" title="<?php echo $t['name'];?>" <?php echo $t['active'] ? 'class="active"' : '';?>><?php echo $t['name'];?></a>
<?php
    // режим конструктора
    if ($tpl['design-tag-control'])
        echo str_replace('%s', $t['id'], $tpl['design-tag-control']);
?>
<?php endfor; ?>
</div>
</div>
<!-- /cloude tags -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>