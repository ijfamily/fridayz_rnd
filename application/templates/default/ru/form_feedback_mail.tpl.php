<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Форма обратной связи (письмо)"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('data-mail'))) return;

?>
<html>
<body>
    <div><strong>Имя: </strong><?php echo $tpl['feedback_name'];?></div>
    <div><strong>E-mail: </strong><?php echo $tpl['feedback_email'];?></div>
    <div><strong>Телефон: </strong><?php echo $tpl['feedback_phone'];?></div>
    <div><strong>Текс: </strong></div>
    <div><?php echo $tpl['feedback_text'];?></div>
</body>
</html>