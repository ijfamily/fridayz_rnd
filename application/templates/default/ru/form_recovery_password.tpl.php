<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Форма восстановления пароля"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('form-recoverypassword'))) return;

Gear::doc(array('title' => 'Восстановление пароля'));

//Gear::code($tpl);
?>
<section class="s-form">
    <h2 class="title tc">Восстановление пароля</h2>
<?php if (!$tpl['hash']) : ?>
    <div class="alert alert-danger"><strong>Ошибка!</strong> Неправильно указан хеш-код или Вы перешли по неверной ссылке.</div>
</section>
<?php return; endif; ?>

<?php if ($tpl['is-submit'] && $tpl['is-success']) : ?>
    <div style="text-align: center;">
        На Ваш e-mail адрес успешно отправлено письмо c данными для входа на сайт.
    </div>
<?php return; endif; ?>

<!-- form recovery password -->
<div class="s-form-wrap">
    <div style="text-align: center;">Введите код восстановления, который был отправлен на ваш e-mail.<br /><br /></div>
    <form  id="<?php echo $tpl['attr']['id'];?>" class="form-horizontal form-remindpassword" role="form" method="post" action="">
<?php
    if ($tpl['message']) {
        if ($tpl['is-error'])
            echo '<div class="alert alert-danger"><strong>Ошибка!</strong> ', $tpl['message'], '</div>';
    }
?>
        <input type="hidden" name="target" value="<?php echo $tpl['target'];?>"/>
        <input type="hidden" name="token" value="<?php echo $tpl['fields']['token'];?>"/>
        <input type="hidden" name="hash" value="<?php echo $tpl['hash'];?>"/>
        <div class="form-group">
            <label class="control-label col-sm-4" for="code">Код:</label>
            <div class="col-sm-8"><input type="text" class="form-control" id="code" name="code" maxlength="50" placeholder="Введите код" required /></div>
        </div>
<?php if ($tpl['attr']['check-captcha']) : ?>
        <div class="form-group">
            <div class="col-sm-4"><img src="<?php echo $tpl['url-captcha'];?>" /></div>
            <div class="col-sm-8"><input type="text" class="form-control" name="code" style="width:170px;" required />введите код на картинке</div>
        </div>
<?php endif; ?>
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-8" style="text-align: left;"><button type="submit" class="btn btn-primary">Восстановить аккаунт</button></div>
        </div>
    </form>
<?php
// использовать проверку
if ($tpl['attr']['use-scripts']) :
?>
<script type="text/javascript">
    (function(w, n, t, id) {
        w[n] = w[n] || {}; w[n][id] = { id: id, ajax: false, data: {}, valid: { name: t, css: true } };
    })(window, "gearForms", "formValidation", "<?php echo $tpl['attr']['id'];?>");
</script>
<?php endif; ?>
<!-- /form recovery password -->
</div>
</section>