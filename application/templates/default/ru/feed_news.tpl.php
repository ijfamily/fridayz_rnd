<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Лента новостей"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('feed-news'))) return;

if (!($count = $tpl['count'])) return;

// режим конструктора
echo $tpl['design-tag-open'];

?>
<!-- feed news -->
<section class="s-news feed">
    <?php if ($tpl['title']) : ?>
    <h2 class="title1"><?php echo $tpl['title']; ?></h2>
    <?php endif; ?>
    <div class="s-news-body">
<?php
for ($i = 0; $i < $count; $i++) :
    $t = $tpl['items'][$i];
?>
    <div class="s-news-i<?php echo $i == 0 ? ' first' : '';?>">
        <a href="{chost}<?php echo $t['url'];?>"><img src="<?php echo $tpl['host'], (empty($t['img']) ? 'no_image.jpg' : $t['img']);?>" /></a>
        <div class="info">
            <a href="{chost}<?php echo $t['url'];?>">
<?php
    // режим конструктора
    if ($tpl['design-tag-control'])
        echo str_replace('%s', $t['id'], $tpl['design-tag-control']);
?>
            <?php if ($t['date']) : ?>
            <div class="date">
            <?php echo GDate::format('l, d F', $t['date']);?>
            </div>
            <?php endif; ?>
            <div class="title"><?php echo $t['title'];?></div>
            <div class="desc"><?php echo $t['text'];?></div>
            </a>
        </div>
<?php if ($t['tags']) : ?>
            <div class="tags"><?php echo $t['tags'];?></div>
<?php endif; ?>
    </div>
<?php endfor; ?>
    </div>
</section>
<!-- /feed news -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>