<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Меню сайта в подвале"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('menu'))) return;

// режим конструктора
echo $tpl['design-tag-open'];

?>
<!-- footer menu -->
<nav>
    <ul>
<?php
foreach ($tpl['items'] as $id => $item) :
    echo _2t, '<li><a href="{scheme}', $item['url'], '" title="', $item['name'], '">', $item['name'];

    // режим конструктора
    if ($tpl['design-tag-control'])
        echo str_replace('%s', $item['id'], $tpl['design-tag-control']);

    echo '</a></li>', _n;
endforeach;
?>
    </ul>
</nav>
<!-- /footer menu -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>