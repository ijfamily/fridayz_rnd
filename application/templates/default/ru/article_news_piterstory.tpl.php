<?php
/**
 * Gear CMS
 *
 * Шаблон компонента "Статья" (новости)
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('article'))) return;
?>
<div class="row row-article">
    <div class="col-md-9 col-article">
        <article id="article-<?=$tpl['id'];?>" data-id="<?=$tpl['id'];?>" class="article">
        <?php
        // режим конструктора
        echo $tpl['design-tag-open'];

        if (!empty($tpl['header']) && $tpl['show-header']) :
?>
        <div class="row city-header">
            <div class="col-md-6 col-sm-6">
        <?php if ($tpl['image']) : ?>
            <img src="<?=$tpl['image'];?>" />
        <?php endif; ?>
            </div>
            <div class="col-md-6 col-sm-6">
                <h2 class="city-header-title"><?=$tpl['header'];?></h2>
            </div>
        </div>
<?php
        endif;

        echo $tpl['html'];
        
        // режим конструктора
        echo $tpl['design-tag-close'];
        ?>
        </article>
    </div>
    <div class="col-md-3 col-sidebar">
        <div class="sidebar-right">
<?php
Gear::component('FeedNews', 'feed/news/', array('limit' => 5, 'category-id' => 2, 'data-random' => true, 'template' => 'sidebar_news.tpl.php'));
?>
        </div>
    </div>
</div>