<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Языки"
 */

defined('_TPL') or die('Restricted access');

if (!($d = &Gear::tpl('language'))) return;

// режим конструктора
echo $tpl['design-tag-open'];

?>
<!-- language -->
<?php
$count = sizeof($d['items']);
foreach ($d['items'] as $key => $value) :
    if ($d['def'] == $key) {
        if ($d['url'] == '/')
            $href = '/';
        else
            $href = $d['url'];
    } else {
        if ($d['url'] != '/')
            $href = '/' . $key . $d['url'];
        else
            $href = '/' . $key;
    }
    if (isset(CUrl::$components['query']))
        $href .= '?' . CUrl::$components['query'];
    print '<a class="flag flag-' . $value['alias'] . '" title="' . $value['title'] . '" href="' . $href . '"></a>';
endforeach;
?>
<!-- /language -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>