<?php
/**
 * Gear CMS
 *
 * Шаблон компонента "Статья"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('article'))) return;

echo $tpl['html'];
?>
