<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Фотогалерея"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('gallery'))) return;

// пагинация
$pagination = '';
$paginationTop = '';
$paginationBottom = '';
if ($tpl['pagination']) {
    $pagination .= '<div style="text-align:left"><ul class="pagination pagination-sm">';
    foreach($tpl['pagination'] as $index => $item) {
        switch ($item['type']) {
            case 'first': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Первая страница"><span aria-hidden="true">&laquo;</span></a></li>'; break;
            case 'prev': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Предыдущая страница"><span aria-hidden="true">&lsaquo;</span></a></li>'; break;
            case 'more': $pagination .= '<li class="disabled"><a href="#">▪▪▪</a></li>'; break;
            case 'page': $pagination .= '<li><a href="' . $item['url'] . '">' . $item['page'] . '</a></li>'; break;
            case 'active': $pagination .= '<li class="active"><a href="#">' . $item['page'] . '</a></li>'; break;
            case 'next': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Следущая страница"><span aria-hidden="true">&rsaquo;</span></a></li>'; break;
            case 'last': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Последняя страница"><span aria-hidden="true">&raquo;</span></a></li>'; break;
        }
    }
    $pagination .= '</ul></div>';
    if ($tpl['pagination-pos'] == 'top' || $tpl['pagination-pos'] == 'both')
        $paginationTop = $pagination;
    if ($tpl['pagination-pos'] == 'bottom' || $tpl['pagination-pos'] == 'both')
        $paginationBottom = $pagination;
}
// режим конструктора
echo $tpl['design-tag-open'];

?>
<div id="bg-banner-afisha" class="banner"></div>
<!-- gallery afisha -->
<div class="s-afisha list">
    <div class="s-afisha-body">
<?php
foreach ($tpl['items'] as $index => $t) :
?>
    <a class="s-afisha-i pirobox_gall" href="{chost}/data/galleries/<?php echo $t['folder'], '/', $t['i']['file'];?>" title="" rel="gallery">
        <div class="img"><img src="{chost}/data/galleries/<?php echo $t['folder'], '/', $t['t']['file'];?>" /></div>
    </a>
<?php endforeach; ?>
    </div>
<?php
// пагинация
echo $paginationBottom;
?>
</div>
<link rel="stylesheet" type="text/css" href="/themes/gear/css/pirobox/style2/style.css" />
<script type="text/javascript" src="/themes/gear/js/jquery-ui.custom.min.js?v=1.8.2"></script>
<script type="text/javascript" src="/themes/gear/js/jquery.pirobox.ext.min.js?v=1.0"></script>
<!-- /gallery afisha -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>