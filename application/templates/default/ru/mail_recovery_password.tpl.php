<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Форма восстановления пароля (письмо)"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('data-mail'))) return;

?>
<html>
<body>
    <p>Ваш аккаунт на сайте <b><?php echo $tpl['domain'];?></b> восстановлен, используйте следующие данные для входа на сайт:
    <p style="padding-left: 20px;">E-mail: <?php echo $tpl['e-mail'];?></p>
    <p style="padding-left: 20px;">Пароль: <?php echo $tpl['password'];?></p>
    <p>После авторизации измените пароль.</p>
    <br />
    <p>---</p>
    <br />
    <p>Вы получили это сообщение, потому что Ваш email-адрес был указан для восcтановления аккаунта на сайте <b><?php echo $tpl['domain'];?></b>.</p>
    <p>Если данное письмо попало к Вам по ошибке, пожалуйста, проигнорируйте его и примите наши извинения.</p><br />
    <p>С наилучшими пожеланиями,<br />
    Администрация сайта <?php echo $tpl['domain'];?></p>
</body>
</html>