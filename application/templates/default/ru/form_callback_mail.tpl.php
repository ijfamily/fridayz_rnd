<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Форма обратной вызова (письмо)"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('data-mail'))) return;

?>
<html>
<body>
    <div><strong>Имя: </strong><?php echo $tpl['feedback_name'];?></div>
    <div><strong>Телефон: </strong><?php echo $tpl['feedback_phone'];?></div>
</body>
</html>