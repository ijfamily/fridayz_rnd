<?php
/**
 * Gear CMS
 *
 * Стек вывода ошибок
 */
 
global $stackErrors;
?>
<!DOCTYPE>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

    <link rel="shortcut icon" href="/themes/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="/themes/gear/css/gear.css" type="text/css" />
</head>

<body>
<?php
for ($i = 0; $i < sizeof($stackErrors); $i++) {
    $err = $stackErrors[$i]['error'];
    if (isset($err['status']))
        $err['level'] = $err['status'];
    if (!isset($err['file']))
        $err['file'] = 'Gear Core';
    else
        $err['file'] = $err['file'] . ' <b>('. $err['line'] . ')</b>';
?>
    <div class="gear-err <?php echo strtolower($err['level']);?>">
        <div>
            <div class="gear-err-status">Gear: <?php echo $err['level'];?></div>
            <div class="gear-err-file"><?php echo $err['file'];?></div>
            <div class="gear-err-msg"><?php echo $err['message'];?></div>
        </div>
    </div>
<?php
}
?>
</body>
</html>