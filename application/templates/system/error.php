<?php
/**
 * Gear CMS
 *
 * Шаблона вывода ошибок
 */
 
global $fatalError;
?>
<!DOCTYPE>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

    <link rel="shortcut icon" href="/themes/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="/themes/gear/css/gear.css" type="text/css" />
</head>

<body>
    <div class="gear-err <?php echo strtolower($fatalError['level']);?>">
        <div>
            <div class="gear-err-status">Gear: <?php echo $fatalError['level'];?></div>
            <div class="gear-err-file"><?php echo $fatalError['file'], ' <b>(', $fatalError['line'], ')</b>';?></div>
            <div class="gear-err-msg"><?php echo $fatalError['message'];?></div>
        </div>
    </div>
</body>
</html>