<?php
/**
 * Gear CMS
 *
 * Шаблона вывода ошибок
 */

global $fatalError;
if (isset($fatalError['status']))
    $status = $fatalError['status'];
else
    $status = 'Error';
if (isset($fatalError['class']))
    $class = $fatalError['class'];
else
    $class = '';
?>
<!DOCTYPE>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

    <link rel="shortcut icon"  href="/themes/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="/themes/gear/css/gear.css" type="text/css" />
    <title><?php echo $status;?></title>
</head>

<body>
    <div class="gear-err <?php echo strtolower($fatalError['level']);?>">
        <div>
            <?php
            if (isset($fatalError['status']))
                echo '<div class="gear-err-status">Gear: ', $status, '</div>';
            if ($class)
                echo '<div class="gear-err-class">', $class, '</div>';
            ?>
            <div class="gear-err-msg"><?php echo $fatalError['message'];?></div>
        </div>
    </div>
</body>
</html>