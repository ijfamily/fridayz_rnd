<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Форма поиска"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('form-search-article'))) return;

// пагинация
$pagination = '';
$paginationTop = '';
$paginationBottom = '';
if ($tpl['pagination']) {
    $pagination .= '<ul class="pagination pagination-sm">';
    foreach($tpl['pagination'] as $index => $item) {
        switch ($item['type']) {
            case 'first': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Первая страница"><span aria-hidden="true">&laquo;</span></a></li>'; break;
            case 'prev': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Предыдущая страница"><span aria-hidden="true">&lsaquo;</span></a></li>'; break;
            case 'more': $pagination .= '<li class="disabled"><a href="#">▪▪▪</a></li>'; break;
            case 'page': $pagination .= '<li><a href="' . $item['url'] . '">' . $item['page'] . '</a></li>'; break;
            case 'active': $pagination .= '<li class="active"><a href="#">' . $item['page'] . '</a></li>'; break;
            case 'next': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Следущая страница"><span aria-hidden="true">&rsaquo;</span></a></li>'; break;
            case 'last': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Последняя страница"><span aria-hidden="true">&raquo;</span></a></li>'; break;
        }
    }
    $pagination .= '</ul>';
    if ($tpl['pagination-pos'] == 'top' || $tpl['pagination-pos'] == 'both')
        $paginationTop = $pagination;
    if ($tpl['pagination-pos'] == 'bottom' || $tpl['pagination-pos'] == 'both')
        $paginationBottom = $pagination;
}
?>
<section class="search">
    <div class="container">
		<h2 class="title">Поиск по сайту</h2>
		<!-- form search -->
		<form id="form-search" class="form-inline form-search" role="form" method="get" action="/search/">
			<div class="form-group">
				<input type="text" class="form-control input-search" id="q" name="q" maxlength="64" value="<?php echo $tpl['fields']['q'];?>" placeholder="Введите слова для поиска" required />
			</div>
			<button type="submit" class="btn btn-search">Найти</button>
		</form>
		<script type="text/javascript">
			(function(w, n, t, id) {
				w[n] = w[n] || {}; w[n][id] = { id: id, ajax: false, data: {}, valid: { name: t, css: true } };
			})(window, "gearForms", "formValidation", "form-search");
		</script>
		<!-- /form search -->
	<?php if (!($count = $tpl['count']) && !empty($tpl['search'])) : ?>
		<div style="text-align: center;">
			<div class="search-none">
				<p>Страницы, содержащие все слова запроса, не найдены.</p>
				<p>По запросу <span>"<?php echo $tpl['search'];?>"</span> ничего не найдено.</p>
				<p>Рекомендации:</p>
				<ul>
					<li>Убедитесь, что все слова написаны без ошибок.</li>
					<li>Попробуйте использовать другие ключевые слова.</li>
					<li>Попробуйте использовать более популярные ключевые слова.</li>
				</ul>
			</div>
		</div>
	<?php  endif; ?>

	<?php if ($tpl['count'] > 0) : ?>
		<div class="search-info margin-bottom-20">По Вашему запросу найдено <span><?=$tpl['count']; ?></span> запис(ь)ей.</div>
	<?php endif; ?>

	<?php if (!$tpl['is-success']) : ?>
		<div class="search-info"><?=$tpl['message']; ?></div>
	<?php endif; ?>

		<div class="list search search-result-grid">
		<?=$paginationTop;?>
			<div class="row">
	<?php foreach ($tpl['items'] as $t) : $date = GDate::format('d F / l', $t['date']); ?>
				 
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 media-card_wrap news-grid-item" >
					<a class="media-card media-card__height__small media-card__width__small media-card__shadowed" href="{chost}<?=$t['url'];?>">
						
						<div class="media-card_background" style="background-color:#5a4b92;background-image:url(<?php echo 'http://fridayz.ru/', (empty($t['img']) ? 'no_image.jpg' : $t['img']);?>);"  ></div>
						<div class="media-card_label" >
							<div class="color-label" style="background-color:#ff5a43;" ><?=$date;?></div>
						</div>
						
						<div class="media-card_content">
							<h3 class="media-card_title" ><?=$t['title'];?></h3>
							
						</div>
					</a>
				</div>
	<?php endforeach; ?>
			</div>
	<?=$paginationBottom; ?>
		</div>
	</div>
</section>
<!--end sect-->