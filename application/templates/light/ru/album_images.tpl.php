<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Изображения фотоальбомов"
 */

defined('_TPL') or die('Restricted access');


if (!($tpl = &Gear::tpl('album-images'))) return;

// пагинация
$pagination = '';
$paginationTop = '';
$paginationBottom = '';
if ($tpl['pagination']) {
    $pagination .= '<ul class="pagination pagination-sm">';
    foreach($tpl['pagination'] as $index => $item) {
        switch ($item['type']) {
            case 'first': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Первая страница"><span aria-hidden="true">&laquo;</span></a></li>'; break;
            case 'prev': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Предыдущая страница"><span aria-hidden="true">&lsaquo;</span></a></li>'; break;
            case 'more': $pagination .= '<li class="disabled"><a href="#">▪▪▪</a></li>'; break;
            case 'page': $pagination .= '<li><a href="' . $item['url'] . '">' . $item['page'] . '</a></li>'; break;
            case 'active': $pagination .= '<li class="active"><a href="#">' . $item['page'] . '</a></li>'; break;
            case 'next': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Следущая страница"><span aria-hidden="true">&rsaquo;</span></a></li>'; break;
            case 'last': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Последняя страница"><span aria-hidden="true">&raquo;</span></a></li>'; break;
        }
    }
    $pagination .= '</ul>';
    if ($tpl['pagination-pos'] == 'top' || $tpl['pagination-pos'] == 'both')
        $paginationTop = $pagination;
    if ($tpl['pagination-pos'] == 'bottom' || $tpl['pagination-pos'] == 'both')
        $paginationBottom = $pagination;
}

$count = sizeof($tpl['items']);

// режим конструктора
echo $tpl['design-tag-open'];
?>
<link rel="stylesheet" type="text/css" href="/themes/light/css/component.css"> 
 <script src="/themes/light/js/modernizr.custom.js"></script>
<!-- <script src="/themes/light/js/masonry.pkgd.min.js"></script>
<script src="/themes/light/js/imagesloaded.js"></script> -->
<script src="//unpkg.com/masonry-layout@4/dist/masonry.pkgd.js"></script>
<script src="//unpkg.com/imagesloaded@4/imagesloaded.pkgd.js"></script>

<script src="/themes/light/js/classie.js"></script>
<script src="/themes/light/js/AnimOnScroll.js"></script>



<!-- album-photo -->
<section class="album-photo list">
    <div class="container">
        <h2 class="tc"><?=GDate::format('d.m.Y', $tpl['album-date']), ' / ', $tpl['album-title'], ' / #Fridayz.ru';?></h2>
        <div class="album-photo-wrap">
            <?=$paginationTop;?>
            <div class="album-photo-body">
				<!-- <?php //print_r($tpl)?>-->
                <ul class="masonry-container grid effect-2" id="grid">
					<!--<li class="masonry-sizer"></li>-->
				  <?php foreach ($tpl['items'] as $t) : ?>
                    <li class="album-photo-i-mas masonry-item" data-src="{chost}/data/albums/<?=$t['folder'], '/', $t['i']['file'];?>" data-sub-html="">
				  <?php if ($tpl['design-tag-control']) echo str_replace('%s', $t['id'], $tpl['design-tag-control']); ?>
                       <img class="img-responsive" src="{chost}/data/albums/<?=$t['folder'], '/', $t['t']['file'];?>" alt="<?=$t['alt'];?>" title="<?=$t['title'];?>"> 
                    </li>
				  <?php endforeach; ?>
				  
                </ul>
            </div>
        <?=$paginationBottom;?>
        </div>
    </div>
</section>

<script>
  new AnimOnScroll( document.getElementById( 'grid' ), {
		minDuration : 0.9,
		maxDuration : 0.9,
		viewportFactor : 0.3
	} );  

/*$(document).ready(function(){
 
	// Main content container
	var $grid = $('.masonry-container');

	// Masonry + ImagesLoaded
	$grid.imagesLoaded(function(){
		$grid.masonry({
			// selector for entry content
			itemSelector: '.masonry-item',
			 
		});
	});
	
	$grid.infinitescroll({

		// selector for the paged navigation (it will be hidden)
		navSelector  : ".navigation",
		// selector for the NEXT link (to page 2)
		nextSelector : ".nav-previous a",
		// selector for all items you'll retrieve
		itemSelector : ".masonry-item",

		// finished message
		loading: {
			finishedMsg: 'No more pages to load.'
			}
		},

		// Trigger Masonry as a callback
		function( newElements ) {
			// hide new items while they are loading
			var $newElems = $( newElements ).css({ opacity: 0 });
			// ensure that images load before adding to masonry layout
			$newElems.imagesLoaded(function(){
				// show elems now they're ready
				$newElems.animate({ opacity: 1 });
				$grid.masonry( 'appended', $newElems, true );
			});

	});
});*/


</script>
<!-- /.album-photo -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>
<!--LightGallery -->
<link href="{theme}/plugins/lightgallery/css/lightgallery.css" rel="stylesheet" />
<script type="text/javascript" src="{theme}/plugins/lightgallery/js/lightgallery.js"></script>
<!--LightGallery widgets -->
<script type="text/javascript" src="{theme}/plugins/lightgallery/js/lg-hash.js"></script>