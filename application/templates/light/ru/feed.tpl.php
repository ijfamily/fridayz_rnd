<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Лента"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('feed'))) return;

// режим конструктора
echo $tpl['design-tag-open'];

?>
<!-- feed -->
<div>
<?php
$count = $tpl['count'];
for ($i = 0; $i < $count; $i++) :
    $t = $tpl['items'][$i];
    if ($t['title'])
        echo $t['title'];
    if ($t['src'])
        echo $t['src'];
    if ($t['text'])
        echo $t['text'];
    // режим конструктора
    if ($tpl['design-tag-control'])
        echo str_replace('%s', $t['id'], $tpl['design-tag-control']);
endfor;
?>
</div>
<!-- /feed -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>