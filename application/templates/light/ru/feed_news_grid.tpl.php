<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Лента новостей"
 */

defined('_TPL') or die('Restricted access');
$tpl['host'] = '//rnd.fridayz.ru';
if (!($tpl = &Gear::tpl('feed-news'))) return;

if (!($count = $tpl['count'])) return;

// режим конструктора
echo $tpl['design-tag-open'];

$t = $tpl['items'][0];
if (!empty($t['plain']))
    $t['plain'] = GString::copyStr($t['plain'], 0, 400);
?>
<!-- feed news grid -->
<div class="row">
				
 
	<?php for ($i = 0; $i < $count; $i++) : $t = $tpl['items'][$i];  $date = GDate::format('d F / l', $t['date']);  ?>
	 
	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 media-card_wrap news-grid-item" >
		<a class="media-card media-card__height__small media-card__width__small media-card__shadowed" href="<?=$t['url'];?>" >
			
			<div class="media-card_background" style="background-color:#5a4b92;background-image:url(<?=''.(empty($t['img']) ? '/data/images/no-image.jpg' : $t['img']);?>);"  ></div>
			<div class="media-card_label" >
				<div class="color-label" style="background-color:#ff5a43;" ><?=$date;?></div>
			</div>
			
			<div class="media-card_content">
				<h3 class="media-card_title" ><?=$t['title'];?></h3>
				 
			</div>
		</a>
	</div>
	 
		<?php
			// режим конструктора
			if ($tpl['design-tag-control'])
			echo str_replace('%s', $t['id'], $tpl['design-tag-control']);
		?>
	 
	<?php endfor; ?>
 </div >

<!-- /feed news grid -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>