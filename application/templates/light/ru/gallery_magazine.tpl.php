<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Фотогалерея страниц журнала"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('gallery'))) return;

// пагинация
$pagination = '';
$paginationTop = '';
$paginationBottom = '';
if ($tpl['pagination']) {
    $pagination .= '<div style="text-align:left"><ul class="pagination pagination-sm">';
    foreach($tpl['pagination'] as $index => $item) {
        switch ($item['type']) {
            case 'first': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Первая страница"><span aria-hidden="true">&laquo;</span></a></li>'; break;
            case 'prev': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Предыдущая страница"><span aria-hidden="true">&lsaquo;</span></a></li>'; break;
            case 'more': $pagination .= '<li class="disabled"><a href="#">▪▪▪</a></li>'; break;
            case 'page': $pagination .= '<li><a href="' . $item['url'] . '">' . $item['page'] . '</a></li>'; break;
            case 'active': $pagination .= '<li class="active"><a href="#">' . $item['page'] . '</a></li>'; break;
            case 'next': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Следущая страница"><span aria-hidden="true">&rsaquo;</span></a></li>'; break;
            case 'last': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Последняя страница"><span aria-hidden="true">&raquo;</span></a></li>'; break;
        }
    }
    $pagination .= '</ul></div>';
    if ($tpl['pagination-pos'] == 'top' || $tpl['pagination-pos'] == 'both')
        $paginationTop = $pagination;
    if ($tpl['pagination-pos'] == 'bottom' || $tpl['pagination-pos'] == 'both')
        $paginationBottom = $pagination;
}
// режим конструктора
echo $tpl['design-tag-open'];

?>




<section class="magazine-pages gallery_magazine-tpl">
    <div class="container_">
		<section class="afisha list magazine-pages-list-mobile hidden">
			<div class="container afisha-list " style="">
				<?php foreach ($tpl['items'] as $index => $t) :  ?>
				<? print_r($t);?>
				<a class="kafisha-i pirobox_gall" href="{chost}/data/galleries/<?=$t['folder'], '/', $t['i']['file'];?>" title="<?=strip_tags($t['title']);?>" rel="gallery">
					<div class="kafisha-i-img"><img src="{chost}/data/galleries/<?=$t['folder'], '/', $t['i']['file'];?>" /></div>
					<div class="kafisha-i-title"><?=$t['title'] ? str_replace(';', '<br>', $t['title']) : '';?></div>
				</a>
				<?php endforeach; ?>
			</div>
		</section>
    </div>
</section>
<!-- /.afisha -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>

</script>
