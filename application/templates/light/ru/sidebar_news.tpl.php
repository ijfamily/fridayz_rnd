<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Сайдбар статей"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('feed-news'))) return;

if (!($count = $tpl['count'])) return;

// режим конструктора
echo $tpl['design-tag-open'];

?>
<!-- sidebar news -->
 
<?php foreach ($tpl['items'] as $t) : ?>
	 
    <div class="sidebar-news-i">
		<div class="sidebar-news-i-img">
			<a class="sidebar-news-i-img-round" href="{chost}<?=$t['url'];?>" ><img class=" "  src="<?=$t['img'];?>"></a>
		</div>
        <div class="sidebar-news-i-text">
			<a class="title" href="{chost}<?=$t['url'];?>"><?=$t['title'];?></a>
			<div class="date small"><?=date('d.m.Y H:i', strtotime($t['date'] . ' ' . $t['time']));?></div>
		</div >
<?php if ($tpl['design-tag-control']) echo str_replace('%s', $t['id'], $tpl['design-tag-control']); ?>
    </div>
<?php endforeach; ?>
 
<!-- /sidebar news -->
<?=$tpl['design-tag-close'] ?>