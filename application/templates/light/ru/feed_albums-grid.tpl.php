<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Лента фотоальбомов"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('feed-albums'))) return;

if (!($count = $tpl['count'])) return;

// режим конструктора
echo $tpl['design-tag-open'];
?>
<!-- feed albums -->
 
            <div class="row  ">
<?php foreach ($tpl['items'] as $t) : $date = GDate::format('d F / l', $t['date']); ?>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 media-card_wrap news-grid-item  albums-grid-item" >
					<!--<a class="media-card media-card__height__small media-card__width__small media-card__shadowed" href="{chost}/albums/<?=$t['id'];?>">-->
					<a class="media-card media-card__height__small media-card__width__small media-card__shadowed" href="{chost}/albums/<?=$t['id'];?>">	
						<div class="media-card_background" style="background-color:#5a4b92;background-image:url(/data/albums/<?=$t['folder'], '/', $t['cover'];?>"></div>
						<div class="media-card_label" >
							<div class="color-label" style="background-color:#ff5a43;"><?=$date;?></div>
						</div>
						
						<div class="media-card_content">
							<h3 class="media-card_title" > <?=$t['name'];?></h3>
						
						</div>
					</a>
				 
<?php
    // режим конструктора
    if ($tpl['design-tag-control'])
        echo str_replace('%s', $t['id'], $tpl['design-tag-control']);
?>
                </div>
<?php endforeach; ?>
            </div>
         
<!-- /feed albums -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>