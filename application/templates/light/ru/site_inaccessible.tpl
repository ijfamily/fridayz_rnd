<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Внутренняя ошибка сервера (500-я ошибка)</title>

    <link type="image/x-icon" rel="shortcut icon" href="/resources/favicon.ico" />
    <link media="all" href="/resources/default/css/error.css" type="text/css" rel="stylesheet" />
</head>

<body>
    <div class="wrapper">
        <div class="page">
            <div class="msg">
                <h1>Сайт находится в разработке</h1>
            </div>
        </div>
    </div>
</body>
</html>