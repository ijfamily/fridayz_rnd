<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
    <component class="Meta"></component>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

    <link type="image/x-icon" href="/resources/favicon.ico" rel="shortcut icon"  /><component class="StyleSheet"></component>
    <link type="text/css" href="/resources/css/template.css" rel="stylesheet" media="all"/>
    <link type="text/css" href="/resources/css/components/style.css" rel="stylesheet" media="all"/>
</head>
<body>
<!--[if lt IE 9]>
    <div class="brw-ie">Вы пользуетесь устаревшей версией браузера Internet Explorer.
        <a href="http://www.microsoft.com/rus/windows/internet-explorer/">Перейти к загрузке Internet Explorer</a>
    </div>
<![endif]-->
<div class="article fixed">
    <component class="Article" path="article"></component>
</div>
