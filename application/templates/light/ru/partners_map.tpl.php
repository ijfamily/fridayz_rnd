<?php
/**
 * Gear CMS
 *
 * Шаблон компонента "Карта партнёров"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('partners-map'))) return;

// если нет партнёров
if (empty($tpl['list'])) return;

$arr = array();
$places = '';
foreach ($tpl['list'] as $index => $item) {
	$item['name'] =  htmlentities($item['name'], ENT_QUOTES);
    $arr[] = "[ [" . $item['coord'] 
            . "], { balloonContent: '" . $item['name'] 
            . "'}, { iconImageHref: '/data/images/icon-bar.png', iconImageSize: [36, 40], iconImageOffset: [10, -38] } ]  \r\n";
}
$places = implode(',', $arr);
?>
<div class="col-md-4 col-sm-12">
    <h2 class="title">Рядом</h2>
    <section class="map border">
        <script type="text/javascript">
            var partnerPlaces = [<?php echo $places; ?>];
        </script>
        <div id="map-places"></div>
    </section>
</div>