<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Лента видеозаписей"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('video-feed'))) return;

if (!($count = $tpl['count'])) return;

// режим конструктора
echo $tpl['design-tag-open'];

?>
<!-- feed video -->
<section class="video feed">
    <div class="container">
        <h2 class="title-tv"><span>Fridayz TV</span></h2>
        <div class="video-body">
            <div class="row row-offset">
<?php foreach ($tpl['items'] as $t) : ?>
                <div class="col-md-4 col-sm-4">
                    <div class="video-i">
                        <div class="video-i-img"><img src="<?=$t['thumbnail'];?>" alt="<?=$t['title'];?>" title="<?=$t['title'];?>" ></div>
                        <div id="video-<?=$t['code'];?>" class="video-i-footer" data-id="<?=$t['code'];?>">
                            <span>
                                <span class="icon"></span><span class="count"></span>
                            </span>
                        </div>
                        <div class="video-i-title"><?=$t['title'];?></div>
                        <a class="video-i-play" href="#" data-code="<?=$t['code'];?>" data-type="<?=strtolower($t['type']);?>" title="<?=$t['title'];?>"></a>
                    </div>
                </div>
<?php endforeach; ?>
            </div>
        </div>
    </div>
</section>
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>
