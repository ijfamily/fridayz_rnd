<!doctype html>

<html lang="{lang}">
<head>
    <component class="Meta"></component>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
   <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="yandex-verification" content="42ade3dcd327b719" />
    <meta name="google-site-verification" content="yYVv6Fs95sU_xmmECQ8L18v-gpaAzyMbGngYrloqVdw" />

    <link rel="shortcut icon" type="image/x-icon" href="{theme}/favicon.ico" />
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" type="text/css" href="{gear}/css/bootstrap.min.css" />
	
	<component class="StyleSheet"></component>
    <!-- Plugins CSS -->
    <link rel="stylesheet" type="text/css" href="{theme}/plugins/simplyscroll/css/jquery.simplyscroll.css" />
    <link rel="stylesheet" type="text/css" href="{theme}/plugins/bootstrap/css/bootstrap.touch-carousel.css" />
    <link rel="stylesheet" type="text/css" href="{theme}/plugins/barrating/themes/css-stars.css" />
    <link rel="stylesheet" type="text/css" href="{theme}/plugins/slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="{theme}/plugins/slick/slick-theme.css" />

	<link href="{theme}/css/jquery.fancybox.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{theme}/css/style.css?19-11-2019" />
    <link rel="stylesheet" type="text/css" href="{gear}/css/gear.css" />
	
	<link href="{theme}/css/font-awesome.min.css" rel="stylesheet">
	<link href="{theme}/css/jquery.fancybox.min.css" rel="stylesheet">

	<link href="{theme}/css/fast.css?05012020" rel="stylesheet">
    <!-- jQuery -->
    <script type="text/javascript" src="{gear}/js/jquery.min.js?v1.10.1"></script>

    <!--[if lt IE 9]>
        <script src="{gear}/js/html5shiv.js"></script>
        <script src="{gear}/js/respond.min.js"></script>
        <link href="{theme}/css/style-ie.css" rel="stylesheet" />
    <![endif]-->
</head>

<body id="body">
<!--[if lt IE 9]>
    <div class="brw-ie">Вы пользуетесь устаревшей версией браузера Internet Explorer.
        <a href="http://www.microsoft.com/rus/windows/internet-explorer/">Перейти к загрузке Internet Explorer</a>
    </div>
<![endif]-->

    <component class="Counter"></component>

    <component class="StyleDeclaration"></component>
	
	<condition for="page" for-data="index">
 
		<component id="banner-home-top" class="Banner" path="banner/"></component> 
 
	</condition>

	<condition for="category" for-data="5">
		<component id="bg-banner-albums" class="Banner" path="banner/"></component> 
	</condition>

	<!-- top nav-->
<div class="relative wrap">
	<section class="top-navigation clearfix affix-top" id="top-navigation" >
		<nav class="navbar navbar-default top-menu">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- <a class="navbar-brand visible-xs" href="{host}"><img src="/template-new/img/logo-white.png" class="img-responsive" alt="fridayz.ru"></a> -->
					
					<div class="header-logo-wrap-mobile visible-xs">
					 
						<condition for="url" for-data="/fridayz-tv/">
							 <a class="header-logo" id="logo-fridayz" href="{host}"><img src="{theme}/images/fridayz-tv-white.png" title="Fridayz TV" /></a> 
						</condition>
						<condition for="url" for-data-no="/fridayz-tv/">
							<div id="line-logo-fr" class="line-logo-fr"> <a  class="header-logo" href="{host}"><img src="{theme}/images/logo-white.png" title="#Fridayz - первый медийный онлайн журнал" /></a> </div>
						</condition>
					</div>
				</div>
				<div id="navbar" class="collapse navbar-collapse">
					 
					<ul class="nav navbar-nav">
						<li><a href="{host}"><span>Главная</span></a></li>
						<!--<li><a href="{host}/kinoafisha/"><span>Киноафиша</span></a></li>-->
						<li><a href="{host}/afisha/">Афиша</a></li>
						<li class="visible-xs visible-sm-inline-block">
							<a href="{host}/teatrevents/">Афиша&nbsp;театра</a>
						</li> 
						<li class="visible-xs visible-sm-inline-block">
							<a href="{host}/films/">Афиша&nbsp;кино</a>
						</li>
					 
						<!--<li><a href="{host}/top-people/">Top People</a></li>-->
						<li><a href="{host}/news/">Новости</a></li>
						<li><a href="{host}/places/bars-and-restaurants/">Места</a></li>
						<!--<li><a href="{host}/residents.html">Резиденты</a></li>-->
						<li><a href="{host}/albums/">В кадре</a></li>
						<!-- <li><a href="{host}/fridayz-tv/">FRIDAYZ TV</a></li> -->
						<!-- <li><a href="{host}/ny2020/">Новый год 2020</a></li> -->
					</ul>
				</div><!--/.nav-collapse -->
			</div>
		</nav>
	</section >
	<!-- top nav-->
	<div class="body-affix-height"></div>
	  <!-- Header -->
	<header class="page-header bg-gradient clearfix">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-7 col-sm-push-5 col-md-4 col-md-push-0">
					<ul class="header-eventtypes-menu list-inline">
						 <li>
							<a href="{host}/teatrevents/" class="teatr-link">Афиша<br class="hidden-xs"> театра</a>
						</li> 
						<li>
							<a href="{host}/films/"  class="kino-link">Афиша<br class="hidden-xs"> кино</a>
						</li>
						<li>
							<a href="{host}/afisha/"  class="cafe-link">Афиша<br class="hidden-xs"> мероприятий</a>
						</li>
					</ul>
				</div>
				<div class="hidden-xs col-sm-5 col-sm-pull-7 col-md-4 col-md-pull-0">
					<div class="header-logo-wrap-desctop">
					 
						<condition for="url" for-data="/fridayz-tv/">
							 <a class="header-logo" id="logo-fridayz" href="{host}"><img src="{theme}/images/fridayz-tv-white.png" title="Fridayz TV" /></a> 
						</condition>
						<condition for="url" for-data-no="/fridayz-tv/">
							<div id="line-logo-fr" class="line-logo-fr"> <a class="header-logo" href="{host}"><img src="{theme}/images/logo-white.png" title="#Fridayz - первый медийный онлайн журнал" /></a> </div>
						</condition>
					</div> 
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4">
					<div class="row header-search-wrap">
						<div class="col-xs-8 col-sm-7 col-md-8 col-lg-9 nopadding-right">
							<div class="header-search">
								<form action="/search/" method="get" role="form" class="header-search-form">
									<input type="text" class="form-control input-search show header-search-input" name="q" required="" maxlength="64" placeholder="Поиск">
									<button type="submit" id="btn-search" class="btn-search header-search-btn"></button>
								</form>
								
							</div >
						</div>
						<div class="col-xs-4 col-sm-5 col-md-4 col-lg-3 nopadding-left nopadding-right">
							<div class="header-icons">
								<a href="https://vk.com/fridayz_ru" target="_blank"><img class="" style="vertical-align:bottom" src="{theme}/images/vk-w.png"></a>
								
								<a href="https://instagram.com/fridayz_ru" target="_blank"><img class="" style="vertical-align:bottom" src="{theme}/images/insta-w.png"></a>
							
							</div >

						</div>
					</div>
				</div>
			</div>
		</div>
		 
           
	</header>
	  <!-- Header -->
	
    <div class="body-wrap">
    
	<condition for="url" for-data="/albums/">
		<style>
			.body-wrap{
				position:relative;
			}
		</style>
		<div class="container">
			<div class="page-albums-slider-home-wrap_place"></div>
		</div>
		
	</condition>
	<!-- Page Content -->	
    <component id="Article" class="Article" path="article/" for="page" for-data-no="index" ></component>
    <!-- END Page Content -->
	
	<condition for="url" for-data="/afisha/">
		<!--afisha popup banner -->
		<component id="banner-adisha-popup" class="Banner" path="banner/"></component> 
	</condition>


	<condition for="url" for-data="/news/">

		<div class="page-description container">
			<p>
				Портал Fridayz содержит информацию обо всех культурных событиях Ростова-на-Дону: самые актуальные новости, анонсы событий, предстоящие премьеры, концерты и многое другое. 
			</p>
			<p>
			Сотни событий на любой вкус ежедневно, а в выходные их ещё больше! Планируете выходные, хотите пойти на концерт известных артистов? В таком случае подборка новостей Fridayz послужит лучшим гидом!

			</p>
			
			<component id="banner-news-popup" class="Banner" path="banner/"></component>
		</div>
	</condition>
		
	<condition for="url" for-data="/places/bars-and-restaurants/">
	 
		<div class="page-description container">
			<p>
				Ищите где можно отдохнуть, сделать маникюр или заняться спортом? Самый полный список магазинов, спортивных клубов, салонов красоты, всевозможных услуг, баров и ресторанов Ростова-на-Дону ждёт Вас в разделе «Места» на сайте медийного онлайн журнала Fridayz. 
			</p>
			<p>
			Каждое заведение имеет свою визитную страничку, где указана полная информация, контактные данные, адреса и ссылки на социальные сети. В разделе «Места» Вы найдёте только самые топовые заведения, лучшие спортклубы, востребованные услуги, которые выбирают тысячи горожан. 

			</p>
			
			
		</div>
		
		<component id="banner-places-popup" class="Banner" path="banner/"></component>
	</condition> 
	

	<condition for="url" for-data="/albums/">
		<div class="page-albums-slider-home-wrap">
			   <component class="Gallery" path="gallery/gallery/" template="page_albums_slider.tpl.php" data-id="12" list-limit="5"></component>
		</div>
		
		<div class="page-description container">
			<p>
				Фотоотчёты культурных событий Ростова-на-Дону, самых ярких вечеринок и других мероприятий на сайте медийного онлайн журнала Fridayz. Раздел «В кадре» разработан для удобного размещения и поиска фотографий. Снимки находятся в открытом доступе, любой желающий сможет скачать понравившееся фото. 
			</p>
			<p>
			Фотографии выполнены резидентами Fridayz, самыми востребованными фотографами Ростова-на-Дону. Это не просто снимки, а профессиональная качественная работа. 

			</p>
			<component id="banner-fotootchet-popup" class="Banner" path="banner/"></component>
			
		</div>
	</condition>
	

	<condition for="url" for-data="/ny2020/">
		<!--NY2020 category popup banner -->
		<component id="banner-ny2020-popup" class="Banner" path="banner/"></component> 
	</condition>
	

	<condition for="url" for-data="/fridayz-tv/">
	 
		<div class="page-description container">
			<p>
				 Лучшие видеоотчёты прошедших вечеринок и иных событий города Ростова-на-Дону собраны в разделе Fridayz TV. 
			</p>
			<component id="banner-tv-popup" class="Banner" path="banner/"></component> 
		</div>
	</condition>
	
    <condition for="page" for-data="index">
		
        <component class="PartnersSlider" path="partners/slider/" template="partners_slider.tpl.php"></component>
  
		<section class="main-news bg-dark-blue page-section">
			<div class="container">
				<h2 class="section-title cl-white">Новости</h2>
			</div>
			
			<div class="horizontal-grid-controls">
				<button class="grid-btn-to-prev" id="grid-btn-to-prev"><i class="fa fa-arrow-left"></i></button>
				<button class="grid-btn-to-next" id="grid-btn-to-next"><i class="fa fa-arrow-right"></i></button>
			</div>
			<div class="home-events-grid" id="home-events-grid">
				
				<div class="container">
				
				 <component class="FeedNews" path="feed/news/" template="feed_news_grid-slider.tpl.php" category-id="2" limit="14" title="Новости"></component>
				
				</div ><!--container -->
			</div ><!-- home-events-grid-->
			<div class="btn-all-wrap margin-top-0 text-center">
				<a href="/news/" class="btn btn-outline-inverse  padding-left-32 padding-right-32">Читать все новости</a>
			</div>
		</section>
		
		
		<section class="home-places page-section">
			<div class="container">
				<h2 class="section-title ">TOП кафе и ресторанов Ростова-на-Дону</h2>
				 
				<component class="PartnersSliderBottom" path="partners/slider-bottom/" template="partners_bottom_slider.tpl.php" ></component>
				 
				<div class="btn-all-wrap margin-top-0 text-center">
					<a href="/places/" class="btn btn-outline  padding-left-32 padding-right-32">Смотреть все места</a>
				</div>
			</div>
		</section>		
				
				
		<section class="home-in-cadr page-section bg-red" >
			<div class="container">
				<h2 class="section-title cl-white">В кадре</h2>
				   <component class="FeedAlbums" path="feed/albums/" template="feed_albums-grid.tpl.php" category-id="5" limit="4"></component>
				<div class="btn-all-wrap margin-top-0 text-center">
					<a href="/albums/" class="btn btn-outline-inverse  padding-left-32 padding-right-32">Смотреть всю фотохронику</a>
				</div>
			</div>
			
			 
		</section>
		<section class="home-places-all page-section">
			<div class="container">
				<h2 class="section-title ">Куда сходить в Ростове-на-Дону</h2>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 media-card_wrap" >
						<a class="place-card media-card  media-card__height__small media-card__width__small media-card__shadowed" href="/places/" >
							
							<div class="media-card_background" style="background-color:#5a4b92;background-image:url(/themes/light/images/places-bg.jpg);" data-reactid="311"></div>
							
							
							<div class="media-card_content media-card__centered media-card_content_big text-center">
								<h3 class="media-card_title_lg size-30 cl-white" >Более 100 заведений</h3>
								<div class="media-card_text_lg text-italic" >посетите самые уютные кафе города</div>
								
							</div>
						</a>
					</div>	
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 media-card_wrap" >
						
						<div class="row">
							<div class="col-xs-6 col-sm-6">
								<a href="/places/theatres/" class="home-place-link first-link">
									<div class="home-place-counter">
										<div class="home-place-count  ">
											10
										</div>
										<div class="home-place-text text-center">
											<div class="text-bold " >развлекательных мест, где можно провести досуг</div>
											
											<div class="text-italic  ">Театры, кинотеатры</div>
										</div>
									</div>
								</a>
							</div>
							<div class="col-xs-6 col-sm-6">
								<a href="/places/beauty-salons/" class="home-place-link second-link">
									<div class="home-place-counter">
										<div class="home-place-count  ">
											20
										</div>
										<div class="home-place-text text-center">
											<div class="text-bold " >мест по уходу за красотой и фигурой</div>
											
											<div class="text-italic  ">Салоны красоты и спортклубы</div>
										</div>
									</div>
								</a>
							</div>
						</div>
						
					</div>
					
				</div>
			</div>	
		</section>
		<div class="page-description home-text-description container">
			<p>
				Афиша мероприятий, фототчёты, статьи и анонсы главных событий Ростова-на-Дону! Всё на одном ресурсе – Fridayz Ростов-на-Дону! 
			</p>
			<p>
			
				Выбираете куда сходить в Ростов-на-Дону? Первый медийный онлайн журнал Fridayz выступает путеводителем по культурной и развлекательной жизни. Гости информресурса имеют возможность в кротчайшее время найти информацию о культурно массовых мероприятиях, выбрать заведение для отдыха, узнать, где проходят самые лучшие вечеринки. 
			</p>
			<p>
Fridayz Ростов-на-Дону располагает всей контактной информацией о самых популярных барах и ресторанах Ростова-на-Дону, салонов красоты, магазинов и спортивных клубов, туристических агентств и многих других заведений.
 
			</p>
			 
		</div>
		
		
		

    </condition>
	</div><!--body-wrap-->
	 
	<!-- Footer -->
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-logo">
					<img class="logo-white" src="{theme}/images/f-media-color.png" alt="" title=""/>
					<img class="logo-white" src="{theme}/images/fridayz-blue.png" alt="" title=""/>
					 
					<img class="logo-white" src="{theme}/images/flycard-color.png" alt="" title="" height="100"/>
					<img class="logo-flycard" src="{theme}/images/fidayz-j-color.png" alt="" title="" height="90"/>
				</div>
				
				<div class="col-xs-12 col-md-6 col-sm-6 col-footer-text">
				   
					<p><b>© 2020 Fridayz.ru</b> - первый медийный онлайн журнал.<br />
					Все материалы сайта защищены.<br />
					При использовании любых материалов ссылка с указанием адреса обязательна.<br />
					Ответственность за содержание размещенной информации несет рекламодатель.<br />
					</p>
				</div>
				
				<div class="col-xs-12 col-md-6 col-sm-6 col-contacts">
					<condition for="page" for-data="index">
					   <div class="address">
							
							
								<span class="phone nowrap text-bold"><a href="tel:0505197770"><span class="cl-red">/</span>050<span class="cl-red">/&nbsp;</span>519&nbsp;777&nbsp;0</a></span>
							<br>
						
						 
					   </div>
						<div class="contact">
							<a class="contact-vk" target="_blank" href="https://vk.com/fridayz_ru">fridayz_ru</a>
							<a class="contact-inst"  target="_blank" href="https://instagram.com/fridayz_ru">fridayz_ru</a>
							<a class="contact-mail" target="_blank" href="mailto:info@fridayz.ru">info@fridayz.ru</a>
						</div>
					</condition>
				</div>
			</div>
		</div>
	</footer>
	<!-- /footer -->
</div><!-- end wrap-->

<!-- Sidenav -->
<div class="sidenav-overlay"></div>
<div id="sidenav" class="sidenav">
	<div class="sidenav-menu-header">
		<div class="sidenav-header-close"></div>
	</div>
	<ul class="sidenav-menu">
		
		<li><a href="{host}/afisha/">События</a></li>
	   
		<li><a href="{host}/news/">Новости</a></li>
		<li><a href="{host}/places/bars-and-restaurants/">Места</a></li>
		
		<li><a href="{host}/albums/">Фотоотчеты</a></li>
		<li><a href="{host}/fridayz-tv/">FRIDAYZ TV</a></li>
	</ul>
</div>
<!-- /.sidenav-->





<condition for="page" for-data="index">
	<component id="banner-home-image-popup" class="Banner" path="banner/"></component>
<!--  banner-home-vk-popup -->
	<component id="banner-home-vk-popup" class="Banner" path="banner/"></component>
<!--  end banner-home-vk-popup -->
</condition>

<!-- Video modal -->
<div id="modal-video" class="modal modal-video fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Закрыть"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body"><iframe></iframe><div class="modal-body-footer"></div></div>
			<div class="modal-footer" style="text-align: center;">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Закрыть</button>
			</div>
		</div>
	</div>
</div>
<!-- /.modal -->

 
	<div class="scroll-to eighteen-pluss"><a href="#">
 
		<span class="scroll-text">
			18+
		</span>
 
	</a></div>
 




 

<component class="Script"></component>
<!-- Bootstrap Core JavaScript -->
<script type="text/javascript" src="{gear}/js/bootstrap.min.js?v3.3.4"></script>
<script type="text/javascript" src="{gear}/js/jquery.blockUI.js"></script>
<!-- Plugins JS -->
<script type="text/javascript" src="{theme}/plugins/simplyscroll/js/jquery.simplyscroll.js"></script>
<script type="text/javascript" src="{theme}/plugins/barrating/jquery.barrating.min.js"></script>
<script type="text/javascript" src="{theme}/plugins/slick/slick.min.js"></script>
  
<script type="text/javascript" src="{theme}/js/script.js?v1.0"></script>
<!-- Gear JS -->
<script type="text/javascript" src="{gear}/js/gear.js?v1.0"></script>
<script type="text/javascript" src="{theme}/js/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="{theme}/js/jquery.touchSwipe.min.js"></script>
<script type="text/javascript" src="{theme}/js/custom.js?555"></script>

<script type="text/javascript" src="{theme}/js/jquery.cookie.js"></script>
<component class="ScriptDeclaration"></component>