<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Галерея Pirobox"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('gallery-pirobox'))) return;

// пагинация
$pagination = '';
$paginationTop = '';
$paginationBottom = '';
if ($tpl['pagination']) {
    $pagination .= '<ul class="pagination pagination-sm">';
    foreach($tpl['pagination'] as $index => $item) {
        switch ($item['type']) {
            case 'first': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Первая страница"><span aria-hidden="true">&laquo;</span></a></li>'; break;
            case 'prev': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Предыдущая страница"><span aria-hidden="true">&lsaquo;</span></a></li>'; break;
            case 'more': $pagination .= '<li class="disabled"><a href="#">▪▪▪</a></li>'; break;
            case 'page': $pagination .= '<li><a href="' . $item['url'] . '">' . $item['page'] . '</a></li>'; break;
            case 'active': $pagination .= '<li class="active"><a href="#">' . $item['page'] . '</a></li>'; break;
            case 'next': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Следущая страница"><span aria-hidden="true">&rsaquo;</span></a></li>'; break;
            case 'last': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Последняя страница"><span aria-hidden="true">&raquo;</span></a></li>'; break;
        }
    }
    $pagination .= '</ul>';
    if ($tpl['pagination-pos'] == 'top' || $tpl['pagination-pos'] == 'both')
        $paginationTop = $pagination;
    if ($tpl['pagination-pos'] == 'bottom' || $tpl['pagination-pos'] == 'both')
        $paginationBottom = $pagination;
}

$count = sizeof($tpl['items']);

// режим конструктора
echo $tpl['design-tag-open'];

?>
<!-- gallery pirobox -->
<?php
// пагинация
echo $paginationTop;
?>
<div class="row">
<?php
for ($i = 0; $i < $count; $i++) :
    $t = $tpl['items'][$i];
    echo '<div class="col-xs-6 col-md-3">';
    echo '<a title="', $t['title'], '" rel="gallery"  class="thumbnail pirobox_gall" href="{chost}/', $tpl['dir'], $t['folder'], '/', $t['i']['file'], '">',
         '<img alt="', $t['alt'], '" title="', $t['title'], '" src="{chost}/', $tpl['dir'], $t['folder'], '/', $t['t']['file'], '" />',
         '</a>';
    echo '<caption>', $t['t']['title'], '</caption>';
    // режим конструктора
    echo '<div align="center">', str_replace('%s', $t['id'], $tpl['design-tag-control']), '</div>';
    echo '</div>', _n;
endfor;
?>
</div>
<?php
// пагинация
echo $paginationBottom;
?>
<!-- /gallery pirobox -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>