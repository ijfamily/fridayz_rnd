<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Фотогалерея"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('gallery'))) return;

// пагинация
$pagination = '';
$paginationTop = '';
$paginationBottom = '';
if ($tpl['pagination']) {
    $pagination .= '<div style="text-align:left"><ul class="pagination pagination-sm">';
    foreach($tpl['pagination'] as $index => $item) {
        switch ($item['type']) {
            case 'first': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Первая страница"><span aria-hidden="true">&laquo;</span></a></li>'; break;
            case 'prev': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Предыдущая страница"><span aria-hidden="true">&lsaquo;</span></a></li>'; break;
            case 'more': $pagination .= '<li class="disabled"><a href="#">▪▪▪</a></li>'; break;
            case 'page': $pagination .= '<li><a href="' . $item['url'] . '">' . $item['page'] . '</a></li>'; break;
            case 'active': $pagination .= '<li class="active"><a href="#">' . $item['page'] . '</a></li>'; break;
            case 'next': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Следущая страница"><span aria-hidden="true">&rsaquo;</span></a></li>'; break;
            case 'last': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Последняя страница"><span aria-hidden="true">&raquo;</span></a></li>'; break;
        }
    }
    $pagination .= '</ul></div>';
    if ($tpl['pagination-pos'] == 'top' || $tpl['pagination-pos'] == 'both')
        $paginationTop = $pagination;
    if ($tpl['pagination-pos'] == 'bottom' || $tpl['pagination-pos'] == 'both')
        $paginationBottom = $pagination;
}
// режим конструктора
echo $tpl['design-tag-open'];

?>
<div id="bg-banner-afisha" class="banner"></div>
<!-- afisha -->
<section class="afisha list">
    <div class="container">
        <!--
        <h2 class="title">Афиша</h2>
        -->
        <?=$paginationTop;?>
        
            <div class="row afisha-list">
<?php foreach ($tpl['items'] as $index => $t) : ?>
            <a class="afisha-i pirobox_gall" href="{chost}/data/galleries/<?=$t['folder'], '/', $t['i']['file'];?>" title="" rel="gallery">
				 
                <div class="afisha-i-img" >
					<img src="{chost}/data/galleries/<?=$t['folder'], '/', $t['t']['file'];?>" />
					<div class="afihs-i-bg" style="background-image:url({chost}/data/galleries/<?=$t['folder'], '/', $t['t']['file'];?>)"></div>
				</div>
            </a>
<?php endforeach; ?>
            </div>
         
        <?=$paginationBottom;?>
    </div>
</section>
<!-- /.afisha -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>