<?php
/**
 * Gear CMS
 *
 * Шаблон компонента "Статья"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('article'))) return;
?>
<div class="row row-article">
    <div class="col-md-9 col-article">
        <article id="article-<?php echo $tpl['id'];?>" data-id="<?php echo $tpl['id'];?>" class="article article-edition">
        <?php
        // режим конструктора
        echo $tpl['design-tag-open'];
    
        if (!empty($tpl['header']) && $tpl['show-header'])
            echo '<h2 class="title">', $tpl['header'], '</h2>';
        echo $tpl['html'];
        
        // режим конструктора
        echo $tpl['design-tag-close'];
        ?>
        </article>
    </div>
    <div class="col-md-3 col-sidebar">
        <div class="sidebar sidebar-official">
            <ul>
                <li>
                    <div class="img"><img src="{chost}/data/images/photo-none.jpg" /></div>
                    <div class="title">Виталий Грек</div>
                    <div class="post">менеджер по работе с клиентами</div>
                </li>
                <li>
                    <div class="img"><img src="{chost}/data/images/photo-none.jpg" /></div>
                    <div class="title">Дмитрий Фурманов</div>
                    <div class="post">дизайнер</div>
                </li>
                <li>
                    <div class="img"><img src="{chost}/data/images/photo-none.jpg" /></div>
                    <div class="title">Антон Антонов</div>
                    <div class="post">программист</div>
                </li>
                <li>
                    <div class="img"><img src="{chost}/data/images/photo-none.jpg" /></div>
                    <div class="title">Сергей Антонов</div>
                    <div class="post">журналист</div>
                </li>
            </ul>
        </div>
    </div>
</div>