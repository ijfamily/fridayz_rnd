<!-- Banner top-->
<section class="banner">===
    <div class="container">
        <div class="row row-offset">
            <div class="col-md-6 col-sm-6">
                <a href="banner1_link" banner1_blank><img src="{host}banner1_image"></a>
            </div>
            <div class="col-md-6 col-sm-6">
                <a href="banner2_link" banner2_blank><img src="{host}banner2_image"></a>
            </div>
        </div>
    </div>
</section>
<!-- /banner top-->