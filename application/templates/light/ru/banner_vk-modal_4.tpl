
<!-- VK modal start template banner_vk-modal4.tpl -->
  <div id="modal-vk" class="modal modal-vk fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-vk">
		<div class="modal-content">
			<div class="modal-header" style="border-bottom:0">
				<button type="button" class="close" data-dismiss="modal" aria-label="Закрыть"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title">Мы ВКонтакте</h4>
			</div>
			<div class="modal-body"> <div id="modal-body-vk" ></div> </div>
		 
			<div class="modal-footer" style="text-align:center;border-top:0;">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Закрыть</button>
			</div>
		</div>
	</div>
</div> 
 <!-- /.modal -->