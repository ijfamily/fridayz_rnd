<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Список видеозаписей"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('video-list'))) return;

// пагинация
$pagination = '';
$paginationTop = '';
$paginationBottom = '';
if ($tpl['pagination']) {
    $pagination .= '<ul class="pagination pagination-sm">';
    foreach($tpl['pagination'] as $index => $item) {
        switch ($item['type']) {
            case 'first': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Первая страница"><span aria-hidden="true">&laquo;</span></a></li>'; break;
            case 'prev': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Предыдущая страница"><span aria-hidden="true">&lsaquo;</span></a></li>'; break;
            case 'more': $pagination .= '<li class="disabled"><a href="#">▪▪▪</a></li>'; break;
            case 'page': $pagination .= '<li><a href="' . $item['url'] . '">' . $item['page'] . '</a></li>'; break;
            case 'active': $pagination .= '<li class="active"><a href="#">' . $item['page'] . '</a></li>'; break;
            case 'next': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Следущая страница"><span aria-hidden="true">&rsaquo;</span></a></li>'; break;
            case 'last': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Последняя страница"><span aria-hidden="true">&raquo;</span></a></li>'; break;
        }
    }
    $pagination .= '</ul>';
    if ($tpl['pagination-pos'] == 'top' || $tpl['pagination-pos'] == 'both')
        $paginationTop = $pagination;
    if ($tpl['pagination-pos'] == 'bottom' || $tpl['pagination-pos'] == 'both')
        $paginationBottom = $pagination;
}
// режим конструктора
echo $tpl['design-tag-open'];

?>
 

<!-- video -->
<section class="video list">
    <div class="container">
        <h2 class="title">Видеотчеты</h2>
<?php if ($tpl['use-calendar']) : ?>
        <div class="calendar">
            <span class="calendar-date"><?php echo $tpl['date-on'] ? GDate::format('d F', $tpl['date-on']['timestamp']) : GDate::format('d F', time());?></span>
            <a class="calendar-btn" data-date-format="yyyy-mm-dd" href="#"></a>
            <?php if ($tpl['date-on']) : ?>
            <a class="calendar-all" href="{chost}/fridayz-tv/">все записи</a>
            <?php endif;?>
        </div>
<?php endif; ?>
        <?=$paginationTop;?>
        <div class="video-body">
            <div class="row ">
<?php if (!($count = $tpl['count'])) : ?>
                <div class="gear-page-public">Мы приносим свои извинения, но по вашему запросу нет записей на странице!</div>
<?php endif; ?>
<?php foreach ($tpl['items'] as $t) : ?>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 media-card_wrap news-grid-item video-grid-item">
					<? //print_r($t);?>
                    <div class="video-i media-card__shadowed media-card">
						<!--<div class="media-card_label" >
							<div class="color-label" style="background-color:#ff5a43;" ><?=$t['title'];?></div>
						</div>-->
                        <div class="video-i-img">
                            <img src="<?=$t['thumbnail'];?>" alt="<?=$t['title'];?>" title="<?=$t['title'];?>" >
                        </div>
                        <div id="video-<?=$t['code'];?>" class="video-i-footer" data-id="<?=$t['code'];?>">
                            <span>
                                <span class="icon"></span><span class="count">0</span>
                            </span>
                        </div>
						
						<div class="media-card_content">
							<h3 class="media-card_title"><?=$t['title'];?></h3>
							
						</div>
						
                       <!--<div class="video-i-title"><?=$t['title'];?></div>-->
						
                        <a class="video-i-play" href="#" data-code="<?=$t['code'];?>" data-type="<?=strtolower($t['type']);?>" title=" "></a>
                    </div>
                </div>
<?php endforeach; ?>
            </div>
        </div>
        <?=$paginationBottom;?>
    </div>
</section>
<div id="bg-banner-video" class="banner"></div>
<!-- /.video -->
<?=$tpl['design-tag-close'];?>

<?php if ($tpl['use-calendar']) : ?>
<script type="text/javascript">
    $(document).ready(function(){
        gear.calendarBs({
            selector: '.calendar-btn',
            language: '<?php echo $tpl['language'];?>',
            category: <?php echo $tpl['category-id'];?>,
            highlight: <?php echo $tpl['calendar-highlight'] ? 'true' : 'false';?>,
            dateOn: <?php echo $tpl['date-on'] ? '{year:' . $tpl['date-on']['year'] . ',month:' . ($tpl['date-on']['month'] - 1) . ',day:' . $tpl['date-on']['day'] . '}' : 'false'; ?>
        });
    });
</script>
<?php endif; ?>