<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Форма обратного вызова"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('form-signup'))) return;

Gear::doc(array('title' => 'Регистрация пользователя'));

// режим конструктора
echo $tpl['design-tag-open'];

?>
<section class="s-form">
<h2 class="title tc">Регистрация пользователя</h2>
<?php if ($tpl['is-submit'] && $tpl['is-confirm'] && !$tpl['is-error']) : ?>
    <div class="alert alert-success">Поздравляем, Вы успешно прошли регистрацию. Для подтверждения вашей учетной записи перейдите по ссылке в письме.</a></div>
<?php return; endif; ?>
<!-- form sign up -->
<div class="s-form-wrap">
<form  id="<?php echo $tpl['attr']['id'];?>" class="form-horizontal form-signup" role="form" method="post" action="<?php echo $tpl['use-ajax'] ? $tpl['form-action'] : '#' . $tpl['attr']['id'];?>">
<?php
if (!$tpl['use-ajax']) {
    if ($tpl['message']) {
        if ($tpl['is-success']) {
                echo '<div class="alert alert-success">Поздравляем, Вы успешно прошли регистрацию. Управление учетной записью доступно в <a href="{chost}/user/">Личном кабинете</a>.</div>';
        } else
            echo '<div class="alert alert-danger"><strong>Ошибка!</strong> ', $tpl['message'], '</div>';
    }
}
?>
    <input type="hidden" name="target" value="<?php echo $tpl['target'];?>"/>
    <input type="hidden" name="token" value="<?php echo $tpl['fields']['token'];?>"/>
    <div class="form-wrap">
        <div class="form-group" style="text-align: right;">
            если у вас есть учетная запись авторизируйтесь</a>
        </div>
        <div class="form-group">
             <label class="control-label col-sm-4" for="fname">Имя:</label>
             <div class="col-sm-8">
                <input type="text" class="form-control" id="fname" name="fname" maxlength="50" value="<?php echo $tpl['fields']['fname'];?>" placeholder="Введите ваше имя" required />
             </div>
        </div>
        <div class="form-group">
             <label class="control-label col-sm-4" for="lname">Фамилия:</label>
             <div class="col-sm-8">
                <input type="text" class="form-control" id="lname" name="lname" maxlength="50" value="<?php echo $tpl['fields']['lname'];?>" placeholder="Введите вашу фамилию" required />
             </div>
        </div>
        <div class="form-group">
             <label class="control-label col-sm-4" for="email">E-mail:</label>
             <div class="col-sm-8">
                <input type="text" class="form-control" id="email" name="email" maxlength="50" value="<?php echo $tpl['fields']['email'];?>" placeholder="Введите ваш e-mail" required />
             </div>
        </div>
        <div class="form-group">
             <label class="control-label col-sm-4" for="password">Пароль:</label>
             <div class="col-sm-8">
                <input type="password" class="form-control" id="password" name="password" maxlength="20" value="" required />
             </div>
        </div>
        <div class="form-group">
             <label class="control-label col-sm-4" for="password-cnf">Пароль <small>(подтв.)</small>:</label>
             <div class="col-sm-8">
                <input type="password" class="form-control" id="password-cnf" name="password-cnf" maxlength="20" value="" required />
             </div>
        </div>
    <?php if ($tpl['attr']['check-captcha']) : ?>
        <div class="form-group">
            <div class="col-sm-4 col-code"><img src="<?php echo $tpl['url-captcha'];?>" /></div>
            <div class="col-sm-8"><input type="text" class="form-control" name="code" style="width: 170px" required />введите код на картинке </div>
        </div>
    <?php endif; ?>
    </div>
    <?php if ($tpl['rules']) : ?>
    <div class="rules"><?php echo $tpl['rules'];?></div>
    <div><input type="checkbox" class="form-checkbox" id="rules" name="rules" /> ознакмолен(а) с правилами сайта</div>
    <?php endif; ?>
    <div class="form-wrap">
        <div class="row" style="text-align: center;">
            <button type="submit" class="btn btn-primary">Регистрация</button>
        </div>
    </div>
</form>
<?php
// использовать проверку
if ($tpl['attr']['use-scripts']) :
?>
<script type="text/javascript">
    (function(w, n, t, id) {
        w[n] = w[n] || {}; w[n][id] = { id: id, ajax: <?php echo $tpl['use-ajax'] ? 'true' : 'false';?>, data: {}, valid: { name: t, css: true } };
    })(window, "gearForms", "formValidation", "<?php echo $tpl['attr']['id'];?>");
</script>
<?php endif; ?>
<!-- /form sign up -->
</div>
</section>
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>