<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Облако тегов"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('cloude-tags'))) return;

if (!($count = $tpl['count'])) return;

// режим конструктора
echo $tpl['design-tag-open'];

?>
<!-- article-tags -->
<div class="article-tags">
    <div>
<?php
$limit = 7; $j = 0;
for ($i = 0; $i < $count; $i++) :
    $j++;
    $t = $tpl['items'][$i];
    if ($j > $limit) {
        $j = 0;
        echo '</div><div>';
    }
?>
        <a href="{chost}<?=$t['url'];?>" title="<?=$t['name'];?>" <?=$t['active'] ? 'class="active"' : '';?>><?=$t['name'];?></a>
<?php if ($tpl['design-tag-control']) echo str_replace('%s', $t['id'], $tpl['design-tag-control']); ?>
<?php endfor; ?>
    </div>
</div>
<!-- /.article-tags -->
<?=$tpl['design-tag-close'];?>