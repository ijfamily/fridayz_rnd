<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Форма обратной связи"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('form-feedback'))) return;

// режим конструктора
echo $tpl['design-tag-open'];

?>
<!-- form feedback -->
<section style="text-align: center;">
<div class="s-form s-form-front">
<h2 class="title tc">Обратная связь</h2>
<div class="s-form-wrap" style="text-align: left;">
<form id="<?php echo $tpl['attr']['id'];?>" class="form-horizontal form-feedback" role="form" method="post" action="<?php echo $tpl['use-ajax'] ? $tpl['form-action'] : '#' . $tpl['attr']['id'];?>">
<?php
if (!$tpl['use-ajax']) {
    if ($tpl['message']) {
        if ($tpl['is-success'])
            echo '<div class="alert alert-success">Спасибо за Ваше сообщение!</div>';
        else
            echo '<div class="alert alert-danger"><strong>Ошибка!</strong> ', $tpl['message'], '</div>';
    } else
        echo '<div class="alert alert-success small">Если Вы желаете связаться с нами в кратчайшее время, заполните следующие поля.</div>';
}
if ($tpl['use-ajax']) :
?>
    <input type="hidden" name="id" value="<?php echo $tpl['attr']['id'];?>"/>
    <input type="hidden" name="aid" value="<?php echo $tpl['article-id'];?>"/>
<?php endif; ?>
    <input type="hidden" name="target" value="<?php echo $tpl['target'];?>"/>
    <input type="hidden" name="token" value="<?php echo $tpl['fields']['token'];?>"/>
    <div class="form-group">
         <div class="col-sm-12">
            <input type="text" class="form-control" id="name" name="name" maxlength="50" value="<?php echo $tpl['fields']['name'];?>" placeholder="Введите ваше имя" required />
         </div>
    </div>
    <div class="form-group">
         <div class="col-sm-12">
            <input type="email" class="form-control" id="email" name="email" maxlength="50" value="<?php echo $tpl['fields']['email'];?>" placeholder="Введите ваш e-mail" required />
         </div>
    </div>
    <div class="form-group">
         <div class="col-sm-12">
            <input type="text" class="form-control" id="phone" name="phone" maxlength="20" value="" placeholder="Введите ваш телефон" />
         </div>
    </div>
    <div class="form-group">
         <div class="col-sm-12">
            <textarea name="text" id="text" class="form-control" placeholder="текст сообщения" style="height: 100px;"><?php echo $tpl['fields']['text'];?></textarea>
         </div>
    </div>
<?php if ($tpl['attr']['check-captcha']) : ?>
    <div class="form-group">
        <div class="col-sm-12"><input type="text" class="form-control" name="code" style="width:135px;display:inline-block;" placeholder="Код на картинке" required /><img src="<?php echo $tpl['url-captcha'];?>" height="53px"/></div>
    </div>
<?php endif; ?>
    <div class="row">
        <div class="col-sm-12"><button type="submit" class="btn btn-raised btn-primary">Отправить</button></div>
    </div>
</form>
</div>
</div>
</div>
<?php
// использовать проверку
if ($tpl['attr']['use-scripts']) :
?>
<script type="text/javascript">
    (function(w, n, t, id) {
        w[n] = w[n] || {}; w[n][id] = { id: id, ajax: <?php echo $tpl['use-ajax'] ? 'true' : 'false';?>, data: {}, valid: { name: t, css: true } };
    })(window, "gearForms", "formValidation", "<?php echo $tpl['attr']['id'];?>");
</script>
<?php endif; ?>
<!-- /form feedback -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>