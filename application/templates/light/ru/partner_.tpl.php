<?php
/**
 * Gear CMS
 *
 * Шаблон компонента "Заведение партнёра"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('partner'))) return;

if (empty($tpl['info'])) return;
$inf = $tpl['info'];
$ftr = $tpl['features'];
$isFutures = $ftr['karaoke'] || $ftr['hookah'] || $inf['delivery'] || $ftr['wifi'];
?>
<section class="place">
    <article id="place-<?=$inf['id'];?>" data-id="<?=$inf['id'];?>" class="place">
        <div class="place-header"<?=$tpl['category-image'] ? ' style="background-image: url(' . $tpl['category-image'] . ')"' : '';?>>
            <ul class="place-net">
                <?php if (!empty($ftr['vk'])) : ?>
                <li><a class="place-net-vk" href="<?=$ftr['vk'];?>" target="_blank" title="ВКонтакте"></a></li>
                <?php endif; ?>
                <?php if (!empty($ftr['ok'])) : ?>
                <li><a class="place-net-ok" href="<?=$ftr['ok'];?>" target="_blank" title="Одноклассники"></a></li>
                <?php endif; ?>
                <?php if (!empty($ftr['instagram'])) : ?>
                <li><a class="place-net-ins" href="<?=$ftr['instagram'];?>" target="_blank" title="Instagram"></a></li>
                <?php endif; ?>
                <?php if ($inf['site']) : ?>
                <li><a class="place-net-site" href="http://<?=$inf['site'];?>" target="_blank" title="Сайт"></a></li>
                <?php endif; ?>
            </ul>
            <div class="place-logo">
                <svg class="place-angle" version="1.1" width="100%"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" viewBox="0 0 100 200" preserveAspectRatio="none" >
                    <polygon fill="rgba(0, 0, 0, 0.6)" points="0,0 50,150 100,0"/>
                </svg>
                <div class="place-logo-ct">
                    <?php if ($tpl['info']['image']) { ?>
                    <img src="<?=$tpl['host'], $tpl['info']['image'];?>" />
                    <?php } else { ?>
                    <h2><?=$inf['name'];?></h2>
                    <?php } ?>
                </div>
            </div>
            <?php if ($inf['schedule']) : ?>
                <div class="place-schedule">
                    <div><span><?=$inf['schedule'];?></span></div>
                </div>
            <?php endif; ?>
            <div class="place-info">
                <?php /* if ($inf['kitchen']) : ?>
                <div class="place-kitchen">Кухня: <?php echo $inf['kitchen'];?></div>
                <?php endif; */ ?>
                <div class="place-address"><?php echo $inf['address'];?></div>
                <?php if ($inf['phone']) : ?>
                <div class="place-phone"><?php echo str_replace(',', ', ', $inf['phone']);?></div>
                <?php endif; ?>
            </div>
            <?php /* if ($isFutures) : ?>
            <ul class="place-features">
                <?php if (!empty($ftr['karaoke'])) : ?>
                <li><span class="place-feature-karaoke"></span></li>
                 <?php endif; ?>
                 <?php if (!empty($ftr['hookah'])) : ?>
                <li><span class="place-feature-hookah"></span></li>
                <?php endif; ?>
                <?php if ($inf['delivery']) : ?>
                <li><span class="place-feature-delivery"></span></li>
                <?php endif; ?>
                <?php if (!empty($ftr['wifi'])) : ?>
                <li><span class="place-feature-wifi"></span></li>
                <?php endif; ?>
            </ul>
            <?php endif; */ ?>
        </div>
        <div class="place-body">
            <div class="container">
                <div class="row row-offset">

<?php
        if ($inf['text'] && $inf['extended'])
            echo '<div class="place-text">', $inf['text'], '</div>';
?>
<?php if ($inf['images']) : ?>
                <ul class="place-photos" >
                <?php foreach ($inf['images'] as $t) : 
                    echo '<li class="place-photo-i" data-src="{chost}/data/images/', $inf['folder'], '/', $t['i']['file'], '" data-sub-html="">',
                            '<a href="#"><img class="img-responsive" src="{chost}/data/images/', $inf['folder'], '/', $t['t']['file'], '" alt="', $t['t']['title'], '" title="', $t['t']['title'], '"></a>', 
                         '</li>';
                endforeach;?>
                </ul>
<?php endif; ?>
                </div>
            </div>
        </div>
    </article>

<?php if ($inf['coord']) : ?>
    <script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>
    <script type="text/javascript">
        ymaps.ready(function init(){
            var mapPlace = new ymaps.Map('map-place', { center: [<?=$inf['coord-other'] ? '48.5689472,39.3154929' : $inf['coord'];?>], behaviors: ['default', 'scrollZoom'], zoom: <?=$inf['coord-other'] ? 11: 13;?> }),
                placemark = new ymaps.Placemark([<?=$inf['coord'];?>], { balloonContent: '<img width="200px" src="{host}/<?=$inf['image'];?>" />', iconContent: "<?=$inf['name'];?>" }, { preset: "twirl#blueStretchyIcon", balloonCloseButton: false, hideIconOnBalloonOpen: false });
            mapPlace.behaviors.disable('scrollZoom');
            mapPlace.geoObjects.add(placemark);
<?php
if ($inf['coord-other']) :
    foreach($inf['coord-other'] as $index => $item) : ?>
            placemark<?=$index + 1;?> = new ymaps.Placemark([<?=$item;?>], { balloonContent: '<img width="200px" src="{host}/<?=$inf['image'];?>" />', iconContent: "<?=$inf['name'];?>" }, { preset: "twirl#blueStretchyIcon", balloonCloseButton: false, hideIconOnBalloonOpen: false });
            mapPlace.geoObjects.add(placemark<?=$index + 1;?>);
<?php endforeach; endif; ?>
       });
    </script>
    <div id="map-place" style="width:100%; height:300px" class="map"></div>
<?php endif; ?>
</section>
<?php if ($inf['images']) : ?>
    <!--LightGallery -->
    <link href="{theme}/plugins/lightgallery/css/lightgallery.css" rel="stylesheet" />
    <script type="text/javascript" src="{theme}/plugins/lightgallery/js/lightgallery.js"></script>
    <!--LightGallery widgets -->
    <script type="text/javascript" src="{theme}/plugins/lightgallery/js/lg-hash.js"></script>
    <script type="text/javascript" src="{theme}/plugins/lightgallery/js/lg-social.js"></script>
    <script type="text/javascript">$(document).ready(function($){ $('.place-photos').lightGallery(); });</script>
<?php endif; ?>
<script type="text/javascript">
        $(document).ready(function($){ $('header').hide(); });
</script>