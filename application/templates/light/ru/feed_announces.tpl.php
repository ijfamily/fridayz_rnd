<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Лента анонсов, объявлений"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('feed-announces'))) return;

// режим конструктора
echo $tpl['design-tag-open'];

?>
<h2>Анонсы мероприятий</h2>
<!-- feed annonces -->
<div class="items-ann">
<?php
$count = $tpl['count'];
for ($i = 0; $i < $count; $i++) :
    $t = $tpl['items'][$i];
?>
    <div class="items-ann-i clearfix">
        <div class="date">
<?php
    // режим конструктора
    if ($tpl['design-tag-control'])
        echo str_replace('%s', $t['id'], $tpl['design-tag-control']);
?>
            <?php echo '<span>', date('d', strtotime($t['announce-date'])), '</span> ', GDate::format('M', $t['announce-date']), '</span><span class="time">', date('H-s', strtotime($t['announce-time'])), '</span>';?>
        </div>
        <div class="text">
           <a href="http://<?php echo $_SERVER['HTTP_HOST'] . $t['url'];?>"><?php echo $t['title'];?></a>
        </div>
    </div>
<?php
endfor;
?>
<div class="goto"><a href="/announces/">Все анонсы</a></div>
</div>
<!-- /feed annonces -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>