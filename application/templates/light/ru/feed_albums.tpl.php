<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Лента фотоальбомов"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('feed-albums'))) return;

if (!($count = $tpl['count'])) return;

// режим конструктора
echo $tpl['design-tag-open'];
?>
<!-- feed albums -->
<section class="photo feed">
    <div class="container">
        <h2 class="title">Фотоотчеты</h2>
        <div class="photo-body">
            <div class="row row-offset">
<?php foreach ($tpl['items'] as $t) : $date = GDate::format('d F / l', $t['date']); ?>
                <div class="col-md-4 col-sm-4">
                    <div class="photo-i">
                        <div class="photo-i-img"><img src="{chost}/data/albums/<?=$t['folder'], '/', $t['cover'];?>" title="Фотоотчёт / <?=$date, ' / ', $t['name'];?>" alt="Фотоотчёт / <?=$date, ' / ', $t['name'];?>" /></div>
                        <div class="photo-i-title"><?=$date;?> / <?=$t['name'];?> / #Fridayz.ru</div>
                        <a href="{chost}/albums/<?=$t['id'];?>" class="border"></a>
                    </div>
<?php
    // режим конструктора
    if ($tpl['design-tag-control'])
        echo str_replace('%s', $t['id'], $tpl['design-tag-control']);
?>
                </div>
<?php endforeach; ?>
            </div>
        </div>
    </div>
</section>
<!-- /feed albums -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>