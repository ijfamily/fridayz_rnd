<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Слайдер Slit"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('slider-slit'))) return;

// режим конструктора
echo $tpl['design-tag-open'];

?>
<!-- slider -->
<div class="slider1 sl-slider-wrapper">
<?php
$dots = '';
for ($i = 0; $i < $count; $i++) :
    $t = $tpl['items'][$i];
    $orientation = $i % 2;
    $dOrientation = $orientation ? 'horizontal' : 'vertical';
    $dSlice1Rotation = $orientation ? -25 : -15;
    $dSlice2Rotation = $orientation ? 15 : -3;
    if ($i == 0)
        $dots .= '<span class="nav-dot-current"></span>';
    else
        $dots .= '<span></span>';
?>
    <div class="sl-slider">
        <div class="sl-slide" data-orientation="<?php echo $dOrientation;?>" data-slice1-rotation="<?php echo $dSlice1Rotation;?>" data-slice2-rotation="<?php echo $dSlice2Rotation;?>" data-slice1-scale="1" data-slice2-scale="1">
            <div class="sl-slide-inner">
                <div class="bg-img" style="background-image: url(<?php echo $tpl['dir'], $t['src'];?>);"></div>
<?php
if ($t['title'])
    echo '<h2><span>', $t['title'], '</span></h2>';
?>
                <blockquote>
                    <h3>
<?php
if ($t['title-1'])
    echo $t['title-1'];

// режим конструктора
if ($tpl['design-tag-control'])
    printf($tpl['design-tag-control'], $t['id']);
?>
                    </h3><p><?php echo $t['text'];?></p>
                </blockquote>
            </div>
        </div>
    </div>
<? endfor; ?>
    <!-- /sl-slider -->
    <nav id="nav-arrows" class="nav-arrows">
        <span class="nav-arrow-prev">Previous</span>
        <span class="nav-arrow-next">Next</span>
    </nav>
    <nav id="nav-dots" class="nav-dots"><?php echo $dots;?></nav>
</div>
<!-- /slider -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>
