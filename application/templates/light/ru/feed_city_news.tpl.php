<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Лента новостей"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('feed-news'))) return;

if (!($count = $tpl['count'])) return;

// режим конструктора
echo $tpl['design-tag-open'];

$first = $tpl['items'][0];
?>
<!-- feed news -->
<section class="s-news feed feed-short">
    <h2 class="title1">#Город</h2>
    <div class="s-news-body">
        <a class="s-news-feature" href="{host}<?=$first['url'];?>">
            <div class="s-news-feature-label">#Город</div>
            <img src="{host}<?=empty($first['img']) ? '/data/images/place-none.jpg' : $first['img'];?>" />
            <div class="s-news-feature-text">
                <div class="title"><?=$first['title'];?></div>
                <div class="desc"><?=$first['text'];?></div>
            </div>
        </a>
        <div class="s-news-l">
            <ul>
                <?php for($i = 1; $i < $tpl['count']; $i++) : $t = $tpl['items'][$i];?>
                <li><a href="{chost}<?=$t['url'];?>">
                    <span class="s-news-l-title"><?=$t['title'];?> </span>
                    <time class="s-news-l-time">
                        <div class="s-news-l-time-days"> <?=GDate::format('d/m', $t['date']);?></div>
                    </time>
                </a></li>
                <?php endfor; ?>
            </ul>
        </div>
        <div id="vk_groups" class="s-news-soc"></div>
        <script type="text/javascript" src="//vk.com/js/api/openapi.js?126"></script>
        <!-- VK Widget -->
        <div id="vk_groups"></div>
        <script type="text/javascript">
        if (typeof VK != 'undefined')
            VK.Widgets.Group("vk_groups", {redesign: 0, mode: 0, width: "290", height: "349", color1: 'FFFFFF', color2: '000000', color3: '5E81A8'}, 23474857);
        </script>
    </div>
</section>
<!-- /feed news -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>