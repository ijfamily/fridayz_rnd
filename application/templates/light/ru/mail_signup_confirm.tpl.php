<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Форма регистрации пользователя (письмо)"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('data-mail'))) return;

?>
<html>
<body>
    <p>Для завершения регистрации на сайте <b><?php echo $tpl['domain'];?></b>, перейдите по ссылке 
    <a href="<?php echo $tpl['host'];?>signup/confirm/?<?php echo 'code=', $tpl['code'];?>"><?php echo $tpl['host'];?>signup/confirm/?<?php echo 'code=', $tpl['code'];?></a> </p>
    <p style="padding-left: 20px;">E-mail: <?php echo $tpl['e-mail'];?></p>
    <p style="padding-left: 20px;">Пароль: <?php echo $tpl['password'];?></p>
    <br />
    <p>---</p>
    <br />
    <p>Вы получили это сообщение, потому что Вы прошли регистрацию на сайте <b><?php echo $tpl['domain'];?></b>.</p>
    <p>Если данное письмо попало к Вам по ошибке, пожалуйста, проигнорируйте его и примите наши извинения.</p><br />
    <p>С наилучшими пожеланиями,<br />
    Администрация сайта <?php echo $tpl['domain'];?></p>
</body>
</html>