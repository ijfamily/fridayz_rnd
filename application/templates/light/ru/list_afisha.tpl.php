<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Список статей" (новости)
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('list-articles'))) return;

// пагинация
$pagination = '';
$paginationTop = '';
$paginationBottom = '';
if ($tpl['pagination']) {
    $pagination .= '<ul class="pagination pagination-sm">';
    foreach($tpl['pagination'] as $index => $item) {
        switch ($item['type']) {
            case 'first': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Первая страница"><span aria-hidden="true">&laquo;</span></a></li>'; break;
            case 'prev': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Предыдущая страница"><span aria-hidden="true">&lsaquo;</span></a></li>'; break;
            case 'more': $pagination .= '<li class="disabled"><a href="#">▪▪▪</a></li>'; break;
            case 'page': $pagination .= '<li><a href="' . $item['url'] . '">' . $item['page'] . '</a></li>'; break;
            case 'active': $pagination .= '<li class="active"><a href="#">' . $item['page'] . '</a></li>'; break;
            case 'next': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Следущая страница"><span aria-hidden="true">&rsaquo;</span></a></li>'; break;
            case 'last': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Последняя страница"><span aria-hidden="true">&raquo;</span></a></li>'; break;
        }
    }
    $pagination .= '</ul>';
    if ($tpl['pagination-pos'] == 'top' || $tpl['pagination-pos'] == 'both')
        $paginationTop = $pagination;
    if ($tpl['pagination-pos'] == 'bottom' || $tpl['pagination-pos'] == 'both')
        $paginationBottom = $pagination;
}
// режим конструктора
echo $tpl['design-tag-open'];
?>

<!-- list afisha -->
<div class="s-afisha list">
<?php
/*
if ($tpl['category-name'])
    echo '<h2 class="title tc">', $tpl['category-name'], '</h2>';
*/
// включить календарь
if ($tpl['use-calendar']) :
?>
    <div id="calendar-news" class="calendar">
        <div class="daterow"></div>
        <div class="dateline">
            <a href="#" class="prev-line">&lsaquo;</a>
            <a href="#" class="next-line">&rsaquo;</a>
            <div class="dateline-items"></div>
        </div>
    </div>
<?php
return;
endif;
// пагинация
echo $paginationTop;
?>
    <div class="s-afisha-body">
<?php
if (!($count = $tpl['count']))
    echo  '<div class="gear-page-public">Мы приносим свои извинения, но по вашему запросу нет записей на странице!</div>';

foreach ($tpl['items'] as $index => $t) :
    if ($t['announce-date']) {
        $date = GDate::format('d F, l', $t['announce-date']);
        if ($t['announce-time'])
            $date .= ', в ' . date('H:i', strtotime($t['announce-time']));
    } else
        $date = '';
?>
    <a class="s-afisha-i" href="{chost}<?php echo $t['url'];?>">
        <div class="date">
<?php
    // режим конструктора
    if ($tpl['design-tag-control'])
        echo str_replace('%s', $t['id'], $tpl['design-tag-control']);
?>
        <?php echo $date;?>
        </div>
        <div class="img"><img data-zoom="true" data-zoom-src="<?php echo $t['img'];?>" src="<?php echo $t['img'];?>" /></div>
        <!--
        <div class="attr">
            <div class="desc"><?php echo $t['text'];?></div>
            <div class="status"><span class="view"><?php echo $t['visits'];?></span></div>
        </div>
        -->
    </a>
<?php endforeach; ?>
    </div>
<?php
// пагинация
echo $paginationBottom;
?>
</div>
<!-- /list afisha -->
<?php
// режим конструктора
echo $tpl['design-tag-close'];

// включить календарь
if ($tpl['use-calendar']) :
?>
<script type="text/javascript">
    $(document).ready(function(){
        gear.calendarBs({
            selector: '#calendar-news',
            language: '<?php echo $tpl['language'];?>',
            category: <?php echo $tpl['category-id'];?>,
            highlight: <?php echo $tpl['calendar-highlight'] ? 'true' : 'false';?>,
            dateOn: <?php echo $tpl['date-on'] ? '{year:' . $tpl['date-on']['year'] . ',month:' . ($tpl['date-on']['month'] - 1) . ',day:' . $tpl['date-on']['day'] . '}' : 'false'; ?>
            
        });
    });
</script>
<?php endif; ?>