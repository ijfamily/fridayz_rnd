<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Список партнёров"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('partners-list'))) return;

//print_r($tpl);

// пагинация
$pagination = '';
$paginationTop = '';
$paginationBottom = '';
if ($tpl['pagination']) {
    $pagination .= '<ul class="pagination pagination-sm">';
    foreach($tpl['pagination'] as $index => $item) {
        switch ($item['type']) {
            case 'first': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Первая страница"><span aria-hidden="true">&laquo;</span></a></li>'; break;
            case 'prev': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Предыдущая страница"><span aria-hidden="true">&lsaquo;</span></a></li>'; break;
            case 'more': $pagination .= '<li class="disabled"><a href="#">▪▪▪</a></li>'; break;
            case 'page': $pagination .= '<li><a href="' . $item['url'] . '">' . $item['page'] . '</a></li>'; break;
            case 'active': $pagination .= '<li class="active"><a href="#">' . $item['page'] . '</a></li>'; break;
            case 'next': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Следущая страница"><span aria-hidden="true">&rsaquo;</span></a></li>'; break;
            case 'last': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Последняя страница"><span aria-hidden="true">&raquo;</span></a></li>'; break;
        }
    }
    $pagination .= '</ul>';
    if ($tpl['pagination-pos'] == 'top' || $tpl['pagination-pos'] == 'both')
        $paginationTop = $pagination;
    if ($tpl['pagination-pos'] == 'bottom' || $tpl['pagination-pos'] == 'both')
        $paginationBottom = $pagination;
}
// режим конструктора
echo $tpl['design-tag-open'];

?>
 
<?
$arr = array();
$places = ''; 
$tpl['list'] = $tpl['items'];
foreach ($tpl['list'] as $index => $item) {
	$item['name'] =  htmlentities($item['name'], ENT_QUOTES);
	
    $arr[] = "[ [" . $item['place_coord_lt'].', '.$item['place_coord_ln']. "], { balloonContent: '<a target=\'_blank\' href=\'/".$item['uri']."\'>" . $item['name'] 
            . "</a>'}, { iconImageHref: '/data/images/icon-bar.png', iconImageSize: [36, 40], iconImageOffset: [10, -38] } ]  \r\n";
}
$places = implode(',', $arr);

?>
   
<section class="map " style=" ">
	<script type="text/javascript">
		var partnerPlaces = [<?php echo $places; ?>];
	</script>
	<div id="map-places"></div>
</section>

<!-- places -->
<section class="places list">
<div class="container">
   
<?php if ($tpl['categories']) : ?>
    <div class="places-tags">
        <div class="row row-offset">
<?php foreach ($tpl['categories'] as $index => $item) : ?>
            <a <?=($tpl['category-id'] == $item['id'] ? ' class="active" ' : '');?> href="{host}/<?=$item['uri'];?>"><?=$item['name'];?></a>
<?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>
<?php if (!($count = $tpl['count'])) : ?>
        <div class="gear-page-public">Мы приносим свои извинения, но по вашему запросу нет записей на странице!</div>
<?php endif; ?>
    <?=$paginationTop;?>
    <div class="places-items" style="">
        <div class="places-items-row">
<?php
$j = $icolor = 0;
$colors = array('pink', 'blue', 'green', 'orange');
for ($i = 0; $i < $count; $i++) :
    $t = $tpl['items'][$i];
    if ($j == 0) {
       // echo '<div class="row">';
    }
    if (empty($t['cover'])) {
        $class = '';
        $image = ' style="background-image:  url(/data/images/catalog-none.jpg)"';
        /*
        $class = ' ' . $colors[$icolor];
        $image = '';
        $icolor++;
        if ($icolor > 3) $icolor = 0;
        */
    } else {
        $class = '';
        $image = ' style="background-image:  url(' . $t['cover'] . ')"';
    }
    if ($t['phone']) {
        $phone = explode(',', $t['phone']);
        //$phone ='' $phone[0];
    } else
        $phone = '';
    if ($t['address']) {
        $address = explode(';', $t['address']);
        $address = $address[0];
    } else
        $address = '';
?>
        <div class="place-item col-place">
			<? /* print_r($t); */?>
            <div class="place-i">
                <?php if ($tpl['design-tag-control']) echo str_replace('%s', $t['id'], $tpl['design-tag-control']); ?>
                <div class="place-i-img<?=$class;?>" <?=$image;?>>
					<a href="{chost}/<?=$t['uri'];?>/"  class="place-i-img-link"></a>
					
					<div class="place-i-like <?=$t['like'] ? '' : 'disabled';?>">
						<div>
							<a id="like-<?=$t['id'];?>" href="#">   
								<span class="like-icon"></span>
								<span class="like-count"><?=$t['like-votes'];?></span>
							</a>
						</div>
						<div class="like-msg"></div>
					</div>
					
				</div>
				
				
                <a href="{chost}/<?=$t['uri'];?>/" title="<?=$t['name'];?>">
					<div class="place-i-title"><?=$t['name'];?></div>
				</a>
               
                <div class="place-i-phone">
				
					<?php foreach ($phone as $p) : ?>
							<a href="tel:<?=$p;?>"><i class="fa fa-phone  cl-red"></i> <?=$p?></a><br>
					<?php endforeach; ?>
				</div>
               
				<div class="place-i-address" title="<?=$address;?>"><i class="fa fa-map-marker cl-red"></i> <?=$address;?></div>
                <!--
                <div class="place-i-rating">
                    <select id="rating-<?php echo $t['id'];?>" name="rating" class="barrating" autocomplete="off">
                      <option value="1"<?php echo $t['rating-state'] == 1 ? ' selected' : '';?>>1</option>
                      <option value="2"<?php echo $t['rating-state'] == 2 ? ' selected' : '';?>>2</option>
                      <option value="3"<?php echo $t['rating-state'] == 3 ? ' selected' : '';?>>3</option>
                      <option value="4"<?php echo $t['rating-state'] == 4 ? ' selected' : '';?>>4</option>
                      <option value="5"<?php echo $t['rating-state'] == 5 ? ' selected' : '';?>>5</option>
                    </select>
                    <div class="rating-msg"></div>
                </div>
                -->
                 <!--<div class="button">
                 <?php if ($t['extended']) { ?>
                 <a class="btn btn-small btn-primary" href="{chost}/<?=$t['uri'];?>/">подробнее</a>
                 <?php } else { ?>
                 <a class="btn btn-primary btn-disabled" href="#">подробнее</a>
                 <?php } ?>
                </div>-->
            </div>
        </div>
<?php
    if ($j == 2) {
       // echo '</div>';
        $j = -1;
    }
    $j++;
endfor;
?>
        </div>
    </div>
    <?=$paginationBottom;?>
</div>
</section>
<!-- /.places -->
<?=$tpl['design-tag-close'];?>