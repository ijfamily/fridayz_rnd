<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Слайдер партнёров"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('partners-bottom-slider'))) return;

// режим конструктора
echo $tpl['design-tag-open'];

if ($tpl['count'] == 0) return;


?>
<? //print_r($tpl); ?>
<!-- /slider-bottom -->
      <div class="row home-partners">
<?php foreach($tpl['items'] as $t) : ?>
            
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 media-card_wrap news-grid-item home-partners-item">
				<a class="media-card media-card__height__small media-card__width__small" href="<?=$t['uri'];?>" >
					<div class="home-partners-item-title-wrap">
						<h4 class="home-partners-item-title" title="<?=$t['name'];?>"><?=$t['name'];?></h4>
					</div>
				
					<div class="home-partners-item-img" style="background-image:url(<?=''.(empty($t['cover']) ? '/data/images/no-image.jpg' : $t['cover']);?>);"  ></div>
					
					
					<div class="home-partners-item-descr" title="<?=$t['address'];?>">
						 <i class="fa fa-map-marker cl-red"></i> <?=$t['address'];?>
					</div>
				</a>
			</div>
<?php endforeach; ?>
       </div>


