<?php
/**
 * Gear CMS
 *
 * Шаблон компонента "Статья" (новости)
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('article'))) return;
?>
<section class="article page-news">
    <div class="container">
        <div class="row  ">
            <div class="col-md-9">
                <article id="article-<?=$tpl['id'];?>" data-id="<?=$tpl['id'];?>" class="page-news-content white-block article">
<?=$tpl['design-tag-open'];?>
                    <div class="header page-news-header">
					<? if (!empty($tpl['header']) && $tpl['show-header']) : ?>
						<div class="date page-news-date"><?=date('d.m.Y', strtotime($tpl['published.date']));?> <?=date('H:i', strtotime($tpl['published.time']));?></div>
						
                        <h1 class="page-news-title"><?=$tpl['header'];?></h1>
                       
                        <div class="ya-share2 page-news-share" data-direction="horizontal" data-size ="m" data-services ="vkontakte,facebook,odnoklassniki,moimir,gplus,skype"></div>
					<?php if ($tpl['image']) : ?>
                        <div class="page-news-img"><img src="<?=$tpl['image'];?>" />
						<span class="page-news-img-title">Новости</span></div >
					<?php endif;endif; ?>
                    </div>

					<div class="page-news-cnt">
						<?=$tpl['html'];?>
					</div>
					
					
                    
<?=$tpl['design-tag-close'];?>
                </article>
				
				<div class="message vk text-center">Вступайте в нашу группу <a href="https://vk.com/fridayz_ru" target="_blank"> <b>ВКонтакте</b></a> и подписывайтесь на <a href="https://instagram.com/fridayz_ru" target="_blank"><b>Инстаграм</b></a>, будьте в курсе всех событий!</div>
				
                <div class="sidebar-bottom margin-top-20 ">
                  
<?php
Gear::component('FeedNews', 'feed/news/', array('limit' => 6, 'category-id' => 2, 'data-random' => true, 'ellipsis-header' => 70, 'template' => 'sidebar_bottom_news.tpl.php'));
?>
                </div>
            </div>
            <div class="col-md-3">
                <div class="sidebar-right white-block margin-bottom-20">
                    <h3 class="sidebar-title">Другие новости</h3>
<?php
Gear::component('FeedNews', 'feed/news/', array('limit' => 10, 'category-id' => 2, 'category-default' => true, 'template' => 'sidebar_news.tpl.php'));
?>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
<script type="text/javascript" src="//yastatic.net/share2/share.js"></script>