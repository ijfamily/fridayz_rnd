<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Список категорий статей"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('list-categories'))) return;

// пагинация
$pagination = '';
$paginationTop = '';
$paginationBottom = '';
if ($tpl['pagination']) {
    $pagination .= '<ul class="pagination pagination-sm">';
    foreach($tpl['pagination'] as $index => $item) {
        switch ($item['type']) {
            case 'first': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Первая страница"><span aria-hidden="true">&laquo;</span></a></li>'; break;
            case 'prev': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Предыдущая страница"><span aria-hidden="true">&lsaquo;</span></a></li>'; break;
            case 'more': $pagination .= '<li class="disabled"><a href="#">▪▪▪</a></li>'; break;
            case 'page': $pagination .= '<li><a href="' . $item['url'] . '">' . $item['page'] . '</a></li>'; break;
            case 'active': $pagination .= '<li class="active"><a href="#">' . $item['page'] . '</a></li>'; break;
            case 'next': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Следущая страница"><span aria-hidden="true">&rsaquo;</span></a></li>'; break;
            case 'last': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Последняя страница"><span aria-hidden="true">&raquo;</span></a></li>'; break;
        }
    }
    $pagination .= '</ul>';
    if ($tpl['pagination-pos'] == 'top' || $tpl['pagination-pos'] == 'both')
        $paginationTop = $pagination;
    if ($tpl['pagination-pos'] == 'bottom' || $tpl['pagination-pos'] == 'both')
        $paginationBottom = $pagination;
}
// режим конструктора
echo $tpl['design-tag-open'];

?>
<!-- list categories -->
<div class="container article">
<?php
if ($tpl['category-name'])
    echo '<h2><span>', $tpl['category-name'], '</span> </h2>';
if (!$tpl['count']) :
?>
    <div class="gear-page-public">Мы приносим свои извинения, но по вашему запросу нет записей на странице!</div>
</div>
<?php
return; endif;

// пагинация
echo $paginationTop;
?>
<ul class="gear-list-categories">
<?php
$count = sizeof($tpl['items']);
for ($i = 0; $i < $count; $i++) :
    $t = $tpl['items'][$i];
?>
    <li class="gear-list-i">
        <div class="gear-list-i-title"> <a href="<?php echo $tpl['host'], $t['url'], '">', $t['title'], '</a>';?></div>
    </li>
<?php endfor; ?>
    </ul>
<?php
// пагинация
echo $paginationBottom;
?>
<!-- /list categories -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>