<?php
/**
 * Gear CMS
 *
 * Шаблон компонента "Заведение партнёра"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('partner'))) return;

//print_r($tpl);

if (empty($tpl['info'])) return;
$inf = $tpl['info'];
$ftr = $tpl['features'];
$isFutures = $ftr['karaoke'] || $ftr['hookah'] || $inf['delivery'] || $ftr['wifi'];

$inf['address'] = str_replace('ганск,', 'ганск<br>', $inf['address']);
//$address = $inf['address'];


if (empty($inf['head']))
    $bg = $tpl['category-image'];
else
    $bg = $inf['head'];
$cssPhones = '';

$inf['name']=htmlspecialchars($inf['name']);
?>
<section class="partner page-news">
    <article id="partner-<?=$inf['id'];?>" data-id="<?=$inf['id'];?>" class="place">
        <div class="container">
			 <h1 class="page-place-title visible-xs visible-sm nomargin-top"><?=$inf['name'];?></h1>
			<div class="row partners-col">
				<div class="col-md-9 content-col  margin-bottom-20">
					<div class="page-parnter-content white-block">
						
						<?php if ($inf['coord']) : ?>
						<div class="partner-map">
							<div id="map-place"  style=" width: 100%; height:332px;"></div>
						</div>
						<?php else: ?>
							<?php $logo_st="style='margin-top:0px'"?>
						<?php endif; ?>
						
						<?php if ($inf['image']) : ?>
					 
						<div class="place-logo" <?=$logo_st?>>
							<img src="<?=$inf['image']?>" >
						</div>
						<?php else: ?>
							<?php $title_st="style='padding-left:0px'"?>
						<?php endif; ?>
						 
						
						<div class="partner-title clearfix" <?=$title_st?>>
							 <h1 class="page-place-title"><?=$inf['name'];?></h1>
							 
							
							<div class="place-i-like <?=$inf['like'] ? '' : 'disabled';?>">
								<div>
									<a id="like-<?=$inf['id'];?>" href="#">   
										<span class="like-icon"></span>
										<span class="like-count"><?=$inf['like-votes'];?></span>
									</a>
								</div>
								<div class="like-msg" style="display:none"></div>
							</div>
						</div>
						
						
						
						<!-- <div class="partner-head"<?=$bg ? ' style="background-image: url(' . $bg . ')"' : '';?>></div>-->
						<div class="partner-content">
							<?php
								if ($inf['text'] && $inf['extended'])
								echo '<div class="partner_text">', $inf['text'], '</div>';
								else
								echo '<div class="partner_text"><span class="partner_text-none">Описание отсутствует</span></div>';
							?>
						</div>
					</div >
				</div>
				<div class="col-md-3 info-col margin-bottom-20">
					<div class="sidebar-right partner-info-col white-block margin-bottom-20  ">
						
							<?php if ($inf['images']) { ?>
								<div class="partner-fotos">
									<ul class="partner-photos partner-photos-slider" id="partner-photos" style="display:none">
										<?php
										 
											$count = sizeof($inf['images']);
											$style = '';
											for ($i = 0; $i < $count; $i++) :
											$t = $inf['images'][$i];
											 
											echo '<li ', $style, 'class="partner-photo-i" data-src="{chost}/data/images/', $inf['folder'], '/', $t['i']['file'], '" data-sub-html="">',
											'<a href="#"><img class="img-responsive" src="{chost}/data/images/', $inf['folder'], '/', $t['i']['file'], '" alt="', $t['i']['title'], '" title="', $t['i']['title'], '"></a>', 
											'</li>';
										endfor;?>
									</ul>
									
								</div>
								
								<div class="place-arrows-controls">
									<div class="arr-left">
										<i class="fa fa-angle-left" aria-hidden="true"></i>
									</div>		
									<div class="arr-right">
										<i class="fa fa-angle-right" aria-hidden="true"></i>
									</div>	
								</div>
								
								<div class="place-fotos-inf">
									<div class="place-fotos-title text-bold">
										Фото: <?=$inf['name'];?>
									</div>
									
									<div class="place-fotos-conunter">
										<span class="cur-sl"> </span >
									</div>
								</div >
								
								<?php } else { ?>
								<span class="photo-none">Фото отсутствуют</span>
							<?php } ?>
						
						
					</div>
					
					<div class="sidebar-right partner-info-col white-block margin-bottom-20 nopadding-bottom">
						<h3 class="p-features-title">Информация про <?=$inf['name'];?></h3>
						<div class="p-features">
							 
							<?php if ($inf['address']) : ?>
							<?
								$addreses = explode(';', $inf['address']); 
							?>							
							<div class="p-addr p-features-i" >
								<!--<span class="sm-icon"><i class="fa fa-map-marker cl-red " aria-hidden="true"></i></span >-->
								<span class="p-features-name"><i class="fa fa-map-marker cl-red size-18 " aria-hidden="true"></i> Адрес:</span>
								<span class="p-features-content">
									<?if ($addreses):?>
										<?foreach( $addreses as $s):?>
											<div class=" "><?=$s?></div>
										<?endforeach;?>
									<?else:?>
										<?=$inf['address']?>
									<?php endif; ?>
								</span >
							</div>
							<?php endif; ?>
							
							<?php if ($inf['schedule']) : ?>
							<div class="p-schedule p-features-i">
								<span class="p-features-name"><i class="fa fa-clock-o cl-red size-18" aria-hidden="true"></i> Время работы: </span>
								<span class="p-features-content"><?=$inf['schedule'] ? $inf['schedule'] : '<span style="color:#cacaca">неизвестно</span>';?></span >
							</div>
							<?php endif; ?>
							
							<?php if ($inf['phone']) : ?>
							<?
								if ($inf['phone']) {
									$phones = explode(',', $inf['phone']);
									
									if (strlen($inf['phone']) > 32)
									$cssPhones = ' more-lines';
								}
							?>
							<div class="p-phones p-features-i">
								<span class="p-features-name"><i class="fa fa-phone cl-red size-18 " aria-hidden="true"></i> Телефон:</span>
								<span class="p-features-content">
									<?foreach( $phones as $p):?>
										<div><a class="p-phone-i nowrap" href="tel:<?=$p?>"><?=$p?></a></div>
									<?endforeach;?>
								</span >
							</div>
							<?php endif; ?>
							
							<?php if (!empty($ftr['vk']) || !empty($ftr['ok']) || !empty($ftr['instagram']) || !empty($inf['site'])) : ?>
							<div class="p-phones p-features-i">
								<span class="p-features-name">Мы в социальных сетях:</span>
								<span class="p-features-content">
									<div class="p-socseti">
										<ul class="p-socseti-list list-inline">
											<?php if (!empty($ftr['vk'])) : ?>
											<li><a class="icon icon-vk" href="<?=$ftr['vk'];?>" target="_blank" title="ВКонтакте"></a></li>
											<?php endif; ?>
											<?php if (!empty($ftr['ok'])) : ?>
											<li><a class="icon icon-ok" href="<?=$ftr['ok'];?>" target="_blank" title="Одноклассники"></a></li>
											<?php endif; ?>
											<?php if (!empty($ftr['instagram'])) : ?>
											<li><a class="icon icon-ins" href="<?=$ftr['instagram'];?>" target="_blank" title="Instagram"></a></li>
											<?php endif; ?>
											 
										</ul>	 
									</div>
								</span >
							</div>
							<?php endif; ?>
							
							<?php if (!empty($inf['site'])) : ?>
							<div class="p-site p-features-i">
								<span class="p-features-name">Официальный сайт:</span>
								<span class="p-features-content">
									<a class=" " href="http://<?=$inf['site'];?>" target="_blank" title="Сайт"><?=$inf['site'];?></a>
								</span >
							</div>
							<?php endif; ?>
							
						</div>
						
						
						
						
					</div>
					
				</div>
			</div>
           
        </div>
        
    </article>

<?php if ($inf['coord']) : ?>
    <script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>
    <script type="text/javascript">
        ymaps.ready(function init(){
            var mapPlace = new ymaps.Map('map-place', { center: [<?=$inf['coord-other'] ? '48.5689472,39.3154929' : $inf['coord'];?>], behaviors: ['default', 'scrollZoom'], zoom: <?=$inf['coord-other'] ? 11: 13;?> }),
            
			placemark = new ymaps.Placemark([<?=$inf['coord'];?>], { balloonContent: '<img width="200px" src="{host}/<?=$inf['image'];?>" />', iconContent: "<?=$inf['name'];?>" }, { preset: "twirl#blueStretchyIcon", balloonCloseButton: false, hideIconOnBalloonOpen: false });
			mapPlace.controls.add('zoomControl');
            mapPlace.behaviors.disable('scrollZoom');

			if($(document).innerWidth()<1024){
				//mapPlace.behaviors.disable('drag');
			}
			
            mapPlace.geoObjects.add(placemark);
<?php
if ($inf['coord-other']) :
    foreach($inf['coord-other'] as $index => $item) : ?>
            placemark<?=$index + 1;?> = new ymaps.Placemark([<?=$item;?>], { balloonContent: '<img width="200px" src="{host}/<?=$inf['image'];?>" />', iconContent: "<?=$inf['name'];?>" }, { preset: "twirl#blueStretchyIcon", balloonCloseButton: false, hideIconOnBalloonOpen: false });
            mapPlace.geoObjects.add(placemark<?=$index + 1;?>);
<?php endforeach; endif; ?>
       });
    </script>
<?php endif; ?>
</section>
<?php if ($inf['images']) : ?>
    <!--LightGallery -->
    <link href="{theme}/plugins/lightgallery/css/lightgallery.css" rel="stylesheet" />
    <script type="text/javascript" src="{theme}/plugins/lightgallery/js/lightgallery.js"></script>
    <!--LightGallery widgets -->
    <script type="text/javascript" src="{theme}/plugins/lightgallery/js/lg-hash.js"></script>
    <script type="text/javascript">$(document).ready(function($){ $('.partner-photos').lightGallery(); });</script>
<?php endif; ?>
<script type="text/javascript">
        /* $(document).ready(function($){ $('header').hide(); }); */
</script>