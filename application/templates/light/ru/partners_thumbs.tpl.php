<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Список партнёров"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('partners-list'))) return;

// пагинация
$pagination = '';
$paginationTop = '';
$paginationBottom = '';
if ($tpl['pagination']) {
    $pagination .= '<ul class="pagination pagination-sm">';
    foreach($tpl['pagination'] as $index => $item) {
        switch ($item['type']) {
            case 'first': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Первая страница"><span aria-hidden="true">&laquo;</span></a></li>'; break;
            case 'prev': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Предыдущая страница"><span aria-hidden="true">&lsaquo;</span></a></li>'; break;
            case 'more': $pagination .= '<li class="disabled"><a href="#">▪▪▪</a></li>'; break;
            case 'page': $pagination .= '<li><a href="' . $item['url'] . '">' . $item['page'] . '</a></li>'; break;
            case 'active': $pagination .= '<li class="active"><a href="#">' . $item['page'] . '</a></li>'; break;
            case 'next': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Следущая страница"><span aria-hidden="true">&rsaquo;</span></a></li>'; break;
            case 'last': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Последняя страница"><span aria-hidden="true">&raquo;</span></a></li>'; break;
        }
    }
    $pagination .= '</ul>';
    if ($tpl['pagination-pos'] == 'top' || $tpl['pagination-pos'] == 'both')
        $paginationTop = $pagination;
    if ($tpl['pagination-pos'] == 'bottom' || $tpl['pagination-pos'] == 'both')
        $paginationBottom = $pagination;
}
// режим конструктора
echo $tpl['design-tag-open'];

?>
<div id="bg-banner-top-long" class="banner"></div>

<!-- list partners -->
<section class="s-places list">
    <div class="s-places-body">
<?php
/*
if ($tpl['category-name'])
    echo '<h2 class="title tc">', $tpl['category-name'], '</h2>';
*/
if ($tpl['categories']) {
    echo '<div class="types">';
    foreach ($tpl['categories'] as $index => $item)
        echo '<a ', ($tpl['category-id'] == $item['id'] ? ' class="active" ' : ''), 'href="{host}/', $item['uri'] ,'">', $item['name'], '</a>';
    echo '</div>';
}

// пагинация
echo $paginationTop;
?>
        <div class="row">
        <div class="col-md-12">
<?php
if (!($count = $tpl['count']))
    echo '<div class="gear-page-public">Мы приносим свои извинения, но по вашему запросу нет записей на странице!</div>';
$j = 0;
for ($i = 0; $i < $count; $i++) :
    $t = $tpl['items'][$i];
?>

        <div class="s-places-i">
            <?php if ($t['extended']) { ?>
            <a class="img" href="{chost}/<?php echo $t['uri'];?>/">
            <img src="{chost}<?php echo $t['image'] ? $t['image'] : '/data/images/place-none.jpg';?>" />
            </a>
            <?php } else { ?>
            <a class="img" href="#">
            <img src="{chost}/data/images/place-none.jpg" />
            </a>
            <?php } ?>
            <div class="row">
                <div class="col-md-6 s-places-brand">
                    <?php
                    // режим конструктора
                    if ($tpl['design-tag-control'])
                        echo str_replace('%s', $t['id'], $tpl['design-tag-control']);
                    ?>
                    <h2><?php echo $t['name'];?></h2>
                    <div class="rating">
                        <select id="rating-<?php echo $t['id'];?>" name="rating" class="barrating" autocomplete="off">
                          <option value="1"<?php echo $t['rating-state'] == 1 ? ' selected' : '';?>>1</option>
                          <option value="2"<?php echo $t['rating-state'] == 2 ? ' selected' : '';?>>2</option>
                          <option value="3"<?php echo $t['rating-state'] == 3 ? ' selected' : '';?>>3</option>
                          <option value="4"<?php echo $t['rating-state'] == 4 ? ' selected' : '';?>>4</option>
                          <option value="5"<?php echo $t['rating-state'] == 5 ? ' selected' : '';?>>5</option>
                        </select>
                        <div class="rating-msg"></div>
                        <div class="rating-title"><?php echo $t['rating-average'];?><span>рейтинг</span></div>
                    </div>
                     <?php if ($t['extended']) { ?>
                    <a class="btn btn-primary" href="{chost}/<?php echo $t['uri'];?>/">подробнее</a>
                    <?php } else { ?>
                    <a class="btn btn-primary btn-disabled" href="#">подробнее</a>
                    <?php } ?>
                </div>
                <div class="col-md-6 s-places-info">
                    <?php if ($t['kitchen']) : ?>
                    <div class="kitchen"><b>Кухня</b>: <?php echo $t['kitchen'];?></div>
                    <?php endif; ?>
                    <?php if ($t['schedule']) : ?>
                    <div class="schedule"><b>График работы</b>: <?php echo $t['schedule'];?></div>
                    <?php endif; ?>
                    <?php if ($t['address']) : ?>
                    <div class="address"><b>Адрес</b>: <?php echo $t['address'];?></div>
                    <?php endif; ?>
                    <?php if ($t['phone']) : ?>
                    <div class="phone"><b>Телефон</b>: <?php echo $t['phone'];?></div>
                    <?php endif; ?>
                    <?php if ($t['site']) : ?>
                    <div class="site"><b>Сайт</b>: <?php echo '<a href="http://', $t['site'], '" target="_blank">', $t['site'], '</a>'; ?></div>
                    <?php endif; ?>
                    <?php if ($t['delivery']) : ?>
                    <?php if ($t['delivery-note']) { ?>
                    <div class="delivery"><b><?php echo $t['delivery-note'];?></b></div>
                    <?php } else { ?>
                    <div class="delivery"><b>Доставка</b></div>
                    <?php } ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
<?php
endfor;
?>
        </div>
        </div>
<?php
// пагинация
echo $paginationBottom;
?>
    </div>
</section>
<!-- /.list partners -->
<?php
// режим конструктора
echo $tpl['design-tag-close'];
?>