<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Форма регистрации пользователя (письмо уведомителю)"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('data-mail'))) return;

?>
<html>
<body>
    <div><strong>Имя: </strong><?php echo $tpl['profile_first_name'];?></div>
    <div><strong>Фамилия: </strong><?php echo $tpl['profile_last_name'];?></div>
    <div><strong>E-mail: </strong><?php echo $tpl['contact_email'];?></div>
    <div><strong>IP адрес: </strong><?php echo $tpl['user_account_ip'];?></div>
</body>
</html>