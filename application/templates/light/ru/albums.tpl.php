<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Список фотоальбомов"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('albums'))) return;

// пагинация
$pagination = '';
$paginationTop = '';
$paginationBottom = '';
if ($tpl['pagination']) {
    $pagination .= '<ul class="pagination pagination-sm">';
    foreach($tpl['pagination'] as $index => $item) {
        switch ($item['type']) {
            case 'first': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Первая страница"><span aria-hidden="true">&laquo;</span></a></li>'; break;
            case 'prev': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Предыдущая страница"><span aria-hidden="true">&lsaquo;</span></a></li>'; break;
            case 'more': $pagination .= '<li class="disabled"><a href="#">▪▪▪</a></li>'; break;
            case 'page': $pagination .= '<li><a href="' . $item['url'] . '">' . $item['page'] . '</a></li>'; break;
            case 'active': $pagination .= '<li class="active"><a href="#">' . $item['page'] . '</a></li>'; break;
            case 'next': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Следущая страница"><span aria-hidden="true">&rsaquo;</span></a></li>'; break;
            case 'last': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Последняя страница"><span aria-hidden="true">&raquo;</span></a></li>'; break;
        }
    }
    $pagination .= '</ul>';
    if ($tpl['pagination-pos'] == 'top' || $tpl['pagination-pos'] == 'both')
        $paginationTop = $pagination;
    if ($tpl['pagination-pos'] == 'bottom' || $tpl['pagination-pos'] == 'both')
        $paginationBottom = $pagination;
}
// режим конструктора
echo $tpl['design-tag-open'];

?>
<!--<div id="bg-banner-albums" class="banner"></div>-->
<h2 class="title tc">
		В кадре
	</h2>
<!-- list albums -->
<!--<section class="paralax-title section-img-title v-kadre-title">
	<div class="section-img-title-bg" style="background-image:url(/themes/light/images/v_kadre-bg-3.jpg);">
	</div>
	<h2 class="section-img-title-text">
		В кадре
	</h2>
</section>-->
<section class="photo list">
    <div class="container">
<?php if (!$tpl['use-calendar']) : ?>
        <div class="calendar">
            <span class="calendar-date"><?=$tpl['date-on'] ? GDate::format('d F', $tpl['date-on']['timestamp']) : 'Календарь';?></span>
            <a class="calendar-btn" data-date-format="yyyy-mm-dd" href="#"></a>
            <?php if ($tpl['date-on']) : ?>
            <a class="calendar-all" href="{chost}/albums/">все записи</a>
            <?php endif;?>
        </div>
<?php endif; ?>
<?=$paginationTop; ?>
        <div class="photo-body">
            <div class="row ">
<?php
if (!($count = $tpl['count']))
    echo '<div class="gear-page-public">Мы приносим свои извинения, но по вашему запросу нет записей на странице!</div>';
foreach ($tpl['items'] as $t) : $date = GDate::format('d F / l', $t['date']); ?>
                
				
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 media-card_wrap news-grid-item albums-page-item" >
					<a class="media-card media-card__height__small media-card__width__small media-card__shadowed" href="{chost}/albums/<?=$t['id'];?>/" >
						
						<div class="media-card_background" style="background-color:#5a4b92;background-image:url({chost}/data/albums/<?=$t['folder'], '/', $t['cover'];?>);"  ></div>
						<div class="media-card_label" >
							<div class="color-label" style="background-color:#ff5a43;" ><?=$date;?></div>
						</div>
						
						<div class="media-card_content">
							<h3 class="media-card_title" ><?=$t['name'];?></h3>
							
						</div>
					</a>
				</div>
<?php endforeach; ?>
            </div>
        </div>
<?=$paginationBottom;?>
    </div>

</section>
<!-- /list albums -->
<?=$tpl['design-tag-close'];?>

<?php if ($tpl['use-calendar']) : ?>
<script type="text/javascript">
    $(document).ready(function(){
        gear.calendarBs({
            selector: '.calendar-btn',
            language: '<?php echo $tpl['language'];?>',
            category: <?php echo $tpl['category-id'];?>,
            highlight: <?php echo $tpl['calendar-highlight'] ? 'true' : 'false';?>,
            dateOn: <?php echo $tpl['date-on'] ? '{year:' . $tpl['date-on']['year'] . ',month:' . ($tpl['date-on']['month'] - 1) . ',day:' . $tpl['date-on']['day'] . '}' : 'false'; ?>
        });
    });
</script>
<?php endif; ?>