<?php
/**
 * Gear CMS
 *
 * Шаблон компонента "Статья" (новости)
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('article'))) return;
?>
<section class="article">
    <div class="container">
        <div class="row row-offset">
            <div class="col-md-9">
                <article id="article-<?=$tpl['id'];?>" data-id="<?=$tpl['id'];?>" class="article">
<?=$tpl['design-tag-open'];?>
                    <div class="header">
<? if (!empty($tpl['header']) && $tpl['show-header']) : ?>
                        <h2><?=$tpl['header'];?></h2>
                        <div class="date"><?=date('d.m.Y', strtotime($tpl['published.date']));?> <?=date('H:i', strtotime($tpl['published.time']));?></div>
                        <div class="ya-share2" data-direction="horizontal" data-size ="m" data-services ="vkontakte,facebook,odnoklassniki,moimir,gplus,skype"></div>
<?php if ($tpl['image']) : ?>
                        <img src="<?=$tpl['image'];?>" />
<?php endif;endif; ?>
                    </div>

<?=$tpl['html'];?>
                    <div class="message vk">Вступайте в нашу группу <a href="https://vk.com/fridayz_ru/" target="_blank">ВКонтакте</a> и будьте в курсе всех событий</div>
<?=$tpl['design-tag-close'];?>
                </article>
                <div class="sidebar-bottom">
                    <h2>Читайте также</h2>
<?php
Gear::component('FeedNews', 'feed/news/', array('limit' => 6, 'category-id' => 2, 'data-random' => true, 'ellipsis-header' => 70, 'template' => 'sidebar_bottom_news.tpl.php'));
?>
                </div>
            </div>
            <div class="col-md-3">
                <div class="sidebar-right">
                    <h2 class="title">Новости</h2>
<?php
Gear::component('FeedNews', 'feed/news/', array('limit' => 10, 'category-id' => 2, 'category-default' => true, 'template' => 'sidebar_news.tpl.php'));
?>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
<script type="text/javascript" src="//yastatic.net/share2/share.js"></script>