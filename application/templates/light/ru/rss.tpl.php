<?php
/**
 * Gear CMS
 *
 * Шаблон компонента "RSS лента"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('rss'))) return;

echo '<?xml version="1.0"?>';
?>
<rss version="2.0">
<channel>
    <title>Fridayz - первый интернет-журнал Луганска</title>
    <link>http://<?php echo $_SERVER['SERVER_NAME'];?>/</link>
    <description>первый интернет-журнал Луганска, посвященный культурной и развлекательной жизни города, призванный предоставить всестороннюю и современную информацию о жизни общества. Проект посвящен светским мероприятиям, выставкам и концертам, премьерам и презентациям, а также другим культурным событиям, заслуживающим внимания СМИ и освещения в целях информирования молодой, активной части общества. На сайте можно найти широкий каталог заведений, со своей необходимой информацией о них. Проект Fridayz создан для максимально полного освещения столичной жизни не только посредством профессиональных фото-галерей, но и текстовых публикаций информационных материалов.</description>
    <language>ru-RU</language>
    <pubDate><?php echo date('D, d M Y H:i:s');?></pubDate>
    <lastBuildDate><?php echo date('D, d M Y H:i:s');?></lastBuildDate>
    <generator><?php echo $tpl['generator'];?></generator>
    <copyright><?php echo $_SERVER['SERVER_NAME'];?></copyright>
    <managingEditor>первый интернет-журнал Луганска, посвященный культурной и развлекательной жизни города, призванный предоставить всестороннюю и современную информацию о жизни общества. Проект посвящен светским мероприятиям, выставкам и концертам, премьерам и презентациям, а также другим культурным событиям, заслуживающим внимания СМИ и освещения в целях информирования молодой, активной части общества. На сайте можно найти широкий каталог заведений, со своей необходимой информацией о них. Проект Fridayz создан для максимально полного освещения столичной жизни не только посредством профессиональных фото-галерей, но и текстовых публикаций информационных материалов..</managingEditor>
    <webMaster>journal.fridayz@gmail.com</webMaster>
    <image>
        <url>http://<?php echo $_SERVER['SERVER_NAME'], '/themes/', $tpl['theme'];?>/images/logo.png</url>
        <title>Fridayz - первый интернет-журнал Луганска</title>
        <link>http://<?php echo $_SERVER['SERVER_NAME'];?>/</link>
    </image>
</channel>
<?php
$items = $tpl['articles'];
$count = sizeof($items);
for ($i = 0; $i < $count; $i++) :
?>
<item>
    <title><?php echo $items[$i]['title'];?></title>
    <link><?php echo $items[$i]['url'];?></link>
    <description><?php echo $items[$i]['description'];?></description>
    <author><?php echo $items[$i]['author'];?></author>
    <pubDate><?php echo $items[$i]['date'];?></pubDate>
</item>
<?php endfor; ?>
</rss>