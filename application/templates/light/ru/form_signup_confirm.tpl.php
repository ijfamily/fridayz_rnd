<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Подтверждение регистрации пользователя"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('form-signup-confirm'))) return;

Gear::doc(array('title' => 'Подтверждение регистрации пользователя'));

?>
<section class="s-form">
    <h2 class="title tc">Подтверждение регистрации пользователя</h2>
    <div>
<?php if ($tpl['is-confirm']) : ?>
    <div class="alert alert-success">Поздравляем, Вы успешно прошли регистрацию.</a></div>
<?php return; endif; ?>
<?php if (!$tpl['is-confirm']) : ?>
    <div class="alert alert-danger"><b>Ошибка!</b> Вы неправильно указали код подтверждения!</a></div>
<?php return; endif; ?>
    </div>
</section>