<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Слайдер"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('slider'))) return;

// режим конструктора
echo $tpl['design-tag-open'];
?>

<!-- slider -->
<section id="slider-wrapper">
    <div class="camera_wrap">
<?php
$dots = '';
$count = $tpl['count'];
for ($i = 0; $i < $count; $i++) :
    $t = $tpl['items'][$i];
?>
    <div data-src="<?php echo $tpl['dir'], $t['src'];?>">
<?php
if ($t['title'])
    echo '<h2><span>', $t['title'], '</span></h2>';
?>
<?php
if ($t['title-1'])
    echo '<h3><span>', $t['title-1'], '</span></h3>';
echo $t['text'];

// режим конструктора
if ($tpl['design-tag-control'])
    printf($tpl['design-tag-control'], $t['id']);
?>
    </div>
<? endfor; ?>
    </div>
</section>
<!-- /slider -->

<?php
// режим конструктора
echo $tpl['design-tag-close'];
?>