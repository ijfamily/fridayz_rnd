<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Список статей"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('list-articles'))) return;

// пагинация
$pagination = '';
$paginationTop = '';
$paginationBottom = '';
if ($tpl['pagination']) {
    $pagination .= '<ul class="pagination pagination-sm">';
    foreach($tpl['pagination'] as $index => $item) {
        switch ($item['type']) {
            case 'first': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Первая страница"><span aria-hidden="true">&laquo;</span></a></li>'; break;
            case 'prev': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Предыдущая страница"><span aria-hidden="true">&lsaquo;</span></a></li>'; break;
            case 'more': $pagination .= '<li class="disabled"><a href="#">▪▪▪</a></li>'; break;
            case 'page': $pagination .= '<li><a href="' . $item['url'] . '">' . $item['page'] . '</a></li>'; break;
            case 'active': $pagination .= '<li class="active"><a href="#">' . $item['page'] . '</a></li>'; break;
            case 'next': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Следущая страница"><span aria-hidden="true">&rsaquo;</span></a></li>'; break;
            case 'last': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Последняя страница"><span aria-hidden="true">&raquo;</span></a></li>'; break;
        }
    }
    $pagination .= '</ul>';
    if ($tpl['pagination-pos'] == 'top' || $tpl['pagination-pos'] == 'both')
        $paginationTop = $pagination;
    if ($tpl['pagination-pos'] == 'bottom' || $tpl['pagination-pos'] == 'both')
        $paginationBottom = $pagination;
}
// режим конструктора
echo $tpl['design-tag-open'];
?>

<!-- list news -->
<section class="news list">
	 <h2 class="title"><?=$tpl['category-name'];?></h2>
    <div class="container">
       
        <?=$paginationTop;?>
        <div class="news-body">
            <div class="row">
<?php if (!($count = $tpl['count'])) : ?>
                <div class="gear-page-public">Мы приносим свои извинения, но по вашему запросу нет записей на странице!</div>
<?php endif; ?>
                 
<?php
$date = $tpl['items'][0]['date'];
$first = true;
foreach ($tpl['items'] as $t) :
    if ($date != $t['date']) {
        $date = $t['date'];
        //echo '</div>';
        //echo '<div class="news-i">';
        $first = true;
    }
    if (!empty($t['plain']))
        $t['plain'] = GString::copyStr($t['plain'], 0, 400); $date = GDate::format('d F / l', $t['date']);
?>

					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 media-card_wrap news-grid-item" >
						<a class="media-card media-card__height__small media-card__width__small media-card__shadowed" href="<?=$t['url'];?>" >
							
							<div class="media-card_background" style="background-color:#5a4b92;background-image:url(<?php echo '', (empty($t['img']) ? 'no_image.jpg' : $t['img']);?>);"  ></div>
							<div class="media-card_label" >
								<div class="color-label" style="background-color:#ff5a43;" ><?=$date;?></div>
							</div>
							
							<div class="media-card_content">
								<h3 class="media-card_title" ><?=$t['title'];?></h3>
								
							</div>
						</a>
					</div>
                    
<?php $first = false; endforeach; ?>
               
            </div>
        </div>
        <?=$paginationBottom;?>
    </div>
</section>
<!-- /list news -->
<?=$tpl['design-tag-close'];?>
