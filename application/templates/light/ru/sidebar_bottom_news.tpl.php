<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Сайдбар статей"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('feed-news'))) return;

if (!($count = $tpl['count'])) return;

// режим конструктора
echo $tpl['design-tag-open'];

?>
<!-- sidebar news -->
<div class="sidebar-news bottom row">
<?php foreach ($tpl['items'] as $t) :  $date = GDate::format('d F / l', $t['date']);?>
    
 
		
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 media-card_wrap news-grid-item" >
			<a class="media-card media-card__height__small media-card__width__small media-card__shadowed" href="<?=$t['url'];?>" >
				
				<div class="media-card_background" style="background-color:#5a4b92;background-image:url(<?='', (empty($t['img']) ? '/data/images/no-image.jpg' : $t['img']);?>);"  ></div>
				<div class="media-card_label" >
					<div class="color-label" style="background-color:#ff5a43;" ><?=$date;?></div>
				</div>
				
				<div class="media-card_content">
					<h3 class="media-card_title" ><?=$t['title'];?></h3>
					
				</div>
			</a>
		</div>
		 
<?php endforeach; ?>
 
</div>
<!-- /sidebar news -->
<?=$tpl['design-tag-close'] ?>