<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Форма аторизации пользователя"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('form-signin'))) return;
?>
<!-- form signin -->
<div id="s-form-sign" class="s-form s-form-popup" style="display: none;">
    <h2 class="title enter">Вход</h2>
    <div class="s-form-wrap">
        <form  id="<?php echo $tpl['attr']['id'];?>" class="form-horizontal form-signin form-signin-popup" role="form" method="post" action="<?php echo $tpl['use-ajax'] ? '/request/signin/' : '#' . $tpl['attr']['id'];?>">
        <?php
        if (!$tpl['use-ajax']) {
            if ($tpl['message']) {
                if ($tpl['is-success'])
                    echo '<div class="alert alert-success">Спасибо за Ваше сообщение!</div>';
                else
                    echo '<div class="alert alert-danger"><strong>Ошибка!</strong> ', $tpl['message'], '</div>';
            } else
                echo '<div class="alert alert-success small">Чтобы войти на сайт <b>www.fridayz.ru</b>, пожалуйста, авторизуйтесь с вашим E-mail и паролем или через соцсети. </div>';
        }
        if ($tpl['use-ajax']) {
        ?>
        <?php
        }
        switch ($tpl['signin-type']) :
            case 'login': $p = 'логин'; break;
            case 'e-mail': $p = 'e-mail'; break;
            case 'login_or_e-mail': $p = 'логин или e-mail'; break;
        endswitch;
        ?>
            <input type="hidden" name="target" value="<?php echo $tpl['target'];?>"/>
            <input type="hidden" name="token" value="<?php echo $tpl['fields']['token'];?>"/>
            <div class="form-group">
                 <div class="col-sm-12">
                    <input type="text" class="form-control" id="name" name="name" maxlength="50" placeholder="Ваш <?php echo $p;?>" required />
                 </div>
            </div>
            <div class="form-group">
                 <div class="col-sm-12">
                    <input type="password" class="form-control" id="password" name="password" maxlength="20" placeholder="Пароль" value="" required />
                 </div>
            </div>
        <?php if ($tpl['attr']['check-captcha']) : ?>
            <div class="form-group">
                <div class="col-sm-12"><input type="text" class="form-control" name="code" style="width:135px;display:inline-block;" placeholder="Код на картинке" required /><img src="<?php echo $tpl['url-captcha'];?>" height="53px"/></div>
            </div>
        <?php endif; ?>
        <?php if ($tpl['remind-password']) : ?>
            <div class="form-group">
                <div class="col-sm-4"></div>
                <div class="col-sm-8"><a href="{chost}/remind-password/">Забыли пароль?</a></div>
            </div>
        <?php endif; ?>
            <div class="row">
                <div class="col-sm-12"><button type="submit" class="btn btn-primary">Войти</button></div>
            </div>
        </form>
        <?php if ($tpl['social-icons']) : ?>
            <div class="form-social">
                <div class="text">или с помощью аккаунта в соц. сетях</div>
                <div class="icons"><?php echo $tpl['social-icons'];?></div>
            </div>
        <?php endif; ?>
        <div class="form-subtitle">
            <a href="{chost}/signup/">Регистрация</a>
        </div>
        <?php
        // использовать проверку
        if ($tpl['attr']['use-scripts']) :
        ?>
        <script type="text/javascript">
            (function(w, n, t, id) {
                w[n] = w[n] || {}; w[n][id] = { id: id, ajax: <?php echo $tpl['use-ajax'] ? 'true' : 'false';?>, data: {}, valid: { name: t, css: true } };
            })(window, "gearForms", "formValidation", "<?php echo $tpl['attr']['id'];?>");
        </script>
        <?php endif; ?>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="{theme}/plugins/hybridauth/css/providers.min.css" />
<!-- /form signin -->