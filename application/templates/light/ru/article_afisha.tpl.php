<?php
/**
 * Gear CMS
 *
 * Шаблон компонента "Статья" ля вывода афиш
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('article'))) return;
?>
<article id="article-<?php echo $tpl['id'];?>" data-id="<?php echo $tpl['id'];?>" class="article article_afisha">
<?php
// режим конструктора
echo $tpl['design-tag-open'];

if (!empty($tpl['header']) && $tpl['show-header'])
    echo '<h2 class="title tc afisha-list">', $tpl['header'], '</h2>';
echo $tpl['html'];

// режим конструктора
echo $tpl['design-tag-close'];
?>
</article>