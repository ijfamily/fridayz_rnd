<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Список статей" (топ пипл)
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('list-articles'))) return;

// пагинация
$pagination = '';
$paginationTop = '';
$paginationBottom = '';
if ($tpl['pagination']) {
    $pagination .= '<ul class="pagination pagination-sm">';
    foreach($tpl['pagination'] as $index => $item) {
        switch ($item['type']) {
            case 'first': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Первая страница"><span aria-hidden="true">&laquo;</span></a></li>'; break;
            case 'prev': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Предыдущая страница"><span aria-hidden="true">&lsaquo;</span></a></li>'; break;
            case 'more': $pagination .= '<li class="disabled"><a href="#">▪▪▪</a></li>'; break;
            case 'page': $pagination .= '<li><a href="' . $item['url'] . '">' . $item['page'] . '</a></li>'; break;
            case 'active': $pagination .= '<li class="active"><a href="#">' . $item['page'] . '</a></li>'; break;
            case 'next': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Следущая страница"><span aria-hidden="true">&rsaquo;</span></a></li>'; break;
            case 'last': $pagination .= '<li><a href="' . $item['url'] . '" aria-label="Последняя страница"><span aria-hidden="true">&raquo;</span></a></li>'; break;
        }
    }
    $pagination .= '</ul>';
    if ($tpl['pagination-pos'] == 'top' || $tpl['pagination-pos'] == 'both')
        $paginationTop = $pagination;
    if ($tpl['pagination-pos'] == 'bottom' || $tpl['pagination-pos'] == 'both')
        $paginationBottom = $pagination;
}
// режим конструктора
echo $tpl['design-tag-open'];
?>
<div id="bg-banner-top-people" class="banner"></div>

<!-- list news -->
<section class="news list">
    <div class="container">
        <h2 class="title"><?=$tpl['category-name'];?></h2>
        <?=$paginationTop;?>
        <div class="news-body">
            <div class="row row-offset">
<?php if (!($count = $tpl['count'])) : ?>
                <div class="gear-page-public">Мы приносим свои извинения, но по вашему запросу нет записей на странице!</div>
<?php endif; ?>
                <div class="news-i">
<?php
$date = $tpl['items'][0]['date'];
$first = true;
foreach ($tpl['items'] as $t) :
    if ($date != $t['date']) {
        $date = $t['date'];
        echo '</div>';
        echo '<div class="news-i">';
        $first = true;
    }
    if (!empty($t['plain']))
        $t['plain'] = GString::copyStr($t['plain'], 0, 400);
?>
                    <div class="news-i-wrap">
                        <?php if ($first) : ?>
                        <div class="news-i-date">
                            <div class="news-i-day"><?=date('j', strtotime($t['date']));?></div>
                            <div class="news-i-month"><?=GDate::format('F', $t['date']);?></div>
                            <div class="news-i-weekday"><?=GDate::format('l', $t['date']);?></div>
                        </div>
                        <?php endif; ?>
                        <div class="news-i-info">
                            <a class="news-i-img" href="{chost}<?=$t['url'];?>"><img src="<?php echo $tpl['host'], (empty($t['img']) ? 'no_image.jpg' : $t['img']);?>" /></a>
                            <h3><a href="{chost}<?=$t['url'];?>"><?=$t['title'];?></a></h3>
                            <div class="news-i-text"><?=$t['text'];?></div>
                        </div>
                    </div>
<?php $first = false; endforeach; ?>
                </div>
            </div>
        </div>
        <?=$paginationBottom;?>
    </div>
</section>
<!-- /list news -->
<?=$tpl['design-tag-close'];?>
