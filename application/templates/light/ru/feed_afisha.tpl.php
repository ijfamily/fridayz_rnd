<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Лента новостей"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('gallery'))) return;

// режим конструктора
echo $tpl['design-tag-open'];

?>
<!-- feed afisha -->
<section class="afisha feed">
    <div class="container">
        <div class="afisha-body">
            <div id="afisha-slider">
<?php foreach ($tpl['items'] as $index => $t) : ?>
                <div>
                    <a class="afisha-i pirobox_gall" href="{host}/data/galleries/<?=$t['folder'], '/', $t['i']['file'];?>" title="" rel="gallery">
                        <div class="afisha-i-img"><img src="{host}/data/galleries/<?=$t['folder'], '/', $t['t']['file'];?>" /></div>
                    </a>
                </div>
<?php endforeach; ?>
            </div>
        </div>
    </div>
</section>
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>