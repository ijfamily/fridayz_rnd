<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Слайдер партнёров"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('partners-slider'))) return;

// режим конструктора
echo $tpl['design-tag-open'];

if ($tpl['count'] == 0) return;
//print_r($tpl['items']);
?>
<!-- Slider -->
<section class="main slider margin-bottom-40">
    <div class="container">
        <div id="main-slider" class="main-body">
<?php foreach($tpl['items'] as $t) : ?>
            
			<div class="slide" style="overflow:hidden;">
				<a href="<?=$t['uri'];?>/" class="slide-link-img">
					<img src="<?=$t['background'];?>" style="max-width: 100%;">
				</a> 
                 
            </div>
<?php endforeach; ?>
        </div>
    </div>
</section>
<!-- /slider -->
