<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Лента новостей"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('feed-news'))) return;

if (!($count = $tpl['count'])) return;

// режим конструктора
echo $tpl['design-tag-open'];

$t = $tpl['items'][0];
if (!empty($t['plain']))
    $t['plain'] = GString::copyStr($t['plain'], 0, 400);
?>
<!-- feed news -->
<div class="col-md-4 col-sm-6">
    <h2 class="title"><?=$tpl['title'];?></h2>
    <section class="news feature border">
        <div class="feature-img"><img src="<?=$tpl['host'], (empty($t['img']) ? '/data/images/no-image.jpg' : $t['img']);?>" /></div>
        <div class="feature-body">
            <div class="feature-header"><a href="{chost}<?=$t['url'];?>"><?=$t['title'];?></a></div>
            <div class="feature-datetime"><?=GDate::format('l, d F', $t['date']);?> <span class="author"></span></div>
            <div class="feature-text"><?=$t['plain'];?>...</div>
    </section>
</div>
<div class="col-md-4 col-sm-6">
    <section class="news feed border">
        <div class="news-body">
            <ul class="none news-items">
<?php for ($i = 1; $i < $count; $i++) : $t = $tpl['items'][$i]; ?>
                <li>
                    <div class="news-i-img"><img src="<?=$tpl['host'], (empty($t['img']) ? '/data/images/no-image.jpg' : $t['img']);?>" /></div>
                    <div class="news-i-header"><h3><a href="{chost}<?=$t['url'];?>"><?=$t['title'];?></a></h3></div>
<?php
    // режим конструктора
    if ($tpl['design-tag-control'])
        echo str_replace('%s', $t['id'], $tpl['design-tag-control']);
?>
                </li>
<?php endfor; ?>
            </ul>
        </div>
    </section>
</div>
<!-- /feed news -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>