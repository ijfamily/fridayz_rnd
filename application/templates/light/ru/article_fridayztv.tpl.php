<?php
/**
 * Gear CMS
 *
 * Шаблон компонента "Fridayz TV"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('article'))) return;
?>
<div id="bg-banner1-fridayztv" class="banner"></div>
<article id="article-<?php echo $tpl['id'];?>" data-id="<?php echo $tpl['id'];?>" class="article article-video">
    <div class="s-video">
<?php
// режим конструктора
echo $tpl['design-tag-open'];

if (!empty($tpl['header']) && $tpl['show-header'])
    echo '<div style="text-align:center;margin-bottom:10px;"><img src="{host}/data/images/fridayz-tv.png" title="Fridayz TV" alt="Fridayz TV"></div>';
$html = strip_tags($tpl['html']);
$items = explode(',', $html);
$index = 0;
foreach($items as $id) {
    if ($index == 0)
        echo _n, '<div class="row">', _n;
    $id = trim($id);
?>
    <div class="col-md-6">
        <iframe data-id="<?php echo $id;?>" width="100%" height="360" src="https://player.vimeo.com/video/<?php echo $id;?>" frameborder="0" webkitallowfullscreen="webkitallowfullscreen" mozallowfullscreen="mozallowfullscreen" allowfullscreen="allowfullscreen"></iframe>
        <div id="video<?php echo $id;?>" class="video-footer">
            <span class="video-footer-plays">
                <span class="icon"></span><span class="video-footer-count">0</span>
            </span>
        </div>
    </div>
<?php
    if ($index == 1) {
        echo '</div>';
        $index = -1;
    }
    $index++;
}

// режим конструктора
echo $tpl['design-tag-close'];
?>

    </div>
</article>