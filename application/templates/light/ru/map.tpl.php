<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Карта сайта"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('map'))) return;

// режим конструктора
echo $tpl['design-tag-open'];

?>
<!-- sitemap -->
<ul class="sitemap">
<?php
// категории статей
$items = $tpl['items']['cats'];
$count = sizeof($items);
for ($i = 0; $i < $count; $i++) :
    $t = $items[$i];
    if ($i == $count - 1)
        $tn = $t;
    else
        $tn = $items[$i + 1];
    if ($tn['level'] > $t['level']) {
        echo _t, '<li id="sitemap-item-', $t['id'], '" class="sitemap-item-i sitemap-item-', $t['id'], ($t['is-root'] ? ' sitemap-item-main' : ''), '">';
        if ($t['disabled'])
            echo '<span>', $t['name'], '</span>';
        else
            echo '<a href="{host}/', $t['url'], '">', $t['name'], '</a>';

        // режим конструктора
        if ($tpl['design-tag-control'])
            echo '<span style="float:left;margin-right:7px;">', str_replace('%s', $t['id'], $tpl['design-tag-control']), '</span>';

        echo _n_t, '<ul>', _2t, _n;
    } else {
        echo _t, '<li id="sitemap-item-', $t['id'], '" class="sitemap-item-i sitemap-item-', $t['id'], ($t['is-root'] ? ' sitemap-item-main' : ''), '">';
        if ($t['disabled'])
            echo $t['name'];
        else
            echo '<a href="{host}/', $t['url'], '">', $t['name'], '</a>';

        // режим конструктора
        if ($tpl['design-tag-control'])
            echo '<span style="float:left;margin-right:7px;">', str_replace('%s', $t['id'], $tpl['design-tag-control']), '</span>';
    
        // если есть статьи в категории
        if ($t['pages']) {
            echo _n_t, '<ul>', _2t, _n;
            for ($j = 0; $j < sizeof($t['pages']); $j++) {
                echo _t, '<li><a href="{host}/', $t['pages'][$j]['url'], '">', $t['pages'][$j]['name'], '</a>';

                // режим конструктора
                if ($tpl['design-tag-control'])
                    echo '<span style="float:left;margin-right:7px;">', str_replace('%s', $t['id'], $tpl['design-tag-control']), '</span>';

                echo '</li>', _n;
            
            }
            echo _t, '</ul>', _n;
        }
        echo _t, '</li>', _n;
    }
    if ($tn['level'] < $t['level'])
        echo _t, '</ul>', _n_t, '</li>', _n;
endfor;
$t = $items[$count - 1];
if ($t['level'] > 2)
    echo _t, str_repeat('</li>' . _n . '</ul>', $t['level'] - 2);

// статьи
$items = $tpl['items']['pages'];
$count = sizeof($items);
for ($i = 0; $i < $count; $i++) :
    $t = $items[$i];
    echo _t, '<li id="sitemap-item-', $t['id'], '" class="sitemap-item-i sitemap-item-', $t['id'], '">';
    if ($t['disabled'])
        echo $t['name'];
    else
        echo '<a href="{host}/', $t['url'], '">', $t['name'], '</a>';
    // режим конструктора
    if ($tpl['design-tag-control'])
        echo '<span style="float:left;margin-right:7px;">', str_replace('%s', $t['id'], $tpl['design-tag-control']), '</span>';
    echo '</li>', _n;
endfor;
?>
</ul>
<!-- /sitemap -->
<?php

// режим конструктора
echo $tpl['design-tag-close'];
?>