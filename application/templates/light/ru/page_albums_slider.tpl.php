<?php
/**
 * Gear CMS
 *
 * Шаблона компонента "Слайдер на странице фотоотчетов"
 */

defined('_TPL') or die('Restricted access');

if (!($tpl = &Gear::tpl('gallery'))) return;


//print_r($tpl);
?>
<!-- Slider -->
<section class="main slider margin-bottom-40">
    <div class="container">
        <div id="page-albums_main-slider" class="main-body">
<?php foreach($tpl['items'] as $t) : ?>
            
			<div class="slide" style="overflow:hidden;">
				<?if($t['t']['title']):?><a href="<?=$t['t']['title'];?>/" class="slide-link-img"><?endif;?>
				
					<img src="data/galleries/<?=$t['folder'], '/', $t['i']['file'];?>" style="max-width: 100%;" alt="<?=$t['title']?>" title="<?=$t['title']?>">
				<?if($t['t']['title']):?></a> <?endif;?>
                 
            </div>
<?php endforeach; ?>
        </div>
    </div>
</section>
<!-- /slider -->
