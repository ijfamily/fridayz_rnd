<?php
/**
 * Gear CMS
 *
 * Маршрутизация для компонента "Лайк материала" (для AJAX запроса)
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: like.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Маршрутизация для компонента "Лайк материала" (http://{host}/request/like/ или http://{host}/?request=like)
 * 
 * @params array $settings настройки маршрута
 * @params object $doc указатель на экземпляр класса документа
 * @params object $config указатель на экземпляр класса конфигурации
 * @return boolean
 */
function request_to_like($settings, $doc, $config)
{
    return GFactory::getRelease('jLike', 'like/');
}
?>