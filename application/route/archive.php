<?php
/**
 * Gear CMS
 *
 * Маршрутизация для компонента "Список статей в архиве"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: archive.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Маршрутизация для компонента "Список статей в архиве"
 * (http://{host}/{day}/{month}/{year} или http://{host}/?do=archive&date={date})
 * 
 * @params array $settings настройки маршрута
 * @params object $doc указатель на экземпляр класса документа
 * @params object $config указатель на экземпляр класса конфигурации
 * @return boolean
 */
function route_to_archive($settings, $doc, $config)
{
        // если ЧПУ
        if (Gear::$app->sef) {
            $path = Gear::$app->url->path;
        } else {
            if (Gear::$app->url->do != 'archive') return;
            $path = '/' . Gear::$app->url->getVar('date', '') . '/';
        }
        $allow = array(
            $config->get('LIST/ARCHIVE/DAY'),
            $config->get('LIST/ARCHIVE/MONTH'),
            $config->get('LIST/ARCHIVE/YEAR')
        );
        // проверка даты
        if (!Gear::$app->url->checkDate($allow, $path)) return false;

        $date = explode('/', trim($path, '/'));
        $text = GText::getSection('date');
        switch (sizeof($date)) {
            // year
            case 1: $title = GText::_('archive for year', 'interface', array($date[0])); break;
            // month/year
            case 2: $title = GText::_('archive for month,year', 'interface', array($text['month'][((int) $date[0]) - 1], $date[1])); break;
            // day/month/year
            case 3: $title = GText::_('archive for day,month,year', 'interface', array((int) $date[0], $text['monthBy'][(int) $date[1] - 1], $date[2])); break;
        }
        // данные для компонентов
        Gear::to('Breadcrumbs', 'items', array(
            array('title' => 'main', 'url' => '/'),
            array('title' => $title)
        ));
        Gear::to('ListArchive', 'date', $date);

        // мета на страницу
        $doc->header = $title;
        $doc->addMeta('keywords', $config->get('META/KEYWORDS') . ', ' . $title);
        $doc->addMeta('description', $config->get('META/DESCRIPTION') . ', ' . $title);

        // заменить компонент статьи на список
        $doc->component('Article', array(
            'class'      => 'ListArchive',
            'path'       => 'list/archive/',
            'template'   => $config->get('LIST/ARCHIVE/TEMPLATE'), // шаблон списка
            'list-limit' => $config->get('LIST/LIMIT'), // количество записей на странице (по умолчанию 0 )
            'list-order' => $config->get('LIST/ORDER'), // порядок сортировки (d - по убыванию, a - по возрастанию)
            'list-field' => $config->get('LIST/SORT'), // критерий сортировки (header - по алфавиту, date - по дате публикации)
            'list-subcategory'  => 1, // выводить записи опубликованные в субкатегориях
            'list-article-type' => 2 // выводить статьи по виду (0 - все, 1 - статья, 2 - новость)
        ));

    return true;
}
?>