<?php
/**
 * Gear CMS
 *
 * Маршрутизация для компонента "Изображения фотоальбома"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: albums.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Маршрутизация для компонента "Изображения фотоальбома" (http://{host}/albums/{id}/ или http://{host}/?do=albums&id={id})
 * 
 * @params array $settings настройки маршрута
 * @params object $doc указатель на экземпляр класса документа
 * @params object $config указатель на экземпляр класса конфигурации
 * @return boolean
 */
function route_to_albums($settings, $doc, $config)
{
    $config->load('Albums');

    // заменить компонент статьи на список
    $doc->component('Article', array(
        'class'      => 'AlbumImages',
        'path'       => 'list/albums/',
        'list-limit' => $config->getFrom('ALBUMS', 'LIST/LIMIT', 0), // количество записей на странице (по умолчанию 0 )
        'list-order' => $config->get('LIST/ORDER'), // порядок сортировки (d - по убыванию, a - по возрастанию)
        'list-field' => $config->get('LIST/SORT'), // критерий сортировки (header - по алфавиту, date - по дате публикации)
        'list-subcategory'  => 1, // выводить записи опубликованные в субкатегориях
        'list-article-type' => 2 // выводить статьи по виду (0 - все, 1 - статья, 2 - новость)
    ));

    return true;
}
?>