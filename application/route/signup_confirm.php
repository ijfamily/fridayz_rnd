<?php
/**
 * Gear CMS
 *
 * Маршрутизация для компонента "Форма регистрации пользователя"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: signup.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Маршрутизация для компонента "Форма регистрации пользователя" (http://{host}/signup/ или http://{host}/?do=signup)
 * 
 * @params array $settings настройки маршрута
 * @params object $doc указатель на экземпляр класса документа
 * @params object $config указатель на экземпляр класса конфигурации
 * @return boolean
 */
function route_to_signup_confirm($settings, $doc, $config)
{
    $config->load('Users');

    // если не резрешина авторизация и регистрация
    if (!$config->getFrom('USERS', 'SIGNUP/CONFIRM')) return false;

    $doc->component('Article', array(
        'id'       => 'form-signup-confirm',
        'class'    => 'FormSignUpConfirm',
        'path'     => 'form/signup/'
    ));

    return true;
}
?>