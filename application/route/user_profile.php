<?php
/**
 * Gear CMS
 *
 * Маршрутизация для компонента "Профиль пользователя"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: user_profile.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Маршрутизация для компонента "Профиль пользователя" (http://{host}/user/profile/ или http://{host}/?do=user_profile)
 * 
 * @params array $settings настройки маршрута
 * @params object $doc указатель на экземпляр класса документа
 * @params object $config указатель на экземпляр класса конфигурации
 * @return boolean
 */
function route_to_user_profile($settings, $doc, $config)
{
    $config->load('Users');

    // если не резрешина авторизация и регистрация
    if (!$config->getFrom('USERS', 'SIGNIN/ACCESS')) return false;

    // если пользователь не авторизирован
    if (!GFactory::getAuth()->check())
        Gear::redirect('/user/');

    // замена компонент статьи
    $doc->component('Article', array(
        'id'          => 'form-user-profile',
        'class'       => 'UserProfile',
        'path'        => 'user/profile/',
        'use-scripts' => true,
    ));

    return true;
}
?>