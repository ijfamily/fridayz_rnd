<?php
/**
 * Gear CMS
 *
 * Маршрутизация для компонента "Информация о пользователе"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: user.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Маршрутизация для компонента ""Информация о пользователе" (http://{host}/user/ или http://{host}/?do=user)
 * 
 * @params array $settings настройки маршрута
 * @params object $doc указатель на экземпляр класса документа
 * @params object $config указатель на экземпляр класса конфигурации
 * @return boolean
 */
function route_to_user($settings, $doc, $config)
{
    $config->load('Users');

    // если не резрешина авторизация и регистрация
    if (!$config->getFrom('USERS', 'SIGNIN/ACCESS')) return false;

    // замена компонент статьи
    $doc->component('Article', array(
        'class' => 'UserPage',
        'path'  => 'user/page/'
    ));

    return true;
}
?>