<?php
/**
 * Gear CMS
 *
 * Маршрутизация для компонента "Восстановление аккаунта пользователя"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: remind_password.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Маршрутизация для компонента "Восстановление аккаунта пользователя" (http://{host}/remind-password/ или http://{host}/?do=remind_password)
 * 
 * @params array $settings настройки маршрута
 * @params object $doc указатель на экземпляр класса документа
 * @params object $config указатель на экземпляр класса конфигурации
 * @return boolean
 */
function route_to_remind_password($settings, $doc, $config)
{
    $config->load('Users');

    // если не разрешино восстановление аккаунта
    if (!$config->getFrom('USERS', 'RESTORE')) return false;

    // замена компонент статьи
    $doc->component('Article', array(
        'id'    => 'form-remindpassword',
        'class' => 'FormRemindPassword',
        'path'  => 'form/remindpassword/',
        'use-scripts'   => true,
        'check-captcha' => true
    ));

    return true;
}
?>