<?php
/**
 * Gear CMS
 *
 * Маршрутизация для компонента "Партнёр"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: partner.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Маршрутизация для компонента "Партнёр" (http://{host}/{name}/ или http://{host}/?do=partner&name={name})
 * 
 * @params array $settings настройки маршрута
 * @params object $doc указатель на экземпляр класса документа
 * @params object $config указатель на экземпляр класса конфигурации
 * @return boolean
 */
function route_to_partner($settings, $doc, $config)
{
    
    if (!Gear::$app->url->is('', 'one path')) return;

    Gear::library('/Partners');

    // выбор данных новости по ее идент.
    GFactory::getDb()->connect();

    // если есть заведение партнёра
    if (($place = GPartners::getPlace(Gear::$app->url->getSeg(0))) === false) return;

    Gear::$app->category = GCategories::getById($place['category_id'], Gear::$app->domainId);

    // заменить компонент статьи на страницу заведения
    $doc->component('Article', array(
        'class' => 'Partner',
        'path'  => 'partners/partner/'
    ));

    return true;
}
?>