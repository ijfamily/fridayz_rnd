<?php
/**
 * Gear CMS
 *
 * Маршрутизация для компонента "Форма авторизации пользователя"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: signin.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Маршрутизация для компонента "Форма авторизации пользователя" (http://{host}/signin/ или http://{host}/?do=signin)
 * 
 * @params array $settings настройки маршрута
 * @params object $doc указатель на экземпляр класса документа
 * @params object $config указатель на экземпляр класса конфигурации
 * @return boolean
 */
function route_to_feedback($settings, $doc, $config)
{
    $doc->component('Article', array(
        'id'          => 'form-feedback',
        'class'       => 'FormFeedback',
        'path'        => 'form/feedback/',
        'check-captcha' => true,
        'use-scripts' => true
    ));

    return true;
}
?>