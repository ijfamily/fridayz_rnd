<?php
/**
 * Gear CMS
 *
 * Маршрутизация для компонента "Тег статьи"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: tag.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Маршрутизация для компонента "Тег статьи" (http://{host}/tag/{name}/ или http://{host}/?do=tag&name={name})
 * 
 * @params array $settings настройки маршрута
 * @params object $doc указатель на экземпляр класса документа
 * @params object $config указатель на экземпляр класса конфигурации
 * @return boolean
 */
function route_to_tag($settings, $doc, $config)
{
    if (!$config->get('ARTICLE/TAGS')) return false;

    // заменить компонент статьи на список
    $doc->component('Article', array(
        'class'      => 'ListTag',
        'path'       => 'list/tag/',
        'list-limit' => $config->get('LIST/LIMIT'), // количество записей на странице (по умолчанию 0 )
        'list-order' => $config->get('LIST/ORDER'), // порядок сортировки (d - по убыванию, a - по возрастанию)
        'list-field' => $config->get('LIST/SORT'), // критерий сортировки (header - по алфавиту, date - по дате публикации)
        'list-subcategory'  => 0, // выводить записи опубликованные в субкатегориях
        'list-article-type' => 0 // выводить статьи по виду (0 - все, 1 - статья, 2 - новость)
    ));

    return true;
}
?>