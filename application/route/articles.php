<?php
/**
 * Gear CMS
 *
 * Маршрутизация для компонента "Статья"
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: articles.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Маршрутизация для компонента "Статья" (http://{host}/articles/{id}/ или http://{host}/?do=articles&id={id})
 * 
 * @params array $settings настройки маршрута
 * @params object $doc указатель на объект документа
 * @return boolean
 */
function route_to_articles($settings, $doc, $config)
{
    // выбор данных статьи по ее идент.
    GFactory::getDb()->connect();
    Gear::$app->dataPage = GArticles::getById(Gear::$app->url->getSegId(1), Gear::$app->language->get('id'), 1);
    // определение категории статьи
    if (empty(Gear::$app->category)) {
         if (!empty(Gear::$app->dataPage['category_id']))
            Gear::$app->category = GCategories::getById(Gear::$app->dataPage['category_id'], Gear::$app->domainId);
    }

    return true;
}
?>