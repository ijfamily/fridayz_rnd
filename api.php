<?php
/**
 * Gear CMS
 *
 * Cron
 * классический демон-планировщик задач в UNIX-подобных операционных системах, использующийся для периодического 
 * выполнения заданий в определённое время
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Cron
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: cron.php 2016-01-01 12:00:00 Gear Magic $
 */

header('Access-Control-Allow-Origin: *');

$response = array(1, 2);
die(json_encode($response));
/*
header("Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE");
header("Access-Control-Max-Age: 86400"); // 24 Hours
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, x-auth-token");
*/
if (empty($_POST)) die();

$response = array(
    'success' => false
);

/**
 * Загрузка базовых настроек системы
 */
require('application/core/Gear.php');

// конфигурации системы
$config = GFactory::getConfig(PATH_CONFIG);

 

// ключ
$key = isset($_POST['key']) ? $_POST['key'] : false;
// название
$name = isset($_POST['name']) ? $_POST['name'] : false;
if (!$key || $name) {
    $response['message'] = 'undefined parametres';
    die(json_encode($response));
}

//
if ($key != 'fridayz12345') {
    $response['message'] = 'error key';
    die(json_encode($response));

}

switch ($name) {
    case 'all':
        $data = array(
            'content' => array(
                'afisha' => array(
                    'selector' => '#layer-afisha .layer-content',
                    'template' => '<div class="s-afisha list">' .
                                  '<div class="s-afisha-body">' .
                                  '{{each items}}' .
                                  '<a class="s-afisha-i pirobox_gall" href="${image}" title="" rel="gallery">' .
                                  '<div class="img"><img src="${thumb}" /></div>' .
                                  '</a>' .
                                  '{{/each}}' .
                                  '</div>' .
                                  '</div>',
                    'data' => array(
                        'items' => array(
                            array('image' => 'http://www.fridayz.ru/data/galleries/1477549157/58f90babaa5da.jpg',
                                  'thumb' => 'http://www.fridayz.ru/data/galleries/1477549157/58f90babaa5da_thumb.jpg'),
                            array('image' => 'http://www.fridayz.ru/data/galleries/1477549157/58f90ba3e7dfe.jpg',
                                  'thumb' => 'http://www.fridayz.ru/data/galleries/1477549157/58f90ba3e7dfe_thumb.jpg'),
                            array('image' => 'http://www.fridayz.ru/data/galleries/1477549157/58f90b8324569.jpg',
                                  'thumb' => 'http://www.fridayz.ru/data/galleries/1477549157/58f90b8324569_thumb.jpg')
                        )
                    )
                )
            )
        );

        $response['data'] = $data;
        break;

    default:
        $response['message'] = 'undefined name';
}

$response['success'] = true;
die(json_encode($response));
?>