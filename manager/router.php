<?php
/**
 * Gear Manager (modification for Gear CMS)
 *
 * Маршрутизатор запроса контроллера
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Router
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: router.php 2016-01-01 12:00:00 Gear Magic $
 */

require('core/Gear.php');

// проверка ключа безопасности
if (!Gear::checkKey()) {
    header('location: /error-404/');
    exit;
}

GException::$selfHandler = true;

$session = GFactory::getSession();
$session->start();

Gear::library('Settings');

// настройки пользователя
$settings = $session->get('user/settings');
// если есть отладка
if (!empty($settings['debug/php/errors'])) {
    error_reporting((int) $settings['debug/php/errors']);
}

GFactory::getController()->render();
?>