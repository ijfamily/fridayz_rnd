<?php
/**
 * Gear Manager (modification for Gear CMS)
 *
 * Главный сценарий
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Index
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: index.php 2016-01-01 12:00:00 Gear Magic $
 */

// простая проверка ключа безопасности
if (isset($_GET['key'])) {
    setcookie('key', $_GET['key'], 0, '/');
    $_COOKIE['key'] = $_GET['key'];
} else
    if (!isset($_COOKIE['key'])) {
        header('location: /error-404/');
        exit;
    }

/**
 * @see Gear
 */
require('core/Gear.php');

GException::$selfHandler = false;
// проверка ключа безопасности
if (!Gear::checkKey()) {
    header('location: /error-404/');
    exit;
}
$browser = GFactory::getBrowser(false, false);
// проверка на быстрое обновление
/*
if ($browser->isQuicklyRefresh()) {
     header('location: error?refresh');
     exit;
}
*/
// определение версии браузера
$isIE = $browser->isIE();
// инициализация конфигурации системы
$config = GFactory::getConfig();
$author = $config->get('AUTHOR');
// инициализация языка системы
$language = GLanguage::getInstance($config->get('LANGUAGE'), $config->get('LANGUAGES'));
$alias = $language->get('alias');
// версия
$core = GFactory::getClass('Version_Application', 'Version');
// инициализация хэша
$hash = GFactory::getHash();
$h = $hash->get();
// инициализация языка интерфейса
$_ = GText::getSection('interface');
// для восстановления учетной записи пользователя
$restore = isset($_GET['restore']) ? $_GET['restore'] : '';
$invisible = isset($_GET['invisible']) ? $_GET['invisible'] : '';
?>
<!DOCTYPE html>
<html lang="<?=$alias;?>">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="copyright" content="Gear Magic"/>
    <meta name="robots" content="none"/>
    <link rel="stylesheet" type="text/css" href="resources/js/extjs/ux/css/ux-all.css" />
    <link rel="stylesheet" type="text/css" href="resources/themes/green/signin.css" />
    <link rel="stylesheet" type="text/css" href="resources/css/animate.min.css" />
    <!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="resources/css/logon-ie.css" />
    <![endif]-->
    <link rel="shortcut icon" type="image/x-icon" href="resources/themes/green/images/favicon.ico" />

    <script type="text/javascript" src="resources/js/extjs/ext-core.js"></script>
    <script type="text/javascript">Ext.ROUTER_DELIMITER = "<?=ROUTER_DELIMITER;?>";</script>
    <script type="text/javascript" src="resources/js/manager/notice.min.js"></script>
    <script type="text/javascript" src="resources/js/manager/signin.min.js"></script>
    <script type="text/javascript" src="resources/js/vendors/wow.min.js"></script>
    <script type="text/javascript" src="resources/js/vendors/md5.min.js"></script>
    <script type="text/javascript" src="resources/locale/a-lang-<?=$alias;?>.min.js"></script>
    <title><?=$_['Application name'];?></title>
</head>

<body>
    <div class="loading"></div>
    <div id="mask"><div></div></div>
    <div class="wrapper">
        <div class="notify">
            <div class="notice">
                <span class="notice-icon"></span>
                <div></div>
            </div>
        </div>
        <div class="form wow flipInX animated">
            <div class="title logo"></div>
            <form method="post" onsubmit="return false" id="authorize-form">
                <input type="hidden" name="hash" value="<?=$h['hash'];?>" />
                <input type="hidden" name="token" value="<?=$h['token'];?>" />
                <input type="hidden" id="language" name="language" value="<?=$alias;?>" />
                <input type="hidden" name="restore" value="<?=$restore;?>" />
                <?php if ($invisible) : ?>
                <input type="hidden" name="invisible" value="<?=$invisible;?>" />
                <?php endif; ?>
                <fieldset>
                    <label><?=$_['User name'];?></label>
                    <div class="field field-login"><input type="text" id="username" name="username" tabindex="1" placeholder="<?=$_['Enter the user name'];?>" /></div>
                </fieldset>
                <fieldset>
                    <label><?=$_['Password'];?></label>
                    <div class="field field-password"><input type="password" id="password" name="password" tabindex="2"  placeholder="<?=$_['Enter the account password'];?>" /></div>
                </fieldset>
                <div class="buttons">
                    <a class="btn-sm btn-info" href="http://cms.gearmagic.ru/" target="_blank" title="<?=$_['Help'];?>"></a>
                    <?php if ($config->get('LOGON/RESTORE_ACCOUNT')) : ?>
                    <a class="btn-sm btn-recovery" href="/<?=PATH_APPLICATION;?>recovery" title="<?=$_['Title recovery account'];?>"></a>
                    <?php endif; ?>
                    <a class="btn-big btn-site" href="/" title="<?=$_['To site'];?>"></a>
                    <a class="btn-big btn-apply" id="authorize-submit" href="#" title="<?=$_['Sign in'];?>"></a>
                </div>
            </form>
            <?php if ($isIE) : ?>
            <div class="disabled"></div>
            <?php endif; ?>
        </div>
        <div class="langs">
            <ul><?php foreach ($config->get('LANGUAGES') as $key => $value) : ?>
                <li><a href="?language=<?=$key;?>" lang="<?=$key;?>" title="<?=$value['title'];?>"><?=$value['name'];?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>

    <div class="footer">
        <ul class="social">
            <li><a class="icon icon-twitter" href="#" title="Twitter" onclick="javascript:window.open('<?=$author['Twitter'];?>',null,null);"></a></li>
            <li><a class="icon icon-in" href="#" title="LinkedIn" onclick="javascript:window.open('<?=$author['LinkedIn'];?>',null,null);"></a></li>
            <li><a class="icon icon-google" href="#" title="Google +" onclick="javascript:window.open('<?=$author['Google +'];?>',null,null);"></a></li>
            <li><a class="icon icon-vk" href="#" title="ВКонтакте" onclick="javascript:window.open('http://vk.com/gearmagic',null,null);"></a></li>
            <li><a class="icon icon-skype" href="skype:<?=$author['Skype'];?>" title="Skype"></a></li>
        </ul>
        <span class="copyright">Copyright © 2011-2016 <a target="_blank" href="http://gearmagic.ru/">Gear Magic</a> <?=$_['All right reserved'];?></span>
        <span class="version"><?php printf($_['version %s'], $core->getVersionAlias());?></span>
    </div>
    <?php if ($isIE) : ?>
    <script type="text/javascript">Ext.onReady(function(){ Ext.Notice.show('error', '<?=$_['Browser error'];?>', false); });</script>
    <?php endif; ?>
</body>
</html>