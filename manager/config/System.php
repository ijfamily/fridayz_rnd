<?php
/**
 * Gear Manager
 *
 * Файл конфигурации системы
 * 
 * LICENSE
 * 
 * Distributed under the Lesser General Public License (LGPL)
 * http://www.gnu.org/copyleft/lesser.html
 *
 * @category   Gear
 * @package    Config
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: System.php 2016-01-01 12:00:00  Gear Magic $
 */

return array(
    // ключ безопасности
    'SECURITY_KEY'      => 'day',
    // ip адрес
    'SERVER/IP_ADDRESS' => '127.0.0.1',
    // имя хоста (www.simple.com,simple.com)
    'SERVER/NAME'       => array('rnd.fridayz.ru', 'www.rnd.fridayz.ru'),
    // веб сервер
    'SERVER/WEB'        => 'apache',
    // используемые языки
    'LANGUAGES' => array(
        'ru-RU' => array('id' => 1, 'alias' => 'ru-RU', 'name' => 'Русский', 'title' => 'Русский - Russian'),
        'uk-UA' => array('id' => 2, 'alias' => 'uk-UA', 'name' => 'Українська', 'title' => 'Українська - Ukrainia'),
        'en-GB' => array('id' => 3, 'alias' => 'en-GB', 'name' => 'English (United Kingdom)', 'title' => 'English (United Kingdom)')
    ),
    // E-mail сервера (то кем будут подписаны письма отправленные сервером)
    'MAIL/HOST'         => 'noreply@fridayz.ru',
    // кому будут отправляться письма от клиентов (подписываться MAIL/HOST)
    'MAIL/ADMIN'        => 'fridayz@gmail.com',
    // справочники
    'REFERENCES' => array(
    ),
    // подключения к серверам баз данных
    'DB' => array(
        'default' => array(
            // dns имя сервера базы данных или ip адрес (127.0.0.1 или localhost)
            'SERVER'   => 'localhost',
            // схема базы данных
            'SCHEMA'   => 'fridayz_rnd',
            // логин для подключения к серверу базы данных
            'USERNAME' => 'fridayz_rnd',
            // пароль для подключения к серверу базы данных
            'PASSWORD' => 'gZF65c^D#Rsd',
            // порт подключения к серверу, по умолчанию (3306)
            'PORT'     => 3306,
            // кодировка базы данных
            'CHARSET'  => 'utf8'
        )
    )
);
?>