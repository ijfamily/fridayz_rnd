<?php
/**
 * Gear Manager
 *
 * Файл конфигурации капчи
 *
 * LICENSE
 * 
 * Distributed under the Lesser General Public License (LGPL)
 * http://www.gnu.org/copyleft/lesser.html
 *
 * @category   Gear
 * @package    Config
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Captcha.php 2016-01-01 12:00:00  Gear Magic $
 */

return array(
    // JPEG quality of CAPTCHA image (bigger is better quality, but larger file size)
    'jpegQuality' => 90,
    // CAPTCHA image colors (RGB, 0-255)
    'foregroundColor' => array(/*mt_rand(0,80), mt_rand(0,80), mt_rand(0,80)*/255, 255, 255),
    'backgroundColor' => array(228, 234, 237),
    //  set to false to remove credits line. Credits adds 12 pixels to image height
    'showСredits' => false,
    'credits'     => $_SERVER['SERVER_NAME'],
    // increase safety by prevention of spaces between symbols
    'noSpaces' => true,
    // folder with fonts
    'fontsDir' => 'fonts',
    // CAPTCHA image size (you do not need to change it, this parameters is optimal)
    'width'  => 150,
    'height' => 70,
    // CAPTCHA string length random 5 or 6 or 7
    'length' => mt_rand(5, 7),
    // KCAPTCHA configuration file (do not change without changing font files!)
    'alphabet' => "0123456789abcdefghijklmnopqrstuvwxyz",
    // symbols used to draw CAPTCHA (alphabet without similar symbols (o=0, 1=l, i=j, t=f))
    'allowedSymbols' => "23456789abcdegikpqsvxyz",
    // symbol's vertical fluctuation amplitude
    'fluctuationAmplitude' => 8,
    // noise no white noise
    'whiteNoiseDensity' => 1/6,
    // noise no white noise no black noise
    'blackNoiseDensity' => 1/30,
    // type file output
    'type' => 'PNG'
);
?>