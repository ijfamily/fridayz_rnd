/**
 * plugin.js
 *
 * Image
 * 
* @author    Gear Magic
* @copyright (c) 2015, by Gear Magic, gearmagic.ru@gmail.com
* @date      Oct 01, 2015
* @version   $Id: 0.1 $
 */

/**
 * @author gearmagic.ru@gmail.com
 * @class Manager.widget.TinyMCE.ImageWindow
 * @extends Ext.Window
 * Окно вставки изображения
 */
Manager.widget.TinyMCE.ImageWindow = Ext.extend(Ext.Window, {
    iconCls: 'x-wnd-image',
    closable: true,
    title: 'Вставка изображения в текст',
    width: 700,
    autoHeight: true,
    resizable: false,
    modal: true,
    layout: 'fit',
    bodyStyle: 'padding:5px;',
    buttonAlign: 'right',

    alignList: [],
    tagImg: null,
    // если файл был загружен
    fileUploaded: false,
    // название ранее загруженного файла
    filename: '',
    editor: null,

    // сбросить поля
    reset: function(){
        this.tagImgAttr = {src: '', width: '', height: '', 'class': '', style: '', alt: '', align: ''};
    },

    // вставка изображения
    doInsert: function(){
        var frm = this.formPanel.getForm();
        if (frm.isValid()) {
            var attr = '', value = '', name = '', src = '';
            frm.items.each(function(f){
                if (typeof f.getName() != 'undefined') {
                    name = f.getName();
                    if (typeof this.tagImgAttr[name] != 'undefined') {
                        value = f.getValue();
                        if (value.length > 0) {
                            attr += ' ' + name + '="' + value + '"';
                            if (name == 'alt')
                                attr += ' title="' + value + '"';
                        }
                    }
                    if (name == 'src') src = value;
                    if (name == 'zoom')
                        if (f.checked) {
                            var type = Ext.getCmp('fldAttrImageType');
                            if (type.isVisible())
                                attr += 'data-zoom=true data-zoom-src="' + type.getDefImgSrc() + '"';
                            else
                                attr += 'data-zoom=true data-zoom-src="' + src + '"';
                        }
                }
            }, this);

            this.editor.insertContent('<img' + attr + '>&nbsp;');
            frm.reset();
            this.close();
            this.reset();
        }
    },

    // обновить изображение
    doUpdate: function(){
        var frm = this.formPanel.getForm();
        if (frm.isValid()) {
            var img = Ext.getDom(this.tagImg);
            var value = '', name = '', src = '', pt = [];
            frm.items.each(function(f){
                if (typeof f.getName() != 'undefined') {
                    name = f.getName();
                    if (typeof this.tagImgAttr[name] != 'undefined') {
                        value = f.getValue();
                        if (value.length > 0) {
                            img.setAttribute(name, value);
                            if (name == 'alt')
                                img.setAttribute('title', value);
                        } else {
                            if (img.getAttribute(name) != null)
                                img.setAttribute(name, '');
                        }
                    }
                    if (name == 'src') src = value;
                    if (name == 'zoom') {
                        if (f.checked) {
                            img.setAttribute('data-zoom', true);
                            pt = src.split('_thumb');
                            if (pt.length > 1)
                                img.setAttribute('data-zoom-src', pt[0] + pt[1]);
                            else {
                                pt = src.split('_medium');
                                if (pt.length > 1)
                                    img.setAttribute('data-zoom-src', pt[0] + pt[1]);
                                else
                                    img.setAttribute('data-zoom-src', src);
                            }
                        } else {
                            img.removeAttribute('data-zoom');
                            img.removeAttribute('data-zoom-src');
                        }
                    }
                }
            }, this);
            img.setAttribute('data-mce-src', src);
            frm.reset();
            this.close();
            this.reset();
        }
    },

    // удаление файла
    doDelete: function(fileName, action){
        var self = this;
        Ext.Ajax.request({
            method: App.restful ? 'DELETE' : 'POST',
            url: this.editor.settings.uploadUrl,
            params: { method: 'DELETE', file_name: fileName, file_type: 'image' },
            failure: function(r, o){ Ext.Msg.failure(r); },
            success: function(r, o){
                var r = Ext.util.JSON.decode(r.responseText);
                if (r.success) {
                    App.setAlert(r.status, r.message);
                    if (action == 'close')
                        self.close();
                    else
                    if (action == 'upload')
                        self.doUpload();
                } else
                    Ext.Msg.error(r);
            }
        });
    },

    // удаление файла
    doDeleteFile: function(){
        var self = this;
        Ext.Ajax.request({
            method: App.restful ? 'DELETE' : 'POST',
            url: this.editor.settings.uploadUrl,
            params: { method: 'DELETE', file_name: self.filename, file_type: 'image' },
            failure: function(r, o){ Ext.Msg.failure(r); },
            success: function(r, o){
                var r = Ext.util.JSON.decode(r.responseText);
                if (r.success) {
                    App.setAlert(r.status, r.message);
                    Ext.get(self.tagImg).remove();
                    self.close();
                } else
                    Ext.Msg.error(r);
            }
        });
    },

    // вырезать изображение
    doErase: function(){
        Ext.get(this.tagImg).remove();
        this.close();
    },

    // загрузка изображения на сервер
    doUpload: function () {
        Ext.Msg.progress(Ext.Msg.T.loading, Ext.Msg.M.progress);
        var f = this.formPanel.getForm(),
            self = this;
        Ext.Ajax.request({
            url: this.editor.settings.uploadUrl,
            form: f.id,
            params: {method: 'POST'},
            failure: function(r, o){ Ext.Msg.failure(r); },
            success: function(r, o) {
                Ext.Msg.hide();
                var r = Ext.util.JSON.decode(r.responseText);
                if (r.success) {
                    if (r.url.length) {
                        Ext.getCmp('fldAttrImageSrc').setValue(r.url);
                        self.filename = r.filename;
                        self.fileUploaded = true;
                    }
                    App.setAlert(r.status, r.message);
               } else {
                    Ext.Msg.error(r);
               }
            }
        });
    },

    // private
    initComponent:function(){
        this.reset();
        if (this.tagImg != null) {
            this.title = 'Правка изображения в тексте';
            var attr;
            for (i = 0; i < this.tagImg.attributes.length; i++) {
                attr = this.tagImg.attributes[i];
                if (typeof this.tagImgAttr[attr.name] != undefined)
                    this.tagImgAttr[attr.name] = attr.value;
            }
            this.filename = this.tagImgAttr.src;

            if (typeof this.tagImgAttr['data-zoom'] != undefined)
                this.tagImgAttr['data-zoom'] = this.tagImgAttr['data-zoom'] ? true : false;
            else
                this.tagImgAttr['data-zoom'] = false;
        }

        // вкладки
        var self = this, fldAttrImage = null;
        this.formPanel = new Ext.FormPanel({
            fileUpload: true,
            labelWidth: 80,
            baseCls: 'x-plain',
            items: [{
                xtype: 'displayfield',
                hideLabel: true,
                anchor: '100%',
                value: '<div id="fldAttrImage" class="x-field-image-bg"></div>'
            }, {
                xtype: 'tabpanel',
                anchor: '100%',
                height: 310,
                activeTab: 0,
                items: [{
                    title: 'Атрибуты',
                    layout: 'form',
                    baseCls: 'mn-form-tab-body',
                    labelWidth: 90,
                    items: [{
                        xtype: 'mn-field-browse',
                        triggerWdgUrl: 'site/media/' + Ext.ROUTER_DELIMITER + 'dialog/interface/',
                        params: 'view=images',
                        triggerWdgSetTo: true,
                        fieldLabel: 'URL ресурс',
                        allowBlank: false,
                        anchor: '100%',
                        id: 'fldAttrImageSrc',
                        value: this.tagImgAttr.src,
                        name: 'src',
                        listeners: {
                            valid: function(fld){
                                if (fldAttrImage == null)
                                    fldAttrImage = Ext.get('fldAttrImage');
                                fldAttrImage.dom.style.backgroundImage = 'url(' + fld.getValue() + ')';
                            }
                        }
                    }, {
                        xtype: 'textfield',
                        fieldLabel: 'Загаловок',
                        tooltip: 'Атрибут изображения подставляемый в &lt;img&gt; и &lt;title&gt;',
                        allowBlank: true,
                        anchor: '100%',
                        id: 'fldAttrImageAlt',
                        value:  this.tagImgAttr.alt,
                        name: 'alt'
                    }, {
                        xtype: 'textfield',
                        fieldLabel: 'CSS класс',
                        allowBlank: true,
                        width: 200,
                        id: 'fldAttrImageClass',
                        value:  this.tagImgAttr['class'],
                        name: 'class'
                    }, {
                        xtype: 'textfield',
                        fieldLabel: 'CSS стиль',
                        allowBlank: true,
                        width: 200,
                        id: 'fldAttrImageStyle',
                        value:  this.tagImgAttr.style,
                        name: 'style'
                    }, {
                        xtype: 'combo',
                        fieldLabel: 'Выравнивание',
                        editable: false,
                        width: 200,
                        typeAhead: false,
                        triggerAction: 'all',
                        mode: 'local',
                        store: {
                            xtype: 'arraystore',
                            fields: ['name', 'value'],
                            data: this.alignList
                        },
                        name: 'align',
                        value: this.tagImgAttr.align,
                        hiddenName: 'align',
                        valueField: 'value',
                        displayField: 'name',
                        allowBlank: true
                    }, {
                        xtype: 'mn-btn-selecttypeimg',
                        id: 'fldAttrImageType',
                        hidden: true,
                        width: 200,
                        fieldLabel: 'Вставить',
                        name: 'select'
                    }, {
                        xtype: 'checkbox',
                        id: 'fldZoom',
                        fieldLabel: 'Увеличить<hb></hb>',
                        labelTip: 'Увеличивать изображение при клике на его миниатюру',
                        checked: this.tagImgAttr['data-zoom'],
                        name: 'zoom'
                    }, {
                        xtype: 'fieldset',
                        labelWidth: 70,
                        title: 'Размеры изображения',
                        autoHeight: true,
                        items: [{
                            xtype: 'textfield',
                            fieldLabel: 'Ширина',
                            allowBlank: true,
                            width: 100,
                            id: 'fldAttrImageWidth',
                            value: this.tagImgAttr.width,
                            name: 'width'
                        }, {
                            xtype: 'textfield',
                            fieldLabel: 'Высота',
                            allowBlank: true,
                            width: 100,
                            id: 'fldAttrImageHeight',
                            value:  this.tagImgAttr.height,
                            name: 'height'
                        }]
                    }]
                }, {
                    title: 'Загрузка на сервер',
                    layout: 'form',
                    baseCls: 'mn-form-tab-body',
                    labelWidth: 190,
                    align: 'right',
                    items: [{
                        xtype: 'mn-field-upload',
                        buttonText: '...',
                        width: '100%',
                        name: 'file_name'
                    }, {
                        xtype: 'hidden',
                        name: 'file_type',
                        value: 'image'
                    }, {
                        xtype: 'mn-field-chbox',
                        fieldLabel: 'сгенерировать название файла',
                        'default': 0,
                        name: 'file_generate'
                    }, {
                        xtype: 'button',
                        text: 'Загрузить на сервер',
                        handler: function(){
                            if (self.fileUploaded) {
                                Ext.Msg.confirm(Ext.Msg.T.confirm, 'Удалить загруженный ранее файл?',
                                    function(btn){
                                        if (btn == 'yes')
                                            self.doDelete(self.filename, 'upload');
                                        else
                                            self.doUpload();
                                }, this);
                            } else
                                self.doUpload();
                        }
                    }]
                }]
            }]
        });
        this.items = [this.formPanel];
        this.buttons = [{
            text: 'Добавить',
            tooltip: 'Добавить изображение в текст',
            hidden: !(this.tagImg == null),
            handler: this.doInsert,
            iconCls: 'icon-fbtn-insert',
            scope: this
        }, {
            text: 'Изменить',
            tooltip: 'Изменить изображение в тексте',
            hidden: this.tagImg == null,
            handler: this.doUpdate,
            iconCls: 'icon-fbtn-update',
            scope: this
        }, {
            text: 'Вырезать',
            tooltip: 'Вырезать изображение из текста',
            hidden: this.tagImg == null,
            handler: this.doErase,
            iconCls: 'icon-fbtn-erase',
            scope: this
        }, {
            text: 'Удалить',
            tooltip: 'Удалить файл на сервере и вырезать изображение из текста',
            hidden: this.tagImg == null,
            handler: this.doDeleteFile,
            iconCls: 'icon-fbtn-delete',
            scope: this
        }, {
            text: 'Отмена',
            scope: this,
            handler: function(){
                if (this.fileUploaded) {
                    Ext.Msg.confirm(Ext.Msg.T.confirm, 'Удалить загруженный ранее файл?',
                        function(btn){
                            if (btn == 'yes')
                                this.doDelete(self.filename, 'close');
                            else
                                this.close();
                    }, this);
                } else
                    this.close();
                this.reset();
            }
        }];

        Manager.widget.TinyMCE.ImageWindow.superclass.initComponent.call(this);
    }
});


tinymce.PluginManager.add('mimage', function(editor, url){
    var alignList = [
        ['неизвестно', ''], ['по левому краю', 'left'], ['по правому краю', 'right'], 
        ['по верхнему краю', 'top'], ['по нижниму краю', 'bottom'], ['по центру', 'middle']
    ];

    editor.addButton('mimage', {
        icon: 'image',
        tooltip: 'Insert/edit image',
        onclick: function () {
            var wnd = new Manager.widget.TinyMCE.ImageWindow({ editor: editor, alignList: alignList });
            wnd.show();
        },
        stateSelector: 'img:not([data-mce-object],[data-mce-placeholder])'
    });

    editor.addMenuItem('mimage', {
        icon: 'image',
        text: 'Insert/edit image',
        onclick: function () {
            var wnd, el = editor.selection.getNode();
            if (el != null) {
                if (el.tagName == 'IMG')
                    wnd = new Manager.widget.TinyMCE.ImageWindow({ editor: editor, tagImg: el, alignList: alignList });
                else
                    wnd = new Manager.widget.TinyMCE.ImageWindow({ editor: editor, alignList: alignList });
                wnd.show();
            }
        },
        context: 'insert',
        prependToContext: true
    });

    editor.addCommand('mceImage', function () {});
});