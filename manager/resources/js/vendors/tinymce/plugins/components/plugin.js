/**
 * plugin.js
 *
 * Site components
 * 
* @author    Gear Magic
* @copyright (c) 2015, by Gear Magic, gearmagic.ru@gmail.com
* @date      Oct 01, 2015
* @version   $Id: 0.1 $
 */

tinymce.PluginManager.add('components', function(editor, url){
    var menuItems = [];
    if (editor.settings.components.length == 0) return;

    tinymce.each(editor.settings.components, function(item){
        item.icon = 'component';
        item.image = editor.settings.componentIcons + item.image;
        item.onclick = function (me) {
            Manager.widget.load({
                url: Manager.COMPONENTS_URL 
                  + 'site/articles/articles/~/component/interface/?to=editor&editor=' + editor.id 
                  + '&cClass=' + me.control.settings.cmpConfig.className
                  + '&language=' + editor.settings.languageId
            });
        };
        menuItems.push(item);
    });

    editor.addButton('components', {
        type: "menubutton",
        image: 'resources/js/vendors/tinymce/plugins/components/img/icon.png',
        menu: menuItems,
        tooltip: 'Компоненты'
    });
});