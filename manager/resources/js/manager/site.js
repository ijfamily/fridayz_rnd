/**
* Manager
* Site
* 
* @author    Gear Magic
* @copyright (c) 2015, by Gear Magic, gearmagic.ru@gmail.com
* @date      Jun 01, 2015
* @version   $Id: 1.0 $
*
* @license shell.js is licensed under the terms of the Open Source
* LGPL 3.0 license. Commercial use is permitted to the extent that the
* code/component(s) do NOT become part of another Open Source or Commercially
* licensed development library or toolkit without explicit permission.
*
* License details: http://www.gnu.org/licenses/lgpl.html
*/

// создание пространства имен
Ext.ns('Manager.site');

var Site = {
    load: function(url) {
        Manager.widget.load({ url: Manager.COMPONENTS_URL + url });
    },
    design: {
        component: null,
        menu: null,
        menuItems: null,
        frameId: null,
        hideMenu: function(){
            this.menu.hide();
            this.menuItems.hide();
            this.menuProps.hide();
        },
        showMenu: function(pos, type, settings, recId, component){
            var st = Ext.util.JSON.decode(settings),
                posFrame = Ext.get(this.frameId).getXY();
            this.component = component;
            switch (type) {
                case 'control':
                    if (this.menu == null) return;
                    alert('ok');
                    this.menu.updateItem('add', st.add, 0, recId);
                    this.menu.updateItem('edit', st.edit, 1, recId);
                    this.menu.updateItem('list', st.list, 3, recId);
                    this.menu.updateItem('property', st.property, 5, recId);
                    this.menu.showAt([pos[0] + posFrame[0], pos[1] + posFrame[1]]);
                    break;

                case 'item':
                    if (this.menuItems == null) return;
                    this.menuItems.updateItem('edit', st.edit, 0, recId);
                    this.menuItems.showAt([pos[0] + posFrame[0], pos[1] + posFrame[1]]);
                    break;

                case 'component':
                    if (this.menuProps == null) return;
                    this.menuProps.updateItem('property', st.property, 0, recId);
                    this.menuProps.showAt([pos[0] + posFrame[0], pos[1] + posFrame[1]]);
                    break;
            }
        }
    }
};

/**
 * @class Manager.form.ComboCategory
 * @extends Ext.form.ComboBox
 * Выпадающий список категорий статей
 * @xtype mn-field-category
 */
Manager.form.ComboCategory = Ext.extend(Manager.form.ComboField, {
    listeners: {
        select: function(combo, record, index) {
            if (record.json.data) {
                node = Ext.getCmp('fldArticleTree'),
                node.setValue(record.json.data.id)
                node.setRawValue(record.json.data.name)
            }
        }
    }
});
Ext.reg('mn-field-category', Manager.form.ComboCategory);


/**
 * @class Manager.button.GeneratorUrl
 * @extends Ext.Button
 * Генерация URI ресурса
 * @xtype mn-btn-genurl
 */
Manager.button.GeneratorUrl = Ext.extend(Ext.Button, {
    /**
     * @cfg {String} url ресурс доступа к компоненту (генератор)
     */
    url: '',
    msgSelectHeader: '',
    type: 0,
    prefix: '',
    fldAddress: 'fldArticleUri',
    fldTree: '',

    // private
    initComponent : function(){
        Ext.apply(this, {width: 30});

        Manager.button.GeneratorUrl.superclass.initComponent.call(this);
    },

    listeners: {
        click: function(b, e){
            var uri = Ext.getCmp(this.fldAddress),
                hv = '',
                self = this;
            hv = uri.getValue();
            if (hv == undefined) hv = '';
            if (hv.length == 0) {
                Ext.Msg.show({
                   title: Ext.Msg.T.warning, msg: self.msgSelectHeader, buttons: Ext.Msg.OK, icon: Ext.Msg.ERROR
               });
               return false;
            }
            Ext.Msg.show({
               msg: 'Пожалуйста подождите...', progressText: 'Обработка...', width:300, wait: true, waitConfig: {interval:200}, icon: 'icon-progress-gear', animEl: this
           });
           var treeId = 0;
           if (this.fldTree.length)
                treeId = Ext.getCmp(this.fldTree).hiddenField.value;
            Ext.Ajax.request({
                method: 'POST',
                url: Manager.COMPONENTS_URL + this.url,
                params: { header: hv, tree: treeId },
                failure: function(r, o){ Ext.Msg.hide(); Ext.Msg.failure(r); },
                success: function(r, o) {
                    Ext.Msg.hide();
                    var r = Ext.util.JSON.decode(r.responseText);
                    if (r.success)
                        uri.setValue(r.data.text);
                    else
                        Ext.Msg.error(r);
                }
            });
        }
    }
});
Ext.reg('mn-btn-genurl', Manager.button.GeneratorUrl);


/**
 * @class Manager.button.Translate
 * @extends Ext.Button
 * Кнопка перевод текста в профиле записи
 * @xtype mn-btn-translate
 */
Manager.button.Translate = Ext.extend(Ext.Button, {
    tooltip: 'Перевод текста',
    text: 'Перевод',
    iconCls: 'icon-fbtn-translate',

    // private
    clickItem: function(i, e){
        function getFormValues(f){
            var v, t = {};
            f.items.each(function(f){
            if (f.translate != undefined)
                if (f.translate) {
                    v = f.getValue();
                    if (v != undefined)
                    if (v.length > 0) t['fields[' + f.name + ']' ] = v;
                }
            });
            return t;
        }

        Ext.Msg.show({title: Ext.Msg.T.loading, msg: Ext.Msg.M.loading, width: 300, progress: true, closable: false});
        var form = Ext.getCmp(i.parentMenu.windowId).getForm();
        var params = getFormValues(form);
        params.lang = i.langFrom + '-' + i.langTo;
        Ext.Ajax.request({
            method: 'POST',
            url: i.parentMenu.url,
            params: params,
            failure: function(r, o){ Ext.Msg.failure(r); },
            success: function(r, o){
                Ext.Msg.hide();
                var r = Ext.util.JSON.decode(r.responseText);
                if (r.success)
                    form.setValues(r.data);
                else
                    Ext.Msg.error(r);
            }
        });
    },

    // private
    initComponent : function(){
        Manager.button.Translate.superclass.initComponent.call(this);

        this.menu.addListener('itemclick', this.clickItem);
    }

});
Ext.reg('mn-btn-translate', Manager.button.Translate);


/**
 * @class Manager.form.FieldFileStatic
 * @extends Ext.BoxComponent
 * Вывод изображения в область
 * @xtype mn-field-file-static
 */
Manager.form.FieldFileStatic = Ext.extend(Ext.BoxComponent, {
    autoEl: {
        tag: 'div',
        cls: 'mn-field-fs',
        children: [
            {tag: 'a', cls: 'mn-field-fs upload', href: ''},
            {tag: 'a', cls: 'mn-field-fs remove', href: ''},
            {tag: 'div', cls: 'mn-field-fs text'}
        ]
    },

    // private
    afterRender : function(){
        Manager.form.ImageView.superclass.afterRender.call(this);
        var me = this;

        this.ctDiv = this.el.select('div.mn-field-fs');
        var txt = this.ctDiv.select('div.mn-field-fs.text');
    }
});
Ext.reg('mn-field-file-static', Manager.form.FieldFileStatic);


/**
 * @class Manager.site.Menu
 * @extends Ext.menu.Menu
 * Контекстное меню в конструкторе
 */
Manager.site.Menu = Ext.extend(Ext.menu.Menu, {
    updateItem: function(type, st, index, recId){
        var it = this.items.get(index);
        if (st != null) {
            it.type = type;
            it.setDisabled(false);
            it.setText(st.text);
            it.url = st.url;
            it.recId = recId;
        } else
            it.setDisabled(true);
    },

    // private
    callClickItem: function(item){
        if (item.url.length) {
            var aId = Site.design.component.attr('page-id'),
                dId = Site.design.component.attr('data-id'),
                tId = Site.design.component.attr('template-id'),
                bt = Site.design.component.attr('belong-to'),
                url = '?to=frame';
            if (item.type == 'property') {
                if (bt == 'template') {
                    aId = tId;
                }
                url = aId + url + '&bTo=' + bt + '&tId=' + tId + '&cId=' + Site.design.component.attr('id') + '&cClass=' + Site.design.component.attr('class');
            } else
            if (item.type == 'add') {
                // чтбы добавить запись в ложенный список
                url = url + '&parent=' + item.recId;
            } else
                url = item.recId + url;

            Manager.widget.load({ url: Manager.COMPONENTS_URL + item.url + url });
        }
    },

    listeners: {
        click: function(menu, item, e){ this.callClickItem(item); }
    }
});


/**
 * @class Manager.button.SiteMode
 * @extends Ext.Button
 * Переход сайта в режим конструктора
 * @xtype mn-btn-sitemode
 */
Manager.button.SiteMode = Ext.extend(Ext.Button, {
    tlpModeDesign: 'Сайт в режиме конструктора',
    tlpModeView: 'Сайт в режиме просмотра',
    tooltip: 'Сайт в режиме конструктора',
    enableToggle: true,
    iconCls: 'icon-btn-mview',
    /**
     * @cfg {String} token ключ для перехода сайта в режим конструктора
     */
    token: '',
    /**
     * @cfg {Object} frameSt объект фрейма
     */
    frameSt: null,

    // private
    initComponent : function(){
        if (this.pressed)
            this.setIconClass('icon-btn-mdesign');

        Manager.button.SiteMode.superclass.initComponent.call(this);
    },

    listeners: {
        toggle: function(btn, pressed){
            if (pressed) {
                Ext.util.Cookies.set('cms-mode', 'design');
                Ext.util.Cookies.set('cms-token', btn.token);
                btn.setTooltip(btn.tlpModeDesign);
                btn.setIconClass('icon-btn-mdesign');
            } else {
                Ext.util.Cookies.set('cms-mode', 'view');
                Ext.util.Cookies.set('cms-token', btn.token);
                btn.setTooltip(btn.tlpModeView);
                btn.setIconClass('icon-btn-mview');
            }
            this.frameSt.reload();
        }
    }
});
Ext.reg('mn-btn-sitemode', Manager.button.SiteMode);


/**
 * @class Manager.site.Panel
 * @extends Ext.Panel
 * Панель вывода сайта
 * @xtype mn-site-panel
 */
Manager.site.Panel = Ext.extend(Ext.Panel, {
    /**
     * @cfg {String} frameId идент. фрейма
     */
    frameId: 'site',
    /**
     * @cfg {String} infoId url фрейма
     */
    infoId: 'stFrameUrl',
    /**
     * @cfg {String} token ключ для перехода сайта в режим конструктора
     */
    token: '',
    /**
     * @cfg {Boolean} designMode сайта в режим конструктора
     */
    designMode: false,
    /**
     * @cfg {String} url ресурс фрейма
     */
    url: '',

    // private
    initComponent : function(){
        var self = this;
        Site.design.frameId = 'iframe-' + this.frameId;
        var frameTl = new Ext.form.DisplayField({ id: this.infoId, style: 'margin-left:10px;' });
        this.frameSt = new Manager.form.Iframe({ id: this.frameId, url: self.url, title: frameTl });

        this.tbar = [
            frameTl,  '->', {
            tooltip: 'Обновить страницу сайта',
            iconCls: 'x-tbar-loading',
            listeners: { click: function(){ self.frameSt.reload(); } }
        }, {
            xtype: 'mn-btn-sitemode',
            pressed: this.designMode,
            frameSt: this.frameSt,
            token: this.token
        }];

        this.items = [this.frameSt];

        if (Site.design.menu == null) {
            Site.design.menu = new Manager.site.Menu({
                items: [{ 
                    text: 'Добавить',
                    iconCls: 'icon-item-add'
                }, {
                    text: 'Редактировать',
                    iconCls: 'icon-item-edit',
                    url: ''
                }, '-', {
                    text: 'Список',
                    iconCls: 'icon-item-list',
                    url: ''
                }, '-', {
                    text: 'Свойства компонента',
                    iconCls: 'icon-item-component',
                    url: ''
                }]
            });
        }
        if (Site.design.menuItems == null) {
            Site.design.menuItems = new Manager.site.Menu({
                items: [{
                    text: 'Редактировать',
                    iconCls: 'icon-item-edit',
                    url: ''
                }]
            });
        }
        if (Site.design.menuProps == null) {
            Site.design.menuProps = new Manager.site.Menu({
                items: [{
                    text: 'Свойства компонента',
                    iconCls: 'icon-item-component',
                    url: ''
                }]
            });
        }

        Manager.site.Panel.superclass.initComponent.call(this);
    }
});
Ext.reg('mn-site-panel', Manager.site.Panel);


/**
 * @class Manager.button.WidgetForm
 * @extends Ext.Button
 * Кнопка вызова компонента
 * @xtype mn-btn-widget-form
 */
Manager.button.ResetCache = Ext.extend(Ext.Button, {
    /**
     * @cfg {String} сообщение для потвержденя запроса
     */
    msgConfirm: '',
    /**
     * @cfg {String} идент. окна (закрывается после выполнения запроса)
     */
    ctId: '',
    /**
     * @cfg {String} url ресурс к которому выполняется запрос
     */
    url: '',

    /**
     * Выполнить запрос по клику кнопки
     */
    callAction: function(){
        var owner = this;
        Ext.Ajax.request({
            method: 'POST',
            url: Manager.COMPONENTS_URL + owner.url,
            params: { method: 'CLEAR' },
            failure: function(r, o){ Ext.Msg.failure(r); },
            success: function(r, o){
                var r = Ext.util.JSON.decode(r.responseText);
                if (r.success) {
                    App.setAlert(r.status, r.message);
                    Ext.getCmp(owner.ctId).hide();
                } else
                    Ext.Msg.error(r);
            }
        });
    },

    listeners: {
        click: function() {
            var o = this;
            if (this.msgConfirm.length) {
                Ext.Msg.show({title: Ext.Msg.T.warning, msg: this.msgConfirm, buttons: Ext.Msg.YESNO, fn: function(btn){
                    if (btn == 'yes') o.callAction();
                }, animEl: this, icon: Ext.Msg.QUESTION});
            } else
                this.callAction();
        }
    }
});
Ext.reg('mn-btn-resetcache', Manager.button.ResetCache);


/**
 * @class Manager.button.SelectTypeImg
 * @extends Ext.CycleButton
 * Выбор изображения для вставки
 * @xtype mn-btn-selecttypeimg
 */
Manager.button.SelectTypeImg = Ext.extend(Ext.CycleButton, {
    imgData: {},
    showText: true,
    prefix: '',
    items: [{
        text: 'оригинал изображения',
        index: 1,
        prefix: '',
        checked:true
    }, {
        text: 'среднее изображение',
        prefix: '_medium',
        index: 2
    }, {
        text: 'превью изображения',
        prefix: '_thumb',
        index: 3
    }],
    changeHandler:function(btn, item){
        this.prefix = item.prefix;
        Ext.getCmp('fldAttrImageSrc').setValue(this.imgData.path + this.imgData.name + item.prefix + this.imgData.ext);
    },
    setType: function(data){
        this.imgData = data;
        if (!data.medium && !data.thumb) {
            this.hide();
            return;
        }
        if (data.medium)
            this.menu.items.itemAt(1).enable();
        else
            this.menu.items.itemAt(1).disable();
        if (data.thumb)
            this.menu.items.itemAt(2).enable();
        else
            this.menu.items.itemAt(2).disable();
        this.show();
    },
    getDefImgSrc: function(){ return this.imgData.path + this.imgData.name + this.imgData.ext; },
    getImgSrc: function(){ return this.imgData.path + this.imgData.name + this.prefix + this.imgData.ext; }
});
Ext.reg('mn-btn-selecttypeimg', Manager.button.SelectTypeImg);


/**
 * @class Manager.button.SendData
 * Кнопка отправки данных формы
 * @xtype mn-btn-senddata
 */
Manager.button.SendData = Ext.extend(Ext.Button, {
    /**
     * @cfg {String} идент. окна (закрывается после выполнения запроса)
     */
    windowId: null,
    /**
     * @cfg {String} url ресурс к которому выполняется запрос
     */
    url: '',

    // private
    listeners: {
        click: function(b, e){
            function getFormValues(f){
                var v, t = {};
                f.items.each(function(f){
                    v = f.getValue();
                    if (v != undefined)
                    if (v.length > 0) t['fields[' + f.name + ']' ] = v;
                });
                return t;
            }

            Ext.Msg.show({title: Ext.Msg.T.loading, msg: Ext.Msg.M.loading, width: 300, progress: true, closable: false});
            var form = Ext.getCmp(this.windowId).getForm();
            var params = getFormValues(form);
            Ext.Ajax.request({
                method: 'POST',
                url: Manager.COMPONENTS_URL + this.url,
                params: params,
                failure: function(r, o){ Ext.Msg.failure(r); },
                success: function(r, o){
                    Ext.Msg.hide();
                    var r = Ext.util.JSON.decode(r.responseText);
                    if (r.success)
                        form.setValues(r.data);
                    else
                        Ext.Msg.error(r);
                }
            });
        },
    }

});
Ext.reg('mn-btn-senddata', Manager.button.SendData);