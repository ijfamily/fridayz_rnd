<?php
/**
 * Gear Manager
 *
 * Капча
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Captcha
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: captcha.php 2016-01-01 12:00:00 Gear Magic$
 */

// если нет запроса
if(!isset($_REQUEST['_dc'])) return;

/**
 * @see Gear
 */
require('core/Gear.php');

GException::$selfHandler = false;
// инициализация сессии
$sess = GFactory::getSession();
$sess->start();
// создание капчи
$captcha = GFactory::getClass('Captcha', 'Captcha/Captcha');
$captcha->render();
$sess->set('captcha', $captcha->getKey());
?>