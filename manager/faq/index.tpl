<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="resources/css/faq.css" />
    <link rel="stylesheet" type="text/css" href="../resources/themes/%theme%/default.css" />

    <script type="text/javascript" src="../resources/js/extjs/adapter/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="resources/js/faq.js"></script>
</head>

<body>
    <div class="wrap">
        <table class="wrap-tbl" width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td valign="top" class="menu">
                    <div class="side-box-inner">
                        <div class="side-box-toolbar">
                            <a id="collapse" href="#" title="%tipCollapse%"><img src="resources/images/icon-collapse-all.gif" /></a>
                            <a id="expand" href="#" title="%tipExpand%"><img src="resources/images/icon-expand-all.gif" /></a>
                        </div>
                        <ul class="features">%features%</ul>
                    </div>
                </td>
                <td valign="top" class="view">
                    <h1>%tlHeader%</h1>
                    %sections%
                    </dl><div class="wrap"></div></div>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>