/*
 * Common
 *
 * Copyright(c) 2013, Gear Magic
 * Licensed under the MIT License (http://www.opensource.org/licenses/mit-license.php)
 * Date: Jul 1 2013
 */

$(document).ready(function(){
    function expandRows(){
        $('.view > div').removeClass('collapsed');
    }
    function collapseRow(rowId){
        $('.view > div').addClass('collapsed');
        $('#' + rowId).removeClass('collapsed');
    }
    $('dd').hover(function(){
        $(this).addClass('over');
    }, function(){
        $(this).removeClass('over');
    });
    $('dd').click(function(){
        document.location = $(this).attr('url');
    });
    $('.view > div').each(function(i, o){
        var p = $(this);
        var dd = $(o).children('dd');
        $(o).children('h2').click(function(){
            if (p.attr('class') == 'collapsed')
                p.removeClass('collapsed');
            else
                p.addClass('collapsed');
        });
    });
    $('.menu li a').each(function(i, o){
        $(this).click(function(){
            collapseRow('row' + $(this).attr('section'));
        });
    });
    $('#expand').click(function(){expandRows();});
    $('#collapse').click(function(){collapseRow();});
    $('#btn-zoomin').click(function(){
        $('.q').addClass('zoom-in');
        return false;
    });
    $('#btn-zoomout').click(function(){
        $('.q').removeClass('zoom-in');
        return false;
    });
});