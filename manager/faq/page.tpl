<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="resources/css/page.css" />
    <link rel="stylesheet" type="text/css" href="resources/css/%theme%-page.css" />
    <link rel="stylesheet" type="text/css" href="../resources/themes/%theme%/default.css" />

    <script type="text/javascript" src="../resources/js/extjs/adapter/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="resources/js/faq.js"></script>
</head>

<body>
    <div class="control">
        <a id="btn-up" href="#header" title="%tipUp%"></a>
        <a id="btn-down" href="#footer" title="%tipDown%"></a>
    </div>
    <div id="header" class="header">
        <div class="ct">
            <a href="." title="%tipHome%"></a>
            <h1>%faq_name%</h1>
            <h3><div>%faq_description%</div></h3>
        </div>
        <div class="zoom">
            <a id="btn-home" href="." title="%tipHome%"></a>
            <a id="btn-zoomin" href="#" title="%tipZoomin%"></a>
            <a id="btn-zoomout" href="#" title="%tipZoomout%"></a>
        </div>
    </div>
    <div class="q">%faq_text%</div>
    %footer%
</body>
</html>