<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    'tlHeader'    => 'Часто задаваемые вопросы & ответы',
    'tipCollapse' => 'Свернуть',
    'tipExpand'   => 'Развернуть',
    'tipZoomout'  => 'Уменьшить',
    'tipZoomin'   => 'Увеличить',
    'tipHome'     => 'Главная страница',
    'tlFooter'    => 'Статьи в этом разделе',
    'tipUp'       => 'В начало страницы',
    'tipDown'     => 'В конец страницы',
    'msgNotExist' => 'Выбранный Вам пункт справки не существует!',
    'msgSelect'   => 'попробуйте выбрать другой раздел'
);
?>