<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    'tlHeader'    => 'Часті питання & відповіді',
    'tipCollapse' => 'Згорнути',
    'tipExpand'   => 'Розгорнути',
    'tipZoomout'  => 'Зменшити',
    'tipZoomin'   => 'Збільшити',
    'tipHome'     => 'Головна сторінка',
    'tlFooter'    => 'Статті в цьому розділі',
    'tipUp'       => 'На початок сторінки',
    'tipDown'     => 'У кінець сторінки',
    'msgNotExist' => 'Обраний Вам пункт довідки не існує!',
    'msgSelect'   => 'спробуйте вибрати інший розділ'
);
?>