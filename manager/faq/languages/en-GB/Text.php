<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    'tlHeader'    => 'Frequently Asked Questions & Answers',
    'tipCollapse' => 'Collapse',
    'tipExpand'   => 'Expand',
    'tipZoomout'  => 'Reduce',
    'tipZoomin'   => 'Enlarge',
    'tipHome'     => 'Main page',
    'tlFooter'    => 'Articles in this section',
    'tipUp'       => 'Top of page',
    'tipDown'     => 'At the end of the page',
    'msgNotExist' => 'You selected a point of reference does not exist!',
    'msgSelect'   => 'try with another partition'
);
?>