<?php
/**
 * Gear Manager
 *
 * Базовые настройки системы
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Core
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Gear.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Спецсимволы
 */
define('_t', '    ');
define('_2t', '        ');
define('_3t', '            ');
define('_n', "\n");
define('_n_t', "\n    ");

/**
 * Определение путей к библиотекам и шаблонам системы
 */
// путь к приложению (каталог: "/{manager}/login")
define('PATH_APPLICATION', 'manager/');
// роутер отвечающий за все запросы
define('ROUTER_SCRIPT', 'router.php/');
// разделитель запросов в роутере
define('ROUTER_DELIMITER', '~/');
// корень сайта (если работает "open_basedir" с результатом вида "/home/site:/usr/lib/php:/usr/local/lib/php:/tmp",
// то необходимо вписать значение "/home/site/www/", если "open_basedir" отключен то "../")
//define('DOCUMENT_ROOT', '../');
define('DOCUMENT_ROOT', '../');
// название ядра (каталог: "/gear/libraries/{Gear}/...")
define('CORE_NAME', 'Gear/');
// путь к данным
define('PATH_DATA', 'data/');
// путь к языкам
define('PATH_LANGUAGE', 'languages/');
// путь к компонентам системы
define('PATH_COMPONENT', 'components/');
// путь к каталогу настроек системы
define('PATH_CONFIG', PATH_APPLICATION . 'config/');
// путь к каталогу настроек CMS
define('PATH_CONFIG_CMS', 'application/config/');

/**
 * @see GFactory
 */
require('Factory.php');

/**
 * Фабрика классов
 * 
 * @category   Gear
 * @package    Core
 * @copyright  Copyright (c) 2013-2014 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Gear.php 2014-08-01 12:00:00 Gear Magic $
 */
final class Gear
{
    /**
     * Перенаправление на нужный URL с заголовком PHP или использование JavaScript location.href
     *
     * @param string $url  URL куда перенаправить
     * @param string $mode PHP|JS
     */
    public static function redirect($url, $mode = "PHP") {
        if ((PHP_VERSION_ID >= 50400 && PHP_SESSION_ACTIVE === session_status()) || (PHP_VERSION_ID < 50400 && isset($_SESSION) && session_id())) {
            session_write_close();
        }

        if ($mode == "PHP") {
            header("Location: $url");
        } elseif ($mode == "JS") {
            echo '<html><head><script type="text/javascript">';
            echo 'function redirect(){ window.top.location.href="' . $url . '"; }';
            echo '</script></head>';
            echo '<body onload="redirect()">Redirecting, please wait...</body></html>';
        }

        die();
    }

    /**
     * Подключение сценария библиотеки
     * (путь "Gear/{$name}")
     * 
     * @param  string $name название файла
     * @param  boolean $once подлючить сценарий только 1-н раз
     * @param  boolean $library если true - подключить сценарий из Gear/{$name}", если false - Gear/Imports/{$name}"
     * @return mixed
     */
    public static function controller($name)
    {
        require_once(CORE_NAME . 'Controller/' . $name . '.php');
    }

    /**
     * Подключение сценария библиотеки
     * (путь "Gear/{$name}")
     * 
     * @param  string $name название файла
     * @param  boolean $once подлючить сценарий только 1-н раз
     * @param  boolean $library если true - подключить сценарий из Gear/{$name}", если false - Gear/Imports/{$name}"
     * @return mixed
     */
    public static function library($name)
    {
        require_once(CORE_NAME . $name . '.php');
    }

    /**
     * Подключение сценария библиотеки
     * (путь "Gear/{$name}")
     * 
     * @param  string $name название файла
     * @param  boolean $once подлючить сценарий только 1-н раз
     * @param  boolean $library если true - подключить сценарий из Gear/{$name}", если false - Gear/Imports/{$name}"
     * @return mixed
     */
    public static function ns($name)
    {
        require_once(CORE_NAME . $name . '.php');
    }

    /**
     * Подключение сценария библиотеки
     * (путь "Gear/{$name}")
     * 
     * @param  string $name название файла
     * @param  boolean $once подлючить сценарий только 1-н раз
     * @param  boolean $library если true - подключить сценарий из Gear/{$name}", если false - Gear/Imports/{$name}"
     * @return mixed
     */
    public static function ext($name)
    {
        require_once(CORE_NAME . 'Ext/' . $name . '.php');
    }

    /**
     * Подключение сценария библиотеки
     * (путь "Gear/{$name}")
     * 
     * @param  string $name название файла
     * @param  boolean $once подлючить сценарий только 1-н раз
     * @param  boolean $library если true - подключить сценарий из Gear/{$name}", если false - Gear/Imports/{$name}"
     * @return mixed
     */
    public static function import($name)
    {
        require_once(CORE_NAME . 'Imports/' . $name . '.php');
    }

    /**
     * Проверка ключа безопасности
     * 
     * @param  string $key ключ
     * @return boolean
     */
    public static function checkKey()
    {
        $config = GFactory::getConfig();

        return isset($_COOKIE['key']) ? $_COOKIE['key'] == $config->get('SECURITY_KEY') : false;
    }

    /**
     * Проверка акаунта пользователя
     * 
     * @return boolean
     */
    public static function checkAccount()
    {
        return GFactory::getSession()->get('user_id', false);
    }

    /**
     * Проверка ip адресов
     * 
     * @param  mixed $ipAddress доступный ip адрес
     * @return boolean
     */
    public static function checkAvailableIpAdr($ipAddress)
    {
        if (empty($ipAddress))
            return true;

        // $ipAddress -> array('192.168.0.1', ...)
        if (is_array($ipAddress))
            return in_array($_SERVER['REMOTE_ADDR'], $ipAddress);
        // $ipAddress = '192.168.0.1'
        if (is_string($ipAddress))
            return $_SERVER['REMOTE_ADDR'] == $ipAddress;

        return false;
    }

    /**
     * Проверка сервера
     * 
     * @param  mixed $host хост
     * @return boolean
     */
    public static function checkServer($host)
    {
        if (empty($host))
            return true;

        // $host -> array('host.com', 'www.host.com')
        if (is_array($host)) {
            if (isset($_SERVER['HTTP_REFERER'])) {
                $referer = parse_url($_SERVER['HTTP_REFERER']);
                return in_array($referer['host'], $host);
            } else
                return in_array($_SERVER['HTTP_HOST'], $host);
        }
        // $host -> 'host.com'
        if (is_string($host)) {
            if (isset($_SERVER['HTTP_REFERER'])) {
                $referer = parse_url($_SERVER['HTTP_REFERER']);
                return $referer['host'] == $host;
            } else
                return $_SERVER['HTTP_HOST'] == $host;
        }

        return false;
    }

    /**
     * Возращает "соль" для пароля
     * 
     * @return boolean
     */
    public static function getPasswordSalt()
    {
        return uniqid();
    }

    /**
     * Генерирует пароль
     * 
     * @param  string $value пароль
     * @param  boolean $structure возращает "соль" и пароль
     * @return boolean
     */

    public static function getPassword($value, $structure = false)
    {
        $salt = self::getPasswordSalt();
        $password = md5(md5($value) . $salt);
        if ($structure)
            return array('salt' => $salt, 'password' => $password);

        return $password;
    }

    /**
     * Возращает хэш пароля для восстановления по ссылке
     * 
     * @param  string $value строка
     * @return string
     */

    public static function getPasswordHash($value)
    {
        return md5($value);
    }

    /**
     * Проверка пароля
     * 
     * @param  string $value веденный пароль
     * @param  string $password пароль
     * @param  string $salt "соль" пароля
     * @param  boolean $isMD5 генерировать в md5
     * @return boolean
     */
    public static function checkPassword($value, $password, $salt, $isMD5 = false)
    {
        return $password == md5(($isMD5 ? md5($value) : $value) . $salt);
    }

    /**
     * Проверка invisible
     * 
     * @param  string $code код
     * @return boolean
     */
    public static function checkInvisible($code)
    {
        return $code == md5($_SERVER['SERVER_ADDR']);
    }

    /**
     * Возращает переменные для шаблона
     * 
     * @return mixed
     */
    public static function &tpl($name)
    {
        if (empty($GLOBALS[$name]))
            return $GLOBALS[$name];
        else
            return $GLOBALS[$name];
    }
}


/**
 * Привилегии доступа к контроллеру
 * 
 * @category   Gear
 * @package    Core
 * @copyright  Copyright (c) 2013-2014 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Gear.php 2014-08-01 12:00:00 Gear Magic $
 */
final class GPrivileges
{
    /**
     * Привилегии доступа к компоненту по умолчанию
     * 
     * @var array
     */
    public static $default = array('select', 'insert', 'update', 'delete', 'clear', 'export', 'root');

    /**
     * Возращает привилегии доступа к компоненту по умолчанию
     * (для формирования списка привилегий компонента)
     * 
     * @param  array $items список привилегий (если нет - по умолчанию)
     * @return string
     */
    public static function defaultToJson($items = array())
    {
        if (empty($items))
            $items = self::$default;
        $result = array();
        $count = sizeof($items);
        for ($i = 0; $i < $count; $i++) {
            $result[] = array('label' => '', 'action' => $items[$i]);
        }

        return json_encode($result);
    }

    /**
     * Возращает привилегии доступа к компоненту по умолчанию
     * (для формирования списка привилегий компонента)
     * 
     * @param  array $items список привилегий (если нет - по умолчанию)
     * @return array
     */
    public static function getDefault($items = array())
    {
        if (empty($items))
            $items = self::$default;

        $result = array();
        $text = GText::get('privileges', 'interface');
        $count = sizeof($items);
        for ($i = 0; $i < $count; $i++) {
            if (key_exists($items[$i], $text))
                $result[] = array('label' => $text[$items[$i]], 'action' => $items[$i]);
        }

        return $result;
    }

    /**
     * Возращает привилегии доступа к компоненту по умолчанию
     * (для формирования списка привилегий компонента)
     * 
     * @param  array $items список привилегий (если нет - по умолчанию)
     * @return array
     */
    public static function getTypeAccess($privileges)
    {
        $count = sizeof($privileges);
        for ($i = 0; $i < $count; $i++) {
            $alias[$privileges[$i]['action']] = $privileges[$i]['label'];
        }
        // Чтение / Запись
        if ($count == 3 && isset($alias['select']) && isset($alias['insert']) && isset($alias['update']))
            return 1;
        else
        // Чтение
        if ($count == 1 && isset($alias['select']))
            return 2;
        else
        // Особый
        if ($count == 2 && isset($alias['clear']))
            return 4;
        else
        // Полный доступ
        if ($count == 1 && isset($alias['root']))
            return 6;
        // По привилегиям
        else
            return 5;
    }

    /**
     * Конвертирует список привилегий для среды компонента
     * 
     * @param  array $items список привилегий
     * @return array
     */
    public static function toStore($json)
    {
        $items = json_decode($json, true);
        $count = sizeof($items);
        $result = array();
        for($i = 0; $i < $count; $i++)
            $result[$items[$i]['action']] = true;

        return $result;
    }

    /**
     * Конвертирует список привилегий в строку
     * 
     * @param  array $items список привилегий
     * @param  string $glue разделитель
     * @return string
     */
    public static function toString($items, $glue = ',')
    {
        $result = array();
        $text = GText::get('privileges', 'interface');
        $count = sizeof($items);
        for ($i = 0; $i < $count; $i++) {
            $action = $items[$i]['action'];
            if (key_exists($action, $text))
                    $result[] = $text[$action];
            else
                $result[] = $items[$i]['label'];
        }

        return implode($glue, $result);
    }

    /**
     * Конвертирует список привилегий в json
     * (для формирования списка привилегий компонента)
     * 
     * @param  array $items список привилегий
     * @param  boolean $replace если true - будет поставлять в интерфейс название права доступа
     * (для вывода просмотра в профиль записи)
     * @return string
     */
    public static function to($items, $replace = false)
    {
        $result = array();
        $text = GText::get('privileges', 'interface');
        $count = sizeof($items);
        for ($i = 0; $i < $count; $i++) {
            $action = $items[$i]['action'];
            if (key_exists($action, $text)) {
                if ($replace)
                    $result[] = array('label' => $text[$action], 'action' => $action);
                else
                    $result[] = array('label' => '', 'action' => $action);
            } else
                $result[] = $items[$i];
        }

        return $result;
    }

    /**
     * Конвертирует список привилегий в json
     * (для формирования списка привилегий компонента)
     * 
     * @param  array $items список привилегий
     * @param  boolean $replace если true - будет поставлять в интерфейс название права доступа
     * (для вывода просмотра в профиль записи)
     * @return string
     */
    public static function toJson($items, $replace = false)
    {
        return json_encode(self::to($items, $replace));
    }

}

/**
 * @see GLanguage
 */
require('Language.php');

/**
 * @see GException
 */
require('Exception.php');

// инициализация путей
try {
    // путь к каталогу ".../gear/libraries"
    $pathL = DOCUMENT_ROOT.PATH_APPLICATION . 'libraries';
    // путь к каталогу ".../gear"
    $pathA = DOCUMENT_ROOT . PATH_APPLICATION;
    if (!file_exists($pathL))
        die('Configuration error: can`t set the local path "' . $pathL . '" (path not exists)');
    $path = $pathL . PATH_SEPARATOR . $pathA . PATH_SEPARATOR . get_include_path();
    if (set_include_path($path) === false)
        die('Configuration error: can`t set the local path "' . $pathL . '" (path not include)');
} catch(GException $e) {}
?>