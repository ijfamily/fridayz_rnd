<?php
/**
 * Gear Manager
 * 
 * Пакет языка
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Core
 * @package    Language
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Language.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Класс обработки запросов перехода между языками приложения
 * 
 * @category   Core
 * @package    Language
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Language.php 2016-01-01 12:00:00 Gear Magic $
 */
class GLanguage
{
    /**
     * Сылка на экземпляр класса
     *
     * @var mixed
     */
    public static $instance = null;

    /**
     * Префикс языка по умолчанию (из конфига)
     *
     * @var string
     */
    public $default = '';

    /**
     * Данные о текущем языке (array("id", "alias", "prefix", "prefixDef", "title"))
     *
     * @var array
     */
    protected $_data = array();

    /**
     * Список досупных языков (из конфига)
     *
     * @var array
     */
    protected $_languages = array();

    /**
     * Конструктор
     * 
     * @param  string $prefix префикс языка (1-й сегмент в uri, "en-GB", "ua-UK", ...)
     * @param  string $prefixDef префикс языка по умолчанию (из конфига)
     * @param  array $languages список доступных языков (из конфига)
     * @return void
     */
    public function __construct($default = '', $languages = array())
    {
        // префикс по умолчанию
        $this->default = $default;
        // если есть список языков
        if ($languages)
            $this->_languages = $languages;
        // устанавливаем язык
        $this->set($this->getLanguage());
    }

    /**
     * Возращает указатель на созданный экземпляр класса (если класс не создан, создаст его)
     * 
     * @return mixed
     */
    public static function getInstance($default = '', $languages = array())
    {
        if (null === self::$instance) 
            self::$instance = new self($default, $languages);

        return self::$instance;
    }

    /**
     * Возращает выбранный язык
     * 
     * @return string
     */
    public function getLanguage()
    {
        if (isset($_POST['language']))
            $key = $_POST['language'];
        else
        if (isset($_GET['language']))
            $key = $_GET['language'];
        else
        if (isset($_COOKIE['language']))
            $key = $_COOKIE['language'];
        else
        if (isset($_SESSION['language/default/alias']))
            $key = $_SESSION['language/default/alias'];
        else
            $key = GFactory::getConfig()->get('LANGUAGE');

        return $key;
    }

    /**
     * Возращает значение атрибута $name языка
     * 
     * @param  string $name один из атрибутов выбранного языка
     * @return mixed
     */
    public function get($name, $default = '')
    {
        if (isset($this->_data[$name]))
            return $this->_data[$name];
        else
            return $default;
    }

    /**
     * Возращает список доступных языков из конфига по псевдониму
     * alias => [id, alias, name, title]
     * 
     * @return array
     */
    public function getLanguages($byId = false)
    {
        return $this->_languages;
    }

    /**
     * Проверка существованмя языка по псевдониму
     * 
     * @param  string $alias псевдоним
     * @return boolean
     */
    public function isExist($alias)
    {
        return isset($this->_languages[$alias]);
    }

    /**
     * Возращает список доступных языков из конфига по псевдониму
     * $name => [id, alias, name, title]
     * 
     * @return array
     */
    public function getLanguagesBy($name, $value = '', $default = '')
    {
        $langs = array();
        foreach($this->_languages as $alias => $data) {
            $langs[$data[$name]] = $data;
        }
        // если есть выбранный параметр
        if ($value) {
            if (isset($langs[$value]))
                return $langs[$value];
            else
                return $langs[$default];
        } else
            return $langs;
    }

    /**
     * Устанавливает язык
     * 
     * @param  string $alias префикс языка ("en-GB", "ua-UK", ...)
     * @return mixed
     */
    public function set($alias = '')
    {
        try {
            // если выбранный язык существует
            if (isset($this->_languages[$alias]))
                $this->alias = $alias;
            else
                $this->alias = $this->default;
            $this->_data = $this->_languages[$this->alias];
            // путь к языкам
            $path = DOCUMENT_ROOT . PATH_APPLICATION . PATH_LANGUAGE . $this->alias . '/';
            // если устанавлеваемый язык существует
            $fileName = $path . 'Exception.php';
            if (!file_exists($fileName)) {
                GException::$location = 'error?languages&set=' . $this->alias;
                throw new GException('Configuration error', 'Unable to mount language (%s)', $this->alias, true);
            }
            // подключаем языки
            GText::$path = $path;
            GText::add('exception', 'Exception.php');
            GText::add('interface', 'Interface.php');
        } catch(GException $e) {}
    }

    /**
     * Устанавливает языки доступные в системе
     * 
     * @param  array $languages список языков
     * @return void
     */
    public function setLanguages($languages)
    {
        $this->_languages = $languages;
    }
}


/**
 * Класс обработки текста языков
 * 
 * @category   Core
 * @package    Language
 * @subpackage Text
 * @copyright  Copyright (c) 2013-2014 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Language.php 2014-08-01 12:00:00 Gear Magic $
 */
class GText
{
    /**
     * Путь к файлам языков
     *
     * @var string
     */
    public static $path = '';

    /**
     * Массив для хранения текста
     *
     * @var array
     */
    protected static $_data = array();

    /**
     * Возвращает текст
     * 
     * @param  string $str текст ("abcb .. %s ...")
     * @param  array $args массив значений для подстановки вместо "%s"
     * @return string
     */
    public static function formatStr($str, $args)
    {
        if (!sizeof($args)) return $str;

        if (is_array($args))
            array_unshift($args, $str);
        
        if (is_string($args))
            $args = array($str, $args);

        return call_user_func_array('sprintf', $args);
    }

    /**
     * Добавление текст в раздел
     * 
     * @param  string $section название раздела
     * @param  string $filename имя файла
     * @return void
     */
    public static function add($section, $filename)
    {
        self::$_data[$section] = @require(self::$path . $filename);
    }

    /**
     * Добавление текст компонента в раздел
     * {component}/languages/{language}/Text.php
     * 
     * @param  string $section название раздела или каталога с компонентами ("forms", "sliders") 
     * @return void
     */
    public static function addText($path, $section = 'controller', $alias = '')
    {
        if ($alias)
            $path = $path . $alias . '/Text.php';
        else
            $path = $path . GFactory::getLanguage('alias') . '/Text.php';

        return self::$_data[$section] = require_once($path);
    }

    /**
     * Возращает текст по ключу
     * 
     * @param  string $name ключ
     * @param  string $section название раздела
     * @return mixed
     */
    public static function get($name, $section = '')
    {
        if ($section) {
            if (isset(self::$_data[$section][$name]))
                return self::$_data[$section][$name];
            else
                return $name;
        } else {
            if (isset(self::$_data[$name]))
                return self::$_data[$name];
            else
                return $name;
        }
    }

    /**
     * Возращает текст по ключу
     * 
     * @param  string $name ключ
     * @param  string $section название раздела
     * @return mixed
     */
    public static function &getSection($name = '')
    {
        if ($name) {
            if (isset(self::$_data[$name]))
                return self::$_data[$name];
            else
                return false;
        } else
            return false;
    }

    /**
     * Возращает весь текст
     * 
     * @return array
     */
    public static function getAll()
    {
        return self::$_data;
    }

    /**
     * Устанавливает текст по ключу
     * 
     * @param  string $name ключ
     * @param  string $value текст
     * @return void
     */
    public static function set($name, $value)
    {
        self::$_data[$name] = $value;
    }

    /**
     * Проверка существования текста с ключем
     * 
     * @param  string $name ключ
     * @param  string $section название раздела
     * @return boolean
     */
    public static function has($name, $section = '')
    {
        if ($section)
            return isset(self::$_data[$section][$name]);
        else
            return isset(self::$_data[$name]);
    }

    /**
     * Возращает текст из указанного раздела и выполняет подстановку параметров
     * 
     * @param  string $name ключ
     * @param  string $section название раздела
     * @param  mixed $params параметры ("abcd" или array("a", "b", ...))
     * @return string
     */
    public static function _($name, $section = 'controller', $params = '')
    {
        if (($str = self::get($name, $section)) === false)
            $str = $name;

        if ($params)
            return self::formatStr($str, $params);
        else
            return $str;
    }
}
?>