<?php
/**
 * Gear Manager
 *
 * Фабрика классов
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Core
 * @package    Factory
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Factory.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Фабрика классов
 * 
 * @category   Core
 * @package    Factory
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Factory.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GFactory
{
    /**
     * Указатель на экземпляр класса базы данных
     *
     * @return object
     */
    protected static $_db = null;

    /**
     * Указатель на экземпляр класса кэша
     *
     * @return object
     */
    protected static $_cache = null;

    /**
     * Указатель на экземпляр класса конфигурации
     *
     * @return object
     */
    protected static $_config = null;


    /**
     * Указатель на экземпляр класса маршрутизатора
     *
     * @return object
     */
    protected static $_router = null;

    /**
     * Указатель экземпляра класса запроса пользователя
     *
     * @return object
     */
    protected static $_request = null;

    /**
     * Указатель экземпляра класса запроса пользователя
     *
     * @return object
     */
    protected static $_response = null;

    /**
     * Указатель экземпляра класса отладчика
     *
     * @return object
     */
    protected static $_debugger = null;

    /**
     * Указатель экземпляра класса сессии
     *
     * @return object
     */
    protected static $_session = null;

    /**
     * Указатель экземпляра класса справочника
     *
     * @return object
     */
    protected static $_reference = null;

    /**
     * Указатель экземпляра класса URI
     *
     * @return object
     */
    protected static $_uri = null;

    /**
     * Указатель экземпляра класса даты
     *
     * @return object
     */
    protected static $_date = null;

    /**
     * Указатель экземпляра класса почты
     *
     * @return object
     */
    protected static $_mail = null;

    /**
     * Указатель экземпляра класса браузера
     *
     * @return object
     */
    protected static $_browser = null;

    /**
     * Указатель экземпляра класса браузера
     *
     * @return object
     */
    protected static $_input = null;

    /**
     * Указатель экземпляра класса браузера
     *
     * @return object
     */
    protected static $_hash = null;

    /**
     * Указатель экземпляра класса пользователей
     *
     * @return object
     */
    protected static $_users = null;

    /**
     * Указатель на экземпляр класса запроса к базе данных
     *
     * @return object
     */
    protected static $_query = null;

    /**
     * Возращает указатель на экземпляр класса запроса к базе данных
     * 
     * @return mixed
     */
    public static function getQuery()
    {
        if (self::$_query === null) {
            self::$_query = new GDb_Query();
        }

        return self::$_query;
    }

    /**
     * Возращает URI
     * 
     * @return mixed
     */
    public static function getUri($uri = '')
    {
        if (self::$_uri === null)
            self::$_uri = self::getIClass('Uri', '', $uri);
        else {
            if ($uri)
                self::$_uri->define($uri);
        }

        return self::$_uri;
    }

    /**
     * Возращает URI
     * 
     * @return object
     */
    public static function getInput()
    {
        if (self::$_input === null)
            self::$_input = self::getClass('Input');

        return self::$_input;
    }

    /**
     * Возращает язык из запроса пользователя
     * 
     * @return void
     */
    public static function getLanguage($key = '')
    {
        return GLanguage::getInstance()->get($key);
    }

    /**
     * Возращает указатель на класс справочника
     * 
     * @return mixed
     */
    public static function getReference($tables = array())
    {
        if (self::$_reference === null)
            self::$_reference = GFactory::getClass('Reference', 'Reference', $tables);

        return self::$_reference;
    }

    /**
     * Возращает указатель на класс браузера
     * 
     * @return mixed
     */
    public static function getBrowser()
    {
        if (self::$_browser === null)
            self::$_browser = GFactory::getClass('Browser');

        return self::$_browser;
    }

    /**
     * Возращает указатель на класс даты
     * 
     * @return mixed
     */
    public static function getDate($timezone = '')
    {
        if (self::$_date === null)
            self::$_date = GFactory::getClass('Date', '', $timezone);

        return self::$_date;
    }

    /**
     * Возращает указатель на класс конфигурации
     * 
     * @return mixed
     */
    public static function getConfig($path = 'config')
    {
        if (self::$_config === null) {
            self::$_config = self::getClass('Config', '', $path);
        }

        return self::$_config;
    }

    /**
     * Возращает указатель на класс базы данных
     * 
     * @return mixed
     */
    public static function getDb($conName = 'default')
    {
        if (self::$_db === null) {
            Gear::library('Db/Driver');
            self::$_db = GDb_Driver::getDb(self::getConfig()->getParams(), $conName);
        }

        return self::$_db;
    }

    /**
     * Возращает указатель на класс базы данных
     * 
     * @return mixed
     */
    public static function getDbDriver($name)
    {
        if (self::$_db === null) {
            GFactory::getDg()->log('null');
            return false;
        }

        return GDb_Driver::factory($name);
    }

    /**
     * Возращает указатель на класс отладчика
     * 
     * @return mixed
     */
    public static function getDg()
    {
        if (self::$_debugger === null) {
            Gear::library('Debugger');
            self::$_debugger = FirePHP::getInstance(true);
        }

        return self::$_debugger;
    }

    /**
     * Возращает указатель на класс почты
     * 
     * @return mixed
     */
    public static function getMail()
    {
        if (self::$_mail === null)
            self::$_mail = GFactory::getClass('Mail');

        return self::$_mail;
    }

    /**
     * Возращает указатель на класс кэша
     * 
     * @return mixed
     */
    public static function getCache()
    {
        if (self::$_cache === null)
            self::$_cache = GFactory::getClass('Cache');

        return self::$_cache;
    }

    /**
     * Возращает указатель на класс маршрутизатора
     * 
     * @return mixed
     */
    public static function getRouter()
    {
        if (self::$_router === null)
            self::$_router = self::getComponent('Router');

        return self::$_router;
    }

    /**
     * Возращает указатель на класс сессии
     * 
     * @return mixed
     */
    public static function getSession()
    {
        if (self::$_session === null) {
            self::$_session = self::getClass('Session');
        }

        return self::$_session;
    }

    /**
     * Возращает указатель на класс сессии
     * 
     * @return mixed
     */
    public static function getHash()
    {
        if (self::$_hash === null) {
            self::$_hash = self::getClass('Hash');
        }

        return self::$_hash;
    }

    /**
     * Возращает указатель на класс браузера
     * 
     * @return mixed
     */
    public static function getResponse()
    {
        if (self::$_response === null)
            self::$_response = GFactory::getClass('Response');

        return self::$_response;
    }

    /**
     * Возращает указатель на класс браузера
     * 
     * @return mixed
     */
    public static function getRequest()
    {
        if (self::$_request === null)
            self::$_request = GFactory::getClass('Request');

        return self::$_request;
    }

    /**
     * Возращает указатель на класс пользователей
     * 
     * @return mixed
     */
    public static function getUsers()
    {
        if (self::$_users === null)
            self::$_users = GFactory::getClass('Users');

        return self::$_users;
    }

    /**
     * Возращает указатель на класс контроллера
     * 
     * @return mixed
     */
    public static function getController($uri = '')
    {
        Gear::library('Users');
        Gear::controller('Controller');
        try {
            $uri = self::getUri($uri);
            $controller = ucfirst($uri->controller);
            $action = ucfirst($uri->action);
            $componentPath = 'components/' .  $uri->component;
            $path = $componentPath . $controller . '/';
            $file = $path . $action . '.php';
            $settingsPath = $componentPath . 'settings.php';
            // настройки контроллера
            if (!file_exists($settingsPath)) {
                throw new GException(
                    'Controller error',
                    'Can`t load settings',
                    $settingsPath,
                    true,
                    array('icon'      => 'icon-msg-error',
                          'buttons'   => array('ok' => 'OK', 'yes' => GText::_('Send error report', 'exception')),
                          'btnAction' => 'send report')
                );
            }
            $settings = require($settingsPath);
            $settings['controller'] = $uri->controller;
            $settings['action'] = $uri->action;
            // класс контроллера
            $className = GController::genClassName($settings['clsPrefix'], $controller, $action);
            // если сценарий контроллера не существует
            if (!file_exists($file))
                throw new GException(
                    'Controller error',
                    'Controller "%s" does not exist',
                    $className,
                    true,
                    array('icon'      => 'icon-msg-error',
                          'buttons'   => array('ok' => 'OK', 'yes' => GText::_('Send error report', 'exception')),
                          'btnAction' => 'send report')
                );
            // подлючение сценария контроллера
            require($file);
            // иницилизация языка
            $config = GFactory::getConfig();
            GLanguage::getInstance($config->get('LANGUAGE'), $config->get('LANGUAGES'));
            // добавления пакета локализации языка
            $languagePath = $path . 'languages/';
            // если в настройках указано какой язык (псевдоним) необходимо использовать для контроллера
            if (isset($settings['language']))
                GText::addText($languagePath, 'controller', $settings['language']);
            else
                GText::addText($languagePath);
            // проверка существования контроллера
            if (!class_exists($className))
                throw new GException(
                    'Controller error',
                    'The controller class "%s" is not found in the script "%s"',
                    array($className, $uri->get('file/controller')),
                    true,
                    array('icon'      => 'icon-msg-error',
                          'buttons'   => array('ok' => 'OK', 'yes' => GText::_('Send error report', 'exception')),
                          'btnAction' => 'send report')
                );
            $cnt = new $className($settings);
            $cnt->className = $className;
            $cnt->settingsPath = $settingsPath;
            $cnt->componentUri = $uri->component;
            $cnt->componentUrl = $uri->component . ROUTER_DELIMITER;
            $cnt->componentPath = $componentPath;
            $cnt->languagePath = $languagePath;
            $cnt->resourcePath = $path . 'resources/';
            $cnt->file = $file;
            $cnt->path = $componentPath . $controller;
            $cnt->construct();
        } catch(GException $e) {}

        return $cnt;
    }

    /**
     * Подключение сценария библиотеки
     * (путь "Gear/{$name}")
     * 
     * @param  string $name название файла
     * @param  string $class название класса
     * @param  boolean $attr атрибуты класса
     * @return mixed
     */
    public static function getClass($class, $filename = '', $attr = null)
    {
        if (empty($filename))
            $filename = $class;

        require_once(CORE_NAME . $filename . '.php');

        if ($class) {
            $class = 'G' . $class;
            if ($attr)
                return new $class($attr);
            else
                return new $class();
        }
    }

    /**
     * Подключение сценария библиотеки
     * (путь "Gear/{$name}")
     * 
     * @param  string $name название файла
     * @param  string $class название класса
     * @param  boolean $once подлючить сценарий только 1-н раз
     * @return mixed
     */
    public static function getIClass($class, $filename = '', $attr = null)
    {
        if (empty($filename))
            $filename = $class;

        @require_once(CORE_NAME . $filename . '.php');

        if ($class) {
            $class = 'G' . $class;
            if ($attr) {
                return $class::getInstance($attr);
            } else
                return $class::getInstance();
        }
    }
}
?>