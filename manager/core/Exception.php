<?php
/**
 * Gear Manager
 * 
 * Пакет обработки исключений
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Core
 * @package    Exception
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Exception.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Класс исключений
 * 
 * @category   Core
 * @package    Libraries
 * @subpackage Exception
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Exception.php 2016-01-01 12:00:00 Gear Magic $
 */
class GException extends Exception
{
    public static $location = '';

    /**
     * Если ответ уже отправлен
     *
     * @var boolean
     */
    public static $sent = false;

    /**
     * Виды ошибок PHP
     *
     * @var array
     */
    protected static $_errorType = array(
        E_ERROR             => 'Error',
        E_WARNING           => 'Warning',
        E_PARSE             => 'Parsing Error',
        E_NOTICE            => 'Notice',
        E_CORE_ERROR        => 'Core Error',
        E_CORE_WARNING      => 'Core Warning',
        E_COMPILE_ERROR     => 'Compile Error',
        E_COMPILE_WARNING   => 'Compile Warning',
        E_USER_ERROR        => 'User Error',
        E_USER_WARNING      => 'User Warning',
        E_USER_NOTICE       => 'User Notice',
        E_STRICT            => 'Runtime Notice',
        E_RECOVERABLE_ERROR => 'Catchable Fatal Error');

    /**
     * Ошибки пользователя
     *
     * @var array
     */
    public static $userErrors = array(
        // ошибки, предупреждения
        7     => array(E_ERROR, E_WARNING, E_PARSE),
        // ошибки, предупреждения, уведомления
        15    => array(E_ERROR, E_WARNING, E_PARSE, E_NOTICE),
        // все кроме уведомлений
        30711 => array(E_ERROR, E_WARNING, E_PARSE, E_USER_ERROR, E_USER_WARNING, E_STRICT, E_RECOVERABLE_ERROR)
    );

    /**
     * Шаблон ответа при исключении
     *
     * @var array
     */
    protected static $_response = array(
        'success'      => false,
        'status'       => '',
        'message'      => '',
        'action'       => 'error',
        'sound'        => true,
        'isAuthorized' => true,
        'data'         => array()
    );

    /**
     * Показывать детали ошибки
     *
     * @var boolean
     */
    protected static $isDetail = false;

    /**
     * Самому обрабатывать ошибки
     *
     * @var boolean
     */
    public static $selfHandler = true;

    /**
     * Вывод ошибки в JSON формате
     *
     * @var boolean
     */
    public static $toJson = true;

    /**
     * Установить ответ на ошибку
     * 
     * @param  array $response массив параметров ответаы
     * @return string
     */
    public static function setResponse($response)
    {
        self::$_response = array_merge(self::$_response, $response);
    }

    /**
     * Кодировать ответ в JSON
     * 
     * @param  string $message текст сообщения
     * @param  string $status статус сообщения
     * @return string
     */
    public static function toJson($status, $message)
    {
        self::$_response['status'] = empty($status) ? 'unknow' : $status;
        self::$_response['message'] = empty($message) ? 'unknow' : $message;

        return json_encode(self::$_response);
    }

    /**
     * Кодировать ответ в JSON
     * 
     * @param  string $message текст сообщения
     * @param  string $status статус сообщения
     * @return string
     */
    public static function toWrite($status, $message)
    {
        self::$_response['status'] = empty($status) ? 'unknow' : $status;
        self::$_response['message'] = empty($message) ? 'unknow' : $message;

        echo '<div class="gear-err ', strtolower(self::$_response['php']['level']), '"><div>',
             '<div class="gear-err-status">Gear: ', self::$_response['php']['level'], '</div>',
             '<div class="gear-err-file">', self::$_response['php']['file'], ' <b>(', self::$_response['php']['line'], ')</b></div>',
             '<div class="gear-err-msg">', self::$_response['php']['msg'], '</div>', '</div></div>';
    }

    /**
     * Установить вывод деталей ошибок
     * 
     * @param  boolean $value true или false
     * @return void
     */
    public static function setDetail($value)
    {
        self::$isDetail = $value;
    }

    /**
     * Скрывает пути
     * 
     * @param  boolean $path путь к сценарию
     * @return string
     */
    public static function hidePath($path)
    {
        $path = str_replace(array(chr(92), '.php'), array('/', ''), $path);
        $p = strpos($path, PATH_APPLICATION);
        if ($p !== false)
            return substr($path, $p + strlen(PATH_APPLICATION), strlen($path));

        return $path;
    }

    /**
     * Вывод обработчика ошибок по умолчанию (если установлен $handler=true)
     * 
     * @param  integer $level вид ошибки
     * @param  string $message сообщение
     * @param  string $file сценарий
     * @param  integer $line номер строки
     * @return void
     */
    public static function errorHandler($level = 0, $message, $file = '', $line = 0)
    {
        // если нет своего обработчика ошибок
        if (!GException::$selfHandler) return true;

        // настройки пользователя
        $session = GFactory::getSession();
        $settings = $session->get('user/settings');
        // если есть отладка
        if (isset($settings['debug/php/errors'])) {
            $userError = (int) $settings['debug/php/errors'];
            // не выводить
            if ($userError == 0) return;
            if (isset(self::$userErrors[$userError]))
                if (!in_array($level, self::$userErrors[$userError])) return;
        }
        // ответ
        self::$_response['dialog'] = array(
            'icon'      => 'icon-msg-error',
            'buttons'   => array('ok' => 'OK', 'yes' => GText::_('Send error report', 'exception')),
            'btnAction' => 'send report'
        );
        self::$_response['php'] = array(
            'level' => self::$_errorType[$level],
            'line'  => $line,
            'file'  => self::hidePath($file),
            'msg'   => strip_tags($message)
        );
        self::$sent = true;

        if (self::$toJson)
            die(self::toJson(GText::get('Controller error', 'exception'), ''));
        else
            self::toWrite(GText::get('Controller error', 'exception'), '');
    }

    /**
     * Вывод обработчика ошибок по умолчанию (если установлен $handler=true)
     * 
     * @param  integer $level вид ошибки
     * @param  string $message сообщение
     * @param  string $file сценарий
     * @param  integer $line номер строки
     * @return void
     */
    public static function catchFatalError()
    {
        if (self::$sent) exit;

        // если нет своего обработчика ошибок
        if (!GException::$selfHandler) return true;

        $error = error_get_last();
        if (is_null($error)) return;
        if ($error['type'] == 8192) return;
        // если ошибка неизвестна (скрипт отработал но в конце добавляется warning, пример был с "open_basedir")
        if ($error['line'] == 0) return;

        // ответ
        self::$_response['dialog'] = array(
            'icon'      => 'icon-msg-error',
            'buttons'   => array('ok' => 'OK', 'yes' => GText::_('Send error report', 'exception')),
            'btnAction' => 'send report'
        );
        self::$_response['php'] = array(
            'level' => isset(self::$_errorType[$error['type']]) ? self::$_errorType[$error['type']] : $error['type'],
            'line'  => $error['line'],
            'file'  => self::hidePath($error['file']),
            'msg'   => strip_tags($error['message'])
        );

        if (self::$toJson)
            die(self::toJson(GText::get('Controller error', 'exception'), ''));
        else
            self::toWrite(GText::get('Controller error', 'exception'), '');
    }

    /**
     * Конструктор
     * 
     * @param  string $message текст сообщения
     * @param  string $status статус сообщения
     * @param  string $detail детали сообщения
     * @param  boolean format форматированный вывод сообщения
     * @return void
     */
    public function __construct($status = '', $message = '', $detail = '', $format = true, $dialog = array())
    {
        // если нет своего обработчика ошибок
        if (!self::$selfHandler) {
            if (GException::$location) {
                header('location: ' . self::$location);
                exit;
            }
            return true;
        }
        $response = GFactory::getResponse();
        $response->setMsgError($status, $message, $detail, $format);
        self::$sent = true;
        // если есть настройки диалога
        if ($dialog)
            $response->set('dialog', $dialog);
        if (self::$toJson)
            die($response->toJson());
        else
            print_r($response);
    }
}


/**
 * Класс исключений для SQL запросов
 * 
 * @category   Core
 * @package    Libraries
 * @subpackage Exception
 * @copyright  Copyright (c) 2011-2014 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Exception.php 2014-08-01 12:00:00 Gear Magic $
 */
class GSqlException extends GException
{
    /**
     * Отправляет ошибку в лог
     * 
     * @return integer
     */
    public function toLog()
    {
        // соединение с базой данных
        GFactory::getDb()->connect();
        // таблица логов
        $table = new GDb_Table('gear_log_sql', 'log_id');
        $params = array(
            'user_id'        => GFactory::getSession()->get('user_id'),
            'log_query'      => GDb_Result::$sql,
            'log_error'      => GDb_Result::$error,
            'log_error_code' => (int) GDb_Result::$errorCode,
            'log_date'       => date('Y-m-d'),
            'log_time'       => date('H:i:s'),
        );
        if ($table->insert($params) === false) return 0;

        return $table->getLastInsertId();
    }

    /**
     * Конструктор
     * 
     * @param  string $message текст сообщения
     * @param  string $status статус сообщения
     * @param  string $detail детали сообщения
     * @param  boolean format форматированный вывод сообщения
     * @return void
     */
    public function __construct($status = '', $message = '', $detail = '', $format = true, $dialog = array())
    {
        // отправить в базу
        $logId = $this->toLog();
        // если есть статус
        if (empty($status))
            $status = 'Data processing error';
        // если есть сообщение
        if (empty($message)) {
            $message = 'Error executing a database query (malformed request)';
            $detail = array($logId);
        }
        $dialog = array('icon' => 'icon-msg-query');

        parent::__construct($status, $message, $detail, $format, $dialog);
    }
}

// устанавливаем свой обработчик фатальных ошибок
register_shutdown_function('GException::catchFatalError');
// устанавливаем свой обработчик ошибок
set_error_handler('GException::errorHandler', E_ALL & ~E_DEPRECATED);
?>