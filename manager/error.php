<?php
/**
 * Gear Manager
 *
 * Ошибки обработки интерфейса
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Error
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: error.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * @see Gear
 */
require('core/Gear.php');

// проверка ключа безопасности
if (!Gear::checkKey()) {
    header('HTTP/1.0 404 Not Found');
    exit;
}

// отключить вывод ошибок в JSON формате
GException::$toJson = false;

// инициализация конфигурации системы
$config = GFactory::getConfig();
$author = $config->get('AUTHOR');
// инициализация языка системы
$language = GLanguage::getInstance($config->get('LANGUAGE'), $config->get('LANGUAGES'));
$alias = $language->get('alias');
// инициализация языка интерфейса
$_ = GText::getSection('interface');
// параметры запроса
$get = array_keys($_GET);
if (sizeof($get) > 0)
    $err = $get[0];
else
    $err = 'unknow';
$set = isset($_GET['set']) ? $_GET['set'] : 'unknow';
$title = $_['Error'];
$text = $_['Unknow error'];
switch ($err) {
    case '404': $text = $_['404 error']; $title .= ' 404'; break;
    case '500': $text = $_['505 error']; $title .= ' 500'; break;
    case 'extensions': $text = sprintf($_['Extension error'], $set); break;
    case 'php': $text = sprintf($_['PHP error'], $set); break;
    case 'database': $text = $_['Database error']; break;
    case 'browser': $text = $_['Browser error']; break;
    case 'languages': $text = sprintf($_['Language error'], $set); break;
    case 'timezone': $text = sprintf($_['Timezone error'], $set); break;
    case 'session': $text = sprintf($_['Session error'], $set); break;
    case 'location': $text = sprintf($_['Location error'], $set); break;
    case 'hack': $text = $_['Hack']; $title = $_['Error access']; break;
    case 'refresh': $text = $_['Refresh warning']; break;
    case 'action': $text = $_['Action error']; break;
    case 'user': $text = $_['User error']; break;
    case 'blocked': $text = $_['Your account is blocked']; $title = $_['Error access']; break;
}
?>
<!DOCTYPE html>
<html lang="<?=$alias;?>">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="copyright" content="Gear Magic"/>
    <meta name="robots" content="none"/>

    <link rel="stylesheet" type="text/css" href="resources/themes/green/signin.css" />
    <link rel="stylesheet" type="text/css" href="resources/css/animate.min.css" />
    <link rel="shortcut icon" type="image/x-icon" href="resources/themes/green/images/favicon.ico" />

    <title><?=$title;?></title>
</head>

<body>
    <div class="wrapper">
        <div class="form form-error">
            <div class="title logo"></div>
            <div class="message">
                <h1><?=$title;?></h1>
                <?=$text;?>
            </div>
            <div class="buttons">
                <a class="btn-big btn-back" href="/<?=PATH_APPLICATION;?>" title="<?=$_['Login page'];?>"></a>
            </div>
        </div>
    </div>

    <div class="footer">
        <ul class="social">
            <li><a class="icon icon-twitter" href="#" title="Twitter" onclick="javascript:window.open('<?=$author['Twitter'];?>',null,null);"></a></li>
            <li><a class="icon icon-in" href="#" title="LinkedIn" onclick="javascript:window.open('<?=$author['LinkedIn'];?>',null,null);"></a></li>
            <li><a class="icon icon-google" href="#" title="Google +" onclick="javascript:window.open('<?=$author['Google +'];?>',null,null);"></a></li>
            <li><a class="icon icon-vk" href="#" title="ВКонтакте" onclick="javascript:window.open('http://vk.com/gearmagic',null,null);"></a></li>
            <li><a class="icon icon-skype" href="skype:<?=$author['Skype'];?>" title="Skype"></a></li>
        </ul>
        <span class="copyright">Copyright © 2011-2016 <a target="_blank" href="http://gearmagic.ru/">Gear Magic</a> <?=$_['All right reserved'];?></span>
        <span class="version"><?php printf($_['version %s'], GFactory::getClass('Version_Application', 'Version')->getVersionAlias());?></span>
    </div>
</body>
</html>