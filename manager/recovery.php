<?php
/**
 * Gear Manager (modification for Gear CMS)
 *
 * Восстановление учетной записи пользователя
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Recovery password
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: recovery.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * @see Gear
 */
require('core/Gear.php');

GException::$selfHandler = false;
// проверка ключа безопасности
if (!Gear::checkKey()) {
    header('location: /error-404/');
    exit;
}
$browser = GFactory::getBrowser();
// проверка на быстрое обновление
if ($browser->isQuicklyRefresh()) {
     header('location: error?refresh');
     exit;
}
// инициализация конфигурации системы
$config = GFactory::getConfig();
// если нет возможность восстановления
if (!$config->get('LOGON/RESTORE_ACCOUNT')) {
    header('location: /error-404/');
    exit;
}
$author = $config->get('AUTHOR');
// инициализация языка системы
$language = GLanguage::getInstance($config->get('LANGUAGE'), $config->get('LANGUAGES'));
$alias = $language->get('alias');
// версия
$core = GFactory::getClass('Version_Core', 'Version');
// инициализация хэша
$hash = GFactory::getHash();
$h = $hash->get();
// инициализация сессии
$sess = GFactory::getSession();
$sess->start();
// инициализация класса обработки формы
$input = GFactory::getInput();
// инициализация языка интерфейса
$_ = GText::getSection('interface');

$notice = $message = '';
// если действие "restore"
$isRestore = $input->has('restore');
if ($isRestore) {
    // если captcha правильна
    if (!($sess->has('captcha') && $sess->get('captcha') === $input->get('code'))) {
        $notice = 'error';
        $message = $_['You entered the code wrong!'];
    }
    // 1) установка соединения с базой данных
    if (empty($notice)) {
        try {
            $db = GFactory::getDb();
            $db->connect();
        } catch (GException $e) {
            $notice = 'error';
            $message = $_['Database error'];
        }
    }
    // 2) учетная запись пользователя
    if (empty($notice)) {
        try {
            $query = new GDb_Query();
            $sql = 'SELECT * '
                 . 'FROM `gear_users` `u`, `gear_user_profiles` `p` '
                 . 'WHERE `u`.`user_id`=`p`.`user_id` AND '
                 . '`p`.`contact_email`=' . $query->escapeStr($input->getString('email'));
            $user = $query->getRecord($sql);
        } catch(GException $e) {
            $notice = 'error';
            $message = $_['Action error'];
        }
        if (empty($user)) {
            $notice = 'error';
            $message = $_['User error'];
        }
    }
    // 3) подготовка параметров для восстановления
    if (empty($notice)) {
        $password = uniqid();
        $account = Gear::getPassword($password, true);
        $hash = Gear::getPasswordHash($account['salt']);
        // 4) отправка письма пользователю
        $mail = GFactory::getMail();
        // версия
        $core = GFactory::getClass('Version_Application', 'Version');
        if (!$mail->send(array(
            'to'       => $input->get('email'),
            'from'     => 'noreply@' . $_SERVER['HTTP_HOST'],
            'subject'  => $_['Title recovery account'],
            'template' => 'data/templates/restore-' . $alias . '.php',
            'type'     => 'template',
            'data'     => array(
                'version'  => $core->name,
                'host'     => $_SERVER['HTTP_HOST'],
                'password' => $password,
                'login'    => $user['user_name'],
                'link'     => 'http://' . $_SERVER['HTTP_HOST'] . '/'. PATH_APPLICATION 
                            . '/?restore=' . $hash . '&key=' . $config->get('SECURITY_KEY')
            )
        ))) {
            $notice = 'error';
            $message = $_['Action error'];
        }
    }
    // 5) добавление записи о восстановлении
    if (empty($notice)) {
        $notice = 'info';
        $message = $_['In your e-mail will be sent instructions'];
        try {
            $table = new GDb_Table('gear_user_restore', 'restore_id');
            $params = array(
                'restore_ipaddress'     => $_SERVER['REMOTE_ADDR'],
                'restore_email'         => $input->get('email'),
                'restore_hash'          => $hash,
                'restore_date'          => date('Y-m-d H:i:s'),
                'restore_actived'       => 0,
                'restore_password'      => $account['password'],
                'restore_password_salt' => $account['salt'],
                'user_id'               => $user['user_id']
            );
            $table->insert($params);
        } catch(GException $e) {
            $notice = 'error';
            $message = $_['Action error'];
        }
    }
}
$sess->set('captcha');
?>
<!DOCTYPE html>
<html lang="<?=$alias;?>">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="copyright" content="Gear Magic"/>
    <meta name="robots" content="none"/>

    <link rel="stylesheet" type="text/css" href="resources/themes/green/signin.css" />
    <link rel="stylesheet" type="text/css" href="resources/css/animate.min.css" />
    <link rel="shortcut icon" type="image/x-icon" href="resources/themes/green/images/favicon.ico" />

    <script type="text/javascript" src="resources/js/extjs/ext-core.js"></script>
    <script type="text/javascript" src="resources/js/manager/notice.min.js"></script>
    <script type="text/javascript" src="resources/js/manager/recovery.min.js"></script>
    <script type="text/javascript" src="resources/js/vendors/wow.min.js"></script>
    <script type="text/javascript" src="resources/locale/a-lang-<?=$alias;?>.min.js"></script>

    <title><?=$_['Recover account'];?></title>
</head>

<body>
    <div class="wrapper">
        <div class="notify">
            <div class="notice<?=$notice ? ' notice-' . $notice : '';?>">
                <span class="notice-icon"></span>
                <div><?=$message;?></div>
            </div>
        </div>
        <div class="form form-restore wow flipInX animated">
            <div class="title logo"></div>
            <form method="post" id="restore-form" action="">
                <input type="hidden" name="restore"/>
                <input type="hidden" id="language" name="language" value="<?=$alias;?>" />
                <h1><?=$_['Title recovery account'];?></h1>
                <fieldset>
                    <label>E-mail</label>
                    <div class="field field-mail"><input type="text" id="email" name="email" tabindex="1" placeholder="<?=$_['Enter the email'];?>" /></div>
                </fieldset>
                <fieldset>
                    <label><?=$_['Code'];?></label>
                    <div class="field field-code"><input type="text" id="code" name="code" tabindex="2"  placeholder="<?=$_['Enter the code'];?>"/></div>
                </fieldset>
                <fieldset class="captcha">
                    <img id="captcha" src="captcha.php?_dc=<?=time();?>"/>
                    <div class="buttons">
                        <a class="btn-sm btn-refresh" href="/<?=PATH_APPLICATION;?>recovery" title="<?=$_['Refresh code'];?>"></a>
                        <a class="btn-big btn-back" href="/<?=PATH_APPLICATION;?>" title="<?=$_['Login page'];?>"></a>
                        <a class="btn-big btn-apply" id="restore-submit" href="#" title="<?=$_['Recovery'];?>"></a>
                        
                    </div>
                </fieldset>
            </form>
        </div>
        <div class="langs">
            <ul><?php foreach ($config->get('LANGUAGES') as $key => $value) : ?>
                <li><a href="?language=<?=$key;?>" lang="<?=$key;?>" title="<?=$value['title'];?>"><?=$value['name'];?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>

    <div class="footer">
        <ul class="social">
            <li><a class="icon icon-twitter" href="#" title="Twitter" onclick="javascript:window.open('<?=$author['Twitter'];?>',null,null);"></a></li>
            <li><a class="icon icon-in" href="#" title="LinkedIn" onclick="javascript:window.open('<?=$author['LinkedIn'];?>',null,null);"></a></li>
            <li><a class="icon icon-google" href="#" title="Google +" onclick="javascript:window.open('<?=$author['Google +'];?>',null,null);"></a></li>
            <li><a class="icon icon-vk" href="#" title="ВКонтакте" onclick="javascript:window.open('http://vk.com/gearmagic',null,null);"></a></li>
            <li><a class="icon icon-skype" href="skype:<?=$author['Skype'];?>" title="Skype"></a></li>
        </ul>
        <span class="copyright">Copyright © 2011-2016 <a target="_blank" href="http://gearmagic.ru/">Gear Magic</a> <?=$_['All right reserved'];?></span>
        <span class="version"><?php printf($_['version %s'], $core->getVersionAlias());?></span>
    </div>
</body>
</html>