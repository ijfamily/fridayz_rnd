<?php
/**
 * Gear Manager
 *
 * Часто задаваемые вопросы
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    FAQ
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: faq.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * @see Gear
 */
require('core/Gear.php');

GException::$selfHandler = true;

// проверка ключа безопасности
if (!Gear::checkKey()) {
    header('HTTP/1.0 404 Not Found');
    exit;
}
// инициализация конфигурации системы
$config = GFactory::getConfig();
// инициализация сессии
$sess = GFactory::getSession();
$sess->start();
// инициализация языка системы
$language = GLanguage::getInstance($config->get('LANGUAGE'), $config->get('LANGUAGES'));
$lAlias = $language->get('alias');
$lId = $language->get('id');
$_ = require('faq/languages/' . $lAlias . '/Text.php');
// верефикация пользователя
if (!$sess->has('user_id')) {
    header('HTTP/1.0 404 Not Found');
    exit;
}
// запросы клиента
$uri = GFactory::getUri();
$faqId = $uri->getVar('id', 0);

// соединение с базой данных
GFactory::getDb()->connect();
$query = new GDb_Query();

$userSettings = $sess->get('user/settings');
// если выбрана страница
if ($faqId) {
    $data = array(
        '%footer%' => '', '%tipUp%' => $_['tipUp'], '%tipDown%' => $_['tipDown'], '%tipHome%' => $_['tipHome'],
        '%tipZoomin%' => $_['tipZoomin'], '%tipZoomout%' => $_['tipZoomout'],
        '%theme%' => $userSettings['theme']
    );
    // пункт раздела
    $sql = 'SELECT * FROM `gear_faq` `f`, `gear_faq_l` `l` '
         . 'WHERE `f`.`faq_id`=`l`.`faq_id` '
         . "AND `l`.`language_id`=$lId "
         . 'AND `f`.`faq_id`=' . $faqId;
    $page = $query->getRecord($sql);
    $isEmpty = empty($page);
    // если справки нет
    if ($isEmpty) {
        $data['%faq_name%'] = $_['msgNotExist'];
        $data['%faq_description%'] = $_['msgSelect'];
        $data['%faq_text%'] = '';
    } else {
        $data['%faq_name%'] = $page['faq_name'];
        $data['%faq_description%'] = $page['faq_description'];
        $data['%faq_text%'] = str_replace('%path%', 'data/' . $page['faq_id'] . '/', $page['faq_text']);

        // пункты раздела
        $sql = 'SELECT * FROM `gear_faq` `f`, `gear_faq_l` `l` '
             . 'WHERE `f`.`faq_id`=`l`.`faq_id` '
             . "AND `l`.`language_id`=$lId "
             . 'AND `f`.`faq_id`<>' . $page['faq_id'] . ' '
             . 'AND `f`.`faq_parent_id`=' . $page['faq_parent_id']
             .' ORDER BY `f`.`faq_index` ASC';
        $query->execute($sql);
        $html = '<div class="footer">';
        if ($query->getCountRecords() > 0) {
            $html .= '<h1 id="footer"><div class="ct">' . $_['tlFooter'] . '</div></h1><div class="ct"><ul>';
            while (!$query->eof()) {
                $item = $query->next();
                $html .= '<li><a href="?id=' . $item['faq_id'] . '">' . $item['faq_name'] . '</a></li>';
            }
            $html .= '</ul></div>';
        }
        $html .= '</div>';
        $data['%footer%'] = $html;
    }
    if (empty($page['faq_description']))
        $data['%faq_description%'] = '&nbsp;';
    // шаблон
    $template = 'faq/page.tpl';

// если главная страница
} else {
    $data = array(
        '%footer%' => '', '%tipUp%' => $_['tipUp'], '%tipDown%' => $_['tipDown'], '%tipHome%' => $_['tipHome'],
        '%tipZoomin%' => $_['tipZoomin'], '%tipZoomout%' => $_['tipZoomout'], '%tlHeader%' => $_['tlHeader'],
        '%theme%' => $userSettings['theme']
    );
    // список разделов
    $sql = 'SELECT * FROM `gear_faq` `f`, `gear_faq_l` `l` '
         . 'WHERE `f`.`faq_id`=`l`.`faq_id` '
         . "AND `l`.`language_id`=$lId "
         . 'AND `f`.`faq_parent_id` IS NULL '
         . 'ORDER BY `f`.`faq_index` ASC';
    $query->execute($sql);
    $html = '';
    while (!$query->eof()) {
        $item = $query->next();
        $html .= '<li><a href="#" section="' . $item['faq_id'] . '">' . $item['faq_name'] . '</a></li>';
    }
    $data['%features%'] = $html;
    // список пунктов раздела
    $sql = 'SELECT `c`.*, `p`.`pfaq_name` '
        .  'FROM (SELECT `l`.`faq_name` `pfaq_name`, `f`.`faq_id`, `f`.`faq_index` FROM `gear_faq` `f`, `gear_faq_l` `l` '
         . 'WHERE `f`.`faq_id`=`l`.`faq_id` AND `l`.`language_id`=' . $lId . ' AND `f`.`faq_parent_id` IS NULL) `p`, '
         . '(SELECT `l`.*, `f`.`faq_parent_id`, `f`.`faq_index` FROM `gear_faq` `f`, `gear_faq_l` `l` '
         . 'WHERE `f`.`faq_id`=`l`.`faq_id` AND `l`.`language_id`=' . $lId
         . ' AND `f`.`faq_parent_id` IS NOT NULL) `c` '
         . 'WHERE `p`.`faq_id`=`c`.`faq_parent_id` ORDER BY `p`.`faq_index`, `c`.`faq_index`';
    $query->execute($sql);
    $html = '';
    $first = true;
    $faqId = 0;
    while (!$query->eof()) {
        $item = $query->next();
        if ($first)
            $faqId = $item['faq_parent_id'];
        if ($faqId != $item['faq_parent_id'] || $first) {
            if (!$first)
                $html .= '</dl><div class="wrap"></div></div>' ."\r\n";
            $html .= '<div id="row' . $item['faq_parent_id'] . '"><h2><div>' . $item['pfaq_name'] . '</div></h2><dl>';
            $faqId = $item['faq_parent_id'];
        }
        $html .= '<dd url="?id=' . $item['faq_id'] . '"><img src="data/' . $item['faq_id'] . '/icon.png"/><div>';
        $html .= '<h4>' . $item['faq_name'] . '</h4><p>' . $item['faq_description'] . '</p></div></dd>';
        if ($first)
            $first = false;
    }
    $data['%sections%'] = $html;
    // шаблон
    $template = 'faq/index.tpl';
}

$text = file_get_contents($template, true);
echo strtr($text, $data);
?>