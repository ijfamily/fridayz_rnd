<?php
/**
 * Gear
 *
 * Пакет контроллеров "Тест скорости"
 * Группа пакетов     "Виджеты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Distributed under the Lesser General Public License (LGPL)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    GController_YWSpeedTest
 * @copyright  Copyright (c) 2013-2014 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: settings.php 2014-08-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Тест скорости передачи данных
 * Описание: Тест скорости передачи данных
 * ID класса: gcontroller_ywspeedtest_widget
 * Группа: Виджеты
 * Очищать: нет
 * Ресурс
 *    Пакет: widgets/speedtest/
 *    Контроллер: widgets/speedtest/Widget/
 *    Интерфейс: widget/interface/
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске: нет
 */

return array(
    // {Y}- модуль "System" {W} - пакет контроллеров "Widget speed test" -> YWSpeedTest
    'clsPrefix' => 'YWSpeedTest'
);
?>