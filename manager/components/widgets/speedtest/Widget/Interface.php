<?php
/**
 * Gear
 *
 * Контроллер         "Интерфейс виджета скорости передачи данных"
 * Пакет контроллеров "Тест скорости"
 * Группа пакетов     "Виджеты"
 * Модуль             "Система""
 *
 * LICENSE
 * 
 * Distributed under the Lesser General Public License (LGPL)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    GController_YWSpeedTest_Widget
 * @copyright  Copyright (c) 2011-2014 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Interface.php 2014-08-01 12:00:00 Gear Magic $
 */

Gear::controller('Interface');
Gear::ext('Container/Panel');

/**
 * Интерфейс виджета скорости передачи данных
 * 
 * @category   Gear
 * @package    GController_YWSpeedTest_Widget
 * @subpackage Interface
 * @copyright  Copyright (c) 2011-2014 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Interface.php 2014-08-01 12:00:00 Gear Magic $
 */
final class GController_YWSpeedTest_Widget_Interface extends GController_Interface
{
    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // вкладка (ExtJS class "Ext.Panel")
        $this->_cmp = new Ext_Panel(
            array('id'           => strtolower(__CLASS__),
                  'closable'     => true,
                  'autoScroll'   => true,
                  'bodyCssClass' => 'main-tab-body ',
                  'component'    => array('container' => 'content-tab', 'destroy' => true))
        );
    }

    /**
     * Возращает интерфейс компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // вкладка (ExtJS class "Ext.Panel")
        $this->_cmp->iconSrc = $this->resourcePath. 'icon.png';
        $this->_cmp->iconTpl = $this->resourcePath . 'shortcut.png';
        $this->_cmp->html = $this->renderHtml();
        $this->_cmp->title = $this->_['title_widget'];

        parent::getInterface();
    }

    /**
     * Вывод интерфейса компонента
     * 
     * @return void
     */
    protected function renderHtml()
    {
        ob_start();
?>
<style>
    .lnk-speedtest {
        border: 0px solid none;
        background: url("components/widgets/speedtest/Widget/resources/icon.png") 2px center no-repeat;
        padding-top: 10px;
        padding-left: 38px;
        padding-right: 5px;
        padding-bottom: 10px;
        margin: 5px;
        font-size: 13px;
        display: inline-block;
        width: 180px;
    }
    .lnk-speedtest:hover {
        text-decoration: none;
        background-color: #f7f9fd;
        border: 0px solid silver;
    }
    .lnk-speedtest:focus {
        border: 1px dotted silver;
    }
</style>
<table cellpadding="0" cellspacing="0" border="0" width="100%" height="100%">
    <tr><td valign="top" height="130px">
        <div style="padding:10px;">
            <h1 style="font-size:16px;color:#333333"><?php print $this->_['title_widget'];?> </h1>
        </div>
        <div style="padding-left: 15px;"> 
        <a class="lnk-speedtest" href="#" onclick="javascript:Ext.getDom('fr').src='components/widgets/speedtest/Widget/resources/index.html'">
        <?php print $this->_['label_1'];?>
        </a>
        <br />
        <a class="lnk-speedtest" href="#" onclick="javascript:Ext.getDom('fr').src='components/widgets/speedtest/Widget/resources/index1.html'">
        <?php print $this->_['label_2'];?>
        </a>
        </div>
        </td>
    </tr>
    <tr>
        <td height="100%" valign="center" align="center" style="padding:1px;">
            <iframe id="fr" src="components/widgets/speedtest/Widget/resources/index.html" width="100%" height="100%" style="border:0px none;"></iframe>
        </td>
    </tr>
</table>
<?php
        $html = ob_get_contents();
        ob_end_clean();

        return $html;
    }
}
?>