<?php
/**
 * Gear
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Distributed under the Lesser General Public License (LGPL)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2014 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Text.php 2014-08-01 12:00:00 Gear Magic $
 */

return array(
    'title_widget' => 'Тест скорости передачи данных',
    'label_1'      => 'от клиента к серверу',
    'label_2'      => 'от клиента в интернет'
);
?>