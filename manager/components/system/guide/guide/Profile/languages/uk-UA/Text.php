<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'profile_title_insert'  => 'Створення запису "Довідкова інформація"',
    'profile_title_update'  => 'Зміна запису "%s"',
    // поля
    'title_tab_common'        => 'Атрибуты',
    'title_tab_html'          => 'Статья',
    'label_guide_name'        => 'Название',
    'label_guide_description' => 'Описание',
    'label_guide_class'       => 'Класс',
    'label_guide_label'       => 'Метка',
    'label_guide_notice'      => 'Примечание',
    'label_pguide_name'       => 'Предок',
    'label_guide_folder'      => 'Путь к ресурсу',
    'title_tab_text'          => 'Текст'
);
?>