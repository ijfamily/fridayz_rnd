<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля справочной информации"
 * Пакет контроллеров "Справочная информация"
 * Группа пакетов     "Настройки"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YGuide_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные профиля справочной информации
 * 
 * @category   Gear
 * @package    GController_YGuide_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YGuide_Profile_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($_tableName)
     *
     * @var array
     */
    public $fields = array('guide_parent_id', 'guide_label', 'guide_class', 'guide_folder', 'guide_notice');

    /**
     * Первичный ключ таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'guide_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_guide';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_yguide_grid';

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        $sql = 'SELECT `gl`.*, `g`.* FROM `gear_guide` `g` JOIN `gear_guide_l` `gl` USING(`guide_id`) '
             . 'WHERE `gl`.`language_id`=' . $this->language->get('id') . ' AND `g`.`guide_id`=' . $this->uri->id;

        parent::dataView($sql);
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        $setTo = array();
        if ($record['guide_parent_id'] != 1) {
            $query = new GDb_Query();
            $sql = 'SELECT * FROM `gear_guide` `g` JOIN `gear_guide_l` `gl` USING(`guide_id`) '
                 . 'WHERE `guide_id`=' . $record['guide_parent_id'] . ' AND `gl`.`language_id`=' . $this->language->get('id');
            if (($parent = $query->getRecord($sql)) === false)
                throw new GSqlException();
        } else
            $parent = array('guide_name' => 'Guide', 'guide_id' => 1, 'type' => 'combo');

        // установка полей
        if (!empty($parent))
            $this->response->add('setTo', array(
                array('id' => 'parentGuideId', 'text' => $parent['guide_name'], 'value' => $parent['guide_id'], 'type' => 'combo')
            ));
        unset($record['guide_parent_id']);

        return $record;
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        // состояние профиля "правка"
        if ($this->isUpdate) {
            if (isset($params['guide_parent_id']))
                if (!is_numeric($params['guide_parent_id']))
                    unset($params['guide_parent_id']);
        }
    }

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['profile_title_update'], $record['guide_name']);
    }

    /**
     * Вставка данных
     * 
     * @param  array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataInsert($params = array())
    {
        parent::dataInsert();

        $data = $this->input->getBy(array('guide_name', 'guide_description', 'guide_html'));
        $data['guide_id'] = $this->_recordId;
        $table = new GDb_Table('gear_guide_l', 'guide_lang_id');
        $langs = $this->session->get('language/list');
        $count = count($langs);
        for ($i = 0; $i < $count; $i++) {
            $data['language_id'] = $langs[$i]['language_id'];
            $table->insert($data);
        }
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        $query = new GDb_Query();
        // удаление локализации языков ("gear_guide_l")
        $sql = 'DELETE FROM `gear_guide_l` WHERE `guide_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }
}
?>