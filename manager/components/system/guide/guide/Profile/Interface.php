<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля справочной информации"
 * Пакет контроллеров "Справочная информация"
 * Группа пакетов     "Настройки"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YGuide_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля справочной информации
 * 
 * @category   Gear
 * @package    GController_YGuide_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YGuide_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['profile_title_insert'],
                  'titleEllipsis' => 70,
                  'gridId'        => 'gcontroller_yguide_grid',
                  'width'         => 570,
                  'height'        => $this->isUpdate ? 250 : 400,
                  'resizable'     => false,
                  'stateful'      => false)
        );

        // поля вкладки "Атрибуты"
        $tabCommonItems = array();
        if ($this->isInsert) {
            array_push($tabCommonItems,
                array('xtype'      => 'textfield',
                      'fieldLabel' => $this->_['label_guide_name'],
                      'name'       => 'guide_name',
                      'maxLength'  => 255,
                      'anchor'     => '100%',
                      'allowBlank' => false),
                array('xtype'      => 'textfield',
                      'fieldLabel' => $this->_['label_guide_description'],
                      'name'       => 'guide_description',
                      'maxLength'  => 255,
                      'anchor'     => '100%',
                      'allowBlank' => false)
            );
        }
        array_push($tabCommonItems,
            array('xtype'      => 'mn-field-combo-tree',
                  'id'         => 'parentGuideId',
                  'fieldLabel' => $this->_['label_pguide_name'],
                  'resetable'  => false,
                  'width'      => 235,
                  'name'       => 'guide_parent_id',
                  'hiddenName' => 'guide_parent_id',
                  'treeWidth'  => 400,
                  'treeRoot'   => array('id' => 'root', 'expanded' => true, 'expandable' => true),
                  'allowBlank' => false,
                  'store'      => array(
                      'xtype' => 'jsonstore',
                      'url'   => $this->componentUrl . 'combo/nodes/'
                  )
            ),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_guide_notice'],
                  'name'       => 'guide_notice',
                  'maxLength'  => 255,
                  'width'      => 235,
                  'allowBlank' => true),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_guide_class'],
                  'name'       => 'guide_class',
                  'maxLength'  => 255,
                  'width'      => 235,
                  'allowBlank' => true),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_guide_label'],
                  'name'       => 'guide_label',
                  'maxLength'  => 255,
                  'width'      => 235,
                  'allowBlank' => true),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_guide_folder'],
                  'name'       => 'guide_folder',
                  'maxLength'  => 255,
                  'anchor'     => '100%',
                  'allowBlank' => true)
        );
        // вкладка "Атрибуты"
        $tabCommon = array(
            'title'      => $this->_['title_tab_common'],
            'layout'     => 'form',
            'labelWidth' => 100,
            'baseCls'    => 'mn-form-tab-body',
            'items'      => $tabCommonItems
        );

        // вкладка "Статья"
        $tabArticle = array(
            'title'      => $this->_['title_tab_text'],
            'layout'     => 'anchor',
            'labelWidth' => 100,
            'baseCls'    => 'mn-form-tab-body',
            'items'      => array(
                array('xtype'  => 'htmleditor',
                      'name'   => 'guide_html',
                      'anchor' => '100% 100%')
            )
        );

        // вкладки
        $tabPanel = array(
            'xtype'             => 'tabpanel',
            'layoutOnTabChange' => true,
            'deferredRender'    => false,
            'activeTab'         => 0,
            'enableTabScroll'   => true,
            'anchor'            => '100% 100%',
            'defaults'          => array('autoScroll' => true),
            'items'             => array($tabCommon)
        );
        if ($this->isInsert)
            $tabPanel['items'][] = $tabArticle;

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->addItems(array($tabPanel));
        $form->url = $this->componentUrl . 'profile/';

        parent::getInterface();
    }
}
?>