<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'grid_title'   => 'Разделы и пункты справки',
    'tooltip_grid' => 'Разделы и пункты справочной информации',
    'rowmenu_edit' => 'Редактировать',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">"Разделы и пункты справки"</span> ?',
    'text_btn_install'         => 'Установка',
    'tooltip_btn_install'      => 'Установка разделов и пунктов справки',
    // столбцы
    'header_guide_name'        => 'Название',
    'header_guide_description' => 'Описание',
    'header_guide_filename'    => 'Метка',
    'header_guide_count'       => 'Количество',
    'header_language_name'     => 'Язык',
    'header_pguide_name'       => 'Название "предка"',
    'header_guide_notice'      => 'Примечание',
    'header_guide_folder'      => 'Путь к ресурсу',
    'header_guide_class'       => 'Класс',
    'header_guide_label'       => 'Метка'
);
?>