<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка справочной информации"
 * Пакет контроллеров "Справочная информация"
 * Группа пакетов     "Настройки"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YGuide_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные списка справочной информации
 * 
 * @category   Gear
 * @package    GController_YGuide_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YGuide_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'guide_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_guide';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS `g`.*, `pg`.`pguide_name` '
            // table `gear_guide` `g`
          . 'FROM (SELECT `g`.*, `gl`.`guide_name`, `gl`.`guide_description` '
          . 'FROM `gear_guide` `g`, `gear_guide_l` `gl` '
          . 'WHERE `g`.`guide_id`=`gl`.`guide_id` AND '
          . '`g`.`guide_id`<>1 AND '
          . '`gl`.`language_id`=' . $this->language->get('id') . ') `g` '
            // join `gear_guide` `pg`
          . 'LEFT JOIN (SELECT `g`.`guide_id`, `gl`.`guide_name` `pguide_name` '
          . 'FROM `gear_guide` `g`, `gear_guide_l` `gl` '
          . 'WHERE `g`.`guide_id`=`gl`.`guide_id` AND '
          . '`gl`.`language_id`=' . $this->language->get('id') . ') `pg` '
          . 'ON `g`.`guide_parent_id`=`pg`.`guide_id` '
          . 'WHERE 1 %filter '
          . 'ORDER BY %sort '
          . 'LIMIT %limit';

        //  поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            'guide_id' => array('type' => 'integer'),
            'guide_name' => array('type' => 'string'),
            'guide_description' => array('type' => 'string'),
            'guide_notice' => array('type' => 'string'),
            'pguide_name' => array('type' => 'string'),
            'guide_label' => array('type' => 'string'),
            'guide_class' => array('type' => 'string'),
            'guide_folder' => array('type' => 'string')
        );
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        $query = new GDb_Query();
        // удаление локализации языков ("gear_guide_l")
        $sql = 'DELETE FROM `gear_guide_l` WHERE `guide_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_guide_l` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        $query = new GDb_Query();
        // удаление справки ("gear_guide")
        $sql = 'DELETE FROM `gear_guide`';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_guide` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление локализации языков справки ("gear_guide_l")
        $sql = 'DELETE FROM `gear_guide_l`';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_guide_l` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }
}
?>