<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка справочной информации"
 * Пакет контроллеров "Справочная информация"
 * Группа пакетов     "Настройки"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YGuide_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс списка справочной информации
 * 
 * @category   Gear
 * @package    GController_YGuide_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YGuide_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'guide_id';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'guide_name';

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('name' => 'guide_id', 'type' => 'integer'),
            array('name' => 'guide_name', 'type' => 'string'),
            array('name' => 'guide_description', 'type' => 'string'),
            array('name' => 'guide_notice', 'type' => 'string'),
            array('name' => 'pguide_name', 'type' => 'string'),
            array('name' => 'guide_label', 'type' => 'string'),
            array('name' => 'guide_class', 'type' => 'string'),
            array('name' => 'guide_folder', 'type' => 'string')
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $this->columns = array(
            array('xtype'     => 'numbercolumn'),
            array('xtype'     => 'rowmenu'),
            array('dataIndex' => 'guide_id',
                  'header'    => '<em class="mn-grid-hd-id"></em>ID',
                  'width'     => 52,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'guide_name',
                  'header'    => '<em class="mn-grid-hd-details"></em>'
                               . $this->_['header_guide_name'],
                  'width'     => 140,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'guide_description',
                  'header'    => $this->_['header_guide_description'],
                  'width'     => 170,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'pguide_name',
                  'header'    => $this->_['header_pguide_name'],
                  'width'     => 140,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'guide_notice',
                  'header'    => $this->_['header_guide_notice'],
                  'width'     => 140,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'guide_folder',
                  'header'    => $this->_['header_guide_folder'],
                  'width'     => 147,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'guide_class',
                  'header'    => $this->_['header_guide_class'],
                  'width'     => 167,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'guide_label',
                  'header'    => $this->_['header_guide_label'],
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string'))
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $this->_['grid_title'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_grid'],
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'profileUrl'    => $this->componentUrl . 'profile/')
        );

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "edit" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "добавить" (ExtJS class "Manager.button.InsertData")
        $group->items->add(array('xtype' => 'mn-btn-insert-data', 'gridId' => $this->classId));
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' => $this->classId));
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'      => 'mn-btn-clear-data',
                  'msgConfirm' => $this->_['msg_btn_clear'],
                  'gridId'     => $this->classId,
                  'isN'        => true)
        );
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "установка" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'   => 'mn-btn-widget',
                  'text'    => $this->_['text_btn_install'],
                  'tooltip' => $this->_['tooltip_btn_install'],
                  'icon'    => $this->resourcePath . 'icon-btn-install.png',
                  'url'     => $this->componentUri . '../install/' . ROUTER_DELIMITER . 'profile/interface/')
        );
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = $this->classId;
        $this->_cmp->tbar->items->add($group);

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $this->_cmp->cellTips = array(
            array('field' => 'guide_name',
                  'tpl'   => '<div class="mn-grid-cell-tooltip-tl">{guide_name}</div>'
                           . 'ID: <b>{guide_id}</b><br>'
                           . $this->_['header_guide_description'] . ': <b>{guide_description}</b><br>'
                           . $this->_['header_pguide_name'] . ': <b>{pguide_name}</b><br>'
                           . $this->_['header_guide_notice'] . ': <b>{guide_notice}</b><br>'
                           . $this->_['header_guide_folder'] . ': <b>{guide_folder}</b><br>'
                           . $this->_['header_guide_class'] . ': <b>{guide_class}</b><br>'
                           . $this->_['header_guide_label'] . ': <b>{guide_label}</b><br>'
                           ),
            array('field' => 'guide_description', 'tpl' => '{guide_description}'),
            array('field' => 'pguide_name', 'tpl' => '{pguide_name}'),
            array('field' => 'guide_notice', 'tpl' => '{guide_notice}'),
            array('field' => 'guide_label', 'tpl' => '{guide_label}'),
            array('field' => 'guide_class', 'tpl' => '{guide_class}'),
            array('field' => 'guide_folder', 'tpl' => '{guide_folder}')
        );

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $items = array(array('text'    => $this->_['rowmenu_edit'],
                             'iconCls' => 'icon-form-edit',
                             'url'     => $this->componentUrl . 'profile/interface/'),
                       array('xtype'   => 'menuseparator'));
        // список доступных языков
        $itemsL = array();
        $langs = $this->session->get('language/list');
        $count = count($langs);
        for ($i = 0; $i < $count; $i++) {
            if ($langs[$i]['language_enabled'])
                $itemsL[] = array(
                    'text'    => $langs[$i]['language_name'],
                    'params'  => 'lang=' . $langs[$i]['language_id'],
                    'iconCls' => 'icon-form-translate',
                    'url'     => $this->componentUrl . 'translation/interface/'
                );
        }
        array_push($items, $itemsL);
        $this->_cmp->rowMenu = array('xtype' => 'mn-grid-rowmenu', 'items' => $items);

        parent::getInterface();
    }
}
?>