<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск справочной информации"
 * Пакет контроллеров "Справочная информация"
 * Группа пакетов     "Настройки"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YGuide_Search
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск справочной информации
 * 
 * @category   Gear
 * @package    GController_YGuide_Search
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YGuide_Search_Data extends GController_Search_Data
{
    /**
     * Идентификатор компонента
     *
     * @var string
     */
    public $gridId = 'gcontroller_yguide_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_yguide_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор
     * Используется для инициализации атрибутов контроллер не переданного в конструктор
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'guide_id', 'as' => 'g.guide_id', 'label' => 'ID'),
            array('dataIndex' => 'guide_name', 'as' => 'g.guide_name', 'label' => $this->_['header_guide_name']),
            array('dataIndex' => 'guide_description', 'as' => 'g.guide_description', 'label' => $this->_['header_guide_description']),
            array('dataIndex' => 'pguide_name', 'as' => 'pg.pguide_name', 'label' => $this->_['header_pguide_name']),
            array('dataIndex' => 'guide_notice', 'as' => 'g.guide_notice', 'label' => $this->_['header_guide_notice']),
            array('dataIndex' => 'guide_folder', 'as' => 'g.guide_folder', 'label' => $this->_['header_guide_folder']),
            array('dataIndex' => 'guide_class', 'as' => 'g.guide_class', 'label' => $this->_['header_guide_class']),
            array('dataIndex' => 'guide_label', 'as' => 'g.guide_label', 'label' => $this->_['header_guide_label'])
            
        );
    }
}
?>