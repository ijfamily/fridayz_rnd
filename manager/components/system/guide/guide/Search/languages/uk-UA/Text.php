<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'search_title' => 'Поиск в списке "Разделы и пункты справки"',
    // поля
    'header_guide_name'        => 'Название',
    'header_guide_description' => 'Описание',
    'header_guide_filename'    => 'Метка',
    'header_guide_count'       => 'Количество',
    'header_language_name'     => 'Язык',
    'header_pguide_name'       => 'Название "предка"',
    'header_guide_notice'      => 'Примечание',
    'header_guide_folder'      => 'Путь к ресурсу',
    'header_guide_class'       => 'Класс',
    'header_guide_label'       => 'Метка'
);
?>