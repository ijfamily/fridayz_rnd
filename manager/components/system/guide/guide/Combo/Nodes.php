<?php
/**
 * Gear Manager
 *
 * Контроллер         "Тригер обработки справочной информации"
 * Пакет контроллеров "Справочная информация"
 * Группа пакетов     "Настройки"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YGuide_Combo
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Nodes.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Combo/Nodes');

/**
 * Тригер обработки справочной информации
 * 
 * @category   Gear
 * @package    GController_YGuide_Combo
 * @subpackage Nodes
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Nodes.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YGuide_Combo_Nodes extends GController_Combo_Nodes
{
    /**
     * Префикс полей для запросов в nested set
     *
     * @var string
     */
    public $fieldPrefix = 'guide';

    /**
     * Первичный ключ $tableName
     *
     * @var string
     */
    public $idProperty = 'guide_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_guide';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_yguide_docs';

    /**
     * Ресурс справки
     *
     * @var string
     */
    protected $_resource = '';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        $this->_resource = $this->config->getFromCms('Site', 'GUIDE/TREE', '');
    }

    /**
     * Вызывается перед предварительной обработкой запроса при формировании записей
     * 
     * @param  array $prefix префикс полей
     * @param  array $id идинд. узла дерева
     * @return string
     */
    protected function queryPreprocessing($prefix, $id)
    {
        if ($id == 'root')
            $query = 'SELECT `g1`.*, `g`.* '
                   . 'FROM `gear_guide` `g` '
                   . 'LEFT JOIN (SELECT COUNT(`guide_id`) `isOpen`, `guide_parent_id` '
                   . 'FROM `gear_guide` GROUP BY `guide_parent_id`) `g1` '
                   . 'ON `g`.`guide_id`=`g1`.`guide_parent_id` '
                   . 'WHERE `g`.`guide_parent_id` IS NULL ';
        else
            $query = 'SELECT `g1`.*, `gl`.*, `g`.* '
                   . 'FROM `gear_guide_l` `gl`, `gear_guide` `g` '
                   . 'LEFT JOIN (SELECT COUNT(`guide_id`) `isOpen`, `guide_parent_id` '
                   . 'FROM `gear_guide` GROUP BY `guide_parent_id`) `g1` '
                   . 'ON `g`.`guide_id`=`g1`.`guide_parent_id` '
                   . 'WHERE `g`.`guide_parent_id`=' . $id . ' AND '
                   . '`gl`.`guide_id`=`g`.`guide_id` AND '
                   . '`gl`.`language_id`=' . $this->language->get('id') . ' '
                   . 'ORDER BY `gl`.`guide_name`';

        return $query;
    }

    /**
     * Предварительная обработка узла дерева перед формированием массива записей JSON
     * 
     * @params array $node узел дерева
     * @params array $record запись
     * @return array
     */
    protected function nodesPreprocessing($node, $record)
    {
        if ($record['guide_id'] == 1)
            $record['guide_name'] = 'Guide';
        $node['text'] = $record['guide_name'];
        $node['doc'] = $record['guide_class'];
        $node['leaf'] = !($record['isOpen'] > 0);
        if ($node['leaf'])
            $node['iconCls'] = 'icon-item-book';

        return $node;
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function nodesView($sql = '')
    {
        parent::nodesAccessView();

        $text = file_get_contents($this->_resource);
        if ($text === false)
            throw new GException('Error', $this->_['It is impossible to gain access to the Help section']);

        $json = json_decode($text, true);
        $this->response->data = $json;
    }
}
?>