<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля вывода справочной информации"
 * Пакет контроллеров "Справочная информация"
 * Группа пакетов     "Настройки"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YGuide_Modal
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */


Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля вывода справочной информации
 * 
 * @category   Gear
 * @package    GController_YGuide_Modal
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YGuide_Modal_Interface extends GController_Profile_Interface
{
    /**
     * Документ
     *
     * @var string
     */
    protected $_doc = '';

    /**
     * Ресурс справки
     *
     * @var string
     */
    protected $_resource = 'http://cms.gearmagic.gm/guide/v3_0/';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        $this->_doc = $this->uri->getVar('doc');
        if ($this->_doc)
            $this->_doc = pathinfo($this->_doc, PATHINFO_FILENAME) . '.html';
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('id'            => $this->classId,
                  'title'         => $this->_['profile_title'],
                  'titleEllipsis' => 70,
                  'width'         => 800,
                  'height'        => 500,
                  'resizable'     => true,
                  'state'         => 'none',
                  'bodyStyle'     => 'padding:0',
                  'bodyCssClass' => 'mn-loading',
                  'stateful'      => false)
        );

        $st = $this->session->get('user/settings');
        if (isset($st['theme']))
            $theme = $st['theme'];
        else
            $theme = 'default';
        $form = $this->_cmp->items->get(0);
        $form->layout = 'fit';
        $form->items->addItems(array(array('xtype' => 'mn-iframe', 'url' => $this->config->getFromCms('Site', 'GUIDE/RESOURCE') . $this->_doc . '?view=modal&theme=' . $theme)));

        parent::getInterface();
    }
}
?>