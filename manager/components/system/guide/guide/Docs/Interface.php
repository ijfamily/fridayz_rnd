<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс вывода справочной информации"
 * Пакет контроллеров "Справочная информация"
 * Группа пакетов     "Настройки"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YGuide_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::ext('Container/Panel');
Gear::controller('Grid/Interface');

/**
 * Интерфейс вывода справочной информации
 * 
 * @category   Gear
 * @package    GController_YGuide_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YGuide_Docs_Interface extends GController_Interface
{
    /**
     * Вид документа
     *
     * @var string
     */
    protected $_docs = '';

    /**
     * Файл раздела справки
     *
     * @var string
     */
    protected $_fileName = 'index';

    /**
     * Ресурс справки
     *
     * @var string
     */
    protected $_resource = '';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        $this->_docs = $this->uri->getVar('docs');
        $this->_resource = $this->config->getFromCms('Site', 'GUIDE/RESOURCE', '');
        $this->_fileName = $this->uri->getVar('c', $this->_fileName);

        // панель (ExtJS class "Ext.Panel")
        $component = array(
            'id'        => strtolower(__CLASS__),
            'iconCls'   => 'icon-item-help',
            'closable'  => true,
            'component' => array('container' => 'content-tab', 'destroy' => true)
        );
        if ($this->_docs == 'help')
            $component['layout'] = 'border';
        $this->_cmp = new Ext_Panel($component);
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // панель (ExtJS class "Ext.Panel")
        switch ($this->_docs) {
            // помощь
            case 'help':
                $this->_cmp->iconTpl = $this->resourcePath . 'shortcut.png';
                $this->_cmp->tabTip = $this->_['panel_title_help'];
                $this->_cmp->title = $this->_['panel_title_help'];
                $this->_cmp->titleTpl = $this->_['panel_tooltip_help'];
            break;

            default:
                $this->_cmp->iconTpl = $this->resourcePath . 'shortcut.png';
                $this->_cmp->tabTip = $this->_['panel_title_help'];
                $this->_cmp->title = $this->_['panel_title_unknow'];
                $this->_cmp->titleTpl = $this->_['panel_tooltip_help'];
                $url = '';
        }
        if ($this->_docs == 'help') {
            $st = $this->session->get('user/settings');
            if (isset($st['theme']))
                $theme = $st['theme'];
            else
                $theme = 'default';

            //echo $this->_resource . $this->_fileName . '.html' . '?view=article&theme=' . $theme;
            //$this->_resource . $this->_fileName . '.html' . ($this->_fileName == 'index' ? '?view=article&theme=' . $theme : '')
            $this->_cmp->items->addItems(array(
                array('xtype'     => 'mn-tree-guide',
                      'targetDoc' => 'guide-doc',
                      'title'     => $this->_['treepanel_title'],
                      'width'     => 300,
                      'theme'     => $theme,
                      'urlDoc'    => $this->_resource),
                array('xtype'     => 'panel',
                      'layout'    => 'fit',
                      'region'    => 'center',
                      'margins'   => '3 3 3 0',
                      'bodyCssClass' => 'mn-loading',
                      'items'     => array(
                          'xtype' => 'mn-iframe',
                          'id'    => 'guide-doc',
                          'url'   => $this->_resource . $this->_fileName . '.html?view=article&theme=' . $theme)
                      )
                )
            );
        } else
            $this->_cmp->items->add(array('xtype' => 'mn-iframe', 'url' => $url));

        parent::getInterface();
    }
}
?>