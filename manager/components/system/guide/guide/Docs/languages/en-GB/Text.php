<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    'panel_tooltip_help'  => 'справочная информация',
    'panel_title_project' => 'About system',
    'panel_title_help'    => 'Help',
    'panel_title_support' => 'Technical support',
    'panel_title_unknow'  => 'Unknow page',
    'treepanel_title'     => 'Help'
);
?>