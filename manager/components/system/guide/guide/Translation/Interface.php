<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс локализации справочной информации"
 * Пакет контроллеров "Справочная информация"
 * Группа пакетов     "Настройки"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Controller_YGuide_Translation
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс локализации справочной информации
 * 
 * @category   Gear
 * @package    Controller_YGuide_Translation
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YGuide_Translation_Interface extends GController_Translation_Interface
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_yguide_grid';

    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        $data = array('guide_id' => $this->uri->id, 'language_alias' =>'', 'record_id' => '');

        // соединение с базой данных
        GFactory::getDb()->connect();
        $query = new GDb_Query();
        $sql = 'SELECT * FROM `gear_guide_l` WHERE `guide_id`=' . (int)$this->uri->id . ' AND `language_id`=' . $this->_languageId;
        $record = $query->getRecord($sql);
        if ($record === false)
            throw new GSqlException();
        // если есть запись
        if (!empty($record))
            $data['record_id'] = $record['guide_lang_id'];
        // выбранный язык
        $language = $this->language->getLanguagesBy('id', $this->_languageId);
        $data['language_alias'] = $language['alias'];

        return $data;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();
        $this->uri->id = $data['record_id'];

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => sprintf($this->_['title_translation_insert'], $data['language_alias']),
                  'titleEllipsis' => 70,
                  'gridId'        => 'gcontroller_yguide_grid',
                  'width'         => 600,
                  'height'        => 400,
                  'resizable'     => false,
                  'stateful'      => false)
        );

        // вкладка "общее"
        $tabCommon = array(
            'title'       => $this->_['title_tab_common'],
            'layout'      => 'form',
            'labelWidth'  => 85,
            'baseCls'     => 'mn-form-tab-body',
            'items'       => array(
                array('xtype'      => 'textfield',
                      'fieldLabel' => $this->_['label_guide_name'],
                      'name'       => 'guide_name',
                      'maxLength'  => 255,
                      'anchor'     => '100%',
                      'allowBlank' => false),
                array('xtype'      => 'textfield',
                      'fieldLabel' => $this->_['label_guide_description'],
                      'name'       => 'guide_description',
                      'maxLength'  => 255,
                      'anchor'     => '100%',
                      'allowBlank' => false)
            )
        );

        // вкладка "html"
        $tabHtml = array(
            'title'       => $this->_['title_tab_html'],
            'layout'      => 'anchor',
            'labelWidth'  => 85,
            'items'       => array(
                array('xtype'      => 'htmleditor',
                      'name'       => 'guide_html',
                      'anchor'     => '100% 100%',
                      'allowBlank' => false)
            )
        );

        // поля формы
        $items = array(
            array('xtype'             => 'tabpanel',
                  'layoutOnTabChange' => true,
                  'deferredRender'    => false,
                  'activeTab'         => 0,
                  'enableTabScroll'   => true,
                  'anchor'            => '100% 100%',
                  'defaults'          => array('autoScroll' => true),
                  'items'             => array($tabCommon, $tabHtml)
            )
        );
        // если состояние формы "вставка"
        if (empty($data['record_id'])) {
            $items[] = array(
                'xtype' => 'hidden',
                'value' => $this->_languageId,
                'name'  => 'language_id'
            );
            $items[] = array(
                'xtype' => 'hidden',
                'value' => $data['guide_id'],
                'name'  => 'guide_id'
            );
        }

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->addItems($items);
        $form->url = $this->componentUrl . 'translation/';

        parent::getInterface();
    }
}
?>