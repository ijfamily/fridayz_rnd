<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_translation_insert' => 'Create translating record (%s) "Help"',
    'title_translation_update' => 'Translating records (%s) "%s"',
    // поля
    'title_tab_common'        => 'Attributes',
    'title_tab_html'          => 'Article',
    'label_guide_name'        => 'Name',
    'label_guide_description' => 'Description'
);
?>