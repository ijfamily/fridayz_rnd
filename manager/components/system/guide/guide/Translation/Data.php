<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные локализации справочной информации"
 * Пакет контроллеров "Справочная информация"
 * Группа пакетов     "Настройки"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Controller_YGuide_Translation
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные локализации справочной информации
 * 
 * @category   Gear
 * @package    Controller_YGuide_Translation
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YGuide_Translation_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array('guide_name', 'guide_description', 'guide_html', 'guide_id', 'language_id');

    /**
     * Первичный ключ таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'guide_lang_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_guide_l';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ycontrollers_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_translation_update'], $record['language_alias'], $record['guide_name']);
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        $sql = 'SELECT `g`.*, `gl`.*, `l`.`language_alias` '
             . 'FROM `gear_guide` `g`, `gear_guide_l` `gl`, `gear_languages` `l` '
             . 'WHERE `l`.`language_id`=`gl`.`language_id` AND '
             . '`gl`.`guide_id`=`g`.`guide_id` AND '
             . '`gl`.`guide_lang_id`=' . $this->uri->id;

        parent::dataView($sql);
    }
}
?>