<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_install' => 'Установка справки',
    'text_btn_install'      => 'Установить',
    // поля
    'title_tab_file'     => 'Файл "%s"',
    'label_clear_guide'  => 'перед установкой удалить разделы и пункты справки',
    'label_remove_files' => 'после установки удалить временные файлы',
    'label_guide_use'    => 'Установить',
    'tip_guide_use'      => 'Установить раздел справки',
    'label_guide_index'  => 'Порядок',
    'tip_guide_index'    => 'Порядок установки раздела справки',
    'label_hd'           => 'Раздел / пункт "%s"',
    'label_path'         => 'Путь к разделу',
    'tip_path'           => 'Путь к разделу справки',
    'label_to'           => 'Добавить в',
    'tip_to'             => 'Добавить статью в ветвь дерева с меткой ',
    'label_class'        => 'Класс',
    'tip_class'          => 'Класс компонента системы, для вызова справки из компонента',
    'label_folder'       => 'Папка',
    'tip_folder'         => 'Папка с ресурсами статьи',
    'label_notice'       => 'Примечание',
    'tip_notice'         => 'Примечание (любой текст)',
    'label_name'         => 'Название',
    'tip_name'           => 'Название статьи',
    'label_label'        => 'Метка',
    'tip_label'          => 'Метка по каторой определяется ветвь дерева для добавления в нее узлов',
    'label_description'  => 'Описание',
    'tip_description'    => 'Описание статьи',
    'label_file'         => 'Файл (.html)',
    'tip_file'           => 'Файл статьи c расширением ".html"',
    'label_file1'        => 'Файл (.json)',
    'tip_file1'          => 'Файл раздела справки с расширением ".json"',
    'label_separator_error' => 'Ошибки установки',
    'value_unknow'          => '[неизвестно]',
    'value_dir_not_exist'    => 'каталог <b>"%s"</b> не существует',
    'value_file_not_exist'   => 'файл <b>"%s"</b> не существует',
    'value_status_error'     => 'В процесе анализа данных возникли ошибки ...',
    // сообщения
    'msg_index_exist'     => 'Файлы установки содержат одинаковые порядковые номера!',
    'msg_empty_files'     => 'Не выбраны файлы для установки!',
    'msg_no_files'        => 'Нет файлов ".json" в каталоге "%s" для установки разделов справки!',
    'msg_status_install'  => 'Установка',
    'msg_success_install' => 'Установка выполнена успешно'
);
?>