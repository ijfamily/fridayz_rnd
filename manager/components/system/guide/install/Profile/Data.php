<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля установщика справки"
 * Пакет контроллеров "Установка справки"
 * Группа пакетов     "Справочная информация"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YInstallGuide_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2014-12-04 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные профиля установщика справки
 * 
 * @category   Gear
 * @package    GController_YInstallGuide_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YInstallGuide_Profile_Data extends GController_Profile_Data
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_yinstallguide_profile';

    /**
     * Путь установки
     *
     * @var string
     */
    public $installDir = 'guide/data/';

    /**
     * Возращает список файлов
     * 
     * @return array
     */
    protected function getFiles()
    {
        $guide = $this->input->get('guide');
        $files = array();
        foreach ($guide as $i => $item) {
            if (!$item['use']) continue;
            $index = (int) $item['index'];
            if (isset($files[$index]))
                throw new GException('Data processing error', $this->_['msg_index_exist']);
            $files[$index] = $item['file'];
        }
        if (empty($files))
            throw new GException('Data processing error', $this->_['msg_empty_files']);
        ksort($files);

        return $files;
    }

    /**
     * Удаление всех записей справки
     * 
     * @return void
     */
    protected function filesClear()
    {
        Gear::library('File');

        $query = new GDb_Query();
        $sql = 'SELECT * FROM `gear_guide`';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        while (!$query->eof()) {
            $rec = $query->next();
            if ($rec['guide_folder'])
                GDir::clear($this->installDir . $rec['guide_folder']);
        }
    }

    /**
     * Удаление всех записей справки
     * 
     * @return void
     */
    protected function dataClear()
    {
        $query = new GDb_Query();
        // удаление справки ("gear_guide")
        $sql = 'DELETE FROM `gear_guide`';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_guide` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление локализации языков справки ("gear_guide_l")
        $sql = 'DELETE FROM `gear_guide_l`';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_guide_l` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Вставка данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataInsert($params = array())
    {
        parent::dataAccessInsert();

        $this->response->set('action', 'install');
        $this->response->setMsgResult($this->_['msg_status_install'], $this->_['msg_success_install'] , true);

        // инициализация установщика
        $install = GFactory::getClass('InstallGuide', 'Install');
        $install->path = $this->installDir;
        // установочные файлы (.json)
        $files = $this->getFiles();
        // удалить разделы и пункты справки
        if ($this->input->get('guide_clear', false)) {
            $this->dataClear();
        }
        // установка справки
        foreach ($files as $index => $file) {
            $json = file_get_contents($this->installDir . $file);
            if ($json === false)
                throw new GException('Data processing error', 'Can`t open file "%s" for reading', $file);
            $install->fromJson($json);
            // проверка json представления
            $install->check();
            // добавить данные
            $install->execute($install->data);
        }
        // удаление временных файлов
        if ($this->input->get('guide_remove_files', false)) {
            Gear::library('File');
            GDir::clearByExt($this->installDir, array('json', 'html'), true);
        }
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {}
}
?>