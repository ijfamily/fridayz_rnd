<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля установки справки"
 * Пакет контроллеров "Установка справки"
 * Группа пакетов     "Справочная информация"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YInstallGuide_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2014-12-04 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля установки справки
 * 
 * @category   Gear
 * @package    GController_YInstallGuide_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YInstallGuide_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_yinstallguide_profile';

    /**
     * Путь установки
     *
     * @var string
     */
    public $installDir = 'guide/data/';

    /**
     * Установщик
     *
     * @var object
     */
    public $install;

    /**
     * Ошибки на каждой вкладке
     *
     * @var array
     */
    protected $_errors = array();

    /**
     * Если есть ошибки установки
     *
     * @var boolean
     */
    protected $_isError = false;

    /**
     * Возвращает интерфейс поля вкладки
     * 
     * @param  array $item запись
     * @param  string $name название поля
     * @return void
     */
    protected function getItem($item, $name, $modPath = '')
    {
        if (empty($item[$name])) {
            $value = $this->_['value_unknow'];
            $class = 'mn-field-value-none';
        } else {
            $value = $item[$name];
            $class = 'mn-field-value-info';
        }
        switch ($name) {
            // папка установки
            case 'folder':
                if (!empty($item[$name]))
                    if (!is_dir($this->installDir . $value)) {
                        $class = 'mn-field-value-error';
                        $this->_errors[] = sprintf($this->_['value_dir_not_exist'], $this->installDir . $value);
                    }
                break;

            // файл статьи
            case 'file':
                if (!empty($item[$name])) {
                    $folder = empty($item['folder']) ? '' : $item['folder'];
                    if (!file_exists($this->installDir . $modPath . $value)) {
                        $class = 'mn-field-value-error';
                        $this->_errors[] = sprintf($this->_['value_file_not_exist'], $this->installDir . $modPath . $value);
                    }
                }
                break;
        }

        return array(
            'xtype'      => 'displayfield',
            'fieldClass' => $class,
            'fieldLabel' => $this->_['label_' . $name],
            'labelTip'   => $this->_['tip_' . $name],
            'value'      => $value
        );
    }

    /**
     * Возвращает интерфейс вкладки
     * 
     * @param  string $filename название файла
     * @param  integer $tabIndex индекс вкладки
     * @return void
     */
    protected function getTabInterface($filename, $tabIndex)
    {
        $json = file_get_contents($this->installDir . $filename);
        // если не открывается
        if ($json === false)
            throw new GException('Data processing error', 'Can`t open file "%s" for reading', $filename);
        // данные в json
        $data = $this->install->fromJson($json);

        $errors = array();
        $groups = array(
            array('xtype'      => 'mn-field-chbox',
                  'fieldLabel' => $this->_['label_guide_use'],
                  'labelTip'   => $this->_['tip_guide_use'],
                  'name'       => 'guide[' . $tabIndex . '][use]',
                  'default'    => 1),
            array('xtype'      => 'numberfield',
                  'fieldLabel' => $this->_['label_guide_index'],
                  'labelTip'   => $this->_['tip_guide_index'],
                  'allowBlank' => false,
                  'width'      => 70,
                  'name'       => 'guide[' . $tabIndex . '][index]',
                  'value'      => $tabIndex + 1),
            array('xtype'      => 'displayfield',
                  'fieldClass' => 'mn-field-value-info',
                  'fieldLabel' => $this->_['label_file1'],
                  'labelTip'   => $this->_['tip_file1'],
                  'value'      => $filename),
            array('xtype'      => 'hidden',
                  'name'       => 'guide[' . $tabIndex . '][file]',
                  'value'      => $filename)
        );
        if (empty($data['path'])) {
            $value = $this->_['value_unknow'];
            $class = 'mn-field-value-error';
        } else {
            $value = $data['path'];
            $class = 'mn-field-value-info';
        }
        $groups[] = array(
            'xtype'      => 'displayfield',
            'fieldClass' => $class,
            'fieldLabel' => $this->_['label_path'],
            'labelTip'   => $this->_['tip_path'],
            'value'      => $value
        );
        $groups[] = array('xtype' => 'displayfield', 'value' => '<br>');
        // список статей
        $articles = $data['articles'];
        for ($i = 0; $i < sizeof($articles); $i++) {
            if (empty($articles[$i]['name']))
                $value = $this->_['value_unknow'];
            else
                $value = $articles[$i]['name'];
            $groups[] = array(
                'xtype' => 'mn-field-separator',
                'html'  => sprintf($this->_['label_hd'], $value)
            );
            $groups[] = $this->getItem($articles[$i], 'name');
            $groups[] = $this->getItem($articles[$i], 'description');
            $groups[] = $this->getItem($articles[$i], 'notice');
            $groups[] = $this->getItem($articles[$i], 'to');
            $groups[] = $this->getItem($articles[$i], 'label');
            $groups[] = $this->getItem($articles[$i], 'class');
            $groups[] = $this->getItem($articles[$i], 'folder', $data['path']);
            $groups[] = $this->getItem($articles[$i], 'file', $data['path']);
            $groups[] = array('xtype' => 'displayfield', 'value' => '<br>');
        }

        // если есть ошибки обработки
        if ($this->_errors) {
            $this->_isError = true;
            $groups[] = array('xtype' => 'mn-field-separator', 'html' => $this->_['label_separator_error']);
            for ($i = 0; $i < sizeof($this->_errors); $i++) {
                $groups[] = array(
                    'xtype'      => 'displayfield',
                    'fieldClass' => 'mn-field-value-error',
                    'hideLabel'  => true,
                    'value'      => $this->_errors[$i]
                );
            }
        }

        return $groups;
    }

    /**
     * Возвращает интерфейс
     * 
     * @return void
     */
    protected function getInterface()
    {
        Gear::library('File');

        // инициализация установщика
        $this->install = GFactory::getClass('InstallGuide', 'Install');
        // список файлов в каталоге
        $files = GDir::getFiles($this->installDir, array('json'));
        if (empty($files))
            throw new GException('Error', sprintf($this->_['msg_no_files'], $this->installDir));

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_install'],
                  'titleEllipsis' => 45,
                  'width'         => 600,
                  'resizable'     => false,
                  'stateful'      => false,
                  'btnInsertIcon' => 'icon-fbtn-setup',
                  'btnInsertText' => $this->_['text_btn_install']
            )
        );

        // вкладки
        $tabPanel = array(
            'xtype'             => 'tabpanel',
            'layoutOnTabChange' => true,
            'deferredRender'    => false,
            'activeTab'         => 0,
            'enableTabScroll'   => true,
            'anchor'            => '100%',
            'height'            => 337,
            'bodyStyle'         => 'margin-bottom:10px;',
            'items'             => array()
        );
        // создание вкладок
        for ($i = 0; $i < sizeof($files); $i++) {
            $title = basename($files[$i], '.json');
            $tabItem = array(
                'title'       => strtoupper($title),
                'layout'      => 'form',
                'labelWidth'  => 120,
                'baseCls'     => 'mn-form-tab-body',
                'bodyStyle'   => 'background: url("' . $this->resourcePath. 'tab-bg.png") no-repeat center center;',
                'autoScroll'  => true,
                'items'       => $this->getTabInterface($files[$i], $i)
            );
            $tabItem['iconSrc'] = $this->resourcePath . 'icon-tab-code' . ($this->_errors ? '1' : '') . '.png';
            $tabPanel['items'][] = $tabItem;
            $this->_errors = array();
        }

        // поля формы
        $formItems = array($tabPanel);

        // если есть ошибки в установке
        if ($this->_isError) {
            $this->_cmp->height = 440;
            $this->state = 'info';
            $formItems[] = array(
                'xtype'      => 'displayfield',
                'fieldClass' => 'mn-field-value-error',
                'hideLabel'  => true,
                'value'      => $this->_['value_status_error']
            );
        } else {
            $this->_cmp->height = 470;
            $formItems[] = array(
                'xtype'      => 'mn-field-chbox',
                'fieldLabel' => $this->_['label_clear_guide'],
                'name'       => 'guide_clear',
                'disabled'   => $this->_isError,
                'default'    => 1
            );
            $formItems[] = array(
                'xtype'      => 'mn-field-chbox',
                'fieldLabel' => $this->_['label_remove_files'],
                'name'       => 'guide_remove_files',
                'disabled'   => $this->_isError,
                'default'    => 1
            );
        }

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->labelWidth = 320;
        $form->items->addItems($formItems);
        $form->url = $this->componentUrl . 'profile/';

        parent::getInterface();
    }
}
?>