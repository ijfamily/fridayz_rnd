<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Установка справки"
 * Группа пакетов     "Справочная информация"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YInstallGuide
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Установка справки
 * Описание: Установка справки системы
 * ID класса: gcontroller_yinstallguide_profile
 * Группа: Система / Компоненты
 * Очищать: нет
 * Ресурс
 *    Пакет: system/guide/install/
 *    Контроллер: system/guide/install/Profile/
 *    Интерфейс: profile/interface/
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске: нет
 */

return array(
    // {Y}- модуль "System" {C} - пакет контроллеров "Components install" -> YInstallGuide
    'clsPrefix' => 'YInstallGuide'
);
?>