<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_update' => 'Настройки интерфейса панели',
    'text_btn_help'        => 'Справка',
    // поля формы
    'title_tab_interface'  => 'Персонализация',
    'title_tab_control'    => 'Общие настройки',
    'label_footer'         => 'Настройки вступят в силу только после выполненых изменений',
    'label_type_shortcuts' => 'Вид',
    'title_shortcuts'      => 'Ярлыки рабочего стола',
    'label_desktop_view'   => 'Вид рабочего стола',
    'label_view'           => 'Вид',
    'label_theme'          => 'Тема',
    'label_separator_debug' => 'Отладка',
    'label_debug_js_core'   => 'отладка ядра системы (JavaScript)',
    'label_debug_js'        => 'отладка виджетов и плагинов системы (JavaScript)',
    'label_debug_php'       => 'отладка компонентов системы (PHP)',
    'label_debug_php_errors' => 'вывод ошибок (PHP)',
    'label_debug_info'       => '<div class="mn-field-comment">Для отладки PHP сценариев установите для Mozilla Firefox расширение <a target="blank" href="http://www.firephp.org/">FirePHP</a>, <br>'
                             . 'для отладки JavaScript - <a target="blank" href="http://www.getfirebug.com/">Firebug</a>',
    'label_separator_msg'     => 'Сообщения',
    'label_receive_mails'     => 'получать системные письма от пользователей',
    'label_receive_messages'  => 'получать системные сообщения от пользователей',
    'label_receive_js_errors' => 'получать сообщения о ошибках JavaScript',
    'label_sound'             => 'Звук',
    'label_separator_grid'    => 'Управление списком',
    'label_grid_columns'      => 'сбросить положение столбцов',
    'label_grid_bg_use'       => 'использовать в фоне списка изображение',
    'label_grid_bg_color'     => 'цвет фона списка (если нет изображения)',
    'title_tab_date'          => 'Дата и время',
    'label_separator_date'    => 'Шаблон даты',
    'fieldset_title_day'      => 'День',
    'label_char_d'            => 'Символ <b>"d"</b>',
    'value_char_d'            => 'День месяца, 2 цифры с ведущим нулём<br>Например: от 01 до 31',
    'label_char_D'            => 'Символ <b>"D"</b>',
    'value_char_D'            => 'Текствое представление дня недели, 3 символа<br>Например: от Mon до Sun',
    'label_char_j'            => 'Символ <b>"j"</b>',
    'value_char_j'            => 'День месяца без ведущего нуля<br>Например: от 1 до 31',
    'label_char_L'            => 'Символ <b>"L"</b>',
    'value_char_L'            => 'Полное наименование дня недели<br>Например: от  Sunday до Saturday',
    'label_char_N'            => 'Символ <b>"N"</b>',
    'value_char_N'            => 'Порядковый номер дня недели в соответствии со стандартом ISO-8601<br>Например: от 1 (понедельник) до 7 (воскресенье)',
    'label_char_S'            => 'Символ <b>"S"</b>',
    'value_char_S'            => 'Английский суффикс порядкового числительного дня месяца, 2 символа<br>Например: st, nd, rd или th. Применяется совместно с j ',
    'label_char_w'            => 'Символ <b>"w"</b>',
    'value_char_w'            => 'Порядковый номер дня недели<br>Например: от 0 (воскресенье) до 6 (суббота)',
    'label_char_z'            => 'Символ <b>"z"</b>',
    'value_char_z'            => 'Порядковый номер дня в году (начиная с 0)<br>Например: От 0 до 365',
    'fieldset_title_week'     => 'Неделя',
    'label_char_W'            => 'Символ <b>"W"</b>',
    'value_char_W'            => 'Порядковый номер недели года в соответствии со стандартом ISO-8601; недели начинаются с понедельника<br>Например: 42 (42-я неделя года)',
    'fieldset_title_month'    => 'Месяц',
    'label_char_F'            => 'Символ <b>"F"</b>',
    'value_char_F'            => 'Полное наименование месяца, например January или March<br>Например: January through December',
    'label_char_m'            => 'Символ <b>"m"</b>',
    'value_char_m'            => 'Порядковый номер месяца с ведущим нулём<br>Например: от 01 до 12',
    'label_char_M'            => 'Символ <b>"M"</b>',
    'value_char_M'            => 'Сокращенное наименование месяца, 3 символа<br>Например: от Jan до Dec',
    'label_char_n'            => 'Символ <b>"n"</b>',
    'value_char_n'            => 'Порядковый номер месяца без ведущего нуля<br>Например: от 1 до 12',
    'label_char_t'            => 'Символ <b>"t"</b>',
    'value_char_t'            => 'Количество дней в указанном месяце<br>Например: от 28 до 31',
    'fieldset_title_year'     => 'Год',
    'label_char_L'            => 'Символ <b>"L"</b>',
    'value_char_L'            => 'Признак високосного года<br>Например: 1, если год високосный, иначе 0',
    'label_char_O'            => 'Символ <b>"O"</b>',
    'value_char_O'            => 'Номер года в соответствии со стандартом ISO-8601. Имеет то же значение, что и Y, кроме случая, когда номер недели ISO (W) принадлежит предыдущему или следующему году; тогда будет использован год этой недели.<br>Например: 1999 или 2003',
    'label_char_Y'            => 'Символ <b>"Y"</b>',
    'value_char_Y'            => 'Порядковый номер года, 4 цифры<br>Например: 1999, 2003',
    'label_char_y'            => 'Символ <b>"y"</b>',
    'value_char_y'            => 'Номер года, 2 цифры<br>Например: 99, 03',
    'fieldset_title_time'     => 'Время',
    'label_char_a'            => 'Символ <b>"a"</b>',
    'value_char_a'            => 'Ante meridiem (англ. "до полудня") или Post meridiem (англ. "после полудня") в нижнем регистре<br>Например: am или pm',
    'label_char_A'            => 'Символ <b>"A"</b>',
    'value_char_A'            => 'Ante meridiem или Post meridiem в верхнем регистре<br>Например: AM или PM',
    'label_char_B'            => 'Символ <b>"B"</b>',
    'value_char_B'            => 'Время в формате Интернет-времени (альтернативной системы отсчета времени суток)<br>example: от 000 до 999',
    'label_char_g'            => 'Символ <b>"g"</b>',
    'value_char_g'            => 'Часы в 12-часовом формате без ведущего нуля<br>Например: от 1 до 12',
    'label_char_G'            => 'Символ <b>"G"</b>',
    'value_char_G'            => 'Часы в 24-часовом формате без ведущего нуля<br>Например: от 0 до 23',
    'label_char_h'            => 'Символ <b>"h"</b>',
    'value_char_h'            => 'Часы в 12-часовом формате с ведущим нулём<br>Например: от 01 до 12',
    'label_char_H'            => 'Символ <b>"H"</b>',
    'value_char_H'            => 'Часы в 24-часовом формате с ведущим нулём<br>Например: от 00 до 23',
    'label_char_i'            => 'Символ <b>"i"</b>',
    'value_char_i'            => 'Минуты с ведущим нулём<br>Например: от 00 до 59',
    'label_char_s'            => 'Символ <b>"s"</b>',
    'value_char_s'            => 'Секунды с ведущим нулём<br>Например: от 00 до 59',
    'label_char_u'            => 'Символ <b>"u"</b>',
    'value_char_u'            => 'Микросекунды <br>Например: 54321',
    'label_format_date'       => 'Формат даты',
    'label_format_time'       => 'Формат времени',
    'label_separator_time'    => 'Шаблон времени',
    'label_format_datetime'   => 'Формат даты и времени',
    // сообщения
    'msg_create_file' => 'Невозможно создать файл настройки сайта "%s"',
    // типы
    'data_shortcuts' => array(array(1, 'Ярлыки'), array(2, 'Компоненты')),
    'data_php_errors' => array(
        array(0, 'не выводить'),
        array(30719, 'все'),
        array(7, 'ошибки, предупреждения'),
        array(15, 'ошибки, предупреждения, уведомления'),
        array(30711, 'все кроме уведомлений')
    )
);
?>