<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля настроеек системы"
 * Пакет контроллеров "Настройки системы"
 * Группа пакетов     "Система"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YConfig_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные профиля настроеек системы
 * 
 * @category   Gear
 * @package    GController_YConfig_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YConfig_Profile_Data extends GController_Profile_Data
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_yconfig_profile';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return $this->_['profile_title_update'];
    }

    /**
     * Обновление настроеек пользователя
     * 
     * @return void
     */
    protected function settingsUpdate()
    {
        $data = $settings = $this->input->get('user', false);
        // если нет данных
        if (empty($data)) return;

        $table = new GDb_Table('gear_users', 'user_id');
        $data = json_encode($data);
        if ($table->update(array('user_settings' => $data), $this->session->get('user_id')) === false)
            throw new GException('Data processing error', 'Query error (Internal Server Error)');
        // обновить сессию
        $this->session->set('user/settings', $settings);
    }

    /**
     * Обновление настроеек списка
     * 
     * @return void
     */
    protected function gridUpdate()
    {
        $gridReset = $this->input->get('grid_reset', false);

        if ($gridReset) {
            $query = new GDb_Query();
            $sql = 'DELETE FROM `gear_controller_store` WHERE `user_id`=' . $this->session->get('user_id');
            if ($query->execute($sql) === false)
                throw new GException('Data processing error', 'Query error (Internal Server Error)');
        }
    }

    /**
     * Обновление настроеек модулей
     * 
     * @return void
     */
    protected function modulesUpdate()
    {
        $modules = $this->input->get('modules', false);
        if (!$modules) return false;

        $table = new GDb_Table('gear_modules', 'module_id');
        foreach ($modules as $name => $data) {
            $module = $this->session->get('modules/' . $name, false);
            if ($module !== false) {
                $module['settings'] = $data;
                $this->session->set('modules/' . $name, $module);
                if ($table->update(array('module_settings' => json_encode($data)), $module['id']) === false)
                    throw new GSqlException();
            }
        }
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        parent::dataAccessUpdate();

        if ($this->input->isEmpty('PUT'))
            throw new GException('Warning', 'To execute a query, you must change data!');
        // обновление настроеек пользователя
        $this->settingsUpdate();
        // обновление настроеек списка
        $this->gridUpdate();
        // проверки входных данных
        $this->isDataCorrect($params);
        // проверка существования записи с одинаковыми значениями
        $this->isDataExist($params);
        // использование системных полей в запросе SQL
        if ($this->isSysFields) {
            $params['sys_date_update'] = date('Y-m-d');
            $params['sys_time_update'] = date('H:i:s');
            $params['sys_user_update'] = $this->session->get('user_id');
        }
        // обновление настроеек модулей
        $this->modulesUpdate();
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($params, 'method "PUT"');
        }
        //$this->response->add('refresh', 1);
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {}
}
?>