<?php
/**
 * Gear Manager Manager
 *
 * Контроллер         "Интерфейс профиля настройки системы"
 * Пакет контроллеров "Настройки системы"
 * Группа пакетов     "Система"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YConfig_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля настройки системы
 * 
 * @category   Gear
 * @package    GController_YConfig_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YConfig_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Список подключаемых плагинов (из module_name_settings)
     * 
     * @var array
     */
    protected $_plugins = array();

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор,
     * используется для инициализации атрибутов контроллера без конструктора
     * 
     * @return void
     */
    public function construct()
    {
        $modules = $this->session->get('modules');
        // список модулей
        for ($i = 0; $i < sizeof($modules); $i++) {
            // язык плагина
            $path = $this->languagePath . GFactory::getLanguage('alias') . '/' . $modules[$i] . '.php';
            if (file_exists($path)) {
                $this->_[$modules[$i]] = require($path);
            }
            // плагин
            $path = $this->path . '/plugins/' . $modules[$i] . '.php';
            if (file_exists($path))
                $this->_plugins[$modules[$i]] = $path;
        }
    }

    /**
     * Возвращает дополнительные данные для формирования интерфейса
     * 
     * @return mixed
     */
    protected function getUserSettings()
    {
        $data = $this->config->get('USER/SETTINGS');

        // соединение с базой данных
        GFactory::getDb()->connect();
        $query = new GDb_Query();
        $sql = 'SELECT `user_settings` FROM `gear_users` WHERE `user_id`=' . $this->session->get('user_id');
        $record = $query->getRecord($sql);
        if ($record === false)
            throw new GSqlException();
        if (empty($record['user_settings']))
            return $data;
        else {
            $settings = json_decode($record['user_settings'], true);
            return array_merge($data, $settings);
        }
    }

    /**
     * Возвращает дополнительные данные для формирования интерфейса
     * 
     * @return mixed
     */
    protected function getAccessMods()
    {
        $query = new GDb_Query();
        $sql = 'SELECT * FROM `gear_user_groups` WHERE `group_id`=' . $this->session->get('group/id');
        $record = $query->getRecord($sql);
        if ($record === false)
            throw new GSqlException();
        if (empty($record['group_access']))
            return array();
        else {
            $access = json_decode($record['group_access'], true);
            return $access['mods'];
        }
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительные данные для формирования интерфейса
        $userSettings = $this->getUserSettings();
        $accessMods = $this->getAccessMods();

        // Ext_Window (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'           => $this->_['title_profile_update'],
                  'titleEllipsis'   => 40,
                  'width'           => 800,
                  'height'          => 500,
                  'resizable'       => false,
                  'btnDeleteHidden' => true,
                  'stateful'        => false,
                  'buttonsAdd'      => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-system-interface-config',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );
        $this->_cmp->state = 'update';

        // вкладка "Персонализация"
        $tabInterface = array(
            'title'       => $this->_['title_tab_interface'],
            'disabled'    => !$accessMods['system'],
            'layout'      => 'form',
            'labelWidth'  => 60,
            'baseCls'     => 'mn-form-tab-body',
            'bodyStyle'   => 'background: url("' . $this->resourcePath. 'tab-bg-interface.png") no-repeat center center;',
            'autoScroll'  => true,
            'items'       => array(
                array('xtype' => 'mn-field-separator', 'html' => $this->_['label_desktop_view']),
                array('xtype'  => 'mn-field-dataview',
                      'anchor' => '100%',
                      'value'  => (int) $userSettings['desktop'],
                      'name'   => 'user[desktop]',
                      'viewCfg' => array(
                          'store'  => array(
                              'xtype'  => 'arraystore',
                              'id'     => 0,
                              'fields' => array('id', 'title', 'icon'),
                              'data'   => array(
                                  array(1, $this->_['label_view'] . ' 1', $this->resourcePath . 'icon-view-1.png'),
                                  array(2, $this->_['label_view'] . ' 2', $this->resourcePath . 'icon-view-2.png'),
                                  array(3, $this->_['label_view'] . ' 3', $this->resourcePath . 'icon-view-3.png'),
                                  array(4, $this->_['label_view'] . ' 4', $this->resourcePath . 'icon-view-4.png'),
                               )
                          )
                      ),
                     'thumb' => array('width' => 128, 'height' => 100)
                ),
                array('xtype' => 'mn-field-separator', 'html' => $this->_['label_theme']),
                array('xtype'  => 'mn-field-dataview',
                      'anchor' => '100%',
                      'value'  => $userSettings['theme'],
                      'name'   => 'user[theme]',
                      'viewCfg' => array(
                          'store'  => array(
                              'xtype'  => 'arraystore',
                              'id'     => 0,
                              'fields' => array('id', 'title', 'icon'),
                              'data'   => array(
                                  array('gray', 'Gray', $this->resourcePath . 'icon-theme-gray.png'),
                                  array('office2007', 'Office 2007', $this->resourcePath . 'icon-theme-office2007.png'),
                                  array('green', 'Green', $this->resourcePath . 'icon-theme-green.png'),
                               )
                          )
                      ),
                      'thumb' => array('width' => 128, 'height' => 100)
                ),
                array('xtype' => 'mn-field-separator', 'html' => $this->_['title_shortcuts']),
                array('xtype'         => 'combo',
                      'fieldLabel'    => $this->_['label_type_shortcuts'],
                      'checkDirty'    => false,
                      'name'          => 'user[shortcuts]',
                      'value'         => $userSettings['shortcuts'],
                      'width'         => 130,
                      'editable'      => false,
                      'typeAhead'     => false,
                      'triggerAction' => 'all',
                      'mode'          => 'local',
                      'store'         => array(
                          'xtype'  => 'arraystore',
                          'fields' => array('value', 'text'),
                          'data'   => $this->_['data_shortcuts']
                      ),
                     'hiddenName'   => 'user[shortcuts]',
                     'valueField'   => 'value',
                     'displayField' => 'text',
                     'allowBlank'    => true),
                array('xtype'      => 'mn-field-chbox',
                      'fieldLabel' => $this->_['label_sound'],
                      'default'    => $userSettings['sound'],
                      'checkDirty' => false,
                      'name'       => 'user[sound]'),
            )
        );

        // вкладка "Управление"
        $tabControl = array(
            'title'       => $this->_['title_tab_control'],
            'disabled'    => !$accessMods['system'],
            'layout'      => 'form',
            'labelWidth'  => 300,
            'baseCls'     => 'mn-form-tab-body',
            'autoScroll'  => true,
            'items'       => array(
                array('xtype' => 'mn-field-separator', 'html' => $this->_['label_separator_grid']),
                array('xtype'      => 'mn-field-chbox',
                      'fieldLabel' => $this->_['label_grid_columns'],
                      'default'    => 0,
                      'checkDirty' => false,
                      'name'       => 'grid_reset'),
                array('xtype'      => 'mn-field-chbox',
                      'fieldLabel' => $this->_['label_grid_bg_use'],
                      'default'    => $userSettings['grid/background/use'],
                      'checkDirty' => false,
                      'name'       => 'user[grid/background/use]'),
                array('xtype'      => 'displayfield',
                      'hideLabel'  =>true,
                      'value'      => $this->_['label_grid_bg_color']),
                array('xtype' => 'mn-field-colorpalette',
                      'setTo' => 'grid_bg_color'),
                array('xtype'      => 'textfield',
                      'width'      => 60,
                      'id'         => 'grid_bg_color',
                      'hideLabel'  =>true,
                      'name'       => 'user[grid/background/color]',
                      'value'      => $userSettings['grid/background/color']),
                array('xtype' => 'displayfield', 'hideLabel'  =>true, 'value' => '<br>'),
                array('xtype' => 'mn-field-separator', 'html' => $this->_['label_separator_msg']),
                array('xtype'      => 'mn-field-chbox',
                      'fieldLabel' => $this->_['label_receive_mails'],
                      'default'    => $userSettings['receive/mails'],
                      'checkDirty' => false,
                      'name'       => 'user[receive/mails]'),
                array('xtype'      => 'mn-field-chbox',
                      'fieldLabel' => $this->_['label_receive_messages'],
                      'default'    => $userSettings['receive/messages'],
                      'checkDirty' => false,
                      'name'       => 'user[receive/messages]'),
                array('xtype'      => 'mn-field-chbox',
                      'fieldLabel' => $this->_['label_receive_js_errors'],
                      'default'    => $userSettings['receive/js/errors'],
                      'checkDirty' => false,
                      'name'       => 'user[receive/js/errors]'),
                array('xtype' => 'mn-field-separator', 'html' => $this->_['label_separator_debug']),
                array('xtype'      => 'mn-field-chbox',
                      'fieldLabel' => $this->_['label_debug_js_core'],
                      'default'    => $userSettings['debug/js/core'],
                      'checkDirty' => false,
                      'name'       => 'user[debug/js/core]'),
                array('xtype'      => 'mn-field-chbox',
                      'fieldLabel' => $this->_['label_debug_js'],
                      'default'    => $userSettings['debug/js'],
                      'checkDirty' => false,
                      'name'       => 'user[debug/js]'),
                array('xtype'      => 'mn-field-chbox',
                      'fieldLabel' => $this->_['label_debug_php'],
                      'default'    => $userSettings['debug/php'],
                      'checkDirty' => false,
                      'name'       => 'user[debug/php]'),
                array('xtype'         => 'combo',
                      'fieldLabel'    => $this->_['label_debug_php_errors'],
                      'checkDirty'    => false,
                      'name'          => 'user[debug/php/errors]',
                      'value'         => $userSettings['debug/php/errors'],
                      'width'         => 250,
                      'editable'      => false,
                      'typeAhead'     => false,
                      'triggerAction' => 'all',
                      'mode'          => 'local',
                      'store'         => array(
                          'xtype'  => 'arraystore',
                          'fields' => array('value', 'text'),
                          'data'   => $this->_['data_php_errors']
                      ),
                     'hiddenName'   => 'user[debug/php/errors]',
                     'valueField'   => 'value',
                     'displayField' => 'text',
                     'allowBlank'    => true),
                array('xtype' => 'mn-field-separator'),
                array('xtype' => 'label', 'html' => $this->_['label_debug_info'])
            )
        );

        // вкладка "Дата и время"
        $tabDate = array(
            'title'       => $this->_['title_tab_date'],
            'disabled'    => !$accessMods['system'],
            'layout'      => 'form',
            'labelWidth'  => 130,
            'baseCls'     => 'mn-form-tab-body',
            'autoScroll'  => true,
            'items'       => array(
                array('xtype' => 'mn-field-separator', 'html' => $this->_['label_separator_date']),
                array('xtype' => 'fieldset',
                      'title' => $this->_['fieldset_title_day'],
                      'labelWidth' => 100,
                      'items' => array(
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_char_d'],
                                'value'      => $this->_['value_char_d']),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_char_D'],
                                'value'      => $this->_['value_char_D']),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_char_j'],
                                'value'      => $this->_['value_char_j']),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_char_L'],
                                'value'      => $this->_['value_char_L']),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_char_N'],
                                'value'      => $this->_['value_char_N']),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_char_S'],
                                'value'      => $this->_['value_char_S']),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_char_W'],
                                'value'      => $this->_['value_char_W']),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_char_z'],
                                'value'      => $this->_['value_char_z'])
                    )
                ),
                array('xtype' => 'fieldset',
                      'title' => $this->_['fieldset_title_week'],
                      'labelWidth' => 100,
                      'items' => array(
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_char_W'],
                                'value'      => $this->_['value_char_W']),
                      )
                ),
                array('xtype' => 'fieldset',
                      'title' => $this->_['fieldset_title_month'],
                      'labelWidth' => 100,
                      'items' => array(
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_char_F'],
                                'value'      => $this->_['value_char_F']),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_char_m'],
                                'value'      => $this->_['value_char_m']),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_char_M'],
                                'value'      => $this->_['value_char_M']),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_char_n'],
                                'value'      => $this->_['value_char_n']),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_char_t'],
                                'value'      => $this->_['value_char_t']),
                      )
                ),
                array('xtype' => 'fieldset',
                      'title' => $this->_['fieldset_title_year'],
                      'labelWidth' => 100,
                      'items' => array(
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_char_L'],
                                'value'      => $this->_['value_char_L']),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_char_O'],
                                'value'      => $this->_['value_char_O']),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_char_Y'],
                                'value'      => $this->_['value_char_Y']),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_char_y'],
                                'value'      => $this->_['value_char_y'])
                      )
                ),
                array('xtype'      => 'textfield',
                      'fieldLabel' => $this->_['label_format_date'],
                      'value'      => $userSettings['format/date'],
                      'width'      => 90,
                      'checkDirty' => false,
                      'name'       => 'user[format/date]'),
                array('xtype' => 'mn-field-separator', 'html' => $this->_['label_separator_time']),
                array('xtype' => 'fieldset',
                      'title' => $this->_['fieldset_title_time'],
                      'labelWidth' => 100,
                      'items' => array(
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_char_a'],
                                'value'      => $this->_['value_char_a']),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_char_A'],
                                'value'      => $this->_['value_char_A']),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_char_B'],
                                'value'      => $this->_['value_char_B']),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_char_g'],
                                'value'      => $this->_['value_char_g']),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_char_G'],
                                'value'      => $this->_['value_char_G']),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_char_h'],
                                'value'      => $this->_['value_char_h']),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_char_H'],
                                'value'      => $this->_['value_char_H']),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_char_i'],
                                'value'      => $this->_['value_char_i']),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_char_s'],
                                'value'      => $this->_['value_char_s']),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_char_u'],
                                'value'      => $this->_['value_char_u']),
                      )
                ),
                array('xtype'      => 'textfield',
                      'fieldLabel' => $this->_['label_format_time'],
                      'value'      => $userSettings['format/time'],
                      'width'      => 90,
                      'checkDirty' => false,
                      'name'       => 'user[format/time]'),
                array('xtype'      => 'textfield',
                      'fieldLabel' => $this->_['label_format_datetime'],
                      'value'      => $userSettings['format/datetime'],
                      'width'      => 90,
                      'checkDirty' => false,
                      'name'       => 'user[format/datetime]')
            )
        );

        $items = array($tabInterface, $tabControl, $tabDate);
        // подключение плагинов
        if ($this->_plugins) {
            foreach ($this->_plugins as $alias => $path) {
                $tab = require($path);
                if (isset($accessMods[$alias])) {
                    if ($accessMods[$alias])
                        array_push($items, $tab);
                        //$tab['disabled'] = !$accessMods[$alias];
                }
            }
        }

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->url = $this->componentUrl . 'profile/';
        $form->items->addItems(
            array(
                array('xtype'             => 'tabpanel',
                      'layoutOnTabChange' => true,
                      'deferredRender'    => false,
                      'activeTab'         => 0,
                      'style'             => 'padding:3px',
                      'enableTabScroll'   => true,
                      'anchor'            => '100%',
                      'height'            => 402,
                      'items'             => $items),
                array('xtype'      => 'displayfield',
                      'fieldClass' => 'mn-field-value-warning',
                      'style'      => 'margin-top:6px;',
                      'hideLabel'  => true,
                      'value'      => $this->_['label_footer'])
            )
        );

        parent::getInterface();
    }
}
?>