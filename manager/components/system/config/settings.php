<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Настройки системы"
 * Группа пакетов     "Настройки"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YConfig
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Настройки системы
 * Описание: Настройки системы
 * ID класса: gcontroller_yguide_grid
 * Группа: Система / Настройки
 * Очищать: нет
 * Ресурс
 *    Пакет: system/config/
 *    Контроллер: system/config/Profile/
 *    Интерфейс: profile/interface/1
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске: нет
 */

return array(
    // {Y}- модуль "System" {Config} - пакет контроллеров "Config" -> YConfig
    'clsPrefix' => 'YConfig'
);
?>