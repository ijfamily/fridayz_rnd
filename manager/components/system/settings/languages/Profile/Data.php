<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля языка"
 * Пакет контроллеров "Язык системы"
 * Группа пакетов     "Настройки"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YLanguages_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Данные профиля языка
 * 
 * @category   Gear
 * @package    GController_YLanguages_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YLanguages_Profile_Data extends GController_Profile_Data
{
    /**
     * Use system fields in SQL request
     * If table holding system fields
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Массив полей таблицы ($_tableName)
     *
     * @var array
     */
    public $fields = array('language_name', 'language_alias', 'language_default');

    /**
     * Первичный ключ таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'language_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_languages';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ylanguages_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['profile_title_update'], $record['language_name']);
    }

    /**
     * Изменение данных
     * 
     * @param  array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        parent::dataUpdate($params);

        if ($this->input->get('language_default') != 1) return;
        $query = new GDb_Query();
        $sql = 'UPDATE `gear_languages` SET `language_default`=0 WHERE `language_id`<>' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Вставка данных
     * 
     * @param  array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataInsert($params = array())
    {
        parent::dataInsert($params);

        if ($this->input->get('language_default') != 1) return;
        $query = new GDb_Query();
        $sql = 'UPDATE `gear_languages` SET `language_default`=0 WHERE `language_id`<>' . $this->_recordId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }
}
?>