<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля языка"
 * Пакет контроллеров "Язык системы"
 * Группа пакетов     "Настройки"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YLanguages_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля языка
 * 
 * @category   Gear
 * @package    GController_YLanguages_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YLanguages_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['profile_title_insert'],
                  'titleEllipsis' => 40,
                  'gridId'        => 'gcontroller_ylanguages_grid',
                  'width'         => 350,
                  'height'        => 152,
                  'resizable'     => false,
                  'stateful'      => false)
        );

        // список полей (ExtJS class "Ext.Panel")
        $items = array(
           array('xtype'      => 'textfield',
                 'fieldLabel' => $this->_['label_lang_name'],
                 'name'       => 'language_name',
                 'maxLength'  => 50,
                 'anchor'     => '100%',
                 'allowBlank' => false),
           array('xtype'      => 'textfield',
                 'fieldLabel' => $this->_['label_lang_alias'],
                 'name'       => 'language_alias',
                 'maxLength'  => 10,
                 'width'      => 150,
                 'allowBlank' => false),
           array('xtype'         => 'combo',
                 'fieldLabel'    => $this->_['label_lang_default'],
                 'name'          => 'language_default',
                 'editable'      => false,
                 'width'         => 150,
                 'typeAhead'     => true,
                 'triggerAction' => 'all',
                 'mode'          => 'local',
                 'value'         => 1,
                 'store'         => array('xtype'  => 'arraystore',
                                          'fields' => array('value', 'display'),
                                          'data'   => array(array(1, $this->_['data_lang_default'][0]),
                                                            array(0, $this->_['data_lang_default'][1]))),
                 'hiddenName'    => 'language_default',
                 'valueField'    => 'value',
                 'displayField'  => 'display',
                 'allowBlank'    => false));

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->addItems($items);
        $form->url = $this->componentUrl . 'profile/';

        parent::getInterface();
    }
}
?>