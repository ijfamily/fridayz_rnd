<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'profile_title_insert' => 'Create record "Language"',
    'profile_title_update' => 'Update record "%s"',
    // поля
    'label_lang_name'    => 'Name',
    'label_lang_alias'   => 'Alias',
    'label_lang_default' => 'Default',
    // типы
    'data_lang_default' => array('Yes', 'No')
);
?>