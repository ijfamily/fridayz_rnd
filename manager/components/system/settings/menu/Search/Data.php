<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск меню оболочки"
 * Пакет контроллеров "Меню оболочки"
 * Группа пакетов     "Настройки"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YMenu_Search
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск меню оболочки
 * 
 * @category   Gear
 * @package    GController_YMenu_Search
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2014-08-01 12:00:00 Gear Magic 
 */
final class GController_YMenu_Search_Data extends GController_Search_Data
{
    /**
     * Идентификатор компонента
     *
     * @var string
     */
    public $gridId = 'gcontroller_ymenu_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ymenu_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор
     * Используется для инициализации атрибутов контроллер не переданного в конструктор
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'menu_id', 'label' => $this->_['header_menu_id']),
            array('dataIndex' => 'menu_item_index', 'label' => $this->_['header_menu_item_index']),
            array('dataIndex' => 'menu_item_text', 'field' => 'm.menu_item_text', 'label' => $this->_['header_menu_item_text']),
            array('dataIndex' => 'pmenu_item_text', 'field' => 'mp.menu_item_text', 'label' => $this->_['header_pmenu_item_text']),
            array('dataIndex' => 'menu_count', 'label' => $this->_['header_menu_count']),
            array('dataIndex' => 'menu_item_id', 'label' => $this->_['header_menu_item_id']),
            array('dataIndex' => 'menu_menu_id', 'label' => $this->_['header_menu_menu_id']),
            array('dataIndex' => 'menu_xtype', 'label' => $this->_['header_menu_xtype']),
            array('dataIndex' => 'menu_item_iconcls', 'label' => $this->_['header_menu_item_iconcls']),
            array('dataIndex' => 'menu_item_disabled', 'label' => $this->_['header_menu_item_disabled']),
            array('dataIndex' => 'menu_item_hidden', 'label' => $this->_['header_menu_item_hidden'])
        );
    }
}
?>