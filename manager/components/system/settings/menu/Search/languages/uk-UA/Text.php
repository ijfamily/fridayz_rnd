<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'search_title' => 'Пошук у списку "Меню оболонки"',
    // поля
    'header_menu_id'            => 'ID',
    'header_menu_item_index'    => '№',
    'header_menu_item_text'     => 'Назва',
    'header_menu_xtype'         => 'Меню xtype',
    'header_menu_item_id'       => 'Пункт ID',
    'header_menu_menu_id'       => 'Меню ID',
    'header_menu_item_iconcls'  => 'Клас значка',
    'header_menu_item_type'     => 'Тип',
    'header_menu_item_popup'    => 'Псевдоменю',
    'header_menu_item_disabled' => 'Заблоковано',
    'header_menu_item_hidden'   => 'Прихований',
    'header_menu_count'         => 'Підпунктів',
    'header_pmenu_item_text'    => 'Назва "предка"'
);
?>