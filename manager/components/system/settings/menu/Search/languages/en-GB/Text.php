<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'search_title' => 'Search in list "Shell menu"',
    // поля
    'header_menu_id'            => 'ID',
    'header_menu_item_index'    => '№',
    'header_menu_item_text'     => 'Name',
    'header_menu_xtype'         => 'Menu xtype',
    'header_menu_item_id'       => 'Item ID',
    'header_menu_menu_id'       => 'Menu ID',
    'header_menu_item_iconcls'  => 'Icon class',
    'header_menu_item_type'     => 'Type',
    'header_menu_item_popup'    => 'Fantom',
    'header_menu_item_disabled' => 'Disabled',
    'header_menu_item_hidden'   => 'Hidden',
    'header_menu_count'         => 'Subitems',
    'header_pmenu_item_text'    => 'Name "parent"'
);
?>