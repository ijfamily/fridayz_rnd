<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'grid_title'   => 'Меню оболонки',
    'tooltip_grid' => 'разделы и пункты меню оболочки системы',
    'rowmenu_edit' => 'Редагувати',
    // панель управления
    'title_buttongroup_edit'    => 'Правка',
    'title_buttongroup_cols'    => 'Стовпці',
    'title_buttongroup_filter'  => 'Фільтр',
    // столбцы
    'header_menu_id'            => 'ID',
    'header_menu_item_index'    => '№',
    'tooltip_menu_item_index'   => 'Порядковий номер',
    'tooltip_menu_id'           => 'Ідентфікатор меню',
    'header_menu_item_text'     => 'Назва',
    'tooltip_menu_item_text'    => 'Назва пункта меню',
    'header_menu_xtype'         => 'Меню xtype',
    'tooltip_menu_xtype'        => 'xtype подменю пункта',
    'header_menu_item_id'       => 'Пункт ID',
    'tooltip_menu_item_id'      => 'ID DOM элемента пункта',
    'header_menu_menu_id'       => 'Меню ID',
    'tooltip_menu_menu_id'      => 'ID DOM подменю пункта',
    'header_menu_item_iconcls'  => 'Клас значка',
    'tooltip_menu_item_iconcls' => 'CSS клас значка пункта меню',
    'header_menu_item_type'     => 'Тип',
    'tooltip_menu_item_type'    => 'Тип пункта меню',
    'header_menu_item_popup'    => 'Псевдоменю',
    'tooltip_menu_item_popup'   => 'Пункт який має підменю без підпунктів',
    'header_menu_item_disabled' => 'Заблоковано',
    'header_menu_item_hidden'   => 'Прихований',
    'header_menu_count'         => 'Підпунктів',
    'tooltip_menu_count'        => 'Пунктів в підменю',
    'header_pmenu_item_text'    => 'Назва "предка"',
    // типы
    'data_boolean' => array('Ні', 'Так'),
    'data_string'  => array('Текст', 'Селектор')
);
?>