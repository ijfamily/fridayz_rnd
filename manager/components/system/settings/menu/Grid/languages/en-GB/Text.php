<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'grid_title'   => 'Shell menu',
    'tooltip_grid' => 'разделы и пункты меню оболочки системы',
    'rowmenu_edit' => 'Edit',
    // панель управления
    'title_buttongroup_edit'    => 'Edit',
    'title_buttongroup_cols'    => 'Columns',
    'title_buttongroup_filter'  => 'Filter',
    // столбцы
    'header_menu_id'            => 'ID',
    'header_menu_item_index'    => '№',
    'tooltip_menu_item_index'   => 'Item index',
    'tooltip_menu_id'           => 'Menu ID',
    'header_menu_item_text'     => 'Name',
    'tooltip_menu_item_text'    => 'Subitem name',
    'header_menu_xtype'         => 'Menu xtype',
    'tooltip_menu_xtype'        => 'xtype subitem',
    'header_menu_item_id'       => 'Item ID',
    'tooltip_menu_item_id'      => 'ID DOM of item',
    'header_menu_menu_id'       => 'Menu ID',
    'tooltip_menu_menu_id'      => 'ID DOM submenu of item',
    'header_menu_item_iconcls'  => 'Icon class',
    'tooltip_menu_item_iconcls' => 'CSS icon class of item menu',
    'header_menu_item_type'     => 'Type',
    'tooltip_menu_item_type'    => 'Type of menu item',
    'header_menu_item_popup'    => 'Fantom',
    'tooltip_menu_item_popup'   => 'tem that has a sub-menu without',
    'header_menu_item_disabled' => 'Disabled',
    'header_menu_item_hidden'   => 'Hidden',
    'header_menu_count'         => 'Subitems',
    'tooltip_menu_count'        => 'Items in the submenu',
    'header_pmenu_item_text'    => 'Name "parent"',
    // типы
    'data_boolean' => array('No', 'Yes'),
    'data_string'  => array('Text', 'Selector')
);
?>