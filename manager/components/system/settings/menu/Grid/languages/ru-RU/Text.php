<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $$
 */

return array(
    // список
    'grid_title'   => 'Меню оболочки',
    'tooltip_grid' => 'разделы и пункты меню оболочки системы',
    'rowmenu_edit' => 'Редактировать',
    // панель управления
    'title_buttongroup_edit'    => 'Правка',
    'title_buttongroup_cols'    => 'Столбцы',
    'title_buttongroup_filter'  => 'Фильтр',
    // столбцы
    'header_menu_id'            => 'ID',
    'header_menu_item_index'    => '№',
    'tooltip_menu_item_index'   => 'Порядковый номер',
    'tooltip_menu_id'           => 'Идентфикатор меню',
    'header_menu_item_text'     => 'Название',
    'tooltip_menu_item_text'    => 'Название пункта меню',
    'header_menu_xtype'         => 'Меню xtype',
    'tooltip_menu_xtype'        => 'xtype подменю пункта',
    'header_menu_item_id'       => 'Пункт ID',
    'tooltip_menu_item_id'      => 'ID DOM элемента пункта',
    'header_menu_menu_id'       => 'Меню ID',
    'tooltip_menu_menu_id'      => 'ID DOM подменю пункта',
    'header_menu_item_iconcls'  => 'Класс значка',
    'tooltip_menu_item_iconcls' => 'CSS класс значка пункта меню',
    'header_menu_item_type'     => 'Тип',
    'tooltip_menu_item_type'    => 'Тип пункта меню',
    'header_menu_item_popup'    => 'Псевдоменю',
    'tooltip_menu_item_popup'   => 'Пункт который имеет подменю без подпунктов',
    'header_menu_item_disabled' => 'Заблокирован',
    'header_menu_item_hidden'   => 'Скрытый',
    'header_menu_count'         => 'Подпунктов',
    'tooltip_menu_count'        => 'Пунктов в подменю',
    'header_pmenu_item_text'    => 'Название "предка"',
    // типы
    'data_boolean' => array('Нет', 'Да'),
    'data_string'  => array('Текст', 'Селектор')
);
?>