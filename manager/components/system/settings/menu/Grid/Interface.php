<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка меню оболочки"
 * Пакет контроллеров "Меню оболочки"
 * Группа пакетов     "Настройки"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YMenu_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс списка меню оболочки
 * 
 * @category   Gear
 * @package    GController_YMenu_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YMenu_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'menu_id';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'menu_item_text';

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('name' => 'menu_item_text', 'type' => 'string'),
            array('name' => 'menu_xtype', 'type' => 'string'),
            array('name' => 'menu_item_index', 'type' => 'string'),
            array('name' => 'menu_item_type', 'type' => 'string'),
            array('name' => 'menu_item_popup', 'type' => 'string'),
            array('name' => 'menu_item_disabled', 'type' => 'string'),
            array('name' => 'menu_item_hidden', 'type' => 'string'),
            array('name' => 'menu_item_iconcls', 'type' => 'string'),
            array('name' => 'pmenu_item_text', 'type' => 'string'),
            array('name' => 'menu_item_id', 'type' => 'string'),
            array('name' => 'menu_count', 'type' => 'integer'),
            array('name' => 'menu_menu_id', 'type' => 'integer'),
            array('name' => 'menu_id', 'type' => 'integer')
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $this->columns = array(
            array('xtype'     => 'numbercolumn'),
            array('xtype'     => 'rowmenu'),
            array('dataIndex' => 'menu_id',
                  'header'    => $this->_['header_menu_id'],
                  'tooltip'   => $this->_['tooltip_menu_id'],
                  'width'     => 40,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'menu_item_index',
                  'header'    => $this->_['header_menu_item_index'],
                  'tooltip'   => $this->_['tooltip_menu_item_index'],
                  'width'     => 40,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'menu_item_text',
                  'header'    => '<em class="mn-grid-hd-details"></em>'
                               . $this->_['header_menu_item_text'],
                  'width'     => 140,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'pmenu_item_text',
                  'header'    => $this->_['header_pmenu_item_text'],
                  'width'     => 140,
                  'sortable'  => true,
                  'filter'    => array('type'     => 'string',
                                       'disabled' => false)),
            array('dataIndex' => 'menu_count',
                  'header'    => $this->_['header_menu_count'],
                  'tooltip'   => $this->_['tooltip_menu_count'],
                  'width'     => 90,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'menu_item_id',
                  'header'    => $this->_['header_menu_item_id'],
                  'tooltip'   => $this->_['tooltip_menu_item_id'],
                  'hidden'    => true,
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'menu_menu_id',
                  'header'    => $this->_['header_menu_menu_id'],
                  'tooltip'   => $this->_['tooltip_menu_menu_id'],
                  'hidden'    => true,
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'menu_xtype',
                  'header'    => $this->_['header_menu_xtype'],
                  'tooltip'   => $this->_['tooltip_menu_xtype'],
                  'hidden'    => true,
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'menu_item_iconcls',
                  'header'    => $this->_['header_menu_item_iconcls'],
                  'tooltip'   => $this->_['tooltip_menu_item_iconcls'],
                  'hidden'    => true,
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'menu_item_type',
                  'header'    => $this->_['header_menu_item_type'],
                  'tooltip'   => $this->_['tooltip_menu_item_type'],
                  'hidden'    => true,
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('xtype'     => 'booleancolumn',
                  'cls'       => 'mn-bg-color-gray2',
                  'dataIndex' => 'menu_item_disabled',
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-disabled.png" align="absmiddle">',
                  'tooltip'   => $this->_['header_menu_item_disabled'],
                  'align'     => 'center',
                  'width'     => 45,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean')),
            array('xtype'     => 'booleancolumn',
                  'cls'       => 'mn-bg-color-gray2',
                  'dataIndex' => 'menu_item_hidden',
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-visible.png" align="absmiddle">',
                  'tooltip'   => $this->_['header_menu_item_hidden'],
                  'align'     => 'center',
                  'width'     => 45,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean'))
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $this->_['grid_title'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_grid'],
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'profileUrl'    => $this->componentUrl . 'profile/')
        );

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "edit" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "добавить" (ExtJS class "Manager.button.InsertData")
        $group->items->add(array('xtype' => 'mn-btn-insert-data', 'gridId' => $this->classId));
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' => $this->classId));
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = $this->classId;
        $this->_cmp->tbar->items->add($group);

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $this->_cmp->cellTips = array(
            array('field' => 'menu_item_text',
                  'tpl'   => '<div class="mn-grid-cell-tooltip-tl">{menu_item_text}</div>'
                           . $this->_['header_menu_id'] . ': <b>{menu_id}</b><br>'
                           . $this->_['header_menu_item_index'] . ': <b>{menu_item_index}</b><br>'
                           . $this->_['header_pmenu_item_text'] . ': <b>{pmenu_item_text}</b><br>'
                           . $this->_['header_menu_count'] . ': <b>{menu_count}</b><br>'
                           . $this->_['header_menu_item_id'] . ': <b>{menu_item_id}</b><br>'
                           . $this->_['header_menu_menu_id'] . ': <b>{menu_menu_id}</b><br>'
                           . $this->_['header_menu_xtype'] . ': <b>{menu_xtype}</b><br>'
                           . $this->_['header_menu_item_iconcls'] . ': <b>{menu_item_iconcls}</b><br>'
                           . $this->_['header_menu_item_type'] . ': '
                           . '<tpl if="menu_item_type == 0"><b>' . $this->_['data_string'][0] . '</b></tpl> '
                           . '<tpl if="menu_item_type == 1"><b>' . $this->_['data_string'][1] . '</b></tpl><br> '
                           . $this->_['header_menu_item_disabled'] . ': '
                           . '<tpl if="menu_item_disabled == 0"><b>' . $this->_['data_boolean'][0] . '</b></tpl> '
                           . '<tpl if="menu_item_disabled == 1"><b>' . $this->_['data_boolean'][1] . '</b></tpl><br/>'
                           . $this->_['header_menu_item_hidden'] . ': '
                           . '<tpl if="menu_item_hidden == 0"><b>' . $this->_['data_boolean'][0] . '</b></tpl> '
                           . '<tpl if="menu_item_hidden == 1"><b>' . $this->_['data_boolean'][1] . '</b></tpl>'),
           array('field' => 'pmenu_item_text', 'tpl' => '{pmenu_item_text}'),
           array('field' => 'menu_menu_id', 'tpl' => '{menu_menu_id}'),
           array('field' => 'menu_xtype', 'tpl' => '{menu_xtype}'),
           array('field' => 'menu_item_iconcls', 'tpl' => '{menu_item_iconcls}')
        );

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $items = array(
            array('text'    => $this->_['rowmenu_edit'],
                  'iconCls' => 'icon-form-edit',
                  'url'     => $this->componentUrl . 'profile/interface/'),
            array('xtype'   => 'menuseparator')
        );
        // список доступных языков
        $itemsL = array();
        $langs = $this->session->get('language/list');
        $count = count($langs);
        for ($i = 0; $i < $count; $i++) {
            if ($langs[$i]['language_enabled'])
                $itemsL[] = array(
                    'text'    => $langs[$i]['language_name'],
                    'iconCls' => 'icon-form-translate',
                    'params'  => 'lang=' . $langs[$i]['language_id'],
                    'url'     => $this->componentUrl . 'translation/interface/'
                );
        }
        array_push($items, $itemsL);
        $this->_cmp->rowMenu = array('xtype' => 'mn-grid-rowmenu', 'items' => $items);

        parent::getInterface();
    }
}
?>