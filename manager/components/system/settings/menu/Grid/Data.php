<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка меню оболочки"
 * Пакет контроллеров "Меню оболочки"
 * Группа пакетов     "Настройки"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YMenu_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные списка меню оболочки
 * 
 * @category   Gear
 * @package    GController_YMenu_Grid
 * @subpackage Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YMenu_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'menu_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_menu';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 'SELECT SQL_CALC_FOUND_ROWS '
                   . '`m`.*, `mc`.`menu_count`, `mp`.`menu_item_text` `pmenu_item_text` '
                   // table `gear_menu`
                   . 'FROM (SELECT `m`.*, `ml`.`menu_item_text` '
                   . 'FROM `gear_menu` `m`, `gear_menu_l` `ml` '
                   . 'WHERE `m`.`menu_id`=`ml`.`menu_id` AND '
                   . '`ml`.`language_id`=' . $this->language->get('id') . ') `m` '
                   // join `gear_menu` `pg`
                   . 'LEFT JOIN (SELECT `m`.*, `ml`.`menu_item_text` '
                   . 'FROM `gear_menu` `m`, `gear_menu_l` `ml` '
                   . 'WHERE `m`.`menu_id`=`ml`.`menu_id` AND '
                   . '`ml`.`language_id`=' . $this->language->get('id') . ') `mp` '
                   . 'ON `mp`.`menu_id`=`m`.`menu_parent_id` '
                   // join `gear_menu` `pcg`
                   . 'LEFT JOIN (SELECT `menu_parent_id` `menu_id`, COUNT(`menu_parent_id`) `menu_count` '
                   . 'FROM `gear_menu` GROUP BY `menu_parent_id`) `mc` ON `mc`.`menu_id`=`m`.`menu_id` '
                   . 'WHERE 1 %filter '
                   . 'ORDER BY %sort '
                   . 'LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            'menu_item_text' => array('type' => 'string'),
            'menu_xtype' => array('type' => 'string'),
            'menu_item_index' => array('type' => 'string'),
            'menu_item_type' => array('type' => 'string'),
            'menu_item_popup' => array('type' => 'string'),
            'menu_item_disabled' => array('type' => 'string'),
            'menu_item_hidden' => array('type' => 'string'),
            'menu_item_iconcls' => array('type' => 'string'),
            'pmenu_item_text' => array('type' => 'string'),
            'menu_item_id' => array('type' => 'string'),
            'menu_count' => array('type' => 'integer'),
            'menu_menu_id' => array('type' => 'integer'),
            'menu_id' => array('type' => 'integer')
        );
    }
}
?>