<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Меню оболочки"
 * Группа пакетов     "Настройки"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YMenu
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Меню оболочки
 * Описание: Меню оболочки
 * ID класса: gcontroller_ymenu_grid
 * Группа: Система / Настройки
 * Очищать: нет
 * Ресурс
 *    Пакет: system/settings/menu/
 *    Контроллер: system/settings/menu/Grid/
 *    Интерфейс: grid/interface/
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске: нет
 */

return array(
    // {Y}- модуль "System" {Menu} - пакет контроллеров "Menu" -> YMenu
    'clsPrefix' => 'YMenu'
);
?>