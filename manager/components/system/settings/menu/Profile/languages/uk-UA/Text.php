<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'profile_title_insert' => 'Створення запису "Меню оболонки"',
    'profile_title_update' => 'Зміна запису "%s"',
    // поля
    'title_tab_attributes'     => 'Атрибути',
    'title_fieldset_item'      => 'Атрибуты пункта меню',
    'label_menu_item_index'    => 'індекс',
    'label_menu_item_text'     => 'Назва',
    'label_pmenu_item_text'    => 'Предок',
    'label_menu_item_id'       => 'ID',
    'label_menu_item_iconcls'  => 'Значок',
    'label_menu_item_type'     => 'Тип',
    'label_menu_item_field'    => 'Поле',
    'label_menu_item_value'    => 'Значення',
    'label_menu_item_disabled' => 'Заблоковано',
    'label_menu_item_hidden'   => 'Прихований',
    'label_menu_item_popup'    => 'Псевдоменю',
    'title_fieldset_menu'      => 'Атрибути меню',
    'label_menu_menu_id'       => 'ID',
    'label_menu_xtype'         => 'xtype',
    'label_menu_item_field'    => 'Поле',
    'label_menu_item_value'    => 'Значення',
    'title_tab_handler'        => 'Оброблювач',
    'title_tab_listeners'      => 'Події',
    // типы
    'data_boolean' => array('Ні', 'Так'),
    'data_string'  => array('Текст', 'Селектор')
);
?>