<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'profile_title_insert' => 'Создание записи "Меню оболочки"',
    'profile_title_update' => 'Изменение записи "%s"',
    // поля
    'title_tab_attributes'     => 'Атрибуты',
    'title_fieldset_item'      => 'Атрибуты пункта меню',
    'label_menu_item_index'    => 'Индекс',
    'label_menu_item_text'     => 'Название',
    'label_pmenu_item_text'    => 'Предок',
    'label_menu_item_id'       => 'ID',
    'label_menu_item_iconcls'  => 'Значок',
    'label_menu_item_type'     => 'Тип',
    'label_menu_item_field'    => 'Поле',
    'label_menu_item_value'    => 'Значение',
    'label_menu_item_disabled' => 'Заблокирован',
    'label_menu_item_hidden'   => 'Скрыт',
    'label_menu_item_popup'    => 'Псевдоменю',
    'title_fieldset_menu'      => 'Атрибуты меню',
    'label_menu_menu_id'       => 'ID',
    'label_menu_xtype'         => 'xtype',
    'label_menu_item_field'    => 'Поле',
    'label_menu_item_value'    => 'Значение',
    'title_tab_handler'        => 'Обработчик',
    'title_tab_listeners'      => 'События',
    // типы
    'data_boolean' => array('Нет', 'Да'),
    'data_string'  => array('Текст', 'Селектор')
);
?>