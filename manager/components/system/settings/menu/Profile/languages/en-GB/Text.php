<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'profile_title_insert' => 'Create record "Shell menu"',
    'profile_title_update' => 'Update record "%s"',
    // поля
    'title_tab_attributes'     => 'Attributes',
    'title_fieldset_item'      => 'Item attributes',
    'label_menu_item_index'    => 'Index',
    'label_menu_item_text'     => 'Name',
    'label_pmenu_item_text'    => 'Parent',
    'label_menu_item_id'       => 'ID',
    'label_menu_item_iconcls'  => 'Icon',
    'label_menu_item_type'     => 'Type',
    'label_menu_item_field'    => 'Field',
    'label_menu_item_value'    => 'Value',
    'label_menu_item_disabled' => 'Disabled',
    'label_menu_item_hidden'   => 'Hidden',
    'label_menu_item_popup'    => 'Popupmenu',
    'title_fieldset_menu'      => 'Menu attributes',
    'label_menu_menu_id'       => 'ID',
    'label_menu_xtype'         => 'xtype',
    'label_menu_item_field'    => 'Field',
    'label_menu_item_value'    => 'Value',
    'title_tab_handler'        => 'Handler',
    'title_tab_listeners'      => 'Listeners',
    // типы
    'data_boolean' => array('No', 'Yes'),
    'data_string'  => array('Text', 'Selector')
);
?>