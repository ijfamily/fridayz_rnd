<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля меню оболочки"
 * Пакет контроллеров "Меню оболочки"
 * Группа пакетов     "Настройки"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YMenu_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные профиля меню оболочки
 * 
 * @category   Gear
 * @package    GController_YMenu_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YMenu_Profile_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($_tableName)
     *
     * @var array
     */
    public $fields = array(
        'menu_parent_id', 'menu_item_index', 'menu_item_id',
        'menu_xtype', 'menu_item_iconcls', 'menu_item_handler', 'menu_item_field',
        'menu_item_listeners', 'menu_item_popup', 'menu_item_disabled', 'menu_item_hidden',
        'menu_item_value', 'menu_item_type'
    );

    /**
     * Первичный ключ таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'menu_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_menu';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ymenu_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['profile_title_update'], $record['menu_item_text']);
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        $query = new GDb_Query();
        // удаление локализации языков ("gear_menu_l")
        $sql = 'DELETE FROM `gear_menu_l` WHERE `menu_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Вставка данных
     * 
     * @param  array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataInsert($params = array())
    {
        parent::dataInsert($params);

        $data = $this->input->getBy(array('menu_item_text'));
        $data['menu_id'] = $this->_recordId;
        $table = new GDb_Table('gear_menu_l', 'menu_lang_id');
        $langs = $this->session->get('language/list');
        $count = count($langs);
        for ($i = 0; $i < $count; $i++) {
            $data['language_id'] = $langs[$i]['language_id'];
            $table->insert($data);
        }
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        $sql = 'SELECT `m`.*, `ml`.`menu_item_text` '
             . 'FROM `gear_menu` AS `m`, `gear_menu_l` AS `ml` '
             . 'WHERE `m`.`menu_id`=`ml`.`menu_id` AND '
             . '`ml`.`language_id`=' . $this->language->get('id') . ' AND '
             . '`m`.`menu_id`=' . $this->uri->id;

        parent::dataView($sql);

        unset($this->response->data['menu_parent_id']);
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        // состояние профиля "правка"
        if ($this->isUpdate) {
            if (isset($params['menu_parent_id']))
                if (!is_numeric($params['menu_parent_id']))
                    unset($params['menu_parent_id']);
        }
    }
}
?>