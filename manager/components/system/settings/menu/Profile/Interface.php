<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля меню оболочки"
 * Пакет контроллеров "Меню оболочки"
 * Группа пакетов     "Настройки"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YMenu_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля меню оболочки
 * 
 * @category   Gear
 * @package    GController_YMenu_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YMenu_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        $data = array('menu_parent_id' => '');
        // если состояние формы "insert"
        if ($this->isInsert) return $data;

        // соединение с базой данных
        GFactory::getDb()->connect();
        $query = new GDb_Query();
        $sql = 'SELECT `mp`.`pmenu_item_text` '
             . 'FROM `gear_menu` AS `m` '
               // join `gear_menu` AS `mp`
             . 'LEFT JOIN (SELECT `m`.*, `ml`.`menu_item_text` AS `pmenu_item_text` '
             . 'FROM `gear_menu` AS `m`, `gear_menu_l` AS `ml` '
             . 'WHERE `m`.`menu_id`=`ml`.`menu_id` AND '
             . '`ml`.`language_id`=' . $this->language->get('id') . ') AS `mp` '
             . 'ON `mp`.`menu_id`=`m`.`menu_parent_id` '
             . 'WHERE `m`.`menu_id`=' . $this->uri->id;
        $record = $query->getRecord($sql);
        if ($record === false)
            throw new GSqlException();
        $data['menu_parent_id'] = $record['pmenu_item_text'];

        return $data;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительные данные для создания интерфейса
        $data = $this->getDataInterface();

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['profile_title_insert'],
                  'titleEllipsis' => 45,
                  'gridId'        => 'gcontroller_ymenu_grid',
                  'width'         => 645,
                  'height'        => 460,
                  'resizable'     => false,
                  'stateful'      => false)
        );

        // список полей (ExtJS class "Ext.Panel")
        // поля "Item"
        $fieldsetItem = array(array('xtype'      => 'spinnerfield',
                                    'fieldLabel' => $this->_['label_menu_item_index'],
                                    'name'       => 'menu_item_index',
                                    'width'      => 60,
                                    'allowBlank' => false,
                                    'emptyText'  => 1),
                              array('xtype'      => 'textfield',
                                    'fieldLabel' => $this->_['label_menu_item_text'],
                                    'name'       => 'menu_item_text',
                                    'maxLength'  => 255,
                                    'width'      => 200,
                                    'allowBlank' => false),
                              array('xtype'      => 'mn-field-combo-tree',
                                    'fieldLabel' => $this->_['label_pmenu_item_text'],
                                    'resetable'  => false,
                                    'width'      => 200,
                                    'name'       => 'menu_parent_id',
                                    'hiddenName' => 'menu_parent_id',
                                    'treeWidth'  => 300,
                                    'treeRoot'   => array('id' => 'root', 'expanded' => true, 'expandable' => true),
                                    'value'      => $data['menu_parent_id'],
                                    'allowBlank' => true,
                                    'store'      => array('xtype' => 'jsonstore',
                                                          'url'   => $this->componentUrl . 'combo/nodes/')),
                              array('xtype'      => 'textfield',
                                    'fieldLabel' => $this->_['label_menu_item_id'],
                                    'name'       => 'menu_item_id',
                                    'maxLength'  => 100,
                                    'width'      => 200,
                                    'allowBlank' => true),
                              array('xtype'      => 'textfield',
                                    'fieldLabel' => $this->_['label_menu_item_iconcls'],
                                    'name'       => 'menu_item_iconcls',
                                    'maxLength'  => 100,
                                    'width'      => 200,
                                    'allowBlank' => true),
                              array('xtype'      => 'textfield',
                                    'fieldLabel' => $this->_['label_menu_item_field'],
                                    'name'       => 'menu_item_field',
                                    'maxLength'  => 100,
                                    'width'      => 200,
                                    'allowBlank' => true),
                              array('xtype'      => 'textfield',
                                    'fieldLabel' => $this->_['label_menu_item_value'],
                                    'name'       => 'menu_item_value',
                                    'maxLength'  => 100,
                                    'width'      => 200,
                                    'allowBlank' => true),
                              array('xtype'         => 'combo',
                                    'fieldLabel'    => $this->_['label_menu_item_type'],
                                    'name'          => 'menu_item_type',
                                    'editable'      => false,
                                    'width'         => 100,
                                    'typeAhead'     => true,
                                    'triggerAction' => 'all',
                                    'mode'          => 'local',
                                    'value'         => 0,
                                    'store'         => array('xtype'  => 'arraystore',
                                                             'fields' => array('value', 'display'),
                                                             'data'   => array(array(0, $this->_['data_string'][0]),
                                                                               array(1, $this->_['data_string'][1]))),
                                    'hiddenName'    => 'menu_item_type',
                                    'valueField'    => 'value',
                                    'displayField'  => 'display',
                                    'allowBlank'    => false),
                              array('xtype'         => 'combo',
                                    'fieldLabel'    => $this->_['label_menu_item_popup'],
                                    'name'          => 'menu_item_popup',
                                    'editable'      => false,
                                    'width'         => 60,
                                    'typeAhead'     => true,
                                    'triggerAction' => 'all',
                                    'mode'          => 'local',
                                    'value'         => 0,
                                    'store'         => array('xtype'  => 'arraystore',
                                                             'fields' => array('value', 'display'),
                                                             'data'   => array(array(0, $this->_['data_boolean'][0]),
                                                                               array(1, $this->_['data_boolean'][1]))),
                                    'hiddenName'    => 'menu_item_popup',
                                    'valueField'    => 'value',
                                    'displayField'  => 'display',
                                    'allowBlank'    => false),
                              array('xtype'         => 'combo',
                                    'fieldLabel'    => $this->_['label_menu_item_disabled'],
                                    'name'          => 'menu_item_disabled',
                                    'editable'      => false,
                                    'width'         => 60,
                                    'typeAhead'     => true,
                                    'triggerAction' => 'all',
                                    'mode'          => 'local',
                                    'value'         => 0,
                                    'store'         => array('xtype'  => 'arraystore',
                                                             'fields' => array('value', 'display'),
                                                             'data'   => array(array(0, $this->_['data_boolean'][0]),
                                                                               array(1, $this->_['data_boolean'][1]))),
                                    'hiddenName'    => 'menu_item_disabled',
                                    'valueField'    => 'value',
                                    'displayField'  => 'display',
                                    'allowBlank'    => false),
                              array('xtype'         => 'combo',
                                    'fieldLabel'    => $this->_['label_menu_item_hidden'],
                                    'name'          => 'menu_item_hidden',
                                    'editable'      => false,
                                    'width'         => 60,
                                    'typeAhead'     => true,
                                    'triggerAction' => 'all',
                                    'mode'          => 'local',
                                    'value'         => 0,
                                    'store'         => array('xtype'  => 'arraystore',
                                                             'fields' => array('value', 'display'),
                                                             'data'   => array(array(0, $this->_['data_boolean'][0]),
                                                                               array(1, $this->_['data_boolean'][1]))),
                                    'hiddenName'    => 'menu_item_hidden',
                                    'valueField'    => 'value',
                                    'displayField'  => 'display',
                                    'allowBlank'    => false));
        // если состояние "update"
        if (!$this->isInsert) {
            array_splice($fieldsetItem, 1, 1);
        }
        // поля "Menu"
        $fieldsetMenu = array(array('xtype'      => 'textfield',
                                    'fieldLabel' => $this->_['label_menu_menu_id'],
                                    'name'       => 'menu_menu_id',
                                    'maxLength'  => 100,
                                    'width'      => 200,
                                    'allowBlank' => true),
                              array('xtype'      => 'textfield',
                                    'fieldLabel' => $this->_['label_menu_xtype'],
                                    'name'       => 'menu_xtype',
                                    'maxLength'  => 100,
                                    'width'      => 200,
                                    'allowBlank' => true));
        // вкладка "attributes"
        $columns = array(array('bodyStyle' => 'margin:3px;background-color:#F3F5F4;border:1px solid #F3F5F4',
                               'items'     => array(array('xtype'      => 'fieldset',
                                                          'labelWidth' => 90,
                                                          'title'      => $this->_['title_fieldset_item'],
                                                          'autoHeight' => true,
                                                          'items'      => $fieldsetItem))),
                         array('bodyStyle' => 'margin:3px;background-color:#F3F5F4;border:1px solid #F3F5F4',
                               'items'     => array(array('xtype'      => 'fieldset',
                                                          'labelWidth' => 40,
                                                          'title'      => $this->_['title_fieldset_menu'],
                                                          'autoHeight' => true,
                                                          'items'      => $fieldsetMenu))));
        $tabAttributes = array('title'     => $this->_['title_tab_attributes'],
                               'layout'    => 'column',
                               'bodyStyle' => 'padding:6px;background-color:#F3F5F4;',
                               'items'     => $columns);
        // вкладка "handler"
        $tabHandler = array('title'       => $this->_['title_tab_handler'],
                            'layout'      => 'form',
                            'labelWidth'  => 85,
                            'bodyStyle'   => 'padding:3px;background-color:#F3F5F4',
                            'items'       => array(array('xtype'     => 'textarea',
                                                         'name'      => 'menu_item_handler',
                                                         'hideLabel' => true,
                                                         'width'     => 607,
                                                         'height'    => 337)));
        // вкладка "listeners"
        $tabListeners = array('title'       => $this->_['title_tab_listeners'],
                              'layout'      => 'form',
                              'labelWidth'  => 85,
                              'bodyStyle'   => 'padding:3px;background-color:#F3F5F4',
                              'items'       => array(array('xtype'     => 'textarea',
                                                           'name'      => 'menu_item_listeners',
                                                           'hideLabel' => true,
                                                           'width'     => 607,
                                                           'height'    => 337)));

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add(
            array('xtype'             => 'tabpanel',
                  'height'            => 378,
                  'layoutOnTabChange' => true,
                  'deferredRender'    => false,
                  'activeTab'         => 0,
                  'bodyStyle'         => 'background-color:transparent',
                  'style'             => 'padding:3px',
                  'enableTabScroll'   => true,
                  'anchor'            => '100%',
                  'defaults'          => array('autoScroll' => true),
                  'items'             => array($tabAttributes, $tabHandler, $tabListeners))
        );
        $form->url = $this->componentUrl . 'profile/';

        parent::getInterface();
    }
}
?>