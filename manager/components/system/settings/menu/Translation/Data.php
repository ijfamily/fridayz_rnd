<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные локализации меню оболочки"
 * Пакет контроллеров "Меню оболочки"
 * Группа пакетов     "Настройки"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YMenu_Translation
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные локализации меню оболочки
 * 
 * @category   Gear
 * @package    GController_YMenu_Translation
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YMenu_Translation_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array('menu_item_text', 'menu_id', 'language_id');

    /**
     * Первичный ключ таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'menu_lang_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_menu_l';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ymenu_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_translation_update'], $record['language_alias'], $record['menu_item_text']);
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        $sql = 'SELECT `n`.*, `p`.*, `l`.`language_alias` '
             . 'FROM `gear_menu` `n`, `gear_menu_l` `p`, `gear_languages` AS `l` '
             . 'WHERE `l`.`language_id`=`p`.`language_id` AND '
             . '`p`.`menu_id`=`n`.`menu_id` AND '
             . '`p`.`menu_lang_id`=' . $this->uri->id;

        parent::dataView($sql);
    }
}
?>