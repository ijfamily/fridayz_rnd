<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс локализации меню оболочки"
 * Пакет контроллеров "Меню оболочки"
 * Группа пакетов     "Настройки"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YMenu_Translation
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс локализации меню оболочки
 * 
 * @category   Gear
 * @package    GController_YMenu_Translation
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YMenu_Translation_Interface extends GController_Translation_Interface
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ymenu_grid';

    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        $data = array('menu_id' => $this->uri->id, 'language_alias' =>'', 'record_id' => '');

        // соединение с базой данных
        GFactory::getDb()->connect();
        $query = new GDb_Query();
        $sql = 'SELECT * FROM `gear_menu_l` WHERE `menu_id`=' . (int)$this->uri->id . ' AND `language_id`=' . $this->_languageId;
        $record = $query->getRecord($sql);
        if ($record === false)
            throw new GSqlException();
        // если есть запись
        if (!empty($record))
            $data['record_id'] = $record['menu_lang_id'];
        // выбранный язык
        $language = $this->language->getLanguagesBy('id', $this->_languageId);
        $data['language_alias'] = $language['alias'];

        return $data;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();
        $this->uri->id = $data['record_id'];

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => sprintf($this->_['title_translation_insert'], $data['language_alias']),
                  'titleEllipsis' => 46,
                  'gridId'        => 'gcontroller_ymenu_grid',
                  'width'         => 360,
                  'height'        => 100,
                  'resizable'     => false,
                  'stateful'      => false));

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_menu_item_text'],
                  'name'       => 'menu_item_text',
                  'maxLength'  => 255,
                  'width'      => 250,
                  'allowBlank' => false)
        );
        // если состояние формы "вставка"
        if (empty($data['record_id'])) {
            $items[] = array(
                'xtype' => 'hidden',
                'value' => $this->_languageId,
                'name'  => 'language_id'
            );
            $items[] = array(
                'xtype' => 'hidden',
                'value' => $data['menu_id'],
                'name'  => 'menu_id'
            );
        }

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->addItems($items);
         $form->urlQuery = 'ddddd=1';
        $form->url = $this->componentUrl . 'translation/';

        parent::getInterface();
    }
}
?>