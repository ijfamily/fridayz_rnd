<?php
/**
 * Gear Manager
 *
 * Контроллер         "Триггер выпадающего дерева"
 * Пакет контроллеров "Меню оболочки"
 * Группа пакетов     "Настройки"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YMenu_Combo
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Nodes.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Combo/Nodes');

/**
 * Триггер выпадающего дерева
 * 
 * @category   Gear
 * @package    GController_YMenu_Combo
 * @subpackage Nodes
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Nodes.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YMenu_Combo_Nodes extends GController_Combo_Nodes
{
    /**
     * Префикс полей для запросов в nested set
     *
     * @var string
     */
    public $fieldPrefix = 'menu';

    /**
     * Первичное поле $tableName
     *
     * @var string
     */
    public $idProperty = 'menu_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_menu';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ymenu_grid';

    /**
     * Вызывается перед предварительной обработкой запроса при формировании записей
     * 
     * @param  array $prefix префикс полей
     * @param  array $id идинд. узла дерева
     * @return string
     */
    protected function queryPreprocessing($prefix, $id)
    {
        if ($id == 'root')
            $query = 'SELECT * '
                   . 'FROM `gear_menu_l` AS `ml`, `gear_menu` AS `g` '
                     // join `gear_menu` AS `g1`
                   . 'LEFT JOIN (SELECT COUNT(`menu_id`) AS `leaf`, `menu_parent_id` '
                   . 'FROM `gear_menu` GROUP BY `menu_parent_id`) AS `g1` '
                   . 'ON `g`.`menu_id`=`g1`.`menu_parent_id` '
                   . 'WHERE `ml`.`menu_id`=`g`.`menu_id` AND '
                   . '`ml`.`language_id`=' . $this->language->get('id') . ' AND '
                   . '`g`.`menu_parent_id` IS NULL '
                   . 'ORDER BY `g`.`menu_item_index`';
        else
            $query = 'SELECT * '
                   . 'FROM `gear_menu_l` AS `ml`, `gear_menu` AS `g` '
                     // join `gear_menu` AS `g1`
                   . 'LEFT JOIN (SELECT COUNT(`menu_id`) AS `leaf`, `menu_parent_id` '
                   . 'FROM `gear_menu` GROUP BY `menu_parent_id`) AS `g1` '
                   . 'ON `g`.`menu_id`=`g1`.`menu_parent_id` '
                   . 'WHERE `ml`.`menu_id`=`g`.`menu_id` AND '
                   . '`ml`.`language_id`=' . $this->language->get('id') . ' AND '
                   . '`g`.`menu_parent_id`=' . $id
                   . ' ORDER BY `g`.`menu_item_index`';

        return $query;
    }

    /**
     * Предварительная обработка узла дерева перед формированием массива записей JSON
     * 
     * @params array $node узел дерева
     * @params array $record запись
     * @return array
     */
    protected function nodesPreprocessing($node, $record)
    {
        $node['text'] = $record['menu_item_text'];

        return $node;
    }
}
?>