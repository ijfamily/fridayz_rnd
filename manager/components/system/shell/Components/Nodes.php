<?php
/**
 * Gear Manager
 *
 * Контроллер         "Дерево компонентов"
 * Пакет контроллеров "Компоненты"
 * Группа пакетов     "Оболочка"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Components
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Nodes.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Tree/Nodes');

/**
 * Дерево компонентов
 * 
 * @category   Gear
 * @package    Components
 * @subpackage Nodes
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Nodes.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YShell_Components_Nodes extends GController_Tree_Nodes
{
    /**
     * Проверка привилегий контроллера
     *
     * @var boolean
     */
    public $checkPrivilege = true;

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function nodesView($sql = '')
    {
        // соединение с базой данных
        GFactory::getDb()->connect();
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($this->uri->id, 'id');
        }
        $this->response->set('action', 'select');
        // идентификатор группы компонентов
        $query = new GDb_Query();
        $sql = 'SELECT * FROM `gear_controller_groups` WHERE group_id=' . $this->uri->getVar('node');
        $group = $query->getRecord($sql);
        // группы компонентов
        $sql = 'SELECT cg.*, `cgl`.*, `c`.`group_count` '
             . 'FROM (SELECT `g`.`group_left`, `g`.`group_right` '
             . 'FROM `gear_controller_access` `a`, gear_controllers `c`, `gear_controller_groups` `g` '
             . 'WHERE `a`.`controller_id`=`c`.`controller_id` AND `c`.`group_id`=`g`.`group_id` AND `a`.`group_id`=' . $this->session->get('group/id') . ' '
             . 'GROUP BY `g`.`group_id`) `ca`, `gear_controller_groups_l` `cgl`, `gear_controller_groups` `cg` '
               // компоненты в группе
             . 'LEFT JOIN (SELECT `group_id`, COUNT(`controller_id`) `group_count` FROM `gear_controllers` '
             . 'WHERE `controller_visible`=1 GROUP BY group_id) `c` ON `c`.`group_id`=`cg`.`group_id` '
             . "WHERE `cg`.`group_left`>{$group['group_left']} AND "
             . '`cg`.`group_right`<' . $group['group_right'] . ' AND '
             . '`cg`.`group_level`=' . ($group['group_level'] + 1) . ' AND '
             . '`ca`.`group_left` >= `cg`.`group_left` AND `ca`.`group_right` <= `cg`.`group_right` AND '
             . '`cgl`.`group_id`=`cg`.`group_id` AND `cgl`.`language_id`=' . $this->language->get('id')
             . ' GROUP BY `cg`.`group_id` '
             . 'ORDER BY `cgl`.`group_name`';
        $query->execute($sql);
        $data = array();
        if ($query->getCountRecords() > 0)
            while (!$query->eof()) {
                $record = $query->next();
                if ($record['group_right'] - $record['group_left'] > 1 || $record['group_count'])
                    $data[] = array(
                        'text'     => $record['group_name'],
                        'expanded' => false,
                        'leaf'     => !($record['group_right'] - $record['group_left'] > 1 || $record['group_count']),
                        'id'       => $record['group_id']
                    );
            }
        // компоненты
        $sql = 'SELECT `a`.*, `c`.* '
             . 'FROM (SELECT `c`.*, `cl`.`controller_name`, `cl`.`controller_about` '
             . 'FROM `gear_controllers` `c`, `gear_controllers_l` `cl` '
             . 'WHERE `c`.`controller_id`=`cl`.`controller_id` AND '
             . '`cl`.`language_id`=' . $this->language->get('id') . ') `c`, `gear_controller_access` `a` '
             . 'WHERE `c`.`controller_id`=`a`.`controller_id` AND '
             . '`c`.`group_id`=' . $group['group_id'] . ' AND '
             . '`a`.`group_id`=' . $this->session->get('group/id') . ' AND '
             . '`c`.`controller_visible`=1 '
             . 'ORDER BY `c`.`controller_name`';
        $query->execute($sql);
        $sysPath = $this->config->get('PATH/APPLICATION') . PATH_COMPONENT;
        while (!$query->eof()) {
            $record = $query->next();
            $node = array(
                'text'       => $record['controller_name'],
                'expanded'   => false,
                'disabled'   => !$record['controller_enabled'],
                'leaf'       => true, 
                'controller' => $record['controller_uri_pkg'] . ROUTER_DELIMITER . $record['controller_uri_action']
            );
            $iconPath = $sysPath . $record['controller_uri'] . 'resources/icon.png';
            if (!file_exists($iconPath))
                $node['iconCls'] = 'icon-cmp';
            else
                $node['icon'] = $iconPath;
            $data[] = $node;
        }
        $this->response->data = $data;
    }
}
?>