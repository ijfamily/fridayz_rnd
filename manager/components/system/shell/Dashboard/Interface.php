<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс доски компонентов"
 * Пакет контроллеров "Оболочка"
 * Группа пакетов     "Система"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YShell_Dashboard
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::ext('Container');
Gear::controller('Interface');
Gear::controller('Dashboard/Interface');

/**
 * Интерфейс доски компонентов
 * 
 * @category   Gear
 * @package    GController_YShell_Dashboard
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YShell_Dashboard_Interface extends GController_Interface
{
    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // вкладка (ExtJS class "Ext.ux.Portal")
        $this->_cmp = new Ext_Container(
            array('id'           => strtolower(__CLASS__),
                  'component'    => array('container' => 'content-tab', 'destroy' => true),
                  'xtype'        => 'portal',
                  'bodyCssClass' => 'main-tab-body',
                  'title'        => $this->_['title_panel'],
                  'tabTip'       => $this->_['title_panel'],
                  'titleTpl'     => $this->_['tooltip_grid'],
                  'closable'     => true)
        );
        $this->_t = $this->_;
    }

    /**
     * Возращает данные доски
     * 
     * @param  array $controller данные контроллера
     * @return array
     */
    protected function getDashboard($cnt)
    {
        $filename = 'components/' . $cnt['controller_uri_pkg']. 'Dashboard/Interface.php';
        if (file_exists($filename)) {
            $class = GFactory::getController($cnt['controller_uri_pkg'] . ROUTER_DELIMITER . 'dashboard/interface/');

            return $class->getInterface();
        } else
            // если ошибка при вызове контроллера
            return array(
                'title' => $this->_t['title_error_dashboard'],
                'cls'   => 'mn-dashboard-item',
                'html'  => sprintf($this->_t['msg_error_dashboard'], $filename)
            );
    }

    /**
     * Возвращает интерфейс компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // столбцы
        $columns = array(
            array('columnWidth' => 0.33, 'style' => 'padding:10px 0 10px 10px', 'items' => array()),
            array('columnWidth' => 0.33, 'style' => 'padding:10px 0 10px 10px', 'items' => array()),
            array('columnWidth' => 0.33, 'style' => 'padding:10px 0 10px 10px', 'items' => array())
        );
        // соединения с базой данных
        GFactory::getDb()->connect();
        $query = new GDb_Query();
        // user group id
        $groupId = $this->session->get('group/id');
        $sql = 'SELECT `c`.*,`s`.* '
             . 'FROM `gear_controller_access` `s`, `gear_controller_groups` `g`, '
             . '(SELECT `c`.*, `cl`.`controller_name`, `cl`.`controller_about` '
             . 'FROM `gear_controllers` `c` JOIN `gear_controllers_l` `cl` USING(`controller_id`) '
             . 'WHERE `cl`.`language_id`=' . $this->language->get('id') . ') `c` '
             . 'WHERE `s`.`controller_id`=`c`.`controller_id` '
             . 'AND `c`.`controller_visible`=1 AND `c`.`controller_dashboard`=1 '
             . 'AND `g`.`group_id`=`c`.`group_id` AND `s`.`group_id`=' . $groupId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $data = array();
        $c = 0;
        while (!$query->eof()) {
            $controller = $query->next();
            $columns[$c]['items'][] = $this->getDashboard($controller);
            $c++;
            if ($c >= 3)
                $c = 0;
        }
        $this->_cmp->items->addItems($columns);
        $this->_cmp->iconSrc = $this->resourcePath . 'icon.png';
        $this->_cmp->iconTpl = $this->resourcePath . 'shortcut.png';

        parent::getInterface();
    }
}
?>