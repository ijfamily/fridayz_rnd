<?php
/**
 * Gear Manager
 *
 * Контроллер         "Инициализация данных оболочки"
 * Пакет контроллеров "Инициализация оболочки"
 * Группа пакетов     "Оболочка"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2012-10-04 12:00:00 Gear Magic $
 */

/**
 * Get JSON array for menu
 * 
 * @param  $arr array menu
 * @return string
 */
function jsonMenu($arr)
{
    $out = '';
    foreach($arr as $k => $v) {
        $menuItemText = empty($arr[$k]['menu_item_text']) ? '' : $arr[$k]['menu_item_text'];
        if ($menuItemText == '-') {
            $out .= '"-"';
            if (next($arr))
                $out .= ', ';
        }
        else {
            $out .= "\r\n" . '{text: "' . trim($menuItemText) . '"';
            if (!empty($arr[$k]['menu_item_iconcls']))
                $out .= ', iconCls: "' . $arr[$k]['menu_item_iconcls'] . '"';
            if (!empty($arr[$k]['menu_item_icon']))
                $out .= ', icon: "' . $arr[$k]['menu_item_icon'] . '"';
            if (!empty($arr[$k]['menu_item_id']))
                $out .= ', id: "' . $arr[$k]['menu_item_id'] . '"';
            if (!empty($arr[$k]['menu_item_handler']))
                $out .= ', handler: function(){' . $arr[$k]['menu_item_handler'] . '}';
            if (!empty($arr[$k]['menu_item_listeners']))
                $out .= ', listeners: {' . $arr[$k]['menu_item_listeners'] . '}';
            if (!empty($arr[$k]['menu_item_field']))
                $out .= ', ' . $arr[$k]['menu_item_field'] . ': ' . $arr[$k]['menu_item_value'];
            if (!empty($arr[$k]['menu_item_popup']))
                $out .= ', menu: {xtype: "menu"}';
            if (!empty($arr[$k]['menu_item_disabled']))
                $out .= ', disabled: true';
            if (!empty($arr[$k]['menu_item_hidden']))
                $out .= ', hidden: true';
            if (!empty($arr[$k]['menu_xtype']))
                $out .= ', menu: {xtype: "' . $arr[$k]['menu_xtype'] . '"}';

            if(!empty($v['children'])){
                $out .= ', menu: {xtype: "menu"';
            if ($arr[$k]['menu_menu_id'])
                $out .= ', id: "' . $arr[$k]['menu_menu_id'] . '"';
                $out .= ', items:[' . jsonMenu($v['children']) . ']}';
            }
            if (!next($arr))
                $out .= '}';
            else
                $out .= '}, ';
        }
    }

    return $out;
}


/**
 * Данные для оболочки меню
 * 
 * @category   Gear
 * @package    GController_YShell_Menu
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2012-10-01 12:00:00 Gear Magic $
 */
final class GController_YShell_Init_Data extends GController
{
    /**
     * Возращает меню для кнопки "Пуск"
     * 
     * @return mixed
     */
    protected function getStartMenu(&$tree)
    {
        foreach ($tree as $id => $item) {
            if ($item['menu_item_id'] == 'menu-file') {
                $menu = array($item['children']);
                unset($tree[$id]);

                return $menu;
            }
        }
    }

    /**
     * Возращает пункт меню для быстрого доступа к компоненту
     * 
     * @return array
     */
    protected function getComponentItem($cmp)
    {
        return array(
            'menu_item_icon'    => PATH_COMPONENT . $cmp['controller_uri']. 'resources/icon.png',
            'menu_parent_id'    => 50,
            'menu_item_text'    => $cmp['controller_profile'],
            'menu_id'           => $cmp['controller_id'],
            'menu_item_handler' => "Manager.widget.load({url: Manager.COMPONENTS_URL + '" 
                                  . $cmp['controller_uri_pkg']. "' + Ext.ROUTER_DELIMITER + '" 
                                  . $cmp['controller_uri_menu'] . "'});"
        );
    }

    /**
     * Возращает пункт меню для быстрого доступа к компоненту
     * 
     * @return array
     */
    protected function getComponentSp($parentId)
    {
        return array(
            'menu_parent_id'    => $parentId,
            'menu_item_text'    => '-',
            'menu_id'           => $cmp['controller_id'],
            'menu_item_handler' => "Manager.widget.load({url: Manager.COMPONENTS_URL + '" 
                                  . $cmp['controller_uri_pkg']. "' + Ext.ROUTER_DELIMITER + '" 
                                  . $cmp['controller_uri_menu'] . "'});"
        );
    }

    /**
     * Возращает список компонентов меню
     * 
     * @return mixed
     */
    protected function getComponents()
    {
        $query = new GDb_Query();
        $sql = 'SELECT `a`.*, `c`.* '
             . 'FROM (SELECT `c`.*, `cl`.`controller_profile` '
             . 'FROM `gear_controllers` `c` JOIN `gear_controllers_l` `cl` '
             . 'ON `c`.`controller_id`=`cl`.`controller_id` AND `c`.`controller_menu`=1 AND '
             . '`c`.`controller_visible`=1  AND `cl`.`language_id`=' . $this->language->get('id') . ') `c` '
             . 'JOIN `gear_controller_access` `a` USING(`controller_id`) '
             . 'WHERE `a`.`group_id`=' . $this->session->get('group/id')
             . ' ORDER BY `c`.`controller_profile` ASC';
        $query->execute($sql);
        $items = array();
        while (!$query->eof()) {
            $cmp = $query->next();
            $items[$cmp['controller_id']] = $this->getComponentItem($cmp);
        }

        return $items;
    }

    /**
     * Возращает список компонентов настройки
     * 
     * @return mixed
     */
    protected function getConfigComponents($menuId)
    {
        $query = new GDb_Query();
        $sql = 'SELECT `a`.*, `c`.* '
             . 'FROM (SELECT `c`.*, `cl`.`controller_profile` '
             . 'FROM `gear_controllers` `c` JOIN `gear_controllers_l` `cl` '
             . 'ON `c`.`controller_id`=`cl`.`controller_id` AND `c`.`controller_menu_config`=1 AND '
             . '`c`.`controller_visible`=1  AND `cl`.`language_id`=' . $this->language->get('id') . ') `c` '
             . 'JOIN `gear_controller_access` `a` USING(`controller_id`) '
             . 'WHERE `a`.`group_id`=' . $this->session->get('group/id')
             . ' ORDER BY `c`.`module_id`, `c`.`controller_profile` ASC';
        $query->execute($sql);
        $items = array();
        $moduleId = 0;
        $cmp = $query->first();
        if ($cmp) {
            $moduleId = $cmp['module_id'];
            $items[$cmp['controller_id']] = $this->getComponentItem($cmp);
        }
        while (!$query->eof()) {
            $cmp = $query->next();
            if ($moduleId != $cmp['module_id']) {
                $items[uniqid()] = array(
                    'menu_parent_id'    => $menuId,
                    'menu_item_text'    => '-'
                );
                $moduleId = $cmp['module_id'];
            }
            $items[$cmp['controller_id']] = $this->getComponentItem($cmp);
        }

        return $items;
    }

    /**
     * Формирование данных для интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        // соединение с базой данных
        GFactory::getDb()->connect();
        $query = new GDb_Query();
        $sql = 'SELECT `m`.*, `ml`.`menu_item_text` '
             . 'FROM `gear_menu` `m`, `gear_menu_l` `ml` '
             . 'WHERE `m`.`menu_id`=`ml`.`menu_id` AND '
             . '`ml`.`language_id`=' . $this->language->get('id')
             . ' ORDER BY `m`.`menu_parent_id`, `m`.`menu_item_index` ASC';
        $query->execute($sql);
        $levels = array();
        $tree = array();
        $cur = array();
        while (!$query->eof()) {
            $record = $query->next();
            $cur = &$levels[$record['menu_id']];
            $cur['menu_parent_id'] = $record['menu_parent_id'];
            $cur['menu_item_text'] = $record['menu_item_text'];
            $cur['menu_item_iconcls'] = $record['menu_item_iconcls'];
            $cur['menu_item_handler'] = $record['menu_item_handler'];
            $cur['menu_item_id'] = $record['menu_item_id'];
            $cur['menu_menu_id'] = $record['menu_menu_id'];
            $cur['menu_item_popup'] = $record['menu_item_popup'];
            $cur['menu_item_type'] = $record['menu_item_type'];
            $cur['menu_item_field'] = $record['menu_item_field'];
            $cur['menu_item_value'] = $record['menu_item_value'];
            $cur['menu_item_disabled'] = $record['menu_item_disabled'];
            $cur['menu_xtype'] = $record['menu_xtype'];
            $cur['menu_item_hidden'] = $record['menu_item_hidden'];
            $cur['menu_item_listeners'] = $record['menu_item_listeners'];
            $cur['menu_id'] = $record['menu_id'];
            if($record['menu_parent_id'] == 0)
                $tree[$record['menu_id']] = &$cur;
            else
                $levels[$record['menu_parent_id']]['children'][$record['menu_id']] = &$cur;
        }
        // добавление к пункту меню "Добавить" компоненты для бытсрого доступа
        if (isset($tree[50]))
          $tree[50]['children'] = $this->getComponents();
        // добавление к пункту меню "Настройки"
        if (isset($tree[16])) {
            $tree[16]['children'] = $this->getConfigComponents(16);
        }
        $start = $this->getStartMenu($tree);

        return array('start' => jsonMenu($start[0]), 'menu' => jsonMenu($tree));
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @return void
     */
    protected function dataView()
    {
        $data = $this->getDataInterface();

        header("Content-Type: text/javascript");
        $userSettings = $this->session->get('user/settings');
?>
Ext.extend(Manager.menu.Menu, Ext.Toolbar, {
    initComponent: function(){
        Manager.menu.Menu.superclass.initComponent.call(this);
        this.add(<?php echo $data['menu'];?>);
    }
});

Manager.init.MenuStart = {items: [<?php echo $data['start'];?>]};
Manager.init.language = '<?php echo $this->session->get('language/default/alias');?>';
Manager.init.theme = '<?php echo $userSettings['theme'];?>';
Manager.init.desktop = '<?php echo $userSettings['desktop'];?>';
Manager.init.errorMsg = '<?php echo $userSettings['receive/js/errors'];?>';
<?php
    }

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        // метода запроса
        switch ($this->uri->method) {
            // method "GET"
            case 'GET':
                // вывод данных в интерфейс
                $this->dataView();
                exit;
        }

        parent::init();
    }
}
?>