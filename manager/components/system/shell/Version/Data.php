<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля версии системы"
 * Пакет контроллеров "Оболочка"
 * Группа пакетов     "Система"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YShell_Version
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные профиля версии системы
 * 
 * @category   Gear
 * @package    GController_YShell_Version
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YShell_Version_Data extends GController_Profile_Data
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_yshell_version';

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        Gear::library('Version');

        parent::dataAccessView();

        $data = array();
        //$vApp = new TVersion_App();
        //$vCore = new TVersion_Core();
        // вкладка "Common"
        // version OS

        $this->response->data = $data;
    }
}
?>