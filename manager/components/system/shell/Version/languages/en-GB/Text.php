<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'version_title' => 'Version',
    // поля
    'title_set_client'         => 'Client',
    'title_tab_common'         => 'Common',
    'title_set_core'           => 'Core CMF',
    'label_core_version'       => 'Version',
    'label_core_date'          => 'Date',
    'title_set_system'         => 'System',
    'label_system_name'        => 'Name',
    'label_system_version'     => 'Version',
    'label_system_date'        => 'Date',
    'title_tab_modules'        => 'Modules',
    'title_set_module'         => 'Module - ',
    'label_module_name'        => 'Name',
    'label_browser'            => 'Browser version',
    'label_os'                 => 'OS version',
    'label_module_shortname'   => 'Name (sh.)',
    'label_module_date'        => 'Date',
    'label_module_version'     => 'Version',
    'label_module_components'  => 'Group (cmp.)',
    'label_module_description' => 'About',
    'label_module_author'      => 'Author',
    'title_tab_php'            => 'Preprocessor - PHP',
    'title_set_extension'      => 'Extensions',
    'title_set_params'         => 'Params',
    'label_php_version'        => 'PHP version',
    'title_tab_mysql'          => 'Server DB - MySQL',
    'title_set_mysqls'         => 'Server',
    'label_mysqls_version'     => 'Version server',
    'label_mysqls_proto'       => 'Protocol version',
    'label_mysqls_host'        => 'Server',
    'title_set_mysqlc'         => 'Client',
    'label_mysqlc_version'     => 'Version server',
    'title_tab_http'           => 'WEB-server',
    'label_version'            => 'Version',
    'label_db_driver'          => 'Driver',
    'title_set_shell'          => 'Shell',
    'label_shell_name'         => 'Name',
    'label_shell_alias'        => 'Alias',
    'label_shell_version'      => 'Version',
    'label_shell_date'         => 'Date',
    'label_result_extension'   => 'Нет доступа',
    'data_access_disabled'     => 'неизвестно'
);
?>