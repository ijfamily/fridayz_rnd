<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'version_title'            => 'Версія системи',
    // поля
    'title_set_client'         => 'Клієнт',
    'title_tab_common'         => 'Загальне',
    'title_set_core'           => 'Ядро CMF',
    'label_core_version'       => 'Версія',
    'label_core_date'          => 'Дата',
    'title_set_system'         => 'Система',
    'label_system_name'        => 'Назва',
    'label_system_version'     => 'Версія',
    'label_system_date'        => 'Дата',
    'title_tab_modules'        => 'Модулі',
    'title_set_module'         => 'Модуль - ',
    'label_module_name'        => 'Назва',
    'label_browser'            => 'Версія браузера',
    'label_os'                 => 'Версія ОС',
    'label_module_shortname'   => 'Назва (скр.)',
    'label_module_date'        => 'Дата',
    'label_module_version'     => 'Версія',
    'label_module_components'  => 'Груп (кмп.)',
    'label_module_description' => 'Опис',
    'label_module_author'      => 'Автор',
    'title_tab_php'            => 'Препроцесор - PHP',
    'title_set_extension'      => 'Розширення',
    'title_set_params'         => 'Параметри',
    'label_php_version'        => 'Версія PHP',
    'title_tab_mysql'          => 'Сервер СУБД - MySQL',
    'title_set_mysqls'         => 'Сервер',
    'label_mysqls_version'     => 'Версія сервера',
    'label_mysqls_proto'       => 'Версія протокола',
    'label_mysqls_host'        => 'Сервер',
    'title_set_mysqlc'         => 'Клієнт',
    'label_mysqlc_version'     => 'Версия клієнта',
    'title_tab_http'           => 'Веб-сервер',
    'label_version'            => 'Версія',
    'label_db_driver'          => 'Драйвер',
    'title_set_shell'          => 'Оболонка',
    'label_shell_name'         => 'Назва',
    'label_shell_alias'        => 'Псевдоним',
    'label_shell_version'      => 'Версія',
    'label_shell_date'         => 'Дата',
    'label_result_extension'   => 'Нет доступа',
    'data_access_disabled'     => 'неизвестно'
);
?>