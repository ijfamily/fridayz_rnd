<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'version_title' => 'Версия системы',
    // поля
    'title_set_client'         => 'Клиент',
    'title_tab_common'         => 'Общее',
    'title_set_core'           => 'Ядро CMF',
    'label_core_name'          => 'Название',
    'label_core_version'       => 'Версия',
    'label_core_date'          => 'Дата',
    'title_set_system'         => 'Система',
    'label_system_name'        => 'Название',
    'label_system_version'     => 'Версия',
    'label_system_date'        => 'Дата',
    'title_tab_modules'        => 'Модули',
    'title_set_module'         => 'Модуль - ',
    'label_module_name'        => 'Название',
    'label_browser'            => 'Версия браузера',
    'label_os'                 => 'Версия ОС',
    'label_module_shortname'   => 'Название (сокр.)',
    'label_module_date'        => 'Дата',
    'label_module_version'     => 'Версия',
    'label_module_components'  => 'Групп (кмп.)',
    'label_module_description' => 'Описание',
    'label_module_author'      => 'Автор',
    'title_tab_php'            => 'Препроцессор - PHP',
    'title_set_extension'      => 'Расширения',
    'title_set_params'         => 'Параметры',
    'label_php_version'        => 'Версия PHP',
    'title_tab_mysql'          => 'Сервер СУБД - MySQL',
    'title_set_mysqls'         => 'Сервер',
    'label_mysqls_version'     => 'Версия сервера',
    'label_mysqls_proto'       => 'Версия протокола',
    'label_mysqls_host'        => 'Сервер',
    'title_set_mysqlc'         => 'Клиент',
    'label_mysqlc_version'     => 'Версия клиента',
    'title_tab_http'           => 'Веб-сервер',
    'label_version'            => 'Версия',
    'label_db_driver'          => 'Драйвер',
    'title_set_shell'          => 'Оболочка',
    'label_shell_name'         => 'Название',
    'label_shell_alias'        => 'Псевдоним',
    'label_shell_version'      => 'Версия',
    'label_shell_date'         => 'Дата',
    'label_result_extension'   => 'Нет доступа',
    'data_access_disabled'     => 'неизвестно',
    'value_unknow'             => 'неизвестно'
);
?>