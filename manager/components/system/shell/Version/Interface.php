<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля версии системы"
 * Пакет контроллеров "Оболочка"
 * Группа пакетов     "Система"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YShell_Version
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля версии системы
 * 
 * @category   Gear
 * @package    GController_YShell_Version
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YShell_Version_Interface extends GController_Profile_Interface
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_yshell_version';

    /**
     * PHP ini
     *
     * @var array
     */
    protected $_php;

    /**
     * PHP ini
     *
     * @var array
     */
    protected $_phpVars = array(
        'display_errors', 'display_startup_errors', 'error_reporting', 'file_uploads', 'register_globals', 'post_max_size', 
        'upload_max_filesize', 'safe_mode', 'magic_quotes_gpc', 'max_file_uploads', 'max_execution_time', 'open_basedir'
    );

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        $this->_php = ini_get_all();
    }

    /**
     * Возращает php ini
     * 
     * @param  string раздел php ini
     * @return string
     */
    protected function getValueIni($name)
    {
        if (isset($this->_php[$name])) {
            $value = $this->_php[$name]['local_value'];

            if ($value === '1')
                return 'On';
            else
            if ($value === '0')
                return 'Off';
            if (strlen($value) == 0)
                return 'Off';
            else
                return $value;
        }

        return '?';
    }

    /**
     * Возвращает дополнительные данные для формирования интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        $data = array('modules' => array());

        // вкладка "Общее"
        // версия ядра системы
        $vCore = GFactory::getClass('Version_Core', 'Version');
        // версия приложения
        $vApp = GFactory::getClass('Version_Application', 'Version');
        // версия облочки
        $vShell = GFactory::getClass('Version_Shell', 'Version');
        $data['versionOS'] = $this->session->get('version/os');
        $data['versionBrowser'] = $this->session->get('version/browser');
        $data['coreName'] = $vCore->name;
        $data['coreVersion'] = $vCore->version;
        $data['coreDate'] = $vCore->update;
        $data['sysName'] = $vApp->name;
        $data['sysVersion'] = $vApp->version;
        $data['sysDate'] = $vApp->update;
        $data['shellAlias'] = $vShell->alias;
        $data['shellName'] = $vShell->name;
        $data['shellVersion'] = $vShell->version;
        $data['shellDate'] = $vShell->update;

        // вкладка "PHP"
        // php version
        $data['phpVersion'] = function_exists('phpversion') ? phpversion() : $this->_['data_access_disabled'];
        $data['php'] = array();
        for ($i = 0; $i < sizeof($this->_phpVars); $i++) {
            $data['php'][] = array(
                'fieldLabel' => $this->_phpVars[$i],
                'value'      => $this->getValueINI($this->_phpVars[$i])
            );
        }

        // вкладка "Модули"
        $settings = $this->session->get('user/settings');
        // соединение с базой данных
        $db = GFactory::getDb();
        $db->connect();
        // модуля системы
        $query = new GDb_Query();
        $sql = 'SELECT *, COUNT(`g`.`group_id`) `module_groups` '
             . 'FROM `gear_modules` `m`, `gear_controller_groups` `g` '
             . 'WHERE `g`.`module_id`=`m`.`module_id` '
             . 'GROUP BY `g`.`module_id`';
        $query->execute($sql);
        while (!$query->eof()) {
            $module = $query->next();
            if ($module['module_date'])
                $date = date($settings['format/date'], strtotime($module['module_date']));
            else
                $date = '';
            $fieldset = array(
              'xtype'      => 'fieldset',
              'labelWidth' => 83,
              'title'      => $this->_['title_set_module'] . $module['module_name'],
              'autoHeight' => true,
              'defaults'   => array('width' => 330, 'xtype' => 'displayfield', 'fieldClass' => 'mn-field-value-info'),
              'items'      => array(
                    array('fieldLabel' => $this->_['label_module_name'], 'value' => $module['module_name'] . ' (' . $module['module_shortname'] . ')'),
                    array('fieldLabel' => $this->_['label_module_description'], 'value' => $module['module_description']),
                    array('fieldLabel' => $this->_['label_module_version'], 'value' => $module['module_version'], 'fieldClass' => 'mn-field-value-info b'),
                    array('fieldLabel' => $this->_['label_module_date'], 'value' => $date),
                    array('fieldLabel' => $this->_['label_module_author'], 'value' => $module['module_author'], 'fieldClass' => 'mn-field-value-info b')
                )
            );
            $data['modules'][] = $fieldset;
        }

        // вкладка "Сервер СУБД"
        $data['driverDb'] = $this->config->get('DB/DRIVER');
        if (($data['serverInfo'] = $db->getServerInfo()) === false)
            $data['serverInfo'] = $this->_['data_access_disabled'];
        if (($data['serverProto'] = $db->getProtoInfo()) === false)
            $data['serverProto'] = $this->_['data_access_disabled'];
        if (($data['hostInfo'] = $db->getHostInfo()) === false)
            $data['hostInfo'] = $this->_['data_access_disabled'];
        if (($data['clientInfo'] = $db->getClientInfo()) === false)
            $data['clientInfo'] = $this->_['data_access_disabled'];
        $ext = $this->config->get('PHP/EXTENSIONS');
        if (function_exists('get_loaded_extensions')) {
            $list = get_loaded_extensions();
            $count = count($list);
            $result = '';
            for ($i = 0; $i < $count; $i++) {
                if (in_array($list[$i], $ext))
                    $result .= '<b>' . $list[$i] . '</b>';
                else
                    $result .= $list[$i];
                if ($i < $count - 1)
                    $result .= ', ';
                else
                    $result .= ' ';
            }
            $data['phpExtensions'] = $result;
        } else
            $data['phpExtensions'] = $this->_['data_access_disabled'];

        return $data;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        $data = $this->getDataInterface();

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['version_title'],
                  'titleEllipsis' => 40,
                  'width'         => 640,
                  'autoHeight'    => true,
                  'resizable'     => false,
                  'stateful'      => false,
                  'iconSrc'       => $this->resourcePath . 'icon-form.png',
                  'state'         => 'info')
        );

        // поля формы (Ext.form.FormPanel)
        // вкладка "модули"
        $tabModules = array(
            'title'       => $this->_['title_tab_modules'],
            'layout'      => 'form',
            'labelWidth'  => 70,
            'defaultType' => 'displayfield',
            'bodyStyle'   => 'background: url("' . $this->resourcePath. 'tab-bg-common.png") no-repeat center center;',
            'items'       => array($data['modules'])
        );

        // вкладка "общее"
        $tabCommon = array(
            'title'       => $this->_['title_tab_common'],
            'layout'      => 'form',
            'labelWidth'  => 70,
            'bodyStyle'   => 'background: url("' . $this->resourcePath. 'tab-bg-common.png") no-repeat center center;',
            'items'       => array(
                array('xtype'      => 'fieldset',
                      'title'      => $this->_['title_set_client'],
                      'autoHeight' => true,
                      'labelWidth' => 105,
                      'defaults'   => array('xtype' => 'displayfield', 'fieldClass' => 'mn-field-value-info'),
                      'items'      => array(
                           array('fieldLabel' => $this->_['label_browser'], 'value' => $data['versionBrowser']),
                           array('fieldLabel' => $this->_['label_os'], 'value' => $data['versionOS'])
                      )
                ),
                array('xtype'      => 'fieldset',
                      'title'      => $this->_['title_set_core'],
                      'autoHeight' => true,
                      'labelWidth' => 70,
                      'defaults'   => array('xtype' => 'displayfield', 'fieldClass' => 'mn-field-value-info'),
                      'items'      => array(
                           array('fieldLabel' => $this->_['label_core_name'], 'value' => $data['coreName']),
                           array('fieldLabel' => $this->_['label_core_version'], 'value' => $data['coreVersion'], 'fieldClass' => 'mn-field-value-info b'),
                           array('fieldLabel' => $this->_['label_core_date'], 'value' => $data['coreDate'])
                      )
                ),
                array('xtype'      => 'fieldset',
                      'title'      => $this->_['title_set_shell'],
                      'autoHeight' => true,
                      'labelWidth' => 70,
                      'defaults'   => array('xtype' => 'displayfield', 'fieldClass' => 'mn-field-value-info'),
                      'items'      => array(
                           array('fieldLabel' => $this->_['label_shell_name'], 'value' => $data['shellName']),
                           array('fieldLabel' => $this->_['label_shell_alias'], 'value' => $data['shellAlias']),
                           array('fieldLabel' => $this->_['label_shell_version'], 'value' => $data['shellVersion'], 'fieldClass' => 'mn-field-value-info b'),
                           array('fieldLabel' => $this->_['label_shell_date'], 'value' => $data['shellDate'])
                      )
                ),
                array('xtype'      => 'fieldset',
                      'title'      => $this->_['title_set_system'],
                      'autoHeight' => true,
                      'labelWidth' => 70,
                      'defaults'   => array('xtype' => 'displayfield', 'fieldClass' => 'mn-field-value-info'),
                      'items'      => array(
                           array('fieldLabel' => $this->_['label_system_name'], 'value' => $data['sysName']),
                           array('fieldLabel' => $this->_['label_system_version'], 'value' => $data['sysVersion'], 'fieldClass' => 'mn-field-value-info b'),
                           array('fieldLabel' => $this->_['label_system_date'], 'value' => $data['sysDate'])
                      )
                )
            )
        );

        // проверка доступа
        if ($this->checkPrivilege)
            if ($this->store->hasPrivilege(array('root', 'select'))) {
                // вкладка "PHP"
                $tabPHP = array(
                   'title'       => $this->_['title_tab_php'],
                   'layout'      => 'form',
                   'iconSrc'     => $this->resourcePath . 'icon-tab-php.png',
                   'bodyStyle'   => 'background: url("' . $this->resourcePath. 'tab-bg-common.png") no-repeat center center;',
                   'labelWidth'  => 75,
                   'defaults'   => array('xtype' => 'displayfield', 'fieldClass' => 'mn-field-value-info b'),
                   'items'       => array(
                       array('fieldLabel' => $this->_['label_php_version'], 'value' => $data['phpVersion']),
                       array('xtype'      => 'fieldset',
                             'title'      => $this->_['title_set_extension'],
                             'autoHeight' => true,
                             'html'       => '<span style="font-size:13px;">' . $data['phpExtensions'] . '</span>'),
                       array('xtype'      => 'fieldset',
                             'labelWidth' => 135,
                             'title'      => $this->_['title_set_params'],
                             'defaults'   => array('xtype' => 'displayfield', 'fieldClass' => 'mn-field-value-info b'),
                             'autoHeight' => true,
                             'items'      => $data['php']
                       )
                   )
                );

                // вкладка "MySQL"
                $tabMySQL = array(
                    'title'       => $this->_['title_tab_mysql'],
                    'layout'      => 'form',
                    'iconSrc'     => $this->resourcePath . 'icon-tab-mysql.png',
                    'bodyStyle'   => 'background: url("' . $this->resourcePath. 'tab-bg-common.png") no-repeat center center;',
                    'items'       => array(
                        array('xtype'      => 'fieldset',
                              'labelWidth' => 115,
                              'title'      => $this->_['title_set_mysqls'],
                              'autoHeight' => true,
                              'defaults'   => array('width' => 160, 'xtype' => 'displayfield', 'fieldClass' => 'mn-field-value-info'),
                              'items'      => array(
                                  array('fieldLabel' => $this->_['label_mysqls_version'], 'value' => $data['serverInfo'], 'fieldClass' => 'mn-field-value-info b'),
                                  array('fieldLabel' => $this->_['label_mysqls_proto'], 'value' => $data['serverProto'], 'fieldClass' => 'mn-field-value-info b'),
                                  array('fieldLabel' => $this->_['label_mysqls_host'], 'value' => $data['hostInfo'])
                              )
                        ),
                        array('xtype'      => 'fieldset',
                              'labelWidth' => 115,
                              'title'      => $this->_['title_set_mysqlc'],
                              'autoHeight' => true,
                              'defaults'   => array('xtype' => 'displayfield', 'fieldClass' => 'mn-field-value-info'),
                              'items'      => array(
                                   array('fieldLabel' => $this->_['label_db_driver'], 'value' => $data['driverDb']),
                                   array('fieldLabel' => $this->_['label_mysqlc_version'], 'value' => $data['clientInfo'])
                                   )
                              )
                        )
                );

                $webVersion = $this->config->get('SERVER/WEB');
                // вкладка "Веб-сервер"
                switch ($webVersion) {
                    case 'apache':
                        $modules = function_exists('apache_get_modules') ? implode(', ', apache_get_modules()) : $this->_['data_access_disabled'];
                        $tabWeb = array(
                            'title'       => $this->_['title_tab_http'] . ' - Apache',
                            'layout'      => 'form',
                            'labelWidth'  => 50,
                            'iconSrc'     => $this->resourcePath . 'icon-tab-apache.png',
                            'bodyStyle'   => 'background: url("' . $this->resourcePath. 'tab-bg-common.png") no-repeat center center;',
                            'defaults'   => array('xtype' => 'displayfield', 'fieldClass' => 'mn-field-value-info'),
                            'items'       => array(
                                array('fieldLabel' => $this->_['label_version'],
                                      'value'      => function_exists('apache_get_version') ? apache_get_version() : $this->_['data_access_disabled']),
                                array('xtype'      => 'fieldset',
                                      'labelWidth' => 107,
                                      'title'      => $this->_['title_set_extension'],
                                      'autoHeight' => true,
                                      'html'       => '<span style="font-size:13px;">' . $modules . '</span>')
                            )
                        );
                        break;

                    default:
                        $tabWeb = array(
                            'title'       => $this->_['title_tab_http'],
                            'layout'      => 'form',
                            'labelWidth'  => 50,
                            'bodyStyle'   => 'background: url("' . $this->resourcePath. 'tab-bg-common.png") no-repeat center center;',
                            'defaults'   => array('xtype' => 'displayfield', 'fieldClass' => 'mn-field-value-info'),
                            'items'       => array(
                                array('fieldLabel' => $this->_['label_version'],
                                      'value'      => $this->_['value_unknow']),
                                array('xtype'      => 'fieldset',
                                      'labelWidth' => 107,
                                      'title'      => $this->_['title_set_extension'],
                                      'autoHeight' => true,
                                      'html'       => '<span style="font-size:13px;">' . $this->_['value_unknow'] . '</span>')
                            )
                        );
                }
        }

        // поля формы (Ext.form.FormPanel)
        $items = array(
            array('xtype'             => 'tabpanel',
                  'layoutOnTabChange' => true,
                  'deferredRender'    => false,
                  'activeTab'         => 0,
                  'enableTabScroll'   => true,
                  'anchor'            => '100%',
                  'height'            => 420,
                  'defaults'          => array('autoScroll' => true, 'baseCls' => 'mn-form-tab-body'),
                  'items'             => array($tabCommon, $tabModules))
        );
        // проверка доступа
        if ($this->checkPrivilege)
            if ($this->store->hasPrivilege(array('root', 'select'))) {
                $items[0]['items'][] = $tabPHP;
                $items[0]['items'][] = $tabMySQL;
                $items[0]['items'][] = $tabWeb;
            }

        // Ext_Form_DataProfile (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->url = $this->componentUrl . 'version/';
        $form->items->addItems($items);

        parent::getInterface();
    }
}
?>