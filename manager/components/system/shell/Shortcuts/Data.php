<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные ярлыков компонентов"
 * Пакет контроллеров "Ярлыки"
 * Группа пакетов     "Оболочка"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Shortcuts
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Ярлыки компонентов
 * 
 * @category   Gear
 * @package    Shortcuts
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YShell_Shortcuts_Data extends GController
{
    /**
     * Сортируемое поле
     * 
     * @var string
     */
    protected $_orderBy;

    /**
     * Группа компонентов
     * 
     * @var string
     */
    protected $_group = 1;

    /**
     * Сортировка
     * 
     * @var string
     */
    protected $_sort = 'group';

    /**
     * Вид списка (1 - ярлыки, 2 - группы компонентов)
     * 
     * @var integer
     */
    protected $_type = 1;

    /**
     * Конструктор
     * 
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // вид сортировки
        $this->_sort = $this->uri->getVar('sort', $this->_sort);
        // вид списка
        $this->_type = $this->uri->getVar('type', $this->_type);
        // если вид списка изменен не через меню, а берется из настроек пользователя при 1-й загруке
        if (!$this->uri->getVar('menu', false)) {
            $userSettings = $this->session->get('user/settings');
            $this->_type = $userSettings['shortcuts'];
        }
        // группа компонента
        $this->_group = $this->uri->getVar('group', $this->_group);
        // сортировка
        $this->_orderBy = $this->getSortField();
    }

    /**
     * Список ярлыков
     * 
     * @return void
     */
    protected function dataShortcuts()
    {
        // соединение с базой данных
        GFactory::getDb()->connect();
        // обработчик запросов
        $query = new GDb_Query();
        $data = array();
        $sql = 'SELECT `c`.*,`s`.*,`gl`.`group_name` '
             . 'FROM `gear_controller_access` `s`, '
               // компоненты
             . '(SELECT `c`.*, `cl`.`controller_name`, `cl`.`controller_about` '
             . 'FROM `gear_controllers` `c`, `gear_controllers_l` `cl` '
             . 'WHERE `c`.`controller_id`=`cl`.`controller_id` AND '
             . '`cl`.`language_id`=' . $this->language->get('id') . ') `c`, '
               // группы компонентов
             . '`gear_controller_groups` `g` JOIN `gear_controller_groups_l` `gl` USING(`group_id`) '
             . 'WHERE `s`.`controller_id`=`c`.`controller_id`'
             . ' AND `c`.`controller_visible`=1 AND `s`.`access_shortcut`=1'
             . ' AND `gl`.`language_id`=' . $this->language->get('id')
             . ' AND `g`.`group_id`=`c`.`group_id` AND `s`.`group_id`=' . $this->session->get('group/id') . ' '
             . 'ORDER BY ' . $this->_orderBy;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $controllerExit = $controllerBoard = array();
        $path = $this->config->get('DOCUMENT_ROOT');
        $sysPath = $this->config->get('PATH/APPLICATION') . PATH_COMPONENT;
        while (!$query->eof()) {
            $record = $query->next();
            $icon = $sysPath . $record['controller_uri'] . 'resources/' . ($record['controller_enabled'] ? 'shortcut.png' : 'shortcut-d.png');
            if (!file_exists($icon)) {
                $icon = $this->resourcePath . 'shortcut-none.png';
            }
            $controller = array(
                'title'    => $record['controller_name'],
                'tooltip'  => $record['controller_about'],
                'iconCls'  => $icon,
                'iconSh'   => 'block',
                'url'      => $record['controller_uri_pkg'] . ROUTER_DELIMITER . $record['controller_uri_action'],
                'enabled'  => $record['controller_enabled'],
                'is_group' => false
            );
            switch ($record['controller_id']) {
                case 21:
                    $controllerExit = $controller;
                    $controllerExit['iconSh'] = 'none';
                    break;

                case 22:
                    $controllerBoard = $controller;
                    $controllerBoard['iconSh'] = 'none';
                    break;

                default:
                    $data[] = $controller;
            }
        }
        if ($controllerBoard)
            array_unshift($data, $controllerBoard);
        if ($controllerExit)
            array_unshift($data, $controllerExit);

        $this->response->data = $data;
    }

    /**
     * Список компонентов
     * 
     * @return void
     */
    protected function dataComponents()
    {
        $data = array();
        // соединение с базой данных
        GFactory::getDb()->connect();
        // обработчик запросов
        $query = new GDb_Query();
        // выбранная группа компонентов
        $sql = 'SELECT * FROM `gear_controller_groups` WHERE `group_id`=' . $this->_group;
        $group = $query->getRecord($sql);
        if (empty($group))
            throw new GSqlException();

        // предок группы
        $sql = 'SELECT `pg`.*, `l`.`group_name`'
             . 'FROM `gear_controller_groups` `pg` JOIN `gear_controller_groups_l` `l` USING(`group_id`) '
             . 'WHERE `pg`.`group_left`<' . $group['group_left'] . ' AND '
             . '`pg`.`group_right`>' . $group['group_right'] . ' AND '
             . '`pg`.`group_level`=' . $group['group_level'] . '-1 AND '
             . '`l`.`language_id`=' . $this->language->get('id');
        $parent = $query->getRecord($sql);
        if ($parent === false)
            throw new GException('Data processing error1', 'Query error (Internal Server Error)', $query->getError(false), false);

        // группы компонентов
        $sql = 'SELECT cg.*, `cgl`.`group_name`, `c`.`group_count` '
             . 'FROM (SELECT g.group_left, g.group_right '
             . 'FROM gear_controller_access a, gear_controllers c, gear_controller_groups g '
             . 'WHERE a.controller_id=c.controller_id AND c.group_id=g.group_id AND a.group_id=' . $this->session->get('group/id') . ' '
             . 'GROUP BY g.group_id) ca, `gear_controller_groups_l` `cgl`, `gear_controller_groups` `cg` '
               // компоненты
             . 'LEFT JOIN (SELECT group_id, COUNT(controller_id) `group_count` FROM `gear_controllers` '
             . 'WHERE `controller_visible`=1 GROUP BY group_id) `c` ON (`c`.`group_id`=`cg`.`group_id`) '
             . "WHERE cg.group_left>{$group['group_left']} AND "
             . 'cg.group_right<' . $group['group_right'] . ' AND '
             . 'cg.group_level=' . ($group['group_level'] + 1) . ' AND '
             . 'ca.group_left>=cg.group_left AND ca.group_right<=cg.group_right AND '
             . '`cgl`.`group_id`=`cg`.`group_id` AND `cgl`.`language_id`=' . $this->language->get('id')
             . ' GROUP BY cg.group_id '
             . 'ORDER BY cgl.group_name';
        $query->execute($sql);
        if ($query->execute($sql) === false)
            throw new GSqlException();

        $data = array();
        if ($this->_group != 1) {
            array_unshift($data, array(
                'title'    => '...',
                'group'    => empty($parent) ? 0 :  $parent['group_id'],
                'tooltip'  => '',
                'iconCls'  => $this->resourcePath . 'shortcut-group-back.png',
                'iconSh'  => 'none',
                'enabled'  => true,
                'is_group' => true)
            );
        }
        if ($query->getCountRecords() > 0) {
            while (!$query->eof()) {
                $record = $query->next();
                $data[] = array(
                    'title'    => $record['group_name'],
                    'group'    => $record['group_id'],
                    'tooltip'  => $record['group_name'],
                    'iconCls'  => $this->resourcePath . 'shortcut-group.png',
                    'iconSh'  => 'none',
                    'enabled'  => true,
                    'is_group' => true
                );
            }
        }
        // компоненты
        $sql = 'SELECT `a`.*, `c`.* '
             . 'FROM (SELECT `c`.*, `cl`.`controller_name`, `cl`.`controller_about` '
             . 'FROM `gear_controllers` `c`, `gear_controllers_l` `cl` '
             . 'WHERE `c`.`controller_id`=`cl`.`controller_id` AND '
             . '`cl`.`language_id`=' . $this->language->get('id') . ') `c`, `gear_controller_access` `a` '
             . 'WHERE `c`.`controller_id`=`a`.`controller_id` AND '
             . '`c`.`group_id`=' . $this->_group . ' AND '
             . '`a`.`group_id`=' . $this->session->get('group/id') . ' AND '
             . '`c`.`controller_visible`=1 '
             . 'ORDER BY ' . $this->_orderBy;
        $query->execute($sql);
        $path = $this->config->get('DOCUMENT_ROOT');
        $sysPath = $this->config->get('PATH/APPLICATION') . PATH_COMPONENT;
        while (!$query->eof()) {
            $record = $query->next();
            $icon = $sysPath . $record['controller_uri'] . 'resources/'  . ($record['controller_enabled'] ? 'shortcut.png' : 'shortcut-d.png');
            if (!file_exists($icon))
                $icon = $this->resourcePath . 'shortcut-none.png';
            $data[] = array(
                'title'    => $record['controller_name'],
                'tooltip'  => $record['controller_about'],
                'iconCls'  => $icon,
                'iconSh'   => 'none',
                'url'      => $record['controller_uri_pkg'] . ROUTER_DELIMITER . $record['controller_uri_action'],
                'enabled'  => $record['controller_enabled'],
                'is_group' => false
            );
        }

        $this->response->data = $data;
    }

    /**
     * Возращает сортировку
     * 
     * @return string
     */
    private function getSortField()
    {
        switch ($this->_sort) {
            case 'name':
                return '`c`.`controller_name`';

            case 'group':
                if ($this->_type == 2)
                    return '`c`.`controller_name`';
                else
                    return '`gl`.`group_name`';

            case 'index':
                return '`c`.`controller_id`';
        }
    }

    /**
     * вывод данных в интерфейс
     * 
     * @return void
     */
    protected function dataView()
    {
        // request action
        switch ($this->_type) {
            // ярлыки
            case 1:
                $this->dataShortcuts();
                break;

            // компоненты
            case 2:
                $this->dataComponents();
                break;
        }
    }

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        // метод запроса
        switch ($this->uri->method) {
            // метод "GET"
            case 'GET':
                // вывод данных в интерфейс
                $this->dataView();
                return;
        }

        parent::init();
    }
}
?>