<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Створення облікового запису',
    'title_profile_update' => 'Зміна "облікового запису - %s"',
    // поля
    'label_user_name'          => 'Логін',
    'label_user_password'      => 'Пароль',
    'label_user_password-cfrm' => 'Пароль (пітв.)',
    'title_fieldset_acount'    => 'Обліковий запис',
    'title_fieldset_nacount'   => 'Новий обліковий запис',
    // сообщения
    'msg_mismatch_passwords' => 'значення полів "Пароль" і "Пароль (пітв.)" не збігаються!',
    'msg_wrong_password'     => 'Введений вами пароль облікового запису невірний!',
    'msg_wrong_username'     => 'Обліковий запис з логіном "%s" вже існує!',
    'You have incorrectly entered the "Login" or "Password"' => 'Вы неправильно ввели "Логин" или "Пароль"!'
);
?>