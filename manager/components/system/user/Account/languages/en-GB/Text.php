<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Create an account',
    'title_profile_update' => 'Update "account - %s"',
    // поля
    'label_user_name'          => 'Login',
    'label_user_password'      => 'Password',
    'label_user_password-cfrm' => 'Password (cfrm.)',
    'title_fieldset_acount'    => 'Account',
    'title_fieldset_nacount'   => 'New account',
    // сообщения
    'msg_mismatch_passwords' => 'value fields "Password" and "Password (cfrm.)" do not match!',
    'msg_wrong_password'     => 'The password you entered an incorrect account!',
    'msg_wrong_username'     => 'Account with the login "%s" already exists!',
    'You have incorrectly entered the "Login" or "Password"' => 'Вы неправильно ввели "Логин" или "Пароль"!'
);
?>