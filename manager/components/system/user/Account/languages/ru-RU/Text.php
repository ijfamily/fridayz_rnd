<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Создание учётной записи',
    'title_profile_update' => 'Изменение "учётной записи - %s"',
    // поля
    'label_user_name'          => 'Логин',
    'label_user_password'      => 'Пароль',
    'label_user_password-cfrm' => 'Пароль (потв.)',
    'title_fieldset_acount'    => 'Учётная запись',
    'title_fieldset_nacount'   => 'Новая учётная запись',
    // сообщения
    'msg_mismatch_passwords' => 'значение полей "Пароль" и "Пароль (потв.)" не совпадают!',
    'msg_wrong_password'     => 'Введенный вами пароль учетной записи неверный!',
    'msg_wrong_username'     => 'Учётная запись с логином "%s" уже существует!',
    'You have incorrectly entered the "Login" or "Password"' => 'Вы неправильно ввели "Логин" или "Пароль"!'
);
?>