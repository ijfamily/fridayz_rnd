<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные аккаунта пользователя"
 * Пакет контроллеров "Пользователь"
 * Группа пакетов     "Система"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YUser_Account
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные аккаунта пользователя
 * 
 * @category   Gear
 * @package    GController_YUser_Account
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YUser_Account_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы
     *
     * @var array
     */
    public $fields = array(
        'user_name', 'user_password', 'user_password_salt', 
        'user_visited', 'user_visited_trial', 'user_escaped', 'user_process');

    /**
     * Первичный ключ таблицы
     *
     * @var string
     */
    public $idProperty = 'user_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_users';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_yuser_account';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_profile_update'], $record['user_name']);
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        // массив полей с их соответствующими значениями
        $params = $this->input->getBy($this->fields);
        // шифрование пароля
        $password = Gear::getPassword($this->input->get('user_password'), true);
        $params['user_password_salt'] = $password['salt'];
        $params['user_password'] = $password['password'];
        // сброс параметров аккаунта
        $params['user_visited'] = '';
        $params['user_visited_trial'] = 0;
        $params['user_escaped'] = '';
        $params['user_process'] = '';

        parent::dataUpdate($params);
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        parent::dataView($sql);

        // переопределить идент. группы
        unset($this->response->data['group_id']);
        // скрыть пароль
        $this->response->data['user_password'] = '';
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        // состояние профиля "правка"
        if ($this->isUpdate) {
            // старый пароль 
            $resetPsw = $this->input->get('user_password-reset');
            $query = new GDb_Query();
            // аккаунт пользователя
            $sql = 'SELECT * FROM `gear_users` WHERE `user_id`=' . (int) $this->uri->id;
            $account = $query->getRecord($sql);
            if (empty($account))
                throw new GSqlException();
            // проверка пароля сброса
            if (!Gear::checkPassword($resetPsw, $account['user_password'], $account['user_password_salt'], true))
                throw new GException('Error',  $this->_['You have incorrectly entered the "Login" or "Password"']);
        }

        unset($params['profile_id']);
    }

    /**
     * Проверка существования записи с одинаковыми значениями
     * 
     * @param  array $params array (field => value, ....)
     * @return void
     */
    protected function isDataExist($params)
    {
        $table = new GDb_Table($this->tableName, $this->idProperty);
        if ($table->isDataExist(array('user_name' => $params['user_name']), (int)$this->uri->id) === true)
            throw new GException('Warning', sprintf($this->_['msg_wrong_username'], $params['user_name']));
    }
}
?>