<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс аккаунта пользователя"
 * Пакет контроллеров "Пользователь"
 * Группа пакетов     "Система"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YUser_Account
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2015-06-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс аккаунта пользователя
 * 
 * @category   Gear
 * @package    GController_YUser_Account
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2015-06-01 12:00:00 Gear Magic $
 */
final class GController_YUser_Account_Interface extends GController_Profile_Interface
{
    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор,
     * используется для инициализации атрибутов контроллера без конструктора
     * 
     * @return void
     */
    public function construct()
    {
        $this->uri->id = $this->session->get('user_id');
        $this->isInsert = empty($this->uri->id);
        if ($this->isInsert)
            $this->state = 'insert';
        $this->isUpdate = !$this->isInsert;
        if ($this->isUpdate)
            $this->state = 'update';
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'           => $this->_['title_profile_insert'],
                  'titleEllipsis'   => 40,
                  'width'           => 345,
                  'autoHeight'      => 258,
                  'resizable'       => false,
                  'btnDeleteHidden' => true,
                  'iconSrc'         => $this->resourcePath . 'icon-form.png',
                  'stateful'        => false)
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype'      => 'fieldset',
                  'labelWidth' => 90,
                  'title'      =>$this->_['title_fieldset_acount'],
                  'items'      => array(
                      array('xtype'         => 'textfield',
                            'inputType'     => 'password',
                            'fieldLabel'    =>$this->_['label_user_password'],
                            'name'          => 'user_password-reset',
                            'id'            => 'user_password-reset',
                            'maxLength'     => 32,
                            'width'         => 200,
                            'allowBlank'    => false),
                      array('xtype'         => 'hidden',
                            'name'          => 'user_password_salt')
                  )
            ),
            array('xtype'      => 'fieldset',
                  'labelWidth' => 90,
                  'title'      =>$this->_['title_fieldset_nacount'],
                  'items'      => array(
                       array('xtype'         => 'textfield',
                             'fieldLabel'    =>$this->_['label_user_name'],
                             'name'          => 'user_name',
                             'maxLength'     => 15,
                             'width'         => 200,
                             'checkDirty'    => false,
                             'allowBlank'    => false),
                       array('xtype'         => 'textfield',
                             'inputType'     => 'password',
                             'fieldLabel'    =>$this->_['label_user_password'],
                             'name'          => 'user_password',
                             'id'            => 'user_password',
                             'maxLength'     => 32,
                             'width'         => 200,
                             'allowBlank'    => false),
                       array('xtype'         => 'textfield',
                             'inputType'     => 'password',
                             'vtype'         => 'password',
                             'fieldLabel'    =>$this->_['label_user_password-cfrm'],
                             'name'          => 'user_password-cfrm',
                             'initialPassField' => 'user_password',
                             'width'         => 200,
                             'allowBlank'    => false)
                  )
            )
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->addItems($items);
        $form->url = $this->componentUrl . 'account/';

        parent::getInterface();
    }
}
?>