<?php
/**
 * Gear Manager
 *
 * Контроллер         "Выход пользователя"
 * Пакет контроллеров "Выход пользователя"
 * Группа пакетов     "Пользователь"
 * Модуль             "Система"
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Logout
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Form.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Выход пользователя
 * 
 * @category   Gear
 * @package    Logout
 * @subpackage Form
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Form.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YUser_Logout_Form extends GController
{
    /**
     * Обновить данные пользователя
     * 
     * @return void
     */
    protected function dataUpdate()
    {
        // соединение с базой данных
        GFactory::getDb()->connect();
        // обновить аккаунт
        $table = new GDb_Table('gear_users', 'user_id');
        $data = array('user_process' => '', 'user_escaped' => date('Y-m-d H:i:s'));
        $table->update($data, $this->session->get('user_id'));
        // обновить журнад
        $table = new GDb_Table('gear_user_escapes', 'escape_id');
        $data = array(
            'escape_date' => date('Y-m-d'),
            'escape_time' => date('H:i:s'),
            'user_id'     => $this->session->get('user_id')
        );
        $table->insert($data);
        $this->response->add('logout', true);
        // удалить сессию
        $this->session->destroy();
        // удлить токен
        setcookie('cms-token', null, 0, '/');
    }

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        // метод запроса
        switch ($this->uri->method) {
            // метод "GET"
            case 'GET':

            // метод "POST"
            case 'POST':
                // тип действия
                if ($this->uri->action == 'form') {
                    $this->dataUpdate();
                    return;
                }
                break;
        }

        parent::init();
    }
}
?>