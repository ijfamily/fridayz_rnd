<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    'Unable to perform user authentication' => 'Неможливо виконати авторизацію користувача, помилка сервера!',
    'Unable to check the level of user access to the system' => 'Неможливо виконати перевірку рівня доступу користувача в систему, помилка сервера!',
    'You can not log in' => 'Ви не можете виконати авторизацію, ваш IP адреса (%s) заблоковано!',
    'You have incorrectly entered the "Login" or "Password"' => 'Ви неправильно ввели "Логін" або "Пароль"!',
    'Your account has been disabled' => 'Ваш обліковий запис заблоковано, зверніться до адміністратора системи!',
    'Your account is not active or is damaged' => 'Ваш обліковий запис не активовано або пошкоджена, зверніться до адміністратора системи!',
    'Attempting unauthorized access to the system' => 'Спроба несанкціоінірованного доступу до системи (підміна даних)!',
    'Login contains invalid characters' => 'Значення поля "Логін", містить не коректні символи!',
    'Incorrect login length' => 'Довжина значення поля "Логін", повинна бути від %s-х до %s-і символів!',
    'You didn\'t fill in the login' => 'Ви не заповнили поле "Логін"!',
    'You didn\'t fill in the password' => 'Ви не заповнили поле "Пароль"!',
    'Your user group is inactive' => 'Ваша група користувачів неактивна, зверніться до адміністратора сиситеми!',
    'Invalid key to restore your account' => 'Неправильно введений ключ для відновлення облікового запису!',
    'The key has been used previously' => 'Ключ для відновлення облікового запису був використаний раніше!',
    'Failed to restore account' => 'Виникла помилка при відновленні облікового запису!'
);
?>