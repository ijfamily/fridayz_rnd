<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    'Unable to perform user authentication' => 'Невозможно выполнить авторизацию пользователя, ошибка сервера!',
    'Unable to check the level of user access to the system' => 'Невозможно выполнить проверку уровня доступа пользователя в систему, ошибка сервера!',
    'You can not log in' => 'Вы не можете выполнить авторизацию, ваш IP адрес (%s) заблокирован!',
    'You have incorrectly entered the "Login" or "Password"' => 'Вы неправильно ввели "Логин" или "Пароль"!',
    'Your account has been disabled' => 'Ваша учетная запись заблокирована, обратитесь к администратору системы!',
    'Your account is not active or is damaged' => 'Ваша учетная запись не активна или повреждена, обратитесь к администратору системы!',
    'Attempting unauthorized access to the system' => 'Попытка несанкционированного доступа к системе (подмена данных)!',
    'Login contains invalid characters' => 'Значение поля "Логин", содержит не корректные символы!',
    'Incorrect login length' => 'Длина значения поля "Логин", должна быть от %s-х до %s-и символов!',
    'You didn\'t fill in the login' => 'Вы не заполниили поле "Логин"!',
    'You didn\'t fill in the password' => 'Вы не заполнили поле "Пароль"!',
    'Your user group is inactive' => 'Ваша группа пользователей неактивна, обратитесь к администратору сиситемы!',
    'Invalid key to restore your account' => 'Неправильно введён ключ для восстановления учётной записи!',
    'The key has been used previously' => 'Ключ для восстановления учётной записи был использован ранее!',
    'Failed to restore account' => 'Возникла ошибка при восстановлении учётной записи!'
);
?>