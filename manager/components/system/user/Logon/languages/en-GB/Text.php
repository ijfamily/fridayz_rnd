<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    'Unable to perform user authentication' => 'Unable to perform user authentication, server error!',
    'Unable to check the level of user access to the system' => 'Unable to check the level of user access to the system, server error!',
    'You can not log in' => 'You can not log in, your IP address (%s) is locked!',
    'You have incorrectly entered the "Login" or "Password"' => 'You have incorrectly entered the "Login" or "Password"!',
    'Your account has been disabled' => 'Your account has been disabled, contact your system administrator!',
    'Your account is not active or is damaged' => 'Your account is not active or is damaged, contact your system administrator!',
    'Attempting unauthorized access to the system' => 'Attempting unauthorized access to the system (spoofing)!',
    'Login contains invalid characters' => 'The field "Login" contains the correct characters!',
    'Incorrect login length' => 'The length of the field "Login" should be between %s-s to %s-and characters!',
    'You didn\'t fill in the login' => 'You didn\'t fill in the "Login"!',
    'You didn\'t fill in the password' => 'You didn\'t fill in the "Password"!',
    'Your user group is inactive' => 'Your user group is inactive, contact the administrator of the reference system!',
    'Invalid key to restore your account' => 'Invalid key to restore your account!',
    'The key has been used previously' => 'The key is to restore the account has been used before!',
    'Failed to restore account' => 'An error occurred while restoring your account!'
);
?>