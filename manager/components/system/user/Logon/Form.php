<?php
/**
 * Gear Manager
 * 
 * Контроллер         "Авторизация пользователя"
 * Пакет контроллеров "Пользователь"
 * Группа пакетов     "Система"
 * Модуль             "Система"
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YUser_Logon
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Form.php 2015-09-01 12:00:00 Gear Magic $
 */

Gear::ns('User/Log');

/**
 * Авторизация пользователя
 * 
 * @category   Gear
 * @package    GController
 * @subpackage Logon
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Form.php 2015-09-01 12:00:00 Gear Magic $
 */
class GController_Logon extends GController
{
    /**
     * Проверка учетной записи пользователя
     *
     * @var boolean
     */
    public $checkAccount = false;

    /**
     * Версия ОС
     *
     * @var string
     */
    protected $_os = '';

    /**
     * Версия браузера
     *
     * @var string
     */
    protected $_browser = '';

    /**
     * Имя пользователя
     *
     * @var string
     */
    protected $_username;

    /**
     * Пароль пользователя
     *
     * @var string
     */
    protected $_password;

    /**
     * Хэш код
     *
     * @var array
     */
    protected $_hash;

    /**
     * Хэш ключ
     *
     * @var string
     */
    protected $_hashKey;

    /**
     * База данных
     *
     * @var object
     */
    protected $_db;

    /**
     * Invisible
     *
     * @var string
     */
    protected $_invisible;

    /**
     * Если invisible
     *
     * @var boolean
     */
    protected $_isInvisible = false;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // соединение с базой данных
        GFactory::getDb()->connect();
        $this->input = GFactory::getInput();
        $this->browser = GFactory::getBrowser();
        $this->browser->detect();
        $this->_query = new GDb_Query();
        // ключ для восстановления акаунта
        $this->_restoreKey = $this->input->getString('restore');
        // логин пользователя
        $this->_username = $this->input->getString('username');
        // пароль пользователя
        $this->_password = $this->input->getString('password');
        // хэш код
        $this->_hash = $this->input->get('hash');
        // хэш ключ
        $this->_token = $this->input->get('token');
        $this->_invisible = $this->input->get('invisible');

        GUser_Log_Attend::defData(array(
            'attend_browser'   => $this->browser->fullName,
            'attend_os'        => $this->browser->getOs(),
            'attend_name'      => $this->_username,
            'attend_password'  => $this->_password,
        ));
    }

    /**
     * Занесения данных пользователя в журнал авторизации
     * 
     * @param  array $data данные авторизации
     * @return void
     */
    /*
    private function addUserAttend($data)
    {
        $default = array(
            'attend_ipaddress' => $_SERVER['REMOTE_ADDR'],
            'attend_browser'   => $this->browser->fullName,
            'attend_os'        => $this->browser->getOs(),
            'attend_name'      => $this->_username,
            'attend_password'  => $this->_password,
            'attend_date'      => date('Y-m-d'),
            'attend_time'      => date('H:i:s'),
            'attend_success'   => 0
        );
        $table = new GDb_Table('gear_user_attends', 'attend_id');
        $data = array_merge($default, $data);
        $table->insert($data);
    }
    */

    /**
     * Проверка акаунта пользователя
     * 
     * @return void
     */
    private function check()
    {
        try {
            // проверка логина пользователя
            $this->checkUsername();
            // проверка пароля пользователя
            $this->checkPassword();
            // проверка хэша
            $this->checkHash();
            // проверка ip адреса
            $this->checkIPaddress();
            // восстановления акаунта если есть ключ
            if ($this->_restoreKey)
               $this->checkRestore();
            // если invisible
            if ($this->_invisible)
                $this->checkInvisible();
            // проверка акаунта пользователя
            $account = $this->checkAccount();
            // обновление данных пользователя в хранилище
            $this->updateSession($account);
            //
            $this->success();
        } catch(GException $e) {}
    }

    /**
     * Проверка логина пользователя
     * 
     * @return void
     */
    private function checkUsername()
    {
        $length = strlen($this->_username);
        if ($length == 0)
            throw new GException('Error', $this->_['You didn\'t fill in the login']);
        if (!(($length > 2) && ($length < 16)))
            throw new GException('Error', sprintf($this->_['Incorrect login length'], 3, 16));
        if(!preg_match('/^([0-9a-z]{'. 3 . ',' . 16 . '})$/i', $this->_username))
            throw new GException('Error', $this->_['Login contains invalid characters']);
    }

    /**
     * Проверка пароля пользователя
     * 
     * @return void
     */
    private function checkPassword()
    {
        $length = strlen($this->_password);
        if ($length == 0)
            throw new GException('Error', $this->_['You didn\'t fill in the password']);
    }

    /**
     * Проверка хэшей
     * 
     * @return void
     */
    private function checkHash()
    {
        $hash = GFactory::getHash();
        if ($hash->check($this->_token, $this->_hash) != true)
            throw new GException('Error', $this->_['Attempting unauthorized access to the system']);
    }

    /**
     * Проверка ip адреса пользователя
     * 
     * @return void
     */
    private function checkIPaddress()
    {
        $sql = 'SELECT * FROM `gear_secure_ipaddress` WHERE `secure_disabled`=1';
        $query = new GDb_Query();
        $query->execute($sql);
        $list = array();
        while (!$query->eof()) {
            $ip = $query->next();
            $list[] = $ip['secure_ipaddress'];
        }
        /*
        if ($list)
            if (TNet::checkIPAddress(TNet::getIPAddress(), $list) == true) {
                $message = sprintf($this->_['You can not log in'], TNet::getIPAddress());
                $this->updateUserAttand(array('attend_error' => $message));
                throw new GException('Error', $message);
            }
        */
    }

    /**
     * Проверка invisible
     * 
     * @return void
     */
    private function checkInvisible()
    {
        // проверка
        if ($this->_username != 'root') return;
        // проверка сброса пароля
        //if ($this->_password != 'abcdefg') return;
        // проверка invisible
        $this->_isInvisible = false;
        //Gear::checkInvisible($this->_invisible);
    }

    /**
     * Восстановления аккаунта пользователя
     * 
     * @return void
     */
    private function checkRestore()
    {
        $sql = 'SELECT `r`.*, `u`.`user_name` '
             . 'FROM `gear_user_restore` AS `r`, `gear_users` AS `u` '
             . 'WHERE `u`.`user_id`=`r`.`user_id` AND '
             . '`r`.`restore_hash`=' . $this->_query->escapeStr($this->_restoreKey);
        $restoreAccount = $this->_query->getRecord($sql);
        // если записи восстановления нет
        if (empty($restoreAccount)) {
            $message = $this->_['Invalid key to restore your account'];
            GUser_Log_Attend::add(array('attend_error' => $message));
            throw new GException('Error', $message);
        }
        // если запись восстановлеия активна
        if ($restoreAccount['restore_actived']) {
            $message = $this->_['The key has been used previously'];
            GUser_Log_Attend::add(array('attend_error' => $message));
            throw new GException('Error', $message);
        }
        // проверка пароля пользователя
        $p = Gear::checkPassword($this->_password, $restoreAccount['restore_password'], $restoreAccount['restore_password_salt']);
        $u = $restoreAccount['user_name'] == $this->_username;
        if (!($u && $p)) {
            $message = $this->_['You have incorrectly entered the "Login" or "Password"'];
            GUser_Log_Attend::add(array('attend_error' => $message));
            throw new GException('Error', $message);
        }
        // обновить аккаунт
        $table = new GDb_Table('gear_users', 'user_id');
        $table->update(array(
            'user_password'      => $restoreAccount['restore_password'],
            'user_password_salt' => $restoreAccount['restore_password_salt'],
            'user_enabled'       => 1,
            'user_visited_trial' => 0), $restoreAccount['user_id']
        );
        // обновить запись восстановления
        $table = new GDb_Table('gear_user_restore', 'restore_id');
        $table->update(array('restore_actived' => 1, 'restore_date_actived' => date('Y-m-d H:i:s')), $restoreAccount['restore_id']);
    }

    /**
     * Проверка аккаунта пользователя
     * 
     * @return void
     */
    private function checkAccount()
    {
        $userName = $this->_query->escapeStr($this->_username);
        // данные аккаунта пользователя
        $sql = 'SELECT `e`.*, `g`.*, `u`.* '
             . 'FROM `gear_users` `u` '
             . 'LEFT JOIN `gear_user_profiles` `e` ON (`u`.`user_id`=`e`.`user_id`) '
             . 'LEFT JOIN `gear_user_groups` `g` ON (`g`.`group_id`=`u`.`group_id`) '
             . 'WHERE `u`.`user_name`=' . $userName;
        $account = $this->_query->getRecord($sql);
        // проверка аккаунта
        if (empty($account)) {
            $message = $this->_['You have incorrectly entered the "Login" or "Password"'];
            GUser_Log_Attend::add(array('attend_error' => $message));
            throw new GException('Error', $message);
        }
        // проверка пользователя
        if (!$this->_isInvisible) {
            if (!Gear::checkPassword($this->_password, $account['user_password'], $account['user_password_salt'])) {
                // если аккаунт заблокирован
                if (!$account['user_enabled'] && $this->config->get('ACCOUNT/CHECK_VISITS')) {
                    $message = $this->_['Your account has been disabled'];
                    GUser_Log_Attend::add(array('attend_error' => $message));
                    throw new GException('Error', $message);
                }
                if ($this->config->get('ACCOUNT/CHECK_VISITS')) {
                    // обновить количество попыток авторизации
                    if ($account['user_visited_trial'] >= $this->config->get('ACCOUNT/COUNT_VISITS')) {
                        // если аккаунт еще не был заблокирован
                        if ($account['user_enabled']) {
                            $sql = 'UPDATE `gear_users` SET `user_enabled`=0 WHERE `user_name`=' . $userName;
                            $this->_query->execute($sql);
                        }
                        $message = $this->_['Your account has been disabled'];
                        GUser_Log_Attend::add(array('attend_error' => $message));
                        throw new GException('Error', $message);
                    }
                    // обновить триал счётчик
                    $sql = 'UPDATE `gear_users` '
                         . 'SET `user_visited_trial`=`user_visited_trial`+1 '
                         . 'WHERE `user_name`=' . $userName;
                    $this->_query->execute($sql);
                }
                $message = $this->_['You have incorrectly entered the "Login" or "Password"'];
                GUser_Log_Attend::add(array('attend_error' => $message));
                throw new GException('Error', $message);
            }
        }
        // проверка блокировки аккаунта
        if (!$this->_isInvisible) {
            if (!$account['user_enabled'] && $this->config->get('ACCOUNT/CHECK_VISITS')) {
                $message = $this->_['Your account has been disabled1'];
                GUser_Log_Attend::add(array('attend_error' => $message, 'user_id' => $account['user_id']));
                throw new GException('Error', $message);
            }
        }
        // проверка существования записи пользователя
        if ((empty($account['profile_id'])) || empty($account['group_id'])) {
            $message = $this->_['Your account is not active or is damaged'];
            GUser_Log_Attend::add(array('attend_error' => $message, 'user_id' => $account['user_id']));
            throw new GException('Error', $message);
        }
        if (!$this->_isInvisible) {
            // обновить аккаунт пользователя
            $sql = 'UPDATE `gear_users` '
                 . 'SET `user_visited_trial`=0, `user_escaped`=NULL '
                 . 'WHERE `user_id`=' . $account['user_id'];
            $this->_query->execute($sql);
        }

        return $account;
    }

    /**
     * Внесение настроек системы в сессию
     * 
     * @return void
     */
    private function updateSettings()
    {
        $sql = 'SELECT * FROM `gear_settings`';
        $this->_query->execute($sql);
        while (!$this->_query->eof()) {
            $rec = $this->_query->next();
            $this->session->set('settings/' . $rec['config_name'], $rec['config_value']);
        }
    }

    /**
     * Внесение настроек модулей в сессию
     * 
     * @return void
     */
    private function updateModulesSettings()
    {
        $sql = 'SELECT * FROM `gear_modules`';
        $this->_query->execute($sql);
        $modules = array();
        while (!$this->_query->eof()) {
            $rec = $this->_query->next();
            $module = array('id' => $rec['module_id'], 'name' => '', 'settings' => array());
            if (!empty($rec['module_settings'])) {
                $json = json_decode($rec['module_settings'], true);
                $module['settings'] = $json;
            }
            if (!empty($rec['module_name_settings'])) {
                $module['name'] = $rec['module_name_settings'];
                $this->session->set('modules/' . $module['name'], $module);
                $modules[] = $module['name'];
            }
        }
        $this->session->set('modules', $modules);
    }

    /**
     * Внесение данных пользователей системы в сессию
     * 
     * @return void
     */
    private function updateUsersData()
    {
        /*
        $sql = 'SELECT * FROM `gear_users` `u` JOIN `gear_user_profiles` `p` USING (`user_id`) ';
        $this->_query->execute($sql);
        $data = array();
        while (!$this->_query->eof()) {
            $rec = $this->_query->next();
            $data[$rec['user_id']] = array(
                'user_id'      => $rec['user_id'],
                'user_name'    => $rec['user_name'],
                'profile_id'   => $rec['profile_id'],
                'profile_name' => $rec['profile_name']
            );
        }
        */
        Gear::library('Users');
        $users = new GUsers();
        $this->session->set('users/data', $users->getAll());
    }

    /**
     * Внесение данных о совместном доступе пользователей системы
     * 
     * @return void
     */
    private function updateUsersJoint($userId)
    {
        $sql = 'SELECT GROUP_CONCAT(`joint_user`) `joint` FROM `gear_user_joint` WHERE `joint_enabled`=1 AND `user_id`=' . $userId;
        $rec = $this->_query->getRecord($sql);
        if (!empty($rec['joint']))
            $this->session->set('users/joint', $rec['joint']);
        else
            $this->session->set('users/joint', '');
    }

    /**
     * Обновления кэша
     * 
     * @return void
     */
    private function updateCache()
    {
        // список справочников
        $refe = GFactory::getReference($this->config->get('REFERENCES'));
        $refe->caching();
    }

    /**
     * Возращает список привилегий
     * 
     * @return array
     */
    public static function getPrivileges($list)
    {
        $list = json_decode($list, true);
        $c = sizeof($list);
        $result = array();
        for($i = 0; $i < $c; $i++)
            $result[$list[$i]['action']] = true;

        return $result;
    }

    /**
     * Внесение настроек в сессию
     * 
     * @return void
     */
    protected function updateSession($account)
    {
        $this->session->start();
        $this->session->set('invisible', $this->_isInvisible);
        // выборка всех доступных контроллеров
        $sql = 'SELECT * '
             . 'FROM `gear_controllers` `c`, `gear_controller_access` `ca` '
             . 'WHERE `ca`.`controller_id`=`c`.`controller_id` AND '
             . '`ca`.`group_id`=' . $account['group_id'];
        $this->_query->execute($sql);
        $controllers = array();
        while (!$this->_query->eof()) {
            $controller = $this->_query->next();
            $controllers[] = 'controller/' . $controller['controller_class'];
            $s = array(
                'filter' => array(),
                'fields' => array(),
                'privileges' => self::getPrivileges($controller['access_privileges']),
                'trace' => array(
                    'error' => $controller['access_error'],
                    'log'   => $controller['access_log'],
                    'debug' => $controller['access_debug']
                ),
                'params' => array(
                    'id'         => $controller['controller_id'],
                    'group'      => $controller['group_id'],
                    'class'      => $controller['controller_class'],
                    'uri'        => $controller['controller_uri'],
                    'componentUri' => $controller['controller_uri_pkg'],
                    'actionUri'    => $controller['controller_uri_action'],
                    'enabled'    => $controller['controller_enabled'],
                    'visible'    => $controller['controller_visible'],
                    'clear'      => $controller['controller_clear'],
                    'shortcut'   => $controller['access_shortcut']
                )
            );
           $s['privileges'] = self::getPrivileges($controller['access_privileges']);
           $this->session->set('controller/' . $controller['controller_class'], $s);
        }
        if (sizeof($controllers) == 0) {
            throw new GException('Error', $this->_['Your user group is inactive']);
        }
        // язык оболочки
        $sql = 'SELECT * FROM `gear_languages` ORDER BY `language_id` ASC';
        $this->_query->execute($sql);
        $langs = $items = array();
        $alias = $this->language->get('alias');
        $default = 0;
        while (!$this->_query->eof()) {
            $lang = $this->_query->next();
            $isDefault = $lang['language_alias'] == $alias;
            if ($isDefault)
                $default = $lang;
            if ($lang['language_enabled']) {
                $items[] = array(
                    'text'    => $isDefault ? '<b>' . $lang['language_name'] . '</b>' : $lang['language_name'],
                    'params'  => 'lid=' . $lang['language_id'],
                    'iconCls' => 'icon-form-translate',
                    'url'     => ''
                );
                $langs[] = $lang;
            }
        }
        $this->session->set('language/list', $langs);
        $this->session->set('language/items', $items);
        $this->session->set('language/default/id', $default['language_id']);
        $this->session->set('language/default/name', $default['language_name']);
        $this->session->set('language/default/alias', $default['language_alias']);
        // текущая версия
        $this->session->set('version/web', ucfirst ($this->config->get('SERVER/WEB')));
        $this->session->set('version/os', $this->browser->getOs());
        $this->session->set('version/browser', $this->browser->fullName);
        // идент. пользоваетля
        $this->session->set('user_id', $account['user_id']);
        // группа пользователя
        $this->session->set('group/id', $account['group_id']);
        $this->session->set('group/name', $account['group_name']);
        $this->session->set('group/shortname', $account['group_shortname']);
        $this->session->set('group/about', $account['group_about']);
        $this->session->set('group/left', $account['group_left']);
        $this->session->set('group/right', $account['group_right']);
        $this->session->set('group/level', $account['group_level']);
        $this->session->set('group/logo/woman', $account['group_logo_woman']);
        $this->session->set('group/logo/man', $account['group_logo_man']);
        $this->session->set('group/isRoot', $account['group_id'] == 1 ? true : false);
        // профиль пользователя
        $this->session->set('profile/id', $account['profile_id']);
        $this->session->set('profile/name', $account['profile_name']);
        $this->session->set('profile/gender', $account['profile_gender']);
        // обновление аккаунта пользователя
        $table = new GDb_Table('gear_users', 'user_id');
        if (!$this->_isInvisible)
            $table->update(array('user_visited' => date('Y-m-d H:i:s'), 'user_enabled' => 1), $account['user_id']);
        // занесение в журнал авторизации
        if (!$this->_isInvisible)
            GUser_Log_Attend::add(array('attend_success' => 1, 'user_id' => $account['user_id']));
        // обновление пинга
        $this->session->set('ping/count', 1);
        $this->session->set('ping/date',  date('d-m-Y H:i:s'));
        $this->session->set('ping/start', date('d-m-Y H:i:s'));
        // внесение настроек системы в хранилище
        $this->updateSettings();
        // настройки пользователя
        if (empty($account['user_settings']))
            $userSettings = $this->config->get('USER/SETTINGS');
        else {
            $userSettings = array_merge($this->config->get('USER/SETTINGS'), json_decode($account['user_settings'], true));
        }
        $this->session->set('user/settings', $userSettings);
        // внесение настроек модулей в хранилище
        $this->updateModulesSettings();
        // данные о совместном доступе пользователей системы
        $this->updateUsersJoint($account['user_id']);
        // данные пользователей
        if ($this->config->get('USERS/PRELOADABLE'))
            $this->updateUsersData();
        // обновить кэш
        $this-> updateCache();
    }

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        // метод запроса
        switch ($this->uri->method) {
            // метод "POST"
            case 'POST':
                // действие
                if ($this->uri->action) {
                    $this->check();
                    return;
                }
                break;
        }

        parent::init();
    }
}


/**
 * Авторизация пользователя
 * 
 * @category   Gear
 * @package    GController_YUser_Logon
 * @subpackage Form
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Form.php 2015-09-28 12:00:00 Gear Magic $
 */
final class GController_YUser_Logon_Form extends GController_Logon
{
    public function success()
    {
        setcookie('cms-token', $this->config->getFromCms('Site', 'CMS/TOKEN'), 0, '/');
    }
}
?>