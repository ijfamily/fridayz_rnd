<?php
/**
 * Gear Manager
 *
 * Контроллер         "Воспроизведение звука"
 * Пакет контроллеров "Пользователь"
 * Группа пакетов     "Система"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Sound
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Play.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Воспроизведение звука
 * 
 * @category   Gear
 * @package    Sound
 * @subpackage Play
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Play.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YUser_Sound_Play extends GController
{
    /**
     * Путь к аудио файлам
     *
     * @var string
     */
    protected $_alias = array('welcome' => 'welcome', 'mail' => 'mail', 'error' => 'todo', 'todo' => 'todo');

    /**
     * Вывод аудио тегов по запросу
     * 
     * @return void
     */
    protected function callAudio()
    {
        $action = $this->uri->getVar('action');
        if (isset($this->_alias[$action]))
            $this->outputTags($this->resourcePath . $this->_alias[$action]);
    }

    /**
     * Вывод тегов в frame
     * 
     * @param  string $name название файла
     * @return void
     */
    protected function outputTags($name)
    {
        print '<audio autoplay>';
        print '<source src="/' . PATH_APPLICATION . '/' . $name . '.ogg">';
        print '<source src="/' . $name . '.mp3">';
        print '</audio>';
    }

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        
        // метод запроса
        switch ($this->uri->method) {
            // метод "GET"
            case 'GET':
                $userSettings = $this->session->get('user/settings');
                if ($userSettings['sound'])
                    $this->callAudio();
                exit;
        }

        parent::init();
    }
}
?>