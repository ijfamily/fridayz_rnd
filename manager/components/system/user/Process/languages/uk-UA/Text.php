<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    'msg_mail'    => 'У Вас є %s <img src="resources/images/icons/plugins/icon_mail.png" align="absmiddle"> нове повідомлення!',
    'msg_mails'   => 'У Вас є %s <img src="resources/images/icons/plugins/icon_mail.png" align="absmiddle"> нових повідомлень!',
    'msg_message' => 'У Вас сообщение от: %s'
);
?>