<?php
/**
 * Gear Manager
 *
 * Контроллер         "Обработка состояния пользователя"
 * Пакет контроллеров "Пользователь"
 * Группа пакетов     "Процес"
 * Модуль             "Система"
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Process
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: State.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Process');

/**
 * Обработка состояния пользователя
 * 
 * @category   Gear
 * @package    Process
 * @subpackage State
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: State.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YUser_Process_State extends GController_Process
{
    /**
     * Проверить письма пользователям
     * 
     * @return void
     */
    protected function checkUserMails($query)
    {
        $sql = 'SELECT COUNT(*)  `total` FROM `gear_user_mails` '
             . 'WHERE `receiver_id`=' . $this->session->get('user_id') . ' AND `mail_read`=0';
        $mails = $query->getRecord($sql);
        if ($mails)
            if ($mails['total']) {
                $this->response->add('type', self::TYPE_MAIL);
                if ($mails['total'] > 1)
                    $this->_response->message = sprintf($this->_['msg_mails'], $mails['total']);
                if ($mails['total'] == 1)
                    $this->response->message = sprintf($this->_['msg_mail'], $mails['total']);
                $this->response->add('sound', true);
                $this->response->add('action', 'mail');
                $this->response->add('tooltip', 'Сообщение');
                $this->response->add('url', 'administration/mails/received/' . ROUTER_DELIMITER . 'grid/interface/');
            }
    }

    /**
     * Проверить сообщения пользователя
     * 
     * @return void
     */
    protected function checkUserMsg($query)
    {
        $sql = 'SELECT `m`.*, `p`.`profile_name` FROM `gear_user_messages` `m` '
             . 'LEFT JOIN `gear_user_profiles` `p` ON `m`.`sender_id`=`p`.`user_id` '
             . 'WHERE `m`.`receiver_id`=' . $this->session->get('user_id') . ' AND `m`.`msg_read`=0';
        $msg = $query->getRecord($sql);
        if (!empty($msg)) {
            $this->response->add('type', self::TYPE_MESSAGE);
            $this->response->add('sound', true);
            $this->response->add('action', 'message');
            $this->response->add('tooltip', sprintf($this->_['msg_message'], $msg['profile_name']));
            $this->response->add('url', 'administration/messages/' . ROUTER_DELIMITER . 'grid/interface/');
            $this->response->add('msg', $msg['msg_text']);
            // отметить о доставке сообщения
            $sql = 'UPDATE `gear_user_messages` '
                 . "SET `msg_read`=1, `msg_receive_date`='" . date('Y-m-d') . "', "
                 . "`msg_receive_time`='" . date('H:i:s') . "' "
                 . 'WHERE `msg_id`=' . $msg['msg_id'];
            $query->execute($sql);
        }
    }

    /**
     * Обновить состояние пользователя
     * 
     * @return void
     */
    protected function updateUserState()
    {
        $params = array('user_process' => date('Y-m-d H:i:s'));
        $table = new GDb_Table('gear_users', 'user_id', array('user_process'));
        $table->update($params, $this->session->get('user_id'));
    }

    /**
     * Обновить данные процесса
     * 
     * @return void
     */
    protected function updateState()
    {
        // соединение с базой данных
        GFactory::getDb()->connect();
        $query = new GDb_Query();
        // настройки пользователя
        $userSettings = $this->session->get('user/settings');

        // обновить состояние пользователя
        if (!$this->session->get('invisible'))
            $this->updateUserState();
        // проверить почту пользователя
        if ($userSettings['receive/mails'])
            $this->checkUserMails($query);
        // проверить сообщения пользователя
        if ($userSettings['receive/messages']) 
            $this->checkUserMsg($query);
    }

    /**
     * Обновить пинг
     * 
     * @return void
     */
    protected function updatePing()
    {
        $this->session->set('ping/count', $this->session->get('ping/count') + 1);
        $this->session->set('ping/date', date('d-m-Y H:i:s'));
    }

    /**
     * Действие на запрос
     * 
     * @param  string $action действие
     * @return void
     */
    protected function callProcess($action)
    {
        $this->updatePing();

        // действие
        switch ($action) {
            // состояние
            case 'state': $this->updateState(); return;
        }
    }
}
?>