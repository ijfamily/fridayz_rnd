<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные пинга сервера"
 * Пакет контроллеров "Пользователь"
 * Группа пакетов     "Система"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YUser_Ping
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные пинга сервера
 * 
 * @category   Gear
 * @package    GController_YUser_Ping
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YUser_Ping_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array('ping_date', 'ping_count');

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record array fields
     * @return string
     */
    protected function getTitle($record)
    {
        return $this->_['profile_title_info'];
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        Gear::library('Date');

        $date = '';
        $ps = strtotime($this->session->get('ping/start'));
        $pe = strtotime($this->session->get('ping/date'));
        if (($h = GDate::DateDiff('hh', $ps, $pe)) > 0)
            $date = $h . ':';
        $date .= GDate::DateDiff('mm', $ps, $pe) . ':';
        $date .= GDate::DateDiff('s', $ps, $pe);

        $this->response->data = array(
            'ping_start'  => $this->session->get('ping/start'),
            'ping_date'   => $this->session->get('ping/date'),
            'ping_count'  => $this->session->get('ping/count'),
            'ping_server' => $_SERVER['SERVER_ADDR'],
            'ping_inline' => $date,
            'ping_step'   => 30
        );
    }
}
?>