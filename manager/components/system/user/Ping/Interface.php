<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс пинга сервера"
 * Пакет контроллеров "Пользователь"
 * Группа пакетов     "Система"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YUser_Ping
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс пинга сервера
 * 
 * @category   Gear
 * @package    GController_YUser_Ping
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YUser_Ping_Interface extends GController_Profile_Interface
{
    /**
     * Initialize component interface
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_info'],
                  'titleEllipsis' => 40,
                  'width'         => 275,
                  'height'        => 228,
                  'resizable'     => false,
                  'stateful'      => false,
                  'iconSrc'       =>  $this->resourcePath . 'icon-form.png',
                  'state'         => 'info')
        );

        // поля формы (Ext.form.FormPanel)
        $items = array(
           array('xtype'      => 'textfield',
                 'fieldLabel' => $this->_['label_ping_server'],
                 'name'       => 'ping_server',
                 'width'      => 130,
                 'readOnly'   => true,
                 'allowBlank' => true),
           array('xtype'      => 'textfield',
                 'fieldLabel' => $this->_['label_ping_start'],
                 'name'       => 'ping_start',
                 'width'      => 130,
                 'readOnly'   => true,
                 'allowBlank' => true),
           array('xtype'      => 'textfield',
                 'fieldLabel' => $this->_['label_ping_date'],
                 'name'       => 'ping_date',
                 'width'      => 130,
                 'readOnly'   => true,
                 'allowBlank' => true),
           array('xtype'      => 'textfield',
                 'fieldLabel' => $this->_['label_ping_count'],
                 'name'       => 'ping_count',
                 'width'      => 80,
                 'readOnly'   => true,
                 'allowBlank' => true),
           array('xtype'      => 'textfield',
                 'fieldLabel' => $this->_['label_ping_step'],
                 'name'       => 'ping_step',
                 'width'      => 80,
                 'readOnly'   => true,
                 'allowBlank' => true),
           array('xtype'      => 'textfield',
                 'fieldLabel' => $this->_['label_ping_inline'],
                 'name'       => 'ping_inline',
                 'width'      => 130,
                 'readOnly'   => true,
                 'allowBlank' => true));

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->labelWidth = 105;
        $form->items->addItems($items);
        $form->url = $this->componentUrl . 'ping/';

        parent::getInterface();
    }
}
?>