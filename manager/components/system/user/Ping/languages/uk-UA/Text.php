<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_info' => 'Пінг сервер',
    // поля
    'label_ping_server'  => 'IP сервера',
    'label_ping_start'   => 'Перший пінг',
    'label_ping_step'    => 'Пінг через (сек)',
    'label_ping_date'    => 'Останній пінг',
    'label_ping_count'   => 'Пінгів',
    'label_ping_inline'  => 'В мережі'
);
?>