<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_info' => 'Пинг сервера',
    // поля
    'label_ping_server'  => 'IP сервера',
    'label_ping_start'   => 'Первый пинг',
    'label_ping_step'    => 'Пинг через (сек)',
    'label_ping_date'    => 'Последний пинг',
    'label_ping_count'   => 'Пингов',
    'label_ping_inline'  => 'В сети'
);
?>