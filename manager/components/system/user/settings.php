<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Пользователь"
 * Группа пакетов     "Система"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YUser
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 */

set_error_handler('GException::errorHandler', 0);

// настройки компонента
return array(
    // {Y}- модуль "System" {User} - пакет контроллеров -> YUser
    'clsPrefix' => 'YUser'
);
?>