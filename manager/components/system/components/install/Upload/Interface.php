<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля установки компонента"
 * Пакет контроллеров "Установка компонентов"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YInstall_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2014-08-04 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля установки компонента
 * 
 * @category   Gear
 * @package    GController_YInstall_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YInstall_Upload_Interface extends GController_Profile_Interface
{
    /**
     * Возвращает интерфейс
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'          => $this->_['title_profile_insert'],
                  'titleEllipsis'  => 45,
                  'width'          => 420,
                  'autoHeight'     => true,
                  'resizable'      => false,
                  'stateful'       => false,
                  'btnInsertIcon'  => 'icon-fbtn-upload',
                  'btnInsertText'  => $this->_['text_btn_insert'],
                  'buttonsAdd'    => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-system-modules',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype'      => 'fieldset',
                  'labelWidth' => 80,
                  'title'      => $this->_['title_fieldset_upload'],
                  'autoHeight' => true,
                  'items'      => array(
                      array('xtype'     => 'displayfield',
                            'hideLabel' => true,
                            'value'     => $this->_['text_upload']),
                      array('xtype'      => 'mn-field-upload',
                            'emptyText'  => $this->_['text_empty_upload'],
                            'name'       => 'json',
                            'buttonText' => $this->_['text_btn_upload'],
                            'anchor'     => '100%',
                            'buttonCfg'  => array('width' => 70))
                  )
            )
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->addItems($items);
        $form->fileUpload = true;
        $form->autoScroll = true;
        $form->url = $this->componentUrl . 'upload/';
        $this->state = 'insert';

        parent::getInterface();
    }
}
?>