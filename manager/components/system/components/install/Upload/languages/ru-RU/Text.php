<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Languages
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Установка компонентов и модулей системы',
    'text_btn_help'        => 'Справка',
    // поля
    'text_upload'           => 'Для установки компонентов или модуля выберите файла для загрузки с раширением (.json)<br><br>',
    'text_empty_upload'     => 'Выберите файл ...',
    'text_btn_upload'       => 'Выбрать',
    'title_fieldset_upload' => 'Файл установки (.json)',
    'text_btn_insert'       => 'Загрузить',
    // сообщения
    'msg_status_install'  => 'Установка',
    'msg_success_install' => 'Данные успешно обработаны'
);
?>