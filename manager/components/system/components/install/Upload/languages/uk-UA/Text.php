<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Languages
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Установка компонентів і модулів системи',
    // поля
    'text_upload'           => 'Для установки компонентів або модуля виберіть файлу для завантаження з розширенням (.json)<br><br>',
    'text_empty_upload'     => 'Виберіть файл ...',
    'text_btn_upload'       => 'Вибрати',
    'title_fieldset_upload' => 'Файл установки (.json)',
    'text_btn_insert'       => 'Завантажити',
    // сообщения
    'msg_status_install'  => 'Установка',
    'msg_success_install' => 'Дані успішно оброблені'
);
?>