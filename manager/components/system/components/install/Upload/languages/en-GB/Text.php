<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Languages
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Installation of components and modules',
    // поля
    'text_upload'           => 'To install the component or module, select the file to load with the extension (.json)<br><br>',
    'text_empty_upload'     => 'Select file ...',
    'text_btn_upload'       => 'Select',
    'title_fieldset_upload' => 'Setup file (.json)',
    'text_btn_insert'       => 'Upload',
    // сообщения
    'msg_status_install'  => 'Install',
    'msg_success_install' => 'Data processed successfully'
);
?>