<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля установки компонента"
 * Пакет контроллеров "Установка компонентов"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YInstall_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2014-08-04 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля установки компонента
 * 
 * @category   Gear
 * @package    GController_YInstall_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YInstall_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Если есть ошибка в интерфейсе
     * 
     * @var boolean
     */
    protected $_isError = false;

    /**
     * Кого необходимо установить (module, component, components)
     *
     * @var string
     */
    protected $_install = '';

    /**
     * Возвращает интерфейс групп компонентов
     * 
     * @return void
     */
    protected function getCmpGroupsInterface($data)
    {
        $groups = array();
        $alias = $this->language->get('alias');
        $langs = $this->language->getLanguages();

        foreach ($data as $id => $group) {
            $errors = array();

            // название группы
            $fsNames = array();
            $name = '';
            foreach ($group['name'] as $key => $value) {
                if ($alias == $key)
                    $name = $value;
                $isExist = $this->language->isExist($key);
                if (!$isExist) {
                    $warnings[] = sprintf($this->_['value_unknow_language'], $key);
                }
                $fsNames[] = array(
                    'xtype'      => 'displayfield',
                    'fieldClass' => $isExist ? 'mn-field-value-info' : 'mn-field-value-warning',
                    'fieldLabel' => isset($langs[$key]) ? $langs[$key]['name'] : $key,
                    'value'      => $value);
            }

            // загаловок группы
            $groups[] = array(
                'xtype' => 'mn-field-separator',
                'html'  => $this->_['label_install_cgroup'] . ($name ? ' "' . $name . '"' : '')
            );
            $groups[] = array(
               'xtype'      => 'fieldset',
               'labelWidth' => 150,
               'title'      => $this->_['label_cgroup_name'],
               'autoHeight' => true,
               'items'      => $fsNames
            );

            // описание группы
            $fsDescr = array();
            foreach ($group['description'] as $key => $value) {
                $isExist = $this->language->isExist($key);
                if (!$isExist) {
                    $warnings[] = sprintf($this->_['value_unknow_language'], $key);
                }
                $fsDescr[] = array(
                    'xtype'      => 'displayfield',
                    'fieldClass' => $isExist ? 'mn-field-value-info' : 'mn-field-value-warning',
                    'fieldLabel' => isset($langs[$key]) ? $langs[$key]['name'] : $key,
                    'value'      => $value
                );
            }
            $groups[] = array(
                'xtype'      => 'fieldset',
                'labelWidth' => 150,
                'title'      => $this->_['label_cgroup_about'],
                'autoHeight' => true,
                'items'      => $fsDescr
            );

            // модуль компонента
            // если новый модуль
            if ($group['module'] == 'new' && $this->_install == 'module') {
                $groups[] = array(
                    'xtype'      => 'displayfield',
                    'fieldClass' => 'mn-field-value-new',
                    'fieldLabel' => $this->_['label_module_name'],
                    'value'      => $this->_['value_module_new']
                );
            } else {
                $errors[] = $this->_['value_module_no_set'];
                $groups[] = array(
                    'xtype'      => 'displayfield',
                    'fieldClass' => 'mn-field-value-error',
                    'fieldLabel' => $this->_['label_module_name'],
                    'value'      => $this->_['value_module_no_set']
                );
            }

            // если есть ошибки обработки
            if (sizeof($errors) > 0)
                $this->_isError = true;
            if ($errors)
                $groups[] = array('xtype' => 'mn-field-separator', 'html' => $this->_['label_separator_error']);
            if ($errors) {
                for ($i = 0; $i < sizeof($errors); $i++)
                    $groups[] = array(
                        'xtype'      => 'displayfield',
                        'fieldClass' => 'mn-field-value-error',
                        'hideLabel'  => true,
                        'value'      => $errors[$i]
                    );
            }
            $groups[] = array('xtype'=> 'displayfield', 'hideLabel' => true, 'value' => '<br><br>');
        }

        return $groups;
    }

    /**
     * Возвращает интерфейс компонента
     * 
     * @return void
     */
    protected function getComponentInterface($data)
    {
        $errors = $warnings = $component = array();
        $alias = $this->language->get('alias');
        $langs = $this->language->getLanguages();

        // соединение с базой данных
        GFactory::getDb()->connect();
        $query = new GDb_Query();
        // загаловок
        $component[] = array(
            'xtype' => 'mn-field-separator',
            'html'  => $this->_['label_install_component']
        );

        // значок компонента
        $imgSrc = PATH_COMPONENT . $data['resource']['uri'] . 'resources/shortcut.png';
        if (!file_exists($imgSrc))
            $imgSrc = $this->resourcePath . 'shortcut-none.png';
        $component[] = array(
            'xtype'     => 'displayfield',
            'hideLabel' => true,
            'value'     => '<img src="' . $imgSrc . '">'
        );

        // название компонента
        $fsNames = array();
        $name = '';
        foreach ($data['name'] as $key => $value) {
            if ($alias == $key)
                $name = $value;
            $isExist = $this->language->isExist($key);
            if (!$isExist) {
                $warnings[] = sprintf($this->_['value_unknow_language'], $key);
            }
            $fsNames[] = array(
                'xtype'      => 'displayfield',
                'fieldClass' => $isExist ? 'mn-field-value-info' : 'mn-field-value-warning',
                'fieldLabel' => isset($langs[$key]) ? $langs[$key]['name'] : $key,
                'value'      => $value);
        }
        if ($name)
            $component[0]['html'] = $component[0]['html'] . ' "' . $name . '"';
        $component[] = array(
            'xtype'      => 'fieldset',
            'labelWidth' => 150,
            'title'      => $this->_['label_controller_name'],
            'autoHeight' => true,
            'items'      => $fsNames
         );

        // описание компонента
        $fsDescr = array();
        foreach ($data['description'] as $key => $value) {
            if ($alias == $key)
                $name = $value;
            $isExist = $this->language->isExist($key);
            if (!$isExist) {
                $warnings[] = sprintf($this->_['value_unknow_language'], $key);
            }
            $fsDescr[] = array(
                'xtype'      => 'displayfield',
                'fieldClass' => $isExist ? 'mn-field-value-info' : 'mn-field-value-warning',
                'fieldLabel' => isset($langs[$key]) ? $langs[$key]['name'] : $key,
                'value'      => $value
            );
        }
        $component[] = array(
            'xtype'      => 'fieldset',
            'labelWidth' => 150,
            'title'      => $this->_['label_controller_about'],
            'autoHeight' => true,
            'items'      => $fsDescr
        );

        // меню компонента
        if (isset($data['menu'])) {
            $fsMenu = array();
            foreach ($data['menu'] as $key => $value) {
                if ($alias == $key)
                    $name = $value;
                $isExist = $this->language->isExist($key);
                if (!$isExist) {
                    $warnings[] = sprintf($this->_['value_unknow_language'], $key);
                }
                $fsMenu[] = array(
                    'xtype'      => 'displayfield',
                    'fieldClass' => $isExist ? 'mn-field-value-info' : 'mn-field-value-warning',
                    'fieldLabel' => isset($langs[$key]) ? $langs[$key]['name'] : $key,
                    'value'      => $value
                );
            }
            $component[] = array(
                'xtype'      => 'fieldset',
                'labelWidth' => 150,
                'title'      => $this->_['label_controller_profile'],
                'autoHeight' => true,
                'items'      => $fsMenu
            );
        }

        // проверка существования компонента
        $sql = 'SELECT COUNT(*) `count` FROM `gear_controllers` WHERE `controller_class`=\'' . mysql_real_escape_string($data['class']) . '\'';
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        if (!empty($rec))
            if ($rec['count'] > 0)
                $errors[] = sprintf($this->_['value_controller_exist'], $data['class']);

        // класс компонента
        $component[] = array(
            'xtype'      => 'displayfield',
            'fieldClass' => 'mn-field-value-info',
            'fieldLabel' => $this->_['label_class_id'],
            'value'      => $data['class']
        );

        // модуль компонента
        // если новый модуль
        if ($data['module'] == 'new') {
            if ($this->_install == 'module')
                $component[] = array(
                    'xtype'      => 'displayfield',
                    'fieldClass' => 'mn-field-value-new',
                    'fieldLabel' => $this->_['label_module_name'],
                    'value'      => $this->_['value_module_new']
                );
            else {
                $errors[] = $this->_['value_module_no_set'];
                $component[] = array(
                    'xtype'      => 'displayfield',
                    'fieldClass' => 'mn-field-value-error',
                    'fieldLabel' => $this->_['label_module_name'],
                    'value'      => $this->_['value_module_no_set']
                );
            }
        } else {
            $sql = 'SELECT * FROM `gear_modules` WHERE `module_id`=' . $data['module'];
            if (($module = $query->getRecord($sql)) === false)
                throw new GSqlException();
            if (empty($module['module_name'])) {
                $errors[] = sprintf($this->_['value_unknow_value'], $this->_['label_module_name']);
                $component[] = array(
                    'xtype'      => 'displayfield',
                    'fieldClass' => 'mn-field-value-error',
                    'fieldLabel' => $this->_['label_module_name'],
                    'value'      => $this->_['value_unknow']
                );
            } else {
                $component[] = array(
                    'xtype'      => 'displayfield',
                    'fieldClass' => 'mn-field-value-info',
                    'fieldLabel' => $this->_['label_module_name'],
                    'value'      => '<a onclick="Manager.widget.load({url: ' 
                                  . "Manager.COMPONENTS_URL + 'system/components/modules/' + Ext.ROUTER_DELIMITER + 'profile/interface/" . $data['module'] . "?state=info'"
                                  . '});">' . $module['module_name'] . '</a>'
                );
            }
        }

        // группа компонента
        if ($this->_install != 'module') {
            $sql = 'SELECT * FROM `gear_controller_groups` `g` JOIN `gear_controller_groups_l` `l` USING(`group_id`) '
                 . 'WHERE `l`.`language_id`=' . $this->language->get('id') .' AND `g`.`group_id`=' . $data['group'];
            if (($group = $query->getRecord($sql)) === false)
                throw new GSqlException();
            if (empty($group['group_name'])) {
                $errors[] = sprintf($this->_['value_unknow_value'], $this->_['label_group_name']);
                $component[] = array(
                    'xtype'      => 'displayfield',
                    'fieldClass' => 'mn-field-value-error',
                    'fieldLabel' => $this->_['label_group_name'],
                    'value'      => $this->_['value_unknow']
                );
            } else {
                $component[] = array(
                    'xtype'      => 'displayfield',
                    'fieldClass' => 'mn-field-value-info',
                    'fieldLabel' => $this->_['label_group_name'],
                    'value'      => '<a onclick="Manager.widget.load({url: ' 
                                  . "Manager.COMPONENTS_URL + 'system/components/groups/' + Ext.ROUTER_DELIMITER + 'profile/interface/" . $data['group'] . "?state=info'"
                                  . '});">' . $group['group_name'] . '</a>'
                );
            }
        } else {
                $component[] = array(
                    'xtype'      => 'displayfield',
                    'fieldClass' => 'mn-field-value-new',
                    'fieldLabel' => $this->_['label_group_name'],
                    'value'      => $this->_['value_cgroup_new']
                );
        }

        // очистка компонента
        $component[] = array(
            'xtype'      => 'displayfield',
            'fieldClass' => 'mn-field-value-info',
            'fieldLabel' => $this->_['label_controller_clear'],
            'value'      => $this->_['data_boolean'][$data['clear']]
        );

        // статистика компонента
        $component[] = array(
            'xtype'      => 'displayfield',
            'fieldClass' => 'mn-field-value-info',
            'fieldLabel' => $this->_['label_controller_statistics'],
            'value'      => $this->_['data_boolean'][$data['statistics']]
        );

        // ресурс компонента
        $component[] =
            array('xtype'      => 'fieldset',
                  'labelWidth' => 80,
                  'title'      => $this->_['title_fieldset_resource'],
                  'autoHeight' => true,
                  'items'      => array(
                      array('xtype'      => 'displayfield',
                            'fieldClass' => 'mn-field-value-info',
                            'fieldLabel' => $this->_['label_controller_uri_pkg'],
                            'value'      => $data['resource']['package']),
                      array('xtype'      => 'displayfield',
                            'fieldClass' => 'mn-field-value-info',
                            'fieldLabel' => $this->_['label_controller_uri'],
                            'value'      => $data['resource']['uri']),
                      array('xtype'      => 'displayfield',
                            'fieldClass' => 'mn-field-value-info',
                            'fieldLabel' => $this->_['label_controller_uri_action'],
                            'value'      => $data['resource']['action']),
                      array('xtype'      => 'displayfield',
                            'fieldClass' => 'mn-field-value-info',
                            'fieldLabel' => $this->_['label_controller_uri_menu'],
                            'value'      => $data['resource']['menu']),
                  )
            );

        // интерфейс компонента
        $component[] =
            array('xtype'      => 'fieldset',
                  'labelWidth' => 100,
                  'title'      => $this->_['title_fieldset_interface'],
                  'autoHeight' => true,
                  'items'      => array(
                      array('xtype'      => 'displayfield',
                            'fieldClass' => 'mn-field-value-info',
                            'fieldLabel' => $this->_['label_controller_enabled'],
                            'value'      => $this->_['data_boolean'][(int) $data['interface']['enabled']]),
                      array('xtype'      => 'displayfield',
                            'fieldClass' => 'mn-field-value-info',
                            'fieldLabel' => $this->_['label_controller_visible'],
                            'value'      => $this->_['data_boolean'][(int) $data['interface']['visible']]),
                      array('xtype'      => 'displayfield',
                            'fieldClass' => 'mn-field-value-info',
                            'fieldLabel' => $this->_['label_controller_dashboard'],
                            'value'      => $this->_['data_boolean'][(int) $data['interface']['dashboard']]),
                      array('xtype'      => 'displayfield',
                            'fieldClass' => 'mn-field-value-info',
                            'fieldLabel' => $this->_['label_controller_menu'],
                            'value'      => $this->_['data_boolean'][(int) $data['interface']['menu']])
                  )
            );

        // привилигии доступа к компоненту
        if (!empty($data['privileges'])) {
                $component[] = array(
                    'xtype'      => 'fieldset',
                    'labelWidth' => 80,
                    'title'      => $this->_['title_fieldset_privileges'],
                    'autoHeight' => true,
                    'items'      => array(
                        array('xtype'      => 'displayfield',
                              'hideLabel'  => true,
                              'fieldClass' => 'mn-field-value-info',
                              'value'      => $data['privileges'])
                    )
                );
        }

        // поля компонента
        if (!empty($data['fields'])) {
            $fields = array();
            for ($i = 0; $i < sizeof($data['fields']); $i++) {
                $fields[] = $data['fields'][$i]['field'];
            }
            if ($fields)
                $component[] = array(
                    'xtype'      => 'fieldset',
                    'labelWidth' => 80,
                    'title'      => $this->_['title_fieldset_fields'],
                    'autoHeight' => true,
                    'items'      => array(
                        array('xtype'      => 'displayfield',
                              'hideLabel'  => true,
                              'fieldClass' => 'mn-field-value-info',
                              'value'      => implode(', ', $fields))
                    )
                );
        }

        // права доступа к группам пользователей
        if ($data['userGroups']) {
            $fsGroups = array();
            if ($data['userGroups'][0]['group'] == 'all')
                $fsGroups = array(
                    'xtype'      => 'displayfield',
                    'fieldClass' => 'mn-field-value-info',
                    'fieldLabel' => $this->_['label_ugroup_all'],
                    'value'      => $data['userGroups'][0]['privileges']
                );
            else {
                $groups = $groupId = array();
                for ($i = 0; $i < sizeof($data['userGroups']); $i++) {
                    $groupId[] = $data['userGroups'][$i]['group'];
                    $groups[$data['userGroups'][$i]['group']] = $data['userGroups'][$i];
                }
                $sql = 'SELECT * FROM `gear_user_groups` WHERE `group_id` IN (' . implode(',', $groupId) . ')';
                if ($query->execute($sql) === false)
                    throw new GSqlException();
                while (!$query->eof()) {
                    $group = $query->next();
                    $fsGroups[] = array(
                        'xtype'      => 'displayfield',
                        'fieldClass' => 'mn-field-value-info',
                        'fieldLabel' => $group['group_name'],
                        'value'      => isset($groups[$group['group_id']]) ? $groups[$group['group_id']]['privileges'] : ''
                    );
                }
                if ($query->getCountRecords() == 0) {
                    $errors[] = $this->_['value_unknow_user_groups'];
                    $fsGroups = array(
                        'xtype'      => 'displayfield',
                        'fieldClass' => 'mn-field-value-error',
                        'hideLabel'  => true,
                        'value'      => $this->_['value_unknow_user_groups']
                    );
                }
            }
            $component[] = array(
                'xtype'      => 'fieldset',
                'labelWidth' => 150,
                'title'      => $this->_['title_fieldset_ugroups'],
                'autoHeight' => true,
                'items'      => $fsGroups
            );
        }

        // проверка папки компонента на сервере
        if (!file_exists(PATH_COMPONENT . $data['resource']['uri'])) {
            $errors[] = sprintf($this->_['value_path_error'], $data['resource']['uri']);
        }
        // если есть ошибки обработки
        if (sizeof($errors) > 0)
            $this->_isError = true;
        if ($errors || $warnings)
            $component[] = array('xtype' => 'mn-field-separator', 'html' => $this->_['label_separator_error']);
        if ($errors) {
            for ($i = 0; $i < sizeof($errors); $i++)
                $component[] = array(
                    'xtype'      => 'displayfield',
                    'fieldClass' => 'mn-field-value-error',
                    'hideLabel'  => true,
                    'value'      => $errors[$i]
                );
        }
        if ($warnings) {
            for ($i = 0; $i < sizeof($warnings); $i++)
                $component[] = array(
                    'xtype'      => 'displayfield',
                    'fieldClass' => 'mn-field-value-warning',
                    'hideLabel'  => true,
                    'value'      => $warnings[$i]
                );
        }

        $component[] = array('xtype'=> 'displayfield', 'hideLabel'  => true, 'value' => '<br><br>');

        return $component;
    }

    /**
     * Возвращает интерфейс
     * 
     * @return void
     */
    public function getModuleInterface($data)
    {
        $errors = $module = array();
        $alias = $this->language->get('alias');
        $langs = $this->language->getLanguages();

        // соединение с базой данных
        GFactory::getDb()->connect();
        $query = new GDb_Query();
        // проверка существования модуля
        $sql = 'SELECT COUNT(*) `count` FROM `gear_modules` WHERE `module_name`=\'' . mysql_real_escape_string($data['name']) . '\'';
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        if (!empty($rec))
            if ($rec['count'] > 0)
                $errors[] = sprintf($this->_['value_module_exist'], $data['name']);

        // загаловок
        $module[] = array(
            'xtype' => 'mn-field-separator',
            'html'  => $this->_['label_install_module'] . ' "' . $data['name'] . '"'
        );

        // название модуля
        $module[] = array(
            'xtype'      => 'displayfield',
            'fieldClass' => 'mn-field-value-info',
            'fieldLabel' => $this->_['label_module_name'],
            'value'      => $data['name']
        );

        // название модуля (сокр)
        $module[] = array(
            'xtype'      => 'displayfield',
            'fieldClass' => 'mn-field-value-info',
            'fieldLabel' => $this->_['label_module_shortname'],
            'value'      => $data['shortname']
        );

        // описание
        $module[] = array(
            'xtype'      => 'displayfield',
            'fieldClass' => 'mn-field-value-info',
            'fieldLabel' => $this->_['label_module_description'],
            'value'      => $data['description']
        );

        // версия
        $module[] = array(
            'xtype'      => 'displayfield',
            'fieldClass' => 'mn-field-value-info',
            'fieldLabel' => $this->_['label_module_version'],
            'value'      => $data['version']
        );

        // дата создания
        $module[] = array(
            'xtype'      => 'displayfield',
            'fieldClass' => 'mn-field-value-info',
            'fieldLabel' => $this->_['label_module_date'],
            'value'      => $data['date']
        );

        // автор
        $module[] = array(
            'xtype'      => 'displayfield',
            'fieldClass' => 'mn-field-value-info',
            'fieldLabel' => $this->_['label_module_author'],
            'value'      => $data['author']
        );

        // если таблицы демонтажа модуля
        if ($data['tables'])
            $module[] = array(
                'xtype'      => 'fieldset',
                'labelWidth' => 150,
                'title'      => $this->_['label_module_tables'],
                'autoHeight' => true,
                'items'      => array(
                    'xtype'      => 'displayfield',
                    'hideLabel'  => true,
                    'value'      => $data['tables']
                )
            );

        // если есть ошибки обработки
        if (sizeof($errors) > 0)
            $this->_isError = true;
        if ($errors)
            $module[] = array('xtype' => 'mn-field-separator', 'html' => $this->_['label_separator_error']);
        if ($errors) {
            for ($i = 0; $i < sizeof($errors); $i++)
                $module[] = array(
                    'xtype'      => 'displayfield',
                    'fieldClass' => 'mn-field-value-error',
                    'hideLabel'  => true,
                    'value'      => $errors[$i]
                );
        }

        return $module;
    }

    /**
     * Возвращает интерфейс
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'          => $this->_['title_profile_insert'],
                  'titleEllipsis'  => 45,
                  'width'          => 520,
                  'height'         => 450,
                  'resizable'      => false,
                  'stateful'       => false,
                  'btnInsertIcon'  => 'icon-fbtn-setup',
                  'btnInsertText'  => $this->_['text_btn_insert'])
        );

        // вкладки
        $tabPanel = array(
            'xtype'             => 'tabpanel',
            'layoutOnTabChange' => true,
            'deferredRender'    => false,
            'activeTab'         => 0,
            'enableTabScroll'   => true,
            'anchor'            => '100%',
            'height'            => 370,
            'items'             => array()
        );
        // вкладка "Компоненты"
        $tabComponents = array(
            'title'       => $this->_['title_tab_components'],
            'layout'      => 'form',
            'labelWidth'  => 70,
            'baseCls'     => 'mn-form-tab-body',
            'bodyStyle'   => 'background: url("' . $this->resourcePath. 'tab-bg.png") no-repeat center center;',
            'autoScroll'  => true,
            'items'       => array()
        );
        // вкладка "Группы компонентов"
        $tabCmpGroups = array(
            'title'       => $this->_['title_tab_cgroups'],
            'layout'      => 'form',
            'labelWidth'  => 70,
            'baseCls'     => 'mn-form-tab-body',
            'bodyStyle'   => 'background: url("' . $this->resourcePath. 'tab-bg.png") no-repeat center center;',
            'autoScroll'  => true,
            'items'       => array()
        );
        // вкладка "Модуль"
        $tabModule = array(
            'title'       => $this->_['title_tab_module'],
            'layout'      => 'form',
            'labelWidth'  => 120,
            'baseCls'     => 'mn-form-tab-body',
            'bodyStyle'   => 'background: url("' . $this->resourcePath. 'tab-bg.png") no-repeat center center;',
            'autoScroll'  => true,
            'items'       => array()
        );
        // вкладка "Источник"
        $tabSource = array(
            'title'       => $this->_['title_tab_source'],
            'layout'      => 'anchor',
            'labelWidth'  => 120,
            'baseCls'     => 'mn-form-tab-body',
            'bodyStyle'   => 'background: url("' . $this->resourcePath. 'tab-bg.png") no-repeat center center;',
            'autoScroll'  => true,
            'items'       => array(
                array('xtype'  => 'textarea',
                      'anchor' => '100% 100%',
                      'value'  => '')
            )
        );

        $data = $this->session->get('install/array', 'gcontroller_yinstall_upload');
        $this->_install = $data['install'];
        switch ($this->_install) {
            // установка компонента
            case 'component':
                $tabComponents['items'] = $this->getComponentInterface($data['component']);
                $tabPanel['items'][]= $tabComponents;
                break;

            // установка модуля
            case 'module':
                $tabModule['items'] = $this->getModuleInterface($data['module']);
                $tabPanel['items'][]= $tabModule;
                if ($data['components']) {
                    $components = array();
                    for ($i = 0; $i < sizeof($data['components']); $i++) {
                        $tabComponents['items'][] = $this->getComponentInterface($data['components'][$i]);
                    }
                    $tabPanel['items'][]= $tabComponents;
                }
                // если указаны группы компонентов
                if (!empty($data['componentGroups'])) {
                    $tabCmpGroups['items'] = $this->getCmpGroupsInterface($data['componentGroups']);
                    $tabPanel['items'][]= $tabCmpGroups;
                }
                break;

            // установка компонентов
            case 'components':
                if ($data['components']) {
                    $components = array();
                    for ($i = 0; $i < sizeof($data['components']); $i++) {
                        $tabComponents['items'][] = $this->getComponentInterface($data['components'][$i]);
                    }
                    $tabPanel['items'][]= $tabComponents;
                }
                break;
        }
        $tabSource['items'][0]['value'] = $this->session->get('install/json');
        $tabPanel['items'][]= $tabSource;

        // если есть ошибки в установке
        if ($this->_isError)
            $tabPanel['height'] = 350;
        // поля формы
        $formItems = array($tabPanel);

        // если есть ошибки в установке
        if ($this->_isError) {
            $this->state = 'info';
            $formItems[] = array(
                'xtype'      => 'displayfield',
                'fieldClass' => 'mn-field-value-warning',
                'style'      => 'margin-top:6px;',
                'hideLabel'  => true,
                'value'      => $this->_['value_install_error']
            );
        } else
            $this->state = 'insert';

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->autoScroll = false;
        $form->url = $this->componentUrl . 'profile/';
        $form->items->addItems($formItems);

        parent::getInterface();
    }
}
?>