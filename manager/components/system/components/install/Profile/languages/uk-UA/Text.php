<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Установка компонентов и модулей системы',
    'text_btn_insert'      => 'Установить',
    // поля
    'label_install_component'     => 'Установка компонента',
    'label_install_module'        => 'Установка модуля',
    'label_install_cgroup'        => 'Установка группы компонентов',
    'label_module_shortname'      => 'Название (сокр)',
    'label_module_description'    => 'Описание',
    'label_module_version'        => 'Версия',
    'label_module_date'           => 'Дата',
    'label_module_author'         => 'Автор',
    'label_module_tables'         => 'Таблицы базы данных для демонтажа модуля',
    'label_module_name'           => 'Модуль',
    'label_controller_name'       => 'Название',
    'label_controller_about'      => 'Описание',
    'label_controller_profile'    => 'Меню',
    'label_class_id'              => 'ID класса',
    'label_group_name'            => 'Группа',
    'label_controller_clear'      => 'Очищать',
    'label_controller_statistics' => 'Статистика',
    'label_controller_uri_pkg'    => 'Компонент',
    'label_controller_uri'        => 'Контроллер',
    'label_controller_uri_action' => 'Интерфейс',
    'label_controller_uri_menu'   => 'Меню',
    'label_controller_class'      => 'ID класса',
    'label_controller_iconsh'     => 'Значок',
    'label_controller_enabled'    => 'Доступный',
    'label_controller_visible'    => 'Видимый',
    'label_controller_dashboard'  => 'На доске',
    'label_controller_menu'       => 'В главном меню',
    'label_separator_error'       => 'Ошибки установки',
    'label_cgroup_name'           => 'Название',
    'label_cgroup_about'          => 'Описание',
    'label_ugroup_all'            => 'Для всех групп',
    'title_fieldset_privileges'   => 'Привилегии доступа',
    'title_fieldset_fields'       => 'Поля компонента',
    'title_fieldset_ugroups'      => 'Права доступа для групп пользователей',
    'title_fieldset_interface'    => 'Интерфейс',
    'title_fieldset_resource'     => 'Ресурс',
    'title_tab_components'        => 'Компоненты',
    'title_tab_module'            => 'Модуль',
    'title_tab_source'            => 'Источник',
    'title_tab_cgroups'           => 'Группы компонентов',
    'value_controller_exist'      => 'компонент <b>"%s"</b> уже существует',
    'value_path_error'            => 'каталог компонента <b>"%s"</b> на сервере не существует',
    'value_module_exist'          => 'модуль <b>"%s"</b> уже существует',
    'value_module_new'            => 'новый модуль',
    'value_module_no_set'         => 'не указан модуль',
    'value_unknow'                => 'неизвестно',
    'value_unknow_value'          => 'неизвестное значение <b>"%s"</b>',
    'value_unknow_language'       => 'неизвестный язык <b>"%s"</b>',
    'value_unknow_user_groups'    => 'неправильно указаны группы пользователей',
    'value_install_error'         => 'Невозможно выполнить установку, возникли ошибки',
    'value_cgroup_new'            => 'новая группа компонентов',
    // сообщения
    'msg_status_install'  => 'Установка',
    'msg_success_install' => 'Установка выполнена успешно',
    // типы
    'data_boolean' => array('нет', 'да')
);
?>