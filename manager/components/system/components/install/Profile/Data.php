<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля установщика компонента"
 * Пакет контроллеров "Установка компонентов"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YInstall_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2014-08-04 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные профиля установщика компонента
 * 
 * @category   Gear
 * @package    GController_YInstall_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YInstall_Profile_Data extends GController_Profile_Data
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_yinstall_upload';

    /**
     * Вставка данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataInsert($params = array())
    {
        parent::dataAccessInsert();

        $this->response->set('action', 'install');
        $this->response->setMsgResult($this->_['msg_status_install'], $this->_['msg_success_install'] , true);

        $this->install = GFactory::getClass('InstallComponents', 'Install');
        $this->install->execute($this->session->get('install/array'));

        $this->response->set('refreshShortcuts', true);
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {}
}
?>