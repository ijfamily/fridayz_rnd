<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Статистика компонентов"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YStatControllers
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Статистика компонентов
 * Описание: Статистика обработанных данных компонентов системы
 * ID класса: gcontroller_ystatcontrollers_grid
 * Группа: Система / Компоненты
 * Очищать: нет
 * Ресурс
 *    Пакет: system/components/statistics/
 *    Контроллер: system/components/statistics/Grid/
 *    Интерфейс: grid/interface/
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске: нет
 */

return array(
    // {Y}- модуль "System" {C} - пакет контроллеров "Statistics controllers" -> YStatControllers
    'clsPrefix' => 'YStatControllers'
);
?>