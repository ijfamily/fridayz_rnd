<?php
/**
 * Gear Manager
 *
 * Контроллер         "Развёрнутая запись статистики компонента"
 * Пакет контроллеров "Список статистики компонентов"
 * Группа пакетов     "Статистика компонентов"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YStatControllers_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Expand');

/**
 * Развёрнутая запись статистики компонента
 * 
 * @category   Gear
 * @package    GController_YStatControllers_Grid
 * @subpackage Expand
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YStatControllers_Grid_Expand extends GController_Grid_Expand
{
    /**
     * Тайтл записи
     * 
     * @var string
     */
    protected $_title = '';

    /**
     * Вывод данных в интерфейс
     * 
     * @return void
     */
    protected function dataExpand()
    {
        parent::dataAccessView();

        $data = $this->getAttributes();
        $this->response->data = $data;
    }

    /**
     * Возращает атрибуты записи
     * 
     * @return string
     */
    protected function getAttributes()
    {
        $data = '';
        $settings = $this->session->get('user/settings');
        // яызык по умолчанию
        $languageId = $this->session->get('language/default/id');
        $query = new GDb_Query();
        $sql = 'SELECT `c`.*,`cl` .*,`cg`.`group_name`,`cm`.`module_id`,`cm`.`module_name` FROM `gear_controllers` `c` '
             . 'LEFT JOIN `gear_controllers_l` `cl` USING(`controller_id`) '
               // группа компонентов
             . 'LEFT JOIN (SELECT `g`.*,`l`.`group_name`,`l`.`group_about` FROM `gear_controller_groups` `g` JOIN `gear_controller_groups_l` `l` USING(`group_id`) '
             . 'WHERE `l`.`language_id`=' . $this->language->get('id') . ') `cg` USING (`group_id`) '
               // модули
             . 'LEFT JOIN `gear_modules` `cm` ON `cm`.`module_id`=`c`.`module_id` '
             . 'WHERE `cl`.`language_id`=' . $languageId . ' AND `c`.`controller_id`=' . $this->uri->id;
        if (($cmp = $query->getRecord($sql)) === false)
            throw new GSqlException();
        $table = $cmp['controller_statistics_table'];
        if (empty($table))
            throw new GException('Warning', $this->_['msg_settings_empty']);
        $date = date('Y-m-d');
        $this->_title = '<img align="absmiddle" style="margin-left:5px;" src="' . PATH_COMPONENT . $cmp['controller_uri']. 'resources/icon.png">' . $cmp['controller_about'];

        // за весь период
        $data .= '<fieldset class="fixed"><label>' . $this->_['title_fieldset_all'] . '</label><ul>';
        $sql = "SELECT COUNT(*) `count` FROM `$table`";
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        $data .= '<li><label>' . $this->_['label_total_records'] . ':</label> ' . $rec['count'] . '</li>';
        $sql = "SELECT COUNT(*) `count` FROM `$table` WHERE `sys_user_update` IS NULL OR `sys_user_insert` IS NULL";
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        $data .= '<li><label>' . $this->_['label_unknow_users'] . ':</label> ' . $rec['count'] . '</li>';
        $data .= '</fieldset>';

        // за месяц
        $newDate = date('Y-m-d', mktime(0, 0, 0, date("m")-1, date("d"), date("Y")));
        $data .= '<fieldset class="fixed"><label>' . $this->_['title_fieldset_month'] . '</label><ul>';
        // добавленых записей:
        $sql = "SELECT COUNT(*) `count` FROM `$table` WHERE `sys_date_insert` BETWEEN '$newDate' AND '$date'";
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        $data .= '<li><label>' . $this->_['label_insert_records'] . ':</label> ' . '0' . '</li>';
        // измененных записей:
        $sql = "SELECT COUNT(*) `count` FROM `$table` WHERE `sys_date_update` BETWEEN '$newDate' AND '$date'";
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        $data .= '<li><label>' . $this->_['label_update_records'] . ':</label> ' . $rec['count'] . '</li>';
        // без учета пользователя:
        $sql = "SELECT COUNT(*) `count` FROM `$table` "
             . "WHERE ((`sys_date_update` BETWEEN '$newDate' AND '$date') OR (`sys_date_insert` BETWEEN '$newDate' AND '$date')) AND "
             . "(`sys_user_update` IS NULL OR `sys_user_insert` IS NULL)";
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        $data .= '<li><label>' . $this->_['label_unknow_users'] . ':</label> ' . $rec['count'] . '</li>';
        $data .= '</fieldset>';

        // за неделю
        $newDate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d") - 7, date("Y")));
        $data .= '<fieldset class="fixed"><label>' . $this->_['title_fieldset_week'] . '</label><ul>';
        // добавленых записей:
        $sql = "SELECT COUNT(*) `count` FROM `$table` WHERE `sys_date_insert` BETWEEN '$newDate' AND '$date'";
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        $data .= '<li><label>' . $this->_['label_insert_records'] . ':</label> ' . '0' . '</li>';
        // измененных записей:
        $sql = "SELECT COUNT(*) `count` FROM `$table` WHERE `sys_date_update` BETWEEN '$newDate' AND '$date'";
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        $data .= '<li><label>' . $this->_['label_update_records'] . ':</label> ' . $rec['count'] . '</li>';
        // без учета пользователя:
        $sql = "SELECT COUNT(*) `count` FROM `$table` "
             . "WHERE ((`sys_date_update` BETWEEN '$newDate' AND '$date') OR (`sys_date_insert` BETWEEN '$newDate' AND '$date')) AND "
             . "(`sys_user_update` IS NULL OR `sys_user_insert` IS NULL)";
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        $data .= '<li><label>' . $this->_['label_unknow_users'] . ':</label> ' . $rec['count'] . '</li>';
        $data .= '</fieldset>';

        // за день
        $data .= '<fieldset class="fixed"><label>' . $this->_['title_fieldset_day'] . '</label><ul>';
        // добавленых записей:
        $sql = "SELECT COUNT(*) `count` FROM `$table` WHERE `sys_date_insert`='$date'";
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        $data .= '<li><label>' . $this->_['label_insert_records'] . ':</label> ' . '0' . '</li>';
        // измененных записей:
        $sql = "SELECT COUNT(*) `count` FROM `$table` WHERE `sys_date_update`='$date'";
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        $data .= '<li><label>' . $this->_['label_update_records'] . ':</label> ' . $rec['count'] . '</li>';
        // без учета пользователя:
        $sql = "SELECT COUNT(*) `count` FROM `$table` "
             . "WHERE (`sys_date_update`='$date' OR `sys_date_insert`='$date') AND "
             . "(`sys_user_update` IS NULL OR `sys_user_insert` IS NULL)";
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        $data .= '<li><label>' . $this->_['label_unknow_users'] . ':</label> ' . $rec['count'] . '</li>';
        $data .= '</fieldset>';

        // за час
        $newDate = date('Y-m-d', mktime(date("H")-1, date("i"), date("s"), date("m"), date("d"), date("Y")));
        $data .= '<fieldset class="fixed"><label>' . $this->_['title_fieldset_hour'] . '</label><ul>';
        // добавленых записей:
        $sql = "SELECT COUNT(*) `count` FROM `$table` WHERE `sys_date_insert` BETWEEN '$newDate' AND '$date'";
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        $data .= '<li><label>' . $this->_['label_insert_records'] . ':</label> ' . '0' . '</li>';
        // измененных записей:
        $sql = "SELECT COUNT(*) `count` FROM `$table` WHERE `sys_date_update` BETWEEN '$newDate' AND '$date'";
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        $data .= '<li><label>' . $this->_['label_update_records'] . ':</label> ' . $rec['count'] . '</li>';
        // без учета пользователя:
        $sql = "SELECT COUNT(*) `count` FROM `$table` "
             . "WHERE (`sys_date_update` ='$date' OR `sys_date_insert`='$date') AND "
             . "(`sys_user_update` IS NULL OR `sys_user_insert` IS NULL)";
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        $data .= '<li><label>' . $this->_['label_unknow_users'] . ':</label> ' . $rec['count'] . '</li>';
        $data .= '</fieldset>';

        // последняя запись
        $sql = 'SELECT * FROM `' . $cmp['controller_statistics_table'] . '` ORDER BY `sys_date_insert` DESC,`sys_time_insert` DESC LIMIT 1';
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        $data .= '<fieldset class="fixed"><label>' . $this->_['title_fieldset_last'] . '</label><ul>';
            if (empty($rec['sys_date_update']))
                $value = '';
            else
                $value = date($settings['format/datetime'], strtotime($rec['sys_date_update'] . ' ' . $rec['sys_time_update']));
        $data .= '<li><label>' . $this->_['label_insert_record'] . ':</label> ' . $value . '</li>';
        $sql = 'SELECT * FROM `' . $cmp['controller_statistics_table'] . '` ORDER BY `sys_date_update` DESC,`sys_time_update` DESC LIMIT 1';
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
            if (empty($rec['sys_date_insert']))
                $value = '';
            else
                $value = date($settings['format/datetime'], strtotime($rec['sys_date_insert'] . ' ' . $rec['sys_time_insert']));
        $data .= '<li><label>' . $this->_['label_update_record'] . ':</label> ' . $value . '</li>';
        $data .= '</fieldset>';
        $data = '<div class="mn-row-body border">' . $data . '<div class="wrap"></div>';

        return $data;
    }
}
?>