<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Статистика компонентов системы',
    'tooltip_grid' => 'статистика обработанных данных компонентов',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    // столбцы
    'header_controller_name'  => 'Название',
    'header_group_name'       => 'Группа',
    'tooltip_group_name'      => 'Группа компонентов',
    'header_module_name'      => 'Модуль',
    'tooltip_module_name'     => 'Модуль системы',
    'header_controller_about' => 'Описание',
    // типы
    'data_boolean' => array('Нет', 'Да'),
    // развернутая запись
    'header_attr'          => 'Статистика обработанных данных компонента',
    'title_fieldset_all'   => 'За весь период',
    'label_total_records'  => 'Всего записей',
    'label_insert_records' => 'Добавлено записей',
    'label_insert_record'  => 'Добавлена запись',
    'label_update_records' => 'Изменено записей',
    'label_update_record'  => 'Изменена запись',
    'label_unknow_users'   => 'Без учета пользователя',
    'title_fieldset_month' => 'За месяц',
    'title_fieldset_week'  => 'За неделю',
    'title_fieldset_day'   => 'За день',
    'title_fieldset_hour'  => 'За последний час',
    'title_fieldset_last'  => 'Последняя запись',
    // сообщения
    'msg_settings_empty' => 'Невозможно выполнить статистику обработанных данных компонента (отсутствуют настройки компонента)'
);
?>