<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Статистика компонентів системи',
    'tooltip_grid' => 'статистика обработанных данных компонентов',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Стовпці',
    'title_buttongroup_filter' => 'Фільтр',
    // столбцы
    'header_controller_name'  => 'Назва',
    'header_group_name'       => 'Група',
    'tooltip_group_name'      => 'Група компонентів',
    'header_module_name'      => 'Модуль',
    'tooltip_module_name'     => 'Модуль системи',
    'header_controller_about' => 'Опис',
    // типы
    'data_boolean' => array('Ні', 'Так'),
    // развернутая запись
    'header_attr'          => 'Статистика оброблених даних компонента',
    'title_fieldset_all'   => 'За весь період',
    'label_total_records'  => 'Всього записів',
    'label_insert_records' => 'Додано записів',
    'label_insert_record'  => 'Додана запис',
    'label_update_records' => 'Змінено записів',
    'label_update_record'  => 'Змінена запис',
    'label_unknow_users'   => 'Без урахування користувача',
    'title_fieldset_month' => 'За місяць',
    'title_fieldset_week'  => 'За тиждень',
    'title_fieldset_day'   => 'За день',
    'title_fieldset_hour'  => 'За останню годину',
    'title_fieldset_last'  => 'Останній запис'
);
?>