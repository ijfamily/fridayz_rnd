<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка статистики компонентов"
 * Пакет контроллеров "Список статистики компонентов"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YStatControllers_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные списка статистики компонентов
 * 
 * @category   Gear
 * @package    GController_YStatControllers_Grid
 * @subpackage Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YStatControllers_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'controller_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_controllers';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS `c`.*,`cg`.`group_name`,`cm`.`module_name`, `cl`.`controller_name`, `cl`.`controller_about` '
          . 'FROM `gear_controllers` `c` '
          . 'JOIN `gear_controllers_l` `cl` USING(`controller_id`) '
            // группа компонентов
          . 'LEFT JOIN (SELECT `g`.*,`l`.`group_name`,`l`.`group_about` FROM `gear_controller_groups` `g` JOIN `gear_controller_groups_l` `l` USING(`group_id`) '
          . 'WHERE `l`.`language_id`=' . $this->language->get('id') . ') `cg` USING (`group_id`) '
            // модули
          . 'LEFT JOIN `gear_modules` `cm` ON `cm`.`module_id`=`c`.`module_id` '
          . 'WHERE `c`.`controller_statistics`=1 AND `cl`.`language_id`=' . $this->language->get('id') . ' %filter '
          . 'ORDER BY %sort '
          . 'LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            'controller_name' => array('type' => 'string'),
            'group_name' => array('type' => 'string'),
            'controller_path' => array('type' => 'string'),
            'controller_about' => array('type' => 'string'),
            'controller_uri' => array('type' => 'string'),
            'module_name' => array('type' => 'string')
        );
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        $record['controller_name'] = '<img align="absmiddle" style="margin-right:5px;" src="' . PATH_COMPONENT . $record['controller_uri']. 'resources/icon.png">' . $record['controller_name'];

        return $record;
    }
}
?>