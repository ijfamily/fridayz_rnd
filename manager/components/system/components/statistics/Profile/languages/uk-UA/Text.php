<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Создание записи "Компонент"',
    'title_profile_update' => 'Изменение записи "%s"',
    // поля
    'title_tab_fields'          => 'Поля',
    'title_tab_events'          => 'События',
    'title_tab_common'          => 'Атрибуты',
    'label_module_name'         => 'Модуль',
    'title_fieldset_path'       => 'Ресурс',
    'label_group_id'            => 'Группа',
    'label_controller_uri_pkg'  => 'Компонент',
    'tip_controller_uri_pkg'    => 'Путь к каталогу компонента (путь/.../каталог компонента/)',
    'label_controller_about'    => 'Описание',
    'label_controller_class'    => 'ID класса',
    'tip_controller_class'      => 'Идентификатор класса, который будут использовать все контроллеры компонента',
    'label_controller_name'     => 'Название',
    'label_controller_uri'      => 'Контроллер',
    'tip_controller_uri'        => 'Путь к каталогу контроллера (путь/.../каталог компонента/Каталог контроллера)',
    'label_controller_uri_action' => 'Интерфейс',
    'tip_controller_uri_action'   => 'Название контроллера / Интерфейс контроллера /',
    'label_controller_iconsh'   => 'Значок',
    'label_controller_enabled'  => 'Доступный',
    'label_controller_visible'  => 'Видимый',
    'label_controller_clear'    => 'Очищать',
    'title_fieldset_interface'  => 'Интерфейс',
    'empty_controller_name'     => 'Название компонента',
    'header_label'              => 'Название',
    'header_action'             => 'Событие',
    'header_field'              => 'Поле',
    'label_controller_dashboard' => 'На доске',
    'tip_controller_dashboard'   => 'Присутствие компонента на доске компонентов',
    'tip_controller_enabled'    => 'Доступность компонент из списка компонентов',
    'tip_controller_visible'    => 'Видимость компонента в списке компонентов',
    'tip_controller_clear'      => 'Выполнять очистку (удаление всех) данных компонента',
    'label_controller_statistics' => 'Статистика',
    'tip_controller_statistics'   => 'Данные компонента участвуют в статистике',
    'title_tab_source'            => 'Источник',
    // типы
    'data_array' => array(
         array('label' => 'Чтение', 'action' => 'select'),
         array('label' => 'Вставка', 'action' => 'insert'),
         array('label' => 'Правка', 'action' => 'update'),
         array('label' => 'Удаление', 'action' => 'delete'),
         array('label' => 'Очистка', 'action' => 'clear'),
         array('label' => 'Экспорт', 'action' => 'export'),
         array('label' => 'Полный доступ', 'action' => 'root')
    )
);
?>