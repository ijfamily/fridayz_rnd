<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля компонентов"
 * Пакет контроллеров "Список компонентов"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Controller_YControllers_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля компонентов
 * 
 * @category   Gear
 * @package    Controller_YControllers_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YControllers_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ycontrollers_grid';

    /**
     * Возвращает дополнительные данные для формирования интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        $data = array('group_name' => '', 'module_name' => '');
        if ($this->isInsert) return $data;

        // соединение с базой данных
        GFactory::getDb()->connect();
        $query = new GDb_Query();
        $sql = 'SELECT `group_name`, `m`.`module_name` '
             . 'FROM `gear_controller_groups` `cg`, `gear_controllers` `c` '
             . 'JOIN `gear_controller_groups_l` `l` USING(`group_id`) '
             . 'LEFT JOIN `gear_modules` `m` ON `m`.`module_id`=`c`.`module_id` '
             . 'WHERE `c`.`group_id`=`cg`.`group_id` AND `c`.`controller_id`=' . $this->uri->id . ' AND '
             . '`l`.`language_id`=' . $this->language->get('id');
        $record = $query->getRecord($sql);
        if ($record === false)
            throw new GSqlException();
        if (empty($record))
            return $data;

        return $record;
    }

    protected function getPriviliges($items)
    {
        $result = array();
        $text = GText::get('privileges', 'interface');
        $count = sizeof($items);
        for ($i = 0; $i < $count; $i++) {
            if (key_exists($items[$i], $text))
                $result[] = array('label' => $text[$items[$i]], 'action' => $items[$i]);
        }

        return $result;
    }

    /**
     * Возвращает XML компонента
     * 
     * @return string
     */
    protected function getXMLCode()
    {
        $query = new GDb_Query();
        // название и описание компонента
        $sql = 'SELECT * FROM `gear_controllers_l` `cl` '
             . 'JOIN `gear_languages` `l` USING(`language_id`) '
             . 'WHERE `l`.`language_enabled`=1 AND `cl`.`controller_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $xmlName = $xmlDesc = '';
        while (!$query->eof()) {
            $rec = $query->next();
            $xmlName .= "\t\t<" . $rec['language_alias'] . '>' . $rec['controller_name'] . '</' . $rec['language_alias'] .">\r\n";
            $xmlDesc .= "\t\t<" . $rec['language_alias'] . '>' . $rec['controller_about'] . '</' . $rec['language_alias'] .">\r\n";
        }
        // данные компонента
        $sql = 'SELECT * FROM `gear_controllers` WHERE `controller_id`=' . $this->uri->id;
        if (($cmp = $query->getRecord($sql)) === false)
            throw new GSqlException();
        if (empty($cmp))
            throw new GException('Data processing error', 'Query error (Internal Server Error)');
        // привилегии компонента
        $xmlPrivileges = '';
        if ($cmp['controller_privileges']) {
            $privileges = json_decode($cmp['controller_privileges'], true);
            for ($i = 0; $i < sizeof($privileges); $i++) {
                $xmlPrivileges .= "\t\t<item>\r\n";
                $xmlPrivileges .= "\t\t\t<label>" . $privileges[$i]['label'] . "</label>\r\n";
                $xmlPrivileges .= "\t\t\t<action>" . $privileges[$i]['action'] . "</action>\r\n";
                $xmlPrivileges .= "\t\t</item>\r\n";
            }
        }
        // данные компонента
        $xmlFields = '';
        if ($cmp['controller_fields']) {
            $fields = json_decode($cmp['controller_fields'], true);
            for ($i = 0; $i < sizeof($fields); $i++) {
                $xmlFields .= "\t\t<item>\r\n";
                $xmlFields .= "\t\t\t<label>" . $fields[$i]['label'] . "</label>\r\n";
                $xmlFields .= "\t\t\t<field>" . $fields[$i]['field'] . "</field>\r\n";
                $xmlFields .= "\t\t</item>\r\n";
            }
        }

        $xml = '<?xml version="1.0" encoding="UTF-8" ?>' . "\r\n";
        $xml .= "<component>\r\n";
        $xml .= "\t<name>\n" . $xmlName . "\t</name>\r\n";
        $xml .= "\t<description>\n" . $xmlDesc . "\t</description>\r\n";
        $xml .= "\t<class>" . $cmp['controller_class'] . "</class>\r\n";
        $xml .= "\t<module>" . $cmp['module_id'] . "</module>\r\n";
        $xml .= "\t<group>" . $cmp['group_id'] . "</group>\r\n";
        $xml .= "\t<clear>" . $cmp['controller_clear'] . "</clear>\r\n";
        $xml .= "\t<statistic>" . $cmp['controller_statistics'] . "</statistic>\r\n";

        $xml .= "\t<resource>\r\n";
        $xml .= "\t\t<package>" . $cmp['controller_uri_pkg'] . "</package>\r\n";
        $xml .= "\t\t<uri>" . $cmp['controller_uri'] . "</uri>\r\n";
        $xml .= "\t\t<action>" . $cmp['controller_uri_action'] . "</action>\r\n";
        $xml .= "\t</resource>\r\n";

        $xml .= "\t<interface>\r\n";
        $xml .= "\t\t<dashboard>" . $cmp['controller_dashboard'] . "</dashboard>\r\n";
        $xml .= "\t\t<enabled>" . $cmp['controller_enabled'] . "</enabled>\r\n";
        $xml .= "\t\t<visible>" . $cmp['controller_visible'] . "</visible>\r\n";
        $xml .= "\t</interface>\r\n";
        if ($xmlPrivileges)
            $xml .= "\t<privileges>\n" . $xmlPrivileges . "\t</privileges>\r\n";
        if ($xmlFields)
            $xml .= "\t<fields>\n" . $xmlFields . "\t</fields>\r\n";
        $xml .= '</component>';

        return $xml;
    }

    /**
     * Возвращает JSON код  компонента
     * 
     * @return string
     */
    protected function getJsonCode()
    {
        $query = new GDb_Query();
        // название и описание компонента
        $sql = 'SELECT * FROM `gear_controllers_l` `cl` '
             . 'JOIN `gear_languages` `l` USING(`language_id`) '
             . 'WHERE `l`.`language_enabled`=1 AND `cl`.`controller_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $xmlName = $xmlDesc = '';
        $count = $query->getCountRecords();
        $i = 1;
        while (!$query->eof()) {
            $rec = $query->next();
            $xmlName .= "\t\t\t\"" . $rec['language_alias'] . '" : "' . $rec['controller_name'];
            $xmlDesc .= "\t\t\t\"" . $rec['language_alias'] . '" : "' . $rec['controller_about'];
            if ($i < $count) {
                $xmlName .= "\",\r\n";
                $xmlDesc .= "\",\r\n";
            } else {
                $xmlName .= "\"\r\n";
                $xmlDesc .= "\"\r\n";
            }
            $i++;
        }
        // данные компонента
        $sql = 'SELECT * FROM `gear_controllers` WHERE `controller_id`=' . $this->uri->id;
        if (($cmp = $query->getRecord($sql)) === false)
            throw new GSqlException();
        if (empty($cmp))
            throw new GException('Data processing error', 'Query error (Internal Server Error)');
        // привилегии компонента
        $xmlPrivileges = '';
        if ($cmp['controller_privileges']) {
            $privileges = json_decode($cmp['controller_privileges'], true);
            $count = sizeof($privileges);
            for ($i = 0; $i < $count; $i++) {
                $xmlPrivileges .= "\t\t\t{ \"label\" : \"" . $privileges[$i]['label'] . "\", \"action\" : \"" . $privileges[$i]['action'];
                if ($i < $count - 1)
                    $xmlPrivileges .= "\" },\n";
                else
                    $xmlPrivileges .= "\" }\n";
            }
        }
        // данные компонента
        $xmlFields = '';
        if ($cmp['controller_fields']) {
            $fields = json_decode($cmp['controller_fields'], true);
            for ($i = 0; $i < sizeof($fields); $i++) {
                $xmlFields .= "\t\t\t{ \"label\" : \"" . $fields[$i]['label'] . "\", \"field\" : \"" . $fields[$i]['field'];
                if ($i < $count - 1)
                    $xmlFields .= "\" },\n";
                else
                    $xmlFields .= "\" }\n";
            }
        }

        $xml = "{\n";
        $xml .= "\t\"component\" : {\n";
        $xml .= "\t\t\"name\" : {\r" . $xmlName . "\t\t},\n";
        $xml .= "\t\t\"description\" : {\n" . $xmlDesc . "\t\t},\n";
        $xml .= "\t\t\"class\" : \"" . $cmp['controller_class'] . "\",\n";
        $xml .= "\t\t\"module\" : " . $cmp['module_id'] . ",\n";
        $xml .= "\t\t\"group\" : " . $cmp['group_id'] . ",\n";
        $xml .= "\t\t\"clear\" : " . $cmp['controller_clear'] . ",\n";
        $xml .= "\t\t\"statistic\" : " . $cmp['controller_statistics'] . ",\n";

        $xml .= "\t\t\"resource\" : {\n";
        $xml .= "\t\t\t\"package\" : \"" . $cmp['controller_uri_pkg'] . "\",\n";
        $xml .= "\t\t\t\"uri\" : \"" . $cmp['controller_uri'] . "\",\n";
        $xml .= "\t\t\t\"action\" : \"" . $cmp['controller_uri_action'] . "\"\n";
        $xml .= "\t\t},\n";

        $xml .= "\t\t\"interface\" : {\n";
        $xml .= "\t\t\t\"dashboard\" : " . $cmp['controller_dashboard'] . ",\n";
        $xml .= "\t\t\t\"enabled\" : " . $cmp['controller_enabled'] . ",\n";
        $xml .= "\t\t\t\"visible\" : " . $cmp['controller_visible'] . "\n";
        $xml .= "\t\t}";
        if ($xmlPrivileges)
            $xml .= ",\n\t\t\"privileges\" : [\n" . $xmlPrivileges . "\t\t]";
        if ($xmlFields)
            $xml .= ",\n\t\t\"fields\" : [\n" . $xmlFields . "\t\t]\n";
        $xml .= "\t}\n";
        $xml .= '}';

        return $xml;
    }

    /**
     * Возвращает интерфейс
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительные данные для формирования интерфейса
        $data = $this->getDataInterface();

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'titleEllipsis' => 70,
                  'gridId'        => 'gcontroller_ycontrollers_grid',
                  'width'         => 450,
                  'height'        => $this->isInsert ? 540 : 488,
                  'resizable'     => false,
                  'stateful'      => false)
        );

        // поля "ресурс"
        $fieldsetPath = array(
            'xtype'      => 'fieldset',
            'labelWidth' => 75,
            'title'      => $this->_['title_fieldset_path'],
            'autoHeight' => true,
            'defaults'   => array('anchor' => '100%', 'maxLength' => 255, 'allowBlank' => false),
            'items'      => array(
                array('xtype'      => 'textfield',
                      'fieldLabel' => $this->_['label_controller_uri_pkg'],
                      'labelTip'   => $this->_['tip_controller_uri_pkg'],
                      'name'       => 'controller_uri_pkg',
                      'emptyText'  => 'path/component folder/'),
                array('xtype'      => 'textfield',
                      'fieldLabel' => $this->_['label_controller_uri'],
                      'labelTip'   => $this->_['tip_controller_uri'],
                      'name'       => 'controller_uri',
                      'emptyText'  => 'path/component folder/controller folder/'),
                array('xtype'      => 'textfield',
                      'fieldLabel' => $this->_['label_controller_uri_action'],
                      'labelTip'   => $this->_['tip_controller_uri_action'],
                      'name'       => 'controller_uri_action',
                      'emptyText'  => 'controller/interface/')
            )
        );
        // поля "интерфейс"
        $fieldsetInt = array(
            'xtype'      => 'fieldset',
            'labelWidth' => 70,
            'title'      => $this->_['title_fieldset_interface'],
            'autoHeight' => true,
            'items'      => array(
                array('xtype'      => 'mn-field-chbox',
                      'fieldLabel' => $this->_['label_controller_enabled'],
                      'labelTip'   => $this->_['tip_controller_enabled'],
                      'name'       => 'controller_enabled',
                      'default'    => 1),
                array('xtype'      => 'mn-field-chbox',
                      'fieldLabel' => $this->_['label_controller_visible'],
                      'labelTip'   => $this->_['tip_controller_visible'],
                      'name'       => 'controller_visible',
                      'default'    => 1),
                array('xtype'      => 'mn-field-chbox',
                      'fieldLabel' => $this->_['label_controller_dashboard'],
                      'labelTip'   => $this->_['tip_controller_dashboard'],
                      'name'       => 'controller_dashboard',
                      'default'    => 1)
            )
        );
        $tabCommonItems = array(
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_controller_class'],
                  'labelTip'   => $this->_['tip_controller_class'],
                  'name'       => 'controller_class',
                  'emptyText'  => 'controller_xxx_xxx',
                  'anchor'     => '100%',
                  'maxLength'  => 50,
                  'allowBlank' => false),
            array('xtype'      => 'mn-field-combo',
                  'fieldLabel' => $this->_['label_module_name'],
                  'tpl'        => '<tpl for="."><div ext:qtip="{name}" class="x-combo-list-item">{data}</div></tpl>',
                  'value'      => $data['module_name'],
                  'name'       => 'module_id',
                  'editable'   => false,
                  'pageSize'   => 50,
                  'anchor'     => '100%',
                  'hiddenName' => 'module_id',
                  'allowBlank' => false,
                  'store'      => array(
                       'xtype' => 'jsonstore',
                       'url'   => $this->componentUrl . 'combo/trigger/?name=modules'
                  )
            ),
            array('xtype'      => 'mn-field-combo-tree',
                  'fieldLabel' => $this->_['label_group_id'],
                  'resetable'  => false,
                  'width'      => 240,
                  'name'       => 'group_id',
                  'hiddenName' => 'group_id',
                  'treeWidth'  => 300,
                  'treeRoot'   => array('id' => 'root', 'expanded' => false, 'expandable' => true),
                  'value'      => $data['group_name'],
                  'allowBlank' => true,
                  'store'      => array(
                      'xtype' => 'jsonstore',
                      'url'   => $this->componentUrl . 'combo/nodes/')
                 ),
            array('xtype'      => 'mn-field-chbox',
                  'fieldLabel' => $this->_['label_controller_clear'],
                  'labelTip'   => $this->_['tip_controller_clear'],
                  'name'       => 'controller_clear',
                  'default'    => 1),
            array('xtype'      => 'mn-field-chbox',
                  'fieldLabel' => $this->_['label_controller_statistics'],
                  'labelTip'   => $this->_['tip_controller_statistics'],
                  'name'       => 'controller_statistics',
                  'default'    => 1),
            $fieldsetPath,
            $fieldsetInt
        );
        // состояние профиля "правка"
        if ($this->isInsert) {
            array_unshift($tabCommonItems, 
                  array('xtype'      => 'textfield',
                        'fieldLabel' => $this->_['label_controller_name'],
                        'name'       => 'controller_name',
                        'emptyText'  => $this->_['empty_controller_name'],
                        'maxLength'  => 100,
                        'anchor'     => '100%',
                        'allowBlank' => false),
                  array('xtype'      => 'textfield',
                        'fieldLabel' => $this->_['label_controller_about'],
                        'name'       => 'controller_about',
                        'maxLength'  => 255,
                        'anchor'     => '100%',
                        'allowBlank' => false));
        }
        // вкладка "атрибуты"
        $tabCommon = array(
            'title'       => $this->_['title_tab_common'],
            'layout'      => 'form',
            'labelWidth'  => 75,
            'baseCls'     => 'mn-form-tab-body',
            'defaultType' => 'textfield',
            'items'       => array($tabCommonItems)
        );

        // вкладка "события"
        $tabEvents = array(
            'title'       => $this->_['title_tab_events'],
            'layout'      => 'form',
            'labelWidth'  => 75,
            'iconSrc'     => $this->resourcePath . 'icon-tab-events.png',
            'defaultType' => 'textfield',
            'items'       => array(
                array('xtype'     => 'mn-field-grid-editor',
                      'name'      => 'controller_privileges',
                      'hideLabel' => true,
                      'anchor'    => '100% 100%',
                      'fields'    => array('label', 'action'),
                      'value'     => $this->isUpdate ? '' : GPrivileges::getDefault(),
                      'columns'   => array(
                          array('header'    => $this->_['header_label'],
                                'dataIndex' => 'label',
                                'width'     => 150,
                                'sortable'  => true),
                          array('header'    => $this->_['header_action'],
                                'dataIndex' => 'action',
                                'width'     => 150,
                                'sortable'  => true)
                      )
                )
            )
        );

        // вкладка "поля"
        $tabFields = array(
            'title'       => $this->_['title_tab_fields'],
            'layout'      => 'form',
            'labelWidth'  => 75,
            'iconSrc'     => $this->resourcePath . 'icon-tab-fields.png',
            'defaultType' => 'textfield',
            'items'       => array(
                array('xtype'     => 'mn-field-grid-editor',
                      'name'      => 'controller_fields',
                      'hideLabel' => true,
                      'anchor'    => '100% 100%',
                      'fields'    => array('label', 'field'),
                      'columns'   => array(
                          array('header'    => $this->_['header_label'],
                                'dataIndex' => 'label',
                                'width'     => 150,
                                'sortable'  => true),
                          array('header'    => $this->_['header_field'],
                                'dataIndex' => 'field',
                                'width'     => 150,
                                'sortable'  => true)
                      )
                )
            )
        );

        // поля формы
        $items = array(
            'xtype'             => 'tabpanel',
            'layoutOnTabChange' => true,
            'deferredRender'    => false,
            'activeTab'         => 0,
            'enableTabScroll'   => true,
            'anchor'            => '100% 100%',
            'items'             => array($tabCommon, $tabEvents, $tabFields)
        );
        // если состояние формы "правка"
        if (!$this->isInsert) {
            // вкладка "источник"
            $items['items'][] = array(
                'title'    => $this->_['title_tab_source'],
                'layout'   => 'anchor',
                'iconSrc'  => $this->resourcePath . 'icon-tab-source.png',
                'items'    => array(
                    array('xtype'     => 'textarea',
                          'name'      => 'controller_source',
                          'hideLabel' => true,
                          'value'     => $this->getJsonCode(),
                          'anchor'    => '100% 100%')
                )
            );
        }

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->url = $this->componentUrl . 'profile/';
        $form->items->addItems($items);

        parent::getInterface();
    }
}
?>