<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля модуля системы"
 * Пакет контроллеров "Модули системы"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YCModules_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля модуля системы
 * 
 * @category   Gear
 * @package    GController_YCModules_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YCModules_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ycmodules_grid';

    /**
     * Возращает список компонентов модуля
     * 
     * @return array
     */
    protected function getComponents()
    {
        // соединение с базой данных
        GFactory::getDb()->connect();
        $query = new GDb_Query();
        $sql = 'SELECT `c`.*,`cl` .*,`cg`.`group_name` FROM `gear_controllers` `c` '
             . 'LEFT JOIN `gear_controllers_l` `cl` USING(`controller_id`) '
               // группа компонентов
             . 'LEFT JOIN (SELECT `g`.*,`l`.`group_name`,`l`.`group_about` FROM `gear_controller_groups` `g` JOIN `gear_controller_groups_l` `l` USING(`group_id`) '
             . 'WHERE `l`.`language_id`=' . $this->language->get('id') . ') `cg` USING (`group_id`) '
             . 'WHERE `cl`.`language_id`=' . $this->language->get('id') . ' AND `c`.`module_id`=' . $this->uri->id;
        if (($query->execute($sql)) === false)
            throw new GSqlException();
        $data = array();
        while (!$query->eof()) {
            $cmp = $query->next();
            // События
            $json = json_decode($cmp['controller_privileges'], true);
            if ($json)
                $privileges = GPrivileges::toString($json, ', ');
            else
                $privileges = '';
            // Поля
            $json = json_decode($cmp['controller_fields'], true);
            $res = array();
            for ($i = 0; $i < sizeof($json); $i++) {
                $res[] = $json[$i]['label'];
            }
            if ($res)
                $fields = implode(', ', $res);
            else
                $fields = '';

            $url = 'Manager.COMPONENTS_URL + \'' . $cmp['controller_uri_pkg'] . '\' + Ext.ROUTER_DELIMITER + \'' . $cmp['controller_uri_action'] . '\'';
            $item = array(
                array('xtype' => 'mn-field-separator', 'html' => $cmp['controller_name']),
                array('xtype'      => 'displayfield',
                      'hideLabel'  => true,
                      'value'      => '<img src="' . PATH_COMPONENT . $cmp['controller_uri'] . 'resources/shortcut.png" style="cursor:pointer;" onclick="Manager.widget.load({url:' . $url . '});"/>'),
                array('xtype'      => 'displayfield',
                      'fieldClass' => 'mn-field-value-info', 
                      'fieldLabel' => $this->_['label_controller_class'],
                      'value'      => $cmp['controller_class']),
                array('xtype'      => 'displayfield',
                      'fieldClass' => 'mn-field-value-info',
                      'fieldLabel' => $this->_['label_group_id'],
                      'value'      => $cmp['group_name']),
                array('xtype'      => 'displayfield',
                      'fieldClass' => 'mn-field-value-info',
                      'fieldLabel' => $this->_['label_controller_clear'],
                      'value'      => $this->_['type_boolean'][$cmp['controller_clear']]),
                array('xtype'      => 'displayfield',
                      'fieldClass' => 'mn-field-value-info',
                      'fieldLabel' => $this->_['label_controller_statistics'],
                      'value'      => $this->_['type_boolean'][$cmp['controller_statistics']]),
                array('xtype'      => 'fieldset',
                      'labelWidth' => 75,
                      'title'      => $this->_['title_fieldset_path'],
                      'autoHeight' => true,
                      'items'      => array(
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_controller_uri_pkg'],
                                'value'      => $cmp['controller_uri_pkg']),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_controller_uri'],
                                'value'      => $cmp['controller_uri']),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_controller_uri_action'],
                                'value'      => $cmp['controller_uri_action'])
                      )
                ),
                array('xtype'      => 'fieldset',
                      'labelWidth' => 75,
                      'title'      => $this->_['title_fieldset_interface'],
                      'autoHeight' => true,
                      'items'      => array(
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_controller_enabled'],
                                'value'      => $this->_['type_boolean'][$cmp['controller_enabled']]),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_controller_visible'],
                                'value'      => $this->_['type_boolean'][$cmp['controller_visible']]),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_controller_dashboard'],
                                'value'      => $this->_['type_boolean'][$cmp['controller_dashboard']])
                      )
                )
            );
            // если есть привилегии
            if ($privileges) {
                $item[] = array(
                    'xtype'      => 'fieldset',
                    'title'      => $this->_['title_fieldset_privileges'],
                    'autoHeight' => true,
                    'items'      => array(
                        array('xtype'      => 'displayfield',
                              'fieldClass' => 'mn-field-value-info',
                              'hideLabel'  => true,
                              'value'      => $privileges)
                    )
                );
            }
            // если есть поля
            if ($fields) {
                $item[] = array(
                    'xtype'      => 'fieldset',
                    'title'      => $this->_['title_fieldset_fields'],
                    'autoHeight' => true,
                    'items'      => array(
                        array('xtype'      => 'displayfield',
                              'fieldClass' => 'mn-field-value-info',
                              'hideLabel'  => true,
                              'value'      => $fields)
                    )
                );
            }
            $item[] = array('xtype' => 'displayfield', 'hideLabel' => true, 'value' => '<br>');
            $data[] = $item;
        }

        return $data;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'id'            => $this->classId,
                  'titleEllipsis' => 45,
                  'gridId'        => 'gcontroller_ycmodules_grid',
                  'width'         => 480,
                  'height'        => 370,
                  'resizable'     => true,
                  'stateful'      => false,
                  'buttonsAdd'    => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-system-modules',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );

        // вкладка "атрибуты"
        $tabAttr = array(
            'title'       => $this->_['title_tab_attributes'],
            'layout'      => 'form',
            'labelWidth'  => 75,
            'defaultType' => 'textfield',
            'baseCls'     => 'mn-form-tab-body',
            'iconSrc'     => $this->resourcePath . 'icon-tab-attr.png',
            'height '     => 990,
            'items'       => array(
               array('fieldLabel' => $this->_['label_module_name'],
                     'name'       => 'module_name',
                     'maxLength'  => 255,
                     'anchor'     => '100%',
                     'allowBlank' => false),
               array('fieldLabel' => $this->_['label_module_shortname'],
                     'name'       => 'module_shortname',
                     'maxLength'  => 20,
                     'width'      => 100,
                     'allowBlank' => false),
               array('fieldLabel' => $this->_['label_module_version'],
                     'name'       => 'module_version',
                     'maxLength'  => 10,
                     'width'      => 100,
                     'allowBlank' => false),
               array('xtype'      => 'datefield',
                     'fieldLabel' => $this->_['label_module_date'],
                     'name'       => 'module_date',
                     'format'     => 'd-m-Y',
                     'width'      => 100,
                     'allowBlank' => false),
               array('fieldLabel' => $this->_['label_module_author'],
                     'name'       => 'module_author',
                     'maxLength'  => 100,
                     'anchor'     => '100%',
                     'allowBlank' => false),
               array('xtype'      => 'textarea',
                     'itemCls'    => 'mn-form-item-quiet',
                     'name'       => 'module_description',
                     'fieldLabel' => $this->_['label_module_description'],
                     'anchor'     => '100%',
                     'height'     => 80,
                     'allowBlank' => true)
                )
        );

        // вкладка "настройки"
        $tabSettings = array(
            'title'       => $this->_['title_tab_settings'],
            'layout'      => 'form',
            'labelWidth'  => 75,
            'defaultType' => 'textfield',
            'iconSrc'     => $this->resourcePath . 'icon-tab-settings.png',
            'baseCls'     => 'mn-form-tab-body',
            'items'       => array(
                array('xtype'      => 'fieldset',
                      'labelWidth' => 75,
                      'title'      => $this->_['title_fieldset_tables'],
                      'autoHeight' => true,
                      'items'      => array(
                         array('xtype'      => 'textarea',
                               'name'       => 'module_tables',
                               'hideLabel'  => true,
                               'anchor'     => '100%',
                               'height'     => 100,
                               'allowBlank' => true)
                      )
                )
            )
        );

        // поля формы
        $items = array(
            'xtype'             => 'tabpanel',
            'layoutOnTabChange' => true,
            'deferredRender'    => false,
            'activeTab'         => 0,
            'enableTabScroll'   => true,
            'anchor'            => '100% 100%',
            'items'             => array($tabAttr, $tabSettings)
        );
        // если состояние формы "правка"
        if (!$this->isInsert) {
            // вкладка "компоненты"
            $tabComponents = array(
                'title'       => $this->_['title_tab_components'],
                'layout'      => 'form',
                'labelWidth'  => 75,
                'baseCls'     => 'mn-form-tab-body',
                'iconSrc'     => $this->resourcePath . 'icon-tab-cmp.png',
                'autoScroll'  => true,
                'items'       => $this->getComponents()
            );
            $items['items'][] = $tabComponents;
        }

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->url = $this->componentUrl . 'profile/';
        $form->items->add($items);

        parent::getInterface();
    }
}
?>