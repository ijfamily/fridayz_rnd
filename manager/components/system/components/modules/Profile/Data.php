<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля модуля системы"
 * Пакет контроллеров "Модули системы"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YCModules_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные профиля модуля системы
 * 
 * @category   Gear
 * @package    GController_YCModules_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YCModules_Profile_Data extends GController_Profile_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Массив полей таблицы ($_tableName)
     *
     * @var array
     */
    public $fields = array(
        'module_name','module_shortname', 'module_description', 'module_version',
        'module_date', 'module_author', 'module_tables'
    );

    /**
     * Первичный ключ таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'module_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_modules';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ycmodules_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        if ($this->state == 'update')
            return sprintf($this->_['title_profile_update'], $record['module_name']);
        else
            return sprintf($this->_['title_profile_info'], $record['module_name']);
    }

    /**
     * Проверка существования зависимых записей (в каскадное удаление)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        $query = new GDb_Query();
        // обновить группы компонентов ("gear_controller_groups")
        $sql = 'UPDATE `gear_controller_groups` SET `module_id`=NULL WHERE `module_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // обновить компоненты ("gear_controllers")
        $sql = 'UPDATE `gear_controllers` SET `module_id`=NULL WHERE `module_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    { 
        if (!empty($params['module_date']))
            $params['module_date'] = date('Y-m-d', strtotime($params['module_date']));
    }
}
?>