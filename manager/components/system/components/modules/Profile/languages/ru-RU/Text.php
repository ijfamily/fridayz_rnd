<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Создание записи "Модуль системы"',
    'title_profile_update' => 'Изменение записи "%s"',
    'title_profile_info'   => 'Информация о записи "%s"',
    'text_btn_help'        => 'Справка',
    // поля
    'title_tab_attributes'      => 'Атрибуты',
    'title_tab_settings'        => 'Настройки',
    'title_fieldset_tables'     => 'Таблицы базы данных для демонтажа модуля',
    'label_module_name'         => 'Название',
    'label_module_shortname'    => 'Название (сокр.)',
    'label_module_description'  => 'Описание',
    'label_module_version'      => 'Версия',
    'label_module_date'         => 'Дата',
    'label_module_author'       => 'Авторы',
    'title_tab_components'      => 'Компоненты',
    'header_attr'                => 'Атрибуты компонента',
    'title_fieldset_common'      => 'Компонент: ',
    'label_controller_class'     => 'ID класса',
    'label_group_id'             => 'Группа',
    'label_controller_clear'     => 'Очищать',
    'label_controller_statistics' => 'Статистика',
    'title_fieldset_path'        => 'Ресурс',
    'title_fieldset_interface'   => 'Интерфейс',
    'label_controller_uri_pkg'   => 'Компонент',
    'label_controller_uri'       => 'Контроллер',
    'label_controller_uri_action' => 'Интерфейс',
    'label_controller_enabled'   => 'Доступный',
    'label_controller_visible'   => 'Видимый',
    'label_controller_dashboard' => 'На доске',
    'title_fieldset_privileges'  => 'Допустимые права',
    'title_fieldset_fields'      => 'Права на поля',
    // типы
    'type_boolean' => array('нет', 'да')
);
?>