<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Модули системы"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YCModules
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Модули системы
 * Описание: Модули системы
 * ID класса: gcontroller_ycgroups_grid
 * Группа: Система / Компоненты
 * Очищать: нет
 * Ресурс
 *    Пакет: system/components/modules/
 *    Контроллер: system/components/modules/Grid/
 *    Интерфейс: grid/interface/
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске: нет
 */

return array(
    // {Y}- модуль "System" {СM} - пакет контроллеров "Сontrollers modules" -> YCModules
    'clsPrefix' => 'YCModules'
);
?>