<?php
/**
 * Gear Manager
 *
 * Контроллер         "Демонтаж модулей системы"
 * Пакет контроллеров "Модули системы"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YCModules_Uninstall
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Демонтаж модулей системы
 * 
 * @category   Gear
 * @package    GController_YCModules_Uninstall
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YCModules_Uninstall_Data extends GController
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ycmodules_grid';

    /**
     * Доступ на удаление всех данных
     * 
     * @return void
     */
    protected function dataAccessUninstall()
    {
        // проверка доступа к контроллеру класса
        if ($this->checkPrivilege)
            if (!$this->store->hasPrivilege(array('root'))) {
                $this->response->add('icon', 'icon-msq-access');
                throw new GException('Error access', 'No privileges to perform this action');
            }
        // соединение с базой данных
        GFactory::getDb()->connect();
        $this->response->set('action', 'uninstall');
        $this->response->setMsgResult($this->_['msg_uninstall_title'], $this->_['msg_uninstall'], true);
    }

    /**
     * Добавление в журнал действий пользователей
     * (удаление данных)
     * 
     * @param array $params параметры журнала
     * @return void
     */
    protected function logUninstall($params = array())
    {
        // если invisible нечего не делать
        if ($this->session->set('invisible')) return;

        $params['log_action'] = 'uninstall';
        $params['log_query_id'] = $this->uri->id;
        $params['controller_id'] = $this->store->getFrom('params', 'id', 0, $this->accessId);
        $params['controller_class'] = $this->classId;
        GFactory::getDbDriver('log')->add($params);
    }

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор,
     * используется для инициализации атрибутов контроллера без конструктора
     * 
     * @return void
     */
    public function uninstall()
    {
        $this->dataAccessUninstall();

        $query = new GDb_Query();
        // выбранный модуль
        $sql = 'SELECT * FROM `gear_modules` WHERE `module_id`=' . $this->uri->id;
        if (($module = $query->getRecord($sql)) === false)
            throw new GSqlException();
        // если нет модуля
        if (empty($module))
            throw new GException('Data processing error', $this->_['msg_module_not_exists']);
        // если модуль системный
        if ($module['sys_record'])
            throw new GException('Data processing error', 'Unable to delete records!');
        // список групп компонетов
        $sql = 'SELECT * FROM `gear_controller_groups` WHERE `module_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $groups = array();
        while (!$query->eof()) {
            $group = $query->next();
            $groups[] = $group['group_id'];
        }
        $groupId = implode(',', $groups);
        // удаление доступа к компонентам ("gear_controller_access")
        $sql = 'DELETE `a` FROM `gear_controller_access` `a`, `gear_controllers` `c` WHERE `a`.`controller_id`=`c`.`controller_id` AND '
             . '`c`.`module_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        // удаление локализации компонентов ("gear_controllers_l")
        $sql = 'DELETE `cl` FROM `gear_controllers` `c`, `gear_controllers_l` `cl` WHERE `cl`.`controller_id`=`c`.`controller_id` AND '
             . '`c`.`module_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        // удаление компонентов ("gear_controllers")
        $sql = 'DELETE FROM `gear_controllers` WHERE `module_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        // удаление локализации групп компонентов ("gear_controller_groups_l")
        $sql = 'DELETE `gl` FROM `gear_controller_groups` `g`, `gear_controller_groups_l` `gl` WHERE `gl`.`group_id`=`g`.`group_id` AND '
             . '`g`.`module_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление групп компонентов ("gear_controller_groups")
        Gear::library('Db/Drivers/MySQL/Tree/Tree');

        $db = GFactory::getDb();
        $tree = new GDb_Tree('gear_controller_groups', 'group', $db->getHandle());
        for ($i = 0; $i < sizeof($groups); $i++) {
            // удаление ветвей дерева
            $tree->DeleteAll($groups[$i], '');
            if (!empty($tree->ERRORS_MES)) {
                // просто группа была ранее удалена
                if ($tree->ERRORS_MES[0] == 'no_element_in_tree') continue;
                throw new GException('Data processing error', $this->_['msg_error_groups_delete'], array($tree->ERRORS_MES[0]));
            }
        }
        // удаление модуля ("gear_modules")
        $sql = 'DELETE FROM `gear_modules` WHERE `module_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // обновление ключей
        $sql = 'ALTER TABLE `gear_controllers` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_controller_groups` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_modules` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        // удаление таблиц модуля
        if (!empty($module['module_tables'])) {
            $tables = $module['module_tables'];
            $tables = explode(',', $tables);
            for ($i = 0; $i < sizeof($tables); $i++) {
                $table = trim($tables[$i]);
                $sql = 'DROP TABLE `' . trim($tables[$i]) . '`';
                if ($query->execute($sql) === false)
                    throw new GSqlException();
            }
        }

        // ответ серверу
        $this->response->add('refreshGrid', 'gcontroller_ycmodules_grid');
        $this->response->add('refreshShortcuts', true);
        // запись в журнал действий пользователя
        $this->logUninstall();
    }

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        // метод запроса
        switch ($this->uri->method) {
            // метод "GET"
            case 'GET':
                // тип действия
                if ($this->uri->action == 'data') {
                    $this->uninstall();
                    return;
                }
                break;
        }

        parent::init();
    }
}
?>