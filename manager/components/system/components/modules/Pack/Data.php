<?php
/**
 * Gear Manager
 *
 * Контроллер         "Упаковка модуля"
 * Пакет контроллеров "Модули системы"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YCModules_Pack
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('File');

/**
 * Упаковка модуля
 * 
 * @category   Gear
 * @package    GController_YCModules_Pack
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YCModules_Pack_Data extends GController
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ycmodules_grid';

    /**
     * Каталог экспорта данных
     *
     * @var string
     */
    protected $_pathData = 'data/export/';

    /**
     * Доступ на удаление всех данных
     * 
     * @return void
     */
    protected function dataAccessPack()
    {
        // проверка доступа к контроллеру класса
        if ($this->checkPrivilege)
            if (!$this->store->hasPrivilege(array('root'))) {
                $this->response->add('icon', 'icon-msq-access');
                throw new GException('Error access', 'No privileges to perform this action');
            }
        // соединение с базой данных
        GFactory::getDb()->connect();
        $this->response->set('action', 'pack');
    }

    /**
     * Добавление в журнал действий пользователей
     * (удаление данных)
     * 
     * @param array $params параметры журнала
     * @return void
     */
    protected function logPack($params = array())
    {
        // если invisible нечего не делать
        if ($this->session->set('invisible')) return;

        if ($params['log_query_params'])
            $params['log_query_params'] = json_encode($params['log_query_params']);
        $params['log_action'] = 'pack';
        $params['log_query_id'] = $this->uri->id;
        $params['controller_id'] = $this->store->getFrom('params', 'id', 0, $this->accessId);
        $params['controller_class'] = $this->classId;
        GFactory::getDbDriver('log')->add($params);
    }

    /**
     * Упаковка модуля в JSON
     * 
     * @param array $data данны пакета
     * @return string
     */
    public function packToJson($data)
    {
        $str = '{';
        $str .= '    "install": "module",' . "\r";
        $str .= '    "module": {' . "\r";
        $str .= '        "name": "' . $data['module']['name'] . '",' . "\r";
        $str .= '        "shortname": "' . $data['module']['shortname'] . '",' . "\r";
        $str .= '        "nameSettings": "' . $data['module']['nameSettings'] . '",' . "\r";
        $str .= '        "description": "' . $data['module']['description'] . '",' . "\r";
        $str .= '        "version": "' . $data['module']['version'] . '",' . "\r";
        $str .= '        "date": "' . $data['module']['date'] . '",' . "\r";
        $str .= '        "author": "' . $data['module']['author'] . '",' . "\r";
        $str .= '        "settings": "' . $data['module']['settings'] . '",' . "\r";
        $str .= '        "tables": "' . $data['module']['tables'] . '"' . "\r";
        $str .= '    },' . "\r";
        $str .= '    "componentGroups": ['. "\r";
        $i = 0;
        foreach ($data['groups'] as $id => $group) {
            $str .= "    {\r";
            $str .= '        "id": "g' . $group['id'] . '",' . "\r";
            $str .= '        "parent": "' . $group['parent'] . '",' . "\r";
            $str .= '        "module": "new",' . "\r";
            $str .= '        "name": {' . "\r";
            // название
            $j = 0;
            foreach ($group['name'] as $key => $value) {
                $str .= '            "' . $key . '": "' . $value;
                 if ($j < sizeof($group['name']) - 1)
                    $str .= '",' . "\r";
                 else
                    $str .= '"' . "\r";
                $j++;
            }
            $str .= "        },\r";
            $str .= '        "description": {' . "\r";
            // описание
            $j = 0;
            foreach ($group['description'] as $key => $value) {
                $str .= '            "' . $key . '": "' . $value;
                 if ($j < sizeof($group['description']) - 1)
                    $str .= '",' . "\r";
                 else
                    $str .= '"' . "\r";
                $j++;
            }
            $str .= "        }\r";
            if ($i < sizeof($data['groups']) - 1)
                $str .= "    },\r";
            else
                $str .= "    }\r";
            $i++;
        }
        $str .= "    ],\r";
        $str .= '    "components": ['. "\r";
        $i = 0;
        foreach ($data['components'] as $id => $cmp) {
            $str .= "    {\r";
            $str .= '        "name": {' . "\r";
            // название
            $j = 0;
            foreach ($cmp['name'] as $key => $value) {
                $str .= '            "' . $key . '": "' . $value;
                 if ($j < sizeof($cmp['name']) - 1)
                    $str .= '",' . "\r";
                 else
                    $str .= '"' . "\r";
                $j++;
            }
            $str .= "        },\r";
            $str .= '        "description": {' . "\r";
            // описание
            $j = 0;
            foreach ($cmp['description'] as $key => $value) {
                $str .= '            "' . $key . '": "' . $value;
                 if ($j < sizeof($cmp['description']) - 1)
                    $str .= '",' . "\r";
                 else
                    $str .= '"' . "\r";
                $j++;
            }
            $str .= "        },\r";
            $str .= '        "class": "' . $cmp['class'] . '",' . "\r";
            $str .= '        "module": "' . $cmp['module'] . '",' . "\r";
            $str .= '        "group": "' . $cmp['group'] . '",' . "\r";
            $str .= '        "clear": ' . $cmp['clear'] . ',' . "\r";
            $str .= '        "statistics": ' . $cmp['statistics'] . ',' . "\r";
            $str .= '        "resource": {' . "\r";
            $str .= '            "package": "' . $cmp['resource']['package'] . '",' . "\r";
            $str .= '            "uri": "' . $cmp['resource']['uri'] . '",' . "\r";
            $str .= '            "action": "' . $cmp['resource']['action'] . '",' . "\r";
            $str .= '            "menu": "' . $cmp['resource']['menu'] . '"' . "\r";
            $str .= "        },\r";
            $str .= '        "interface": {' . "\r";
            $str .= '            "dashboard": ' . $cmp['interface']['dashboard'] . ',' . "\r";
            $str .= '            "enabled": ' . $cmp['interface']['enabled'] . ',' . "\r";
            $str .= '            "visible": ' . $cmp['interface']['visible'] . ',' . "\r";
            $str .= '            "menu": ' . $cmp['interface']['menu'] . "\r";
            $str .= "        },\r";
            $str .= '        "privileges": "' . $cmp['privileges'] . '",' . "\r";
            $str .= '        "userGroups": [' . "\r";
            $str .= '            { "group" : 1, "privileges" : "root" }' . "\r";
            $str .= "        ]\r";
            if ($i < sizeof($data['components']) - 1)
                $str .= "    },\r";
            else
                $str .= "    }\r";
            $i++;
        }
        $str .= "    ]\r";
        $str .= '}';

        return $str;
    }

    /**
     * Упаковка модуля и его компонентов
     * 
     * @return void
     */
    public function pack()
    {
        $this->dataAccessPack();

        $query = new GDb_Query();
        // модуль
        $sql = 'SELECT * FROM `gear_modules` WHERE `module_id`=' . $this->uri->id;
        if (($recModule = $query->getRecord($sql)) === false)
            throw new GSqlException();
        // если нет модуля
        if (empty($recModule))
            throw new GException('Data processing error', 'Query error (Internal Server Error)');
        $module = array(
            'name'         => $recModule['module_name'],
            'shortname'    => $recModule['module_shortname'],
            'nameSettings' => $recModule['module_name_settings'],
            'description'  => $recModule['module_description'],
            'version'      => $recModule['module_version'],
            'date'         => $recModule['module_date'],
            'author'       => $recModule['module_author'],
            'settings'     => $recModule['module_settings'],
            'tables'       => $recModule['module_tables'],
        );

        // список групп компонетов
        $sql = 'SELECT `gp`.`group_id` `pgroup_id`,  `l`.*, `g`.*, `ln`.`language_alias` '
             . 'FROM `gear_controller_groups` `gp`, `gear_controller_groups` `g` '
             . 'JOIN `gear_controller_groups_l` `l` USING(`group_id`) '
             . 'JOIN `gear_languages` `ln` USING(`language_id`) '
             . 'WHERE `g`.`module_id`=' . $this->uri->id
             . ' AND `g`.`group_left` > `gp`.`group_left` AND `gp`.`group_right` > `g`.`group_right` AND `gp`.`group_level` = `g`.`group_level`-1 '
             . ' ORDER BY `g`.`group_id` ASC';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->getCountRecords() == 0)
            throw new GException('Error', $this->_['msg_groups_not_exists']);
        $groups = array();
        while (!$query->eof()) {
            $recGroup = $query->next();
            $gid = $recGroup['group_id'];
            if (!isset($groups[$gid])) {
                $groups[$gid] = array(
                    'id'          => $gid,
                    'parent'      => $recGroup['pgroup_id'] == 1 ? 'root' : 'g' . $recGroup['pgroup_id'],
                    'module'      => 'new',
                    'name'        => array(),
                    'description' => array(),
                );
            }
            $groups[$gid]['name'][$recGroup['language_alias']] = $recGroup['group_name'];
            $groups[$gid]['description'][$recGroup['language_alias']] = $recGroup['group_about'];
        }

        // список компоненто
        $sql = 'SELECT `cl`.*, `c`.*, `ln`.`language_alias` FROM `gear_controllers` `c` '
             . 'JOIN `gear_controllers_l` `cl` USING(`controller_id`) '
             . 'JOIN `gear_languages` `ln` USING(`language_id`) '
             . 'WHERE `c`.`module_id`=' . $this->uri->id . ' ORDER BY `c`.`controller_id` ASC';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->getCountRecords() == 0)
            throw new GException('Error', $this->_['msg_components_not_exists']);
        $components = array();
        while (!$query->eof()) {
            $recCmp = $query->next();
            $cid = $recCmp['controller_id'];
            if (!isset($components[$cid])) {
                if (empty($recCmp['controller_privileges']))
                     $privileges = '';
                else {
                    $json = json_decode($recCmp['controller_privileges'], true);
                    $count = sizeof($json);
                    $arr = array();
                    for ($i = 0; $i < $count; $i++) {
                        $arr[] = $json[$i]['action'];
                    }
                    $recCmp['controller_privileges'] = implode(',', $arr);
                }
                $components[$cid] = array(
                    'name'        => array(),
                    'description' => array(),
                    'class'       => $recCmp['controller_class'],
                    'module'      => 'new',
                    'group'       => 'g' . $recCmp['group_id'],
                    'clear'       => (int) $recCmp['controller_clear'],
                    'statistics'  => (int) $recCmp['controller_statistics'],
                    'resource'    => array(
                        'package' => $recCmp['controller_uri_pkg'],
                        'uri'     => $recCmp['controller_uri'],
                        'action'  => $recCmp['controller_uri_action'],
                        'menu'    => $recCmp['controller_uri_menu']
                    ),
                    'interface'  => array(
                        'dashboard' => $recCmp['controller_dashboard'],
                        'enabled'   => $recCmp['controller_enabled'],
                        'visible'   => $recCmp['controller_visible'],
                        'menu'      => $recCmp['controller_menu']
                    ),
                    'privileges' => $recCmp['controller_privileges'],
                    'userGroups' => array(
                        array('group' => 1, 'privileges' => 'root')
                    )
                );
            }
            $components[$cid]['name'][$recCmp['language_alias']] = $recCmp['controller_name'];
            $components[$cid]['description'][$recCmp['language_alias']] = $recCmp['controller_about'];
        }

        $text = $this->packToJson(array('module' => $module, 'groups' => $groups, 'components' => $components));
        // название файла
        $filename = 'module_' . $module['nameSettings'] . '_v_' . $module['version'] . '.json';
        $path = $this->_pathData . $filename;
        // запись пака
        GFile::write($path, 'w', $text);
        // сообщение
        $this->response->setMsgResult($this->_['msg_pack_title'], sprintf($this->_['msg_pack'], $path, $filename), true);
        // запись в журнал действий пользователя
        $this->logPack(array(
            'log_query'        => sprintf($this->_['msg_log_pack'], $module['name'], $filename),
            'log_query_params' => array(
                'filename' => $path,
                'module'   => $module['nameSettings']
            )
        ));
    }

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        // метод запроса
        switch ($this->uri->method) {
            // метод "GET"
            case 'GET':
                // тип действия
                if ($this->uri->action == 'data') {
                    $this->pack();
                    return;
                }
                break;
        }

        parent::init();
    }
}
?>