<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // сообщения
    'msg_pack_title' => 'Упаковка модуля',
    'msg_pack'       => 'Модуль был успешно упакован<br>скачать: <a href="%s" target="_blank">%s</a>',
    // сообщения
    'msg_log_pack'              => 'Упаковка модуля "%s" в установочный файл "%s"',
    'msg_groups_not_exists'     => 'Модуль не содержит групп компонентов!',
    'msg_components_not_exists' => 'Модуль не содержит компонентов!',
);
?>