<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск в списке модулей"
 * Пакет контроллеров "Модули системы"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YCModules_Search
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск в списке модулей
 * 
 * @category   Gear
 * @package    GController_YCModules_Search
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YCModules_Search_Data extends GController_Search_Data
{
    /**
     * дентификатор компонента (списка) к которому применяется поиск
     *
     * @var string
     */
    public $gridId = 'gcontroller_ycmodules_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ycmodules_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор,
     * используется для инициализации атрибутов контроллера без конструктора
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'module_name', 'label' => $this->_['header_module_name']),
            array('dataIndex' => 'module_shortname', 'label' => $this->_['header_module_shortname']),
            array('dataIndex' => 'module_description', 'label' => $this->_['header_module_description']),
            array('dataIndex' => 'module_version', 'label' => $this->_['header_module_version']),
            array('dataIndex' => '_module_date', 'label' => $this->_['header_module_date']),
            array('dataIndex' => 'module_author', 'label' => $this->_['header_module_author']),
            array('dataIndex' => 'module_groups_count', 'label' => $this->_['header_module_groups'])
        );
    }
}
?>