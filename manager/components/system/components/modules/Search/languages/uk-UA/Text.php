<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Поиск в списке "Модулі системи"',
    // поля
    'header_module_name'        => 'Назва',
    'header_module_shortname'   => 'Назва (скр.)',
    'header_module_description' => 'Опис',
    'header_module_version'     => 'Версія',
    'header_module_date'        => 'Дата',
    'header_module_author'      => 'Автори',
    'header_module_groups'      => 'Груп'
    
);
?>