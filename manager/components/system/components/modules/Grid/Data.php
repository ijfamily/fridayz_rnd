<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка модулей системы"
 * Пакет контроллеров "Модули системы"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YCModules_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные списка модулей системы
 * 
 * @category   Gear
 * @package    GController_YCModules_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YCModules_Grid_Data extends GController_Grid_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * (для обработки записей таблицы)
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'module_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_modules';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS `cc`.`groups_count`, `cm`.* '
          . 'FROM `gear_modules` `cm` '
          . 'LEFT JOIN (SELECT `module_id`,COUNT(`module_id`) `groups_count` '
          . 'FROM `gear_controller_groups` GROUP BY `module_id`) `cc` USING (`module_id`)'
          . 'WHERE 1 %filter '
          . 'ORDER BY %sort '
          . 'LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            'module_name' => array('type' => 'string'),
            'module_shortname' => array('type' => 'string'),
            'module_description' => array('type' => 'string'),
            'module_version' => array('type' => 'string'),
            'module_date' => array('type' => 'date'),
            'module_author' => array('type' => 'string'),
            'groups_count' => array('type' => 'integer')
        );
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        $query = new GDb_Query();
        // обновить группы компонентов ("gear_controller_groups")
        $sql = 'UPDATE `gear_controller_groups` `g`,`gear_modules` `m` '
             . 'SET `g`.`module_id`=NULL '
             . 'WHERE `g`.`module_id`=`m`.`module_id` AND `m`.`sys_record`<>1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // обновить компоненты ("gear_controllers")
        $sql = 'UPDATE `gear_controllers` `c`,`gear_modules` `m` '
             . 'SET `c`.`module_id`=NULL '
             . 'WHERE `c`.`module_id`=`m`.`module_id` AND `m`.`sys_record`<>1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление модулей ("gear_modules")
        $sql = 'DELETE FROM `gear_modules` WHERE `sys_record`<>1';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Проверка существования зависимых записей (в каскадное удаление)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        $query = new GDb_Query();
        // обновить группы компонентов ("gear_controller_groups")
        $sql = 'UPDATE `gear_controller_groups` SET `module_id`=NULL WHERE `module_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // обновить компоненты ("gear_controllers")
        $sql = 'UPDATE `gear_controllers` SET `module_id`=NULL WHERE `module_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }
}
?>