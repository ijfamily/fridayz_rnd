<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка модулей системы"
 * Пакет контроллеров "Модули системы"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YCModules_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс списка модулей системы
 * 
 * @category   Gear
 * @package    GController_YCModules_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YCModules_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * (для обработки записей таблицы)
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Type sort
     * (config options - Ext.data.JsonStore.sortInfo.dir)
     *
     * @var string
     */
    public $dir = 'DESC';

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'module_id';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'module_name';

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // поля списка  (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('name' => 'module_name', 'type' => 'string'),
            array('name' => 'module_shortname', 'type' => 'string'),
            array('name' => 'module_description', 'type' => 'string'),
            array('name' => 'module_version', 'type' => 'string'),
            array('name' => 'module_date', 'type' => 'date'),
            array('name' => 'module_author', 'type' => 'string'),
            array('name' => 'groups_count', 'type' => 'integer')
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $settings = $this->session->get('user/settings');
        $this->columns = array(
            array('xtype'     => 'expandercolumn',
                  'url'       => $this->componentUrl . 'grid/expand/',
                  'typeCt'    => 'row'),
            array('xtype'     => 'numbercolumn'),
            array('xtype'     => 'rowmenu'),
            array('dataIndex' => 'module_name',
                  'header'    => '<em class="mn-grid-hd-details"></em>' . $this->_['header_module_name'],
                  'width'     => 150,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'module_shortname',
                  'header'    => $this->_['header_module_shortname'],
                  'width'     => 110,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'module_description',
                  'header'    => $this->_['header_module_description'],
                  'width'     => 170,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'module_version',
                  'header'    => $this->_['header_module_version'],
                  'width'     => 70,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('xtype'     => 'datetimecolumn',
                  'dataIndex' => 'module_date',
                  'frmDate'   => $settings['format/date'],
                  'header'    => $this->_['header_module_date'],
                  'width'     => 80,
                  'sortable'  => true,
                  'filter'    => array('type' => 'date')),
            array('dataIndex' => 'module_author',
                  'header'    => $this->_['header_module_author'],
                  'width'     => 150,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'groups_count',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-groups.png" align="absmiddle">',
                  'tooltip'   => $this->_['tooltip_module_groups'],
                  'align'     => 'center',
                  'width'     => 45,
                  'sortable'  => true,
                  'filter'    => array('type' => 'numeric'))
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_grid'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_grid'],
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'profileUrl'    => $this->componentUrl . 'profile/')
        );

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "edit" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "добавить" (ExtJS class "Manager.button.InsertData")
        $group->items->add(array('xtype' => 'mn-btn-insert-data', 'gridId' => $this->classId));
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' => $this->classId));
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'      => 'mn-btn-clear-data',
                  'msgConfirm' => $this->_['msg_btn_clear'],
                  'gridId'     => $this->classId,
                  'isN'        => true)
        );
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "демонтаж" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'      => 'mn-btn-widget-grid',
                  'text'       => $this->_['text_btn_uninstall'],
                  'tooltip'    => $this->_['tooltip_btn_uninstall'],
                  'msgWarning'    => $this->_['msg_btn_uninstall_select'],
                  'msgWarningRec' => $this->_['msg_btn_uninstall_selects'],
                  'icon'       => $this->resourcePath . 'icon-btn-uninstall.png',
                  'url'        => $this->componentUrl . 'uninstall/data/',
                  'gridId'     => $this->classId)
        );
        // кнопка "установка" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'   => 'mn-btn-widget',
                  'text'    => $this->_['text_btn_install'],
                  'tooltip' => $this->_['tooltip_btn_install'],
                  'icon'    => $this->resourcePath . 'icon-btn-install.png',
                  'url'     => $this->componentUri . '../install/' . ROUTER_DELIMITER . 'upload/interface/')
        );
        // кнопка "упаковка" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'      => 'mn-btn-widget-grid',
                  'text'       => $this->_['text_btn_pack'],
                  'tooltip'    => $this->_['tooltip_btn_pack'],
                  'msgWarning'    => $this->_['msg_btn_pack_select'],
                  'msgWarningRec' => $this->_['msg_btn_pack_selects'],
                  'icon'       => $this->resourcePath . 'icon-btn-pack.png',
                  'url'        => $this->componentUrl . 'pack/data/',
                  'gridId'     => $this->classId)
        );
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = 'component-system-modules';
        $this->_cmp->tbar->items->add($group);

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $cellInfo =
            '<div class="mn-grid-cell-tooltip-tl">{module_name}</div>'
          . '<div class="mn-grid-cell-tooltip" style="width:300px">'
          . '<em>' . $this->_['header_module_shortname'] . '</em>: <b>{module_shortname}</b><br>'
          . '<em>' . $this->_['header_module_description'] . '</em>: <b>{module_description}</b><br>'
          . '<em>' . $this->_['header_module_version'] . '</em>: <b>{module_version}</b><br>'
          . '<em>' . $this->_['header_module_date'] . '</em>: <b>{module_date}</b><br>'
          . '<em>' . $this->_['header_module_author'] . '</em>: <b>{module_author}</b>'
          . '</div>';
        $this->_cmp->cellTips = array(
            array('field' => 'module_name', 'tpl' => $cellInfo),
            array('field' => 'module_description', 'tpl' => '{module_description}'),
            array('field' => 'groups_count', 'tpl' => '{groups_count}')
        );
        $this->_cmp->cellTipConfig = array('maxWidth' => 300);

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $this->_cmp->rowMenu = array(
            'xtype' => 'mn-grid-rowmenu',
            'items' => array(
                array('text'    => $this->_['rowmenu_edit'],
                      'iconCls' => 'icon-form-edit',
                      'url'     => $this->componentUrl . 'profile/interface/')
            )
        );

        parent::getInterface();
    }
}
?>