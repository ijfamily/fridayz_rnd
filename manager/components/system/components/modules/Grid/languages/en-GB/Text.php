<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Modules of system',
    'tooltip_grid' => 'список модулей системы',
    'rowmenu_edit' => 'Edit',
    // панель управления
    'title_buttongroup_edit'   => 'Edit',
    'title_buttongroup_cols'   => 'Columns',
    'title_buttongroup_filter' => 'Filter',
    'msg_btn_delete'           => 'Are you sure you want to delete records <span class="mn-msg-delete">"System modules"</span> ?',
    'msg_btn_clear'            => 'Are you sure you want to delete all entries <span class="mn-msg-delete">"System modules"</span> ?',
    'text_btn_uninstall'       => 'Uninstall',
    'tooltip_btn_uninstall'    => 'Uninstall the module and its components with data',
    'msg_btn_uninstall_select'  => 'To Uninstall the module you need to select the module!',
    'msg_btn_uninstall_selects' => 'To Uninstall the module you need to choose only 1-n module!',
    'text_btn_install'          => 'Install',
    'tooltip_btn_install'       => 'Install modules',
    // поля
    'header_module_name'        => 'Name',
    'header_module_shortname'   => 'Name (sh.)',
    'header_module_description' => 'Description',
    'header_module_version'     => 'Version',
    'header_module_date'        => 'Date',
    'header_module_author'      => 'Author',
    'header_module_groups'      => 'Group',
    'tooltip_module_groups'     => 'Groups of components',
    // развернутая запись
    'header_attr'                 => 'Components module',
    'title_fieldset_common'       => 'Component',
    'label_controller_class'      => 'Class Id',
    'label_group_id'              => 'Group',
    'label_module_name'           => 'Module',
    'label_controller_clear'      => 'Cleaning',
    'label_privileges'            => 'Privileges',
    'label_controller_uri_pkg'    => 'Component (interface)',
    'label_controller_uri'        => 'Controller (interface)',
    'label_controller_uri_action' => 'Interface',
    'label_controller_enabled'    => 'Enabled',
    'label_controller_visible'    => 'Visible',
    'label_controller_dashboard'  => 'On board',
    'title_fieldset_fields'       => 'Fields'
);
?>