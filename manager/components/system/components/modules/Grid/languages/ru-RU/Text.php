<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Модули системы',
    'tooltip_grid' => 'список модулей системы',
    'rowmenu_edit' => 'Редактировать',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_delete'            => 'Вы действительно желаете удалить записи <span class="mn-msg-delete">"Модули системы"</span> ?',
    'msg_btn_clear'             => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">"Модули системы"</span> ?',
    'text_btn_pack'             => 'Упаковка',
    'tooltip_btn_pack'          => 'Упаковка модуля и его компанентов в установочный пакет',
    'msg_btn_pack_select'       => 'Для упаковки модуля Вам необходимо выбрать модуль!',
    'msg_btn_pack_selects'      => 'Для упаковки модуля Вам необходимо выбрать только 1-н модуль!',
    'text_btn_uninstall'        => 'Демонтаж',
    'tooltip_btn_uninstall'     => 'Демонтировать модуль и его компоненты с данными',
    'msg_btn_uninstall_select'  => 'Для демонтажа модуля Вам необходимо выбрать модуль!',
    'msg_btn_uninstall_selects' => 'Для демонтажа модуля Вам необходимо выбрать только 1-н модуль!',
    'text_btn_install'          => 'Установка',
    'tooltip_btn_install'       => 'Установка компонентов',
    // поля
    'header_module_name'        => 'Название',
    'header_module_shortname'   => 'Название (сокр.)',
    'header_module_description' => 'Описание',
    'header_module_version'     => 'Версия',
    'header_module_date'        => 'Дата',
    'header_module_author'      => 'Авторы',
    'header_module_groups'      => 'Групп',
    'tooltip_module_groups'     => 'Групп компонентов',
    // развернутая запись
    'header_attr'                 => 'Компоненты модуля',
    'title_fieldset_common'       => 'Компонент',
    'label_controller_class'      => 'ID класса',
    'label_group_id'              => 'Группа',
    'label_module_name'           => 'Модуль',
    'label_controller_clear'      => 'Очищать',
    'label_privileges'            => 'Допустимые права',
    'label_controller_uri_pkg'    => 'Компонент (интерфейс)',
    'label_controller_uri'        => 'Контроллер (интерфейс)',
    'label_controller_uri_action' => 'Интерфейс',
    'label_controller_enabled'    => 'Доступный',
    'label_controller_visible'    => 'Видимый',
    'label_controller_dashboard'  => 'На доске',
    'title_fieldset_fields'       => 'Права на поля'
);
?>