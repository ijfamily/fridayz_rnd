<?php
/**
 * Gear Manager
 *
 * Пакет украниской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Модулі системи',
    'tooltip_grid' => 'список модулей системы',
    'rowmenu_edit' => 'Редагувати',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Стовпці',
    'title_buttongroup_filter' => 'Фільтр',
    'msg_btn_delete'            => 'Ви дійсно бажаєте видалити записи <span class="mn-msg-delete">"Модулі системи"</span> ?',
    'msg_btn_clear'             => 'Ви дійсно бажаєте видалити всі записи <span class="mn-msg-delete">"Модулі системи"</span> ?',
    'text_btn_uninstall'        => 'Демонтаж',
    'tooltip_btn_uninstall'     => 'Демонтувати модуль і його компоненти з даними',
    'msg_btn_uninstall_select'  => 'Для демонтажу модуля Вам необхідно вибрати модуль!',
    'msg_btn_uninstall_selects' => 'Для демонтажу модуля Вам необхідно вибрати тільки 1-н модуль!',
    'text_btn_install'          => 'Установка',
    'tooltip_btn_install'       => 'Установка компонентов',
    // поля
    'header_module_name'        => 'Назва',
    'header_module_shortname'   => 'Назва (сокр.)',
    'header_module_description' => 'Опис',
    'header_module_version'     => 'Версія',
    'header_module_date'        => 'Дата',
    'header_module_author'      => 'Автор',
    'header_module_groups'      => 'Груп',
    'tooltip_module_groups'     => 'Груп компонентів',
    // развернутая запись
    'header_attr'                 => 'Компоненти модуля',
    'title_fieldset_common'       => 'Компонент',
    'label_controller_class'      => 'ID класа',
    'label_group_id'              => 'Група',
    'label_module_name'           => 'Модуль',
    'label_controller_clear'      => 'Очищати',
    'label_privileges'            => 'Допустимі права',
    'label_controller_uri_pkg'    => 'Компонент (інтерфейс)',
    'label_controller_uri'        => 'Контролер (інтерфейс)',
    'label_controller_uri_action' => 'Iнтерфейс',
    'label_controller_enabled'    => 'Доступний',
    'label_controller_visible'    => 'Відомий',
    'label_controller_dashboard'  => 'На дошці',
    'title_fieldset_fields'       => 'Права на поля'
);
?>