<?php
/**
 * Gear Manager
 *
 * Контроллер         "Развёрнутая запись компонента"
 * Пакет контроллеров "Список компонентов"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Controller_YControllers_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Expand');

/**
 * Развёрнутая запись компонента
 * 
 * @category   Gear
 * @package    Controller_YControllers_Grid
 * @subpackage Expand
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YControllers_Grid_Expand extends GController_Grid_Expand
{
    /**
     * Тайтл записи
     * 
     * @var string
     */
    protected $_title = '';

    /**
     * Вывод данных в интерфейс
     * 
     * @return void
     */
    protected function dataExpand()
    {
        parent::dataAccessView();

        $data = $this->getAttributes();
        $this->response->data = $data;
    }

    /**
     * Возращает атрибуты записи
     * 
     * @return string
     */
    protected function getAttributes()
    {
        $data = '';
        // яызык по умолчанию
        $languageId = $this->session->get('language/default/id');
        $query = new GDb_Query();
        $sql = 'SELECT `c`.*,`cl` .*,`cg`.`group_name`,`cm`.`module_id`,`cm`.`module_name` FROM `gear_controllers` `c` '
             . 'LEFT JOIN `gear_controllers_l` `cl` USING(`controller_id`) '
               // группа компонентов
             . 'LEFT JOIN (SELECT `g`.*,`l`.`group_name`,`l`.`group_about` FROM `gear_controller_groups` `g` JOIN `gear_controller_groups_l` `l` USING(`group_id`) '
             . 'WHERE `l`.`language_id`=' . $this->language->get('id') . ') `cg` USING (`group_id`) '
               // модули
             . 'LEFT JOIN `gear_modules` `cm` ON `cm`.`module_id`=`c`.`module_id` '
             . 'WHERE `cl`.`language_id`=' . $languageId . ' AND `c`.`controller_id`=' . $this->uri->id;
        if (($cmp = $query->getRecord($sql)) === false)
            throw new GSqlException();
        $this->_title = $cmp['controller_name'];
        // формирование атрибутов
        // Компонент
        $path = '';
        $data = '<fieldset class="fixed"><label>' . $this->_['title_fieldset_common'] . '</label><ul>';
        $data .= '<li><img src="' . PATH_COMPONENT . $cmp['controller_uri'] . 'resources/shortcut.png"/></li>';
        $data .= '<li><label>' . $this->_['label_controller_class'] . ':</label> ' . $cmp['controller_class'] . '</li>';
        $data .= '<li><label>' . $this->_['label_group_id'] . ':</label> ' 
               . '<a type="widget" url="system/components/groups/' . ROUTER_DELIMITER . 'profile/interface/' . $cmp['group_id'] . '?state=info" href="#">' . $cmp['group_name'] . '</a></li>';
        $data .= '<li><label>' . $this->_['label_module_name'] . ':</label> ' 
               . '<a type="widget" url="system/components/modules/' . ROUTER_DELIMITER . 'profile/interface/' . $cmp['module_id'] . '?state=info" href="#">' . $cmp['module_name'] . '</a></li>';
        $data .= '</fieldset>';
        // Ресурс
        $data .= '<fieldset class="fixed"><label>' . $this->_['title_fieldset_path'] . '</label><ul>';
        $data .= '<li><label>' . $this->_['label_controller_uri_pkg'] . ':</label> ' . $cmp['controller_uri_pkg'] . '</li>';
        $data .= '<li><label>' . $this->_['label_controller_uri'] . ':</label> ' . $cmp['controller_uri'] . '</li>';
        $data .= '<li><label>' . $this->_['label_controller_uri_action'] . ':</label> ' . $cmp['controller_uri_action'] . '</li>';
        $data .= '</fieldset>';
        // Интерфейс
        $data .= '<fieldset class="fixed"><label>' . $this->_['title_fieldset_interface'] . '</label><ul>';
        $data .= '<li><label>' . $this->_['label_controller_enabled'] . ':</label> <span type="chbox" mn:value="' . $cmp['controller_enabled'] . '"></span></li>';
        $data .= '<li><label>' . $this->_['label_controller_visible'] . ':</label> <span type="chbox" mn:value="' . $cmp['controller_visible'] . '"></span></li>';
        $data .= '<li><label>' . $this->_['label_controller_dashboard'] . ':</label> <span type="chbox" mn:value="' . $cmp['controller_dashboard'] . '"></span></li>';
        $data .= '</fieldset>';
        // События
        $json = json_decode($cmp['controller_privileges'], true);
        if ($json) {
            $data .= '<fieldset class="fixed"><label>' . $this->_['title_fieldset_privileges'] . '</label><ul><li>';
            $data .= GPrivileges::toString($json, ', ');
            $data .= '<li></ul></fieldset>';
        }
        // Поля
        $json = json_decode($cmp['controller_fields'], true);
        $res = array();
        for ($i = 0; $i < sizeof($json); $i++) {
            $res[] = $json[$i]['label'];
        }
        if ($res) {
            $data .= '<fieldset class="fixed"><label>' . $this->_['title_fieldset_fields'] . '</label><ul><li>';
            $data .= implode(', ', $res);
            $data .= '<li></ul></fieldset>';
        }

        $data .= '<div class="wrap"></div>';
        $data = '<div class="mn-row-body border">' . $data . '<div class="wrap"></div></div>';

        return $data;
    }
}
?>