<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка компонентов"
 * Пакет контроллеров "Список компонентов"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Controller_YControllers_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные списка компонентов
 * 
 * @category   Gear
 * @package    Controller_YControllers_Grid
 * @subpackage Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YControllers_Grid_Data extends GController_Grid_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * (для обработки записей таблицы)
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'controller_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_controllers';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS `c`.*,`cg`.`group_name`,`cm`.`module_name`, `cl`.`controller_name`, `cl`.`controller_about` '
          . 'FROM `gear_controllers` `c` '
          . 'JOIN `gear_controllers_l` `cl` USING(`controller_id`) '
            // группа компонентов
          . 'LEFT JOIN (SELECT `g`.*,`l`.`group_name`,`l`.`group_about` FROM `gear_controller_groups` `g` JOIN `gear_controller_groups_l` `l` USING(`group_id`) '
          . 'WHERE `l`.`language_id`=' . $this->language->get('id') . ') `cg` USING (`group_id`) '
            // модули
          . 'LEFT JOIN `gear_modules` `cm` ON `cm`.`module_id`=`c`.`module_id` '
          . 'WHERE `cl`.`language_id`=' . $this->language->get('id') . ' %filter %fastfilter '
          . 'ORDER BY %sort '
          . 'LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            'controller_name' => array('type' => 'string'),
            'group_name' => array('type' => 'string'),
            'controller_path' => array('type' => 'string'),
            'controller_about' => array('type' => 'string'),
            'controller_uri' => array('type' => 'string'),
            'controller_uri_pkg' => array('type' => 'string'),
            'controller_uri_action' => array('type' => 'string'),
            'controller_class' => array('type' => 'string'),
            'controller_enabled' => array('type' => 'string'),
            'controller_visible' => array('type' => 'string'),
            'controller_dashboard' => array('type' => 'string'),
            'controller_clear' => array('type' => 'string'),
            'controller_statistics' => array('type' => 'string'),
            'controller_menu' => array('type' => 'string'),
            'controller_menu_config' => array('type' => 'integer'),
            'module_name' => array('type' => 'string')
        );
    }

    /**
     * Возращает sql запрос для быстрого фильтра
     * (для подстановки "%fastfilter" в $sql)
     * 
     * @param string $key идендификатор поля
     * @param string $value значение
     * @return string
     */
    protected function getTreeFilter($key, $value)
    {
        // идендификатор поля
        switch ($key) {
            // по дате
            case 'byDt':
                $toDate = date('Y-m-d');
                switch ($value) {
                    case 'day':
                        return ' AND `c`.`sys_date_insert` BETWEEN "' . $toDate . '" AND "' . $toDate . '"';

                    case 'week':
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d") - 7, date("Y")));
                        return ' AND `c`.`sys_date_insert` BETWEEN "' . $fromDate . '" AND "' . $toDate . '"';

                    case 'month':
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, date("m")-1, date("d"), date("Y")));
                        return ' AND `c`.`sys_date_insert` BETWEEN "' . $fromDate . '" AND "' . $toDate . '"';
                }
                break;

            // по группе компонента
            case 'byGr':
                return ' AND `c`.`group_id`="' . $value . '" ';

            // по модулю
            case 'byMd':
                return ' AND `c`.`module_id`="' . $value . '" ';

            // по очистке
            case 'byCl':
                return ' AND `controller_clear`=' . (int) $value;

            // по статистике
            case 'bySt':
                return ' AND `controller_statistics`=' . (int) $value;

            // по доступности
            case 'byEn':
                return ' AND `controller_enabled`=' . (int) $value;

            // по видимости
            case 'byVs':
                return ' AND `controller_visible`=' . (int) $value;

            // на доске
            case 'byDs':
                return ' AND `controller_dashboard`=' . (int) $value;
        }

        return '';
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        $query = new GDb_Query();
        // удаление прав доступа ("gear_controller_access")
        $sql = 'DELETE FROM `gear_controller_access` WHERE `controller_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление языков контроллера ("gear_controllers_l")
        $sql = 'DELETE FROM `gear_controllers_l` WHERE `controller_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        //  удаление прав доступа ("gear_controller_access")
        $query = new GDb_Query();
        $sql = 'DELETE `a` FROM `gear_controllers` `c`, `gear_controller_access` `a` '
             . 'WHERE `c`.`controller_id`=`a`.`controller_id` AND '
             . '`c`.`sys_record`=0 ';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_controller_access` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        //  удаление языков контроллера ("gear_controllers_l")
        $query = new GDb_Query();
        $sql = 'DELETE `a` FROM `gear_controllers` `c`, `gear_controllers_l` `a` '
             . 'WHERE `c`.`controller_id`=`a`.`controller_id` AND '
             . '`c`.`sys_record`=0 ';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_controllers_l` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление контроллеров ("gear_controllers")
        $query = new GDb_Query();
        $sql = 'DELETE FROM `gear_controllers` WHERE `sys_record`=0';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_controllers` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        $record['controller_name'] = '<img align="absmiddle" style="margin-right:5px;" src="' . PATH_COMPONENT . $record['controller_uri']. 'resources/icon.png">' . $record['controller_name'];

        return $record;
    }
}
?>