<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка компонентов"
 * Пакет контроллеров "Список компонентов"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Controller_YControllers_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс списка компонентов
 * 
 * @category   Gear
 * @package    Controller_YControllers_Grid
 * @subpackage Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YControllers_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * (для обработки записей таблицы)
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'controller_id';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'controller_name';

    /**
     * Использовать быстрый фильтр
     *
     * @var boolean
     */
    public $useSlidePanel = true;

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('name' => 'controller_name', 'type' => 'string'),
            array('name' => 'group_name', 'replacer' => 'group_id', 'type' => 'string'),
            array('name' => 'controller_path', 'type' => 'string'),
            array('name' => 'controller_about', 'type' => 'string'),
            array('name' => 'controller_uri', 'type' => 'string'),
            array('name' => 'controller_uri_pkg', 'type' => 'string'),
            array('name' => 'controller_uri_action', 'type' => 'string'),
            array('name' => 'controller_class', 'type' => 'string'),
            array('name' => 'controller_enabled', 'type' => 'string'),
            array('name' => 'controller_visible', 'type' => 'string'),
            array('name' => 'controller_dashboard', 'type' => 'string'),
            array('name' => 'controller_clear', 'type' => 'string'),
            array('name' => 'controller_statistics', 'type' => 'string'),
            array('name' => 'controller_menu', 'type' => 'string'),
            array('name' => 'controller_menu_config', 'type' => 'integer'),
            array('name' => 'module_name', 'type' => 'string')
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $this->columns = array(
            array('xtype'     => 'expandercolumn',
                  'url'       => $this->componentUrl . 'grid/expand/',
                  'typeCt'    => 'row'),
            array('xtype'     => 'numbercolumn'),
            array('xtype'     => 'rowmenu'),
            array('dataIndex' => 'controller_id',
                  'header'    => '<em class="mn-grid-hd-id"></em> ID',
                  'hidden'    => true,
                  'width'     => 60,
                  'sortable'  => true,
                  'filter'    => array('type' => 'numeric')),
            array('dataIndex' => 'controller_name',
                  'header'    => '<em class="mn-grid-hd-details"></em>'
                               . $this->_['header_controller_name'],
                  'width'     => 140,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'group_name',
                  'header'    => $this->_['header_group_name'],
                  'tooltip'   => $this->_['tooltip_group_name'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'module_name',
                  'header'    => $this->_['header_module_name'],
                  'tooltip'   => $this->_['tooltip_module_name'],
                  'hidden'    => true,
                  'width'     => 140,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'controller_about',
                  'header'    => $this->_['header_controller_about'],
                  'width'     => 160,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'controller_class',
                  'header'    => $this->_['header_controller_class'],
                  'width'     => 150,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'controller_uri_pkg',
                  'header'    => $this->_['header_controller_uri_pkg'],
                  'width'     => 190,
                  'hidden'    => true,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'controller_uri',
                  'header'    => $this->_['header_controller_uri'],
                  'width'     => 190,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'controller_uri_action',
                  'header'    => $this->_['header_controller_uri_action'],
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('xtype'     => 'booleancolumn',
                  'cls'       => 'mn-bg-color-gray2',
                  'dataIndex' => 'controller_enabled',
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-enabled.png" align="absmiddle">',
                  'tooltip'   => $this->_['header_controller_enabled'],
                  'align'     => 'center',
                  'width'     => 45,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean')),
            array('xtype'     => 'booleancolumn',
                  'cls'       => 'mn-bg-color-gray2',
                  'dataIndex' => 'controller_visible',
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-visible.png" align="absmiddle">',
                  'tooltip'   => $this->_['header_controller_visible'],
                  'align'     => 'center',
                  'width'     => 45,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean')),
            array('xtype'     => 'booleancolumn',
                  'cls'       => 'mn-bg-color-gray2',
                  'dataIndex' => 'controller_dashboard',
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-desk.png" align="absmiddle">',
                  'tooltip'   => $this->_['tooltip_controller_board'],
                  'align'     => 'center',
                  'width'     => 45,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean')),
            array('xtype'     => 'booleancolumn',
                  'cls'       => 'mn-bg-color-gray2',
                  'dataIndex' => 'controller_clear',
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-clear.png" align="absmiddle">',
                  'tooltip'   => $this->_['tooltip_controller_clear'],
                  'align'     => 'center',
                  'width'     => 45,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean')),
            array('xtype'     => 'booleancolumn',
                  'cls'       => 'mn-bg-color-gray2',
                  'dataIndex' => 'controller_statistics',
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-chart.png" align="absmiddle">',
                  'tooltip'   => $this->_['tooltip_controller_statistics'],
                  'align'     => 'center',
                  'width'     => 45,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean')),
            array('xtype'     => 'booleancolumn',
                  'cls'       => 'mn-bg-color-gray2',
                  'dataIndex' => 'controller_menu',
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-menu.png" align="absmiddle">',
                  'tooltip'   => $this->_['tooltip_controller_menu'],
                  'align'     => 'center',
                  'width'     => 45,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean')),
            array('xtype'     => 'booleancolumn',
                  'cls'       => 'mn-bg-color-gray2',
                  'dataIndex' => 'controller_menu_config',
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-menu-config.png" align="absmiddle">',
                  'tooltip'   => $this->_['tooltip_controller_menu_config'],
                  'align'     => 'center',
                  'width'     => 45,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean'))
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_grid'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_grid'],
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'profileUrl'    => $this->componentUrl . 'profile/')
        );

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "edit" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "добавить" (ExtJS class "Manager.button.InsertData")
        $group->items->add(array('xtype' => 'mn-btn-insert-data', 'gridId' => $this->classId));
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' => $this->classId));
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'      => 'mn-btn-clear-data',
                  'msgConfirm' => $this->_['msg_btn_clear'],
                  'gridId'     => $this->classId,
                  'isN'        => true)
        );
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "установка" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'   => 'mn-btn-widget',
                  'text'    => $this->_['text_btn_install'],
                  'tooltip' => $this->_['tooltip_btn_install'],
                  'icon'    => $this->resourcePath . 'icon-btn-install.png',
                  'url'     => $this->componentUri . '../install/' . ROUTER_DELIMITER . 'upload/interface/')
        );
        // кнопка "упаковка" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'      => 'mn-btn-widget-grid',
                  'text'       => $this->_['text_btn_pack'],
                  'tooltip'    => $this->_['tooltip_btn_pack'],
                  'msgWarning'    => $this->_['msg_btn_pack_select'],
                  //'msgWarningRec' => $this->_['msg_btn_pack_selects'],
                  'selOneRecord'  => false,
                  'icon'       => $this->resourcePath . 'icon-btn-pack.png',
                  'url'        => $this->componentUrl . 'pack/data/',
                  'gridId'     => $this->classId)
        );
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = 'component-components';
        $this->_cmp->tbar->items->add($group);

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $cellInfo =
            '<div class="mn-grid-cell-tooltip-tl">{controller_name}</div>'
          . '<div class="mn-grid-cell-tooltip" style="width:400px">'
          . '<img src="' . PATH_COMPONENT . '{controller_uri}/resources/shortcut.png" width="60px" height="60px">'
          . '<em>' . $this->_['header_group_name'] . '</em>: <b>{group_name}</b><br>'
          . '<em>' . $this->_['header_controller_uri_pkg'] . '</em>: <b>{controller_uri_pkg}</b><br>'
          . '<em>' . $this->_['header_controller_uri'] . '</em>: <b>{controller_uri}</b><br>'
          . '<em>' . $this->_['header_controller_uri_action'] . '</em>: <b>{controller_uri_action}</b><br>'
          . '<em>' . $this->_['header_controller_class'] . '</em>: <b>{controller_class}</b>'
          . '</div>';
        $this->_cmp->cellTips = array(
            array('field' => 'controller_name', 'tpl' => $cellInfo),
            array('field' => 'group_name', 'tpl' => '{group_name}'),
            array('field' => 'controller_about', 'tpl' => '{controller_about}'),
            array('field' => 'controller_class', 'tpl' => '{controller_class}'),
            array('field' => 'controller_uri_pkg', 'tpl' => '{controller_uri_pkg}'),
            array('field' => 'controller_uri', 'tpl' => '{controller_uri}'),
            array('field' => 'controller_uri_action', 'tpl' => '{controller_uri_action}'),
            array('field' => 'module_name', 'tpl' => '{module_name}')
        );
        $this->_cmp->cellTipConfig = array('maxWidth' => 400);

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $this->_cmp->rowMenu = array(
            'xtype' => 'mn-grid-rowmenu',
            'items' => array(
                array('text'    => $this->_['rowmenu_edit'],
                      'icon'    => $this->resourcePath . 'icon-item-edit.png',
                      'url'     => $this->componentUrl . 'profile/interface/'),
                array('xtype'   => 'menuseparator'),
                array('text'    => $this->_['rowmenu_translate'],
                      'icon'    => $this->resourcePath . 'icon-item-text.png',
                      'url'     => $this->componentUrl . 'translation/interface/')
            )
        );

        // быстрый фильтр списка (ExtJs class "Manager.tree.GridFilter")
        $this->_slidePanel->cls = 'mn-tree-gridfilter';
        $this->_slidePanel->width = 250;
        $this->_slidePanel->initRoot = array(
            'text'     => 'Filter',
            'id'       => 'byRoot',
            'expanded' => true,
            'children' => array(
                array('text'     => $this->_['text_all_records'],
                      'value'    => 'all',
                      'expanded' => true,
                      'leaf'     => false,
                      'children' => array(
                        array('text'     => $this->_['text_by_date'],
                              'id'       => 'byDt',
                              'leaf'     => false,
                              'expanded' => true,
                              'children' => array(
                                  array('text'     => $this->_['text_by_day'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 'day'),
                                  array('text'     => $this->_['text_by_week'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true, 
                                        'value'    => 'week'),
                                  array('text'     => $this->_['text_by_month'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 'month')
                              )
                        ),
                        array('text'    => $this->_['text_by_group'],
                              'id'      => 'byGr',
                              'leaf'    => false),
                        array('text'    => $this->_['text_by_module'],
                              'id'      => 'byMd',
                              'leaf'    => false),
                        array('text'     => $this->_['text_by_clear'],
                              'id'       => 'byCl',
                              'leaf'     => false,
                              'expanded' => true,
                              'children' => array(
                                  array('text'     => $this->_['text_by_clear_0'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 1),
                                  array('text'     => $this->_['text_by_clear_1'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 0)
                              )
                        ),
                        array('text'     => $this->_['text_by_statistics'],
                               'id'      => 'bySt',
                              'leaf'     => false,
                              'expanded' => true,
                              'children' => array(
                                  array('text'     => $this->_['text_by_statistics_0'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 0),
                                  array('text'     => $this->_['text_by_statistics_1'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 1)
                              )
                        ),
                        array('text'    => $this->_['text_by_enable'],
                              'id'      => 'byEn',
                              'leaf'    => false,
                              'expanded' => true,
                              'children' => array(
                                  array('text'     => $this->_['text_by_enable_0'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 0),
                                  array('text'     => $this->_['text_by_enable_1'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 1)
                              )
                        ),
                        array('text'     => $this->_['text_by_visible'],
                              'id'      => 'byVs',
                              'leaf'     => false,
                              'expanded' => true,
                              'children' => array(
                                  array('text'     => $this->_['text_by_visible_0'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 0),
                                  array('text'     => $this->_['text_by_visible_1'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 1)
                              )
                        ),
                        array('text'     => $this->_['text_by_dashboard'],
                              'id'      => 'byDs',
                              'leaf'     => false,
                              'expanded' => true,
                              'children' => array(
                                  array('text'     => $this->_['text_by_dashboard_0'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 0),
                                  array('text'     => $this->_['text_by_dashboard_1'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 1)
                              )
                        )
                    )
                )
            )
        );

        parent::getInterface();
    }
}
?>