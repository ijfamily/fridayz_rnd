<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'        => 'Компоненты',
    'tooltip_grid'      => 'список компонентов системы',
    'rowmenu_edit'      => 'Редактировать',
    'rowmenu_translate' => 'Текст',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">("Компоненты системы")</span> ?',
    'text_btn_install'         => 'Установка',
    'tooltip_btn_install'      => 'Установка компонентов',
    'text_btn_pack'             => 'Упаковка',
    'tooltip_btn_pack'          => 'Упаковка компанентов в установочный пакет',
    'msg_btn_pack_select'       => 'Для упаковки компонентов Вам необходимо выделить записи!',
    // столбцы
    'header_controller_name'        => 'Название',
    'header_group_name'             => 'Группа',
    'tooltip_group_name'            => 'Группа компонентов',
    'header_module_name'            => 'Модуль',
    'tooltip_module_name'           => 'Модуль системы',
    'header_controller_about'       => 'Описание',
    'header_controller_class'       => 'ID класса',
    'header_controller_uri_pkg'     => 'Компонент',
    'tooltip_controller_uri_pkg'    => 'Путь к каталогу компонента',
    'header_controller_uri'         => 'Контроллер',
    'tooltip_controller_uri'        => 'Путь к каталогу контроллера',
    'header_controller_uri_action'  => 'Интерфейс',
    'tooltip_controller_uri_action' => 'Интерфейс контроллера',
    'header_controller_enabled'  => 'Доступный',
    'header_controller_visible'  => 'Видимый',
    'header_controller_board'    => 'На доске',
    'tooltip_controller_board'   => 'Присутствие компонента на доске компонентов',
    'header_controller_clear'    => 'Очистка',
    'tooltip_controller_clear'   => 'Выполнять очистку (удаление всех) данных компонента',
    'tooltip_controller_statistics' => 'Данные компонента участвуют в статистике',
    'tooltip_controller_menu'       => 'В главном меню (пункт &laquo;Добавить&raquo;)',
    // типы
    'data_boolean' => array('Нет', 'Да'),
    // развернутая запись
    'header_attr'                => 'Атрибуты компонента',
    'title_fieldset_common'      => 'Компонент',
    'label_controller_class'     => 'ID класса',
    'label_group_id'             => 'Группа',
    'label_module_name'          => 'Модуль',
    'label_controller_clear'     => 'Очищать',
    'title_fieldset_path'        => 'Ресурс',
    'title_fieldset_interface'   => 'Интерфейс',
    'label_controller_uri_pkg'   => 'Компонент',
    'label_controller_uri'       => 'Контроллер',
    'label_controller_uri_action' => 'Интерфейс',
    'label_controller_enabled'   => 'Доступный',
    'label_controller_visible'   => 'Видимый',
    'label_controller_dashboard' => 'На доске',
    'title_fieldset_privileges'  => 'Допустимые права',
    'title_fieldset_fields'      => 'Права на поля',
    // быстрый фильтр
    'text_all_records'     => 'Все компоненты',
    'text_by_date'         => 'По дате',
    'text_by_day'          => 'За день',
    'text_by_week'         => 'За неделю',
    'text_by_month'        => 'За месяц',
    'text_by_module'       => 'По модулю',
    'text_by_group'        => 'По группе',
    'text_by_clear'        => 'По очистке',
    'text_by_clear_1'      => 'С очисткой данных',
    'text_by_clear_0'      => 'Без очистики данных',
    'text_by_statistics'   => 'По статистике',
    'text_by_statistics_1' => 'С учётом статистики',
    'text_by_statistics_0' => 'Без учёта статистики',
    'text_by_enable'       => 'По доступности',
    'text_by_enable_1'     => 'Доступный',
    'text_by_enable_0'     => 'Не доступный',
    'text_by_visible'      => 'По видимости',
    'text_by_visible_1'    => 'Видимый',
    'text_by_visible_0'    => 'Невидимый',
    'text_by_dashboard'    => 'На доске',
    'text_by_dashboard_1'  => 'Присутствует',
    'text_by_dashboard_0'  => 'Отсутсвует'
);
?>