<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'        => 'Компоненти',
    'tooltip_grid'      => 'список компонентов системы',
    'rowmenu_edit'      => 'Редагувати',
    'rowmenu_translate' => 'Транслювання',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Стовпці',
    'title_buttongroup_filter' => 'Фільтр',
    'msg_btn_clear'            => 'Ви дійсно бажаєте видалити всі записи <span class="mn-msg-delete">("Компоненти системи")</span> ?',
    'text_btn_install'         => 'Установка',
    'tooltip_btn_install'      => 'Установка компонентов',
    'text_btn_pack'             => 'Упаковка',
    'tooltip_btn_pack'          => 'Упаковка компанентов в установочный пакет',
    'msg_btn_pack_select'       => 'Для упаковки компонентов Вам необходимо выделить записи!',
    // столбцы
    'header_controller_name'        => 'Назва',
    'header_group_name'             => 'Група',
    'tooltip_group_name'            => 'Група компонентів',
    'header_module_name'            => 'Модуль',
    'tooltip_module_name'           => 'Модуль системи',
    'header_controller_about'       => 'Опис',
    'header_controller_class'       => 'ID класу',
    'header_controller_uri_pkg'     => 'Компонент',
    'tooltip_controller_uri_pkg'    => 'Шлях до каталогу компонента',
    'header_controller_uri'         => 'Контролер',
    'tooltip_controller_uri'        => 'Шлях до каталогу контролера',
    'header_controller_uri_action'  => 'Iнтерфейс',
    'tooltip_controller_uri_action' => 'Iнтерфейс контролера',
    'header_controller_enabled'  => 'Доступний',
    'header_controller_visible'  => 'Відомий',
    'header_controller_board'    => 'На дошці',
    'tooltip_controller_board'   => 'Присутність компонента на дошці компонентів',
    'header_controller_clear'    => 'Очищення',
    'tooltip_controller_clear'   => 'Виконувати очищення (видалення всіх) даних компонента',
    'tooltip_controller_statistics' => 'Дані компонента беруть участь в статистиці',
    'tooltip_controller_menu'       => 'В главном меню (пункт &laquo;Добавить&raquo;)',
    // типы
    'data_boolean' => array('Нi', 'Так'),
    // развернутая запись
    'header_attr'                => 'Атрибути компонента',
    'title_fieldset_common'      => 'Компонент',
    'label_controller_class'     => 'ID класу',
    'label_group_id'             => 'Група',
    'label_module_name'          => 'Модуль',
    'label_controller_clear'     => 'Очищати',
    'title_fieldset_path'        => 'Ресурс',
    'title_fieldset_interface'   => 'Iнтерфейс',
    'label_controller_uri_pkg'   => 'Компонент',
    'label_controller_uri'       => 'Контролер',
    'label_controller_uri_action' => 'Iнтерфейс',
    'label_controller_enabled'   => 'Доступний',
    'label_controller_visible'   => 'Відомий',
    'label_controller_dashboard' => 'На дошці',
    'title_fieldset_privileges'  => 'Допустимі права',
    'title_fieldset_fields'      => 'Права на поля',
    // быстрый фильтр
    'text_all_records'     => 'Все компоненты',
    'text_by_date'         => 'По дате',
    'text_by_day'          => 'За день',
    'text_by_week'         => 'За неделю',
    'text_by_month'        => 'За месяц',
    'text_by_module'       => 'По модулю',
    'text_by_group'        => 'По группе',
    'text_by_clear'        => 'По очистке',
    'text_by_clear_1'      => 'С очисткой данных',
    'text_by_clear_0'      => 'Без очистики данных',
    'text_by_statistics'   => 'По статистике',
    'text_by_statistics_1' => 'С учётом статистики',
    'text_by_statistics_0' => 'Без учёта статистики',
    'text_by_enable'       => 'По доступности',
    'text_by_enable_1'     => 'Доступный',
    'text_by_enable_0'     => 'Не доступный',
    'text_by_visible'      => 'По видимости',
    'text_by_visible_1'    => 'Видимый',
    'text_by_visible_0'    => 'Невидимый',
    'text_by_dashboard'    => 'На доске',
    'text_by_dashboard_1'  => 'Присутствует',
    'text_by_dashboard_0'  => 'Отсутсвует'

);
?>