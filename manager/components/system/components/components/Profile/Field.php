<?php
/**
 * Gear Manager
 *
 * Контроллер         "Изменение записи профиля компонента"
 * Пакет контроллеров "Компоненты"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Controller_YControllers_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Field.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Field');

/**
 * Изменение записи профиля компонента
 * 
 * @category   Gear
 * @package    Controller_YControllers_Profile
 * @subpackage Field
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Field.php 2014-08-04 12:00:00 Gear Magic $
 */
final class GController_YControllers_Profile_Field extends GController_Profile_Field
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array(
        'controller_enabled', 'controller_visible', 'controller_clear', 'controller_dashboard', 'controller_statistics',
        'controller_menu', 'controller_menu_config'
    );

    /**
     * Первичное поле таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'controller_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_controllers';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ycontrollers_grid';
}
?>