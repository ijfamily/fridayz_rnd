<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля компонента"
 * Пакет контроллеров "Компоненты"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Controller_YControllers_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные профиля компонента
 * 
 * @category   Gear
 * @package    Controller_YControllers_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YControllers_Profile_Data extends GController_Profile_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Массив полей таблицы ($_tableName)
     *
     * @var array
     */
    public $fields = array(
        'controller_uri_pkg', 'controller_uri', 'controller_uri_action', 'controller_uri_menu',
        'module_id', 'group_id', 'controller_privileges', 'controller_enabled', 'controller_visible',
        'controller_class', 'controller_clear', 'controller_menu', 'controller_menu_config', 'controller_fields', 'controller_dashboard',
        'controller_statistics', 'controller_statistics_table'
    );

    /**
     * Первичный ключ таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'controller_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_controllers';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ycontrollers_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_profile_update'], $record['controller_name']);
    }

    /**
     * Вставка данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataInsert($params = array())
    {
        parent::dataInsert($params);

        // добавление записи описания контроллера
        $data = $this->input->getBy(array('controller_name', 'controller_about', 'controller_profile'));
        $data['controller_id'] = $this->_recordId;
        $table = new GDb_Table('gear_controllers_l', 'controller_lang_id');
        $langs = $this->session->get('language/list');
        $count = count($langs);
        for ($i = 0; $i < $count; $i++) {
            $data['language_id'] = $langs[$i]['language_id'];
            $table->insert($data);
        }
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        $sql = 'SELECT `c`.*, `cl`.`controller_name`, `cl`.`controller_about`, `cl`.`controller_profile` '
             . 'FROM `gear_controllers` `c` JOIN `gear_controllers_l` `cl` USING(`controller_id`) '
             . 'WHERE `cl`.`language_id`=' . $this->language->get('id') . ' AND `c`.`controller_id`=%id%';

        parent::dataView($sql);

        unset($this->response->data['group_id']);
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        $query = new GDb_Query();
        // удаление прав доступа ("gear_controller_access")
        $sql = 'DELETE FROM `gear_controller_access` WHERE `controller_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление языков контроллера ("gear_controllers_l")
        $sql = 'DELETE FROM `gear_controllers_l` WHERE `controller_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        // если изменения привилегии
        if (!empty($params['controller_privileges'])) {
            $pr = json_decode($params['controller_privileges'], true);
            $params['controller_privileges'] = GPrivileges::toJson($pr);
        }
        // состояние профиля "правка"
        if ($this->isUpdate) {
            if (isset($params['group_id']))
                if (!is_numeric($params['group_id']))
                    unset($params['group_id']);
        }

        return true;
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        if (!empty($record['controller_privileges'])) {
            $pr = json_decode($record['controller_privileges'], true);
            $record['controller_privileges'] = GPrivileges::toJson($pr, true);
        }

        return $record;
    }
}
?>