<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Creating a record "Component"',
    'title_profile_update' => 'Updating entry "%s"',
    // поля
    'title_tab_fields'          => 'Fields',
    'title_tab_events'          => 'Events',
    'title_tab_common'          => 'Attributes',
    'label_module_name'         => 'Module',
    'title_fieldset_path'       => 'Resource',
    'label_group_id'            => 'Group',
    'label_controller_uri_pkg'  => 'Component',
    'tip_controller_uri_pkg'    => 'The directory path component (path/.../component dir/)',
    'label_controller_about'    => 'Description',
    'label_controller_class'    => 'Class Id',
    'tip_controller_class'      => 'The Id of the class that will use all controllers component',
    'label_controller_name'     => 'Name',
    'label_controller_uri'      => 'Controller',
    'tip_controller_uri'        => 'The directory path controller (path/.../component dir/controller dir)',
    'label_controller_uri_action' => 'Interface',
    'tip_controller_uri_action'   => 'Controller name / Controller interface /',
    'label_controller_iconsh'   => 'Icon',
    'label_controller_enabled'  => 'Eanbled',
    'label_controller_visible'  => 'Visible',
    'label_controller_clear'    => 'Clean',
    'title_fieldset_interface'  => 'Interface',
    'empty_controller_name'     => 'Component name',
    'header_label'              => 'Name',
    'header_action'             => 'Event',
    'header_field'              => 'Field',
    'label_controller_dashboard' => 'On dashboard',
    'tip_controller_dashboard'   => 'The presence of a component on the board components',
    'tip_controller_enabled'    => 'Availability component from the component',
    'tip_controller_visible'    => 'Visibility component in the list of components',
    'tip_controller_clear'      => 'Perform cleanup (delete all) component data',
    'label_controller_statistics' => 'Statistics',
    'tip_controller_statistics'   => 'These components are involved in statistics',
    'title_tab_source'            => 'Source',
    'title_tab_settings'          => 'Settings',
    'title_fieldset_statistic'    => 'Statistics',
    'label_controller_statistics_table' => 'Table',
    // типы
    'data_array' => array(
         array('label' => 'Read', 'action' => 'select'),
         array('label' => 'Insert', 'action' => 'insert'),
         array('label' => 'Update', 'action' => 'update'),
         array('label' => 'Delete', 'action' => 'delete'),
         array('label' => 'Clear', 'action' => 'clear'),
         array('label' => 'Export', 'action' => 'export'),
         array('label' => 'Full access', 'action' => 'root')
    )
);
?>