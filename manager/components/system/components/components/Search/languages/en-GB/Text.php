<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Search in the list "Components"',
    // столбцы
    'header_controller_name'       => 'Name',
    'header_group_name'            => 'Group',
    'header_module_name'           => 'Module',
    'tooltip_module_name'          => 'System module',
    'header_controller_about'      => 'Description',
    'header_controller_class'      => 'Class Id',
    'header_controller_uri_pkg'    => 'Component',
    'header_controller_uri'        => 'Controller',
    'header_controller_uri_action' => 'Interface',
    'header_controller_enabled'    => 'Enabled',
    'header_controller_visible'    => 'Visible',
    'header_controller_board'      => 'On dashboard',
    'header_controller_clear'      => 'Clear'
);
?>