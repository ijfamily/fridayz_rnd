<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // сообщения
    'msg_pack_title' => 'Упаковка компонентов',
    'msg_pack'       => 'Компоненты были успешно упакованы<br>скачать: <a href="%s" target="_blank">%s</a>',
    // сообщения
    'msg_log_pack'              => 'Упаковка компонентов "%s" в установочный файл "%s"',
    'msg_component_not_exists'  => 'Выбранный Вами компонент не существует!',
);
?>