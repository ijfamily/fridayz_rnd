<?php
/**
 * Gear Manager
 *
 * Контроллер         "Упаковка компонентов"
 * Пакет контроллеров "Список компонентов"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YControllers_Pack
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('File');

/**
 * Упаковка компонентов
 * 
 * @category   Gear
 * @package    GController_YControllers_Pack
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YControllers_Pack_Data extends GController
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ycontrollers_grid';

    /**
     * Каталог экспорта данных
     *
     * @var string
     */
    protected $_pathData = 'data/export/';

    /**
     * Доступ на удаление всех данных
     * 
     * @return void
     */
    protected function dataAccessPack()
    {
        // проверка доступа к контроллеру класса
        if ($this->checkPrivilege)
            if (!$this->store->hasPrivilege(array('root'))) {
                $this->response->add('icon', 'icon-msq-access');
                throw new GException('Error access', 'No privileges to perform this action');
            }
        // соединение с базой данных
        GFactory::getDb()->connect();
        $this->response->set('action', 'pack');
    }

    /**
     * Добавление в журнал действий пользователей
     * (удаление данных)
     * 
     * @param array $params параметры журнала
     * @return void
     */
    protected function logPack($params = array())
    {
        // если invisible нечего не делать
        if ($this->session->set('invisible')) return;

        if ($params['log_query_params'])
            $params['log_query_params'] = json_encode($params['log_query_params']);
        $params['log_action'] = 'pack';
        $params['controller_id'] = $this->store->getFrom('params', 'id', 0, $this->accessId);
        $params['controller_class'] = $this->classId;
        GFactory::getDbDriver('log')->add($params);
    }

    /**
     * Упаковка компонента в JSON
     * 
     * @param array $cmp данны пакета
     * @return string
     */
    public function packCmpToJson($cmp)
    {
        $str = '{';
        $str .= '    "install": "component",' . "\r";
        $str .= '    "component": ['. "\r";
        $str .= "    {\r";
        $str .= '        "name": {' . "\r";
        // название
        $j = 0;
        foreach ($cmp['name'] as $key => $value) {
            $str .= '            "' . $key . '": "' . $value;
             if ($j < sizeof($cmp['name']) - 1)
                $str .= '",' . "\r";
             else
                $str .= '"' . "\r";
            $j++;
        }
        $str .= "        },\r";
        $str .= '        "description": {' . "\r";
        // описание
        $j = 0;
        foreach ($cmp['description'] as $key => $value) {
            $str .= '            "' . $key . '": "' . $value;
             if ($j < sizeof($cmp['description']) - 1)
                $str .= '",' . "\r";
             else
                $str .= '"' . "\r";
            $j++;
        }
        $str .= "        },\r";
        $str .= '        "class": "' . $cmp['class'] . '",' . "\r";
        $str .= '        "module": ' . $cmp['module'] . ',' . "\r";
        $str .= '        "group": ' . $cmp['group'] . ',' . "\r";
        $str .= '        "clear": ' . $cmp['clear'] . ',' . "\r";
        $str .= '        "statistics": ' . $cmp['statistics'] . ',' . "\r";
        $str .= '        "resource": {' . "\r";
        $str .= '            "package": "' . $cmp['resource']['package'] . '",' . "\r";
        $str .= '            "uri": "' . $cmp['resource']['uri'] . '",' . "\r";
        $str .= '            "action": "' . $cmp['resource']['action'] . '",' . "\r";
        $str .= '            "menu": "' . $cmp['resource']['menu'] . '"' . "\r";
        $str .= "        },\r";
        $str .= '        "interface": {' . "\r";
        $str .= '            "dashboard": ' . $cmp['interface']['dashboard'] . ',' . "\r";
        $str .= '            "enabled": ' . $cmp['interface']['enabled'] . ',' . "\r";
        $str .= '            "visible": ' . $cmp['interface']['visible'] . ',' . "\r";
        $str .= '            "menu": ' . $cmp['interface']['menu'] . "\r";
        $str .= "        },\r";
        $str .= '        "privileges": "' . $cmp['privileges'] . '",' . "\r";
        $str .= '        "userGroups": [' . "\r";
        $str .= '            { "group" : 1, "privileges" : "root" }' . "\r";
        $str .= "        ]\r";
        $str .= "    },\r";
        $str .= "    ]\r";
        $str .= '}';

        return $str;
    }

    /**
     * Упаковка компонента
     * 
     * @param string $id идент. компонентов
     * @return void
     */
    public function packComponent($id)
    {
        $query = new GDb_Query();
        // компонент
        $sql = 'SELECT `cl`.*, `c`.*, `ln`.`language_alias` FROM `gear_controllers` `c` '
             . 'JOIN `gear_controllers_l` `cl` USING(`controller_id`) '
             . 'JOIN `gear_languages` `ln` USING(`language_id`) '
             . 'WHERE `c`.`controller_id`=' . $id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->getCountRecords() == 0)
            throw new GException('Error', $this->_['msg_component_not_exists']);
        $component = array();
        $name = '';
        while (!$query->eof()) {
            $recCmp = $query->next();
            if (empty($component)) {
                if (empty($recCmp['controller_privileges']))
                     $privileges = '';
                else {
                    $json = json_decode($recCmp['controller_privileges'], true);
                    $count = sizeof($json);
                    $arr = array();
                    for ($i = 0; $i < $count; $i++) {
                        $arr[] = $json[$i]['action'];
                    }
                    $recCmp['controller_privileges'] = implode(',', $arr);
                }
                $component = array(
                    'name'        => array(),
                    'description' => array(),
                    'class'       => $recCmp['controller_class'],
                    'module'      => $recCmp['module_id'],
                    'group'       => $recCmp['group_id'],
                    'clear'       => (int) $recCmp['controller_clear'],
                    'statistics'  => (int) $recCmp['controller_statistics'],
                    'resource'    => array(
                        'package' => $recCmp['controller_uri_pkg'],
                        'uri'     => $recCmp['controller_uri'],
                        'action'  => $recCmp['controller_uri_action'],
                        'menu'    => $recCmp['controller_uri_menu']
                    ),
                    'interface'  => array(
                        'dashboard' => $recCmp['controller_dashboard'],
                        'enabled'   => $recCmp['controller_enabled'],
                        'visible'   => $recCmp['controller_visible'],
                        'menu'      => $recCmp['controller_menu']
                    ),
                    'privileges' => $recCmp['controller_privileges'],
                    'userGroups' => array(
                        array('group' => 1, 'privileges' => 'root')
                    )
                );
            }
            if ($recCmp['language_alias'] == 'ru-RU')
                $name = '<b>' . $recCmp['controller_name'] . '</b> ';
            $component['name'][$recCmp['language_alias']] = $recCmp['controller_name'];
            $component['description'][$recCmp['language_alias']] = $recCmp['controller_about'];
        }

        $text = $this->packCmpToJson($component);
        // название файла
        $filename = 'component.json';
        $path = $this->_pathData . $filename;
        // запись пака
        GFile::write($path, 'w', $text);
        // сообщение
        $this->response->setMsgResult($this->_['msg_pack_title'], sprintf($this->_['msg_pack'], $path, $filename), true);
        // запись в журнал действий пользователя
        $this->logPack(array(
            'log_query_id'     => $id,
            'log_query'        => sprintf($this->_['msg_log_pack'], $name, $filename),
            'log_query_params' => array(
                'filename'  => $path,
                'component' => $component['class']
            )
        ));
    }

    /**
     * Упаковка компонентов в JSON
     * 
     * @param array $cmp данны пакета
     * @return string
     */
    public function packCmpsToJson($cmps)
    {
        $str = '{';
        $str .= '    "install": "components",' . "\r";
        $str .= '    "components": ['. "\r";
        $i = 0;
        foreach ($cmps as $id => $cmp) {
            $str .= "    {\r";
            $str .= '        "name": {' . "\r";
            // название
            $j = 0;
            foreach ($cmp['name'] as $key => $value) {
                $str .= '            "' . $key . '": "' . $value;
                 if ($j < sizeof($cmp['name']) - 1)
                    $str .= '",' . "\r";
                 else
                    $str .= '"' . "\r";
                $j++;
            }
            $str .= "        },\r";
            $str .= '        "description": {' . "\r";
            // описание
            $j = 0;
            foreach ($cmp['description'] as $key => $value) {
                $str .= '            "' . $key . '": "' . $value;
                 if ($j < sizeof($cmp['description']) - 1)
                    $str .= '",' . "\r";
                 else
                    $str .= '"' . "\r";
                $j++;
            }
            $str .= "        },\r";
            $str .= '        "class": "' . $cmp['class'] . '",' . "\r";
            $str .= '        "module": ' . $cmp['module'] . ',' . "\r";
            $str .= '        "group": ' . $cmp['group'] . ',' . "\r";
            $str .= '        "clear": ' . $cmp['clear'] . ',' . "\r";
            $str .= '        "statistics": ' . $cmp['statistics'] . ',' . "\r";
            $str .= '        "resource": {' . "\r";
            $str .= '            "package": "' . $cmp['resource']['package'] . '",' . "\r";
            $str .= '            "uri": "' . $cmp['resource']['uri'] . '",' . "\r";
            $str .= '            "action": "' . $cmp['resource']['action'] . '",' . "\r";
            $str .= '            "menu": "' . $cmp['resource']['menu'] . '"' . "\r";
            $str .= "        },\r";
            $str .= '        "interface": {' . "\r";
            $str .= '            "dashboard": ' . $cmp['interface']['dashboard'] . ',' . "\r";
            $str .= '            "enabled": ' . $cmp['interface']['enabled'] . ',' . "\r";
            $str .= '            "visible": ' . $cmp['interface']['visible'] . ',' . "\r";
            $str .= '            "menu": ' . $cmp['interface']['menu'] . "\r";
            $str .= "        },\r";
            $str .= '        "privileges": "' . $cmp['privileges'] . '",' . "\r";
            $str .= '        "userGroups": [' . "\r";
            $str .= '            { "group" : 1, "privileges" : "root" }' . "\r";
            $str .= "        ]\r";
            if ($i < sizeof($cmps) - 1)
                $str .= "    },\r";
            else
                $str .= "    }\r";
            $i++;
        }
        $str .= "    ]\r";
        $str .= '}';

        return $str;
    }

    /**
     * Упаковка компонентов
     * 
     * @param string $id идент. компонентов
     * @return void
     */
    public function packComponents($id)
    {
        $query = new GDb_Query();
        // компонент
        $sql = 'SELECT `cl`.*, `c`.*, `ln`.`language_alias` FROM `gear_controllers` `c` '
             . 'JOIN `gear_controllers_l` `cl` USING(`controller_id`) '
             . 'JOIN `gear_languages` `ln` USING(`language_id`) '
             . 'WHERE `c`.`controller_id` IN (' . $id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->getCountRecords() == 0)
            throw new GException('Error', $this->_['msg_component_not_exists']);
        $components = array();
        $names = $classes = array();
        while (!$query->eof()) {
            $recCmp = $query->next();
            $cid = $recCmp['controller_id'];
            if (!isset($components[$cid])) {
                if (empty($recCmp['controller_privileges']))
                     $privileges = '';
                else {
                    $json = json_decode($recCmp['controller_privileges'], true);
                    $count = sizeof($json);
                    $arr = array();
                    for ($i = 0; $i < $count; $i++) {
                        $arr[] = $json[$i]['action'];
                    }
                    $recCmp['controller_privileges'] = implode(',', $arr);
                }
                $components[$cid] = array(
                    'name'        => array(),
                    'description' => array(),
                    'class'       => $recCmp['controller_class'],
                    'module'      => $recCmp['module_id'],
                    'group'       => $recCmp['group_id'],
                    'clear'       => (int) $recCmp['controller_clear'],
                    'statistics'  => (int) $recCmp['controller_statistics'],
                    'resource'    => array(
                        'package' => $recCmp['controller_uri_pkg'],
                        'uri'     => $recCmp['controller_uri'],
                        'action'  => $recCmp['controller_uri_action'],
                        'menu'    => $recCmp['controller_uri_menu']
                    ),
                    'interface'  => array(
                        'dashboard' => $recCmp['controller_dashboard'],
                        'enabled'   => $recCmp['controller_enabled'],
                        'visible'   => $recCmp['controller_visible'],
                        'menu'      => $recCmp['controller_menu']
                    ),
                    'privileges' => $recCmp['controller_privileges'],
                    'userGroups' => array(
                        array('group' => 1, 'privileges' => 'root')
                    )
                );
                $classes[] = $recCmp['controller_class'] . ' ';
            }
            if ($recCmp['language_alias'] == 'ru-RU')
                $names[] = '<b>' . $recCmp['controller_name'] . '</b> ';
            $components[$cid]['name'][$recCmp['language_alias']] = $recCmp['controller_name'];
            $components[$cid]['description'][$recCmp['language_alias']] = $recCmp['controller_about'];
        }

        $text = $this->packCmpsToJson($components);
        // название файла
        $filename = 'components.json';
        $path = $this->_pathData . $filename;
        // запись пака
        GFile::write($path, 'w', $text);
        // сообщение
        $this->response->setMsgResult($this->_['msg_pack_title'], sprintf($this->_['msg_pack'], $path, $filename), true);
        // запись в журнал действий пользователя
        $this->logPack(array(
            'log_query_id'     => $id,
            'log_query'        => sprintf($this->_['msg_log_pack'], implode($names, ','), $filename),
            'log_query_params' => array(
                'filename'   => $path,
                'components' => implode($classes, ',')
            )
        ));
    }

    /**
     * Упаковка модуля и его компонентов
     * 
     * @return void
     */
    public function pack()
    {
        $this->dataAccessPack();

        // идент. выбранных записей
        $ids = GFactory::getInput()->get('id');
        if (strpos($ids, ',') === false)
            $this->packComponent($ids);
        else
            $this->packComponents($ids);
    }

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        // метод запроса
        switch ($this->uri->method) {
            // метод "POST"
            case 'POST':
                // тип действия
                if ($this->uri->action == 'data') {
                    $this->pack();
                    return;
                }
                break;
        }

        parent::init();
    }
}
?>