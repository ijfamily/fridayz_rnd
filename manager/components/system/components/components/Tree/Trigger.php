<?php
/**
 * Gear Manager
 *
 * Контроллер         "Триггер дерева"
 * Пакет контроллеров "Триггер дерева"
 * Группа пакетов     "Список компонентов"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YControllers_Tree
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Tree/Trigger');

/**
 * Триггер дерева
 * 
 * @category   Gear
 * @package    GController_YControllers_Tree
 * @subpackage Trigger
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YControllers_Tree_Trigger extends GController_Tree_Trigger
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ycontrollers_grid';

    /**
     * Возращает список модулей
     *
     * @return array
     */
    protected function getModules()
    {
        $data = array();
        $query = new GDb_Query();
        $sql = 'SELECT `m`.*, COUNT(*) `count` FROM `gear_controllers` `c` JOIN `gear_modules` `m` USING(`module_id`) GROUP BY `module_id` ';
        if ($query->execute($sql) !== true)
            throw new GSqlException();
        while (!$query->eof()) {
            $rec = $query->next();
            $data[] = array(
                'text'    => $rec['module_name'] . ' <b>(' . $rec['count'] . ')</b>',
                'qtip'    => $rec['module_name'],
                'leaf'    => true,
                'iconCls' => 'icon-folder-find',
                'value'   => $rec['module_id']
            );
        }

        return $data;
    }

    /**
     * Возращает список груп пользователей
     *
     * @return array
     */
    protected function getGroups()
    {
        $data = array();
        $query = new GDb_Query();
        $sql = 'SELECT `gl`.*, COUNT(*) `count` '
             . 'FROM `gear_controllers` `c` '
             . 'JOIN `gear_controller_groups_l` `gl` USING(`group_id`) '
             . 'WHERE `gl`.`language_id`=' . $this->language->get('id')
             . ' GROUP BY `c`.`group_id` ';
        if ($query->execute($sql) !== true)
            throw new GSqlException();
        while (!$query->eof()) {
            $rec = $query->next();
            $data[] = array(
                'text'    => $rec['group_name'] . ' <b>(' . $rec['count'] . ')</b>',
                'qtip'    => $rec['group_name'],
                'leaf'    => true,
                'iconCls' => 'icon-folder-find',
                'value'   => $rec['group_id']
            );
        }

        return $data;
    }

    /**
     * Вывод данных в интерфейс компонента
     * 
     * @param  string $name название триггера
     * @param  string $node id выбранного узла
     * @return void
     */
    protected function dataView($name, $node)
    {
        parent::dataView($name, $node);

        // триггер
        switch ($name) {
            // быстрый фильтр списка
            case 'gridFilter':
                // id выбранного узла
                switch ($node) {
                    // по группе пользователя
                    case 'byGr':
                        $this->response->data = $this->getGroups();
                        break;

                    // по модулю
                    case 'byMd':
                        $this->response->data = $this->getModules();
                        break;
                }
        }
    }
}
?>