<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные транслятора компонента"
 * Пакет контроллеров "Список компонентов"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Controller_YControllers_Translation
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * "Данные транслятора компонента
 * 
 * @category   Gear
 * @package    Controller_YControllers_Translation
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YControllers_Translation_Data extends GController_Translation_Data
{
    /**
     * Массив полей таблицы ($_tableName)
     *
     * @var array
     */
    public $fields = array('controller_name', 'controller_about', 'controller_profile');

    /**
     * Первичный ключ таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'controller_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_controllers_l';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ycontrollers_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        $id = $this->session->get('language/default/id');
        $alias = $this->session->get('language/default/alias');
        if (empty($record['controller_name' . $id]))
            $str = '';
        else
            $str = $record['controller_name' . $id];

        return sprintf($this->_['title_translation_update'], $alias, $str);
    }
}
?>