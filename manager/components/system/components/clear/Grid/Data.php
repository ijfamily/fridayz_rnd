<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка компонентов"
 * Пакет контроллеров "Очистка компонентов"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YCClear_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные списка компонентов
 * 
 * @category   Gear
 * @package    GController_YCClear_Grid
 * @subpackage Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YCClear_Grid_Data extends GController_Grid_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * (для обработки записей таблицы)
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'controller_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_controllers';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS `c`.*,`cg`.`group_name`,`cm`.`module_name`, `cl`.`controller_name`, `cl`.`controller_about` '
          . 'FROM `gear_controllers` `c` '
          . 'JOIN `gear_controllers_l` `cl` USING(`controller_id`) '
            // группа компонентов
          . 'LEFT JOIN (SELECT `g`.*,`l`.`group_name`,`l`.`group_about` FROM `gear_controller_groups` `g` JOIN `gear_controller_groups_l` `l` USING(`group_id`) '
          . 'WHERE `l`.`language_id`=' . $this->language->get('id') . ') `cg` USING (`group_id`) '
            // модули
          . 'LEFT JOIN `gear_modules` `cm` ON `cm`.`module_id`=`cg`.`module_id` '
          . 'WHERE `c`.`controller_clear`=1 AND `cl`.`language_id`=' . $this->language->get('id') . ' %filter '
          . 'ORDER BY %sort '
          . 'LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            'controller_name' => array('type' => 'string'),
            'group_name' => array('type' => 'string'),
            'controller_path' => array('type' => 'string'),
            'controller_about' => array('type' => 'string'),
            'controller_uri' => array('type' => 'string'),
            'controller_uri_pkg' => array('type' => 'string'),
            'controller_uri_clear' => array('type' => 'string'),
            'controller_uri_action' => array('type' => 'string'),
            'controller_class' => array('type' => 'string'),
            'controller_enabled' => array('type' => 'string'),
            'controller_visible' => array('type' => 'string'),
            'controller_dashboard' => array('type' => 'string'),
            'controller_clear' => array('type' => 'string'),
            'module_name' => array('type' => 'string')
        );
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        $record['controller_uri_clear'] = $record['controller_uri_pkg'] . ROUTER_DELIMITER . 'grid/data/';

        return $record;
    }
}
?>