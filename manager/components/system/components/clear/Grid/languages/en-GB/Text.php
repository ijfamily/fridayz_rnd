<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Cleaning components',
    'tooltip_grid' => 'удаление записей из базы данных',
    // панель управления
    'title_buttongroup_edit'   => 'Edit',
    'title_buttongroup_cols'   => 'Columns',
    'title_buttongroup_filter' => 'Filter',
    'msg_btn_clear'            => 'Are you sure you want to delete all entries <span class="mn-msg-delete"> '
                                . '("System components")</span> ?',
    // столбцы
    'header_controller_name'        => 'Name',
    'header_group_name'             => 'Group',
    'tooltip_group_name'            => 'Component group',
    'header_module_name'            => 'Module',
    'tooltip_module_name'           => 'System module',
    'header_controller_about'       => 'Description',
    'header_controller_class'       => 'Class id',
    'header_controller_uri_pkg'     => 'Component',
    'tooltip_controller_uri_pkg'    => 'The directory path component',
    'header_controller_uri'         => 'Controller',
    'tooltip_controller_uri'        => 'The directory path controller',
    'header_controller_uri_action'  => 'Interface',
    'tooltip_controller_uri_action' => 'Interface controller',
    'header_controller_enabled'  => 'Eanbled',
    'header_controller_visible'  => 'Visible',
    'header_controller_board'    => 'On board',
    'tooltip_controller_board'   => 'The presence of a component on the board components',
    'header_controller_clear'    => 'Cleaning',
    'tooltip_controller_clear'   => 'Perform cleanup (delete all) component data',
    // типы
    'data_boolean' => array('No', 'Yeas')
);
?>