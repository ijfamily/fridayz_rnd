<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Очищення даних компонентів',
    'tooltip_grid' => 'удаление записей из базы данных',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Стовпці',
    'title_buttongroup_filter' => 'Фільтр',
    'msg_btn_clear'            => 'Ви дійсно бажаєте видалити всі записи <span class="mn-msg-delete"> '
                                . '("Компоненти системи")</span> ?',
    // столбцы
    'header_controller_name'        => 'Назва',
    'header_group_name'             => 'Група',
    'tooltip_group_name'            => 'Група компонентів',
    'header_module_name'            => 'Модуль',
    'tooltip_module_name'           => 'Модуль системи',
    'header_controller_about'       => 'Опис',
    'header_controller_class'       => 'ID класу',
    'header_controller_uri_pkg'     => 'Компонент',
    'tooltip_controller_uri_pkg'    => 'Шлях до каталогу компонента',
    'header_controller_uri'         => 'Контролер',
    'tooltip_controller_uri'        => 'Шлях до каталогу контролера',
    'header_controller_uri_action'  => 'Iнтерфейс',
    'tooltip_controller_uri_action' => 'Iнтерфейс контролера',
    'header_controller_enabled'  => 'Доступний',
    'header_controller_visible'  => 'Відомий',
    'header_controller_board'    => 'На дошці',
    'tooltip_controller_board'   => 'Присутність компонента на дошці компонентів',
    'header_controller_clear'    => 'Очищення',
    'tooltip_controller_clear'   => 'Виконувати очищення (видалення всіх) даних компонента',
    // типы
    'data_boolean' => array('Нi', 'Так')
);
?>