<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка компонентов"
 * Пакет контроллеров "Очистка компонентов"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YCClear_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс списка компонентов
 * 
 * @category   Gear
 * @package    GController_YCClear_Grid
 * @subpackage Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YCClear_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * (для обработки записей таблицы)
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'controller_id';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'controller_name';

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('name' => 'controller_name', 'type' => 'string'),
            array('name' => 'group_name', 'replacer' => 'group_id', 'type' => 'string'),
            array('name' => 'controller_path', 'type' => 'string'),
            array('name' => 'controller_about', 'type' => 'string'),
            array('name' => 'controller_uri', 'type' => 'string'),
            array('name' => 'controller_uri_clear', 'type' => 'string'),
            array('name' => 'controller_uri_pkg', 'type' => 'string'),
            array('name' => 'controller_uri_action', 'type' => 'string'),
            array('name' => 'controller_class', 'type' => 'string'),
            array('name' => 'controller_enabled', 'type' => 'string'),
            array('name' => 'controller_visible', 'type' => 'string'),
            array('name' => 'controller_dashboard', 'type' => 'string'),
            array('name' => 'controller_clear', 'type' => 'string'),
            array('name' => 'module_name', 'type' => 'string')
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $this->columns = array(
            array('xtype'     => 'numbercolumn'),
            array('dataIndex' => 'controller_id',
                  'header'    => '<em class="mn-grid-hd-id"></em> ID',
                  'hidden'    => true,
                  'width'     => 60,
                  'sortable'  => true,
                  'filter'    => array('type' => 'numeric')),
            array('dataIndex' => 'controller_name',
                  'header'    => '<em class="mn-grid-hd-details"></em>'
                               . $this->_['header_controller_name'],
                  'width'     => 140,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'group_name',
                  'header'    => $this->_['header_group_name'],
                  'tooltip'   => $this->_['tooltip_group_name'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'module_name',
                  'header'    => $this->_['header_module_name'],
                  'tooltip'   => $this->_['tooltip_module_name'],
                  'width'     => 140,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'controller_about',
                  'header'    => $this->_['header_controller_about'],
                  'width'     => 160,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'controller_class',
                  'header'    => $this->_['header_controller_class'],
                  'width'     => 150,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'controller_uri_pkg',
                  'header'    => $this->_['header_controller_uri_pkg'],
                  'width'     => 190,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'controller_uri',
                  'header'    => $this->_['header_controller_uri'],
                  'width'     => 190,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'controller_uri_action',
                  'header'    => $this->_['header_controller_uri_action'],
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string'))
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_grid'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_grid'],
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'profileUrl'    => $this->componentUrl . 'profile/',
                  'sm'            => array('singleSelect' => true))
        );

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "правка" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' => $this->classId));
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'  => 'mn-btn-clear-ctrl',
                  'icon'   => $this->resourcePath . 'icon-btn-clear.png',
                  'gridId' => $this->classId)
        );
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = 'component-clear-components';
        $this->_cmp->tbar->items->add($group);

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $cellInfo =
            '<div class="mn-grid-cell-tooltip-tl">{controller_name}</div>'
          . '<div class="mn-grid-cell-tooltip" style="width:400px">'
          . '<img src="' . PATH_COMPONENT . '{controller_uri}/resources/shortcut.png" width="60px" height="60px">'
          . '<em>' . $this->_['header_group_name'] . '</em>: <b>{group_name}</b><br>'
          . '<em>' . $this->_['header_controller_uri_pkg'] . '</em>: <b>{controller_uri_pkg}</b><br>'
          . '<em>' . $this->_['header_controller_uri'] . '</em>: <b>{controller_uri}</b><br>'
          . '<em>' . $this->_['header_controller_uri_action'] . '</em>: <b>{controller_uri_action}</b><br>'
          . '<em>' . $this->_['header_controller_class'] . '</em>: <b>{controller_class}</b>'
          . '</div>';
        $this->_cmp->cellTips = array(
            array('field' => 'controller_name', 'tpl' => $cellInfo),
            array('field' => 'group_name', 'tpl' => '{group_name}'),
            array('field' => 'controller_about', 'tpl' => '{controller_about}'),
            array('field' => 'controller_class', 'tpl' => '{controller_class}'),
            array('field' => 'controller_uri_pkg', 'tpl' => '{controller_uri_pkg}'),
            array('field' => 'controller_uri', 'tpl' => '{controller_uri}'),
            array('field' => 'controller_uri_action', 'tpl' => '{controller_uri_action}'),
            array('field' => 'module_name', 'tpl' => '{module_name}')
        );
        $this->_cmp->cellTipConfig = array('maxWidth' => 400);

        parent::getInterface();
    }
}
?>