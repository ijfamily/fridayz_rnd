<?php
/**
 * Gear Manager
 *
 * Controller          "Поиск данных в списке"
 * Package controllers "Компоненты"
 * Group of packages   "Компоненты"
 * Module              "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YCClear_Search
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Interface');

/**
 * Поиск данных в списке
 * 
 * @category   Gear
 * @package    GController_YCClear_Search
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YCClear_Search_Interface extends GController_Search_Interface
{
    /**
     * Идентификатор DOM компонента (списка)
     *
     * @var string
     */
    public $gridId = 'gcontroller_ycclear_grid';

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataSearch")
        $this->_cmp->setProps(
            array('title'       => $this->_['title_search'],
                  'gridId'      => $this->gridId,
                  'btnSearchId' => $this->gridId . '-bnt_search',
                  'url'         => $this->componentUrl . 'search/data/')
        );

        // виды полей для поиска записей
        $this->_cmp->editorFields = array(
          array('xtype' => 'textfield'),
          array('xtype'      => 'mn-field-combo',
                'tpl'        => '<tpl for="."><div ext:qtip="{name}" class="x-combo-list-item">{data}</div></tpl>',
                'name'       => 'group_id',
                'editable'   => false,
                'pageSize'   => 50,
                'width'      => 260,
                'hiddenName' => 'group_id',
                'allowBlank' => true,
                'store'      => array(
                    'xtype' => 'jsonstore',
                    'url'   => $this->componentUrl . 'combo/trigger/?name=groups&string=1'
                )
          ),
          array('xtype'      => 'mn-field-combo',
                'tpl'        => '<tpl for="."><div ext:qtip="{name}" class="x-combo-list-item">{data}</div></tpl>',
                'name'       => 'module_id',
                'editable'   => true,
                'pageSize'   => 50,
                'hiddenName' => 'module_id',
                'allowBlank' => true,
                'store'      => array(
                    'xtype' => 'jsonstore',
                    'url'   => $this->componentUrl . 'combo/trigger/?name=modules&string=1'
                )
          ),
          array('xtype' => 'textfield'),
          array('xtype' => 'textfield'),
          array('xtype' => 'textfield'),
          array('xtype' => 'textfield'),
          array('xtype' => 'textfield')
        );

        parent::getInterface();
    }
}
?>