<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Поиск в списке "Очистка компонентов"',
    // столбцы
    'header_controller_name'        => 'Название',
    'header_group_name'             => 'Группа',
    'tooltip_group_name'            => 'Группа компонентов',
    'header_module_name'            => 'Модуль',
    'tooltip_module_name'           => 'Модуль системы',
    'header_controller_about'       => 'Описание',
    'header_controller_class'       => 'ID класса',
    'header_controller_uri_pkg'     => 'Компонент',
    'tooltip_controller_uri_pkg'    => 'Путь к каталогу компонента',
    'header_controller_uri'         => 'Контроллер',
    'tooltip_controller_uri'        => 'Путь к каталогу контроллера',
    'header_controller_uri_action'  => 'Интерфейс',
    'tooltip_controller_uri_action' => 'Интерфейс контроллера'
);
?>