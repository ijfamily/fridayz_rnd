<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'        => 'Group of components',
    'tooltip_grid'      => 'список групп компонентов системы',
    'rowmenu_edit'      => 'Edit',
    'rowmenu_translate' => 'Translate',
    // панель управления
    'title_buttongroup_edit'   => 'Edit',
    'title_buttongroup_cols'   => 'Columns',
    'title_buttongroup_filter' => 'Filter',
    'msg_btn_clear'            => 'Are you sure you want to delete all records <span class="mn-msg-delete"> '
                                . '("Group of components")</span> ?',
    // поля
    'header_group_name'    => 'Name',
    'header_pgroup_name'   => 'Name "parent"',
    'header_group_about'   => 'Description',
    'header_module_name'   => 'Module',
    'header_pgroup_about'  => 'Description "parent"',
    'header_group_icon'    => 'Icon',
    'header_group_level'   => 'Level',
    'tooltip_group_level'  => 'The nesting level of the group in the tree',
    'header_group_count'   => 'Components',
    'tooltip_group_count'  => 'Components in the group'
);
?>