<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'        => 'Группы компонентов',
    'tooltip_grid'      => 'список групп компонентов системы',
    'rowmenu_edit'      => 'Редактирование',
    'rowmenu_translate' => 'Транслирование',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete"> '
                                . '("Группы компонентов")</span> ?',
    // поля
    'header_group_name'    => 'Название',
    'header_pgroup_name'   => 'Название "предка"',
    'header_group_about'   => 'Описание',
    'header_module_name'   => 'Модуль',
    'header_pgroup_about'  => 'Описание "предка"',
    'header_group_icon'    => 'Значок',
    'header_group_level'   => 'Уровень',
    'tooltip_group_level'  => 'Уровень вложенности группы в дереве',
    'header_group_count'   => 'Компонентов',
    'tooltip_group_count'  => 'Компонентов в группе'
);
?>