<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'        => 'Групи компонентів',
    'tooltip_grid'      => 'список групп компонентов системы',
    'rowmenu_edit'      => 'Редагувати',
    'rowmenu_translate' => 'Транслювання',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Стовпці',
    'title_buttongroup_filter' => 'Фільтр',
    'msg_btn_clear'            => 'Ви дійсно бажаєте видалити всі записи <span class="mn-msg-delete"> '
                                . '("Групи компонентів")</span> ?',
    // поля
    'header_group_name'    => 'Назва',
    'header_pgroup_name'   => 'Назва "предка"',
    'header_group_about'   => 'Опис',
    'header_module_name'   => 'Модуль',
    'header_pgroup_about'  => 'Опис "предка"',
    'header_group_icon'    => 'Значок',
    'header_group_level'   => 'Рівень',
    'tooltip_group_level'  => 'Рівень вкладеності групи в дереві',
    'header_group_count'   => 'Компонентів',
    'tooltip_group_count'  => 'Компонентів у групі'
);
?>