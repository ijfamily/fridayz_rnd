<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка группы компонентов сситемы"
 * Пакет контроллеров "Группы компонентов"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YCGroups_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс списка группы компонентов сситемы
 * 
 * @category   Gear
 * @package    GController_YCGroups_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YCGroups_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'group_id';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'group_name';

    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('name' => 'group_name', 'type' => 'string'),
            array('name' => 'group_about', 'type' => 'string'),
            array('name' => 'pgroup_name', 'type' => 'string'),
            array('name' => 'pgroup_about', 'type' => 'string'),
            array('name' => 'group_icon', 'type' => 'string'),
            array('name' => 'group_level', 'type' => 'integer'),
            array('name' => 'group_count', 'type' => 'integer'),
            array('name' => 'module_name', 'type' => 'string')
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $this->columns = array(
            array('xtype'     => 'numbercolumn'),
            array('xtype'     => 'rowmenu'),
            array('dataIndex' => 'group_name',
                  'header'    => '<em class="mn-grid-hd-details"></em>'
                               . $this->_['header_group_name'],
                  'width'     => 150,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'group_about',
                  'header'    => $this->_['header_group_about'],
                  'width'     => 150,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'module_name',
                  'header'    => $this->_['header_module_name'],
                  'width'     => 150,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'pgroup_name',
                  'header'    => $this->_['header_pgroup_name'],
                  'width'     => 150,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'pgroup_about',
                  'header'    => $this->_['header_pgroup_about'],
                  'width'     => 150,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'group_icon',
                  'header'    => $this->_['header_group_icon'],
                  'hidden'    => true,
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'group_level',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-level.png" align="absmiddle">',
                  'tooltip'   => $this->_['tooltip_group_level'],
                  'align'     => 'center',
                  'width'     => 45,
                  'sortable'  => true),
            array('dataIndex' => 'group_count',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-cmp.png" align="absmiddle">',
                  'tooltip'   => $this->_['tooltip_group_count'],
                  'align'     => 'center',
                  'width'     => 45,
                  'sortable'  => true,
                  'filter'    => array('type' => 'numeric'))
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_grid'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_grid'],
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'profileUrl'    => $this->componentUrl . 'profile/',
                  'sm'            => array('singleSelect' => true)));

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "правка" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "добавить" (ExtJS class "Manager.button.InsertData")
        $group->items->add(array('xtype' => 'mn-btn-insert-data', 'gridId' => $this->classId));
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' => $this->classId));
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = 'component-group-components';
        $this->_cmp->tbar->items->add($group);

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $cellInfo =
            '<div class="mn-grid-cell-tooltip-tl">{group_name}</div>'
          . '<div class="mn-grid-cell-tooltip" style="width:400px">'
          . '<em>' . $this->_['header_group_about'] . '</em>: <b>{group_about}</b><br>'
          . '<em>' . $this->_['header_pgroup_name'] . '</em>: <b>{pgroup_name}</b><br>'
          . '<em>' . $this->_['header_pgroup_about'] . '</em>: <b>{pgroup_about}</b><br>'
          . '<em>' . $this->_['header_group_count'] . '</em>: <b>{group_count}</b><br>'
          . '<em>' . $this->_['header_module_name'] . '</em>: <b>{module_name}</b>'
          . '</div>';
        $this->_cmp->cellTips = array(
            array('field' => 'group_name', 'tpl' => $cellInfo),
            array('field' => 'group_about', 'tpl' => '{group_about}'),
            array('field' => 'pgroup_name', 'tpl' => '{pgroup_name}'),
            array('field' => 'pgroup_about', 'tpl' => '{pgroup_about}'),
            array('field' => 'group_count', 'tpl' => '{group_components}'),
            array('field' => 'module_name', 'tpl' => '{module_name}')
        );
        $this->_cmp->cellTipConfig = array('maxWidth' => 400);

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $this->_cmp->rowMenu = array(
            'xtype' => 'mn-grid-rowmenu',
            'items' => array(
                array('text'    => $this->_['rowmenu_edit'],
                      'iconCls' => 'icon-form-edit',
                      'url'     => $this->componentUrl . 'profile/interface/'),
                array('xtype'   => 'menuseparator'),
                array('text'    => $this->_['rowmenu_translate'],
                      'iconCls' => 'icon-form-translate',
                      'url'     => $this->componentUrl . 'translation/interface/')
            )
        );

        parent::getInterface();
    }
}
?>