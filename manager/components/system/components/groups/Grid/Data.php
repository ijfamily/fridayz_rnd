<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка группы компонентов сситемы"
 * Пакет контроллеров "Группы компонентов"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YCGroups_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные списка группы компонентов сситемы
 * 
 * @category   Gear
 * @package    GController_YCGroups_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YCGroups_Grid_Data extends GController_Grid_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * (для обработки записей таблицы)
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Префикс полей для запросов в nested set
     *
     * @var string
     */
   public $fieldPrefix = 'group';

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'group_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_controller_groups';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS `m`.`module_name`, '
          . '`g`.*, `c`.`group_count`, `gp`.`group_name` `pgroup_name`, `gp`.`group_about` `pgroup_about` '
          . 'FROM (SELECT `g`.*,`l`.`group_name`,`l`.`group_about` FROM `gear_controller_groups` `g` JOIN `gear_controller_groups_l` `l` USING(`group_id`) '
          . 'WHERE `l`.`language_id`=' . $this->language->get('id'). ') `gp`, '
          . '(SELECT `g`.*,`l`.`group_name`,`l`.`group_about` FROM `gear_controller_groups` `g` JOIN `gear_controller_groups_l` `l` USING(`group_id`) '
          . 'WHERE `l`.`language_id`=' . $this->language->get('id'). ') `g` '
          // модули компонентов
          . 'LEFT JOIN `gear_modules` `m` USING (`module_id`) '
          // количество компонентов в группе
          . 'LEFT JOIN (SELECT `group_id`, COUNT(`controller_id`) `group_count` '
          . 'FROM `gear_controllers` GROUP BY `group_id`) `c` USING (`group_id`) '
          . 'WHERE `g`.`group_left` > `gp`.`group_left` AND `gp`.`group_right` > `g`.`group_right` AND '
          . '`gp`.`group_level` = `g`.`group_level`-1 %filter '
          . 'ORDER BY %sort '
          . 'LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            'group_name' => array('type' => 'string'),
            'group_about' => array('type' => 'string'),
            'pgroup_name' => array('type' => 'string', 'as' => 'gp.group_name'),
            'pgroup_about' => array('type' => 'string', 'as' => 'gp.group_about'),
            'group_icon' => array('type' => 'string'),
            'group_level' => array('type' => 'integer'),
            'group_count' => array('type' => 'integer'),
            'group_components' => array('type' => 'string'),
            'module_name' => array('type' => 'string')
        );
    }

    /**
     * Удаление данных
     * 
     * @return void
     */
    protected function dataDelete()
    {
        parent::dataAccessDelete();

        // проверка существования зависимых записей
        $this->isDataDependent();
        // определяем откуда id брать
        if ($this->uri->id)
            $id = $this->uri->id;
        else {
            $input = GFactory::getInput();
            $id = $input->get('id');
        }
        // если не указан $id
        if (empty($id))
            throw new GException('Error', 'Unable to delete records!');

        $query = new GDb_Query();
        // выбранная запись
        $sql = 'SELECT * FROM `gear_controller_groups` WHERE `group_id`=' . $id;
        if (($node = $query->getRecord($sql)) === false)
            throw new GSqlException();

        // обновление всех компонентов в группах
        $sql = 'UPDATE `gear_controllers` `c`, (SELECT `group_id` FROM `gear_controller_groups` '
             . 'WHERE `group_left`>=' . $node['group_left'] . ' AND `group_right`<=' . $node['group_right'] . ') `g` '
             . 'SET `c`.`group_id`=NULL WHERE `c`.`group_id`=`g`.`group_id`';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление языков группа
        $sql = 'DELETE `l` FROM `gear_controller_groups_l` `l`,`gear_controller_groups` `g` '
             . 'WHERE `g`.`group_left`>=' . $node['group_left'] . ' AND `g`.`group_right`<=' . $node['group_right']
             . ' AND `g`.`group_id`=`l`.`group_id`';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        Gear::library('Db/Drivers/MySQL/Tree/Tree');

        // указатель на базу данных
        $db = GFactory::getDb();
        $tree = new GDb_Tree($this->tableName, $this->fieldPrefix, $db->getHandle());
        // удаление ветвей дерева
        $tree->DeleteAll($id, '');
        if (!empty($tree->ERRORS_MES)) {
            $error = implode(',', $tree->ERRORS_MES);
            throw new GException('Data processing error', 'Query error (Internal Server Error)', $error);
        }
    }
}
?>