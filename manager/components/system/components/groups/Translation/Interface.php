<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс транслятора компонента"
 * Пакет контроллеров "Список компонентов"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Controller_YCGroups_Translation
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * "Интерфейс транслятора компонента
 * 
 * @category   Gear
 * @package    Controller_YCGroups_Translation
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YCGroups_Translation_Interface extends GController_Profile_Interface
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ycgroups_grid';

    /**
     * Возвращает интерфейс
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(array(
            'title'           => $this->_['title_translation_insert'],
            'titleEllipsis'   => 46,
            'gridId'          => 'gcontroller_ycgroups_grid',
            'width'           => 375,
            'height'          => 170,
            'resizable'       => false,
            'stateful'        => false,
            'btnDeleteHidden' => true)
        );

        // поля формы (ExtJS class "Ext.Panel")
        // вкладка "языки"
        $tabs = array();
        $list = $this->session->get('language/list');
        $count = count($list);
        for ($i = 0; $i < $count; $i++) {
            $langId = $list[$i]['language_id'];
            $tabs[] = array(
                'title'     => $list[$i]['language_name'],
                'layout'    => 'form',
                'baseCls'   => 'mn-form-tab-body',
                'items'     => array(
                    array('xtype'      => 'textfield',
                          'fieldLabel' => $this->_['label_group_name'],
                          'name'       => 'group_name' . $langId,
                          'maxLength'  => 100,
                          'anchor'     => '100%',
                          'allowBlank' => false),
                    array('xtype'      => 'textfield',
                          'fieldLabel' => $this->_['label_group_about'],
                          'name'       => 'group_about' . $langId,
                          'maxLength'  => 255,
                          'anchor'     => '100%',
                          'allowBlank' => false)
                )
            );
        }

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->url = $this->componentUrl . 'translation/';
        $form->items->add(array(
            'xtype'             => 'tabpanel',
            'tabPosition'       => 'bottom',
            'layoutOnTabChange' => true,
            'deferredRender'    => false,
            'activeTab'         => 0,
            'enableTabScroll'   => true,
            'anchor'            => '100% 100%',
            'items'             => array($tabs))
        );

        parent::getInterface();
    }
}
?>