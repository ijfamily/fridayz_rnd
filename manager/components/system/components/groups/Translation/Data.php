<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные транслятора группы компонента"
 * Пакет контроллеров "Список групп компонентов"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Controller_YCGroups_Translation
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные транслятора группы компонента
 * 
 * @category   Gear
 * @package    Controller_YCGroups_Translation
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YCGroups_Translation_Data extends GController_Translation_Data
{
    /**
     * Массив полей таблицы ($_tableName)
     *
     * @var array
     */
    public $fields = array('group_name', 'group_about');

    /**
     * Первичный ключ таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'group_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_controller_groups_l';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ycgroups_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        $id = $this->session->get('language/default/id');
        $alias = $this->session->get('language/default/alias');
        if (empty($record['group_name' . $id]))
            $str = '';
        else
            $str = $record['group_name' . $id];

        return sprintf($this->_['title_translation_update'], $alias, $str);
    }
}
?>