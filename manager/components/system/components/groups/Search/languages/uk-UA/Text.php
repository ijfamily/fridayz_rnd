<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Пошук у списку "Групи компонентів"',
    // поля
    'header_group_name'    => 'Назва',
    'header_pgroup_name'   => 'Назва "предка"',
    'header_group_about'   => 'Опис',
    'header_module_name'   => 'Модуль',
    'header_pgroup_about'  => 'Опис "предка"',
    'header_group_icon'    => 'Значок',
    'header_group_level'   => 'Рівень',
    'header_group_count'   => 'Компонентів'
);
?>