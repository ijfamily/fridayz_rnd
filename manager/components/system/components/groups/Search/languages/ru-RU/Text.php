<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Поиск в списке "Группы компонентов"',
    // поля
    'header_group_name'   => 'Название',
    'header_pgroup_name'  => 'Название "предка"',
    'header_group_about'  => 'Описание',
    'header_module_name'  => 'Модуль',
    'header_pgroup_about' => 'Описание "предка"',
    'header_group_icon'   => 'Иконка',
    'header_group_level'  => 'Уровень',
    'header_group_count'  => 'Компонентов'
);
?>