<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск в списка групп компонентов"
 * Пакет контроллеров "Группы компонентов"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YCGroups_Search
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск в списке групп компонентов
 * 
 * @category   Gear
 * @package    GController_YCGroups_Search
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YCGroups_Search_Data extends GController_Search_Data
{
    /**
     * дентификатор компонента (списка) к которому применяется поиск
     *
     * @var string
     */
    public $gridId = 'gcontroller_ycgroups_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ycgroups_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор,
     * используется для инициализации атрибутов контроллера без конструктора
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'group_name', 'label' => $this->_['header_group_name']),
            array('dataIndex' => 'group_about', 'label' => $this->_['header_group_about']),
            array('dataIndex' => 'module_name', 'label' => $this->_['header_module_name']),
            array('dataIndex' => 'pgroup_name', 'label' => $this->_['header_pgroup_name'], 'as' => 'gp.group_name'),
            array('dataIndex' => 'pgroup_about', 'label' => $this->_['header_pgroup_about'], 'as' => 'gp.group_about'),
            array('dataIndex' => 'group_count', 'label' => $this->_['header_group_count'])
        );
    }
}
?>