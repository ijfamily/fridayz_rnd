<?php

/**
 * @see GDb_Tree
 */
//require_once('Gear/Db/Drivers/MySQL/Tree/Tree.php');

/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля группы компонента"
 * Пакет контроллеров "Группы компонентов"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YCGroups_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2014-08-04 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные профиля группы компонента
 * 
 * @category   Gear
 * @package    GController_YCGroups_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YCGroups_Profile_Data extends GController_Profile_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Префикс полей для запросов в nested set
     *
     * @var string
     */
   public $fieldPrefix = 'group';

    /**
     * Массив полей таблицы
     *
     * @var array
     */
    public $fields = array('module_id');

    /**
     * Первичный ключ таблицы
     *
     * @var string
     */
    public $idProperty = 'group_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_controller_groups';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ycgroups_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        if ($this->state == 'update')
            return $this->_['title_profile_update'];
        else
            return $this->_['title_profile_info'];
    }

    /**
     * Удаление данных
     * 
     * @return void
     */
    protected function dataDelete()
    {
        parent::dataAccessDelete();

        // определяем откуда id брать
        if (empty($this->uri->id)) {
            $input = GFactory::getInput();
            $this->uri->id = $input->get('id');
        }
        // если не указан $id
        if (empty($this->uri->id))
            throw new GException('Error', 'Unable to delete records!');

        // проверка существования зависимых записей
        $this->isDataDependent();

        $query = new GDb_Query();
        // выбранная запись
        $sql = 'SELECT * FROM `gear_controller_groups` WHERE `group_id`=' . $this->uri->id;
        if (($node = $query->getRecord($sql)) === false)
            throw new GSqlException();

        // обновление всех компонентов в группах
        $sql = 'UPDATE `gear_controllers` `c`, (SELECT `group_id` FROM `gear_controller_groups` '
             . 'WHERE `group_left`>=' . $node['group_left'] . ' AND `group_right`<=' . $node['group_right'] . ') `g` '
             . 'SET `c`.`group_id`=NULL WHERE `c`.`group_id`=`g`.`group_id`';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление языков группа
        $sql = 'DELETE `l` FROM `gear_controller_groups_l` `l`,`gear_controller_groups` `g` '
             . 'WHERE `g`.`group_left`>=' . $node['group_left'] . ' AND `g`.`group_right`<=' . $node['group_right']
             . ' AND `g`.`group_id`=`l`.`group_id`';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        Gear::library('Db/Drivers/MySQL/Tree/Tree');

        // указатель на базу данных
        $db = GFactory::getDb();
        $tree = new GDb_Tree($this->tableName, $this->fieldPrefix, $db->getHandle());
        // удаление ветвей дерева
        $tree->DeleteAll($this->uri->id, '');
        if (!empty($tree->ERRORS_MES)) {
            $error = implode(',', $tree->ERRORS_MES);
            throw new GException('Data processing error', 'Query error (Internal Server Error)', $error);
        }
    }

    /**
     * Вставка данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataInsert($params = array())
    {
        parent::dataAccessInsert();

        if ($this->input->isEmpty('POST'))
            throw new GException('Warning', 'To execute a query, you must change data!');
        // массив полей с их соответствующими значениями
        if (empty($params))
            $params = $this->input->getBy($this->fields);
        // проверки входных данных
        $this->isDataCorrect($params);
        // проверка существования записи с одинаковыми значениями
        $this->isDataExist($params);
        // использование системных полей в запросе SQL
        if ($this->isSysFields) {
            $params['sys_date_insert'] = date('Y-m-d');
            $params['sys_time_insert'] = date('H:i:s');
            $params['sys_user_insert'] = $this->session->get('user_id');
        }

        Gear::library('Db/Drivers/MySQL/Tree/Tree');

        // указатель на базу данных
        $db = GFactory::getDb();
        $tree = new GDb_Tree($this->tableName, $this->fieldPrefix, $db->getHandle());
        $parentNode = $this->input->get('group_id');
        // добавление записи
        $this->_recordId = $tree->insert($parentNode, '', $params);
        if (!empty($tree->ERRORS_MES)) {
            $error = implode(',', $tree->ERRORS_MES);
            throw new GException('Data processing error', 'Query error (Internal Server Error)', $error, false);
        }

        // добавление записи описания группы
        $data = $this->input->getBy(array('group_name', 'group_about'));
        $data['group_id'] = $this->_recordId;
        $table = new GDb_Table('gear_controller_groups_l', 'group_lang_id');
        $langs = $this->session->get('language/list');
        $count = count($langs);
        for ($i = 0; $i < $count; $i++) {
            $data['language_id'] = $langs[$i]['language_id'];
            $table->insert($data);
        }

        // запись в журнал действий пользователя
        $this->logInsert(
            array('log_query'        => '[node sql]',
                  'log_error'        => null,
                  'log_query_params' => $params)
        );
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        parent::dataUpdate($params);

        $table = new GDb_Table($this->tableName, $this->idProperty, $this->fields);
        // редактируемая запись
        $record = $table->getRecord($this->uri->id);
        // идентификатор группы компонентов
        $groupId = $this->input->get('group_id');
        if ($groupId)
            $groupId = is_numeric($groupId) ? $groupId : $record['group_id'];
        else
            $groupId = $record['group_id'];
        // если было изменение группы компонентов
        if ($record['group_id'] != $groupId) {
            Gear::library('Db/Drivers/MySQL/Tree/Tree');

            // указатель на базу данных
            $db = GFactory::getDb();
            $tree = new GDb_Tree($this->tableName, $this->fieldPrefix, $db->getHandle());
            $tree->MoveAll($record['group_id'], $groupId);
            if (!empty($tree->ERRORS_MES)) {
                $error = implode(',', $tree->ERRORS_MES);
                throw new GException('Data processing error', 'Query error (Internal Server Error)', $error, false);
            }
        }
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        parent::dataView($sql);

        unset($this->response->data['group_id']);
    }
}
?>