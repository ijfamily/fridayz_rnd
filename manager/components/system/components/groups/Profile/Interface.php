<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля группы компонента"
 * Пакет контроллеров "Группы компонентов"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YCGroups_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2014-08-04 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля группы компонента
 * 
 * @category   Gear
 * @package    GController_YCGroups_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YCGroups_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ycgroups_grid';

    /**
     * Возвращает дополнительные данные для формирования интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        $data = array('group_name' => '', 'module_name' => '');
        // состояние профиля "вставка"
        if ($this->isInsert) return $data;

        // соединение с базой данных
        GFactory::getDb()->connect();
        $query = new GDb_Query();
        // компонент
        $sql = 'SELECT `g`.*, `m`.`module_name` '
             . 'FROM `gear_controller_groups` `g` '
             . 'LEFT JOIN `gear_modules` `m` USING (`module_id`) '
             . "WHERE `group_id`={$this->uri->id}";
        $controller = $query->getRecord($sql);
        // группы компонентов
        $sql = 'SELECT group_name, group_id '
             . 'FROM `gear_controller_groups` `g` JOIN `gear_controller_groups_l` `l` USING(`group_id`) '
             . "WHERE {$controller['group_left']} > `group_left` AND {$controller['group_right']} < `group_right` AND "
             . "{$controller['group_level']} = `group_level` + 1 AND "
             . '`l`.language_id=' . $this->language->get('id');
        $record = $query->getRecord($sql);
        $data = array(
            'group_name'  => $record['group_name'],
            'module_name' => $controller['module_name']
        );

        return $data;
    }

    /**
     * Возвращает интерфейс
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительные данные для формирования интерфейса
        $data = $this->getDataInterface();

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'id'            => $this->classId,
                  'titleEllipsis' => 45,
                  'gridId'        => 'gcontroller_ycgroups_grid',
                  'width'         => 450,
                  'autoHeight'    => true,
                  'resizable'     => false,
                  'stateful'      => false,
                  'buttonsAdd'    => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-group-components',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array();
        // состояние профиля "вставка"
        if ($this->isInsert)
            array_push($items,
               array('xtype'      => 'textfield',
                     'fieldLabel' => $this->_['label_group_name'],
                     'name'       => 'group_name',
                     'emptyText'  => $this->_['empty_group_name'],
                     'maxLength'  => 50,
                     'anchor'     => '100%',
                     'allowBlank' => false),
               array('xtype'      => 'textfield',
                     'fieldLabel' => $this->_['label_group_about'],
                     'name'       => 'group_about',
                     'emptyText'  => $this->_['empty_group_about'],
                     'anchor'     => '100%',
                     'maxLength'  => 100,
                     'allowBlank' => false)
            );
        array_push($items,
           array('xtype'      => 'mn-field-combo',
                 'fieldLabel' => $this->_['label_module_name'],
                 'tpl'        => '<tpl for="."><div ext:qtip="{name}" class="x-combo-list-item">{data}</div></tpl>',
                 'value'      => $data['module_name'],
                 'name'       => 'module_id',
                 'pageSize'   => 50,
                 'anchor'     => '100%',
                 'hiddenName' => 'module_id',
                 'allowBlank' => false,
                 'store'      => array(
                      'xtype' => 'jsonstore',
                      'url'   => $this->componentUrl . 'combo/trigger/?name=modules'
                 )
           ),
           array('xtype'      => 'fieldset',
                 'labelWidth' => 60,
                 'title'      => $this->_['title_fieldset_group'],
                 'autoHeight' => true,
                 'items'      => array(
                    array('xtype'      => 'mn-field-combo-tree',
                          'fieldLabel' => $this->_['label_pgroup_name'],
                          'resetable'  => false,
                          'anchor'     => '100%',
                          'name'       => 'group_id',
                          'hiddenName' => 'group_id',
                          'treeWidth'  => 400,
                          'treeRoot'   => array('id' => 'root', 'expanded' => true, 'expandable' => true),
                          'value'      => $data['group_name'],
                          'allowBlank' => false,
                          'store'      => array(
                              'xtype' => 'jsonstore',
                              'url'   => $this->componentUrl . 'combo/nodes/'
                          )
                    )
                )
           )
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->addItems($items);
        $form->url = $this->componentUrl . 'profile/';

        parent::getInterface();
    }
}
?>