<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Create record "Group components"',
    'title_profile_update' => 'Update record "%s"',
    'title_profile_info'   => 'Detail',
    // поля
    'label_group_name'     => 'Name',
    'label_group_about'    => 'Description',
    'label_module_name'    => 'Module',
    'label_group_icon'     => 'Icon (css)',
    'title_fieldset_group' => 'State in group components',
    'label_pgroup_name'    => 'Parent',
    'dependent_groups'     => '"Group components"',
    'dependent_components' => '"Components"',
    'empty_group_name'     => 'New group',
    'empty_group_about'    => 'About group'
);
?>