<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Создание записи "Группа компонентов"',
    'title_profile_update' => 'Изменение записи',
    'title_profile_info'   => 'Детали записи',
    // поля
    'label_group_name'     => 'Название',
    'label_group_about'    => 'Описание',
    'label_module_name'    => 'Модуль',
    'label_group_icon'     => 'Значок (css)',
    'title_fieldset_group' => 'Положение в группе компонентов',
    'label_pgroup_name'    => 'Предок',
    'dependent_groups'     => '"Группы компонентов"',
    'dependent_components' => '"Компоненты"',
    'empty_group_name'     => 'Новая группа',
    'empty_group_about'    => 'Описание группы'
);
?>