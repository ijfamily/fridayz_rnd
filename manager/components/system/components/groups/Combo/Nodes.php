<?php
/**
 * Gear Manager
 *
 * Контроллер         "Триггер выпадающего дерева"
 * Пакет контроллеров "Группы компонентов"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YCGroups_Combo
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Nodes.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Combo/Nodes');

/**
 * Триггер выпадающего дерева
 * 
 * @category   Gear
 * @package    GController_YCGroups_Combo
 * @subpackage Nodes
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Nodes.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YCGroups_Combo_Nodes extends GController_Combo_NNodes
{
    /**
     * Префикс полей для запросов в nested set
     *
     * @var string
     */
    public $fieldPrefix = 'group';

    /**
     * Поле для сортовки в запросе
     *
     * @var string
     */
    public $orderBy = '';

    /**
     * Первичное поле
     *
     * @var string
     */
    public $idProperty = 'group_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_controller_groups';

    /**
     * Значок (css) для ветвей дерева
     *
     * @var string
     */
    public $iconChildCls = 'icon-ctr-groups';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ycgroups_grid';

    /**
     * Вызывается перед предварительной обработкой запроса при формировании записей
     * 
     * @param  array $prefix префикс полей
     * @param  array $id идинд. узла дерева
     * @return string
     */
    protected function queryPreprocessing($prefix, $node)
    {
        $query = 'SELECT * '
               . 'FROM `gear_controller_groups` `g` JOIN `gear_controller_groups_l` `l` USING(`group_id`) '
               . 'WHERE `' . $prefix['left'] . '`>' . $node[$prefix['left']] . ' AND '
               . '`' . $prefix['right'] . '`<' . $node[$prefix['right']] . ' AND '
               . '`' . $prefix['level'] . '`=' . ($node[$prefix['level']] + 1) . ' AND '
               . '`l`.`language_id`=' . $this->language->get('id');
        if ($this->orderBy) {
            if (isset($prefix[$this->orderBy]))
                $query .= 'ORDER BY `' . $prefix[$this->orderBy] . '`';
            else
                $query .= 'ORDER BY `' . $this->orderBy . '`';
        }

        return $query;
    }

    /**
     * Предварительная обработка узла дерева перед формированием массива записей JSON
     * 
     * @params array $node узел дерева
     * @params array $record запись
     * @return array
     */
    protected function nodesPreprocessing($node, $record)
    {
        if ($record['group_id'] == 1)
            $node['text'] = 'Gear';
        else
            $node['text'] = $record['group_name'];

        return $node;
    }
}
?>