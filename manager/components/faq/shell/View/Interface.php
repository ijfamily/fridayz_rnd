<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс оболочки ЧаВо"
 * Пакет контроллеров "Часто задаваемые вопросы"
 * Группа пакетов     "Справка"
 * Модуль             "ЧаВо"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_FAQShell_View
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Interface');
Gear::ext('Container/Panel');

/**
 * Интерфейс оболочки ЧаВо
 * 
 * @category   Gear
 * @package    GController_FAQShell_View
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_FAQShell_View_Interface extends GController_Interface
{
    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // Ext_Panel (ExtJS class "Ext.Panel")
        $this->_cmp = new Ext_Panel(
            array('id'        => strtolower(__CLASS__),
                  'closable'  => true,
                  'component' => array('container' => 'content-tab', 'destroy' => true))
        );
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // Ext_Panel (ExtJS class "Ext.Panel")
        $this->_cmp->iconSrc = $this->resourcePath . 'icon.png';
        $this->_cmp->iconTpl = $this->resourcePath . 'shortcut.png';
        $this->_cmp->title = $this->_['title'];
        $this->_cmp->titleTpl = $this->_['tooltip'];
        $this->_cmp->tabTip = $this->_['title'];
        $this->_cmp->items->add(array('xtype' => 'mn-iframe', 'url' => 'faq/'));

        parent::getInterface();
    }
}
?>