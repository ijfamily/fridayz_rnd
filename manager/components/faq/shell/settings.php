<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Часто задаваемые вопросы"
 * Группа пакетов     "Справка"
 * Модуль             "ЧаВо"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_FAQShell
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Настройки системы
 * Описание: Настройки системы
 * ID класса: gcontroller_faqshell_view
 * Группа: Справка / Часто задаваемые вопросы
 * Очищать: нет
 * Ресурс
 *    Пакет: faq/shell/
 *    Контроллер: faq/shell/View/
 *    Интерфейс: view/interface/
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске: нет
 */

return array(
    // {Y}- модуль "FAQ" {Config} - пакет контроллеров "Shell" -> FAQShell
    'clsPrefix' => 'FAQShell'
);
?>