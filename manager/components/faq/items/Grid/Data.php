<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка пунктов разделов ЧаВо"
 * Пакет контроллеров "Пункты разделов ЧаВо"
 * Группа пакетов     "ЧаВо"
 * Модуль             "ЧаВо"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_FAQItems_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные списка пунктов разделов ЧаВо
 * 
 * @category   Gear
 * @package    GController_FAQItems_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_FAQItems_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'faq_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_faq';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS `c`.*, `p`.`pfaq_name` '
            // join `gear_faq`
          . 'FROM (SELECT `l`.`faq_name` `pfaq_name`, `f`.`faq_id` FROM `gear_faq` `f`, `gear_faq_l` `l` '
          . 'WHERE `f`.`faq_id`=`l`.`faq_id` AND `l`.`language_id`=' . $this->language->get('id') . ' AND `f`.`faq_parent_id` IS NULL) `p`, '
            // join `gear_faq`
          . '(SELECT `l`.*, `f`.`faq_parent_id`, `f`.`faq_index`, `f`.`faq_hidden` FROM `gear_faq` `f`, `gear_faq_l` `l` '
          . 'WHERE `f`.`faq_id`=`l`.`faq_id` AND `l`.`language_id`=' . $this->language->get('id') . ' '
          . 'AND `f`.`faq_parent_id` IS NOT NULL) `c` '
          . 'WHERE `p`.`faq_id`=`c`.`faq_parent_id` %filter '
          . 'ORDER BY %sort '
          . 'LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // идент. раздела
            'faq_id' => array('name' => 'faq_id', 'type' => 'integer'),
            // порядковый номер
            'faq_index' => array('name' => 'faq_index', 'type' => 'integer'),
            // название
            'faq_name' => array('name' => 'faq_name', 'type' => 'string'),
            // описание
            'faq_description' => array('name' => 'faq_description', 'type' => 'string'),
            // раздел
            'pfaq_name' => array('name' => 'pfaq_name', 'type' => 'string'),
            // скрыть пункт
            'faq_hidden' => array('name' => 'faq_hidden', 'type' => 'integer')
        );
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        // удаление справки ("gear_faq")
        $query = new GDb_Query();
        $sql = 'DELETE FROM `gear_faq`';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_faq` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление языков справки ("gear_faq_l")
        $query = new GDb_Query();
        $sql = 'DELETE FROM `gear_faq_l`';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_faq_l` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        // удаление записей пакета локализации языков ("gear_faq_l")
        $query = new GDb_Query();
        $sql = 'DELETE FROM `gear_faq_l` WHERE `faq_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_faq_l` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }
}
?>