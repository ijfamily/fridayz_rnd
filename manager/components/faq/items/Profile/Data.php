<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные для интерфейса профиля пункта раздела ЧаВо"
 * Пакет контроллеров "Профиль пункта раздела ЧаВо"
 * Группа пакетов     "ЧаВо"
 * Модуль             "ЧаВо"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_FAQItems_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные для интерфейса профиля пункта раздела ЧаВо
 * 
 * @category   Gear
 * @package    GController_FAQItems_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_FAQItems_Profile_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($_tableName)
     *
     * @var array
     */
    public $fields = array('faq_parent_id', 'faq_hidden', 'faq_index');

    /**
     * Первичный ключ таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'faq_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_faq';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_faqitems_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_profile_update'], $record['faq_name']);
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        $sql = 'SELECT `f`.*, `l`.`faq_name` FROM `gear_faq_l` `l`, `gear_faq` `f` '
             . 'WHERE `l`.`faq_id`=`f`.`faq_id` AND `l`.`language_id`=' . $this->language->get('id') . ' AND '
             . '`f`.`faq_id`=' . $this->uri->id;

        parent::dataView($sql);
    }

    /**
     * Вставка данных
     * 
     * @param  array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataInsert($params = array())
    {
        parent::dataInsert();

        $data = $this->input->getBy(array('faq_name', 'faq_description'));
        $data['faq_id'] = $this->_recordId;
        $table = new GDb_Table('gear_faq_l', 'faq_lang_id');
        $langs = $this->session->get('language/list');
        $count = count($langs);
        for ($i = 0; $i < $count; $i++) {
            $data['language_id'] = $langs[$i]['language_id'];
            $table->insert($data);
        }
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        $query = new GDb_Query();
        // удаление пакета локализации языков ("gear_faq_l")
        $sql = 'DELETE FROM `gear_faq_l` WHERE `faq_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_faq_l` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }
}
?>