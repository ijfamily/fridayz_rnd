<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля пункта раздела ЧаВо"
 * Пакет контроллеров "Профиль пункта раздела ЧаВо"
 * Группа пакетов     "ЧаВо"
 * Модуль             "ЧаВо"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_FAQItems_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля пункта раздела ЧаВо
 * 
 * @category   Gear
 * @package    GController_FAQItems_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_FAQItems_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        $data = array('faq_name' => '');
        // если состояние формы "insert"
        if ($this->isInsert) return $data;

        // соединение с базой данных
        GFactory::getDb()->connect();
        $query = new GDb_Query();
        // выбранный раздел
        $sql = 'SELECT `p`.`faq_name` '
             . 'FROM `gear_faq` `c`, '
             . '(SELECT `f`.`faq_id`, `faq_name` FROM `gear_faq` `f`, `gear_faq_l` `l` '
             . 'WHERE `l`.`faq_id`=`f`.`faq_id` AND `l`.`language_id`=' . $this->language->get('id') . ') `p` '
             . 'WHERE `p`.`faq_id`=`c`.`faq_parent_id` AND `c`.`faq_id`=' . (int)$this->uri->id;
        $record = $query->getRecord($sql);
        if ($record === false)
            throw new GSqlException();
        $data['faq_name'] = $record['faq_name'];

        return $data;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'titleEllipsis' => 80,
                  'gridId'        => 'gcontroller_faqitems_grid',
                  'width'         => $this->isInsert ? 600: 373,
                  'height'        => $this->isInsert ? 400 : 195,
                  'resizable'     => false,
                  'stateful'      => false)
        );

        // поля формы (ExtJS class "Ext.Panel")
        // поля закладки "страницы"
        $tabFaqItems = array(
            array('xtype'      => 'spinnerfield',
                  'itemCls'    => 'mn-form-item-quiet',
                  'fieldLabel' => $this->_['label_faq_index'],
                  'labelTip'   => $this->_['tip_faq_index'],
                  'name'       => 'faq_index',
                  'width'      => 70,
                  'allowBlank' => true,
                  'emptyText'  => 1),
            array('xtype'      => 'mn-field-combo',
                  'fieldLabel' => $this->_['label_faq_section'],
                  'twinTriggers'  => true,
                  'triggerWdgUrl' => $this->componentUri . '../sections/' . ROUTER_DELIMITER . 'profile/interface/?setto=1',
                  'value'      => $data['faq_name'],
                  'id'         => 'faqParent',
                  'name'       => 'faq_parent_id',
                  'editable'   => true,
                  'width'      => 250,
                  'hiddenName' => 'faq_parent_id',
                  'allowBlank' => false,
                  'store'      => array(
                      'xtype' => 'jsonstore',
                      'url'   => $this->componentUrl . 'combo/trigger/?name=sections'
                  )
            ),
            array('xtype'      => 'mn-field-chbox',
                  'itemCls'    => 'mn-form-item-quiet',
                  'fieldLabel' => $this->_['label_faq_hidden'],
                  'labelTip'   => $this->_['tip_faq_hidden'],
                  'default'    => $this->isInsert ? 0 : null,
                  'name'       => 'faq_hidden')
        );
        // если состояние формы "insert"
        if ($this->isInsert) {
            $tabFaqItems[] = array(
                'xtype'      => 'textfield',
                'fieldLabel' => $this->_['label_faq_name'],
                'name'       => 'faq_name',
                'maxLength'  => 255,
                'anchor'     => '100%',
                'allowBlank' => false
            );
            $tabFaqItems[] = array(
                'xtype'      => 'textarea',
                'itemCls'    => 'mn-form-item-quiet',
                'fieldLabel' => $this->_['label_faq_description'],
                'name'       => 'faq_description',
                'maxLength'  => 255,
                'anchor'     => '100%',
                'height'     => 70,
                'allowBlank' => true
            );
        }
        // закладка "общее"
        $tabFaq = array(
            'title'       => $this->_['title_tab_faq'],
            'layout'      => 'form',
            'baseCls'     => 'mn-form-tab-body',
            'defaultType' => 'textfield',
            'items'       => array($tabFaqItems)
        );

        // поля закладки "текст"
        $tabTextItems = array(
            array('xtype'          => 'htmleditor',
                  'hideLabel'      => true,
                  'name'           => 'faq_text',
                  'anchor'         => '100% 100%',
                  'allowBlank'     => false)
        );
        // закладка "статья"
        $tabText = array(
            'title'      => $this->_['title_tab_text'],
            'layout'     => 'anchor',
            'labelWidth' => 85,
            'baseCls'    => 'mn-form-tab-body',
            'items'      => array($tabTextItems)
        );

        // закладки окна
        $tabs = array(
            array('xtype'             => 'tabpanel',
                  'layoutOnTabChange' => true,
                  'activeTab'         => 0,
                  'enableTabScroll'   => true,
                  'anchor'            => '100% 100%',
                  'defaults'          => array('autoScroll' => true),
                  'items'             => array($tabFaq))
        );
        // если состояние формы "insert"
        if ($this->isInsert)
            $tabs[0]['items'][] = $tabText;


        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($tabs);
        $form->url = $this->componentUrl . 'profile/';

        parent::getInterface();
    }
}
?>