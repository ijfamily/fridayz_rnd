<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные для интерфейса профиля пункта раздела ЧаВо"
 * Пакет контроллеров "Профиль пункта раздела ЧаВо"
 * Группа пакетов     "ЧаВо"
 * Модуль             "ЧаВо"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_FAQItems_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Field.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Field');

/**
 * Данные для интерфейса профиля пункта раздела ЧаВо
 * 
 * @category   Gear
 * @package    GController_FAQItems_Profile
 * @subpackage Field
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Field.php2016-01-01 12:00:00 Gear Magic $
 */
final class GController_FAQItems_Profile_Field extends GController_Profile_Field
{
    /**
     * Массив полей таблицы ($_tableName)
     *
     * @var array
     */
    public $fields = array('faq_hidden');

    /**
     * Первичный ключ таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'faq_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_faq';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_faqitems_grid';
}
?>