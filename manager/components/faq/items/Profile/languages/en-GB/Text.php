<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Creating a Record "Section of Item FAQ"',
    'title_profile_update' => 'Change the record "%s"',
    // закладка "общее"
    'title_tab_faq'         => 'Common',
    'label_faq_name'        => 'Name',
    'label_faq_description' => 'Description',
    'label_faq_index'   => 'Index',
    'tip_faq_index'     => 'Index number (for list generation)',
    'label_faq_section' => 'Section',
    'label_faq_hidden'  => 'Hidden',
    'tip_faq_hidden'    => 'Hide item help topic',
    // закладка "текст"
    'title_tab_text' => 'Text'
);
?>