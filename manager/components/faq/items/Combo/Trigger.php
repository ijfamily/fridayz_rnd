<?php
/**
 * Gear Manager
 *
 * Контроллер         "Триггер выпадающего списка"
 * Пакет контроллеров "Пункты разделов ЧаВо"
 * Группа пакетов     "ЧаВо"
 * Модуль             "ЧаВо"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_FAQItems_Combo
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Combo/Trigger');

/**
 * Триггер выпадающего списка
 * 
 * @category   Gear
 * @package    GController_FAQItems_Combo
 * @subpackage Trigger
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_FAQItems_Combo_Trigger extends GController_Combo_Trigger
{
    /**
     * Вывод разделов ЧаВо
     * 
     * @return void
     */
    protected function querySections()
    {
        $table = new GDb_Table('gear_faq', 'faq_id');
        $table->setStart($this->_start);
        $table->setLimit($this->_limit);
        $sql = 'SELECT SQL_CALC_FOUND_ROWS * '
             . 'FROM (SELECT `f`.*, `l`.`faq_name` '
             . 'FROM `gear_faq_l` `l`, `gear_faq` `f` '
             . 'WHERE `l`.`faq_id`=`f`.`faq_id` AND '
             . '`l`.`language_id`=' . $this->language->get('id') . ') `t` '
             . 'WHERE `faq_parent_id` IS NULL ' . ($this->_query ? "AND `faq_name` LIKE '{$this->_query}%' " : '')
             . 'ORDER BY `faq_name` ASC '
             . 'LIMIT %limit';
        $table->query($sql);
        $data = array();
        while (!$table->query->eof()) {
            $record = $table->query->next();
            $data[] = array(
                'id'   => $record['faq_id'],
                'name' => $record['faq_name']
            );
        }
        $this->response->set('totalCount', $table->query->getFoundRows());
        $this->response->success = true;
        $this->response->data = $data;
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $name название триггера
     * @return void
     */
    protected function dataView($name)
    {
        parent::dataView($name);

        // имя триггера
        switch ($name) {
            // разделы ЧаВо
            case 'sections': $this->querySections(); break;
        }
    }
}
?>