<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Пункты разделов ЧаВо"
 * Группа пакетов     "ЧаВо"
 * Модуль             "ЧаВо"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_FAQItems
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Пункты разделов ЧаВо
 * Описание: Пункты разделов ЧаВо
 * ID класса: gcontroller_faqitems_grid
 * Группа: ЧаВо
 * Очищать: нет
 * Ресурс
 *    Пакет: faq/items/
 *    Контроллер: faq/items/Grid/
 *    Интерфейс: grid/interface/
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске: нет
 */

return array(
    // {Y}- модуль "FAQ" {Config} - пакет контроллеров "Items" -> FAQItems
    'clsPrefix' => 'FAQItems'
);
?>