<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск пункта раздела ЧаВо"
 * Пакет контроллеров "Пункты разделов ЧаВо"
 * Группа пакетов     "ЧаВо"
 * Модуль             "ЧаВо"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_FAQItems_Search
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Interface');

/**
 * Поиск пункта раздела ЧаВо
 * 
 * @category   Gear
 * @package    GController_FAQItems_Search
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_FAQItems_Search_Interface extends GController_Search_Interface
{
    /**
     * Идентификатор компонента
     *
     * @var string
     */
    public $gridId = 'gcontroller_faqitems_grid';

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // Ext_Window (ExtJS class "Manager.window.DataSearch")
        $this->_cmp->setProps(
            array('title'       => $this->_['title_search'],
                  'gridId'      => $this->gridId,
                  'btnSearchId' => $this->gridId . '-bnt_search',
                  'url'         => $this->componentUrl . 'search/data/')
        );

        parent::getInterface();
    }
}
?>