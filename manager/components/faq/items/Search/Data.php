<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск пункта раздела ЧаВо"
 * Пакет контроллеров "Пункты разделов ЧаВо"
 * Группа пакетов     "ЧаВо"
 * Модуль             "ЧаВо"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_FAQItems_Search
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск пункта раздела ЧаВо
 * 
 * @category   Gear
 * @package    GController_FAQItems_Search
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_FAQItems_Search_Data extends GController_Search_Data
{
    /**
     * Идентификатор компонента
     *
     * @var string
     */
    public $gridId = 'gcontroller_faqitems_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_faqitems_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор
     * Используется для инициализации атрибутов контроллер не переданного в конструктор
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'faq_index', 'label' => $this->_['header_faq_index']),
            array('dataIndex' => 'faq_name', 'label' => $this->_['header_faq_name']),
            array('dataIndex' => 'faq_description', 'label' => $this->_['header_faq_description']),
            array('dataIndex' => 'pfaq_name', 'label' => $this->_['header_faq_section']),
            array('dataIndex' => 'faq_hidden', 'label' => $this->_['header_faq_hidden'])
        );
    }
}
?>