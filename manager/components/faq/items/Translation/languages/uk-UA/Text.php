<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_translation_insert' => 'Створення транслятора запису (%s) "Пункт розділу ЧаВо"',
    'title_translation_update' => 'транслювання запису (%s) "%s"',
    // закладка "общее"
    'title_tab_faq'         => 'Загальне',
    'label_faq_name'        => 'Назва',
    'label_faq_description' => 'Опис',
    // закладка "текст"
    'title_tab_text' => 'Текст'
);
?>