<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_translation_insert' => 'Creating a Text records (%s) "Paragraph of section FAQ"',
    'title_translation_update' => 'Translate record (%s) "%s"',
    // закладка "общее"
    'title_tab_faq'         => 'Common',
    'label_faq_name'        => 'Name',
    'label_faq_description' => 'Description',
    // закладка "текст"
    'title_tab_text' => 'Text'
);
?>