<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные интерфейса локализации пунктов разделов ЧаВо"
 * Пакет контроллеров "Пункты разделов ЧаВо"
 * Группа пакетов     "ЧаВо"
 * Модуль             "ЧаВо"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_FAQItems_Translation
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные интерфейса локализации пунктов разделов ЧаВо
 * 
 * @category   Gear
 * @package    GController_FAQItems_Translation
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_FAQItems_Translation_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($_tableName)
     *
     * @var array
     */
    public $fields = array('faq_id', 'language_id', 'faq_name', 'faq_description', 'faq_text');

    /**
     * Первичный ключ таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'faq_lang_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_faq_l';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_faqitems_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_translation_update'], $record['language_alias'], $record['faq_name']);
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        $sql = 'SELECT `f`.*, `l`.`language_alias` FROM `gear_faq_l` `f`, `gear_languages` `l` '
             . 'WHERE `l`.`language_id`=`f`.`language_id` AND `f`.`faq_lang_id`=' . $this->uri->id;

        parent::dataView($sql);
    }
}
?>