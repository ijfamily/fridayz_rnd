<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Розділи ЧпВi',
    'tooltip_grid' => 'разделы модуля "Часто задаваемые вопросы"',
    'rowmenu_edit' => 'Редагувати',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Стовпці',
    'title_buttongroup_filter' => 'Фільтр',
    'msg_btn_clear'            => 'Ви дійсно бажаєте видалити всі записи <span class="mn-msg-delete">("Розділи ЧаВо", "Пункти розділів ЧаВо")</span> ?',
    // столбцы
    'header_faq_index'  => '№',
    'tooltip_faq_index' => 'Порядковий номер',
    'header_faq_name'   => 'Назва',
    'header_faq_hidden' => 'Прихован'
);
?>