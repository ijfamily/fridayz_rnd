<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка разделов ЧаВо"
 * Пакет контроллеров "Разделы ЧаВо"
 * Группа пакетов     "ЧаВо"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_FAQSections_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные списка разделов ЧаВо
 * 
 * @category   Gear
 * @package    GController_FAQSections_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_FAQSections_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'faq_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_faq';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS * '
          . 'FROM `gear_faq` `f`, `gear_faq_l` `l` '
          . 'WHERE `f`.`faq_id`=`l`.`faq_id` AND '
          . '`l`.`language_id`=' . $this->language->get('id') . ' AND `f`.`faq_parent_id` IS NULL %filter '
          . 'ORDER BY %sort '
          . 'LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // идент. раздела
            'faq_id' => array('type' => 'integer'),
            // порядковый номер
            'faq_index' => array('type' => 'integer'),
            // название
            'faq_name' => array('type' => 'string'),
            // скрыть раздел
            'faq_hidden' => array('type' => 'integer')
        );
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        // удаление справки ("gear_faq")
        $query = new GDb_Query();
        $sql = 'DELETE FROM `gear_faq`';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_faq` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление языков справки ("gear_faq_l")
        $query = new GDb_Query();
        $sql = 'DELETE FROM `gear_faq_l`';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_faq_l` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        $query = new GDb_Query();
        // удаление пунктов разделов из пакета локализации языков ("gear_faq_l")
        $sql = 'DELETE `l` FROM `gear_faq_l` `l`, `gear_faq` `f` '
             . 'WHERE `f`.`faq_id`=`l`.`faq_id` AND `f`.`faq_parent_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление разделов из пакета локализации языков ("gear_faq_l")
        $sql = 'DELETE `l` FROM `gear_faq_l` `l`, `gear_faq` `f` '
             . 'WHERE `f`.`faq_id`=`l`.`faq_id` AND `f`.`faq_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление пунктов разделов ("gear_faq")
        $sql = 'DELETE FROM `gear_faq` WHERE `faq_parent_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }
}
?>