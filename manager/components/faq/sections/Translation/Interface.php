<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс локализации раздела ЧаВо"
 * Пакет контроллеров "Локализации раздела ЧаВо"
 * Группа пакетов     "ЧаВо"
 * Модуль             "ЧаВо"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_FAQSections_Translation
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс локализации раздела ЧаВо
 * 
 * @category   Gear
 * @package    GController_FAQSections_Translation
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_FAQSections_Translation_Interface extends GController_Translation_Interface
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_faqsections_grid';

    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        $data = array('faq_id' => $this->uri->id, 'language_alias' =>'', 'record_id' => '');

        // соединение с базой данных
        GFactory::getDb()->connect();
        $query = new GDb_Query();
        $sql = 'SELECT * FROM `gear_faq_l` WHERE `faq_id`=' . (int)$this->uri->id . ' AND `language_id`=' . $this->_languageId;
        $record = $query->getRecord($sql);
        if ($record === false)
            throw new GException('Data processing error', 'Query error (Internal Server Error)', $query->getError(true), false);
        // если есть запись
        if (!empty($record))
            $data['record_id'] = $record['faq_lang_id'];
        // выбранный язык
        $language = $this->language->getLanguagesBy('id', $this->_languageId);
        $data['language_alias'] = $language['alias'];

        return $data;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();
        $this->uri->id = $data['record_id'];

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => sprintf($this->_['title_translation_insert'], $data['language_alias']),
                  'titleEllipsis' => 80,
                  'gridId'        => 'gcontroller_faqsections_grid',
                  'width'         => 400,
                  'height'        => 110,
                  'resizable'     => false,
                  'stateful'      => false,
                  'iconCls'       => 'icon-form-translate')
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_faq_name'],
                  'name'       => 'faq_name',
                  'maxLength'  => 255,
                  'anchor'     => '100%',
                  'allowBlank' => false)
        );
        // если состояние формы "вставка"
        if (empty($data['record_id'])) {
            $items[] = array(
                'xtype' => 'hidden',
                'value' => $this->_languageId,
                'name'  => 'language_id'
            );
            $items[] = array(
                'xtype' => 'hidden',
                'value' => $data['faq_id'],
                'name'  => 'faq_id'
            );
        }

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->addItems($items);
        $form->url = $this->componentUrl . 'translation/';

        parent::getInterface();
    }
}
?>