<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные интерфейса локализации раздела ЧаВо"
 * Пакет контроллеров "Профиль раздела ЧаВо"
 * Группа пакетов     "ЧаВо"
 * Модуль             "ЧаВо"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_FAQSections_Translation
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные интерфейса локализации разделов ЧаВо
 * 
 * @category   Gear
 * @package    GController_FAQSections_Translation
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_FAQSections_Translation_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array('faq_id', 'language_id', 'faq_name');

    /**
     * Первичный ключ таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'faq_lang_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_faq_l';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_faqsections_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_translation_update'], $record['language_alias'], $record['faq_name']);
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        $sql = 'SELECT `f`.*, `l`.`language_alias` FROM `gear_faq_l` `f`, `gear_languages` `l` '
             . 'WHERE `l`.`language_id`=`f`.`language_id` AND `f`.`faq_lang_id`=' . $this->uri->id;

        parent::dataView($sql);
    }
}
?>