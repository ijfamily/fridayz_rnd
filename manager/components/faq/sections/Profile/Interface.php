<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля раздела ЧаВо"
 * Пакет контроллеров "Профиль раздела ЧаВо"
 * Группа пакетов     "ЧаВо"
 * Модуль             "ЧаВо"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_FAQSections_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля раздела ЧаВо
 * 
 * @category   Gear
 * @package    GController_FAQSections_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_FAQSections_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        $data = array('faq_name' => '');
        // если состояние формы "insert"
        if ($this->isInsert) return $data;

        // соединение с базой данных
        GFactory::getDb()->connect();
        $query = new GDb_Query();
        // выбранный раздел
        $sql = 'SELECT `p`.`faq_name` '
             . 'FROM `gear_faq` `c`, '
             . '(SELECT `f`.`faq_id`, `faq_name` FROM `gear_faq` `f`, `gear_faq_l` `l` '
             . 'WHERE `l`.`faq_id`=`f`.`faq_id` AND `l`.`language_id`=' . $this->language->get('id') . ') `p` '
             . 'WHERE `p`.`faq_id`=`c`.`faq_parent_id` AND `c`.`faq_id`=' . (int)$this->uri->id;
        $record = $query->getRecord($sql);
        if ($record === false)
            throw new GSqlException();
        $data['faq_name'] = $record['faq_name'];

        return $data;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'id'            => $this->classId,
                  'titleEllipsis' => 80,
                  'gridId'        => 'gcontroller_faqsections_grid',
                  'width'         => 400,
                  'height'        => $this->isInsert ? 150 : 122,
                  'resizable'     => false,
                  'stateful'      => false)
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype'      => 'spinnerfield',
                  'itemCls'    => 'mn-form-item-quiet',
                  'fieldLabel' => $this->_['label_faq_index'],
                  'labelTip'   => $this->_['tip_faq_index'],
                  'name'       => 'faq_index',
                  'width'      => 70,
                  'allowBlank' => true,
                  'emptyText'  => 1),
            array('xtype'      => 'mn-field-chbox',
                  'itemCls'    => 'mn-form-item-quiet',
                  'fieldLabel' => $this->_['label_faq_hidden'],
                  'labelTip'   => $this->_['tip_faq_hidden'],
                  'default'    => $this->isInsert ? 0 : null,
                  'name'       => 'faq_hidden')
        );
        // если состояние формы "insert"
        if ($this->isInsert) {
            $items[] = array(
                'xtype'      => 'textfield',
                'fieldLabel' => $this->_['label_faq_name'],
                'name'       => 'faq_name',
                'maxLength'  => 255,
                'anchor'     => '100%',
                'allowBlank' => false
            );
        }

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->addItems($items);
        $form->url = $this->componentUrl . 'profile/';

        parent::getInterface();
    }
}
?>