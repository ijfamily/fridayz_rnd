<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные для интерфейса профиля раздела ЧаВо"
 * Пакет контроллеров "Профиль раздела ЧаВо"
 * Группа пакетов     "ЧаВо"
 * Модуль             "ЧаВо"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_FAQSections_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные для интерфейса профиля раздела ЧаВо
 * 
 * @category   Gear
 * @package    GController_FAQSections_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_FAQSections_Profile_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array('faq_hidden', 'faq_index');

    /**
     * Первичный ключ таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'faq_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_faq';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_faqsections_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_profile_update'], $record['faq_name']);
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        $sql = 'SELECT `f`.*, `l`.`faq_name` '
             . 'FROM `gear_faq_l` `l`, `gear_faq` `f` '
             . 'WHERE `l`.`faq_id`=`f`.`faq_id` AND '
             . '`l`.`language_id`=' . $this->language->get('id') . ' AND '
             . '`f`.`faq_id`=' . $this->uri->id;

        parent::dataView($sql);
    }

    /**
     * Событие наступает после успешного добавления данных
     * 
     * @param array $data сформированные данные полученные после запроса к таблице
     * @return void
     */
    protected function dataInsertComplete($data = array())
    {
        $data = $this->input->getBy(array('faq_name', 'faq_description'));
        $data['faq_id'] = $this->_recordId;
        $table = new GDb_Table('gear_faq_l', 'faq_lang_id');
        $langs = $this->session->get('language/list');
        $count = count($langs);
        for ($i = 0; $i < $count; $i++) {
            $data['language_id'] = $langs[$i]['language_id'];
            $table->insert($data);
        }

        // если необходимо сделать подстановку
        if ($this->uri->getVar('setto', false)) {
            $this->response->add('setTo', array(
                array('id' => 'faqParent', 'value' => $this->_recordId, 'text' => $data['faq_name'])
            ));
        }
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        $query = new GDb_Query();
        // удаление пунктов разделов из пакета локализации языков ("gear_faq_l")
        $sql = 'DELETE `l` FROM `gear_faq_l` `l`, `gear_faq` `f` '
             . 'WHERE `f`.`faq_id`=`l`.`faq_id` AND `f`.`faq_parent_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление разделов из пакета локализации языков ("gear_faq_l")
        $sql = 'DELETE `l` FROM `gear_faq_l` `l`, `gear_faq` `f` '
             . 'WHERE `f`.`faq_id`=`l`.`faq_id` AND `f`.`faq_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление пунктов разделов ("gear_faq")
        $sql = 'DELETE FROM `gear_faq` WHERE `faq_parent_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }
}
?>