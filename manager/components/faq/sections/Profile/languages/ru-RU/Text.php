<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Создание "Раздел ЧаВо"',
    'title_profile_update' => 'Изменение записи "%s"',
    // вкладка "общее"
    'label_faq_name'   => 'Название',
    'label_faq_index'  => 'Индекс',
    'tip_faq_index'    => 'Порядковый номер (для формирования списков)',
    'label_faq_hidden' => 'Скрыть',
    'tip_faq_hidden'   => 'Скрыть пункт раздела справки',
    // вкладка "текст"
    'title_tab_text' => 'Текст'
);
?>