<?php
/**
 * Gear Manager
 *
 * Контроллер         'Интерфейс профиля общих настроеек форм'
 * Пакет контроллеров 'Общие настройки форм'
 * Группа пакетов     'Настройки'
 * Модуль             'Сайт'
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SConfForms_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля общих настроеек форм
 * 
 * @category   Gear
 * @package    GController_SConfForms_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SConfForms_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // Ext_Window (ExtJS class 'Manager.window.DataProfile')
        $this->_cmp->setProps(
            array('title'           => $this->_['title_profile_update'],
                  'titleEllipsis'   => 40,
                  'width'           => 640,
                  'autoHeight'      => true,
                  'resizable'       => false,
                  'btnDeleteHidden' => true,
                  'stateful'        => false)
        );
        $this->_cmp->state = 'update';

        $items = array(
            array('xtype' => 'label',
                  'html'  => $this->_['html_desc']),
            array('xtype'      => 'fieldset',
                  'labelWidth' => 147,
                  'width'      => '96%',
                  'title'      => $this->_['fs_spam'],
                  'items'      => array(
                      array('xtype'      => 'checkbox',
                            'fieldLabel' => $this->_['label_check_spam'],
                            'labelTip'   => $this->_['tip_check_spam'],
                            'name'       => 'settings[CHECK_SPAM]',
                            'checkDirty' => false,
                            'default'    => 0),
                      array('xtype'      => 'checkbox',
                            'fieldLabel' => $this->_['label_check_captcha'],
                            'labelTip'   => $this->_['tip_check_captcha'],
                            'name'       => 'settings[CHECK_CAPTCHA]',
                            'checkDirty' => false,
                            'default'    => 0),
                      array('xtype'      => 'numberfield',
                            'fieldLabel' => $this->_['label_waiting_time'],
                            'labelTip'   => $this->_['tip_waiting_time'],
                            'name'       => 'settings[WAITING_TIME]',
                            'checkDirty' => false,
                            'width'      => 100,
                            'value'      => 0),
                      array('xtype'      => 'numberfield',
                            'fieldLabel' => $this->_['label_max_messages'],
                            'labelTip'   => $this->_['tip_max_messages'],
                            'name'       => 'settings[MAX_MESSAGES]',
                            'checkDirty' => false,
                            'width'      => 100,
                            'value'      => 0)
                  )
            ),
            array('xtype'      => 'fieldset',
                  'labelWidth' => 145,
                  'width'      => '96%',
                  'title'      => $this->_['fs_sms'],
                  'items'      => array(
                      array('xtype'      => 'checkbox',
                            'fieldLabel' => $this->_['label_send_sms'],
                            'labelTip'   => $this->_['tip_send_sms'],
                            'name'       => 'settings[SEND_SMS]',
                            'checkDirty' => false,
                            'default'    => 0),
                      array('xtype'      => 'textfield',
                            'fieldLabel' => $this->_['label_sms_url'],
                            'labelTip'   => $this->_['tip_sms_url'],
                            'name'       => 'settings[SMS_URL]',
                            'checkDirty' => false,
                            'width'      => '96%')
                  )
            ),
            array('xtype'      => 'fieldset',
                  'labelWidth' => 145,
                  'width'      => '96%',
                  'title'      => $this->_['fs_mails'],
                  'items'      => array(
                      array('xtype'      => 'checkbox',
                            'fieldLabel' => $this->_['label_send_mail'],
                            'labelTip'   => $this->_['tip_send_mail'],
                            'name'       => 'settings[SEND_MAIL]',
                            'checkDirty' => false,
                            'default'    => 0),
                      array('xtype'      => 'textfield',
                            'fieldLabel' => $this->_['label_mail_subject'],
                            'labelTip'   => $this->_['tip_mail_subject'],
                            'name'       => 'settings[MAIL_SUBJECT]',
                            'checkDirty' => false,
                            'width'      => '96%'),
                      array('xtype'      => 'textfield',
                            'fieldLabel' => $this->_['label_emails'],
                            'labelTip'   =>$this->_['tip_emails'],
                            'name'       => 'settings[EMAILS]',
                            'checkDirty' => false,
                            'width'      => '96%')
                  )
            ),
            array('xtype'      => 'fieldset',
                  'labelWidth' => 145,
                  'width'      => '96%',
                  'title'      => $this->_['fs_success'],
                  'items' => array(
                      array('xtype'      => 'textfield',
                            'fieldLabel' => $this->_['label_location'],
                            'labelTip'   => $this->_['tip_location'],
                            'name'       => 'settings[LOCATION]',
                            'checkDirty' => false,
                            'width'      => '96%'),
                      array('xtype'      => 'textfield',
                            'fieldLabel' => $this->_['label_msg_success'],
                            'labelTip'   => $this->_['tip_msg_success'],
                            'name'       => 'settings[MSG_SUCCESS]',
                            'checkDirty' => false,
                            'width'      => '96%')
                  )
            )
        );


        // форма (ExtJS class 'Manager.form.DataProfile')
        $form = $this->_cmp->items->get(0);
        $form->url = $this->componentUrl . 'profile/';
        $form->labelWidth = 270;
        $form->items->addItems($items);

        parent::getInterface();
    }
}
?>