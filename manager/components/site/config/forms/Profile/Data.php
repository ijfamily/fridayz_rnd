<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля общих настроеек форм"
 * Пакет контроллеров "Общие настройки форм"
 * Группа пакетов     "Настройки"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SConfForms_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

define('_INC', 1);

Gear::controller('Profile/Data');

/**
 * Возращает шаблон общих настроеек форм
 * 
 * @params string $tpl данные в шаблоне
 */
function getTplSiteConfig($tpl)
{
    return <<<TEXT
<?php
/**
 * Gear CMS
 *
 * Общие настройки форм (создан: {$tpl['date']})
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 *
 * @category   Gear
 * @package    Config
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    \$Id: Forms.php {$tpl['date']} Gear Magic \$
 */

return array(
    // Проверка на СПАМ
    // проверка ip адреса
    'CHECK_SPAM'    => {$tpl['CHECK_SPAM']},
    // проверка "капчи"
    'CHECK_CAPTCHA' => {$tpl['CHECK_CAPTCHA']},
    // время простоя (сек.)
    'WAITING_TIME'  => {$tpl['WAITING_TIME']},
    // количество сообщений
    'MAX_MESSAGES'  => {$tpl['MAX_MESSAGES']},

    // SMS сообщение на телефон
    // отправить SMS сообщение на телефон
    'SEND_SMS' => {$tpl['SEND_SMS']},
    // ресурс для отправки sms сообщений
    'SMS_URL'  => '{$tpl['SMS_URL']}',

    // Сообщение на e-mail
    // Отправить сообщение на e-mail
    'SEND_MAIL'    => {$tpl['SEND_MAIL']},
    // Тема письма приходящего от клиента
    'MAIL_SUBJECT' => '{$tpl['MAIL_SUBJECT']}',
    // E-mail адрес для отправленных сообщений
    'EMAILS'       => '{$tpl['EMAILS']}',

    // После успешной обработки формы
    // переход по адресу
    'LOCATION'    => '{$tpl['LOCATION']}',
    // сообщение пользователю после успешной обработки формы
    'MSG_SUCCESS' => '{$tpl['MSG_SUCCESS']}'
);
?>
TEXT;
}

/**
 * Данные профиля общих настроеек форм
 * 
 * @category   Gear
 * @package    GController_SConfForms_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SConfForms_Profile_Data extends GController_Profile_Data
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sconfforms_profile';

    /**
     * Файл настроек роботов
     *
     * @var string
     */
    public $filename = 'Forms.php';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return $this->_['profile_title_update'];
    }

    /**
     * Обновление настроеек сайта
     * 
     * @return void
     */
    protected function fileUpdate($params)
    {
        // запись для CMS
        if (file_put_contents('../' . PATH_CONFIG_CMS . $this->filename, getTplSiteConfig($params), FILE_TEXT) === false)
            throw new GException('Error', sprintf($this->_['msg_create_file'], $this->filename));
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        $params['date'] = date('Y-m-d H:i:s');
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        parent::dataAccessUpdate();

        if ($this->input->isEmpty('PUT'))
            throw new GException('Warning', 'To execute a query, you must change data!');
        $params = $this->input->get('settings', array());
        // проверки входных данных
        $this->isDataCorrect($params);
        // обновить файл конфигурации
        $this->fileUpdate($params);
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($params, 'method "PUT"');
        }
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        parent::dataAccessView();

        $setTo = array();

        $filename = '../' . PATH_CONFIG_CMS . $this->filename;
        // если файл настроек сайта не существует
        if (!file_exists($filename))
            throw new GException('Error', sprintf($this->_['msg_file_exist'], $filename));

        // настройки форм
        $arrSettings = include($filename);
        $settings = array();
        foreach ($arrSettings as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $subkey => $subvalue) {
                    $settings['settings[' . $key . '/' . $subkey . ']'] = $subvalue;
                }
            } else
                $settings['settings[' . $key . ']'] = $value;
        }

        $this->response->data = $settings;
    }
}
?>