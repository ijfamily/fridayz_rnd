<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_update' => 'Общие настройки форм',
    // поля
    'html_desc'           => '<note>Это общие настройки для форм, которые не были добавлены в статьи через редактор компонента "Статьи  сайта" и не имеют индивидуальных настроек<br><br></note>',
    'label_check_spam'    => 'Проверка ip адреса<hb></hb>',
    'tip_check_spam'      => 'Проверка ip адреса пользователя в базе данных на СПАМ',
    'label_check_captcha' => 'Проверка &laquo;капчи&raquo;<hb></hb>',
    'tip_check_captcha'   => 'Проверка &laquo;капчи&raquo; (символьный код пользователя)',
    'label_waiting_time'  => 'Время простоя (сек.)<hb></hb>',
    'tip_waiting_time'    => 'Время в течении которого будет отправлены сообщения, будет считаться СПАМом',
    'label_max_messages'  => 'Количество сообщений<hb></hb>',
    'tip_max_messages' => 'Количество сообщений которые может отправить пользователь в течении сессии сайта',
    'label_send_sms'   => 'SMS сообщение<hb></hb>',
    'tip_send_sms'     => 'Отправить SMS сообщение на телефон',
    'label_sms_url'    => 'SMS ресурс<hb></hb>',
    'tip_sms_url'      => 'Ресурс для отправки sms сообщений',
    'label_send_mail'    => 'Сообщение на e-mail<hb></hb>',
    'tip_send_mail'      => 'Отправить сообщение на e-mail',
    'label_mail_subject' => 'Тема письма<hb></hb>',
    'tip_mail_subject'   => 'Тема письма приходящего от клиента',
    'tip_emails'         => 'E-mail адрес для отправленных сообщений (через \';\')',
    'label_emails'      => 'Кому отправлять<hb></hb>',
    'tip_location'      => 'При успешной обработке сообщения перейти по указанному адресу',
    'label_location'    => 'Переход по адресу<hb></hb>',
    'tip_msg_success'   => 'Сообщение пользователю после успешной обработки формы',
    'label_msg_success' => 'Сообщение<hb></hb>',
    'fs_spam'    => 'Проверка на СПАМ',
    'fs_sms'     => 'SMS сообщение на телефон',
    'fs_mails'   => 'Сообщение на e-mail',
    'fs_success' => 'После успешной обработки формы',
    // сообщения
    'msg_file_exist' => 'Невозможно открыть файл настроек "%s"'
);
?>