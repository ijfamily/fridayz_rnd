<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля подключение нескольких доменов"
 * Пакет контроллеров "Подключение нескольких доменов"
 * Группа пакетов     "Настройки"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SDomains_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля подключение нескольких доменов
 * 
 * @category   Gear
 * @package    GController_SDomains_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SDomains_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // Ext_Window (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'           => $this->_['title_profile_update'],
                  'titleEllipsis'   => 40,
                  'width'           => 750,
                  'autoHeight'      => true,
                  'resizable'       => false,
                  'btnDeleteHidden' => true,
                  'stateful'        => false,
                  'buttonsAdd'      => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-site-domains-config',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );
        $this->_cmp->state = 'update';

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->url = $this->componentUrl . 'profile/';
        $form->items->addItems(
            array(
                array('xtype'     => 'mn-field-grid-editor',
                      'id'        => 'list_domains',
                      'name'      => 'list_domains',
                      'hideLabel' => true,
                      'anchor'    => '100%',
                      'height'    => 250,
                      'checkDirty' => false,
                      'fields'    => array('domain', 'desc', 'code', 'title'),
                      'columns'   => array(
                          array('header'    => $this->_['header_domain'],
                                'dataIndex' => 'domain',
                                'width'     => 170,
                                'sortable'  => true),
                          array('header'    => $this->_['header_desc'],
                                'dataIndex' => 'desc',
                                'width'     => 200,
                                'sortable'  => true),
                          array('header'    => $this->_['header_title'],
                                'dataIndex' => 'title',
                                'width'     => 200,
                                'sortable'  => true),
                          array('header'    => $this->_['header_code'],
                                'dataIndex' => 'code',
                                'width'     => 50,
                                'sortable'  => true)
                      )
                )
            )
        );

        parent::getInterface();
    }
}
?>