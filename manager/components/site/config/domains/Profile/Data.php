<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля подключение нескольких доменов"
 * Пакет контроллеров "Подключение нескольких доменов"
 * Группа пакетов     "Настройки"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SDomains_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

define('_INC', 1);

Gear::controller('Profile/Data');

/**
 * Возращает шаблон настроек доменов
 * 
 * @params string $tpl данные в шаблоне
 */
function getTplSiteConfig($tpl)
{
    return <<<TEXT
<?php
/**
 * Gear CMS
 *
 * Настройки подключения нескольких доменов (создан: {$tpl['date']})
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 *
 * @category   Gear
 * @package    Config
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    \$Id: Domains.php {$tpl['date']} Gear Magic \$
 */

{$tpl['domains']}
?>
TEXT;
}

/**
 * Данные профиля подключение нескольких доменов
 * 
 * @category   Gear
 * @package    GController_SDomains_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SDomains_Profile_Data extends GController_Profile_Data
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sdomains_profile';

    /**
     * Файл настроек роботов
     *
     * @var string
     */
    public $fileDomains = 'Domains.php';

    /**
     * Массив полей таблицы ($_tableName)
     *
     * @var array
     */
    public $fields = array('list_domains');

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return $this->_['profile_title_update'];
    }

    /**
     * Обновление настроеек сайта
     * 
     * @return void
     */
    protected function fileUpdate($params)
    {
        // запись для CMS
        if (file_put_contents('../' . PATH_CONFIG_CMS . $this->fileDomains, getTplSiteConfig($params), FILE_TEXT) === false)
            throw new GException('Error', sprintf($this->_['msg_create_file'], $this->filename));
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        if (empty($params['list_domains']))
            $tpl = 'return array();';
        else {
            $domains = json_decode($params['list_domains'], true);
            $tplI = $tplD = '';
            for ($i = 0; $i < sizeof($domains); $i++) {
                $tplI .= "'" . $domains[$i]['code'] . "' => array('" . $domains[$i]['domain'] . "', '" . addslashes($domains[$i]['desc']) . "', '" . addslashes($domains[$i]['title']) . "')";
                $tplD .= "'" . $domains[$i]['domain'] . "' => '" .$domains[$i]['code'] . "'";
                if ($i < sizeof($domains) - 1) {
                    $tplI .= ", ";
                    $tplD .= ", ";
                }
            }
            $tpl = "return array(\n    'indexes' => array($tplI),\n    'domains' => array($tplD)\n);";
        }
        $params['domains'] = $tpl;
        $params['date'] = date('Y-m-d H:i:s');
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        parent::dataAccessUpdate();

        // массив полей с их соответствующими значениями
        if (empty($params))
            $params = $this->input->getBy($this->fields);
        // проверки входных данных
        $this->isDataCorrect($params);
        // проверка существования записи с одинаковыми значениями
        $this->isDataExist($params);
        // использование системных полей в запросе SQL
        if ($this->isSysFields) {
            $params['sys_date_update'] = date('Y-m-d');
            $params['sys_time_update'] = date('H:i:s');
            $params['sys_user_update'] = $this->session->get('user_id');
        }
        // обновить файл конфигурации
        $this->fileUpdate($params);
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($params, 'method "PUT"');
        }
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        parent::dataAccessView();

        $setTo = array();

        $filename = '../' . PATH_CONFIG_CMS . $this->fileDomains;
        // если файл настроек сайта не существует
        if (!file_exists($filename))
            throw new GException('Error', sprintf($this->_['msg_file_exist'], $filename));

        // настройки доменов
        $settings = include($filename);
        if ($settings) {
            $list = array();
            foreach ($settings['indexes'] as $key => $item) {
                $list[] = array('domain' => $item[0], 'desc' => $item[1], 'title' => $item[2], 'code' => $key);
            }
        } else
            $list = array();

        $this->response->add('setTo', array(array('id' => 'list_domains', 'value' => json_encode($list))));
    }
}
?>