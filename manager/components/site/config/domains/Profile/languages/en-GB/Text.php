<?php
/**
 * Gear
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_update' => 'System settings',
    // поля формы
    'title_tab_interface'  => 'Personalization',
    'title_tab_control'    => 'Control',
    'label_footer'         => 'Settings will take effect only after the changes made',
    'label_type_shortcuts' => 'View',
    'title_shortcuts'      => 'Desktop shortcuts',
    'label_desktop_view'   => 'Desktop view',
    'label_view'           => 'View',
    'label_theme'          => 'Theme',
    'label_separator_debug' => 'Debug',
    'label_debug_js_core'   => 'debugging kernel (JavaScript)',
    'label_debug_js'        => 'debugging widgets and plugins system (JavaScript)',
    'label_debug_php'       => 'debugging system components (PHP)',
    'label_debug_php_errors' => 'error output (PHP)',
    'label_debug_info'       => '<div class="mn-field-comment">For debugging PHP scripts set for Mozilla Firefox extension <a target="blank" href="http://www.firephp.org/">FirePHP</a>, <br>'
                             . 'for debug JavaScript - <a target="blank" href="http://www.getfirebug.com/">Firebug</a>',
    'label_separator_msg'     => 'Messages',
    'label_receive_mails'     => 'receive system emails from users',
    'label_receive_messages'  => 'receive system messages from users',
    'label_receive_js_errors' => 'receive error messages JavaScript',
    'label_sound'             => 'Sound',
    'label_separator_grid'    => 'List management',
    'label_grid_columns'      => 'reset the position of the columns',
    'label_grid_bg_use'       => 'use a background image list',
    'label_grid_bg_color'     => 'the background color of the list (if there is no picture)',
    'title_tab_date'          => 'Date and time',
    'label_separator_date'    => 'Date template',
    'fieldset_title_day'      => 'Day',
    'label_char_d'            => 'Character <b>"d"</b>',
    'value_char_d'            => 'Day of the month, 2 digits with leading zeros<br>example: 01 to 31',
    'label_char_D'            => 'Character <b>"D"</b>',
    'value_char_D'            => 'A textual representation of a day, three letters<br>example: Mon through Sun',
    'label_char_j'            => 'Character <b>"j"</b>',
    'value_char_j'            => 'Day of the month without leading zeros<br>example: 1 to 31',
    'label_char_L'            => 'Character <b>"L"</b>',
    'value_char_L'            => 'A full textual representation of the day of the week<br>example: Sunday through Saturday',
    'label_char_N'            => 'Character <b>"N"</b>',
    'value_char_N'            => 'ISO-8601 numeric representation of the day of the week<br>example: 1 (for Monday) through 7 (for Sunday)',
    'label_char_S'            => 'Character <b>"S"</b>',
    'value_char_S'            => 'English ordinal suffix for the day of the month, 2 characters<br>example: st, nd, rd or th. Works well with j ',
    'label_char_W'            => 'Character <b>"W"</b>',
    'value_char_W'            => 'Numeric representation of the day of the week<br>example: 0 (for Sunday) through 6 (for Saturday)',
    'label_char_z'            => 'Character <b>"z"</b>',
    'value_char_z'            => 'The day of the year (starting from 0)<br>example: 0 through 365',
    'fieldset_title_week'     => 'Week',
    'label_char_W'            => 'Character <b>"W"</b>',
    'value_char_W'            => 'ISO-8601 week number of year, weeks starting on Monday<br>example: 42 (the 42nd week in the year)',
    'fieldset_title_month'    => 'Month',
    'label_char_F'            => 'Character <b>"F"</b>',
    'value_char_F'            => 'A full textual representation of a month, such as January or March<br>example: January through December',
    'label_char_m'            => 'Character <b>"m"</b>',
    'value_char_m'            => 'Numeric representation of a month, with leading zeros<br>example: 01 through 12',
    'label_char_M'            => 'Character <b>"M"</b>',
    'value_char_M'            => 'A short textual representation of a month, three letters<br>example: Jan through Dec',
    'label_char_n'            => 'Character <b>"n"</b>',
    'value_char_n'            => 'Numeric representation of a month, without leading zeros<br>example: 1 through 12',
    'label_char_t'            => 'Character <b>"t"</b>',
    'value_char_t'            => 'Number of days in the given month<br>example: 28 through 31',
    'fieldset_title_year'     => 'Year',
    'label_char_L'            => 'Character <b>"L"</b>',
    'value_char_L'            => 'Whether it\'s a leap year<br>example: 1 if it is a leap year, 0 otherwise.',
    'label_char_O'            => 'Character <b>"O"</b>',
    'value_char_O'            => 'ISO-8601 year number. This has the same value as Y, except that if the ISO week number (W) belongs to the previous or next year, that year is used instead. <br>example: 1999 or 2003',
    'label_char_Y'            => 'Character <b>"Y"</b>',
    'value_char_Y'            => 'A full numeric representation of a year, 4 digits<br>example: 1999 or 2003',
    'label_char_y'            => 'Character <b>"y"</b>',
    'value_char_y'            => 'A two digit representation of a year<br>example: 99 or 03',
    'fieldset_title_time'     => 'Time',
    'label_char_a'            => 'Character <b>"a"</b>',
    'value_char_a'            => 'Lowercase Ante meridiem and Post meridiem<br>example: am or pm',
    'label_char_A'            => 'Character <b>"A"</b>',
    'value_char_A'            => 'Uppercase Ante meridiem and Post meridiem<br>example: AM or PM',
    'label_char_B'            => 'Character <b>"B"</b>',
    'value_char_B'            => 'Swatch Internet time<br>example: 000 through 999',
    'label_char_g'            => 'Character <b>"g"</b>',
    'value_char_g'            => '12-hour format of an hour without leading zeros<br>example: 1 through 12',
    'label_char_G'            => 'Character <b>"G"</b>',
    'value_char_G'            => '24-hour format of an hour without leading zeros<br>example: 0 through 23',
    'label_char_h'            => ' <b>"h"</b>',
    'value_char_h'            => '12-hour format of an hour with leading zeros<br>example: 01 through 12',
    'label_char_H'            => 'Character <b>"H"</b>',
    'value_char_H'            => '24-hour format of an hour with leading zeros<br>example: 00 through 23',
    'label_char_i'            => 'Character <b>"i"</b>',
    'value_char_i'            => 'Minutes with leading zeros<br>example: 00 to 59',
    'label_char_s'            => 'Character <b>"s"</b>',
    'value_char_s'            => 'Seconds, with leading zeros<br>example: 00 through 59',
    'label_char_u'            => 'Character <b>"u"</b>',
    'value_char_u'            => 'Microseconds<br>example: 54321',
    'label_format_date'       => 'Date format',
    'label_format_time'       => 'Time format',
    'label_separator_time'    => 'Time template',
    'label_format_datetime'   => 'Date time format',
    // типы
    'data_shortcuts' => array(array(1, 'Shortcuts'), array(2, 'Components')),
    'data_php_errors' => array(
        array(0, 'no show'),
        array(30719, 'all'),
        array(7, 'errors, warnings'),
        array(15, 'errors, warnings, notices'),
        array(30711, 'all except notices')
    )
);
?>