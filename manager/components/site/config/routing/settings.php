<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Маршрутизация запросов"
 * Группа пакетов     "Настройки"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SConfRouting
 * @copyright  Copyright (c) 2013-2016 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Маршрутизация запросов
 * Описание: Маршрутизация запросов клиентов
 * Меню: Маршрутизация запросов
 * ID класса: gcontroller_sconfrouting_profile
 * Модуль: Сайт
 * Группа: Сайт / Настройки
 * Очищать: нет
 * Статистика компонента: нет 
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске компонентов: нет
 *    В главном меню: нет
 * Ресурс
 *    Компонент: site/config/routing/
 *    Контроллер: site/config/routing/Profile/
 *    Интерфейс: profile/interface/
 *    Меню:
 */

return array(
    // {S}- модуль "Site" {Routing} - пакет контроллеров "Config routinf" -> SConfRouting
    'clsPrefix' => 'SConfRouting',
    // использовать язык
    'language'  => 'ru-RU'
);
?>