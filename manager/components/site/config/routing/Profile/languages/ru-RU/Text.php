<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_update' => 'Маршрутизация запросов',
    'text_btn_help'        => 'Справка',
    // поля
    'html_desc'     => '<note>Маршрутизация AJAX запросов к компонентам сайта, где: <b>"псевдоним"</b> - это маршрут вида "http://host/request/{псевдоним}/", <b>"класс"</b> - класс компонента системы, <b>"группа"</b> - расположение компонента "/application/components/{группа}/", <b>"метод запроса"</b> - метод запроса (POST, GET) к компоненту системы.<br><br></note>',
    'html_desc1'    => '<note>Маршрутизация запросов к компонентам сайта, где: <b>"псевдоним"</b> - это маршрут вида "http://host/request/{псевдоним}/", <b>"тип сравнения"</b> - , <b>"размер сегментов"</b> - количество сегменов запроса.<br><br></note>',
    'header_alias'  => 'Псевдоним',
    'header_class'  => 'Класс компонента',
    'header_group'  => 'Группа',
    'header_method' => 'Метод запроса',
    'header_desc'   => 'Описание',
    'header_type'   => 'Тип запроса',
    'header_name'   => 'Название',
    'header_value'  => 'Сравнение',
    'header_size'   => 'Размер',
    'tooltip_size'   => 'Размер сегментов',
    'header_enabled' => 'Доступ',
    // сообщения
    'msg_file_exist' => 'Невозможно открыть файл настроек "%s"'
);
?>