<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля маршрутизации запросов"
 * Пакет контроллеров "Маршрутизация запросов"
 * Группа пакетов     "Настройки"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SConfRouting_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

define('_INC', 1);

Gear::controller('Profile/Data');

/**
 * Возращает шаблон маршрутизации запросов
 * 
 * @params string $tpl данные в шаблоне
 */
function getTplSiteConfig($tpl)
{
    return <<<TEXT
<?php
/**
 * Gear CMS
 *
 * Настройка маршрутизации запросов (создан: {$tpl['date']})
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 *
 * @category   Gear
 * @package    Config
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    \$Id: Router.php {$tpl['date']} Gear Magic \$
 */

return array(
    // маршрутизация при AJAX запросах
    'request' => array({$tpl['request_list']}),
    // маршрутизация запросов
    'route'   => array({$tpl['route_list']})
);
?>
TEXT;
}

/**
 * Данные профиля блокировки IP адресов
 * 
 * @category   Gear
 * @package    GController_SConfRouting_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SConfRouting_Profile_Data extends GController_Profile_Data
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sconfrouting_profile';

    /**
     * Файл настроек роботов
     *
     * @var string
     */
    public $filename = 'Router.php';

    /**
     * Массив полей таблицы ($_tableName)
     *
     * @var array
     */
    public $fields = array('route_list', 'request_list');

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return $this->_['profile_title_update'];
    }

    /**
     * Обновление настроеек сайта
     * 
     * @return void
     */
    protected function fileUpdate($params)
    {
        // запись для CMS
        if (file_put_contents('../' . PATH_CONFIG_CMS . $this->filename, getTplSiteConfig($params), FILE_TEXT) === false)
            throw new GException('Error', sprintf($this->_['msg_create_file'], $this->filename));
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        if (!empty($params['request_list'])) {
            $list = json_decode($params['request_list'], true);
            $tpl = '';
            $count = sizeof($list);
            for ($i = 0; $i < $count; $i++) {
                $alias = trim($list[$i]['alias']);
                $desc = trim($list[$i]['desc']);
                $enabled = $list[$i]['enabled'] ? 'true' : 'false';
                if ($desc)
                    $tpl .= "        // $desc\n";
                $tpl .= "        '$alias' => array('desc' => '$desc','enabled' => $enabled)";
                if ($i < $count - 1)
                    $tpl .= ",\n";
            }
            if ($tpl)
                $tpl = "\n$tpl\n    ";
            $params['request_list'] = $tpl;
        }

        if (!empty($params['route_list'])) {
            $list = json_decode($params['route_list'], true);
            $tpl = '';
            $count = sizeof($list);
            for ($i = 0; $i < $count; $i++) {
                $alias = trim($list[$i]['alias']);
                $size = intval($list[$i]['size']);
                $value = trim($list[$i]['value']);
                $type = trim($list[$i]['type']);
                $desc = trim($list[$i]['desc']);
                $enabled = $list[$i]['enabled'] == 'true' ? 'true' : 'false';
                if ($desc)
                    $tpl .= "        // $desc\n";
                $tpl .= "        '$alias' => array('alias' => '$alias','value' => '$value','size' => '$size','type' => '$type','desc' => '$desc','enabled' => $enabled)";
                if ($i < $count - 1)
                    $tpl .= ",\n";
            }
            if ($tpl)
                $tpl = "\n$tpl\n    ";
            $params['route_list'] = $tpl;
        }
        $params['date'] = date('Y-m-d H:i:s');
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        parent::dataAccessUpdate();

        // массив полей с их соответствующими значениями
        if (empty($params))
            $params = $this->input->getBy($this->fields);
        // проверки входных данных
        $this->isDataCorrect($params);
        // обновить файл конфигурации
        $this->fileUpdate($params);
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($params, 'method "PUT"');
        }
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        parent::dataAccessView();

        $setTo = array();

        $filename = '../' . PATH_CONFIG_CMS . $this->filename;
        // если файл настроек сайта не существует
        if (!file_exists($filename))
            throw new GException('Error', sprintf($this->_['msg_file_exist'], $filename));

        // настройки маршрутизации
        $settings = include($filename);
        $listRequest = $listRoute = array();
        if (isset($settings['request'])) {
            foreach ($settings['request'] as $alias => $item) {
                $listRequest[] = array('alias' => $alias, 'type' => 'request', 'desc' => $item['desc'], 'enabled' => $item['enabled']);
            }
        }
        if (isset($settings['route'])) {
            foreach ($settings['route'] as $alias => $item) {
                $listRoute[] = array('alias' => $alias, 'size' => $item['size'], 'value' => $item['value'], 'type' => $item['type'], 'desc' => $item['desc'], 'enabled' => $item['enabled']);
            }
        }

        $this->response->add('setTo', array(
            array('id' => 'request_list', 'value' => json_encode($listRequest)),
            array('id' => 'route_list', 'value' => json_encode($listRoute))
        ));
    }
}
?>