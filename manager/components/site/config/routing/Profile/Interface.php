<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля маршрутизации запросов"
 * Пакет контроллеров "Маршрутизация запросов"
 * Группа пакетов     "Настройки"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SConfRouting_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля маршрутизации запросов
 * 
 * @category   Gear
 * @package    GController_SConfRouting_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SConfRouting_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // Ext_Window (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'           => $this->_['title_profile_update'],
                  'titleEllipsis'   => 40,
                  'width'           => 800,
                  'autoHeight'      => true,
                  'resizable'       => false,
                  'btnDeleteHidden' => true,
                  'stateful'        => false,
                  'buttonsAdd'      => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-site-routing-config',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );
        $this->_cmp->state = 'update';

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->url = $this->componentUrl . 'profile/';
        $form->items->addItems(
            array(
                array('xtype' => 'label',
                      'html'  => $this->_['html_desc']),
                array('xtype'     => 'mn-field-grid-editor',
                      'id'        => 'request_list',
                      'name'      => 'request_list',
                      'hideLabel' => true,
                      'anchor'    => '100%',
                      'height'    => 200,
                      'checkDirty' => false,
                      'fields'    => array('alias', 'desc', 'enabled'),
                      'columns'   => array(
                          array('header'    => $this->_['header_alias'],
                                'dataIndex' => 'alias',
                                'width'     => 140,
                                'sortable'  => true),
                          array('header'    => $this->_['header_desc'],
                                'dataIndex' => 'desc',
                                'width'     => 250,
                                'sortable'  => true),
                          array(//'xtype'     => 'booleancolumn',
                                'header'    => $this->_['header_enabled'],
                                //'editor'    => array('xtype' => 'checkbox'),
                                'dataIndex' => 'enabled',
                                'width'     => 60,
                                'sortable'  => true)
                      )
                ),
                array('xtype' => 'label',
                      'html'  => $this->_['html_desc1']),
                array('xtype'     => 'mn-field-grid-editor',
                      'id'        => 'route_list',
                      'name'      => 'route_list',
                      'hideLabel' => true,
                      'anchor'    => '100%',
                      'height'    => 200,
                      'checkDirty' => false,
                      'fields'    => array('alias', 'value', 'size', 'type', 'desc', 'enabled'),
                      'columns'   => array(
                          array('header'    => $this->_['header_alias'],
                                'dataIndex' => 'alias',
                                'width'     => 110,
                                'sortable'  => true),
                          array('header'    => $this->_['header_value'],
                                'dataIndex' => 'value',
                                'width'     => 130,
                                'sortable'  => true),
                          array('header'    => $this->_['header_type'],
                                'dataIndex' => 'type',
                                'width'     => 100,
                                'sortable'  => true),
                          array('header'    => $this->_['header_size'],
                                'tooltip'   => $this->_['tooltip_size'],
                                'dataIndex' => 'size',
                                'width'     => 70,
                                'sortable'  => true),
                          array('header'    => $this->_['header_desc'],
                                'dataIndex' => 'desc',
                                'width'     => 220,
                                'sortable'  => true),
                          array(//'xtype'     => 'booleancolumn',
                                'header'    => $this->_['header_enabled'],
                                //'editor'    => array('xtype' => 'checkbox'),
                                'dataIndex' => 'enabled',
                                'width'     => 60,
                                'sortable'  => true)
                      )
                )
            )
        );

        parent::getInterface();
    }
}
?>