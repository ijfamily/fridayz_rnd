<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Блокировка IP адресов"
 * Группа пакетов     "Настройки"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SLockingIp
 * @copyright  Copyright (c) 2013-2016 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Блокировка IP адресов
 * Описание: Блокировка IP адресов
 * Меню: Блокировка IP адресов
 * ID класса: gcontroller_slockingip_profile
 * Модуль: Сайт
 * Группа: Сайт / Настройки
 * Очищать: нет
 * Статистика компонента: нет 
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске компонентов: нет
 *    В главном меню: нет
 * Ресурс
 *    Компонент: site/config/locking/
 *    Контроллер: site/config/locking/Profile/
 *    Интерфейс: profile/interface/
 *    Меню:
 */

return array(
    // {S}- модуль "Site" {Locking} - пакет контроллеров "Site locking IP" -> SLockingIp
    'clsPrefix' => 'SLockingIp',
    // использовать язык
    'language'  => 'ru-RU'
);
?>