<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_update' => 'Блокировка IP адресов',
    'text_btn_help'        => 'Справка',
    // поля
    'header_ip'      => 'IP адрес',
    'label_active'   => 'задействовать блокировку',
    'label_template' => 'вывод шаблона с ошибкой заблокированному IP адресу (ошибка "503 Служба недоступна")',
    // сообщения
    'msg_file_exist' => 'Невозможно открыть файл настроек "%s"'
);
?>