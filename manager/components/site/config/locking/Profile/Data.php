<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля блокировки IP адресов"
 * Пакет контроллеров "Блокировка IP адресов"
 * Группа пакетов     "Настройки"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SLockingIp_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

define('_INC', 1);

Gear::controller('Profile/Data');

/**
 * Возращает шаблон блокировки IP адресов
 * 
 * @params string $tpl данные в шаблоне
 */
function getTplSiteConfig($tpl)
{
    return <<<TEXT
<?php
/**
 * Gear CMS
 *
 * Блокировка IP адресов (создан: {$tpl['date']})
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 *
 * @category   Gear
 * @package    Config
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    \$Id: Locking.php {$tpl['date']} Gear Magic \$
 */

return array(
    // блокировать IP адрес
    'ACTIVE'   => {$tpl['lock_active']},
    // вывод шаблона с ошибкой заблокированному IP адресу
    'TEMPLATE' => {$tpl['lock_template']},
    // список IP адресов
    'IP'       => array({$tpl['ip']})
);
?>
TEXT;
}

/**
 * Данные профиля блокировки IP адресов
 * 
 * @category   Gear
 * @package    GController_SLockingIp_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SLockingIp_Profile_Data extends GController_Profile_Data
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_slockingip_profile';

    /**
     * Файл настроек роботов
     *
     * @var string
     */
    public $filename = 'Locking.php';

    /**
     * Массив полей таблицы ($_tableName)
     *
     * @var array
     */
    public $fields = array('lock_ip', 'lock_template', 'lock_active');

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return $this->_['profile_title_update'];
    }

    /**
     * Обновление настроеек сайта
     * 
     * @return void
     */
    protected function fileUpdate($params)
    {
        // запись для CMS
        if (file_put_contents('../' . PATH_CONFIG_CMS . $this->filename, getTplSiteConfig($params), FILE_TEXT) === false)
            throw new GException('Error', sprintf($this->_['msg_create_file'], $this->filename));
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        if (empty($params['lock_ip']))
            $tpl = '';
        else {
            $ip = json_decode($params['lock_ip'], true);
            $tpl = '';
            for ($i = 0; $i < sizeof($ip); $i++) {
                $tpl .= "        '" . $ip[$i]['ip'] . "' => true";
                if ($i < sizeof($ip) - 1)
                    $tpl .= ",\n";
            }
            if ($tpl)
                $tpl = "\n$tpl\n    ";
        }
        $params['ip'] = $tpl;
        $params['date'] = date('Y-m-d H:i:s');
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        parent::dataAccessUpdate();

        // массив полей с их соответствующими значениями
        if (empty($params))
            $params = $this->input->getBy($this->fields);
        // проверки входных данных
        $this->isDataCorrect($params);
        // проверка существования записи с одинаковыми значениями
        $this->isDataExist($params);
        // использование системных полей в запросе SQL
        if ($this->isSysFields) {
            $params['sys_date_update'] = date('Y-m-d');
            $params['sys_time_update'] = date('H:i:s');
            $params['sys_user_update'] = $this->session->get('user_id');
        }
        // обновить файл конфигурации
        $this->fileUpdate($params);
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($params, 'method "PUT"');
        }
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        parent::dataAccessView();

        $setTo = array();

        $filename = '../' . PATH_CONFIG_CMS . $this->filename;
        // если файл настроек сайта не существует
        if (!file_exists($filename))
            throw new GException('Error', sprintf($this->_['msg_file_exist'], $filename));

        // настройки доменов
        $settings = include($filename);
        $list = array();
        if (isset($settings['IP'])) {
            foreach ($settings['IP'] as $ip => $res) {
                $list[] = array('ip' => $ip);
            }
        }

        $this->response->add('setTo', array(
            array('id' => 'lock_ip', 'value' => json_encode($list)),
            array('id' => 'lock_template', 'value' => $settings['TEMPLATE']),
            array('id' => 'lock_active', 'value' => $settings['ACTIVE'])
        ));
    }
}
?>