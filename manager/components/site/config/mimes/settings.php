<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Настройка списка MIMES"
 * Группа пакетов     "Настройки"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SMimes
 * @copyright  Copyright (c) 2013-2016 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Настройка списка MIMES
 * Описание: Настройка списка MIMES
 * Меню: Настройка списка MIMES
 * ID класса: gcontroller_smimes_profile
 * Модуль: Сайт
 * Группа: Сайт / Настройки
 * Очищать: нет
 * Статистика компонента: нет 
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске компонентов: нет
 *    В главном меню: нет
 * Ресурс
 *    Компонент: site/config/mimes/
 *    Контроллер: site/config/mimes/Profile/
 *    Интерфейс: profile/interface/
 *    Меню:
 */

return array(
    // {S}- модуль "Site" {Mimes} - пакет контроллеров "Site mimes" -> SMimes
    'clsPrefix' => 'SMimes',
    // использовать язык
    'language'  => 'ru-RU'
);
?>