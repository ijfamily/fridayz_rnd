<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля настроеек сайта"
 * Пакет контроллеров "Настройки сайта"
 * Группа пакетов     "Настройки"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SConfig_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля настроеек сайта
 * 
 * @category   Gear
 * @package    GController_SConfig_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SConfig_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Возращает список шаблонов сайта
     * 
     * @return array
     */
    protected function getThemes()
    {
        $path = '../application/templates/';
        $items = array();
        if ($handle = opendir($path)) {
            while (false !== ($file = readdir($handle))) {
                // если файл
                if ($file != "." && $file != ".." && $file != 'system' && $file != 'common' && is_dir($path . $file)) {
                    $items[] = array($file, $file);
                }
            }
            closedir($handle);
        }

        return $items;
    }

    /**
     * Возращает интерфейс кэша RSS ленты
     * 
     * @param object $query указатель на запрос
     * @return mixed
     */
    protected function getCacheRssInterface($query = null)
    {
        if (is_null($query))
            $query = new GDb_Query();
        // данные кэша страницы
        $sql = "SELECT * FROM `site_cache` WHERE `cache_generator`='rss'";
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $items = array();
        while (!$query->eof()) {
            $cache = $query->next();
            $items[] = array(
                'xtype'      => 'displayfield',
                'fieldLabel' => $this->_['label_cache_date'],
                'anchor'     => '100%',
                'fieldClass' => 'mn-field-value-info',
                'value'      => date('d-m-Y H:i:s', strtotime($cache['cache_date']))
            );
            $items[] = array(
                'xtype'      => 'displayfield',
                'fieldLabel' => $this->_['label_cache_file'],
                'anchor'     => '100%',
                'value'      => '<a target="blank" href="/cache/' . $cache['cache_filename'] . '">' . $cache['cache_filename'] . '</a>'
            );
            $items[] = array(
                'xtype'      => 'displayfield',
                'fieldLabel' => $this->_['label_cache_address'],
                'anchor'     => '100%',
                'value'      => '<a target="blank" href="' . $cache['cache_uri'] . '">' . $cache['cache_uri'] . '</a>'
            );
            $items[] = array('xtype' => 'mn-field-separator');
        }
        $fs = array();
        if ($items) {
            $items[] = array(
                'xtype'       => 'mn-btn-resetcache',
                'msgConfirm'  => $this->_['msg_cache_delete'],
                'text'        => $this->_['text_cache_delete'],
                'tooltip'     => $this->_['tip_cache_delete'],
                'icon'        => $this->resourcePath . 'icon-cache-delete.png',
                'ctId'        => 'fs-cache',
                'width'       => 107,
                'url'         => $this->componentUrl . 'profile/cache/' . $this->uri->id . '?for=rss'
            );
            $fs = array(
                'xtype'       => 'fieldset',
                'id'          => 'fs-cache',
                'width'       => 607,
                'title'       => sprintf($this->_['title_fieldset_cache'], date('d-m-Y H:i:s', strtotime($cache['cache_date']))),
                'collapsible' => true,
                'collapsed'   => true,
                'labelWidth'  => 125,
                'items'       => $items
            );
        }

        return $fs;
    }

    /**
     * Возращает интерфейс кэша карты сайта
     * 
     * @param object $query указатель на запрос
     * @return mixed
     */
    protected function getCacheSmInterface($query = null)
    {
        if (is_null($query))
            $query = new GDb_Query();
        // данные кэша страницы
        $sql = "SELECT * FROM `site_cache` WHERE `cache_generator`='sitemap'";
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $items = array();
        while (!$query->eof()) {
            $cache = $query->next();
            $items[] = array(
                'xtype'      => 'displayfield',
                'fieldLabel' => $this->_['label_cache_date'],
                'anchor'     => '100%',
                'fieldClass' => 'mn-field-value-info',
                'value'      => date('d-m-Y H:i:s', strtotime($cache['cache_date']))
            );
            $items[] = array(
                'xtype'      => 'displayfield',
                'fieldLabel' => $this->_['label_cache_file'],
                'anchor'     => '100%',
                'value'      => '<a target="blank" href="/cache/' . $cache['cache_filename'] . '">' . $cache['cache_filename'] . '</a>'
            );
            $items[] = array(
                'xtype'      => 'displayfield',
                'fieldLabel' => $this->_['label_cache_address'],
                'anchor'     => '100%',
                'value'      => '<a target="blank" href="' . $cache['cache_uri'] . '">' . $cache['cache_uri'] . '</a>'
            );
            $items[] = array('xtype' => 'mn-field-separator');
        }
        $fs = array();
        if ($items) {
            $items[] = array(
                'xtype'       => 'mn-btn-resetcache',
                'msgConfirm'  => $this->_['msg_cache_delete'],
                'text'        => $this->_['text_cache_delete'],
                'tooltip'     => $this->_['tip_cache_delete'],
                'icon'        => $this->resourcePath . 'icon-cache-delete.png',
                'ctId'        => 'fs-cache',
                'width'       => 107,
                'url'         => $this->componentUrl . 'profile/cache/' . $this->uri->id. '?for=sitemap'
            );
            $fs = array(
                'xtype'       => 'fieldset',
                'id'          => 'fs-cache',
                'width'       => 607,
                'title'       => sprintf($this->_['title_fieldset_cache'], date('d-m-Y H:i:s', strtotime($cache['cache_date']))),
                'collapsible' => true,
                'collapsed'   => true,
                'labelWidth'  => 125,
                'items'       => $items
            );
        }

        return $fs;
    }

    /**
     * Возращает список языков
     * 
     * @param object $query указатель на запрос
     * @return mixed
     */
    protected function getLanguages($query = null)
    {
        if (is_null($query))
            $query = new GDb_Query();

        // список языков
        $sql = "SELECT * FROM `site_languages` WHERE `language_visible`=1";
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $items = array();
        while (!$query->eof()) {
            $rec = $query->next();
            $items[] = array($rec['language_full'], $rec['language_name']);
        }

        return $items;
    }

    /**
     * Возращает интерфейс списка шаблонов
     * 
     * @param object $query указатель на запрос
     * @return mixed
     */
    protected function getTemplates($query = null)
    {
        if (is_null($query))
            $query = new GDb_Query();
        $sql = 'SELECT * FROM `site_templates` ORDER BY `template_name` DESC';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $items = array();
        while (!$query->eof()) {
            $rec = $query->next();
            $items[] = array($rec['template_name'], $rec['template_filename']);
            //$items[] = array('id'=> 'name' => $rec['template_name'], 'data' => $rec['template_icon']);
        }

        return $items;
    }

    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        parent::getDataInterface();

        return array(
            'rss-cache' => $this->getCacheRssInterface(),
            'sm-cache'  => $this->getCacheSmInterface(),
            'languages' => $this->getLanguages(),
            'themes'    => $this->getThemes(),
            'templates' => $this->getTemplates()
        );
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // Ext_Window (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'           => $this->_['title_profile_update'],
                  'titleEllipsis'   => 40,
                  'width'           => 800,
                  'height'          => 600,
                  'resizable'       => false,
                  'btnDeleteHidden' => true,
                  'stateful'        => false,
                  'buttonsAdd'      => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-site-config',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );
        $this->_cmp->state = 'update';

        // вкладка "Оптимизация"
        $tabOptim = array(
            'title'       => $this->_['title_tab_optim'],
            'layout'      => 'form',
            'labelWidth'  => 400,
            'baseCls'     => 'mn-form-tab-body',
            'iconSrc'     => $this->resourcePath . 'icon-tab-optim.png',
            'autoScroll'  => true,
            'defaults'    => array('labelSeparator' => ''),
            'items'       => array(
                array('xtype'      => 'checkbox',
                      'fieldLabel' => $this->_['label_caching'],
                      'checkDirty' => false,
                      'name'       => 'settings[CACHE]'),
                array('xtype'         => 'combo',
                      'fieldLabel'    => $this->_['label_cache_type'],
                      'name'          => 'settings[CACHE/TYPE]',
                      'checkDirty'    => false,
                      'editable'      => false,
                      'width'         => 180,
                      'typeAhead'     => false,
                      'triggerAction' => 'all',
                      'mode'          => 'local',
                      'store'         => array(
                          'xtype'  => 'arraystore',
                          'fields' => array('display', 'value'),
                          'data'   => $this->_['data_cache']
                      ),
                      'hiddenName'    => 'settings[CACHE/TYPE]',
                      'valueField'    => 'value',
                      'displayField'  => 'display',
                      'allowBlank'    => true),
                array('xtype'      => 'textfield',
                      'fieldLabel' => $this->_['label_cache_mem'],
                      'width'      => 180,
                      'name'       => 'settings[CACHE/MEMCACHE/SERVER]',
                      'checkDirty' => false),
                array('xtype'      => 'textfield',
                      'fieldLabel' => $this->_['label_cache_dir'],
                      'width'      => 180,
                      'name'       => 'settings[CACHE/DIR]',
                      'checkDirty' => false),
                array('xtype' => 'mn-field-separator'),
                array('xtype'      => 'checkbox',
                      'fieldLabel' => $this->_['label_og'],
                      'checkDirty' => false,
                      'name'       => 'settings[OPENGRAPH]'),
                array('xtype'      => 'textfield',
                      'fieldLabel' => $this->_['label_og_image'],
                      'anchor'     => '100%',
                      'name'       => 'settings[OPENGRAPH/IMAGE]',
                      'checkDirty' => false),
                array('xtype' => 'mn-field-separator'),
                array('xtype'      => 'checkbox',
                      'fieldLabel' => $this->_['label_gzip'],
                      'checkDirty' => false,
                      'name'       => 'settings[GZIP]'),
                array('xtype'      => 'checkbox',
                      'fieldLabel' => $this->_['label_global_tags'],
                      'checkDirty' => false,
                      'name'       => 'settings[GLOBAL/TAGS]'),
                array('xtype'      => 'checkbox',
                      'fieldLabel' => $this->_['label_robots_use'],
                      'checkDirty' => false,
                      'name'       => 'settings[ROBOTS]'),
                array('xtype'      => 'checkbox',
                      'fieldLabel' => $this->_['label_consider_visits'],
                      'checkDirty' => false,
                      'name'       => 'settings[CONSIDER/VISITS]'),
                array('xtype'      => 'checkbox',
                      'fieldLabel' => $this->_['label_banners'],
                      'checkDirty' => false,
                      'name'       => 'settings[BANNERS]'),
                array('xtype'      => 'checkbox',
                      'fieldLabel' => $this->_['label_tags'],
                      'checkDirty' => false,
                      'name'       => 'settings[ARTICLE/TAGS]'),
                array('xtype'      => 'checkbox',
                      'fieldLabel' => $this->_['label_canonical'],
                      'checkDirty' => false,
                      'name'       => 'settings[CANONICAL]'),
                array('xtype' => 'mn-field-separator'),
                array('xtype'      => 'checkbox',
                      'fieldLabel' => $this->_['label_calendar'],
                      'checkDirty' => false,
                      'name'       => 'settings[CALENDAR]'),
                array('xtype'      => 'checkbox',
                      'fieldLabel' => $this->_['label_calendar_highlight'],
                      'checkDirty' => false,
                      'name'       => 'settings[CALENDAR/HIGHLIGHT]'),
                array('xtype' => 'mn-field-separator'),
                array('xtype'      => 'checkbox',
                      'fieldLabel' => $this->_['label_breadcrumbs'],
                      'checkDirty' => false,
                      'name'       => 'settings[BREADCRUMBS]'),
                array('xtype'      => 'checkbox',
                      'fieldLabel' => $this->_['label_breadcrumbs_zero'],
                      'checkDirty' => false,
                      'name'       => 'settings[BREADCRUMBS/ZERO]'),
                array('xtype' => 'mn-field-separator'),
                array('xtype'      => 'checkbox',
                      'fieldLabel' => $this->_['label_debug'],
                      'checkDirty' => false,
                      'name'       => 'settings[DEBUG]'),
            )
        );

        // вкладка "Изображения"
        $tabImages = array(
            'title'       => $this->_['title_tab_img'],
            'layout'      => 'form',
            'labelWidth'  => 110,
            'baseCls'     => 'mn-form-tab-body',
            'iconSrc'     => $this->resourcePath . 'icon-tab-img.png',
            'autoScroll'  => true,
            'items'       => array(
                array('xtype'      => 'fieldset',
                      'title'      => $this->_['title_fieldset_original'],
                      'labelWidth' => 100,
                      'items'      => array(
                          array('xtype'      => 'numberfield',
                                'fieldLabel' => $this->_['label_image_width'],
                                'width'      => 100,
                                'name'       => 'settings[IMAGE/ORIGINAL/WIDTH]',
                                'checkDirty' => false),
                          array('xtype'      => 'numberfield',
                                'fieldLabel' => $this->_['label_image_height'],
                                'width'      => 100,
                                'name'       => 'settings[IMAGE/ORIGINAL/HEIGHT]',
                                'checkDirty' => false),
                          array('xtype' => 'label', 'html' => $this->_['label_image_info']),
                      )
                ),
                array('xtype'      => 'fieldset',
                      'title'      => $this->_['title_fieldset_medium'],
                      'labelWidth' => 100,
                      'items'      => array(
                          array('xtype'      => 'numberfield',
                                'fieldLabel' => $this->_['label_image_width'],
                                'width'      => 100,
                                'name'       => 'settings[IMAGE/MEDIUM/WIDTH]',
                                'checkDirty' => false),
                          array('xtype'      => 'numberfield',
                                'fieldLabel' => $this->_['label_image_height'],
                                'width'      => 100,
                                'name'       => 'settings[IMAGE/MEDIUM/HEIGHT]',
                                'minValue'   => 0,
                                'checkDirty' => false),
                          array('xtype' => 'label', 'html' => $this->_['label_image_info1'])
                      )
                ),
                array('xtype'      => 'fieldset',
                      'title'      => $this->_['title_fieldset_thumb'],
                      'labelWidth' => 100,
                      'items'      => array(
                          array('xtype'      => 'numberfield',
                                'fieldLabel' => $this->_['label_image_width'],
                                'width'      => 100,
                                'name'       => 'settings[IMAGE/THUMB/WIDTH]',
                                'checkDirty' => false),
                          array('xtype'      => 'numberfield',
                                'fieldLabel' => $this->_['label_image_height'],
                                'width'      => 100,
                                'name'       => 'settings[IMAGE/THUMB/HEIGHT]',
                                'checkDirty' => false),
                          array('xtype' => 'label', 'html' => $this->_['label_image_info2'])
                      )
                ),
                array('xtype'      => 'numberfield',
                      'fieldLabel' => $this->_['label_image_quality'],
                      'width'      => 100,
                      'value'      => 100,
                      'name'       => 'settings[IMAGE/QUALITY]',
                      'checkDirty' => false),
                array('xtype'      => 'fieldset',
                      'title'      => $this->_['title_fieldset_watermak'],
                      'labelWidth' => 100,
                      'items'      => array(
                          array('xtype'           => 'mn-field-browse',
                                'params'          => 'view=images',
                                'triggerWdgSetTo' => true,
                                'triggerWdgUrl'   => 'site/media/' . ROUTER_DELIMITER .'dialog/interface/',
                                'fieldLabel' => $this->_['label_stamp'],
                                'id'         => 'watermarkStamp',
                                'name'       => 'settings[WATERMARK/STAMP]',
                                'checkDirty' => false,
                                'anchor'     => '100%',
                                'allowBlank' => true),
                          array('xtype'         => 'combo',
                                'fieldLabel'    => $this->_['label_position'],
                                'name'          => 'settings[WATERMARK/POSITION]',
                                'checkDirty'    => false,
                                'editable'      => false,
                                'width'         => 180,
                                'typeAhead'     => false,
                                'triggerAction' => 'all',
                                'mode'          => 'local',
                                'store'         => array(
                                    'xtype'  => 'arraystore',
                                    'fields' => array('display', 'value'),
                                    'data'   => $this->_['data_position']
                                ),
                                'hiddenName'    => 'settings[WATERMARK/POSITION]',
                                'valueField'    => 'value',
                                'displayField'  => 'display',
                                'allowBlank'    => true),
                      )
                )
            )
        );

        // вкладка "Каталоги и файлы"
        $tabDir = array(
            'title'       => $this->_['title_tab_dir'],
            'layout'      => 'form',
            'labelWidth'  => 400,
            'baseCls'     => 'mn-form-tab-body',
            'iconSrc'     => $this->resourcePath . 'icon-tab-dir.png',
            'autoScroll'  => true,
            'items'       => array(
                array('xtype'      => 'fieldset',
                      'title'      => $this->_['title_fieldset_dir'],
                      'labelWidth' => 140,
                      'items'      => array(
                          array('xtype'      => 'textfield',
                                'fieldLabel' => $this->_['label_dir_themes'],
                                'anchor'     => '100%',
                                'name'       => 'settings[DIR/THEMES]',
                                'checkDirty' => false),
                          array('xtype'      => 'textfield',
                                'fieldLabel' => $this->_['label_dir_data'],
                                'anchor'     => '100%',
                                'name'       => 'settings[DIR/DATA]',
                                'checkDirty' => false),
                          array('xtype'      => 'textfield',
                                'fieldLabel' => $this->_['label_dir_categories'],
                                'anchor'     => '100%',
                                'name'       => 'settings[DIR/CATEGORIES]',
                                'checkDirty' => false),
                          array('xtype'      => 'textfield',
                                'fieldLabel' => $this->_['label_dir_audio'],
                                'anchor'     => '100%',
                                'name'       => 'settings[DIR/AUDIO]',
                                'checkDirty' => false),
                          array('xtype'      => 'textfield',
                                'fieldLabel' => $this->_['label_dir_video'],
                                'anchor'     => '100%',
                                'name'       => 'settings[DIR/VIDEO]',
                                'checkDirty' => false),
                          array('xtype'      => 'textfield',
                                'fieldLabel' => $this->_['label_dir_docs'],
                                'anchor'     => '100%',
                                'name'       => 'settings[DIR/DOCS]',
                                'checkDirty' => false),
                          array('xtype'      => 'textfield',
                                'fieldLabel' => $this->_['label_dir_banners'],
                                'anchor'     => '100%',
                                'name'       => 'settings[DIR/BANNERS]',
                                'checkDirty' => false),
                          array('xtype'      => 'textfield',
                                'fieldLabel' => $this->_['label_dir_sliders'],
                                'anchor'     => '100%',
                                'name'       => 'settings[DIR/SLIDERS]',
                                'checkDirty' => false),
                          array('xtype'      => 'textfield',
                                'fieldLabel' => $this->_['label_dir_images'],
                                'anchor'     => '100%',
                                'name'       => 'settings[DIR/IMAGES]',
                                'checkDirty' => false),
                          array('xtype'      => 'textfield',
                                'fieldLabel' => $this->_['label_dir_temp'],
                                'anchor'     => '100%',
                                'name'       => 'settings[DIR/TEMP]',
                                'checkDirty' => false)
                      )
                ),
                array('xtype'      => 'fieldset',
                      'title'      => $this->_['title_fieldset_ext'],
                      'labelWidth' => 100,
                      'items'      => array(
                          array('xtype'      => 'textfield',
                                'fieldLabel' => $this->_['label_ext_images'],
                                'anchor'     => '100%',
                                'name'       => 'settings[FILES/EXT/IMAGES]',
                                'checkDirty' => false),
                          array('xtype'      => 'textfield',
                                'fieldLabel' => $this->_['label_ext_docs'],
                                'anchor'     => '100%',
                                'name'       => 'settings[FILES/EXT/DOCS]',
                                'checkDirty' => false),
                          array('xtype'      => 'textfield',
                                'fieldLabel' => $this->_['label_ext_audio'],
                                'anchor'     => '100%',
                                'name'       => 'settings[FILES/EXT/AUDIO]',
                                'checkDirty' => false),
                          array('xtype'      => 'textfield',
                                'fieldLabel' => $this->_['label_ext_video'],
                                'anchor'     => '100%',
                                'name'       => 'settings[FILES/EXT/VIDEO]',
                                'checkDirty' => false)
                      )
                )
            )
        );

        // вкладка "Почта"
        $tabMail = array(
            'title'       => $this->_['title_tab_mail'],
            'layout'      => 'form',
            'labelWidth'  => 400,
            'baseCls'     => 'mn-form-tab-body',
            'iconSrc'     => $this->resourcePath . 'icon-tab-mail.png',
            'autoScroll'  => true,
            'defaults'    => array('labelSeparator' => ''),
            'items'       => array(
                array('xtype'      => 'textfield',
                      'fieldLabel' => $this->_['label_mail_admin'],
                      'name'       => 'settings[MAIL/ADMIN]',
                      'checkDirty' => false,
                      'width'      => 326,
                      'allowBlank' => true),
                array('xtype'      => 'textfield',
                      'fieldLabel' => $this->_['label_mail_notification'],
                      'name'       => 'settings[MAIL/NOTIFICATION]',
                      'checkDirty' => false,
                      'width'      => 326,
                      'allowBlank' => true),
                array('xtype'      => 'textfield',
                      'fieldLabel' => $this->_['label_mail_header'],
                      'name'       => 'settings[MAIL/HEADER]',
                      'checkDirty' => false,
                      'width'      => 326,
                      'allowBlank' => true),
                array('xtype'         => 'combo',
                      'fieldLabel'    => $this->_['label_mail_type'],
                      'name'          => 'settings[MAIL/TYPE]',
                      'checkDirty'    => false,
                      'editable'      => false,
                      'width'         => 160,
                      'typeAhead'     => false,
                      'triggerAction' => 'all',
                      'mode'          => 'local',
                      'store'         => array(
                          'xtype'  => 'arraystore',
                          'fields' => array('value'),
                          'data'   => array(array('PHP Mail'), array('SMTP'))
                      ),
                      'hiddenName'    => 'settings[MAIL/TYPE]',
                      'valueField'    => 'value',
                      'displayField'  => 'value',
                      'allowBlank'    => true),
                array('xtype'      => 'fieldset',
                      'width'      => 732,
                      'labelWidth' => 390,
                      'collapsible' => true,
                      'collapsed'   => false,
                      'title'      => $this->_['title_fieldset_smpt'],
                      'labelWidth' => 388,
                      'autoHeight' => true,
                      'items'      => array(
                            array('xtype'      => 'textfield',
                                  'fieldLabel' => $this->_['label_mail_smpt_host'],
                                  'name'       => 'settings[MAIL/SMTP/HOST]',
                                  'checkDirty' => false,
                                  'width'      => 160,
                                  'allowBlank' => true),
                            array('xtype'      => 'numberfield',
                                  'fieldLabel' => $this->_['label_mail_smpt_port'],
                                  'name'       => 'settings[MAIL/SMTP/PORT]',
                                  'checkDirty' => false,
                                  'width'      => 70,
                                  'allowBlank' => true,
                                  'emptyText'  => 0),
                            array('xtype'      => 'textfield',
                                  'fieldLabel' => $this->_['label_mail_smpt_login'],
                                  'name'       => 'settings[MAIL/SMTP/LOGIN]',
                                  'checkDirty' => false,
                                  'width'      => 160,
                                  'allowBlank' => true),
                            array('xtype'      => 'textfield',
                                  'fieldLabel' => $this->_['label_mail_smpt_password'],
                                  'name'       => 'settings[MAIL/SMTP/PASSWORD]',
                                  'checkDirty' => false,
                                  'width'      => 160,
                                  'allowBlank' => true),
                            array('xtype'         => 'combo',
                                  'fieldLabel'    => $this->_['label_mail_smpt_secure'],
                                  'name'          => 'settings[MAIL/SMTP/SECURE]',
                                  'checkDirty'    => false,
                                  'editable'      => false,
                                  'width'         => 160,
                                  'typeAhead'     => false,
                                  'triggerAction' => 'all',
                                  'mode'          => 'local',
                                  'store'         => array(
                                      'xtype'  => 'arraystore',
                                      'fields' => array('list', 'value'),
                                      'data'   => array(array($this->_['data_no'], 0), array('SSL', 'SSL'), array('TLS', 'TLS'))
                                  ),
                                  'hiddenName'    => 'settings[MAIL/SMTP/SECURE]',
                                  'valueField'    => 'value',
                                  'displayField'  => 'list',
                                  'allowBlank'    => true),
                            array('xtype'      => 'textfield',
                                  'fieldLabel' => $this->_['label_mail_smpt_mail'],
                                  'name'       => 'settings[MAIL/SMTP/MAIL]',
                                  'checkDirty' => false,
                                  'width'      => 160,
                                  'allowBlank' => true)
                      )
                )
            )
        );

        // вкладка "Безопасность"
        $tabSafeness = array(
            'title'       => $this->_['title_tab_safeness'],
            'layout'      => 'form',
            'labelWidth'  => 400,
            'baseCls'     => 'mn-form-tab-body',
            'iconSrc'     => $this->resourcePath . 'icon-tab-safeness.png',
            'autoScroll'  => true,
            'defaults'    => array('labelSeparator' => ''),
            'items'       => array(
                array('xtype' => 'container', 'id' => 'fldIpAddress', 'width' => 700),
                array('xtype' => 'mn-field-separator'),
                array('xtype'      => 'textfield',
                      'fieldLabel' => $this->_['label_cms_token'],
                      'name'       => 'settings[CMS/TOKEN]',
                      'checkDirty' => false,
                      'width'      => 326,
                      'allowBlank' => false),
                array('xtype'      => 'textfield',
                      'fieldLabel' => $this->_['label_ipaddress'],
                      'name'       => 'settings[IP_ADDRESS]',
                      'checkDirty' => false,
                      'width'      => 180,
                      'allowBlank' => true),
                array('xtype' => 'mn-field-separator'),
                array('xtype'      => 'checkbox',
                      'fieldLabel' => $this->_['label_ajax'],
                      'checkDirty' => false,
                      'name'       => 'settings[AJAX]'),
                array('xtype'      => 'checkbox',
                      'fieldLabel' => $this->_['label_captcha'],
                      'checkDirty' => false,
                      'name'       => 'settings[CAPTCHA]'),
                array('xtype'         => 'combo',
                      'fieldLabel'    => $this->_['label_code_type'],
                      'name'          => 'settings[CAPTCHA/TYPE]',
                      'checkDirty'    => false,
                      'editable'      => false,
                      'width'         => 180,
                      'typeAhead'     => false,
                      'triggerAction' => 'all',
                      'mode'          => 'local',
                      'store'         => array(
                          'xtype'  => 'arraystore',
                          'fields' => array('list', 'value'),
                          'data'   => $this->_['data_code_types']
                      ),
                      'hiddenName'    => 'settings[CAPTCHA/TYPE]',
                      'valueField'    => 'value',
                      'displayField'  => 'list',
                      'allowBlank'    => true),
                array('xtype'      => 'fieldset',
                      'width'      => 732,
                      'labelWidth' => 390,
                      'collapsible' => true,
                      'collapsed'   => true,
                      'title'      => $this->_['title_fieldset_recaptcha'],
                      'autoHeight' => true,
                      'items'      => array(
                          array('xtype'      => 'textfield',
                                'fieldLabel' => $this->_['label_recaptcha_pbkey'],
                                'name'       => 'settings[reCAPTCHA/PUBLIC_KEY]',
                                'checkDirty' => false,
                                'anchor'     => '100%',
                                'allowBlank' => true),
                          array('xtype'      => 'textfield',
                                'fieldLabel' => $this->_['label_recaptcha_prkey'],
                                'name'       => 'settings[reCAPTCHA/PRIVATE_KEY]',
                                'checkDirty' => false,
                                'anchor'     => '100%',
                                'allowBlank' => true),
                          array('xtype'         => 'combo',
                                'fieldLabel'    => $this->_['label_recaptcha_theme'],
                                'name'          => 'settings[reCAPTCHA/THEME]',
                                'checkDirty'    => false,
                                'editable'      => false,
                                'width'         => 180,
                                'typeAhead'     => false,
                                'triggerAction' => 'all',
                                'mode'          => 'local',
                                'store'         => array(
                                    'xtype'  => 'arraystore',
                                    'fields' => array('list', 'value'),
                                    'data'   => array(array('Light', 'light'), array('Dark', 'dark'))
                                ),
                                'hiddenName'    => 'settings[reCAPTCHA/THEME]',
                                'valueField'    => 'value',
                                'displayField'  => 'list',
                                'allowBlank'    => true)
                      )
                ),
                array('xtype'      => 'fieldset',
                      'width'      => 732,
                      'labelWidth' => 390,
                      'collapsible' => true,
                      'collapsed'   => true,
                      'title'      => $this->_['title_fieldset_kcaptcha'],
                      'autoHeight' => true,
                      'items'      => array(
                          array('xtype'      => 'numberfield',
                                'fieldLabel' => $this->_['label_kcaptcha_width'],
                                'name'       => 'settings[kCAPTCHA/width]',
                                'checkDirty' => false,
                                'width'      => 70,
                                'allowBlank' => false,
                                'emptyText'  => 0),
                          array('xtype'      => 'numberfield',
                                'fieldLabel' => $this->_['label_kcaptcha_height'],
                                'name'       => 'settings[kCAPTCHA/height]',
                                'checkDirty' => false,
                                'width'      => 70,
                                'allowBlank' => false,
                                'emptyText'  => 0),
                          array('xtype'      => 'textfield',
                                'fieldLabel' => $this->_['label_kcaptcha_length'],
                                'name'       => 'settings[kCAPTCHA/_length]',
                                'checkDirty' => false,
                                'width'      => 70,
                                'allowBlank' => false),
                          array('xtype'      => 'textfield',
                                'fieldLabel' => $this->_['label_kcaptcha_alphabet'],
                                'name'       => 'settings[kCAPTCHA/alphabet]',
                                'checkDirty' => false,
                                'anchor'     => '100%',
                                'allowBlank' => false),
                          array('xtype'      => 'textfield',
                                'fieldLabel' => $this->_['label_kcaptcha_allowedSymbols'],
                                'name'       => 'settings[kCAPTCHA/allowedSymbols]',
                                'checkDirty' => false,
                                'anchor'     => '100%',
                                'allowBlank' => false),
                          array('xtype'      => 'textfield',
                                'fieldLabel' => $this->_['label_kcaptcha_fluctuationAmplitude'],
                                'name'       => 'settings[kCAPTCHA/fluctuationAmplitude]',
                                'checkDirty' => false,
                                'width'      => 70,
                                'allowBlank' => false),
                          array('xtype'      => 'textfield',
                                'fieldLabel' => $this->_['label_kcaptcha_whiteNoiseDensity'],
                                'name'       => 'settings[kCAPTCHA/whiteNoiseDensity]',
                                'checkDirty' => false,
                                'width'      => 70,
                                'allowBlank' => false),
                          array('xtype'      => 'textfield',
                                'fieldLabel' => $this->_['label_kcaptcha_blackNoiseDensity'],
                                'name'       => 'settings[kCAPTCHA/blackNoiseDensity]',
                                'checkDirty' => false,
                                'width'      => 70,
                                'allowBlank' => false),
                          array('xtype'      => 'textfield',
                                'fieldLabel' => $this->_['label_kcaptcha_type'],
                                'name'       => 'settings[kCAPTCHA/type]',
                                'checkDirty' => false,
                                'width'      => 70,
                                'allowBlank' => false),
                          array('xtype'      => 'textfield',
                                'fieldLabel' => $this->_['label_kcaptcha_backgroundColor'],
                                'name'       => 'settings[kCAPTCHA/backgroundColor]',
                                'checkDirty' => false,
                                'width'      => 120,
                                'allowBlank' => false),
                          array('xtype'      => 'checkbox',
                                'fieldLabel' => $this->_['label_kcaptcha_noSpaces'],
                                'checkDirty' => false,
                                'name'       => 'settings[kCAPTCHA/noSpaces]'),
                          array('xtype'      => 'checkbox',
                                'fieldLabel' => $this->_['label_kcaptcha_showСredits'],
                                'checkDirty' => false,
                                'name'       => 'settings[kCAPTCHA/showСredits]'),
                      )
                )
            )
        );

        // вкладка "Список статей"
        $tabList = array(
            'title'       => $this->_['title_tab_list'],
            'layout'      => 'form',
            'labelWidth'  => 250,
            'baseCls'     => 'mn-form-tab-body',
            'iconSrc'     => $this->resourcePath . 'icon-tab-list.png',
            'autoScroll'  => true,
            'defaults'    => array('labelSeparator' => ''),
            'items'       => array(
                array('xtype' => 'mn-field-separator', 'html' => $this->_['title_fieldset_archive']),
                array('xtype'      => 'checkbox',
                      'fieldLabel' => $this->_['label_list_archive_day'],
                      'checkDirty' => false,
                      'name'       => 'settings[LIST/ARCHIVE/DAY]'),
                array('xtype'      => 'checkbox',
                      'fieldLabel' => $this->_['label_list_archive_month'],
                      'checkDirty' => false,
                      'name'       => 'settings[LIST/ARCHIVE/MONTH]'),
                array('xtype'      => 'checkbox',
                      'fieldLabel' => $this->_['label_list_archive_year'],
                      'checkDirty' => false,
                      'name'       => 'settings[LIST/ARCHIVE/YEAR]'),
                array('xtype'       => 'fieldset',
                      'labelWidth'  => 150,
                      'title'       => $this->_['title_fieldset_lparams'],
                      'width'       => 680,
                      'autoHeight'  => true,
                      'collapsible' => true,
                      'collapsed'   => false,
                      'defaults'    => array('labelSeparator' => ':'),
                      'items'       => array(
                          array('xtype'      => 'mn-field-combo',
                                'id'         => 'fldTemplateA',
                                'tpl'        => 
                                    '<tpl for="."><div ext:qtip="{name}" class="x-combo-list-item">'
                                    . '<img width="16px" height="16px" align="absmiddle" src="' 
                                    . $this->path . '../../../templates/Combo/resources/icons/{data}.png" style="margin-right:5px">{name}</div></tpl>',
                                'fieldLabel' => $this->_['label_template_name'],
                                'labelTip'   => $this->_['tip_template_name'],
                                'name'       => 'settings[LIST/ARCHIVE/TEMPLATE]',
                                'checkDirty' => false,
                                'editable'   => false,
                                'width'      => 270,
                                'pageSize'   => 50,
                                'hiddenName' => 'settings[LIST/ARCHIVE/TEMPLATE]',
                                'allowBlank' => true,
                                'store'      => array(
                                    'xtype' => 'jsonstore',
                                    'url'   => $this->componentUrl . 'combo/trigger/?name=templates'
                                )
                            )
                      )
                ),
                array('xtype' => 'mn-field-separator', 'html' => $this->_['title_fieldset_list']),
                array('xtype'       => 'fieldset',
                      'labelWidth'  => 150,
                      'title'       => $this->_['title_fieldset_lparams'],
                      'width'       => 680,
                      'autoHeight'  => true,
                      'collapsible' => true,
                      'collapsed'   => false,
                      'defaults'    => array('labelSeparator' => ''),
                      'items'       => array(
                          array('xtype'      => 'numberfield',
                                'fieldLabel' => $this->_['label_list_limit'],
                                'labelTip'   => $this->_['tip_list_limit'],
                                'name'       => 'settings[LIST/LIMIT]',
                                'checkDirty' => false,
                                'value'      => 0,
                                'width'      => 70,
                                'allowBlank' => false,
                                'emptyText'  => 0),
                          array('xtype'         => 'combo',
                                'fieldLabel'    => $this->_['label_list_order'],
                                'labelTip'      => $this->_['tip_list_order'],
                                'name'          => 'settings[LIST/ORDER]',
                                'checkDirty'    => false,
                                'editable'      => false,
                                'width'         => 157,
                                'typeAhead'     => false,
                                'triggerAction' => 'all',
                                'mode'          => 'local',
                                'store'         => array(
                                    'xtype'  => 'arraystore',
                                    'fields' => array('list', 'value'),
                                    'data'   => $this->_['data_order']
                                ),
                                'hiddenName'    => 'settings[LIST/ORDER]',
                                'valueField'    => 'value',
                                'displayField'  => 'list',
                                'allowBlank'    => false),
                          array('xtype'         => 'combo',
                                'fieldLabel'    => $this->_['label_list_sort'],
                                'labelTip'      => $this->_['tip_list_sort'],
                                'name'          => 'settings[LIST/SORT]',
                                'value'         => 'date',
                                'checkDirty'    => false,
                                'editable'      => false,
                                'width'         => 157,
                                'typeAhead'     => false,
                                'triggerAction' => 'all',
                                'mode'          => 'local',
                                'store'         => array(
                                    'xtype'  => 'arraystore',
                                    'fields' => array('list', 'value'),
                                    'data'   => $this->_['data_sort']
                                ),
                                'hiddenName'    => 'settings[LIST/SORT]',
                                'valueField'    => 'value',
                                'displayField'  => 'list',
                                'allowBlank'    => false),
                          array('xtype'      => 'mn-field-combo',
                                'id'         => 'fldTemplate',
                                'tpl'        => 
                                    '<tpl for="."><div ext:qtip="{name}" class="x-combo-list-item">'
                                    . '<img width="16px" height="16px" align="absmiddle" src="' 
                                    . $this->path . '../../../templates/Combo/resources/icons/{data}.png" style="margin-right:5px">{name}</div></tpl>',
                                'fieldLabel' => $this->_['label_template_name'],
                                'labelTip'   => $this->_['tip_template_name'],
                                'name'       => 'settings[LIST/TEMPLATE]',
                                'checkDirty' => false,
                                'editable'   => false,
                                'width'      => 270,
                                'pageSize'   => 50,
                                'hiddenName' => 'settings[LIST/TEMPLATE]',
                                'allowBlank' => true,
                                'store'      => array(
                                    'xtype' => 'jsonstore',
                                    'url'   => $this->componentUrl . 'combo/trigger/?name=templates'
                                )
                            ),
                            array('xtype'      => 'checkbox',
                                  'fieldLabel' => $this->_['label_list_caching'],
                                  'labelTip'   => $this->_['tip_list_caching'],
                                  'checkDirty' => false,
                                  'name'       => 'settings[LIST/CACHING]'),
                            array('xtype'      => 'textfield',
                                  'fieldLabel' => $this->_['label_list_date'],
                                  'labelTip'   => $this->_['tip_list_date'],
                                  'name'       => 'settings[LIST/DATE/FORMAT]',
                                  'checkDirty' => false,
                                  'width'      => 157,
                                  'allowBlank' => false),
                            array('xtype'         => 'combo',
                                  'fieldLabel'    => $this->_['label_list_nav'],
                                  'name'          => 'settings[LIST/NAVIGATION]',
                                  'checkDirty'    => false,
                                  'editable'      => false,
                                  'width'         => 160,
                                  'typeAhead'     => false,
                                  'triggerAction' => 'all',
                                  'mode'          => 'local',
                                  'store'         => array(
                                      'xtype'  => 'arraystore',
                                      'fields' => array('list', 'value'),
                                      'data'   => $this->_['data_nav']
                                  ),
                                  'hiddenName'    => 'settings[LIST/NAVIGATION]',
                                  'valueField'    => 'value',
                                  'displayField'  => 'list',
                                  'allowBlank'    => false),
                      )
                )
            )
        );

        // вкладка "Общие"
        $tabCommon = array(
            'title'       => $this->_['title_tab_common'],
            'layout'      => 'form',
            'labelWidth'  => 400,
            'baseCls'     => 'mn-form-tab-body',
            'iconSrc'     => $this->resourcePath . 'icon-tab-common.png',
            'autoScroll'  => true,
            'defaults'    => array('labelSeparator' => ''),
            'items'       => array(
                array('xtype' => 'container', 'id' => 'fldUrl', 'width' => 700),
                array('xtype' => 'mn-field-separator'),
                array('xtype'      => 'textfield',
                      'fieldLabel' => $this->_['label_site_name'],
                      'name'       => 'settings[NAME]',
                      'checkDirty' => false,
                      'width'      => 326,
                      'allowBlank' => false),
                array('xtype'      => 'textarea',
                      'fieldLabel' => $this->_['label_site_keywords'],
                      'name'       => 'settings[META/KEYWORDS]',
                      'checkDirty' => false,
                      'anchor'     => '100%',
                      'height'     => 80,
                      'allowBlank' => false),
                array('xtype'      => 'textarea',
                      'fieldLabel' => $this->_['label_site_description'],
                      'name'       => 'settings[META/DESCRIPTION]',
                      'checkDirty' => false,
                      'anchor'     => '100%',
                      'height'     => 80,
                      'allowBlank' => false),
                array('xtype'      => 'textfield',
                      'fieldLabel' => $this->_['label_site_home'],
                      'name'       => 'settings[HOME]',
                      'checkDirty' => false,
                      'width'      => 326,
                      'allowBlank' => false),
                array('xtype'      => 'textfield',
                      'fieldLabel' => $this->_['label_site_host'],
                      'name'       => 'settings[HOST]',
                      'checkDirty' => false,
                      'width'      => 326,
                      'allowBlank' => false),
                array('xtype'      => 'textfield',
                      'fieldLabel' => $this->_['label_site_title'],
                      'name'       => 'settings[TITLE]',
                      'checkDirty' => false,
                      'width'      => 326,
                      'allowBlank' => false),
                array('xtype'      => 'textfield',
                      'fieldLabel' => $this->_['label_site_charset'],
                      'name'       => 'settings[CHARSET]',
                      'checkDirty' => false,
                      'width'      => 160,
                      'allowBlank' => false),
                array('xtype'         => 'combo',
                      'fieldLabel'    => $this->_['label_timezone'],
                      'name'          => 'settings[TIMEZONE]',
                      'checkDirty'    => false,
                      'editable'      => false,
                      'width'         => 326,
                      'typeAhead'     => false,
                      'triggerAction' => 'all',
                      'mode'          => 'local',
                      'store'         => array(
                          'xtype'  => 'arraystore',
                          'fields' => array('value', 'list'),
                          'data'   => $this->_['data_timezone']
                      ),
                      'hiddenName'    => 'settings[TIMEZONE]',
                      'valueField'    => 'value',
                      'displayField'  => 'list',
                      'allowBlank'    => true),
                array('xtype'         => 'combo',
                      'fieldLabel'    => $this->_['label_language'],
                      'name'          => 'settings[LANGUAGE]',
                      'checkDirty'    => false,
                      'editable'      => false,
                      'width'         => 160,
                      'typeAhead'     => false,
                      'triggerAction' => 'all',
                      'mode'          => 'local',
                      'store'         => array(
                          'xtype'  => 'arraystore',
                          'fields' => array('value', 'list'),
                          'data'   => $data['languages']
                      ),
                      'hiddenName'    => 'settings[LANGUAGE]',
                      'valueField'    => 'value',
                      'displayField'  => 'list',
                      'allowBlank'    => true),
                array('xtype'         => 'combo',
                      'fieldLabel'    => $this->_['label_theme'],
                      'name'          => 'settings[THEME]',
                      'checkDirty'    => false,
                      'editable'      => false,
                      'width'         => 160,
                      'typeAhead'     => false,
                      'triggerAction' => 'all',
                      'mode'          => 'local',
                      'store'         => array(
                          'xtype'  => 'arraystore',
                          'fields' => array('value', 'list'),
                          'data'   => $data['themes']
                      ),
                      'hiddenName'    => 'settings[THEME]',
                      'valueField'    => 'value',
                      'displayField'  => 'list',
                      'allowBlank'    => true),
                array('xtype'      => 'checkbox',
                      'fieldLabel' => $this->_['label_error_article'],
                      'checkDirty' => false,
                      'name'       => 'settings[ERROR/ARTICLE]'),
                array('xtype'      => 'checkbox',
                      'fieldLabel' => $this->_['label_outcontrol'],
                      'checkDirty' => false,
                      'name'       => 'settings[OUTCONTROL]'),
                array('xtype' => 'mn-field-separator'),
                array('xtype'         => 'combo',
                      'fieldLabel'    => $this->_['label_editor_article'],
                      'name'          => 'settings[EDITOR/ARTICLE]',
                      'checkDirty'    => false,
                      'editable'      => false,
                      'width'         => 160,
                      'typeAhead'     => false,
                      'triggerAction' => 'all',
                      'mode'          => 'local',
                      'store'         => array(
                          'xtype'  => 'arraystore',
                          'fields' => array('list', 'value'),
                          'data'   => $this->_['data_editors']
                      ),
                      'hiddenName'    => 'settings[EDITOR/ARTICLE]',
                      'valueField'    => 'value',
                      'displayField'  => 'list',
                      'allowBlank'    => false),
                array('xtype'         => 'combo',
                      'fieldLabel'    => $this->_['label_editor_template'],
                      'name'          => 'settings[EDITOR/TEMPLATE]',
                      'checkDirty'    => false,
                      'editable'      => false,
                      'width'         => 160,
                      'typeAhead'     => false,
                      'triggerAction' => 'all',
                      'mode'          => 'local',
                      'store'         => array(
                          'xtype'  => 'arraystore',
                          'fields' => array('list', 'value'),
                          'data'   => $this->_['data_editors']
                      ),
                      'hiddenName'    => 'settings[EDITOR/TEMPLATE]',
                      'valueField'    => 'value',
                      'displayField'  => 'list',
                      'allowBlank'    => false),
                array('xtype'         => 'combo',
                      'fieldLabel'    => $this->_['label_editor_landing'],
                      'name'          => 'settings[EDITOR/LANDING]',
                      'checkDirty'    => false,
                      'editable'      => false,
                      'width'         => 160,
                      'typeAhead'     => false,
                      'triggerAction' => 'all',
                      'mode'          => 'local',
                      'store'         => array(
                          'xtype'  => 'arraystore',
                          'fields' => array('list', 'value'),
                          'data'   => $this->_['data_editors']
                      ),
                      'hiddenName'    => 'settings[EDITOR/LANDING]',
                      'valueField'    => 'value',
                      'displayField'  => 'list',
                      'allowBlank'    => false),
                array('xtype' => 'mn-field-separator'),
                array('xtype'      => 'checkbox',
                      'fieldLabel' => $this->_['label_sef_use'],
                      'checkDirty' => false,
                      'name'       => 'settings[SEF]'),
                array('xtype'         => 'combo',
                      'fieldLabel'    => $this->_['label_sef_type'],
                      'name'          => 'settings[SEF/TYPE]',
                      'checkDirty'    => false,
                      'editable'      => false,
                      'width'         => 160,
                      'typeAhead'     => false,
                      'triggerAction' => 'all',
                      'mode'          => 'local',
                      'store'         => array(
                          'xtype'  => 'arraystore',
                          'fields' => array('list', 'value'),
                          'data'   => $this->_['data_sef_types']
                      ),
                      'hiddenName'    => 'settings[SEF/TYPE]',
                      'valueField'    => 'value',
                      'displayField'  => 'list',
                      'allowBlank'    => true),
                array('xtype' => 'mn-field-separator'),
                array('xtype'      => 'checkbox',
                      'fieldLabel' => $this->_['label_site_notindex'],
                      'checkDirty' => false,
                      'name'       => 'settings[NOTINDEX]'),
                array('xtype'      => 'checkbox',
                      'fieldLabel' => $this->_['label_site_available'],
                      'checkDirty' => false,
                      'name'       => 'settings[INACCESSIBLE]'),
            )
        );

        // вкладка "Rss лента"
        $tabRss = array(
            'title'       => $this->_['title_tab_rss'],
            'layout'      => 'form',
            'labelWidth'  => 400,
            'baseCls'     => 'mn-form-tab-body',
            'iconSrc'     => $this->resourcePath . 'icon-tab-rss.png',
            'autoScroll'  => true,
            'defaults'    => array('labelSeparator' => ''),
            'items'       => array(
                array('xtype' => 'container', 'id' => 'fldRssUrl', 'width' => 700),
                array('xtype' => 'mn-field-separator'),
                array('xtype'      => 'checkbox',
                      'fieldLabel' => $this->_['label_rss_use'],
                      'checkDirty' => false,
                      'name'       => 'settings[RSS]'),
                array('xtype'         => 'combo',
                      'fieldLabel'    => $this->_['label_rss_export'],
                      'name'          => 'settings[RSS/EXPORT/TYPE]',
                      'checkDirty'    => false,
                      'value'         => 1,
                      'editable'      => false,
                      'width'         => 200,
                      'typeAhead'     => false,
                      'triggerAction' => 'all',
                      'mode'          => 'local',
                      'store'         => array(
                          'xtype'  => 'arraystore',
                          'fields' => array('list', 'value'),
                          'data'   => $this->_['data_rss_export']
                      ),
                      'hiddenName'    => 'settings[RSS/EXPORT/TYPE]',
                      'valueField'    => 'value',
                      'displayField'  => 'list',
                      'allowBlank'    => true),
                array('xtype'      => 'numberfield',
                      'fieldLabel' => $this->_['label_rss_export_count'],
                      'name'       => 'settings[RSS/EXPORT/COUNT]',
                      'checkDirty' => false,
                      'value'      => 0,
                      'width'      => 70,
                      'allowBlank' => true,
                      'emptyText'  => 0),
                array('xtype'         => 'combo',
                      'fieldLabel'    => $this->_['label_rss_format'],
                      'name'          => 'settings[RSS/EXPORT/FORMAT]',
                      'checkDirty'    => false,
                      'value'         => 1,
                      'editable'      => false,
                      'width'         => 200,
                      'typeAhead'     => false,
                      'triggerAction' => 'all',
                      'mode'          => 'local',
                      'store'         => array(
                          'xtype'  => 'arraystore',
                          'fields' => array('list', 'value'),
                          'data'   => $this->_['data_rss_format']
                      ),
                      'hiddenName'    => 'settings[RSS/EXPORT/FORMAT]',
                      'valueField'    => 'value',
                      'displayField'  => 'list',
                      'allowBlank'    => true),
                array('xtype'      => 'checkbox',
                      'fieldLabel' => $this->_['label_rss_caching'],
                      'checkDirty' => false,
                      'name'       => 'settings[RSS/CACHING]'),
            )
        );
        // если есть кэш
        if ($data['rss-cache']) {
            $tabRss['items'][] = $data['rss-cache'];
        }

        // вкладка "Карта сайта"
        $tabSitemap = array(
            'title'       => $this->_['title_tab_sitemap'],
            'layout'      => 'form',
            'labelWidth'  => 400,
            'baseCls'     => 'mn-form-tab-body',
            'iconSrc'     => $this->resourcePath . 'icon-tab-sitemap.png',
            'autoScroll'  => true,
            'defaults'    => array('labelSeparator' => ''),
            'items'       => array(
                array('xtype' => 'container', 'id' => 'fldSmUrl', 'width' => 700),
                array('xtype' => 'mn-field-separator'),
                array('xtype'      => 'checkbox',
                      'fieldLabel' => $this->_['label_sm_use'],
                      'checkDirty' => false,
                      'name'       => 'settings[SITEMAP]'),
                array('xtype'      => 'numberfield',
                      'fieldLabel' => $this->_['label_sm_count'],
                      'name'       => 'settings[SITEMAP/COUNT]',
                      'checkDirty' => false,
                      'value'      => 0,
                      'width'      => 70,
                      'allowBlank' => true,
                      'emptyText'  => 0),
                array('xtype'      => 'checkbox',
                      'fieldLabel' => $this->_['label_sm_caching'],
                      'checkDirty' => false,
                      'name'       => 'settings[SITEMAP/CACHING]'),
                array('xtype'      => 'fieldset',
                      'width'      => 607,
                      'labelWidth' => 390,
                      'collapsible' => true,
                      'collapsed'   => true,
                      'title'      => $this->_['title_fieldset_articles'],
                      'autoHeight' => true,
                      'items'      => array(
                          array('xtype'         => 'combo',
                                'fieldLabel'    => $this->_['label_sm_priority'],
                                'name'          => 'settings[SITEMAP/ARTICLES/PRIORITY]',
                                'checkDirty'    => false,
                                'editable'      => true,
                                'maxLength'     => 100,
                                'width'         => 120,
                                'typeAhead'     => false,
                                'triggerAction' => 'all',
                                'mode'          => 'local',
                                'value'         => '0.0',
                                'store'         => array(
                                    'xtype'  => 'arraystore',
                                    'fields' => array('list'),
                                    'data'   => $this->_['data_priorities']
                                ),
                                'hiddenName'    => 'settings[SITEMAP/ARTICLES/PRIORITY]',
                                'valueField'    => 'list',
                                'displayField'  => 'list',
                                'allowBlank'    => true),
                          array('xtype'         => 'combo',
                                'fieldLabel'    => $this->_['label_sm_changefreq'],
                                'name'          => 'settings[SITEMAP/ARTICLES/CHANGEFREQ]',
                                'checkDirty' => false,
                                'editable'      => false,
                                'maxLength'     => 100,
                                'width'         => 120,
                                'typeAhead'     => false,
                                'triggerAction' => 'all',
                                'mode'          => 'local',
                                'store'         => array(
                                    'xtype'  => 'arraystore',
                                    'fields' => array('list', 'value'),
                                    'data'   => $this->_['data_changefreq']
                                ),
                                'hiddenName'    => 'settings[SITEMAP/ARTICLES/CHANGEFREQ]',
                                'valueField'    => 'value',
                                'displayField'  => 'list',
                                'allowBlank'    => true)
                      )
                ),
                array('xtype'      => 'fieldset',
                      'width'      => 607,
                      'labelWidth' => 390,
                      'collapsible' => true,
                      'collapsed'   => true,
                      'title'      => $this->_['title_fieldset_news'],
                      'autoHeight' => true,
                      'items'      => array(
                          array('xtype'         => 'combo',
                                'fieldLabel'    => $this->_['label_sm_priority'],
                                'name'          => 'settings[SITEMAP/NEWS/PRIORITY]',
                                'checkDirty'    => false,
                                'editable'      => true,
                                'maxLength'     => 100,
                                'width'         => 120,
                                'typeAhead'     => false,
                                'triggerAction' => 'all',
                                'mode'          => 'local',
                                'value'         => '0.0',
                                'store'         => array(
                                    'xtype'  => 'arraystore',
                                    'fields' => array('list'),
                                    'data'   => $this->_['data_priorities']
                                ),
                                'hiddenName'    => 'settings[SITEMAP/NEWS/PRIORITY]',
                                'valueField'    => 'list',
                                'displayField'  => 'list',
                                'allowBlank'    => true),
                          array('xtype'         => 'combo',
                                'fieldLabel'    => $this->_['label_sm_changefreq'],
                                'name'          => 'settings[SITEMAP/NEWS/CHANGEFREQ]',
                                'checkDirty'    => false,
                                'editable'      => false,
                                'maxLength'     => 100,
                                'width'         => 120,
                                'typeAhead'     => false,
                                'triggerAction' => 'all',
                                'mode'          => 'local',
                                'store'         => array(
                                    'xtype'  => 'arraystore',
                                    'fields' => array('list', 'value'),
                                    'data'   => $this->_['data_changefreq']
                                ),
                                'hiddenName'    => 'settings[SITEMAP/NEWS/CHANGEFREQ]',
                                'valueField'    => 'value',
                                'displayField'  => 'list',
                                'allowBlank'    => true)
                      )
                ),
                array('xtype'      => 'fieldset',
                      'width'      => 607,
                      'labelWidth' => 390,
                      'collapsible' => true,
                      'collapsed'   => true,
                      'title'      => $this->_['title_fieldset_categories'],
                      'autoHeight' => true,
                      'items'      => array(
                          array('xtype'         => 'combo',
                                'fieldLabel'    => $this->_['label_sm_priority'],
                                'name'          => 'settings[SITEMAP/CATEGORIES/PRIORITY]',
                                'checkDirty'    => false,
                                'editable'      => true,
                                'maxLength'     => 100,
                                'width'         => 120,
                                'typeAhead'     => false,
                                'triggerAction' => 'all',
                                'mode'          => 'local',
                                'value'         => '0.0',
                                'store'         => array(
                                    'xtype'  => 'arraystore',
                                    'fields' => array('list'),
                                    'data'   => $this->_['data_priorities']
                                ),
                                'hiddenName'    => 'settings[SITEMAP/CATEGORIES/PRIORITY]',
                                'valueField'    => 'list',
                                'displayField'  => 'list',
                                'allowBlank'    => true),
                          array('xtype'         => 'combo',
                                'fieldLabel'    => $this->_['label_sm_changefreq'],
                                'name'          => 'settings[SITEMAP/CATEGORIES/CHANGEFREQ]',
                                'checkDirty'    => false,
                                'editable'      => false,
                                'maxLength'     => 100,
                                'width'         => 120,
                                'typeAhead'     => false,
                                'triggerAction' => 'all',
                                'mode'          => 'local',
                                'store'         => array(
                                    'xtype'  => 'arraystore',
                                    'fields' => array('list', 'value'),
                                    'data'   => $this->_['data_changefreq']
                                ),
                                'hiddenName'    => 'settings[SITEMAP/CATEGORIES/CHANGEFREQ]',
                                'valueField'    => 'value',
                                'displayField'  => 'list',
                                'allowBlank'    => true)
                      )
                )
            )
        );
        // если есть кэш
        if ($data['sm-cache']) {
            $tabSitemap['items'][] = $data['sm-cache'];
        }

        // вкладка "Справка"
        $tabGuide = array(
            'title'       => $this->_['title_tab_guide'],
            'layout'      => 'form',
            'labelWidth'  => 300,
            'baseCls'     => 'mn-form-tab-body',
            'iconSrc'     => $this->resourcePath . 'icon-tab-guide.png',
            'autoScroll'  => true,
            'defaults'    => array('labelSeparator' => ''),
            'items'       => array(
                array('xtype'      => 'textfield',
                      'fieldLabel' => $this->_['label_guide_resource'],
                      'name'       => 'settings[GUIDE/RESOURCE]',
                      'checkDirty' => false,
                      'anchor'     => '100%',
                      'allowBlank' => true),
                array('xtype'      => 'textfield',
                      'fieldLabel' => $this->_['label_guide_tree'],
                      'name'       => 'settings[GUIDE/TREE]',
                      'checkDirty' => false,
                      'anchor'     => '100%',
                      'allowBlank' => true)
            )
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->url = $this->componentUrl . 'profile/';
        $form->items->addItems(
            array(
                array('xtype'             => 'tabpanel',
                      'layoutOnTabChange' => true,
                      'deferredRender'    => false,
                      'activeTab'         => 0,
                      'style'             => 'padding:3px',
                      'enableTabScroll'   => true,
                      'anchor'            => '100% 100%',
                      'items'             => array($tabCommon, $tabSafeness, $tabList, $tabRss, $tabSitemap, $tabDir, $tabImages, $tabOptim, $tabMail, $tabGuide))
            )
        );

        parent::getInterface();
    }
}
?>