<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля настроеек сайта"
 * Пакет контроллеров "Настройки сайта"
 * Группа пакетов     "Настройки"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SConfig_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

define('_INC', 1);

Gear::controller('Profile/Data');

/**
 * Возращает шаблон файла конфигурации сайта
 * 
 * @params string $tpl данные в шаблоне
 */
function getTplSiteConfig($tpl)
{
    return <<<TEXT
<?php
/**
 * Gear CMS
 *
 * Настройки сайта (создан: {$tpl['date']})
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 *
 * @category   Gear
 * @package    Config
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    \$Id: Site.php {$tpl['date']} Gear Magic \$
 */

return array(
    // Общие настройки
    // название сайта
    'NAME'      => '{$tpl['NAME']}',
    // домашняя страница сайта
    'HOME'      => '{$tpl['HOME']}',
    // название домена
    'HOST'      => '{$tpl['HOST']}',
    // IP адрес вашего сервера
    'IP_ADDRESS'=> '{$tpl['IP_ADDRESS']}',
    // шаблон загаловка страницы
    'TITLE'     => '{$tpl['TITLE']}',
    // кодировка страниц сайта
    'CHARSET'   => '{$tpl['CHARSET']}',
    // часовой пояс
    'TIMEZONE'  => '{$tpl['TIMEZONE']}',
    // язык по умолчанию
    'LANGUAGE'       => '{$tpl['LANGUAGE']}',
    'LANGUAGE/ID'    => {$tpl['LANGUAGE/ID']},
    'LANGUAGE/ALIAS' => '{$tpl['LANGUAGE/ALIAS']}',
    // языки на сайте
    'LANGUAGES' => array(
        {$tpl['LANGUAGES']}
    ),
    // шаблон сайта по умолчанию
    'THEME'     => '{$tpl['THEME']}',
    // использовать ЧПУ
    'SEF'       => {$tpl['SEF']},
    // тип ЧПУ
    'SEF/TYPE'  => {$tpl['SEF/TYPE']},
    // если сайт не доступный - false
    'INACCESSIBLE'  => {$tpl['INACCESSIBLE']},
    // ключевые слова (глобальные)
    'META/KEYWORDS'    => '{$tpl['META/KEYWORDS']}',
    // описание (глобальные)
    'META/DESCRIPTION' => '{$tpl['META/DESCRIPTION']}',
    // вывод ошибок в шаблоне статьи
    'ERROR/ARTICLE'    => {$tpl['ERROR/ARTICLE']},
    // вид редактора статьи
    'EDITOR/ARTICLE'   => '{$tpl['EDITOR/ARTICLE']}',
    // вид редактора шаблона
    'EDITOR/TEMPLATE'  => '{$tpl['EDITOR/TEMPLATE']}',
    // вид редактора landing страницы
    'EDITOR/LANDING'   => '{$tpl['EDITOR/LANDING']}',
    // управление несколькими доменами:
    'OUTCONTROL'       => {$tpl['OUTCONTROL']},
    // отключить индексацию
    'NOTINDEX'         => {$tpl['NOTINDEX']},

    // RSS лента
    // использовать RSS экспорт новостей
    'RSS'               => {$tpl['RSS']},
    // тип экспорта RSS ленты
    'RSS/EXPORT/TYPE'   => {$tpl['RSS/EXPORT/TYPE']},
    // количество экспортируемых статей
    'RSS/EXPORT/COUNT'  => {$tpl['RSS/EXPORT/COUNT']},
    // Формат экспорта RSS ленты
    'RSS/EXPORT/FORMAT' => {$tpl['RSS/EXPORT/FORMAT']},
    // кэширование RSS ленты
    'RSS/CACHING'       => {$tpl['RSS/CACHING']},

    // Карта сайта для Google, Yandex, ...
    // использовать карту сайта
    'SITEMAP'                       => {$tpl['SITEMAP']},
    // количество записей
    'SITEMAP/COUNT'                 => {$tpl['SITEMAP/COUNT']},
    // кэширование карты сайта
    'SITEMAP/CACHING'               => {$tpl['SITEMAP/CACHING']},
    // приоритетность статьи
    'SITEMAP/ARTICLES/PRIORITY'     => '{$tpl['SITEMAP/ARTICLES/PRIORITY']}',
    // частота сканирования статьи
    'SITEMAP/ARTICLES/CHANGEFREQ'   => '{$tpl['SITEMAP/ARTICLES/CHANGEFREQ']}',
    // приоритетность новости
    'SITEMAP/NEWS/PRIORITY'         => '{$tpl['SITEMAP/NEWS/PRIORITY']}',
    // частота сканирования новости
    'SITEMAP/NEWS/CHANGEFREQ'       => '{$tpl['SITEMAP/NEWS/CHANGEFREQ']}',
    // приоритетность категории
    'SITEMAP/CATEGORIES/PRIORITY'   => '{$tpl['SITEMAP/CATEGORIES/PRIORITY']}',
    // частота сканирования категории
    'SITEMAP/CATEGORIES/CHANGEFREQ' => '{$tpl['SITEMAP/CATEGORIES/CHANGEFREQ']}',

    // Безопасность
    // ключ безопасности
    'CMS/TOKEN'    => '{$tpl['CMS/TOKEN']}',
    // разрешить AJAX запросы
    'AJAX'         => {$tpl['AJAX']},
    // задействовать капчу
    'CAPTCHA'      => {$tpl['CAPTCHA']},
    // тип кода безопасности капчи
    'CAPTCHA/TYPE' => '{$tpl['CAPTCHA/TYPE']}',
    // настройка reCAPTCHA
    'reCAPTCHA'    => array(
        // публичный ключ сервиса reCAPTCHA
        'public_key'  => '{$tpl['reCAPTCHA/PUBLIC_KEY']}',
        // секретный ключ сервиса reCAPTCHA
        'private_key' => '{$tpl['reCAPTCHA/PRIVATE_KEY']}',
        // оформление reCAPTCHA
        'theme'       => '{$tpl['reCAPTCHA/THEME']}',
    ),
    // настройка kCAPTCHA
    'kCAPTCHA'         => array(
        // JPEG quality of CAPTCHA image (bigger is better quality, but larger file size)
        'jpegQuality'          => 90,
        // CAPTCHA image colors (RGB, 0-255)
        'foregroundColor'      => array(mt_rand(0,80), mt_rand(0,80), mt_rand(0,80)),
        'backgroundColor'      => array({$tpl['kCAPTCHA/backgroundColor']}),
        //  set to false to remove credits line. Credits adds 12 pixels to image height
        'showСredits'          => {$tpl['kCAPTCHA/showСredits']},
        'credits'              => '{$_SERVER['SERVER_NAME']}',
        // increase safety by prevention of spaces between symbols
        'noSpaces'             => {$tpl['kCAPTCHA/noSpaces']},
        // folder with fonts
        'fontsDir'             => 'fonts',
        // CAPTCHA image size (you do not need to change it, this parameters is optimal)
        'width'                => {$tpl['kCAPTCHA/width']},
        'height'               => {$tpl['kCAPTCHA/height']},
        // CAPTCHA string length random 5 or 6 or 7
        'length'               => mt_rand({$tpl['kCAPTCHA/_length']}),
        '_length'              => '{$tpl['kCAPTCHA/_length']}',
        // KCAPTCHA configuration file (do not change without changing font files!)
        'alphabet'             => "{$tpl['kCAPTCHA/alphabet']}",
        // symbols used to draw CAPTCHA (alphabet without similar symbols (o=0, 1=l, i=j, t=f))
        'allowedSymbols'       => "{$tpl['kCAPTCHA/allowedSymbols']}",
        // symbol's vertical fluctuation amplitude
        'fluctuationAmplitude' => {$tpl['kCAPTCHA/fluctuationAmplitude']},
        // noise no white noise
        'whiteNoiseDensity'    => {$tpl['kCAPTCHA/whiteNoiseDensity']},
        // noise no white noise no black noise
        'blackNoiseDensity'    => {$tpl['kCAPTCHA/blackNoiseDensity']},
        // type file output
        'type'                 => '{$tpl['kCAPTCHA/type']}'
    ),

    // Список статей
    // список архива статей
    // вывод архива статей за указанный день
    'LIST/ARCHIVE/DAY'      => {$tpl['LIST/ARCHIVE/DAY']},
    // вывод архива статей за указанный месяц
    'LIST/ARCHIVE/MONTH'    => {$tpl['LIST/ARCHIVE/MONTH']},
    // вывод архива статей за указанный год
    'LIST/ARCHIVE/YEAR'     => {$tpl['LIST/ARCHIVE/YEAR']},
    // шаблон
    'LIST/ARCHIVE/TEMPLATE' => '{$tpl['LIST/ARCHIVE/TEMPLATE']}',
    // список статей
    // записей на странице
    'LIST/LIMIT'    => {$tpl['LIST/LIMIT']},
    // порядок сортировки
    'LIST/ORDER'    => '{$tpl['LIST/ORDER']}',
    // критерий сортировки
    'LIST/SORT'     => '{$tpl['LIST/SORT']}',
    // шаблон
    'LIST/TEMPLATE' => '{$tpl['LIST/TEMPLATE']}',
    // кэширование
    'LIST/CACHING'  => {$tpl['LIST/CACHING']},
    // формат даты
    'LIST/DATE/FORMAT' => '{$tpl['LIST/DATE/FORMAT']}',
    // навигация в списке
    'LIST/NAVIGATION'  => '{$tpl['LIST/NAVIGATION']}',
    // вид списка
    'LIST/TYPES'       => array(
        'ListArticles'   => array('name' => 'Статьи категории', 'path' => 'list/articles/'),
        'Albums'         => array('name' => 'Фотоальбомы', 'path' => 'list/albums/'),
        'PartnersList'   => array('name' => 'Заведения', 'path' => 'partners/list/'),
        'VideoList'      => array('name' => 'Видеозаписи', 'path' => 'video/list/'),
        'ListCategories' => array('name' => 'Подкатегории в категории', 'path' => 'list/categories/')
    ),

    // Настройки E-Mail
    // e-mail адрес администратора
    'MAIL/ADMIN'         => '{$tpl['MAIL/ADMIN']}',
    // e-mail адрес уведомителя
    'MAIL/NOTIFICATION'  => '{$tpl['MAIL/NOTIFICATION']}',
    // Заголовок отправителя писем, при отправке писем
    'MAIL/HEADER'        => '{$tpl['MAIL/HEADER']}',
    // метод отправки почты
    'MAIL/TYPE'          => '{$tpl['MAIL/TYPE']}',
    // SMTP хост
    'MAIL/SMTP/HOST'     => '{$tpl['MAIL/SMTP/HOST']}',
    // SMTP порт
    'MAIL/SMTP/PORT'     => '{$tpl['MAIL/SMTP/PORT']}',
    // SMTP имя пользователя
    'MAIL/SMTP/LOGIN'    => '{$tpl['MAIL/SMTP/LOGIN']}',
    // SMTP пароль
    'MAIL/SMTP/PASSWORD' => '{$tpl['MAIL/SMTP/PASSWORD']}',
    // использовать защищенный протокол
    'MAIL/SMTP/SECURE'   => '{$tpl['MAIL/SMTP/SECURE']}',
    // SMTP e-mail для авторизации на SMTP сервере
    'MAIL/SMTP/MAIL'     => '{$tpl['MAIL/SMTP/MAIL']}',

    // Допустимые расширения файлов
    // для изображений
    'FILES/EXT/IMAGES' => '{$tpl['FILES/EXT/IMAGES']}',
    // для документов
    'FILES/EXT/DOCS'   => '{$tpl['FILES/EXT/DOCS']}',
    // для аудио
    'FILES/EXT/AUDIO'  => '{$tpl['FILES/EXT/AUDIO']}',
    // для видео
    'FILES/EXT/VIDEO'  => '{$tpl['FILES/EXT/VIDEO']}',

    // Каталоги файлов
    // каталог тем
    'DIR/THEMES'     => '{$tpl['DIR/THEMES']}',
    // каталог данных сайта
    'DIR/DATA'       => '{$tpl['DIR/DATA']}',
    // аудио
    'DIR/AUDIO'      => '{$tpl['DIR/AUDIO']}',
    // видео
    'DIR/VIDEO'      => '{$tpl['DIR/VIDEO']}',
    // документы
    'DIR/DOCS'       => '{$tpl['DIR/DOCS']}',
    // изображения банеров
    'DIR/BANNERS'    => '{$tpl['DIR/BANNERS']}',
    // изображения слайдеров
    'DIR/SLIDERS'    => '{$tpl['DIR/SLIDERS']}',
    // изображения в статьях
    'DIR/IMAGES'     => '{$tpl['DIR/IMAGES']}',
    // изображения категорий
    'DIR/CATEGORIES' => '{$tpl['DIR/CATEGORIES']}',
    // временный каталог
    'DIR/TEMP'       => '{$tpl['DIR/TEMP']}',

    // Изображения
    // размеры оригинального изображения
    'IMAGE/ORIGINAL/SIZE' => '{$tpl['IMAGE/ORIGINAL/WIDTH']}x{$tpl['IMAGE/ORIGINAL/HEIGHT']}',
    // размеры средний копии изображения
    'IMAGE/MEDIUM/SIZE'   => '{$tpl['IMAGE/MEDIUM/WIDTH']}x{$tpl['IMAGE/MEDIUM/HEIGHT']}',
    // размеры миниатюры изображения
    'IMAGE/THUMB/SIZE'    => '{$tpl['IMAGE/THUMB/WIDTH']}x{$tpl['IMAGE/THUMB/HEIGHT']}',
    // качество изображения
    'IMAGE/QUALITY'       => {$tpl['IMAGE/QUALITY']},

    // Оптимизация
    // включить кэширование
    'CACHE'                 => {$tpl['CACHE']},
    // тип кеширования
    'CACHE/TYPE'            => '{$tpl['CACHE/TYPE']}',
    // настройки подключения к серверу Memcache
    'CACHE/MEMCACHE/SERVER' => '{$tpl['CACHE/MEMCACHE/SERVER']}',
    // каталог файлов кэша
    'CACHE/DIR'             => '{$tpl['CACHE/DIR']}',
    // включить Gzip сжатие HTML страниц
    'GZIP'                  => {$tpl['GZIP']},
    // использовать отладчик
    'DEBUG'                 => {$tpl['DEBUG']},
    // включить разметку Open Graph
    'OPENGRAPH'             => {$tpl['OPENGRAPH']},
    // одна картинка для всех статей в Open Graph разметке
    'OPENGRAPH/IMAGE'       => '{$tpl['OPENGRAPH/IMAGE']}',
    // навигация по страницам
    'BREADCRUMBS'           => {$tpl['BREADCRUMBS']},
    // выводить нулевой уровень навигации
    'BREADCRUMBS/ZERO'      => {$tpl['BREADCRUMBS/ZERO']},
    // работа с глобальными тегами
    'GLOBAL/TAGS'           => {$tpl['GLOBAL/TAGS']},
    // включить календарь
    'CALENDAR'              => {$tpl['CALENDAR']},
    // подсветка дней в календаре
    'CALENDAR/HIGHLIGHT'    => {$tpl['CALENDAR/HIGHLIGHT']},
    // вести статистику посещения сайта роботами
    'ROBOTS'                => {$tpl['ROBOTS']},
    // вести подсчет посещаемости страниц сайта
    'CONSIDER/VISITS'       => {$tpl['CONSIDER/VISITS']},
    // вести подсчет посещаемости страниц сайта
    'BANNERS'               => {$tpl['BANNERS']},
    // поиск статей по коротким тегам
    'ARTICLE/TAGS'          => {$tpl['ARTICLE/TAGS']},
    // использовать канонические ссылки
    'CANONICAL'             => {$tpl['CANONICAL']},

    // Водяной знак
    // файл штампа
    'WATERMARK/STAMP'    => '{$tpl['WATERMARK/STAMP']}',
    // положение штампа
    'WATERMARK/POSITION' => '{$tpl['WATERMARK/POSITION']}',

    // Справка
    // ресурс справки
    'GUIDE/RESOURCE' => '{$tpl['GUIDE/RESOURCE']}',
    // каталог разделов
    'GUIDE/TREE'     => '{$tpl['GUIDE/TREE']}'
);
?>
TEXT;
}

/**
 * Данные профиля настроеек сайта
 * 
 * @category   Gear
 * @package    GController_SConfig_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SConfig_Profile_Data extends GController_Profile_Data
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sconfig_profile';

    /**
     * Файл настроек сайта
     *
     * @var string
     */
    public $filename = 'Site.php';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return $this->_['profile_title_update'];
    }

    /**
     * Обновление настроеек сайта
     * 
     * @return void
     */
    protected function fileUpdate($params)
    {
        if (file_put_contents('../' . PATH_CONFIG_CMS . $this->filename, getTplSiteConfig($params), FILE_TEXT) === false)
            throw new GException('Error', sprintf($this->_['msg_create_file'], $this->filename));
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        $langs = $this->getLanguages();
        $params['LANGUAGES'] = $langs[0];
        $params['LANGUAGE/ID'] = $langs[1][$params['LANGUAGE']]['language_id'];
        $params['LANGUAGE/ALIAS'] = $langs[1][$params['LANGUAGE']]['language_alias'];
        $params['META/DESCRIPTION'] = htmlspecialchars($params['META/DESCRIPTION'], ENT_QUOTES);
        $params['META/KEYWORDS'] = htmlspecialchars($params['META/KEYWORDS'], ENT_QUOTES);
        if (empty($params['IMAGE/ORIGINAL/WIDTH']))
            $params['IMAGE/ORIGINAL/WIDTH'] = '0';
        if (empty($params['IMAGE/ORIGINAL/HEIGHT']))
            $params['IMAGE/ORIGINAL/HEIGHT'] = '0';
        if (empty($params['IMAGE/MEDIUM/WIDTH']))
            $params['IMAGE/MEDIUM/WIDTH'] = '0';
        if (empty($params['IMAGE/MEDIUM/HEIGHT']))
            $params['IMAGE/MEDIUM/HEIGHT'] = '0';
        if (empty($params['IMAGE/THUMB/WIDTH']))
            $params['IMAGE/THUMB/WIDTH'] = '0';
        if (empty($params['IMAGE/THUMB/HEIGHT']))
            $params['IMAGE/THUMB/HEIGHT'] = '0';

        $params['date'] = date('d-m-Y H:i:s');
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        parent::dataAccessUpdate();

        if ($this->input->isEmpty('PUT'))
            throw new GException('Warning', 'To execute a query, you must change data!');
        $params = $this->input->get('settings', array());
        // проверки входных данных
        $this->isDataCorrect($params);
        // проверка существования записи с одинаковыми значениями
        $this->isDataExist($params);
        // использование системных полей в запросе SQL
        if ($this->isSysFields) {
            $params['sys_date_update'] = date('Y-m-d');
            $params['sys_time_update'] = date('H:i:s');
            $params['sys_user_update'] = $this->session->get('user_id');
        }
        // обновить файл конфигурации
        $this->fileUpdate($params);
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($params, 'method "PUT"');
        }
    }

    /**
     * Возращает список языков сайта
     * 
     * @return string
     */
    protected function getLanguages()
    {
        $query = new GDb_Query();
        // список языков
        $sql = "SELECT * FROM `site_languages` WHERE `language_visible`=1";
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $count = $query->getCountRecords();
        $i = 0;
        $str = '';
        $arr = array();
        while (!$query->eof()) {
            $rec = $query->next();
            $str .= "'{$rec['language_full']}' => array('title' => '{$rec['language_name']}', 'id' => {$rec['language_id']}, 'alias' => '{$rec['language_alias']}')";
            $i++;
            if ($i < $count)
                $str .= ",\n        ";
            $arr[$rec['language_full']] = $rec;
        }

        return array($str, $arr);
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        parent::dataAccessView();

        $setTo = array();

        // если файл настроек сайта не существует
        if (!file_exists('../' . PATH_CONFIG_CMS . $this->filename))
            throw new GException('Error', sprintf($this->_['msg_file_exist'], PATH_CONFIG_CMS . $this->filename));

        // настройки сайта
        $arrSettings = include('../' . PATH_CONFIG_CMS . $this->filename);
        // поумолчанию задать
        if (empty($arrSettings['MAIL/ADMIN']))
            $arrSettings['MAIL/ADMIN'] = 'noreply@' . $_SERVER['SERVER_NAME'];
        if (empty($arrSettings['MAIL/TYPE']))
            $arrSettings['MAIL/TYPE'] = 'PHP mail';
        if (empty($arrSettings['MAIL/SMTP/HOST']))
            $arrSettings['MAIL/SMTP/HOST'] = 'localhost';
        if (empty($arrSettings['MAIL/SMTP/PORT']))
            $arrSettings['MAIL/SMTP/PORT'] = '25';
        if (empty($arrSettings['MAIL/SMTP/SECURE']))
            $arrSettings['MAIL/SMTP/SECURE'] = 0;
        if (empty($arrSettings['IP_ADDRESS']))
            $arrSettings['IP_ADDRESS'] = $_SERVER['SERVER_ADDR'];
        if (!empty($arrSettings['META/KEYWORDS']))
            $arrSettings['META/KEYWORDS'] = htmlspecialchars_decode($arrSettings['META/KEYWORDS'], ENT_QUOTES);
        if (!empty($arrSettings['META/DESCRIPTION']))
            $arrSettings['META/DESCRIPTION'] = htmlspecialchars_decode($arrSettings['META/DESCRIPTION'], ENT_QUOTES);
        list($arrSettings['IMAGE/ORIGINAL/WIDTH'], $arrSettings['IMAGE/ORIGINAL/HEIGHT']) = explode('x', $arrSettings['IMAGE/ORIGINAL/SIZE']);
        list($arrSettings['IMAGE/MEDIUM/WIDTH'], $arrSettings['IMAGE/MEDIUM/HEIGHT']) = explode('x', $arrSettings['IMAGE/MEDIUM/SIZE']);
        list($arrSettings['IMAGE/THUMB/WIDTH'], $arrSettings['IMAGE/THUMB/HEIGHT']) = explode('x', $arrSettings['IMAGE/THUMB/SIZE']);

        $settings = array();
        foreach ($arrSettings as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $subkey => $subvalue) {
                    $settings['settings[' . $key . '/' . $subkey . ']'] = $subvalue;
                }
            } else
                $settings['settings[' . $key . ']'] = $value;
        }

        $query = new GDb_Query();
        // шаблон списка архива статей
        if (!empty($settings['settings[LIST/ARCHIVE/TEMPLATE]'])) {
            $sql = 'SELECT * FROM `site_templates` WHERE `template_filename`=\'' . $settings['settings[LIST/ARCHIVE/TEMPLATE]'] . '\'';
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            //print_r($rec);
            if (!empty($rec))
                $setTo[] = array('xtype' => 'mn-field-combo', 'id' => 'fldTemplateA', 'text' => $rec['template_name'], 'value' => $rec['template_filename']);
        }

        // шаблон списка статей
        if (!empty($settings['settings[LIST/TEMPLATE]'])) {
            $sql = 'SELECT * FROM `site_templates` WHERE `template_filename`=\'' . $settings['settings[LIST/TEMPLATE]'] . '\'';
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            //print_r($rec);
            if (!empty($rec))
                $setTo[] = array('xtype' => 'mn-field-combo', 'id' => 'fldTemplate', 'text' => $rec['template_name'], 'value' => $rec['template_filename']);
        }

        $route = $this->config->getFromCms('Router', 'route');
        // установка полей
        if ($route) {
            if (isset($route['rss']))
                $rssAddr = 'http://' . $_SERVER['SERVER_NAME'] . $route['rss']['value'];
            if (isset($route['sitemap']))
                $smAddr = 'http://' . $_SERVER['SERVER_NAME'] . $route['sitemap']['value'];
        } else {
            $rssAddr = 'http://' . $_SERVER['SERVER_NAME'];
            $smAddr = 'http://' . $_SERVER['SERVER_NAME'];
        }
        $siteAddr = 'http://' . $_SERVER['SERVER_NAME'] . '/';
        array_push($setTo,
            array('id' => 'fldIpAddress', 'func' => 'update', 'args' => array(array(sprintf($this->_['html_ip_address'], $_SERVER['REMOTE_ADDR'])))),
            array('id' => 'fldUrl', 'func' => 'update', 'args' => array(array('<div style="font-size:13px;margin-bottom:5px;">Адрес сайта: <a href="' . $siteAddr . '" target="_blank">' . $siteAddr . '</a></div>'))),
            array('id' => 'fldSmUrl', 'func' => 'update', 'args' => array(array('<div style="font-size:13px;margin-bottom:5px;">Адрес карты сайта: <a href="' . $smAddr . '" target="_blank">' . $smAddr . '</a></div>'))),
            array('id' => 'fldRssUrl', 'func' => 'update', 'args' => array(array('<div style="font-size:13px;margin-bottom:5px;">Адрес RSS ленты: <a href="' . $rssAddr . '" target="_blank">' . $rssAddr . '</a></div>'))) 
        );
        $this->response->add('setTo', $setTo);

        $this->response->data = $settings;
    }
}
?>