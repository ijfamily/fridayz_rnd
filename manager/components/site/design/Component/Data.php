<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные интерфейса свойств компонента"
 * Пакет контроллеров "Профиль свойств компонента"
 * Группа пакетов     "Конструктор страниц"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SDesign_Component
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные интерфейса свойств компонента
 * 
 * @category   Gear
 * @package    GController_SDesign_Component
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SDesign_Component_Data extends GController_Profile_Data
{
    /**
     * Первичный ключ таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'page_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_pages';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sdesign_view';

    /**
     * Идент. компонента
     *
     * @var string
     */
    protected $_componentId = '';

    /**
     * Принадлежность компонента
     *
     * @var string
     */
    protected $_belongTo = '';

    /**
     * Идентификатор шаблона
     *
     * @var integer
     */
    protected $_templateId = '';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // идент. компонента
        $this->_componentId = $this->store->get('componentId');
        // принадлежность компонента
        $this->_belongTo = $this->store->get('belongTo');
        if ($this->_belongTo == 'template') {
            $this->tableName = 'site_templates';
            $this->idProperty = 'template_id';
        }
        // идент. шаблона
        $this->_templateId = $this->store->get('templateId');
        // компонент
        $this->_component = $this->store->get('component');
    }

    /**
     * Возращает компоненты статьи
     * 
     * @param integer $recordId идент. статьи
     * @return array
     */
    protected function getComponents($recordId)
    {
        $query = new GDb_Query();
        // выбранная статья
        if ($this->_belongTo == 'template')
            $sql = 'SELECT * FROM `site_templates` WHERE `template_id`=' . $recordId;
        else
            $sql = 'SELECT * FROM `site_pages` WHERE `page_id`=' . $recordId;
        if (($page = $query->getRecord($sql)) === false)
            throw new GSqlException();
        // если нет данных компонентов в статье
        if (empty($page['component_attributes'])) return array();
        // компоненты статьи
        $components = json_decode($page['component_attributes'], true);
        switch (json_last_error()) {
            case JSON_ERROR_DEPTH:
                throw new GException('Data processing error', 'Maximum stack depth exceeded');
                break;

            case JSON_ERROR_CTRL_CHAR:
                throw new GException('Data processing error', 'Unexpected control character found');
                break;

            case JSON_ERROR_SYNTAX:
            case 2:
                throw new GException('Data processing error', 'Syntax error, malformed JSON');
                break;

            case JSON_ERROR_NONE:
                break;
        }

        return $components;
    }

    /**
     * Возращает текст для выпадающего списка
     * 
     * @param string $field название поля
     * @param mixed $value значение
     * @return string
     */
    protected function getComboText($field, $value, $alias, $query)
    {
        if ($field == 'data-id' && $alias == 'galleries') {
            $sql = "SELECT * FROM `site_gallery` WHERE `gallery_id`='$value' ORDER BY `gallery_name`";
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            else
                return array('id' => 'fldDataId', 'xtype' => 'mn-field-combo', 'value' => $value, 'text' => $rec['gallery_name']);
        } else
        if ($field == 'data-id' && $alias == 'slider') {
            $sql = "SELECT * FROM `site_sliders` WHERE `slider_id`='$value' ORDER BY `slider_name`";
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            else
                return array('id' => 'fldDataId', 'xtype' => 'mn-field-combo', 'value' => $value, 'text' => $rec['slider_name']);
        } else
        if ($field == 'template') {
            $sql = "SELECT * FROM `site_templates` WHERE `template_filename`='$value' ORDER BY `template_name`";
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            else
                return array('id' => 'fldTemplate', 'xtype' => 'mn-field-combo', 'value' => $value, 'text' => $rec['template_name']);
        }

        return false;
    }

    /**
     * Событие наступает после успешного обновления данных
     * 
     * @param array $data сформированные данные полученные после запроса к таблице
     * @return void
     */
    protected function dataUpdateComplete($data = array())
    {
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        parent::dataAccessUpdate();

        $this->response->setMsgResult($this->_['title_updating_property'], $this->_['msg_is_successful_updated'], true);
        $table = new GDb_Table($this->tableName, $this->idProperty, $this->fields);
        $props = $this->input->get('properties');
        if (empty($props))
            throw new GException('Warning', 'To execute a query, you must change data!');
        // компоненты статьи
        $components = $this->getComponents($this->uri->id);
        // если нет компонента
        if (!isset($components[$this->_componentId]))
            $components[$this->_componentId] = $props;
        else
            $components[$this->_componentId] = array_merge($components[$this->_componentId], $props);
        // обновить
        $params = array('component_attributes' => json_encode($components));
        if ($table->update($params, $this->uri->id) === false)
            throw new GSqlException();
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($table->query->getSQL(), 'query');
        }
        // запись в журнал действий пользователя
        $this->logUpdate(
            array('log_query'        => $table->query->getSQL(),
                  'log_error'        => $this->response->success === false ? $table->getError() : null,
                  'log_query_params' => $params)
        );

        $this->dataUpdateComplete($params);
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        parent::dataAccessView();

        // идент. компонента
        $componentId = $this->store->get('componentId');
        // компоненты статьи
        $components = $this->getComponents($this->uri->id);
        // если нет компонента
        if (!isset($components[$componentId])) return;
        $data =  $components[$componentId];
        // свойства компонента
        $setTo = array();
        $alias = $this->_component['cmp_alias'];
        //GFactory::getDg()->log($components);
        $query = new GDb_Query();
        foreach($data as $name => $value) {
            if ($value) {
                $to = $this->getComboText($name, $value, $alias, $query);
                $setTo[] = $to;
            }
            $this->_data['properties[' . $name . ']'] = $value;
        }
        $this->response->data = $this->recordPreprocessing($this->_data);

        // установка полей
        if ($setTo)
            $this->response->add('setTo', $setTo);
    }

}
?>