<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс свойств компонента"
 * Пакет контроллеров "Профиль свойств компонента"
 * Группа пакетов     "Конструктор страниц"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SDesign_Component
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс свойств компонента
 * 
 * @category   Gear
 * @package    GController_SDesign_Component
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SDesign_Component_Interface extends GController_Profile_Interface
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sdesign_view';

    /**
     * Идентификатор компонента
     *
     * @var string
     */
    protected $_componentId = '';

    /**
     * Класс компонента
     *
     * @var string
     */
    protected $_componentClass = '';

    /**
     * Принадлежность компонента
     *
     * @var string
     */
    protected $_belongTo = '';

    /**
     * Идентификатор шаблона
     *
     * @var integer
     */
    protected $_templateId = '';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // идент. компонента
        $this->_componentId = $this->uri->getVar('cId');
        $this->store->set('componentId', $this->_componentId);
        // класс компонента
        $this->_componentClass = $this->uri->getVar('cClass');
        $this->store->set('componentClass', $this->_componentClass);
        // принадлежность компонента
        $this->_belongTo = $this->uri->getVar('bTo');
        $this->store->set('belongTo', $this->_belongTo);
        // идент. шаблона
        $this->_templateId = $this->uri->getVar('tId');
        $this->store->set('templateId', $this->_templateId);
    }

    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        parent::getDataInterface();

        Gear::library('Site');
        // компонент сайта
        $res = GSite::getComponentBy(0, $this->_componentClass);
        // если нет свойств
        if (empty($res['data']))
            throw new GException('Warning', $this->_['msg_empty_properties']);
        // передать компонент
        $this->store->set('component', $res['component']);

        return array('data' => $res['data'], 'component' => $res['component']);
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => sprintf($this->_['title_profile_edit'], $data['component']['cmp_name']),
                  'titleEllipsis' => 65,
                  'width'         => 500,
                  'autoHeight'    => true,
                  'btnDeleteHidden' => true,
                  'stateful'        => false)
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($data['data']['fields']);
        $form->url = $this->componentUrl . 'component/';
        $form->labelWidth = 160;
        $form->autoScroll = true;

        // если есть свойства формы
        if (isset($data['data']['form'])) {
            foreach ($data['data']['form'] as $field => $value)
                $form->$field = $value;
        }

        parent::getInterface();
    }
}
?>