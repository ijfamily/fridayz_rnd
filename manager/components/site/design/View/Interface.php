<?php
/**
 * Gear Manager
 *
 * Контроллер         "Просмотр статьи сайта"
 * Пакет контроллеров "Статьи"
 * Группа пакетов     "Статьи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SArticles_View
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::ext('Container/Panel');
Gear::controller('Interface');

/**
 * Просмотр статьи сайта
 * 
 * @category   Gear
 * @package    GController_SDesign_View
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SDesign_View_Interface extends GController_Interface
{
    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // проверка безопасности
        $url = 'http://<b>www.</b>' . $_SERVER['HTTP_HOST'] . '/' . PATH_APPLICATION . '?key=' . $this->config->get('SECURITY_KEY');
        $url1 = 'http://www.' . $_SERVER['HTTP_HOST'] . '/' . PATH_APPLICATION . '?key=' . $this->config->get('SECURITY_KEY');
        if (strpos($_SERVER['HTTP_HOST'], 'www') === false)
            throw new GException('Warning', sprintf($this->_['msg_warning_url'], $url1, $url));

        $mode = 'view';
        if (isset($_COOKIE['cms-mode']))
            $mode = $_COOKIE['cms-mode'];
        // панель (ExtJS class "Manager.site.Panel")
        $this->_cmp = new Ext_Panel(
            array('id'         => strtolower(__CLASS__),
                  'xtype'      => 'mn-site-panel',
                  'closable'   => true,
                  'component'  => array('container' => 'content-tab', 'destroy' => true),
                  'token'      => $this->config->getFromCms('Site', 'CMS/TOKEN'),
                  'frameId'    => 'site',
                  'infoId'     => 'stFrameUrl',
                  'designMode' => $mode == 'design',
                  'url'        => 'http://' . $_SERVER['HTTP_HOST']
            )
        );
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // панель (ExtJS class "Ext.Panel")
        $this->_cmp->iconSrc = $this->resourcePath . 'icon.png';
        $this->_cmp->title = $this->_['title'];
        $this->_cmp->tabTip = $this->_['title'];
        $this->_cmp->iconTpl = $this->resourcePath . 'shortcut.png';
        $this->_cmp->titleTpl = $this->_['tooltip'];

        parent::getInterface();
    }
}
?>