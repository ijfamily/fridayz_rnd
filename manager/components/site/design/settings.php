<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Конструктор страниц"
 * Группа пакетов     "Конструктор страниц"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SDesign
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
* Установка компонента
 * 
 * Название: Конструктор страницы
 * Описание: Конструктор страницы
 * Меню:
 * ID класса: gcontroller_sdesign_view
 * Модуль: Сайт
 * Группа: Сайт
 * Очищать: нет
 * Ресурс
 *    Компонент: site/design/
 *    Контроллер: site/design/View/
 *    Интерфейс: view/interface/
 *    Меню:
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске компонентов: нет
 *    В главном меню: нет
 */

return array(
    // {S}- модуль "Site" {} - пакет контроллеров "Design" -> SDesign
    'clsPrefix' => 'SDesign',
    // использовать язык
    'language'  => 'ru-RU'
);
?>