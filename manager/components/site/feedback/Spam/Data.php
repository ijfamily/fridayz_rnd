<?php
/**
 * Gear Manager
 *
 * Контроллер         "Отправка письма в спам"
 * Пакет контроллеров "Обратная связь"
 * Группа пакетов     "Обратная связь"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SFeedback_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Data');

/**
 * Отправка письма в спам
 * 
 * @category   Gear
 * @package    GController_SFeedback_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SFeedback_Spam_Data extends GController_Data
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sfeedback_grid';

    /**
     * Отправка записей в СПАМ
     * 
     * @return void
     */
    protected function toSpam()
    {
        $this->dataAccessInsert();

        $this->response->set('action', 'update');
        $this->response->setMsgResult('Updating data', $this->_['msg_success'], true);

        if (empty($this->uri->id))
            throw new GException('Error', 'Selected record was deleted!');
        $query = new GDb_Query();
        // добавление в спам
        $sql = 'INSERT INTO `site_spam` (`spam_ipaddress`, `spam_date`, `spam_mask`) '
             . 'SELECT `feedback_ip_int`, "' . date('Y-m-d H:i:s') . '", 4294967295  FROM `site_feedback` '
             . 'WHERE `feedback_id` IN (' . $this->uri->id . ') AND `feedback_ip_int` NOT IN (SELECT `spam_ipaddress` FROM `site_spam`)';
        if (($res = $query->execute($sql)) === false)
            throw new GSqlException();
    }

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        if ($this->uri->action == 'data')
            // метод запроса
            switch ($this->uri->method) {
                // метод "GET"
                case 'GET': $this->toSpam(); return;
        }

        parent::init();
    }
}
?>