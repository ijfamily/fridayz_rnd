<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск записей в обратной связи"
 * Пакет контроллеров "Обратная связь"
 * Группа пакетов     "Обратная связь"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SFeedback_Search
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск записей в обратной связи
 * 
 * @category   Gear
 * @package    GController_SFeedback_Search
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SFeedback_Search_Data extends GController_Search_Data
{
    /**
     * дентификатор компонента (списка) к которому применяется поиск
     *
     * @var string
     */
    public $gridId = 'gcontroller_sfeedback_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sfeedback_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор
     * Используется для инициализации атрибутов контроллер не переданного в конструктор
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'feedback_name', 'label' => $this->_['header_feedback_name']),
            array('dataIndex' => 'feedback_email', 'label' => $this->_['header_feedback_email']),
            array('dataIndex' => 'feedback_phone', 'label' => $this->_['header_feedback_phone']),
            array('dataIndex' => 'feedback_text', 'label' => $this->_['header_feedback_text']),
            array('dataIndex' => 'feedback_ip', 'label' => $this->_['header_feedback_ip']),
            array('dataIndex' => 'feedback_os', 'label' => $this->_['header_feedback_os']),
            array('dataIndex' => 'feedback_browser', 'label' => $this->_['header_feedback_browser']),
            array('dataIndex' => 'feedback_url', 'label' => $this->_['header_feedback_url']),
            array('dataIndex' => 'feedback_form', 'label' => $this->_['header_feedback_form'])
        );
    }

    /**
     * Применение фильтра
     * 
     * @return void
     */
    protected function dataAccept()
    {
        if ($this->_sender == 'toolbar') {
            if ($this->input->get('action') == 'search') {
                $tlbFilter = '';
                // если попытка сбросить фильтр
                if (!$this->input->isEmptyPost(array('domain'))) {
                    $tlbFilter = array(
                        'domain' => $this->input->get('domain')
                    );
                }
                $this->store->set('tlbFilter', $tlbFilter);
                return;
            }
        }
    }
}
?>