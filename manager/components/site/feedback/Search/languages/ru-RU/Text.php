<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Поиск в списке "Обратная связь"',
    // столбцы
    'header_feedback_name'    => 'Имя',
    'header_feedback_email'   => 'E-mail',
    'header_feedback_phone'   => 'Телефон',
    'header_feedback_text'    => 'Текст',
    'header_feedback_form'    => 'Форма',
    'header_feedback_url'     => 'URL адрес',
    'header_feedback_ip'      => 'IP адрес',
    'header_feedback_os'      => 'ОС',
    'header_feedback_browser' => 'Браузер',
    'header_feedback_date'    => 'Дата'
);
?>