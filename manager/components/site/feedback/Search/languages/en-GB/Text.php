<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'search_title' => 'Search in the "Feedback"',
    // поля
    'header_feedback_name'    => 'Name',
    'header_feedback_email'   => 'E-mail',
    'header_feedback_phone'   => 'Phone',
    'header_feedback_text'    => 'Text',
    'header_feedback_form'    => 'Form',
    'header_feedback_url'     => 'URL address',
    'header_feedback_ip'      => 'IP address',
    'header_feedback_os'      => 'ОS',
    'header_feedback_browser' => 'Browser',
    'header_feedback_date'    => 'Date'
);
?>