<?php
/**
 * Gear Manager
 *
 * Контроллер         "Развёрнутая запись обратной связи"
 * Пакет контроллеров "Обратная связь"
 * Группа пакетов     "Обратная связь"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SFeedback_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Expand');

/**
 * Развёрнутая запись бота
 * 
 * @category   Gear
 * @package    GController_SFeedback_Grid
 * @subpackage Expand
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SFeedback_Grid_Expand extends GController_Grid_Expand
{
    /**
     * Вывод данных в интерфейс
     * 
     * @return void
     */
    protected function dataExpand()
    {
        parent::dataAccessView();

        $this->response->data = $this->getAttributes();
    }

    /**
     * Возращает атрибуты записи
     * 
     * @return string
     */
    protected function getAttributes()
    {
        $data = '';
        $settings = $this->session->get('user/settings');
        $query = new GDb_Query();
        // обратная связь
        $sql = 'SELECT * FROM `site_feedback` WHERE `feedback_id`=' . $this->uri->id;
        if (($fb = $query->getRecord($sql)) === false)
            throw new GSqlException();
        // атрибуты
        $data = '<div><fieldset class="fixed"><label>' . $this->_['title_fieldset_attr'] . '</label><ul>';
        if (!empty($fb['feedback_date']))
            $data .= '<li><label>' . $this->_['header_feedback_date'] . ':</label> ' . date($settings['format/datetime'], strtotime($fb['feedback_date'])) . '</li>';
        $data .= '<li><label>' . $this->_['header_feedback_ip'] . ':</label> ' . $fb['feedback_ip'] . '</li>';
        $data .= '<li><label>' . $this->_['header_feedback_browser'] . ':</label> ' . $fb['feedback_browser'] . '</li>';
        $data .= '<li><label>' . $this->_['header_feedback_os'] . ':</label> ' . $fb['feedback_os'] . '</li>';
        $data .= '<li><label>' . $this->_['header_feedback_form'] . ':</label> ' . $fb['feedback_form'] . '</li>';
        $data .= '<li><label>' . $this->_['header_feedback_url'] . ':</label> http://' . $_SERVER['HTTP_HOST'] . $fb['feedback_url'] . '</li>';
        $data .= '</ul></fieldset>';
        // пользователь
        $data .= '<div><fieldset class="fixed"><label>' . $this->_['title_fieldset_user'] . '</label><ul>';
        $data .= '<li><label>' . $this->_['header_feedback_name'] . ':</label> ' . $fb['feedback_name'] . '</li>';
        $data .= '<li><label>' . $this->_['header_feedback_email'] . ':</label> ' . $fb['feedback_email'] . '</li>';
        $data .= '<li><label>' . $this->_['header_feedback_phone'] . ':</label> ' . $fb['feedback_phone'] . '</li>';
        $data .= '</ul></fieldset><div class="wrap"></div>';
        // текст
        $items = explode('**', $fb['feedback_text']);
        for ($i = 0; $i < sizeof($items); $i++) {
            $tab = explode('::', $items[$i]);
            if (sizeof($tab) == 1) {
                $title = $this->_['title_fieldset_text'];
                $text = $tab[0];
            } else
            if (sizeof($tab) > 1) {
                $title = $tab[0];
                $text = $tab[1];
            }
            // текст
            $data .= '<div><fieldset class="fixed" style="width:100%"><label>' . $title . '</label><ul>';
            $data .= '<li>' . $text . '</li>';
            $data .= '</ul></fieldset><div class="wrap"></div>';
        }

        return '<div class="mn-row-body border">' . $data . '<div class="wrap"></div></div>';
    }
}
?>