<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс обратной связи"
 * Пакет контроллеров "Обратная связь"
 * Группа пакетов     "Обратная связь"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SFeedback_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс обратной связи
 * 
 * @category   Gear
 * @package    GController_SFeedback_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SFeedback_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'feedback_id';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'feedback_date';

    /**
     * Использовать быстрый фильтр
     *
     * @var boolean
     */
    public $useSlidePanel = true;

    /**
     * Возращает список доменов
     * 
     * @return array
     */
    protected function getDomains()
    {
        $arr = $this->config->getFromCms('Domains');
        $list = array();
        if ($arr) {
            $list[] = array('Все', -1);
            $list[] = array($this->config->getFromCms('Site', 'NAME', ''), 0);
            $domains = $arr['domains'];
            foreach ($arr['indexes'] as $key => $value) {
                $list[] = array($value[1], $key);
            } 
        }

        return $list;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // название сайта
            array('name' => 'domain_id', 'type' => 'string'),
            // имя (ФИО)
            array('name' => 'feedback_name', 'type' => 'string'),
            // Email
            array('name' => 'feedback_email', 'type' => 'string'),
            // телефон
            array('name' => 'feedback_phone', 'type' => 'string'),
            // текст
            array('name' => 'feedback_text', 'type' => 'string'),
            // дата
            array('name' => 'feedback_date', 'type' => 'string'),
            // прочитано
            array('name' => 'feedback_read', 'type' => 'string'),
            // ip адрес
            array('name' => 'feedback_ip', 'type' => 'string'),
            // url адрес
            array('name' => 'feedback_url', 'type' => 'string'),
            // браузер
            array('name' => 'feedback_browser', 'type' => 'string'),
            // ос
            array('name' => 'feedback_os', 'type' => 'string'),
            // форма
            array('name' => 'feedback_form', 'type' => 'string')
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $settings = $this->session->get('user/settings');
        $this->columns = array(
            array('xtype'     => 'expandercolumn',
                  'url'       => $this->componentUrl . 'grid/expand/',
                  'typeCt'    => 'row'),
            array('xtype'     => 'numbercolumn'),
            array('xtype'     => 'rowmenu'),
            array('dataIndex' => 'domain_id',
                  'header'    => $this->_['header_site_name'],
                  'width'     => 90,
                  'hidden'    => !$this->config->getFromCms('Site', 'OUTCONTROL'),
                  'sortable'  => true),
            array('xtype'     => 'datetimecolumn',
                  'frmDate'   => $settings['format/date'],
                  'frmTime'   => $settings['format/time'],
                  'dataIndex'     => 'feedback_date',
                  'dateTimeIndex' => 'feedback_date',
                  'header'    => '<em class="mn-grid-hd-details"></em>'
                               . $this->_['header_feedback_date'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'date')),
            array('dataIndex' => 'feedback_name',
                  'header'    => $this->_['header_feedback_name'],
                  'width'     => 180,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'feedback_email',
                  'header'    => $this->_['header_feedback_email'],
                  'width'     => 150,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'feedback_phone',
                  'header'    => $this->_['header_feedback_phone'],
                  'width'     => 150,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'feedback_text',
                  'header'    => $this->_['header_feedback_text'],
                  'width'     => 140,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'feedback_ip',
                  'hidden'    => true,
                  'header'    => $this->_['header_feedback_ip'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'feedback_os',
                  'hidden'    => true,
                  'header'    => $this->_['header_feedback_os'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'feedback_browser',
                  'hidden'    => true,
                  'header'    => $this->_['header_feedback_browser'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'feedback_url',
                  'hidden'    => true,
                  'header'    => $this->_['header_feedback_url'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'feedback_form',
                  'header'    => $this->_['header_feedback_form'],
                  'width'     => 130,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string'))
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_grid'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_grid'],
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'profileUrl'    => $this->componentUrl . 'profile/')
        );

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "edit" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' => $this->classId));
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'      => 'mn-btn-clear-data',
                  'msgConfirm' => $this->_['msg_btn_clear'],
                  'gridId'     => $this->classId,
                  'isN'        => true)
        );
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "в спам" (ExtJS class "Manager.button.widgetGrid")
        $group->items->add(
            array('xtype'      => 'mn-btn-widget-grid',
                  'width'      => 60,
                  'selOneRecord' => false,
                  'gridId'     => $this->classId,
                  'text'       => $this->_['text_btn_spam'],
                  'msgWarning' => $this->_['msg_select_record'],
                  'tooltip'    => $this->_['tooltip_btn_spam'],
                  'icon'       => $this->resourcePath . 'icon-btn-spam.png',
                  'url'        => $this->componentUri . ROUTER_DELIMITER . 'spam/data/')
        );
        // кнопка "в рассылку" (ExtJS class "Manager.button.widgetGrid")
        $group->items->add(
            array('xtype'      => 'mn-btn-widget-grid',
                  'width'      => 60,
                  'selOneRecord' => false,
                  'gridId'     => $this->classId,
                  'text'       => $this->_['text_btn_mail'],
                  'msgWarning' => $this->_['msg_select_record'],
                  'tooltip'    => $this->_['tooltip_btn_mail'],
                  'icon'       => $this->resourcePath . 'icon-btn-mail.png',
                  'url'        => $this->componentUri . ROUTER_DELIMITER . 'mail/data/')
        );
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = 'component-site-feedback';
        $this->_cmp->tbar->items->add($group);

        // управление сайтами на других доменах
        if ($this->config->getFromCms('Site', 'OUTCONTROL')) {
            $domains = $this->getDomains();
            if ($domains) {
                // группа кнопок "фильтр" (ExtJS class "Ext.ButtonGroup")
                $group = new Ext_ButtonGroup_Filter(
                    array('title'  => $this->_['title_buttongroup_filter'],
                          'gridId' => $this->classId)
                );
                // кнопка "справка" (ExtJS class "Manager.button.Help")
                $btn = &$group->items->get(0);
                $btn['gridId'] = $this->classId;
                $btn['url'] = $this->componentUrl . 'search/data/';
                // кнопка "поиск" (ExtJS class "Manager.button.Search")
                $filter = $this->store->get('tlbFilter', array());
                $group->items->add(array(
                    'xtype'       => 'form',
                    'labelWidth'  => 35,
                    'bodyStyle'   => 'border:1px solid transparent;background-color:transparent',
                    'frame'       => false,
                    'bodyBorder ' => false,
                    'items'       => array(
                        array('xtype'         => 'combo',
                              'fieldLabel'    => $this->_['label_domain'],
                              'name'          => 'domain',
                              'checkDirty'    => false,
                              'editable'      => false,
                              'width'         => 140,
                              'typeAhead'     => false,
                              'triggerAction' => 'all',
                              'mode'          => 'local',
                              'store'         => array(
                                  'xtype'  => 'arraystore',
                                  'fields' => array('list', 'value'),
                                  'data'   => $domains
                              ),
                              'value'         => empty($filter) ? '' : $filter['domain'],
                              'hiddenName'    => 'domain',
                              'valueField'    => 'value',
                              'displayField'  => 'list',
                              'allowBlank'    => false)
                    )
                ));
                $this->_cmp->tbar->items->add($group);
            }
        }

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $cellInfo =
            '<div class="mn-grid-cell-tooltip-tl">{feedback_date}</div>'
          . '<div class="mn-grid-cell-tooltip">'
          . '<em>' . $this->_['header_feedback_name'] . '</em>: <b>{feedback_name}</b><br>'
          . '<em>' . $this->_['header_feedback_email'] . '</em>: <b>{feedback_email}</b><br>'
          . '<em>' . $this->_['header_feedback_phone'] . '</em>: <b>{feedback_phone}</b><br>'
          . '<em>' . $this->_['header_feedback_ip'] . '</em>: <b>{feedback_ip}</b><br>'
          . '<em>' . $this->_['header_feedback_os'] . '</em>: <b>{feedback_os}</b><br>'
          . '<em>' . $this->_['header_feedback_browser'] . '</em>: <b>{feedback_browser}</b><br>'
          . '<em>' . $this->_['header_feedback_form'] . '</em>: <b>{feedback_form}</b><br>'
          . '<em>' . $this->_['header_feedback_url'] . '</em>: <b>{feedback_url}</b>'
          . '</div>';
        $this->_cmp->cellTips = array(
            array('field' => 'feedback_date', 'tpl' => $cellInfo),
            array('field' => 'feedback_name', 'tpl' => '{feedback_name}'),
            array('field' => 'feedback_email', 'tpl' => '{feedback_email}'),
            array('field' => 'feedback_phone', 'tpl' => '{feedback_phone}'),
            array('field' => 'feedback_browser', 'tpl' => '{feedback_browser}'),
            array('field' => 'feedback_form', 'tpl' => '{feedback_form}'),
            array('field' => 'feedback_url', 'tpl' => '{feedback_url}')
        );

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $items = array(
            array('text' => $this->_['rowmenu_info'],
                  'icon' => $this->resourcePath . 'icon-item-info.png',
                  'url'  => $this->componentUrl . 'info/interface/')
        );
        $this->_cmp->rowMenu = array('xtype' => 'mn-grid-rowmenu', 'items' => $items);

        // быстрый фильтр списка (ExtJs class "Manager.tree.GridFilter")
        $this->_slidePanel->cls = 'mn-tree-gridfilter';
        $this->_slidePanel->width = 250;
        $this->_slidePanel->initRoot = array(
            'text'     => 'Filter',
            'id'       => 'byRoot',
            'expanded' => true,
            'children' => array(
                array('text'     => $this->_['text_all_records'],
                      'value'    => 'all',
                      'expanded' => true,
                      'leaf'     => false,
                      'children' => array(
                        array('text'     => $this->_['text_by_date'],
                              'id'       => 'byDt',
                              'leaf'     => false,
                              'expanded' => true,
                              'children' => array(
                                  array('text'     => $this->_['text_by_day'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 'day'),
                                  array('text'     => $this->_['text_by_week'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true, 
                                        'value'    => 'week'),
                                  array('text'     => $this->_['text_by_month'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 'month')
                              )
                        ),
                        array('text'    => $this->_['text_by_name'],
                              'id'      => 'byNa',
                              'leaf'    => false),
                        array('text'    => $this->_['text_by_email'],
                              'id'      => 'byEm',
                              'leaf'    => false),
                        array('text'    => $this->_['text_by_ip'],
                              'id'      => 'byIp',
                              'leaf'    => false),
                        array('text'    => $this->_['text_by_os'],
                              'id'      => 'byOs',
                              'leaf'    => false),
                        array('text'    => $this->_['text_by_browser'],
                              'id'      => 'byBr',
                              'leaf'    => false),
                        array('text'    => $this->_['text_by_form'],
                              'id'      => 'byFo',
                              'leaf'    => false)
                    )
                )
            )
        );

        parent::getInterface();
    }
}
?>