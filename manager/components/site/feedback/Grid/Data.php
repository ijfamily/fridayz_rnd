<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка обратной связи"
 * Пакет контроллеров "Обратная связь"
 * Группа пакетов     "Обратная связь"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SFeedback_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные списка обратной связи
 * 
 * @category   Gear
 * @package    GController_SFeedback_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SFeedback_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'feedback_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_feedback';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'feedback_date';

    /**
     * Домены
     *
     * @var array
     */
    public $domains = array();

    /**
     * Управления сайтами на других доменах
     *
     * @var boolean
     */
    public $isOutControl = false;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // управление сайтами на других доменах
        $this->isOutControl = $this->config->getFromCms('Site', 'OUTCONTROL');
        if ($this->isOutControl)
            $this->domains = $this->config->getFromCms('Domains', 'indexes');
        $this->domains[0] = array($_SERVER['SERVER_NAME'], $this->config->getFromCms('Site', 'NAME'));

        // SQL запрос
        $this->sql =  'SELECT SQL_CALC_FOUND_ROWS * FROM `site_feedback` WHERE 1 ';
        // фильтр из панели инструментов
        if ($this->isOutControl) {
            $filter = $this->store->get('tlbFilter', false);
            if ($filter) {
                $domainId = (int) $filter['domain'];
                if ($domainId >= 0)
                    $this->sql .= ' AND `domain_id`=' . $domainId;
            }
        }
        $this->sql .= '%filter %fastfilter ORDER BY %sort LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // название сайта
            'domain_id' => array('type' => 'string'),
            // имя (ФИО)
            'feedback_name' => array('type' => 'string'),
            // Email
            'feedback_email' => array('type' => 'string'),
            // телефон
            'feedback_phone' => array('type' => 'string'),
            // текст
            'feedback_text' => array('type' => 'string'),
            // дата
            'feedback_date' => array('type' => 'string'),
            // прочитано
            'feedback_read' => array('type' => 'string'),
            // ip адрес
            'feedback_ip' => array('type' => 'string'),
            // url адрес
            'feedback_url' => array('type' => 'string'),
            // браузер
            'feedback_browser' => array('type' => 'string'),
            // ос
            'feedback_os' => array('type' => 'string'),
            // форма
            'feedback_form' => array('type' => 'string')
        );
    }

    /**
     * Возращает sql запрос для быстрого фильтра
     * (для подстановки "%fastfilter" в $sql)
     * 
     * @param string $key идендификатор поля
     * @param string $value значение
     * @return string
     */
    protected function getTreeFilter($key, $value)
    {
        // идендификатор поля
        switch ($key) {
            // по дате правки
            case 'byDt':
                $toDate = date('Y-m-d');
                switch ($value) {
                    case 'day':
                        return ' AND `feedback_date` BETWEEN "' . $toDate . '" AND "' . $toDate . '"';

                    case 'week':
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') - 7, date('Y')));
                        return ' AND `feedback_date` BETWEEN "' . $fromDate . '" AND "' . $toDate . '"';

                    case 'month':
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, date('m') - 1, date('d'), date('Y')));
                        return ' AND `feedback_date` BETWEEN "' . $fromDate . '" AND "' . $toDate . '"';
                }
                break;

            // по ip адресу
            case 'byIp': return ' AND `feedback_ip`' . ($value == 'none' ? ' IS NULL ' : "='$value' ");

            // по браузеру
            case 'byBr': return ' AND `feedback_browser`' . ($value == 'none' ? ' IS NULL ' : "='$value' ");

            // по e-mail
            case 'byEm': return ' AND `feedback_email`' . ($value == 'none' ? ' IS NULL ' : "='$value' ");

            // по форме
            case 'byFo': return ' AND `feedback_form`' . ($value == 'none' ? ' IS NULL ' : "='$value' ");

            // по ос
            case 'byOs': return ' AND `feedback_os`' . ($value == 'none' ? ' IS NULL ' : "='$value' ");

            // по имени
            case 'byNa': return ' AND `feedback_name`' . ($value == 'none' ? ' IS NULL ' : "='$value' ");
        }

        return '';
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        $query = new GDb_Query();
        // удаление записей таблицы "site_feedback" (обратная связь)
        if ($query->clear('site_feedback') === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_feedback') === false)
            throw new GSqlException();
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        $domain = $_SERVER['SERVER_NAME'];
        // сайты на других доменах
        if (isset($this->domains[$record['domain_id']])) {
            $domain = $this->domains[$record['domain_id']][0];
            $record['domain_id'] = $this->domains[$record['domain_id']][1];
        } else
            $record['domain_id'] = '';

        if (mb_strlen($record['feedback_text'],'UTF-8') > 100)
            $record['feedback_text'] = mb_substr($record['feedback_text'], 0, 40, 'UTF-8') . ' ...';

        return $record;
    }
}
?>