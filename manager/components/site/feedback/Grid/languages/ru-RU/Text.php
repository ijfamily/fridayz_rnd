<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Обратная связь',
    'tooltip_grid' => 'сообщения пользователей из форм на страницах сайта',
    'rowmenu_info' => 'Информация о записи',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">("Обратная связь")</span> ?',
    'text_btn_spam'            => 'В СПАМ',
    'tooltip_btn_spam'         => 'Отправить письмо в СПАМ',
    'msg_select_record'        => 'Вам необходимо выбрать одну и более записей!',
    'text_btn_mail'            => 'В рассылку',
    'tooltip_btn_mail'         => 'Отправить e-mail пользователя в рассылку',
    'label_domain'             => 'Сайт',
    // столбцы
    'header_site_name'        => 'Сайт',
    'header_feedback_name'    => 'Имя',
    'header_feedback_email'   => 'E-mail',
    'header_feedback_phone'   => 'Телефон',
    'header_feedback_text'    => 'Текст',
    'header_feedback_form'    => 'Форма',
    'header_feedback_url'     => 'URL адрес',
    'header_feedback_ip'      => 'IP адрес',
    'header_feedback_os'      => 'ОС',
    'header_feedback_browser' => 'Браузер',
    'header_feedback_date'    => 'Дата',
    // развернутая запись
    'title_fieldset_attr' => 'Атрибуты',
    'title_fieldset_user' => 'Пользователь',
    'title_fieldset_text' => 'Текст',
    // быстрый фильтр
    'text_all_records' => 'Все сообщения',
    'text_by_date'     => 'По дате сообщения',
    'text_by_day'      => 'За день',
    'text_by_week'     => 'За неделю',
    'text_by_month'    => 'За месяц',
    'text_by_name'     => 'По имени',
    'text_by_email'    => 'По e-mail',
    'text_by_ip'       => 'По IP адресу',
    'text_by_os'       => 'По ОС',
    'text_by_browser'  => 'По браузеру',
    'text_by_form'     => 'По форме',
    
);
?>