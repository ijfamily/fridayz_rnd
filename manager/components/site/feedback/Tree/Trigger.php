<?php
/**
 * Gear Manager
 *
 * Контроллер         "Триггер дерева"
 * Пакет контроллеров "Боты"
 * Группа пакетов     "Сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SFeedback_Tree
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Tree/Trigger');

/**
 * Триггер дерева
 * 
 * @category   Gear
 * @package    GController_SFeedback_Tree
 * @subpackage Trigger
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SFeedback_Tree_Trigger extends GController_Tree_Trigger
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sfeedback_grid';

    /**
     * Возращает список ip адресов
     *
     * @return array
     */
    protected function getByIp()
    {
        $data = array();
        $query = new GDb_Query();
        $sql = 'SELECT *, COUNT(*) `count` FROM `site_feedback` GROUP BY `feedback_ip`';
        if ($query->execute($sql) !== true)
            throw new GSqlException();
        while (!$query->eof()) {
            $rec = $query->next();
            $value = 'none';
            if (empty($rec['feedback_ip']))
                $rec['feedback_ip'] = $this->_['data_unknow'];
            else
                $value = $rec['feedback_ip'];
            $data[] = array(
                'text'    => $rec['feedback_ip'] . ' <b>(' . $rec['count'] . ')</b>',
                'qtip'    => $rec['feedback_ip'],
                'leaf'    => true,
                'iconCls' => 'icon-folder-find',
                'value'   => $value
            );
        }

        return $data;
    }

    /**
     * Возращает список по браузеру
     *
     * @return array
     */
    protected function getByBrowser()
    {
        $data = array();
        $query = new GDb_Query();
        $sql = 'SELECT *, COUNT(*) `count` FROM `site_feedback` GROUP BY `feedback_browser`';
        if ($query->execute($sql) !== true)
            throw new GSqlException();
        while (!$query->eof()) {
            $rec = $query->next();
            $value = 'none';
            if (empty($rec['feedback_browser']))
                $rec['feedback_browser'] = $this->_['data_unknow'];
            else
                $value = $rec['feedback_browser'];
            $data[] = array(
                'text'    => $rec['feedback_browser'] . ' <b>(' . $rec['count'] . ')</b>',
                'qtip'    => $rec['feedback_browser'],
                'leaf'    => true,
                'iconCls' => 'icon-folder-find',
                'value'   => $value
            );
        }

        return $data;
    }

    /**
     * Возращает список по ос
     *
     * @return array
     */
    protected function getByOs()
    {
        $data = array();
        $query = new GDb_Query();
        $sql = 'SELECT *, COUNT(*) `count` FROM `site_feedback` GROUP BY `feedback_os`';
        if ($query->execute($sql) !== true)
            throw new GSqlException();
        while (!$query->eof()) {
            $rec = $query->next();
            $value = 'none';
            if (empty($rec['feedback_os']))
                $rec['feedback_os'] = $this->_['data_unknow'];
            else
                $value = $rec['feedback_os'];
            $data[] = array(
                'text'    => $rec['feedback_os'] . ' <b>(' . $rec['count'] . ')</b>',
                'qtip'    => $rec['feedback_os'],
                'leaf'    => true,
                'iconCls' => 'icon-folder-find',
                'value'   => $value
            );
        }

        return $data;
    }

    /**
     * Возращает список по форме
     *
     * @return array
     */
    protected function getByForm()
    {
        $data = array();
        $query = new GDb_Query();
        $sql = 'SELECT *, COUNT(*) `count` FROM `site_feedback` GROUP BY `feedback_form`';
        if ($query->execute($sql) !== true)
            throw new GSqlException();
        while (!$query->eof()) {
            $rec = $query->next();
            $value = 'none';
            if (empty($rec['feedback_form']))
                $rec['feedback_form'] = $this->_['data_unknow'];
            else
                $value = $rec['feedback_form'];
            $data[] = array(
                'text'    => $rec['feedback_form'] . ' <b>(' . $rec['count'] . ')</b>',
                'qtip'    => $rec['feedback_form'],
                'leaf'    => true,
                'iconCls' => 'icon-folder-find',
                'value'   => $value
            );
        }

        return $data;
    }

    /**
     * Возращает список по e-mail
     *
     * @return array
     */
    protected function getByEmail()
    {
        $data = array();
        $query = new GDb_Query();
        $sql = 'SELECT *, COUNT(*) `count` FROM `site_feedback` GROUP BY `feedback_email`';
        if ($query->execute($sql) !== true)
            throw new GSqlException();
        while (!$query->eof()) {
            $rec = $query->next();
            $value = 'none';
            if (empty($rec['feedback_email']))
                $rec['feedback_email'] = $this->_['data_unknow'];
            else
                $value = $rec['feedback_email'];
            $data[] = array(
                'text'    => $rec['feedback_email'] . ' <b>(' . $rec['count'] . ')</b>',
                'qtip'    => $rec['feedback_email'],
                'leaf'    => true,
                'iconCls' => 'icon-folder-find',
                'value'   => $value
            );
        }

        return $data;
    }

    /**
     * Возращает список по имени
     *
     * @return array
     */
    protected function getByName()
    {
        $data = array();
        $query = new GDb_Query();
        $sql = 'SELECT *, COUNT(*) `count` FROM `site_feedback` GROUP BY `feedback_name`';
        if ($query->execute($sql) !== true)
            throw new GSqlException();
        while (!$query->eof()) {
            $rec = $query->next();
            $value = 'none';
            if (empty($rec['feedback_name']))
                $rec['feedback_name'] = $this->_['data_unknow'];
            else
                $value = $rec['feedback_name'];
            $data[] = array(
                'text'    => $rec['feedback_name'] . ' <b>(' . $rec['count'] . ')</b>',
                'qtip'    => $rec['feedback_name'],
                'leaf'    => true,
                'iconCls' => 'icon-folder-find',
                'value'   => $value
            );
        }

        return $data;
    }

    /**
     * Вывод данных в интерфейс компонента
     * 
     * @param  string $name название триггера
     * @param  string $node id выбранного узла
     * @return void
     */
    protected function dataView($name, $node)
    {
        parent::dataView($name, $node);

        // триггер
        switch ($name) {
            // быстрый фильтр списка
            case 'gridFilter':
                // id выбранного узла
                switch ($node) {
                    // по ip адресу
                    case 'byIp': $this->response->data = $this->getByIp(); break;

                    // по браузеру
                    case 'byBr': $this->response->data = $this->getByBrowser(); break;

                    // по e-mail
                    case 'byEm': $this->response->data = $this->getByEmail(); break;

                    // по форме
                    case 'byFo': $this->response->data = $this->getByForm(); break;

                    // по ос
                    case 'byOs': $this->response->data = $this->getByOs(); break;

                    // по имени
                    case 'byNa': $this->response->data = $this->getByName(); break;
                }
        }
    }
}
?>