<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля обратной связи"
 * Пакет контроллеров "Профиль обратной связи"
 * Группа пакетов     "Обратная связь"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SFeedback_Info
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля обратной связи
 * 
 * @category   Gear
 * @package    GController_SFeedback_Info
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SFeedback_Info_Interface extends GController_Profile_Interface
{
    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        $data = array('tabs' => array());

        // если состояние формы "вставка"
        if ($this->isInsert) return $data;

        parent::getDataInterface();

        $query = new GDb_Query();
        // сообщения
        $sql = 'SELECT * FROM `site_feedback` WHERE `feedback_id`=' . (int) $this->uri->id;
        if (($record = $query->getRecord($sql)) === false)
            throw new GSqlException();
        // текст
        $items = explode('**', $record['feedback_text']);
        $tabs = array();
        for ($i = 0; $i < sizeof($items); $i++) {
            $tab = explode('::', $items[$i]);
            if (sizeof($tab) == 1) {
                $title = $this->_['title_tab_text'];
                $text = $tab[0];
            } else
            if (sizeof($tab) > 1) {
                $title = $tab[0];
                $text = $tab[1];
            }
            $tabs[] = array(
                'title'   => $title,
                'iconSrc' => $this->resourcePath . 'icon-tab-text.png',
                'layout'  => 'anchor',
                'baseCls' => 'mn-form-tab-body',
                'style'   => 'padding:1px',
                'autoScroll'  => true,
                'items'       => array(
                    array('xtype'      => 'textarea',
                          'name'       => 'feedback_text' . $i,
                          'readOnly'   => true,
                          'anchor'     => '100% 100%',
                          'value'      => $text,
                          'allowBlank' => true)
                )
            );
        }

        return array('tabs' => $tabs);
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('titleEllipsis' => 70,
                  'gridId'        => 'gcontroller_sfeedback_grid',
                  'width'         => 600,
                  'autoHeight'    => true,
                  'state'         => 'info',
                  'stateful'      => false)
        );

        // поля формы (ExtJS class "Ext.Panel")
        $tabAttrItems = array(
            array('xtype'      => 'fieldset',
                  'labelWidth' => 75,
                  'title'      => $this->_['title_fieldset_msg'],
                  'autoHeight' => true,
                  'defaultType' => 'displayfield',
                  'items'      => array(
                    array('fieldClass' => 'mn-field-value-info',
                          'fieldLabel' => $this->_['label_feedback_date'],
                          'name'       => 'feedback_date',
                          'width'      => 130,
                          'readOnly'   => true,
                          'allowBlank' => false),
                    array('fieldClass' => 'mn-field-value-info',
                          'fieldLabel' => $this->_['label_feedback_url'],
                          'name'       => 'feedback_url',
                          'anchor'     => '100%',
                          'readOnly'   => true,
                          'allowBlank' => false),
                    array('fieldClass' => 'mn-field-value-info',
                          'fieldLabel' => $this->_['label_feedback_browser'],
                          'name'       => 'feedback_browser',
                          'anchor'     => '100%',
                          'readOnly'   => true,
                          'allowBlank' => false),
                    array('fieldClass' => 'mn-field-value-info',
                          'fieldLabel' => $this->_['label_feedback_os'],
                          'name'       => 'feedback_os',
                          'anchor'     => '100%',
                          'readOnly'   => true,
                          'allowBlank' => false),
                    array('fieldClass' => 'mn-field-value-info',
                          'fieldLabel' => $this->_['label_feedback_form'],
                          'name'       => 'feedback_form',
                          'anchor'     => '100%',
                          'readOnly'   => true,
                          'allowBlank' => false)
                  )
            ),
            array('xtype'      => 'fieldset',
                  'labelWidth' => 75,
                  'title'      => $this->_['title_fieldset_user'],
                  'autoHeight' => true,
                  'defaultType' => 'displayfield',
                  'items'      => array(
                    array('fieldClass' => 'mn-field-value-info',
                          'fieldLabel' => $this->_['label_feedback_name'],
                          'name'       => 'feedback_name',
                          'anchor'     => '100%',
                          'readOnly'   => true,
                          'allowBlank' => false),
                    array('fieldClass' => 'mn-field-value-info',
                          'fieldLabel' => $this->_['label_feedback_email'],
                          'name'       => 'feedback_email',
                          'anchor'     => '100%',
                          'readOnly'   => true,
                          'allowBlank' => false),
                    array('fieldClass' => 'mn-field-value-info',
                          'fieldLabel' => $this->_['label_feedback_phone'],
                          'name'       => 'feedback_phone',
                          'anchor'     => '100%',
                          'readOnly'   => true,
                          'allowBlank' => false)
                  )
            )
        );
        // владка "Атрибуты"
        $tabAttr = array(
            'title'       => $this->_['title_tab_attr'],
            'iconSrc'     => $this->resourcePath . 'icon-tab-attr.png',
            'layout'      => 'form',
            'baseCls'     => 'mn-form-tab-body',
            'defaultType' => 'displayfield',
            'autoScroll'  => true,
            'items'       => array($tabAttrItems)
        );

        // вкладки окна
        $tabs = array(
            array('xtype'             => 'tabpanel',
                  'layoutOnTabChange' => true,
                  'activeTab'         => 0,
                  'style'             => 'padding:3px',
                  'anchor'            => '100%',
                  'height'            => 350,
                  'items'             => array($tabAttr))
        );
        $tabs[0]['items'] = array_merge($tabs[0]['items'], $data['tabs']);

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($tabs);
        $form->url = $this->componentUrl . 'info/';

        parent::getInterface();
    }
}
?>