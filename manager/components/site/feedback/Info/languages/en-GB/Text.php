<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'info_title' => 'Details "%s"',
    // поля
    'label_feedback_name'    => 'Name',
    'label_feedback_email'   => 'E-mail',
    'label_feedback_phone'   => 'Phone',
    'label_feedback_text'    => 'Text',
    'label_feedback_form'    => 'Form',
    'label_feedback_url'     => 'URL address',
    'label_feedback_ip'      => 'IP address',
    'label_feedback_os'      => 'ОS',
    'label_feedback_browser' => 'Browser',
    'label_feedback_date'    => 'Date'
);
?>