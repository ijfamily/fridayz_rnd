<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализаци
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'info_title' => 'Деталі запису "%s"',
    // поля
    'label_feedback_name'    => 'Iм\'я',
    'label_feedback_email'   => 'E-mail',
    'label_feedback_phone'   => 'Телефон',
    'label_feedback_text'    => 'Текст',
    'label_feedback_form'    => 'Форма',
    'label_feedback_url'     => 'URL адреса',
    'label_feedback_ip'      => 'IP адреса',
    'label_feedback_os'      => 'ОС',
    'label_feedback_browser' => 'Браузер',
    'label_feedback_date'    => 'Дата'
);
?>