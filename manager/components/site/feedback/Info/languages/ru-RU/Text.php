<?php
/**
 * Gear Manager
 *
 * Пкает русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'info_title' => 'Информация о записи "%s"',
    // поля
    'title_tab_attr'         => 'Атрибуты',
    'title_tab_text'         => 'Текст',
    'title_fieldset_user'    => 'Пользователь',
    'title_fieldset_msg'     => 'Сообщение',
    'label_feedback_name'    => 'Имя',
    'label_feedback_email'   => 'E-mail',
    'label_feedback_phone'   => 'Телефон',
    'label_feedback_text'    => 'Текст',
    'label_feedback_form'    => 'Форма',
    'label_feedback_url'     => 'URL адрес',
    'label_feedback_ip'      => 'IP адрес',
    'label_feedback_os'      => 'ОС',
    'label_feedback_browser' => 'Браузер',
    'label_feedback_date'    => 'Дата'
);
?>