<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные для интерфейса профиля обратной связи"
 * Пакет контроллеров "Профиль обратной связи"
 * Группа пакетов     "Обратная связь"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SFeedback_Info
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные для интерфейса профиля обратной связи
 * 
 * @category   Gear
 * @package    GController_SFeedback_Info
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SFeedback_Info_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array(
        'feedback_name', 'feedback_email', 'feedback_phone', 'feedback_text', 'feedback_date', 'feedback_ip',
        'feedback_url', 'feedback_browser', 'feedback_os', 'feedback_form'
    );

    /**
     * Первичный ключ таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'feedback_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_feedback';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sfeedback_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        $settings = $this->session->get('user/settings');

        return sprintf($this->_['info_title'],
               date($settings['format/datetime'], strtotime($record['feedback_date'])));
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param string $sql запрос SQL на вывод данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        parent::dataView($sql);

        // обновить статус сообщения
        $table = new GDb_Table('site_feedback', 'feedback_id', array('feedback_read'));
        if ($table->update(array('feedback_read' => 1), $this->uri->id) === false)
            throw new GSqlException();
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        $settings = $this->session->get('user/settings');
        $record['feedback_date'] = date($settings['format/datetime'], strtotime($record['feedback_date']));

        return $record;
    }
}
?>