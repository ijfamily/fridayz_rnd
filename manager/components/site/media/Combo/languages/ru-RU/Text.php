<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    'css'    => 'Каскадные таблицы стилей',
    'images' => 'Изображения',
    'fonts'  => 'Шрифты',
    'font'   => 'Шрифты',
    'js'     => 'Скрипты',
    'audio'  => 'Аудиофайлы',
    'video'  => 'Видеофайлы',
    'docs'   => 'Документы',
    'categories' => 'Категории',
    'galleries' => 'Изображения в галереях',
    'banners' => 'Изображения банеров',
    'sliders' => 'Изображения на слайдах',
    'temp'    => 'Временный каталог',
    'albums' => 'Изображения альбомов',
    'plugins' => 'Плагины',
    // узлы дерева
    'nodes' => array(
        'fld-root' => array(
            array('expanded' => false,
                  'leaf'     => false,
                  'id'       => 'fld-data',
                  'dir'      => '/data/',
                  'text'     => 'Медиафайлы'),
            array('expanded' => false,
                  'leaf'     => false,
                  'id'       => 'fld-themes',
                  'dir'      => '/themes/',
                  'text'     => 'Темы сайта')
        )
    ),
    'text_node_categories' => 'Категории статьи',
    'text_node_images' => 'Изображения статьи',
    'text_node_docs'   => 'Документы статьи',
    'text_node_banners' => 'Изображения банеров',
    // сообщения
    'msg_cant_open_dir' => 'Невозможно получить доступ к каталогу "%s"'
);
?>