<?php
/**
 * Gear Manager
 *
 * Контроллер         "Тригер обработки каталогов"
 * Пакет контроллеров "Медиафайлы"
 * Группа пакетов     "Сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SMedia_Combo
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Nodes.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Combo/Nodes');

/**
 * Тригер обработки каталогов
 * 
 * @category   Gear
 * @package    GController_SMedia_Combo
 * @subpackage Nodes
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Nodes.php 2016-01-01 21:00:00 Gear Magic $
 */
final class GController_SMedia_Combo_Nodes extends GController_Combo_Nodes
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_smedia_dialog';

    /**
     * Псевдонимы имён папок на сервере
     *
     * @var string
     */
    protected $_folderAlias = array();

    /**
     * Конструктор
     * 
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // псевдонимы имён папок на сервере
        $this->_folderAlias = array(
            'fld-themes' => $this->config->getFromCms('Site', 'DIR/THEMES'),
            'fld-data'   => $this->config->getFromCms('Site', 'DIR/DATA'),
            'fld-images' => $this->config->getFromCms('Site', 'DIR/IMAGES'),
            'fld-docs'   => $this->config->getFromCms('Site', 'DIR/DOCS'),
            'fld-categories' => $this->config->getFromCms('Site', 'DIR/CATEGORIES'),
            'fld-banners'    => 'data/banners/',
        );
    }

    /**
     * Убрать излишки
     * 
     * @param string $str путь к файлам
     * @return string
     */
    public static function stripPath($str)
    {
        return str_replace(array('..', '.', 'application'), '', $str);
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function nodesView($sql = '')
    {
        parent::nodesAccessView();

        $_nodes = $this->_['nodes'];

        // вывод дополнительных узлов в зависимости от вида интерфейса
        switch ($this->store->get('view')) {
            case 'categories':
                $dir = $this->_folderAlias['fld-categories'];
                $_nodes['fld-root'][] = array(
                    'expanded' => false,
                    'leaf'     => true,
                    'id'       => 'fld-categories',
                    'dir'      => '/' .$dir,
                    'icon'     => $this->resourcePath . 'icon-node-images.png',
                    'cls'      => 'mn-node-dir',
                    'text'     => $this->_['text_node_categories']
                );
                break;

            case 'images':
                $dir = $this->_folderAlias['fld-images'] . $this->store->get('folder', '', 'gcontroller_sarticles_grid') . '/';
                $_nodes['fld-root'][] = array(
                    'expanded' => false,
                    'leaf'     => true,
                    'id'       => 'fld-images',
                    'dir'      => '/' .$dir,
                    'icon'     => $this->resourcePath . 'icon-node-images.png',
                    'cls'      => 'mn-node-dir',
                    'text'     => $this->_['text_node_images']
                );
                break;

            case 'docs':
                $dir = $this->_folderAlias['fld-docs'] . $this->store->get('folder', '', 'gcontroller_sarticles_grid'). '/';
                $_nodes['fld-root'][] = array(
                    'expanded' => false,
                    'leaf'     => true,
                    'id'       => 'fld-docs',
                    'dir'      => '/' .$dir,
                    'icon'     => $this->resourcePath . 'icon-node-docs.png',
                    'cls'      => 'mn-node-dir',
                    'text'     => $this->_['text_node_docs']
                );
                break;

            case 'banners':
                $dir = $this->_folderAlias['fld-banners'];
                $_nodes['fld-root'][] = array(
                    'expanded' => false,
                    'leaf'     => true,
                    'id'       => 'fld-banners',
                    'dir'      => '/' .$dir,
                    'icon'     => $this->resourcePath . 'icon-node-images.png',
                    'cls'      => 'mn-node-dir',
                    'text'     => $this->_['text_node_banners']
                );
                break;
        }

        // данные узла по выбранному идент.
        $node = $this->getNode($this->uri->getVar('node', 0));
        // сборка узлов по запросу
        $nodes = array();
        $nodeId = self::stripPath($this->uri->getVar('node', 0));
        if (isset($this->_folderAlias[$nodeId])) {
            $path = $this->_folderAlias[$nodeId];
            // если каталог существует
            if (!file_exists(DOCUMENT_ROOT . $path))
                throw new GException('Error', sprintf($this->_['msg_cant_open_dir'], $path));
            // чтение каталога
            if ($handle = opendir(DOCUMENT_ROOT . $path)) {
                while (false !== ($file = readdir($handle))) {
                    if (is_dir(DOCUMENT_ROOT . $path . $file) && $file != '..' && $file != '.') {
                        //GFactory::getDg()->log($path . $file);
                        $node = array(
                            'expanded' => false,
                            'iconCls'  => 'x-tree-node-icon',
                            'leaf'     => false,
                            'id'       => $path . $file . '/::',
                            'dir'      => '/' . $path . $file . '/',
                            'text'     => isset($this->_[$file]) ? $this->_[$file] : $file
                        );
                        switch ($path . $file . '/') {
                            case $this->config->getFromCms('Site', 'DIR/AUDIO', ''): $node['icon'] = $this->resourcePath . 'icon-node-audio.png'; break;
                            case $this->config->getFromCms('Site', 'DIR/VIDEO', ''): $node['icon'] = $this->resourcePath . 'icon-node-video.png'; break;
                            case $this->config->getFromCms('Site', 'DIR/BANNERS', ''):
                            case $this->config->getFromCms('Site', 'DIR/IMAGES', ''):
                                $node['icon'] = $this->resourcePath . 'icon-node-images.png';
                                break;
                            case $this->config->getFromCms('Site', 'DIR/DOCS', ''): $node['icon'] = $this->resourcePath . 'icon-node-docs.png'; break;
                            case $this->config->getFromCms('Galleries', 'DIR', ''):
                            case $this->config->getFromCms('Site', 'DIR/SLIDERS', ''):
                                $node['icon'] = $this->resourcePath . 'icon-node-sliders.png';
                                break;
                        }
                        $nodes[] = $node;
                    }
                }
                closedir($handle);
            }
        } else
        // если есть необходимость выводить вложенные каталоги
        if (($pos = strpos($nodeId, '::')) !== false || $nodeId == 'themes') {
            $path = substr($nodeId, 0, $pos);
            // если каталог существует
            if (!file_exists(DOCUMENT_ROOT . $path))
                throw new GException('Error', sprintf($this->_['msg_cant_open_dir'], $path));
            // чтение каталога
            if ($handle = opendir(DOCUMENT_ROOT . $path)) {
                while (false !== ($file = readdir($handle))) {
                    if (is_dir(DOCUMENT_ROOT . $path . $file) && $file != '..' && $file != '.') {
                        if (isset($this->_[$file]))
                            $title = $this->_[$file];
                        else
                            $title = $file;
                        $nodes[] = array(
                            'expanded' => false,
                            'iconCls'  => 'x-tree-node-icon',
                            'leaf'     => false,
                            'id'       => $path . $file . '/::',
                            'dir'      => '/' . $path . $file . '/',
                            'text'     => $title
                        );
                    }
                }
                closedir($handle);
            }
        } else {
            if (!isset($_nodes[$nodeId])) return;
            $nodes = $_nodes[$nodeId];
        }

        $this->response->data = $nodes;
    }

    /**
     * Возращает выбранный узел дерева по его идент.
     * 
     * @param integer $nodeId идент. выбранного узла дерева
     * @return void
     */
    protected function getNode($nodeId)
    {
        return array('id' => $nodeId);
    }
}
?>