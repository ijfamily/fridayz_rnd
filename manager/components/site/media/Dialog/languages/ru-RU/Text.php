<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_dialog'             => 'Медиафайлы',
    'tooltip_icon_size64x64'   => 'Размер 1164x64 пкс.',
    'tooltip_icon_size128x128' => 'Размер 128x128 пкс.',
    'tooltip_icon_size256x256' => 'Размер 1256x256 пкс.',
    'tooltip_btn_upload'       => 'Загрузить файл',
    'tooltip_btn_delete'       => 'Удалить файл',
    'tooltip_btn_download'     => 'Скачать файл',
    // сообщения
    'msg_confirm_delete' => 'Вы действительно желаете удалить файлы?',
    'msg_select_one'     => 'Необходимо в списке выбрать один файл!'
);
?>