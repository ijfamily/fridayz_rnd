<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля диалогового окна"
 * Пакет контроллеров "Медиафайлы"
 * Группа пакетов     "Сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SMedia_Dialog
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::ext('Component');
Gear::controller('Interface');

/**
 * Интерфейс профиля диалогового окна
 * 
 * @category   Gear
 * @package    GController_SMedia_Dialog
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SMedia_Dialog_Interface extends GController_Interface
{
    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp = new Ext_Component(
            array('xtype'          => 'mn-widget-mediaviewer',
                  'title'          => $this->_['title_dialog'],
                  'tlpIconSize64'  => $this->_['tooltip_icon_size64x64'],
                  'tlpIconSize128' => $this->_['tooltip_icon_size128x128'],
                  'tlpIconSize256' => $this->_['tooltip_icon_size256x256'],
                  'tlpBtnUpload'   => $this->_['tooltip_btn_upload'],
                  'tlpBtnDelete'   => $this->_['tooltip_btn_delete'],
                  'tlpBtnDownload' => $this->_['tooltip_btn_download'],
                  'msgConfirm'     => $this->_['msg_confirm_delete'],
                  'titleEllipsis'  => 40,
                  'width'          => 900,
                  'height'         => 500,
                  'resizable'      => true,
                  'stateful'       => false,
                  'showButtons'    => (bool) $this->uri->getVar('setTo', false))
        );
        $this->store->set('view', $this->uri->getVar('view', 'all'));
    }

    /**
     * Возвращает интерфейс
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->iconSrc = $this->resourcePath . 'icon.png';
        // медиавьювер
        $this->_cmp->mediaConfig = array(
            'urlAction'    => $this->componentUrl . 'files/data/',
            'urlUpload'    => $this->componentUrl . 'upload/interface/',
            'msgSelectOne' => $this->_['msg_select_one'],
            'selectTo'     => $this->uri->getVar('setTo', ''),
            'iconNone'     => $this->resourcePath . '../../Files/resources/icons/Default.png'
        );
        // главный узел
        $this->_cmp->rootConfig = array(
            'id'       => 'fld-root',
            'text'     => $_SERVER['HTTP_HOST'],
            'expanded' => true,
            'dir'      => '/',
            'icon'     => $this->resourcePath . 'icon-node-root.png'
        );
        // сбросить выбранный каталог
        $this->store->set('path', '');

        parent::getInterface();
    }
}
?>