<?php
/**
 * Gear Manager
 *
 * Контроллер         "Каталог файлов"
 * Пакет контроллеров "Медиафайлы"
 * Группа пакетов     "Сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   GController_SMedia_Files
 * @package    Shortcuts
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('File');
Gear::controller('Data');

/**
 * Каталог файлов
 * 
 * @category   Gear
 * @package    GController_SMedia_Files
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SMedia_Files_Data extends GController_Data
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_smedia_dialog';

    /**
     * Сортируемое поле
     * 
     * @var string
     */
    protected $_orderBy;

    /**
     * Сортировка
     * 
     * @var string
     */
    protected $_sort = 'group';

    /**
     * Путь к каталогу
     * 
     * @var string
     */
    public $fpath = '';

    /**
     * Допустимые расширения изображения файлов
     * 
     * @var array
     */
    protected $_imagesExt = array();

    /**
     * Псевдонимы имён папок на сервере
     *
     * @var string
     */
    protected $_folderAlias = array();

    public $folderAlias = false;

    /**
     * Конструктор
     * 
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // псевдонимы имён папок на сервере
        $this->_folderAlias = array(
            'fld-themes'     => $this->config->getFromCms('Site', 'DIR/THEMES'),
            'fld-data'       => $this->config->getFromCms('Site', 'DIR/DATA'),
            'fld-images'     => $this->config->getFromCms('Site', 'DIR/IMAGES'),
            'fld-docs'       => $this->config->getFromCms('Site', 'DIR/DOCS'),
            'fld-categories' => $this->config->getFromCms('Site', 'DIR/CATEGORIES'),
            'fld-banners'    => 'data/banners/',
        );
        // допустимые расширения изображения файлов
        $this->_imagesExt = explode(',', $this->config->getFromCms('Site', 'FILES/EXT/IMAGES'));

        // путь к каталогу
        $this->fpath = $this->uri->getVar('path', '/');
        // проверка существования псевдонима имён папок на сервере
        if (isset($this->_folderAlias[$this->fpath])) {
            $this->folderAlias = $this->fpath;
            if ($this->fpath == 'fld-images' || $this->fpath == 'fld-docs') {
                $this->fpath = $this->_folderAlias[$this->fpath] . $this->store->get('folder', '/', 'gcontroller_sarticles_grid') . '/';
            } else
                $this->fpath = $this->_folderAlias[$this->fpath];
            
        }
        // размер картинки
        $this->imgSize = $this->uri->getVar('imgSize', 641);
    }

    /**
     * Убрать излишки
     * 
     * @param string $str путь к файлам
     * @return string
     */
    public static function stripPath($str)
    {
        return str_replace(array('..', '.', 'application'), '', $str);
    }

    /**
     * Удаление файлов
     * 
     * @return void
     */
    protected function dataDelete()
    {
        $this->dataAccessDelete();

        $this->response->setMsgResult('Deleting data', $this->_['msg_success_delete'], true);

        // путь к файлу
        $path = $this->store->get('path');
        // выделенные файлы
        if (($sFiles = $this->input->get('files', false)) === false)
            throw new GException('Error', $this->_['msg_no_selections']);
        $files = explode(',', $sFiles);
        $count = sizeof($files);
        for ($i = 0; $i < $count; $i++) {
            // проверка расширения
            $ext = pathinfo($files[$i], PATHINFO_EXTENSION);
            if (strtoupper($ext) == 'PHP')
                throw new GException('Deleting data', 'Can`t perform file deletion "%s"', $files[$i]);
            // содержит ли файл изображения привью
            $name = pathinfo($files[$i], PATHINFO_FILENAME);
            $ext = pathinfo($files[$i], PATHINFO_EXTENSION);
            $filename = DOCUMENT_ROOT . $path . $name . '_medium.' . $ext;
            if (file_exists($filename))
                unlink($filename);
            $filename = DOCUMENT_ROOT . $path . $name . '_thumb.' . $ext;
            if (file_exists($filename))
                unlink($filename);

            GFile::delete(DOCUMENT_ROOT . $path . $files[$i]);
        }
    }

    /**
     * Список файлов
     * 
     * @return void
     */
    protected function dataFiles()
    {
        // если путь виртуальный
        if ($this->fpath == 'fld-root' || $this->fpath == '/' || $this->fpath == '') return;
        // если есть необходимость просмотреть вложенны каталоги
        if (($pos = strpos($this->fpath, '::')) !== false)
            $this->fpath = substr($this->fpath, 0, $pos);
        // проверка существования каталога
        $dirExist = file_exists(DOCUMENT_ROOT . $this->fpath);
        if (!$dirExist) {
            if ($this->folderAlias == 'fld-docs' || $this->folderAlias == 'fld-images')
                $this->response->data = array();
            else
                throw new GException('Error', sprintf($this->_['msg_cant_open_dir'], $this->fpath));
        }

        $this->store->set('path', self::stripPath($this->fpath));
        $this->store->set('folderAlias', $this->folderAlias);
        // список каталогов
        $files = array();
        if ($dirExist)
            if ($handle = opendir(DOCUMENT_ROOT . $this->fpath)) {
                while (false !== ($file = readdir($handle))) {
                    // если файл
                    if (is_file(DOCUMENT_ROOT . $this->fpath . $file)) {
                        // расширение
                        $ext = pathinfo($file, PATHINFO_EXTENSION);
                        $extU = strtoupper($ext);
                        // не показывать сценарии
                        if ($extU == 'PHP') continue;
                        // если изображение
                        $isImage = in_array($extU, $this->_imagesExt);
                        if ($isImage)
                            $img = '<img class="img" src="/' . $this->fpath . $file . '" title="' . $file . '" onload="onLoadImg(this, \'/' . $this->fpath . $file . '\');"><span class="loader">';
                        else
                            $img ='<img class="icon" width="' . $this->imgSize . 'px" height="' . $this->imgSize . 'px" src="' . $this->resourcePath . '/icons/' . $ext . '.png" title="' . $file . '">';

                        if (strpos($file, '_medium') !== false || strpos($file, '_thumb') !== false) continue;
                        // содержит ли файл изображения привью
                        $name = pathinfo($file, PATHINFO_FILENAME);
                        $filename = DOCUMENT_ROOT . $this->fpath . $name . '_medium.' . $ext;
                        $shMedium = file_exists($filename) ? '' : ' none';
                        $filename = DOCUMENT_ROOT . $this->fpath . $name . '_thumb.' . $ext;
                        $shThumb = file_exists($filename) ? '' : ' none';

                        $files[] = array(
                            'title'    => $file,
                            'tooltip'  => $file,
                            'filename' => $file,
                            'path'     => $this->fpath . '/???/',
                            'name'     => $name,
                            'ext'      => '.' . $ext,
                            'wrapBg'   => $isImage ? ' bg' : '',
                            'img'      => $img,
                            'imgSize'  => $this->imgSize,
                            'enabled'  => true,
                            'shMedium' => $shMedium,
                            'shThumb'  => $shThumb,
                            'medium'   => !(bool) $shMedium,
                            'thumb'    => !(bool) $shThumb
                        );
                    }
                }
                closedir($handle);
            }

        $this->response->data = $files;
    }

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        // метод запроса
        switch ($this->uri->method) {
            // метод "GET"
            case 'GET':
                // вывод данных в интерфейс
                $this->dataFiles();
                return;

            // метод "DELETE"
            case 'DELETE':
                // удаление файлов
                $this->dataDelete();
            return;
        }

        parent::init();
    }
}
?>