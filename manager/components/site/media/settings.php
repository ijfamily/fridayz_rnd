<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Медиафайлы"
 * Группа пакетов     "Медиафайлы"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SMedia
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
* Установка компонента
 * 
 * Название: Медиафайлы
 * Описание: Библиотека файлов сайта
 * Меню:
 * ID класса: gcontroller_smedia_dialog
 * Модуль: Сайт
 * Группа: Сайт
 * Очищать: нет
 * Ресурс
 *    Компонент: site/media/
 *    Контроллер: site/media/Dialog/
 *    Интерфейс: dialog/interface/
 *    Меню:
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске компонентов: нет
 *    В главном меню: нет
 */

return array(
    // {Y}- модуль "Site" {S} - пакет контроллеров "Media" -> SMedia
    'clsPrefix' => 'SMedia',
    // использовать язык
    'language'  => 'ru-RU'
);
?>