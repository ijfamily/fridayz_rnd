<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля загрузки файлов"
 * Пакет контроллеров "Загрузка файлов"
 * Группа пакетов     "Сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SMedia_Upload
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля загрузки файлов
 * 
 * @category   Gear
 * @package    GController_SMedia_Upload
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SMedia_Upload_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_upload_dialog'],
                  'id'            => $this->classId,
                  'titleEllipsis' => 80,
                  'width'         => 600,
                  'height'        => 550,
                  'resizable'     => false,
                  'stateful'      => false,
                  'btnInsertText' => $this->_['text_btn_insert'])
        );

        // вкладка "Изображения"
        $oSize = explode('x', $this->config->getFromCms('Site', 'IMAGE/ORIGINAL/SIZE'));
        $mSize = explode('x', $this->config->getFromCms('Site', 'IMAGE/MEDIUM/SIZE'));
        $tSize = explode('x', $this->config->getFromCms('Site', 'IMAGE/THUMB/SIZE'));
        $tabImages = array(
            'title'       => $this->_['title_tab_img'],
            'layout'      => 'form',
            'labelWidth'  => 112,
            'baseCls'     => 'mn-form-tab-body',
            'iconSrc'     => $this->resourcePath . 'icon-tab-img.png',
            'autoScroll'  => true,
            'items'       => array(
                array('xtype'      => 'fieldset',
                      'title'      => $this->_['title_fieldset_original'],
                      'labelWidth' => 100,
                      'items'      => array(
                          array('xtype'      => 'numberfield',
                                'fieldLabel' => $this->_['label_image_width'],
                                'width'      => 100,
                                'resetable'  => false,
                                'name'       => 'settings[IMAGE/ORIGINAL/WIDTH]',
                                'value'      => (int) $oSize[0],
                                'checkDirty' => false),
                          array('xtype'      => 'numberfield',
                                'fieldLabel' => $this->_['label_image_height'],
                                'width'      => 100,
                                'resetable'  => false,
                                'name'       => 'settings[IMAGE/ORIGINAL/HEIGHT]',
                                'value'      => (int) $oSize[1],
                                'checkDirty' => false),
                          array('xtype'      => 'checkbox',
                                'fieldLabel' => $this->_['label_img_apply'],
                                'checkDirty' => false,
                                'checked'    => true,
                                'resetable'  => false,
                                'name'       => 'settings[IMAGE/ORIGINAL/APPLY]'),
                          array('xtype' => 'label', 'html' => $this->_['label_image_info']),
                      )
                ),
                array('xtype'      => 'fieldset',
                      'title'      => $this->_['title_fieldset_medium'],
                      'labelWidth' => 100,
                      'items'      => array(
                          array('xtype'      => 'numberfield',
                                'fieldLabel' => $this->_['label_image_width'],
                                'width'      => 100,
                                'resetable'  => false,
                                'name'       => 'settings[IMAGE/MEDIUM/WIDTH]',
                                'value'      => (int) $mSize[0],
                                'checkDirty' => false),
                          array('xtype'      => 'numberfield',
                                'fieldLabel' => $this->_['label_image_height'],
                                'width'      => 100,
                                'resetable'  => false,
                                'name'       => 'settings[IMAGE/MEDIUM/HEIGHT]',
                                'value'      => (int) $mSize[1],
                                'minValue'   => 0,
                                'checkDirty' => false),
                          array('xtype'      => 'checkbox',
                                'fieldLabel' => $this->_['label_img_apply'],
                                'checkDirty' => false,
                                'checked'    => false,
                                'resetable'  => false,
                                'name'       => 'settings[IMAGE/MEDIUM/APPLY]')
                      )
                ),
                array('xtype'      => 'fieldset',
                      'title'      => $this->_['title_fieldset_thumb'],
                      'labelWidth' => 100,
                      'items'      => array(
                          array('xtype'      => 'numberfield',
                                'fieldLabel' => $this->_['label_image_width'],
                                'width'      => 100,
                                'resetable'  => false,
                                'name'       => 'settings[IMAGE/THUMB/WIDTH]',
                                'value'      => (int) $tSize[0],
                                'checkDirty' => false),
                          array('xtype'      => 'numberfield',
                                'fieldLabel' => $this->_['label_image_height'],
                                'width'      => 100,
                                'resetable'  => false,
                                'name'       => 'settings[IMAGE/THUMB/HEIGHT]',
                                'value'      => (int) $tSize[1],
                                'checkDirty' => false),
                          array('xtype'      => 'checkbox',
                                'fieldLabel' => $this->_['label_img_apply'],
                                'checkDirty' => false,
                                'checked'    => false,
                                'resetable'  => false,
                                'name'       => 'settings[IMAGE/THUMB/APPLY]')
                      )
                ),
                array('xtype'      => 'checkbox',
                      'fieldLabel' => $this->_['label_watermark'],
                      'checkDirty' => false,
                      'checked'    => false,
                      'resetable'  => false,
                      'name'       => 'settings[WATERMARK]',
                      'checkDirty' => false),
            )
        );

        // вкладка "Файл"
        $tabCommon = array(
            'title'       => $this->_['title_tab_common'],
            'layout'      => 'form',
            'labelWidth'  => 400,
            'baseCls'     => 'mn-form-tab-body',
            'iconSrc'     => $this->resourcePath . 'icon-tab-upload.png',
            'autoScroll'  => true,
            'items'       => array(
                array('xtype'      => 'fieldset',
                      'title'      => $this->_['title_fieldset_ext'],
                      'labelWidth' => 90,
                      'defaults'   => array('xtype' => 'displayfield', 'fieldClass' => 'mn-field-value-info'),
                      'items'      => array(
                          array('fieldLabel' => $this->_['label_images'],
                                'value'      => $this->config->getFromCms('Site', 'FILES/EXT/IMAGES')),
                          array('fieldLabel' => $this->_['label_docs'],
                                'value'      => $this->config->getFromCms('Site', 'FILES/EXT/DOCS')),
                          array('fieldLabel' => $this->_['label_audio'],
                                'value'      => $this->config->getFromCms('Site', 'FILES/EXT/AUDIO')),
                          array('fieldLabel' => $this->_['label_video'],
                                'value'      => $this->config->getFromCms('Site', 'FILES/EXT/VIDEO')),
                      )
                )
            )
        );
        $count = (int) ini_get('max_file_uploads');
        if ($count == 0) $count = 1;
        for ($i = 1; $i <= $count; $i++) {
            $tabCommon['items'][] = array(
                'xtype'      => 'mn-field-upload',
                'emptyText'  => sprintf($this->_['text_empty'], $i),
                'fieldLabel' => 'Файл ' . $i,
                'name'       => 'upload' . $i,
                'buttonText' => $this->_['text_btn_upload'],
                'anchor'     => '100%',
                'buttonCfg'  => array('width' => 70)
            );
        }

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->url = $this->componentUrl . 'upload/';
        $form->fileUpload = true;
        $form->items->addItems(
            array(
                array('xtype'             => 'tabpanel',
                      'layoutOnTabChange' => true,
                      'deferredRender'    => false,
                      'activeTab'         => 0,
                      'style'             => 'padding:3px',
                      'enableTabScroll'   => true,
                      'anchor'            => '100% 100%',
                      //'height'            => 250,
                      'items'             => array($tabCommon, $tabImages))
            )
        );

        parent::getInterface();
    }
}
?>