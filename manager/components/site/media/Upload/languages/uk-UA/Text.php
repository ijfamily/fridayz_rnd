<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Створення запису "Зображення"',
    'title_profile_update' => 'Зміна запису "%s"',
    // поля формы
    // закладка "атрибуты"
    'title_tab_attributes' => 'Атрибути',
    'label_image_index'    => 'Індекс',
    'tip_image_index'      => 
        'Порядковий номер (для галереї - впорядкованість списку; для статті - формування псевдоніма; для кожної '
      . 'категорії свій порядок)',
    'label_category_name'  => 'Категорія',
    'tip_category_name'    => 'Для кожного виду категорії зображення по різному відображаються в тексті статті',
    'label_image_visible'  => 'Показувати',
    'tip_image_visible'    => 'Показувати зображення',
    'label_image_archive'  => 'Архів',
    'tip_image_archive'    => 'Зображення в архіві (не буде доступно)',
    'label_image_longdesc' => 'URL адреса',
    'label_image_title'    => 'Текст',
    'tip_image_title'      => 
        'Використовується для підпису зображення якщо зображення відсутній (атрибути "alt", "title")',
    'tip_image_longdesc'   => 
        'Вказується URL адресу для переходу на сторінку сайту де розташована розгорнута інформація про зображення',
    // закладка "изображение"
    'title_tab_img'           => 'Зображення',
    'title_fieldset_img'      => 'Файл (%s)',
    'text_empty'              => 'Виберіть файл ...',
    'text_btn_upload'         => 'Выбрати',
    'title_fieldset_aimg'     => 'Атрибути зображення',
    'label_entire_filename'   => 'Файл',
    'label_entire_uri'        => 'Русурс URI',
    'tip_entire_uri'          => 
        'Це символьний рядок, що дозволяє ідентифікувати небудь ресурс: документ, зображення, файл, службу, '
      . 'ящик електронної пошти і т. д.',
    'label_entire_url'        => 'Русурс URL',
    'tip_entire_url'          => 
        'Однаковий локатор (визначник місцезнаходження) ресурсу. Це стандартизований спосіб запису адреси '
      . 'ресурсу в мережі Інтернет.',
    'label_entire_resolution' => 'Дозвіл',
    'tip_entire_resolution'   => 'Дозвіл файлу в пікселях',
    'label_entire_type'       => 'Тип',
    'tip_entire_type'         => 'Тип файлу ("JPEG", "PNG", "GIF")',
    'label_entire_filesize'   => 'Розмір',
    'tip_entire_filesize'     => 'Розмір файлу',
    'label_date_insert'       => 'Створено',
    'label_date_update'       => 'Змінено',
    // закладка "эскиз"
    'title_tab_thm'      => 'Ескиз',
    'title_fieldset_ath' => 'Атрибути ескізу',
);
?>