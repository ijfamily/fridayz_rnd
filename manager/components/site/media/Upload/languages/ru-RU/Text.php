<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_upload_dialog' => 'Загрузка файла на сервер',
    'title_upload'        => 'Загрузка файла',
    'text_btn_insert'     => 'Загрузить',
    // поля формы
    // вкладка "Файл"
    'title_tab_common'   => 'Загрузка файла',
    'title_fieldset'     => 'Загрузка файла',
    'text_empty'         => 'Выберите файл %s ...',
    'text_btn_upload'    => 'Выбрать',
    'title_fieldset_img' => 'Для изображений',
    'title_tab_img'      => 'Для изображений',
    'title_fieldset_ext' => 'Допустимые расширения файла для',
    'label_images'       => 'изображения',
    'label_docs'         => 'документа',
    'label_audio'        => 'аудио',
    'label_video'        => 'видео',
    'label_img_apply'    => 'Применить',
    // вкладка "Изображения"
    'label_watermark' => 'Водяной знак',
    'title_tab_img' => 'Для изображения',
    'label_image_width' => 'Ширина, пкс',
    'label_image_height' => 'Высота, пкс',
    'title_fieldset_original' => 'Размеры оригинального изображения',
    'title_fieldset_medium' => 'Размеры средний копии изображения',
    'title_fieldset_thumb' => 'Размеры миниатюры изображения',
    'label_image_info' => '<note>если размер загруженного изображения будет больше указанного , то изображение будет автоматически уменьшено до нужного размера, '
                        . 'иначе изображение будет с оригинальным размером. '
                        . 'В случаи если указан 0 по одной из сторон, то по другой стороне будет проводиться контроль оригинального изображения.</note>',
    // сообщения
    'msg_dir_empty'      => 'Не выбран каталог!',
    'msg_cant_open_dir'  => 'Невозможно получить доступ к каталогу "%s"',
    'msg_cant_mk_dir'    => 'Невозможно создать каталог "%s"',
    'msg_file_empty'     => 'Не выбран файл!',
    'msg_success_upload' => 'Файл успешно загружен на сервер!'
);
?>