<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные интерфейса профиля загрузки файлов"
 * Пакет контроллеров "Загрузка файлов"
 * Группа пакетов     "Сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SMedia_Upload
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('File');
Gear::controller('Profile/Data');

/**
 * Данные интерфейса профиля загрузки файлов
 * 
 * @category   Gear
 * @package    GController_SMedia_Upload
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SMedia_Upload_Data extends GController_Profile_Data
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_smedia_dialog';

    /**
     * Доступные расширения файлов для загрузки
     *
     * @var array
     */
    protected $_upload = array();

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // путь к файлу
        $this->_path = $this->store->get('path', '', 'gcontroller_smedia_dialog');
        // псевдоними имени папки на сервере
        $this->folderAlias = $this->store->get('folderAlias', '', 'gcontroller_smedia_dialog');
        // доступные расширения файлов для загрузки
        $ext = $this->config->getFromCms('Site', 'FILES/EXT/IMAGES', '');
        $ext .= $this->config->getFromCms('Site', 'FILES/EXT/DOCS', '');
        $ext .= $this->config->getFromCms('Site', 'FILES/EXT/AUDIO', '');
        $ext .= $this->config->getFromCms('Site', 'FILES/EXT/VIDEO', '');
        $this->_upload = explode(',', strtolower($ext));
    }

    /**
     * Вставка данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataInsert($params = array())
    {
        $this->dataAccessInsert();

        $this->response->setMsgResult($this->_['title_upload'], $this->_['msg_success_upload'], true);
        $st = $this->input->get('settings', array());

        // проверка существования пути
        if (empty($this->_path))
            throw new GException('Error', $this->_['msg_dir_empty']);
        $this->_path = DOCUMENT_ROOT . $this->_path;
        if (!file_exists($this->_path)) {
            // если папка изображений сайта еще не создана 
            if ($this->folderAlias == 'fld-images' || $this->folderAlias == 'fld-docs') {
                if (mkdir($this->_path, 0700) === false)
                    throw new GException($this->_['title_upload'], sprintf($this->_['msg_cant_mk_dir'], $this->_path));
            } else
                throw new GException('Error', sprintf($this->_['msg_cant_open_dir'], $this->_path));
        }
        $isUploaded = false;
        $count = (int) ini_get('max_file_uploads');
        if ($count == 0) $count = 1;
        for ($i = 1; $i <= $count; $i++) {
            // если файла для загрузки
            if (($file = $this->input->file->get('upload' . $i)) === false) continue;
            // генерация имени
            Gear::library('String');
            $filename = GString::fileToUrl($file['name'], $this->config->getFromCms('Site', 'LANGUAGE/ALIAS'));
            // загрузка изображения
            GFile::upload($file, $this->_path . $filename, $this->_upload);
    
            // проверка на изображение
            $ext = strtoupper(pathinfo($filename, PATHINFO_EXTENSION));
            $extImages = explode(',', $this->config->getFromCms('Site', 'FILES/EXT/IMAGES', ''));
            // если это изображение
            if (in_array($ext, $extImages)) {
                $this->updateImage($filename);
            }
            $isUploaded = true;
        }
        if (!$isUploaded)
            throw new GException('Adding data', $this->_['msg_file_empty']);

        // установка полей
        $this->response->add('setTo', array(
            array('id' => 'mediaviewer', 'func' => 'reload')
        ));
    }

    /**
     * Изменение размера изображения
     * 
     * @param string $filename файл изображения
     * @return void
     */
    protected function updateImage($filename)
    {
        $st = $this->input->get('settings', array());
        if (!empty($st['WATERMARK'])) {
            $img = GFactory::getClass('Image', 'Image', $this->_path . $filename);
            $img->watermark(
                DOCUMENT_ROOT . $this->config->getFromCms('Site', 'WATERMARK/STAMP', ''),
                $this->config->getFromCms('Site', 'WATERMARK/POSITION', '') 
            );
            $img->save($this->_path . $filename, $this->config->getFromCms('Site', 'IMAGE/QUALITY', 0));
        }

        // изменить оригинал
        if (isset($st['IMAGE/ORIGINAL/APPLY'])) {
            $width = (int) $st['IMAGE/ORIGINAL/WIDTH'];
            $height = (int) $st['IMAGE/ORIGINAL/HEIGHT'];
            $img = GFactory::getClass('Image', 'Image', $this->_path . $filename);
            $cmdImage = 'resize{"width": ' . $width . ', "height": ' . $height . '}';
            if ($img->execute($cmdImage)) {
                // изменение изображения
                $img->save($this->_path . $filename, $this->config->getFromCms('Site', 'IMAGE/QUALITY', 0));
            }
        }
        // создать среднюю копию
        if (isset($st['IMAGE/MEDIUM/APPLY'])) {
            $width = (int) $st['IMAGE/MEDIUM/WIDTH'];
            $height = (int) $st['IMAGE/MEDIUM/HEIGHT'];
            $img = GFactory::getClass('Image', 'Image', $this->_path . $filename);
            $cmdImage = 'resize{"width": ' . $width . ', "height": ' . $height . '}';
            if ($img->execute($cmdImage)) {
                // изменение изображения
                $name = pathinfo($filename, PATHINFO_FILENAME) . '_medium.' . pathinfo($filename, PATHINFO_EXTENSION);
                $img->save($this->_path . $name, $this->config->getFromCms('Site', 'IMAGE/QUALITY', 0));
            }
        }
        // создать миниатюры
        if (isset($st['IMAGE/THUMB/APPLY'])) {
            $width = (int) $st['IMAGE/THUMB/WIDTH'];
            $height = (int) $st['IMAGE/THUMB/HEIGHT'];
            $img = GFactory::getClass('Image', 'Image', $this->_path . $filename);
            $cmdImage = 'resize{"width": ' . $width . ', "height": ' . $height . '}';
            if ($img->execute($cmdImage)) {
                // изменение изображения
                $name = pathinfo($filename, PATHINFO_FILENAME) . '_thumb.' . pathinfo($filename, PATHINFO_EXTENSION);
                $img->save($this->_path . $name, $this->config->getFromCms('Site', 'IMAGE/QUALITY', 0));
            }
        }
    }
}
?>