<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка компонентов cms"
 * Пакет контроллеров "Список компонентов cms"
 * Группа пакетов     "Компоненты cms"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SComponents_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные списка компонентов cms
 * 
 * @category   Gear
 * @package    GController_SComponents_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SComponents_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'cmp_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_components';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'cmp_name';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 'SELECT SQL_CALC_FOUND_ROWS `c`.*, `p`.`pcmp_name` '
                   . 'FROM `site_components` `c` '
                   . 'LEFT JOIN (SELECT `cmp_id`, `cmp_name` `pcmp_name` FROM `site_components`) `p` ON `p`.`cmp_id`=`c`.`cmp_parent_id` '
                   . 'WHERE 1 %filter '
                   . 'ORDER BY %sort '
                   . 'LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // название
            'cmp_name' => array('type' => 'string'),
            // название предка
           'pcmp_name' => array('type' => 'string'),
            // класс
            'cmp_class' => array('type' => 'string'),
            // путь
            'cmp_path' => array('type' => 'string'),
            // заметка
            'cmp_note' => array('type' => 'string'),
            // псевдоним
            'cmp_alias' => array('type' => 'string'),
            // скрыт
            'cmp_hidden' => array('type' => 'integer')
        );
    }

    /**
     * Событие наступает после успешного удаления данных
     * 
     * @return void
     */
    protected function dataDeleteComplete()
    {
        // соединение с базой данных (т.к. для лога ранее возможно было другое соединение)
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();

        $query = new GDb_Query();
        // обновление потомков дерева
        $sql = 'UPDATE `site_components` SET `cmp_parent_id`=NULL '
             . 'WHERE `cmp_parent_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        // удаление записей таблицы "site_components" (компоненты)
        $table = new GDb_Table('site_components', 'component_id');
        if ($table->clear(true) !== true)
            throw new GSqlException();
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        // видимость на сайте
        if ($record['cmp_hidden'])
            $record['cmp_hidden'] = 0;
        else
            $record['cmp_hidden'] = 1;

        return $record;
    }
}
?>