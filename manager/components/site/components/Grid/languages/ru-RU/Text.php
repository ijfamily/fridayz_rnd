<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'    => 'Компоненты сайта',
    'rowmenu_edit'  => 'Редактировать',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete"> '
                                . '("Компоненты сайта")</span> ?',
    // столбцы
    'header_cmp_name'   => 'Название',
    'header_pcmp_name'  => 'Название (п)',
    'tooltip_pcmp_name' => 'Название &quot;предка&quot; компонента',
    'header_cmp_class'  => 'Класс',
    'header_cmp_path'   => 'Путь',
    'header_cmp_note'   => 'Заметка',
    'header_cmp_alias'  => 'Псевдоним',
    'header_cmp_hidden' => 'Скрыть'
);
?>