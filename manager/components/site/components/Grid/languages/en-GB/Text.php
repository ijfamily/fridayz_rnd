<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'    => 'Components cms',
    'rowmenu_edit'  => 'Edit',
    // панель управления
    'title_buttongroup_edit'   => 'Edit',
    'title_buttongroup_cols'   => 'Columns',
    'title_buttongroup_filter' => 'Filter',
    'msg_btn_clear'            => 'Do you really want to delete all records <span class="mn-msg-delete"> '
                                . '("Components cms")</span> ?',
    // столбцы
    'header_cmp_name'   => 'Name',
    'header_pcmp_name'  => 'Name (p)',
    'tooltip_pcmp_name' => 'Name &quot;parent&quot; component',
    'header_cmp_class'  => 'Class',
    'header_cmp_path'   => 'Path',
    'header_cmp_note'   => 'Note',
    'header_cmp_alias'  => 'Alias',
    'header_cmp_hidden' => 'Hidden',
);
?>