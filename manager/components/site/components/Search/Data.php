<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск компонентов сайта"
 * Пакет контроллеров "Компоненты cms"
 * Группа пакетов     "Компоненты cms"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SComponents_Search
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск компонентов сайта
 * 
 * @category   Gear
 * @package    GController_SComponents_Search
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SComponents_Search_Data extends GController_Search_Data
{
    /**
     * дентификатор компонента (списка) к которому применяется поиск
     *
     * @var string
     */
    public $gridId = 'gcontroller_scomponents_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_scomponents_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор
     * Используется для инициализации атрибутов контроллер не переданного в конструктор
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'cmp_name', 'label' => $this->_['header_cmp_name']),
            array('dataIndex' => 'pcmp_name', 'label' => $this->_['header_pcmp_name']),
            array('dataIndex' => 'cmp_class', 'label' => $this->_['header_cmp_class']),
            array('dataIndex' => 'cmp_path', 'label' => $this->_['header_cmp_path']),
            array('dataIndex' => 'cmp_note', 'label' => $this->_['header_cmp_note']),
            array('dataIndex' => 'cmp_alias', 'label' => $this->_['header_cmp_alias']),
            array('dataIndex' => 'cmp_hidden', 'label' => $this->_['header_cmp_hidden'])
        );
    }
}
?>