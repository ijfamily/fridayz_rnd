<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Search in the list "Components cms"',
    // поля
    'header_cmp_name'   => 'Name',
    'header_pcmp_name'  => 'Name (p)',
    'header_cmp_class'  => 'Class',
    'header_cmp_path'   => 'Path',
    'header_cmp_note'   => 'Note',
    'header_cmp_alias'  => 'Alias',
    'header_cmp_hidden' => 'Hidden'
);
?>