<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Пошук у списку "Компоненти cms"',
    // поля
    'header_cmp_name'   => 'Назва',
    'header_pcmp_name'  => 'Назва (п)',
    'header_cmp_class'  => 'Клас',
    'header_cmp_path'   => 'Шлях',
    'header_cmp_note'   => 'Примітка',
    'header_cmp_alias'  => 'Псевдонім',
    'header_cmp_hidden' => 'Приховати'
);
?>