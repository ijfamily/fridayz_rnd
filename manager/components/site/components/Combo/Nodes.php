<?php
/**
 * Gear Manager
 *
 * Контроллер         "Триггер выпадающего дерева для обработки данных статей"
 * Пакет контроллеров "Статьи"
 * Группа пакетов     "Статьи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SArticles_Combo
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Nodes.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Combo/Nodes');;

/**
 * Триггер выпадающего дерева для обработки данных статей
 * 
 * @category   Gear
 * @package    GController_SArticles_Combo
 * @subpackage Nodes
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Nodes.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SComponents_Combo_Nodes extends GController_Combo_Nodes
{
    /**
     * Префикс полей таблицы для формирования nested set
     *
     * @var string
     */
    public $fieldPrefix = 'cmp';

    /**
     * Первичное поле таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'cmp_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_components';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_scomponents_grid';

    /**
     * Вызывается перед предварительной обработкой запроса при формировании записей
     * 
     * @param  array $prefix префикс полей
     * @param  array $id идинд. узел дерева
     * @return string
     */
    protected function queryPreprocessing($prefix, $id)
    {
        if ($id == 'root')
            $query = 'SELECT `c1`.*, `c`.* '
                   . 'FROM `site_components` `c` '
                   . 'LEFT JOIN (SELECT COUNT(`cmp_id`) `isOpen`, `cmp_parent_id` '
                   . 'FROM `site_components` GROUP BY `cmp_parent_id`) AS `c1` '
                   . 'ON `c`.`cmp_id`=`c1`.`cmp_parent_id` '
                   . 'WHERE `c`.`cmp_parent_id` IS NULL '
                   . 'ORDER BY `c`.`cmp_name`';
        else
            $query = 'SELECT `c1`.*, `c`.* '
                   . 'FROM `site_components` `c` '
                   . 'LEFT JOIN (SELECT COUNT(`cmp_id`) `isOpen`, `cmp_parent_id` '
                   . 'FROM `site_components` GROUP BY `cmp_parent_id`) AS `c1` '
                   . 'ON `c`.`cmp_id`=`c1`.`cmp_parent_id` '
                   . 'WHERE `c`.`cmp_parent_id`=' . $id . ' '
                   . 'ORDER BY `c`.`cmp_name`';

        return $query;
    }

    /**
     * Предварительная обработка узла дерева перед формированием массива записей JSON
     * 
     * @params array $node узел дерева
     * @params array $record запись
     * @return array
     */
    protected function nodesPreprocessing($node, $record)
    {
        $node['text'] = $record['cmp_name'];
        $node['class'] = $record['cmp_class'];
        $node['path'] = $record['cmp_path'];
        $node['note'] = $record['cmp_note'];
        $node['alias'] = $record['cmp_alias'];
        if (isset($record['isOpen']))
            $node['leaf'] = !($record['isOpen'] > 0);
        else
            $node['leaf'] = true;
        if ($node['leaf'])
            $node['iconCls'] = 'icon-item-cmp';

        return $node;
    }
}
?>