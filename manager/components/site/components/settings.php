<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Компоненты сайта"
 * Группа пакетов     "Компоненты сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SComponents
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Компоненты сайта
 * Описание: Компоненты сайта
 * Меню:
 * ID класса: gcontroller_scomponents_grid
 * Модуль: Сайт
 * Группа: Сайт
 * Очищать: нет
 * Ресурс
 *    Компонент: site/components/
 *    Контроллер: site/components/Grid/
 *    Интерфейс: grid/interface/
 *    Меню:
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске компонентов: нет
 *    В главном меню: нет
 */

return array(
    // {S}- модуль "Site" {} - пакет контроллеров "Components" -> SComponents
    'clsPrefix' => 'SComponents',
    // выбрать базу данных из настроек конфига
    'conName'   => 'site',
    // использовать язык
    'language'  => 'ru-RU'
);
?>