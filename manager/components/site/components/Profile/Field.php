<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные для интерфейса компонентов cms"
 * Пакет контроллеров "Профиль компонента cms"
 * Группа пакетов     "Компоненты cms"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SComponents_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Field.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Field');

/**
 * Данные для интерфейса компонентов cms
 * 
 * @category   Gear
 * @package    GController_SComponents_Field
 * @subpackage Field
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Field.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SComponents_Profile_Field extends GController_Profile_Field
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array('cmp_hidden');

    /**
     * Первичный ключ таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'cmp_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_components';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_scomponents_grid';

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        if (isset($params['cmp_hidden'])) {
            if ($params['cmp_hidden'])
                $params['cmp_hidden'] = 0;
            else
                $params['cmp_hidden'] = 1;
        }
    }
}
?>