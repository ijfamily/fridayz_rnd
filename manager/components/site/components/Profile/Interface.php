<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля компонента cms"
 * Пакет контроллеров "Профиль компонента cms"
 * Группа пакетов     "Компоненты cms"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SComponents_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля компонента cms
 * 
 * @category   Gear
 * @package    GController_SComponents_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SComponents_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        $data = array('cmp_parent_id' => '');
        // если состояние формы "insert"
        if ($this->isInsert) return $data;

        parent::getDataInterface();

        $query = new GDb_Query();
        // выбранный потомок компонента
        $sql = 'SELECT `p`.`cmp_name` '
             . 'FROM `site_components` `p`, (SELECT `cmp_parent_id` FROM `site_components` WHERE `cmp_id`=' . (int)$this->uri->id . ') `c` '
             . 'WHERE `p`.`cmp_id`=`c`.`cmp_parent_id`';
        if (($record = $query->getRecord($sql)) === false)
            throw new GSqlException();
        if (!empty($record['cmp_name']))
            $data['cmp_parent_id'] = $record['cmp_name'];

        return $data;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'titleEllipsis' => 40,
                  'gridId'        => 'gcontroller_scomponents_grid',
                  'width'         => 450,
                  'height'        => 255,
                  'stateful'      => false,
                  'buttonsAdd'    => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-site-components',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype'      => 'mn-field-combo-tree',
                  'fieldLabel' => $this->_['label_pcmp_name'],
                  'labelTip'   => $this->_['tip_pcmp_name'],
                  'resetable'  => false,
                  'anchor'     => '100%',
                  'name'       => 'cmp_parent_id',
                  'hiddenName' => 'cmp_parent_id',
                  'treeWidth'  => 300,
                  'treeRoot'   => array('id' => 'root', 'expanded' => true, 'expandable' => true),
                  'value'      => $data['cmp_parent_id'],
                  'allowBlank' => true,
                  'store'      => array('xtype' => 'jsonstore',
                                        'url'   => $this->componentUrl . 'combo/nodes/')),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_cmp_name'],
                  'name'       => 'cmp_name',
                  'maxLength'  => 100,
                  'anchor'     => '100%',
                  'allowBlank' => false),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_cmp_class'],
                  'name'       => 'cmp_class',
                  'maxLength'  => 100,
                  'anchor'     => '100%',
                  'allowBlank' => true),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_cmp_path'],
                  'name'       => 'cmp_path',
                  'maxLength'  => 100,
                  'anchor'     => '100%',
                  'allowBlank' => true),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_cmp_note'],
                  'name'       => 'cmp_note',
                  'maxLength'  => 255,
                  'anchor'     => '100%',
                  'allowBlank' => true),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_cmp_alias'],
                  'name'       => 'cmp_alias',
                  'maxLength'  => 20,
                  'allowBlank' => true),
            array('xtype'      => 'mn-field-chbox',
                  'fieldLabel' => $this->_['label_cmp_hidden'],
                  'default'    => $this->isUpdate ? null : 0,
                  'name'       => 'cmp_hidden')
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($items);
        $form->url = $this->componentUrl . 'profile/';
        $form->labelWidth = 85;

        parent::getInterface();
    }
}
?>