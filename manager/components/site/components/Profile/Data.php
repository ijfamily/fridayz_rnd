<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные для интерфейса компонентов сайта"
 * Пакет контроллеров "Профиль компонента сайта"
 * Группа пакетов     "Компоненты сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SComponents_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные для интерфейса компонентов сайта
 * 
 * @category   Gear
 * @package    GController_SComponents_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SComponents_Profile_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($_tableName)
     *
     * @var array
     */
    public $fields = array('cmp_parent_id', 'cmp_name', 'cmp_class', 'cmp_path', 'cmp_note', 'cmp_alias', 'cmp_hidden');

    /**
     * Первичный ключ таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'cmp_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_components';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_scomponents_grid';

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql дополнительный запрос для получения данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        parent::dataView($sql);

        unset($this->response->data['cmp_parent_id']);
    }

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_profile_update'], $record['cmp_name']);
    }

    /**
     * Событие наступает после успешного удаления данных
     * 
     * @return void
     */
    protected function dataDeleteComplete()
    {
        // соединение с базой данных (т.к. для лога ранее возможно было другое соединение)
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();

        $query = new GDb_Query();
        // обновление потомков дерева
        $sql = 'UPDATE `site_components` SET `cmp_parent_id`=NULL '
             . 'WHERE `cmp_parent_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }
}
?>