<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Create record "Components cms"',
    'title_profile_update' => 'Update record "%s"',
    // поля формы
    'label_cmp_name'   => 'Name',
    'label_pcmp_name'  => 'Name (п)',
    'tip_pcmp_name'    => 'Name &quot;parent&quot; component',
    'label_cmp_class'  => 'Class',
    'label_cmp_path'   => 'Path',
    'label_cmp_note'   => 'Note',
    'label_cmp_alias'  => 'Alias',
    'label_cmp_hidden' => 'Hidden'
);
?>