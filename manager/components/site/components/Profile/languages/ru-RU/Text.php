<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Создание записи "Компоненты CMS"',
    'title_profile_update' => 'Изменение записи "%s"',
    'text_btn_help'        => 'Справка',
    // поля формы
    'label_cmp_name'   => 'Название',
    'label_pcmp_name'  => 'Название (п)',
    'tip_pcmp_name'    => 'Название &quot;предка&quot; компонента',
    'label_cmp_class'  => 'Класс',
    'label_cmp_path'   => 'Путь',
    'label_cmp_note'   => 'Заметка',
    'label_cmp_alias'  => 'Псевдоним',
    'label_cmp_hidden' => 'Скрыть'
);
?>