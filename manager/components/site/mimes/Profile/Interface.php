<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля настройки списка MIMES"
 * Пакет контроллеров "Настройка списка MIMES"
 * Группа пакетов     "Настройка списка MIMES"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SMimes_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля настройки списка MIMES
 * 
 * @category   Gear
 * @package    GController_SMimes_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SMimes_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // Ext_Window (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'           => $this->_['title_profile_update'],
                  'titleEllipsis'   => 40,
                  'width'           => 640,
                  'autoHeight'      => true,
                  'resizable'       => false,
                  'btnDeleteHidden' => true,
                  'stateful'        => false)
        );
        $this->_cmp->state = 'update';

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->url = $this->componentUrl . 'profile/';
        $form->items->addItems(
            array(
                array('xtype' => 'label',
                      'html'  => $this->_['html_desc']),
                array('xtype'     => 'mn-field-grid-editor',
                      'id'        => 'list_mimes',
                      'name'      => 'list_mimes',
                      'hideLabel' => true,
                      'anchor'    => '100%',
                      'height'    => 400,
                      'checkDirty' => false,
                      'fields'    => array('ext', 'type'),
                      'columns'   => array(
                          array('header'    => $this->_['header_ext'],
                                'dataIndex' => 'ext',
                                'width'     => 150,
                                'sortable'  => true),
                          array('header'    => $this->_['header_type'],
                                'dataIndex' => 'type',
                                'width'     => 400,
                                'sortable'  => true),
                      )
                )
            )
        );

        parent::getInterface();
    }
}
?>