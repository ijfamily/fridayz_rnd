<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля страницы"
 * Пакет контроллеров "Профиль страницы"
 * Группа пакетов     "Landing"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Distributed under the Lesser General Public License (LGPL)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    GController_SLandingPages_Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');
Gear::library('Site');

/**
 * Интерфейс профиля страницы
 * 
 * @category   Gear
 * @package    GController_SLandingPages_Text
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SLandingPages_Text_Interface extends GController_Profile_Interface
{
    /**
     * Возращает список компонентов для редактора
     * 
     * @return array
     */
    protected function getEditorComponents()
    {
        if ($items = $this->store->get('editorComponents', false))
            return $items;

        // соединение с базой данных
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();

        $items = GSite::getEditorComponents1();
        $this->store->set('editorComponents', $items);

        return $items;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getEditor()
    {
        switch ($this->config->getFromCms('Site', 'EDITOR/LANDING', 'text')) {
            case 'text':
                return array(
                    'xtype'  => 'textarea',
                    'name'   => 'block_text',
                    'anchor' => '100% 100%',
                    'hideLabel' => true,
                    'allowBlank' => false,
                );

            case 'tinymce':
                return array(
                    'xtype'      => 'mn-tinymce',
                      'id'         => 'htmleditor-landing',
                      'hideLabel'  => true,
                      'name'       => 'block_text',
                      'anchor'     => '100% 100%',
                      'allowBlank' => false,
                      'tinyMCEConfig' => array(
                          'languageId'     => $this->config->getFromCms('Site', 'LANGUAGE/ID'),
                          'language'       => $this->config->getFromCms('Site', 'LANGUAGE'),
                          'uploadUrl'      => $this->uri->scriptName . '/' . $this->componentUrl . 'editor/data/',
                          'componentIcons' => PATH_COMPONENT . 'site/articles/articles/Component/resources/icons/',
                          'components'     => $this->getEditorComponents(),
                          'keep_styles' => false,
                          'allow_html_in_named_anchor' => true,
                          'allow_conditional_comments' => true,
                          'protected' => array(),
                          'cleanup' => false,
                          'verify_html' => false,
                          'cleanup_on_startup' => false,
                          '‘extended_valid_elements' => '*[*]',
                          'schema' => 'html5'
                          
                      )
                );

            case 'htmleditor':
                return array(
                    'xtype'          => 'mn-htmleditor',
                  'id'             => 'htmleditor-landing',
                  'hideLabel'      => true,
                  'name'           => 'block_text',
                  'anchor'         => '100% 100%',
                  'enableLinks'    => false,
                  'xplugins'       => array('Divider', 'Component', 'RemoveFormat', 'HR','Word', 'Table', 'SpecialCharacters', 'Image', 'Link', 'SubSuperScript', 'IndentOutdent', 'HeadingButtons'),
                  'xpluginsConfig' => array(
                      'images'    => array(
                          'alignsList' => array(
                              array('неизвестно', ''), array('по левому краю', 'left'), array('по правому краю', 'right'), 
                              array('по верхнему краю', 'top'), array('по нижниму краю', 'bottom'), array('по центру', 'middle')
                          )
                      ),
                      'languageId'   => 1,
                      'cmpMenuItems' => $this->getEditorComponents(),
                      
                      'Component' => array(
                          'url'      => $this->componentUrl . 'editor/nodes/',
                          'urlApply' => $this->componentUrl . 'combo/trigger/?name=xxx'
                      )
                  ),
                  'uploadUrl'  => $this->uri->scriptName . '/' . $this->componentUrl . 'editor/data/',
                  'allowBlank' => false);
        }
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('id'            => $this->classId,
                  'title'         => $this->_['title_profile_insert'],
                  'titleEllipsis' => 60,
                  'gridId'        => 'gcontroller_slandingpages_grid',
                  'width'         => 950,
                  'height'        => 500,
                  'stateful'      => false,
                  'btnDeleteHidden' => true)
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            $this->getEditor()
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($items);
        $form->url = $this->componentUrl . 'text/';

        parent::getInterface();
    }
}
?>