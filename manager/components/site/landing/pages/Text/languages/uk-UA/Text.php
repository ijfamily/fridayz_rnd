<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Distributed under the Lesser General Public License (LGPL)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Створення запису "Слайдер"',
    'title_profile_update' => 'Зміна запису"%s"',
    // поля формы
    'label_slider_index'       => 'Iндекс',
    'tip_slider_index'         => 'Порядковий номер (для формування списків)',
    'label_slider_name'        => 'Назва',
    'label_slider_description' => 'Опис',
    'label_article_name'       => 'Стаття',
    'tip_article_name'         => 'Стаття в якій буде розміщений слайдер'
);
?>