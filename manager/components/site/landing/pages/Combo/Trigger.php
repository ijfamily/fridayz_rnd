<?php
/**
 * Gear Manager
 *
 * Контроллер         "Триггер выпадающего списка для обработки данных"
 * Пакет контроллеров "Триггер"
 * Группа пакетов     "Landing"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Distributed under the Lesser General Public License (LGPL)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    GController_SLandingPages_Combo
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Combo/Trigger');

/**
 * Триггер выпадающего списка для обработки данных
 * 
 * @category   Gear
 * @package    GController_SLandingPages_Combo
 * @subpackage Trigger
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SLandingPages_Combo_Trigger extends GController_Combo_Trigger
{
    /**
     * Возращает список статей
     * 
     * @return void
     */
    protected function queryArticles()
    {
        $languageId = $this->config->getFromCms('Site', 'LANGUAGE/ID');
        $table = new GDb_Table('site_articles', 'article_id');
        $table->setStart($this->_start);
        $table->setLimit($this->_limit);
        $sql = 'SELECT SQL_CALC_FOUND_ROWS `a`.`article_id`, `p`.`page_header` '
             . 'FROM `site_articles` `a` JOIN `site_pages` `p` ON `p`.`article_id`=`a`.`article_id` AND `p`.`language_id`=' . $languageId
             . ' WHERE 1 '. ($this->_query ? " AND `page_header` LIKE '{$this->_query}%' " : '')
             . 'ORDER BY `page_header` LIMIT %limit';
        if ($table->query($sql) === false)
            throw new GSqlException();
        $data = array();
        while (!$table->query->eof()) {
            $record = $table->query->next();
            $data[] = array('id' => $record['article_id'], 'name' => $record['page_header']);
        }
        $this->response->set('totalCount', $table->query->getFoundRows());
        $this->response->success = true;
        $this->response->data = $data;
    }

    /**
     * Возращает список блоков
     * 
     * @return void
     */
    protected function queryBlocks()
    {
        $articleId = $this->store->get('record', 0, 'gcontroller_sarticles_grid');
        if (empty($articleId))
            throw new GException('Error', $this->_['msg_select_article']);

        $table = new GDb_Table('site_landing_blocks', 'item_id');
        $table->setStart($this->_start);
        $table->setLimit($this->_limit);
        $sql = 'SELECT SQL_CALC_FOUND_ROWS * FROM `site_landing_blocks` '
             . 'WHERE `block_id` NOT IN (SELECT `block_id` FROM `site_landing_pages` WHERE `article_id`=' . $articleId . ') '
             . 'ORDER BY `block_name` LIMIT %limit';
        if ($table->query($sql) === false)
            throw new GSqlException();
        $data = array();
        while (!$table->query->eof()) {
            $record = $table->query->next();
            $data[] = array('id' => $record['block_id'], 'name' => $record['block_name']);
        }
        $this->response->set('totalCount', $table->query->getFoundRows());
        $this->response->success = true;
        $this->response->data = $data;
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $triggerName название триггера
     * @return void
     */
    protected function dataView($triggerName)
    {
        parent::dataView($triggerName);

        $isString = $this->uri->getVar('string', false);
        // имя триггера
        switch ($triggerName) {
            // статьи
            case 'articles': $this->queryArticles(); break;

            // блоки
            case 'blocks': $this->queryBlocks(); break;
        }
    }
}
?>