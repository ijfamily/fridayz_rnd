<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные интерфейса профиля страницы"
 * Пакет контроллеров "Профиль страницы"
 * Группа пакетов     "Landing"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Distributed under the Lesser General Public License (LGPL)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    GController_SLandingPages_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');
Gear::library('Landing');

/**
 * Данные интерфейса профиля страницы
 * 
 * @category   Gear
 * @package    GController_SLandingPages_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SLandingPages_Profile_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array('block_comment', 'block_index', 'block_id', 'article_id');

    /**
     * Первичный ключ таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'item_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_landing_pages';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sarticles_grid';

    /**
     * Идентификатор статьи
     *
     * @var integer
     */
    protected $_articleId = 0;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // едент. статьи
        $this->_articleId = $this->input->get('article_id', 0);
    }

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_profile_update'], $record['block_name']);
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        $sql = 'SELECT * FROM `site_landing_pages` `p` JOIN `site_landing_blocks` `b` USING(`block_id`) WHERE `p`.`item_id`=' . $this->uri->id;

        parent::dataView($sql);
    }

    /**
     * Событие наступает после успешного добавления данных
     * 
     * @param array $data сформированные данные полученные после запроса к таблице
     * @return void
     */
    protected function dataInsertComplete($data = array())
    {
        $query = new GDb_Query();
        // обновить статью
        $sql = 'UPDATE `site_articles` SET `article_landing`=1 WHERE `article_id`=' . $this->_articleId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        // если состояние формы "правка"
        if ($this->isUpdate) return;

        $params['block_index'] = GLanding::getLastBlockIndex($this->_articleId) + 1;
        //$params['article_id'] = $this->_article['article_id'];
    }
}
?>