<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Distributed under the Lesser General Public License (LGPL)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Добавление блока на страницу',
    'title_profile_update' => 'Правка - %s',
    'text_btn_help'        => 'Справка',
    // поля формы
    'label_block_name'    => 'Название',
    'label_block_comment' => 'Комментарий',
    // сообщения
    'msg_select_article' => 'Вы не выбрали страницу!'
);
?>