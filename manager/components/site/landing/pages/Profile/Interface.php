<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля страницы"
 * Пакет контроллеров "Профиль страницы"
 * Группа пакетов     "Landing"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Distributed under the Lesser General Public License (LGPL)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    GController_SLandingPages_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля страницы
 * 
 * @category   Gear
 * @package    GController_SLandingPages_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SLandingPages_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'titleEllipsis' => 50,
                  'gridId'        => 'gcontroller_slandingpages_grid',
                  'width'         => 400,
                  'height'        => $this->isUpdate ? 100 : 130,
                  'stateful'      => false,
                  'resizable'     => false,
                  'btnDeleteHidden' => true,
                  'buttonsAdd'    => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-site-landing',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );

        // поля формы (ExtJS class "Ext.Panel")
        // если состояние формы "правка"
        if ($this->isUpdate) {
            $items = array(
                array('xtype'      => 'textfield',
                      'fieldLabel' => $this->_['label_block_comment'],
                      'name'       => 'block_comment',
                      'anchor'     => '100%',
                      'maxLength'  => 100,
                      'allowBlank' => true)
            );
        } else {
            $items = array(
                array('xtype'      => 'mn-field-combo',
                      'fieldLabel' => $this->_['label_block_name'],
                      'name'       => 'block_id',
                      'editable'   => false,
                      'anchor'     => '100%',
                      'hiddenName' => 'block_id',
                      'allowBlank' => false,
                      'pageSize'   => 150,
                      'store'      => array(
                          'xtype' => 'jsonstore',
                          'url'   => $this->componentUrl . 'combo/trigger/?name=blocks'
                      )
                ),
                array('xtype'      => 'textfield',
                      'fieldLabel' => $this->_['label_block_comment'],
                      'name'       => 'block_comment',
                      'anchor'     => '100%',
                      'maxLength'  => 100,
                      'allowBlank' => true)
            );
        }
        $items[] = array(
            array('xtype' => 'hidden',
                  'name'  => 'article_id',
                  'value' => $this->store->get('record', 0, 'gcontroller_sarticles_grid'))
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($items);
        $form->labelWidth = 90;
        $form->url = $this->componentUrl . 'profile/';

        parent::getInterface();
    }
}
?>