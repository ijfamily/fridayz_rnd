<?php
/**
 * Gear Manager
 *
 * Контроллер         "Изменение записи профиля страницы"
 * Пакет контроллеров "Страницы"
 * Группа пакетов     "Landing"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Distributed under the Lesser General Public License (LGPL)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    GController_SLandingPages_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Field.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Field');
Gear::library('Landing');

/**
 * Изменение записи профиля страницы
 * 
 * @category   Gear
 * @package    GController_SLandingPages_Profile
 * @subpackage Field
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Field.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SLandingPages_Profile_Field extends GController_Profile_Field
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array('block_visible');

    /**
     * Первичный ключ таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'item_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_landing_pages';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_slandingpages_grid';

    /**
     * Страница лендинга
     *
     * @var mixed
     */
    protected $_article = false;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        $this->_article = $this->store->get('tlbFilter', false, 'gcontroller_slandingpages_grid');
    }

    /**
     * Событие наступает после успешного обновления данных
     * 
     * @param array $data сформированные данные полученные после запроса к таблице
     * @return void
     */
    protected function dataUpdateComplete($data = array())
    {
        if (isset($data['block_visible'])) {
            // атрибуты компонентов установленные через инспектор компонентов
            $componentsAttr = $this->store->get('components', array(), 'gcontroller_sarticles_grid');
            // сборка страницы
            GLanding::collectPage($this->_article, $componentsAttr);
        }
    }
}
?>