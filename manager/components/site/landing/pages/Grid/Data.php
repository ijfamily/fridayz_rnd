<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка страниц"
 * Пакет контроллеров "Список страниц"
 * Группа пакетов     "Landing"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Distributed under the Lesser General Public License (LGPL)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    GController_SLandingPages_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');
Gear::library('Landing');

/**
 * Данные списка страниц
 * 
 * @category   Gear
 * @package    GController_SLandingPages_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SLandingPages_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'item_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_landing_pages';

    /**
     * Идентификатор статьи
     *
     * @var integer
     */
    protected $_articleId = 0;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // идент. статьи
        $this->_articleId = $this->store->get('record', 0, 'gcontroller_sarticles_grid');

        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS `p`.*, `b`.* FROM `site_landing_pages` `p` LEFT JOIN `site_landing_blocks` `b` USING(`block_id`) '
          . 'WHERE `p`.`article_id`=' . $this->_articleId . ' %filter ORDER BY %sort LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // порядок
            'block_index' => array('type' => 'integer'),
            // показывать вниз
            'block_down' => array('type' => 'integer'),
            // показывать вверх
            'block_up' => array('type' => 'integer'),
            // название
            'block_name' => array('type' => 'string'),
            // описание
            'block_description' => array('type' => 'string'),
            // комментарий
            'block_comment' => array('type' => 'string'),
            // схема
            'block_image' => array('type' => 'string'),
            // показывать
            'block_visible' => array('type' => 'integer')
        );
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        if (empty($this->_article['article_id']))
            throw new GException('Error', $this->_['msg_select_article']);

        GLanding::removePage($this->_article['article_id']);
    }

    /**
     * Событие наступает после успешного удаления данных
     * 
     * @return void
     */
    protected function dataDeleteComplete()
    {
        $query = new GDb_Query();
        // количесто блоков
        $count = GLanding::updatePage($this->_article['article_id'], $query);
        // если нет блоков
        if ($count == 0) {
            $sql = 'UPDATE `site_articles` SET `article_landing`=0 WHERE `article_id`=' . $this->_article['article_id'];
            if ($query->execute($sql) === false)
                throw new GSqlException();
        }

        // атрибуты компонентов установленные через инспектор компонентов
        $componentsAttr = $this->store->get('components', array(), 'gcontroller_sarticles_grid');
        // сборка страницы
        GLanding::collectPage($this->_article, $componentsAttr);
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        // если есть обозначение
        if (!empty($record['block_image']))
            $record['block_image'] = '<img src="' . $this->resourcePath . '/blocks/' . $record['block_image'] . '" style="border:1px solid #a2b8c6;"/>';
        $record['block_up'] = $record['block_index'] != 1;
        $record['block_down'] = $this->countRecords != $record['block_index'];
        // комментарий
        if (!empty($record['block_comment']))
            $record['block_name'] = $record['block_name'] . '<div style="font-style:italic">' . $record['block_comment'] . '</div>';

        return $record;
    }
}
?>