<?php
/**
 * Gear Manager
 *
 * Контроллер         "Перемещение блоков"
 * Пакет контроллеров "Список страниц"
 * Группа пакетов     "Landing"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Distributed under the Lesser General Public License (LGPL)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    GController_SLandingPages_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Move.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');
Gear::library('Landing');

/**
 * Перемещение блоков
 * 
 * @category   Gear
 * @package    GController_SLandingPages_Grid
 * @subpackage Move
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Move.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SLandingPages_Grid_Move extends GController_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'item_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_landing_pages';

    /**
     * Страница лендинга
     *
     * @var mixed
     */
    protected $_article = false;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        $this->_article = $this->store->get('tlbFilter', false, 'gcontroller_slandingpages_grid');
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataMove()
    {
        $this->dataAccessView();

        // действие
        $action = $this->input->get('action');
        $query = new GDb_Query();
        $sql = 'SELECT * FROM `site_landing_pages` WHERE `item_id`=' . (int) $this->uri->id;
        if (($block = $query->getRecord($sql)) === false)
            throw new GSqlException();
        if (empty($block))
            throw new GException('Error', 'Selected record was deleted!');
        // предыдущий блок
        $sql = 'SELECT * FROM `site_landing_pages` WHERE `article_id`=' . $block['article_id'] . ' AND `block_index`=';
        switch ($action) {
            case 'moveUp': $bIndex = $block['block_index'] - 1; break;
            case 'moveDown': $bIndex =$block['block_index'] + 1; break;
        }
        $sql .= $bIndex;
        if (($prev = $query->getRecord($sql)) === false)
            throw new GSqlException();
        if (empty($prev)) return;
        // перемещение блоков
        $sql = 'UPDATE `site_landing_pages` SET `block_index`=' . $prev['block_index'] . ' WHERE `item_id`=' . (int) $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'UPDATE `site_landing_pages` SET `block_index`=' . $block['block_index'] . ' WHERE `item_id`=' . $prev['item_id'];
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // атрибуты компонентов установленные через инспектор компонентов
        $componentsAttr = $this->store->get('components', array(), 'gcontroller_sarticles_grid');
        // сборка страницы
        GLanding::collectPage($this->_article, $componentsAttr);
    }

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        if ($this->uri->action == 'move')
            // метод запроса
            switch ($this->uri->method) {
                // метод "POST"
                case 'POST': $this->dataMove(); return;
            }

        parent::init();
    }
}
?>