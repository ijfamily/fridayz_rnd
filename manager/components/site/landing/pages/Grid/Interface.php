<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка страниц"
 * Пакет контроллеров "Список страниц"
 * Группа пакетов     "Landing"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Distributed under the Lesser General Public License (LGPL)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    GController_SLandingPages_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс списка страниц
 * 
 * @category   Gear
 * @package    GController_SLandingPages_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SLandingPages_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'item_id';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sarticles_grid';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'block_index';

    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        parent::getDataInterface();

        $query = new GDb_Query();
        $languageId = $this->config->getFromCms('Site', 'LANGUAGE/ID');
        // выбранная статья
        $sql = 'SELECT * FROM `site_pages` WHERE `language_id`=' . $languageId . ' AND `article_id`=' . (int)$this->uri->id;
        if (($record = $query->getRecord($sql)) === false)
            throw new GSqlException();
        if (!empty($record['page_header']))
            $data['title'] = $record['page_header'];
        else
            $data['title'] = $this->_['title_grid'];

        return $data;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // порядок
            array('name' => 'block_index', 'type' => 'integer'),
            // показывать вниз
            array('name' => 'block_down', 'type' => 'integer'),
            // показывать вверх
            array('name' => 'block_up', 'type' => 'integer'),
            // название
            array('name' => 'block_name', 'type' => 'string'),
            // описание
            array('name' => 'block_description', 'type' => 'string'),
            // комментарий
            array('name' => 'block_comment', 'type' => 'string'),
            // схема
            array('name' => 'block_image', 'type' => 'string'),
            // показывать
            array('name' => 'block_visible', 'type' => 'integer')
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $this->columns = array(
            array('xtype'     => 'numbercolumn'),
            array('dataIndex' => 'block_index',
                  'header'    => $this->_['header_block_index'],
                  'width'     => 45,
                  'sortable'  => false,
                  'hidden'    => true),
            array('dataIndex' => 'block_image',
                  'cls'       => 'mn-bg-color-white',
                  'header'    => $this->_['header_block_image'],
                  'width'     => 454,
                  'sortable'  => false,
                  'resizable' => false),
            array('xtype'     => 'actioncolumn',
                  'dataIndex' => 'block_index',
                  'enabledIndex' => 'block_up',
                  'gridId'    => $this->classId,
                  'url'       => $this->componentUrl . 'grid/move/',
                  'icon'      => $this->resourcePath . 'icon-arrow-up.png',
                  'iconDisabled' => $this->resourcePath . 'icon-arrow-up-d.png',
                  'action'    => 'moveUp',
                  'tooltip'   => $this->_['tooltip_move_up'],
                  'header'    => '&nbsp;',
                  'width'     => 55,
                  'sortable'  => false),
            array('xtype'     => 'actioncolumn',
                  'dataIndex' => 'block_index',
                  'enabledIndex' => 'block_down',
                  'gridId'    => $this->classId,
                  'url'       => $this->componentUrl . 'grid/move/',
                  'icon'      => $this->resourcePath . 'icon-arrow-down.png',
                  'iconDisabled' => $this->resourcePath . 'icon-arrow-down-d.png',
                  'action'    => 'moveDown',
                  'tooltip'   => $this->_['tooltip_move_down'],
                  'header'    => '&nbsp;',
                  'width'     => 55,
                  'sortable'  => false),
            array('xtype'     => 'booleancolumn',
                  'align'     => 'center',
                  'clsAction' => 'mn-grid-icon-top',
                  'dataIndex' => 'block_visible',
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-visible.png" align="absmiddle">',
                  'tooltip'   => $this->_['tooltip_block_visible'],
                  'align'     => 'center',
                  'width'     => 45,
                  'sortable'  => true,
                  'resizable' => false,
                  'filter'    => array('type' => 'boolean')),
            array('xtype'     => 'widgetcolumn',
                  'dataIndex' => 'item_id',
                  'icon'      => $this->resourcePath . 'icon-html.png',
                  'url'       => $this->componentUrl . 'text/interface/'),
            array('xtype'     => 'widgetcolumn',
                  'dataIndex' => 'item_id',
                  'icon'      => $this->resourcePath . 'icon-form.png',
                  'url'       => $this->componentUrl . 'profile/interface/'),
            array('dataIndex' => 'block_name',
                  'header'    => $this->_['header_block_name'],
                  'width'     => 200,
                  'sortable'  => false,
                  'filter'    => array('type' => 'string'))
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $data['title'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_grid'],
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'profileUrl'    => $this->componentUrl . 'profile/')
        );

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "edit" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "добавить" (ExtJS class "Manager.button.InsertData")
        $group->items->add(array('xtype' => 'mn-btn-insert-data', 'gridId' => $this->classId));
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' => $this->classId));
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'      => 'mn-btn-clear-data',
                  'msgConfirm' => $this->_['msg_btn_clear'],
                  'gridId'     => $this->classId,
                  'isN'        => true)
        );
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = 'component-site-landing';
        $this->_cmp->tbar->items->add($group);
        // группа кнопок "фильтр" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Filter(
            array('title'  => 'Фильтр',
                  'gridId' => $this->classId)
        );


        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $this->_cmp->cellTips = array(
            array('field' => 'block_name', 'tpl' => '{block_name}')
        );

        parent::getInterface();
    }
}
?>