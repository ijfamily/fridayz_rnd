<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Distributed under the Lesser General Public License (LGPL)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'    => 'Целевая страница',
    'tooltip_grid'  => 'Целевая страница - веб-страница, построенная определенным образом, основной задачей которой является сбор контактных данных целевой аудитории',
    'rowmenu_edit'  => 'Редактировать',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'label_article_name'       => 'Страница',
    'tip_article_name'         => 'Страница в которой будут размещены блоки ',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete"> ("Landing страницы")</span> ?',
    // столбцы
    'header_block_index'       => '№',
    'header_block_name'        => 'Название',
    'header_block_description' => 'Описание',
    'header_block_image'       => 'Схема',
    'tooltip_move_up'          => 'Переместить блок вверх',
    'tooltip_move_down'        => 'Переместить блок вниз',
    'tooltip_block_visible'    => 'Показать блок',
    // сообщения
    'msg_select_article' => 'Вы не выбрали страницу!'
);
?>