<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Distributed under the Lesser General Public License (LGPL)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'    => 'Слайдери',
    'rowmenu_edit'  => 'Редагувати',
    'rowmenu_image' => 'Зображення',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Стовпці',
    'title_buttongroup_filter' => 'Фільтр',
    'msg_btn_clear'            => 'Ви дійсно бажаєте видалити всі записи <span class="mn-msg-delete"> '
                                . '("Слайдеры", "Изображения")</span> ?',
    // столбцы
    'header_slider_index'       => '№',
    'tooltip_slider_index'      => 'Порядковий номер слайдера',
    'header_article_note'       => 'Стаття',
    'tooltip_article_note'      => 'Примітка статті',
    'header_slider_name'        => 'Назва',
    'header_slider_description' => 'Опис',
    'header_image_count'        => 'Зображень'
);
?>