<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Distributed under the Lesser General Public License (LGPL)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Пошук у списку "Слайдери"',
    // поля
    'header_slider_index'       => '№',
    'header_article_note'       => 'Стаття',
    'header_slider_name'        => 'Назва',
    'header_slider_description' => 'Опис',
    'header_image_count'        => 'Зображень'
);
?>