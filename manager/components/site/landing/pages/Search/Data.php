<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск страниц"
 * Пакет контроллеров "Страницы"
 * Группа пакетов     "Landing"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Distributed under the Lesser General Public License (LGPL)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    GController_SLandingPages_Search
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск страниц
 * 
 * @category   Gear
 * @package    GController_SLandingPages_Search
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SLandingPages_Search_Data extends GController_Search_Data
{
    /**
     * дентификатор компонента (списка) к которому применяется поиск
     *
     * @var string
     */
    public $gridId = 'gcontroller_slandingpages_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_slandingpages_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор
     * Используется для инициализации атрибутов контроллер не переданного в конструктор
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'block_name', 'label' => $this->_['header_block_name']),
        );
    }

    /**
     * Применение фильтра toolbar
     * 
     * @return void
     */
    protected function toolbarAccept()
    {
        if ($this->input->get('action') == 'search') {
            // если попытка сбросить фильтр
            if ($this->input->isEmptyPost(array('article_id'))) return;
            // язык поумолчанию
            $languageId = $this->config->getFromCms('Site', 'LANGUAGE/ID');
            $query = new GDb_Query();
            $sql = 'SELECT `p`.*, `a`.* '
                 . 'FROM `site_articles` `a` JOIN `site_pages` `p` ON `p`.`article_id`=`a`.`article_id` AND '
                 . '`p`.`language_id`=' . $languageId . ' AND `a`.`article_id`=' . (int) $this->input->get('article_id');
            if (($article = $query->getRecord($sql)) === false)
                throw new GSqlException();
            if (empty($article))
                throw new GException('Warning', $this->_['msg_select_article']);

            $this->store->set('tlbFilter', $article);
        }
    }
}
?>