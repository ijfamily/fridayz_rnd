<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск изображений в слайдере"
 * Пакет контроллеров "Изображения слайдера"
 * Группа пакетов     "Слайдеры"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SSliderImages_Search
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск изображений в слайдере
 * 
 * @category   Gear
 * @package    GController_SSliderImages_Search
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SSliderImages_Search_Data extends GController_Search_Data
{
    /**
     * дентификатор компонента (списка) к которому применяется поиск
     *
     * @var string
     */
    public $gridId = 'gcontroller_ssliderimages_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ssliders_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор
     * Используется для инициализации атрибутов контроллер не переданного в конструктор
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'image_index', 'label' => $this->_['header_image_index']),
            array('dataIndex' => 'image_title', 'label' => $this->_['header_image_title']),
            array('dataIndex' => 'image_title_1', 'label' => $this->_['header_image_title_1']),
            array('dataIndex' => 'image_entire_filename', 'label' => $this->_['header_image_entire_filename']),
            array('dataIndex' => 'image_entire_resolution', 'label' => $this->_['header_image_entire_resolution']),
            array('dataIndex' => 'image_entire_filesize', 'label' => $this->_['header_image_entire_filesize']),
            array('dataIndex' => 'image_visible', 'label' => $this->_['header_image_visible']),
        );
    }
}
?>