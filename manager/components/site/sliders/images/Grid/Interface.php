<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка изображений слайдера"
 * Пакет контроллеров "Изображения слайдера"
 * Группа пакетов     "Слайдеры"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SSliderImages_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс списка изображений слайдера
 * 
 * @category   Gear
 * @package    GController_SSliderImages_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SSliderImages_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'image_id';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'image_index';

    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        parent::getDataInterface();

        $query = new GDb_Query();
        // выбранная статья
        $sql = 'SELECT `slider_name` FROM `site_sliders` `a` WHERE `slider_id`=' . (int)$this->uri->id;
        if (($record = $query->getRecord($sql)) === false)
            throw new GSqlException();
        $data['title'] = sprintf($this->_['title_grid'], $record['slider_name']);

        return $data;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // индекс изображения
            array('name' => 'image_index', 'type' => 'string'),
            // файл изображения
            array('name' => 'image_entire_filename', 'type' => 'string'),
            // разрешение изображения
            array('name' => 'image_entire_resolution', 'type' => 'string'),
            // размер файла изображения
            array('name' => 'image_entire_filesize', 'type' => 'string'),
            // заглавие изображения
            array('name' => 'image_title', 'type' => 'string'),
            // заглавие изображения 1
            array('name' => 'image_title_1', 'type' => 'string'),
            // показывать изображение
            array('name' => 'image_visible', 'type' => 'integer')
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $this->columns = array(
            array('xtype'     => 'expandercolumn',
                  'url'       => $this->componentUrl . 'grid/expand/',
                  'typeCt'    => 'cell'),
            array('xtype'     => 'numbercolumn'),
            array('xtype'     => 'rowmenu'),
            array('dataIndex' => 'image_index',
                  'header'    => $this->_['header_image_index'],
                  'tooltip'   => $this->_['tooltip_image_index'],
                  'width'     => 45,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'image_title',
                  'header'    => $this->_['header_image_title'],
                  'tooltip'   => $this->_['tooltip_image_title'],
                  'width'     => 170,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'image_title_1',
                  'header'    => $this->_['header_image_title_1'],
                  'tooltip'   => $this->_['tooltip_image_title_1'],
                  'width'     => 170,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'image_entire_filename',
                  'id'        => 'image-file',
                  'header'    => '<em class="mn-grid-hd-details"></em>' . $this->_['header_image_entire_filename'],
                  'tooltip'   =>$this->_['tooltip_image_entire_filename'],
                  'width'     => 190,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'image_entire_resolution',
                  'header'    =>$this->_['header_image_entire_resolution'],
                  'tooltip'   =>$this->_['tooltip_image_entire_resolution'],
                  'width'     => 110,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'image_entire_filesize',
                  'header'    =>$this->_['header_image_entire_filesize'],
                  'tooltip'   =>$this->_['tooltip_image_entire_filesize'],
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('xtype'     => 'booleancolumn',
                  'align'     => 'center',
                  'cls'       => 'mn-bg-color-gray2',
                  'dataIndex' => 'image_visible',
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-visible.png" align="absmiddle">',
                  'tooltip'   => $this->_['tooltip_image_visible'],
                  'width'     => 40,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean')),
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $data['title'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'profileUrl'    => $this->componentUrl . 'profile/')
        );

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "edit" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "добавить" (ExtJS class "Manager.button.InsertData")
        $group->items->add(array('xtype' => 'mn-btn-insert-data', 'gridId' => $this->classId));
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' => $this->classId));
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'      => 'mn-btn-clear-data',
                  'msgConfirm' => $this->_['msg_btn_clear'],
                  'gridId'     => $this->classId,
                  'isN'        => true)
        );
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = 'component-site-sliders';
        $this->_cmp->tbar->items->add($group);

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $cellInfo =
            '<div class="mn-grid-cell-tooltip-tl">{image_entire_filename}</div>'
          . '<div class="mn-grid-cell-tooltip">'
          . '<em>' . $this->_['header_image_index'] . '</em>: <b>{image_index}</b><br>'
          . '<em>' . $this->_['header_image_title'] . '</em>: <b>{image_title}</b><br>'
          . '<em>' . $this->_['header_image_title_1'] . '</em>: <b>{image_title_1}</b><br>'
          . '<em>' . $this->_['header_image_entire_resolution'] . '</em>: <b>{image_entire_resolution}</b><br>'
          . '<em>' . $this->_['header_image_entire_filesize'] . '</em>: <b>{image_entire_filesize}</b><br>'
          . '</div>';
        $this->_cmp->cellTips = array(
            array('field' => 'image_title', 'tpl' => '{image_title}'),
            array('field' => 'image_title_1', 'tpl' => '{image_title_1}'),
            array('field' => 'image_entire_filename', 'tpl' => $cellInfo)
        );

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $items = array(
            array('text'    => $this->_['rowmenu_edit'],
                  'iconCls' => 'icon-form-edit',
                  'url'     => $this->componentUrl . 'profile/interface/'),
            array('xtype'   => 'menuseparator'),
            array('text'    => $this->_['rowmenu_file'],
                  'icon'    => $this->resourcePath . 'icon-rename.png',
                  'url'     => $this->componentUrl . 'rename/interface/')
        );
        $this->_cmp->rowMenu = array('xtype' => 'mn-grid-rowmenu', 'items' => $items);

        parent::getInterface();
    }
}
?>