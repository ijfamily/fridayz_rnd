<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'        => 'Images of slider "%s"',
    'rowmenu_edit'      => 'Edit',
    'rowmenu_file'      => 'Rename',
    // панель управления
    'title_buttongroup_edit'   => 'Edit',
    'title_buttongroup_cols'   => 'Columns',
    'title_buttongroup_filter' => 'Filter',
    'msg_btn_clear'            => 'Do you really want to delete all records <span class="mn-msg-delete">("Images")</span> ?',
    // столбцы
    'header_image_index'              => '№',
    'tooltip_image_index'             => 'The index number of the image',
    'header_image_title'              => 'Text',
    'tooltip_image_title'             => 'Text to describe the image',
    'header_image_visible'            => 'Show',
    'tooltip_image_visible'           => 'Show image',
    'header_image_archive'            => 'Archive',
    'tooltip_image_archive'           => 'Image in acrchive',
    'header_image_entire_filename'    => 'File (i)',
    'tooltip_image_entire_filename'   => 'Image file',
    'header_image_entire_resolution'  => 'Resolution (i)',
    'tooltip_image_entire_resolution' => 'Image resolution in px.',
    'header_image_entire_type'        => 'Type (i)',
    'tooltip_image_entire_type'       => 'Image type (JPEG, PNG, GIF)',
    'header_image_entire_filesize'    => 'Size (i)',
    'tooltip_image_entire_filesize'   => 'Size of file image',
    'header_languages_name'           => 'Language'
);
?>