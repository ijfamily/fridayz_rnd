<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'        => 'Слайды "%s"',
    'rowmenu_edit'      => 'Редактировать',
    'rowmenu_file'      => 'Переименовать',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">("Все слайды")</span> ?',
    // столбцы
    'header_image_index'              => '№',
    'tooltip_image_index'             => 'Порядковый номер изображения',
    'header_image_title'              => 'Заглавие',
    'tooltip_image_title'             => 'Заглавие в изображении слайдера',
    'header_image_title_1'            => 'Заглавие 1',
    'tooltip_image_title_1'           => 'Заглавие 1 в изображении слайдера',
    'header_image_entire_filename'    => 'Файл изображения',
    'tooltip_image_entire_filename'   => 'Файл изображения',
    'header_image_entire_resolution'  => 'Разрешение',
    'tooltip_image_entire_resolution' => 'Разрешение изображения в пкс.',
    'header_image_entire_filesize'    => 'Размер',
    'tooltip_image_entire_filesize'   => 'Размер файла изображения',
    'header_image_visible'            => 'Показывать',
    'tooltip_image_visible'           => 'Показывать изображения',
);
?>