<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'        => 'Зображення слайдера "%s"',
    'rowmenu_edit'      => 'Редагувати',
    'rowmenu_file'      => 'Перейменувати',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Стовпці',
    'title_buttongroup_filter' => 'Фільтр',
    'msg_btn_clear'            => 'Ви дійсно бажаєте видалити всі записи <span class="mn-msg-delete">("зображення")</span> ?',
    // столбцы
    'header_image_index'              => '№',
    'tooltip_image_index'             => 'Порядковий номер зображення',
    'header_image_title'              => 'Текст',
    'tooltip_image_title'             => 'Текст для опису зображення',
    'header_image_visible'            => 'Показувати',
    'tooltip_image_visible'           => 'Показувати зображення',
    'header_image_archive'            => 'Архів',
    'tooltip_image_archive'           => 'Зображення в архіві',
    'header_image_entire_filename'    => 'Файл (и)',
    'tooltip_image_entire_filename'   => 'Файл зображення',
    'header_image_entire_resolution'  => 'Дозвіл (и)',
    'tooltip_image_entire_resolution' => 'Дозвіл зображення в пкс.',
    'header_image_entire_type'        => 'Тип (и)',
    'tooltip_image_entire_type'       => 'Тип зображення (JPEG, PNG, GIF)',
    'header_image_entire_filesize'    => 'Розмір (и)',
    'tooltip_image_entire_filesize'   => 'Розмір файлу зображення',
    'header_languages_name'           => 'Мова'
);
?>