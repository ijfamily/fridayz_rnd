<?php
/**
 * Gear Manager
 *
 * Контроллер         "Развёрнутая запись изображения"
 * Пакет контроллеров "Изображения слайдера"
 * Группа пакетов     "Слайдеры"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SSliderImages_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Expand');

/**
 * Развёрнутая запись изображения
 * 
 * @category   Gear
 * @package    GController_SSliderImages_Grid
 * @subpackage Expand
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SSliderImages_Grid_Expand extends GController_Grid_Expand
{
    /**
     * Столбцы (id => field)
     *
     * @var array
     */
    public $columns = array('image-file' => 'image_entire_filename');

    /**
     * Каталог данных сайта
     *
     * @var string
     */
    public $pathData = '../data/sliders/';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ssliders_grid';

    /**
     * Возращает SQL запрос
     * 
     * @return string
     */
    protected function getQuery()
    {
        return 'SELECT * FROM `site_slider_images` WHERE `image_id`=' . $this->uri->id;
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        if (!empty($record['image-file']))
            if (file_exists($this->pathData . $record['image-file']))
                $record['image-file'] = '<span mn:bar="download,picture'
                                       . '" class="mn-img mn-img-xl'
                                       . '" mn:image-thumb="/data/sliders/' . $record['image-file']
                                       . '" mn:image-full="/data/sliders/' . $record['image-file'] . '"></span>';
            else
                $record['image-file'] = '';

        return $record;
    }


}
?>