<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные интерфейса списка изображений слайдера"
 * Пакет контроллеров "Изображения слайдера"
 * Группа пакетов     "Слайдеры"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SSliderImages_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('File');
Gear::controller('Grid/Data');

/**
 * Данные интерфейса списка изображений слайдера
 * 
 * @category   Gear
 * @package    GController_SSliderImages_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SSliderImages_Grid_Data extends GController_Grid_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'image_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_slider_images';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ssliders_grid';

    /**
     * Каталог данных сайта
     *
     * @var string
     */
    public $pathData = 'data/sliders/';

    /**
     * Идентификатор слайдера
     *
     * @var integer
     */
    protected $_sliderId;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // идентификатор слайдера
        $this->_sliderId = $this->store->get('record', 0, 'gcontroller_ssliderimages_grid');
        // каталог загрузки изображений
        $this->_uploadPath = '../' . $this->pathData;

        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS * FROM `site_slider_images` WHERE `slider_id`=' . $this->_recordId . ' %filter '
          . 'ORDER BY %sort LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // индекс изображения
            'image_index' => array('type' => 'string'),
            // файл изображения
            'image_entire_filename' => array('type' => 'string'),
            // разрешение изображения
            'image_entire_resolution' => array('type' => 'string'),
            // размер файла изображения
            'image_entire_filesize' => array('type' => 'string'),
            // заглавие изображения
            'image_title' => array('type' => 'string'),
            // заглавие 1 изображения
            'image_title_1' => array('type' => 'integer'),
            // показывать изображение
            'image_visible' => array('type' => 'integer')
        );
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        $query = new GDb_Query();
        // удаление записей таблицы "site_slider_images" (файлы изображений)
        $sql = 'SELECT * FROM `site_slider_images` WHERE `slider_id`=' . $this->_sliderId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        while (!$query->eof()) {
            $image = $query->next();
            // если есть изображение
            if ($image['image_entire_filename'])
                GFile::delete($this->_uploadPath . $image['image_entire_filename']);
        }
        // удаление изображений
        $sql = 'DELETE FROM `site_slider_images` WHERE `slider_id`=' . $this->_sliderId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_slider_images') === false)
            throw new GSqlException();
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Событие наступает после успешного удаления данных
     * 
     * @return void
     */
    protected function dataDeleteComplete()
    {
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        // изображения слайдера
        $query = new GDb_Query();
        $sql = 'SELECT * FROM `site_slider_images` WHERE `image_id` IN (' . $this->uri->id. ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        while (!$query->eof()) {
            $image = $query->next();
            // если есть изображение
            if ($image['image_entire_filename'])
                GFile::delete($this->_uploadPath . $image['image_entire_filename']);
        }
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        
        // если файл изображения не существует
        if (!empty($record['image_entire_filename']))
            if (!file_exists($this->_uploadPath . $record['image_entire_filename']))
                $record['image_entire_filename'] = '<span style="color:#eeacaf">' . $record['image_entire_filename'] . '</span>';

        return $record;
    }
}
?>