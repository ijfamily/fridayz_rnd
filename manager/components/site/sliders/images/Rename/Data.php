<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные интерфейса изменения имени файла слайдера"
 * Пакет контроллеров "Изображения слайдера"
 * Группа пакетов     "Слайдеры"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SSliderImages_Rename
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * @see GController_Profile_Data
 */
require_once('Gear/Controllers/Profile/Data.php');

/**
 * Данные интерфейса изменения имени файла изображения
 * 
 * @category   Gear
 * @package    GController_SSliderImages_Rename
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SSliderImages_Rename_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($_tableName)
     *
     * @var array
     */
    public $fields = array('image_entire_filename');

    /**
     * Первичный ключ таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'image_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_slider_images';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ssliders_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['profile_title_update'], $record['image_entire_filename']);
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        $this->dataAccessUpdate();

        if ($this->input->isEmpty('PUT'))
            throw new GException('Warning', 'To execute a query, you must change data!');
        // массив полей с их соответствующими значениями
        if (empty($params))
            $params = $this->input->getBy($this->fields);
        // проверки входных данных
        $this->isDataCorrect($params);
        // проверка существования записи с одинаковыми значениями
        $this->isDataExist($params);
        $query = new GDb_Query();
        // изображение
        $sql = 'SELECT * FROM `site_slider_images` WHERE `image_id`=' . (int)$this->uri->id;
        if (($image = $query->getRecord($sql)) === false)
            throw new GSqlException();
        $path = DOCUMENT_ROOT . $this->config->getFromCms('Site', 'DIR/SLIDERS');
        // переименовывание файла изображения
        if ($image['image_entire_filename'] != $this->input->get('image_entire_filename_new')) {
            $from = $path . $image['image_entire_filename'];
            $to = $path . $this->input->get('image_entire_filename_new');
            if (rename($from, $to) === false)
                throw new GException('Error', 'Unable to move file!');
            $params['image_entire_filename'] = $this->input->get('image_entire_filename_new');
        }
        // если нет данных для изменений
        if (empty($params))
            throw new GException('Warning', 'To execute a query, you must change data!');

        $table = new GDb_Table($this->tableName, $this->idProperty);
        if ($table->update($params, $this->uri->id) === false)
            throw new GSqlException();
        // запись в журнал действий пользователя
        $this->logUpdate(
            array('log_query'        => $table->query->getSQL(),
                  'log_error'        => $this->response->success === false ? $table->getError() : null,
                  'log_query_params' => $params)
        );
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        if ($this->input->get('image_entire_filename_new', false) === false)
            throw new GException('Warning', 'To execute a query, you must change data!');
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        $record['image_entire_filename_new'] = $record['image_entire_filename'];

        return $record;
    }
}
?>