<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'profile_title_update'   => 'Переименовать "%s"',
    // поля формы
    'label_image_path'       => 'Путь',
    'title_image_entire'     => 'Файл изображения',
    'label_image_entire'     => 'Название',
    'label_image_entire_new' => 'Новое название'
);
?>