<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс изменения имени файла изображения"
 * Пакет контроллеров "Изображения слайдера"
 * Группа пакетов     "Слайдеры"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SSliderImages_Rename
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс изменения имени изображения
 * 
 * @category   Gear
 * @package    GController_SSliderImages_Rename
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SSliderImages_Rename_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'           => '',
                  'titleEllipsis'   => 54,
                  'gridId'          => 'gcontroller_ssliderimages_grid',
                  'width'           => 400,
                  'autoHeight'      => true,
                  'btnDeleteHidden' => true,
                  'stateful'        => false)
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype'      => 'fieldset',
                  'labelWidth' => 100,
                  'title'      => $this->_['title_image_entire'],
                  'autoHeight' => true,
                  'items'      => array(
                      array('xtype'         => 'textfield',
                            'fieldLabel'    => $this->_['label_image_entire'],
                            'itemCls'       => 'mn-form-item-info',
                            'name'          => 'image_entire_filename',
                            'checkDirty'    => false,
                            'maxLength'     => 255,
                            'anchor'        => '100%',
                            'disabled'      => true,
                            'allowBlank'    => true),
                      array('xtype'         => 'textfield',
                            'fieldLabel'    => $this->_['label_image_entire_new'],
                            'name'          => 'image_entire_filename_new',
                            'checkDirty'    => false,
                            'maxLength'     => 255,
                            'anchor'        => '100%',
                            'allowBlank'    => true)
                  )
            )
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($items);
        $form->labelWidth = 85;
        $form->url = $this->componentUrl . 'rename/';

        parent::getInterface();
    }
}
?>