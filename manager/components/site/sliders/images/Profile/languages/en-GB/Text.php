<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Create record "Image"',
    'title_profile_update' => 'Update record "%s"',
    // поля формы
    // закладка "атрибуты"
    'title_tab_attributes' => 'Attributes',
    'label_image_index'    => 'Index',
    'tip_image_index'      => 
        'Serial number (for the gallery - ordering the list, and for articles - the formation of a pseudonym; each '
      . 'category of its own order)',
    'label_image_visible'  => 'Show',
    'tip_image_visible'    => 'Show image',
    'label_image_archive'  => 'Archive',
    'tip_image_archive'    => 'Image in archive (disabled)',
    'label_image_longdesc' => 'URL address',
    'label_image_title'    => 'Text',
    'tip_image_title'      => 
        'It is used for the signature image if the image is missing (attributes "alt", "title")',
    'tip_image_longdesc'   => 
        'Specifies the URL address to access the site where the detailed information about the image',
    'label_image_url'      => 'URL link',
    'tip_image_url'        => 'URL link address used to access the site when you click on the slider',
    // закладка "изображение"
    'title_tab_img'           => 'Image',
    'title_fieldset_img'      => 'File (%s)',
    'text_empty'              => 'Browse file ...',
    'text_btn_upload'         => 'Browse',
    'title_fieldset_aimg'     => 'Image attributes',
    'label_entire_filename'   => 'File',
    'label_entire_uri'        => 'Resource URI',
    'tip_entire_uri'          => 
        'Эa character string, which allows to identify a resource: a document, image, file, service, '
      . 'e-mail box, etc.',
    'label_entire_url'        => 'Resource URL',
    'tip_entire_url'          => 
        'Uniform locator (the determinant of the location) of the resource. It is a standardized method of recording addresses '
      . 'of Internet resource.',
    'label_entire_resolution' => 'Resolution',
    'tip_entire_resolution'   => 'Image resolution in px.',
    'label_entire_type'       => 'Type',
    'tip_entire_type'         => 'File type ("JPEG", "PNG", "GIF")',
    'label_entire_filesize'   => 'Size',
    'tip_entire_filesize'     => 'File size',
    'label_date_insert'       => 'Created',
    'label_date_update'       => 'Updated',
    // закладка "эскиз"
    'title_tab_thm'      => 'Thumb',
    'title_fieldset_ath' => 'Thumb attributes',
    // закладка "текст"
    'title_tab_text' => 'Text'
);
?>