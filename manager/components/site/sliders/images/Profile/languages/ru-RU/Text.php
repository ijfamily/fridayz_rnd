<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Создание слайда',
    'title_profile_update' => 'Изменение слайда "%s"',
    // поля формы
    // закладка "атрибуты"
    'title_tab_attributes' => 'Атрибуты',
    'label_image_index'    => 'Индекс',
    'tip_image_index'      => 'Порядковый номер (для формирования порядка в списке)',
    'label_image_visible'  => 'Показывать',
    'tip_image_visible'    => 'Показывать изображение',
    'label_image_title'    => 'Заглавие',
    'tip_image_title'      => 'Заглавие в шапке слайда',
    'label_image_title_1'  => 'Заглавие 1',
    'tip_image_title_1'    => 'Заглавие в шапке слайда (если несколько загаловков)',
    'label_image_url'      => 'URL ресурса',
    'tip_image_url'        => 'URL адрес ссылки используемая для перехода на страницу сайта при клике на слайде (если используется)',
    'title_bar_rename'     => 'Переименовать файл',
    // закладка "изображение"
    'title_tab_img'           => 'Изображение',
    'title_fieldset_img'      => 'Файл изображения',
    'text_empty'              => 'Выберите файл ...',
    'text_btn_upload'         => 'Выбрать',
    'title_fieldset_aimg'     => 'Атрибуты изображения',
    'label_entire_filename'   => 'Файл',
    'label_entire_url'        => 'Ресурс URL',
    'tip_entire_url'          => 
        'Eдинообразный локатор (определитель местонахождения) ресурса. Это стандартизированный способ записи адреса '
      . 'ресурса в сети Интернет.',
    'label_entire_resolution' => 'Разрешение',
    'tip_entire_resolution'   => 'Разрешение файла в пикселях',
    'label_entire_type'       => 'Тип',
    'tip_entire_type'         => 'Файл (".JPEG", ".JPG", ".PNG", ".GIF")',
    'label_entire_filesize'   => 'Размер',
    'tip_entire_filesize'     => 'Размер файла',
    'label_date_insert'       => 'Создан',
    'label_date_update'       => 'Изменён',
    'note_img'                => '<note>допустимые расширения файлов &laquo;%s&raquo;</note>',
    // закладка "текст"
    'title_tab_text' => 'Текст'
);
?>