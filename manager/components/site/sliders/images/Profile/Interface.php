<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля изображения"
 * Пакет контроллеров "Изображения слайдера"
 * Группа пакетов     "Слайдеры"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SSliderImages_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля изображения
 * 
 * @category   Gear
 * @package    GController_SSliderImages_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SSliderImages_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Каталог изображений слайдера
     *
     * @var string
     */
    public $pathData = '/data/sliders/';

    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        $data = array('max' => 1, 'image_entire_filename' => '');

        parent::getDataInterface();

        $query = new GDb_Query();
        // если состояние формы "правка"
        if ($this->isInsert) {
            $sql = 'SELECT MAX(`image_index`) `max` FROM `site_slider_images` '
                 . 'WHERE `slider_id`=' . $this->store->get('record', 0, 'gcontroller_ssliderimages_grid');
            if (($record = $query->getRecord($sql)) === false)
                throw new GSqlException();
            $data['max'] = $record['max'] + 1;
            return $data;
        }

        // выбранное изображение
        $sql = 'SELECT * FROM `site_slider_images` WHERE `image_id`=' . (int) $this->uri->id;
        if (($record = $query->getRecord($sql)) === false)
            throw new GSqlException();
        $data['image_entire_filename'] = $record['image_entire_filename'];

        return $data;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'id'            => $this->classId,
                  'titleEllipsis' => 80,
                  'gridId'        => 'gcontroller_ssliderimages_grid',
                  'width'         => 505,
                  'authoHeight'   => true,
                  'resizable'     => false,
                  'stateful'      => false)
        );

        // поля формы (ExtJS class "Ext.Panel")
        // поля вкладки "атрибуты"
        $tabAttrItems = array(
            array('xtype'      => 'spinnerfield',
                  'fieldLabel' => $this->_['label_image_index'],
                  'labelTip'   => $this->_['tip_image_index'],
                  'name'       => 'image_index',
                  'value'      => $data['max'],
                  'width'      => 70,
                  'allowBlank' => false,
                  'emptyText'  => 1),
            array('xtype'      => 'mn-field-chbox',
                  'fieldLabel' => $this->_['label_image_visible'],
                  'labelTip'   => $this->_['tip_image_visible'],
                  'default'    => $this->isUpdate ? null : 1,
                  'name'       => 'image_visible'),
            array('xtype'      => 'textfield',
                  'itemCls'    => 'mn-form-item-quiet',
                  'fieldLabel' => $this->_['label_image_title'],
                  'labelTip'   => $this->_['tip_image_title'],
                  'name'       => 'image_title',
                  'maxLength'  => 255,
                  'anchor'     => '100%',
                  'allowBlank' => true),
            array('xtype'      => 'textfield',
                  'itemCls'    => 'mn-form-item-quiet',
                  'fieldLabel' => $this->_['label_image_title_1'],
                  'labelTip'   => $this->_['tip_image_title_1'],
                  'name'       => 'image_title_1',
                  'maxLength'  => 255,
                  'anchor'     => '100%',
                  'allowBlank' => true),
            array('xtype'      => 'textfield',
                  'itemCls'    => 'mn-form-item-quiet',
                  'fieldLabel' => $this->_['label_image_url'],
                  'labelTip'   => $this->_['tip_image_url'],
                  'name'       => 'image_url',
                  'maxLength'  => 255,
                  'anchor'     => '100%',
                  'allowBlank' => true)
        );
        // вкладка "атрибуты"
        $tabAttr = array(
            'title'       => $this->_['title_tab_attributes'],
            'iconSrc'    => $this->resourcePath . 'icon-tab-attr.png',
            'layout'      => 'form',
            'labelWidth'  => 85,
            'baseCls'     => 'mn-form-tab-body',
            'defaultType' => 'textfield',
            'items'       => array($tabAttrItems)
        );

        // поля вкладки "изображение"
        // если состояние формы "правка"
        if ($this->isUpdate) {
            $tabImageItems = array(
                array('xtype'   => 'mn-form-image-view',
                      'image'   => array(
                          'src'    => $this->pathData . $data['image_entire_filename'],
                          'full'   => $this->pathData . $data['image_entire_filename'],
                          'title'  => $data['image_entire_filename'],
                          'width'  => 300,
                          'height' => 180,
                      ),
                      'default' => $this->pathData . 'list_none.jpg',
                      'buttons' => array(
                          'download',
                          'picture',
                          array('type'  => 'rename',
                                'title' => $this->_['title_bar_rename'],
                                'url'   => $this->componentUrl . 'rename/interface/' . $this->uri->id)
                      )
                ),
                array('xtype'      => 'textfield',
                      'itemCls'    => 'mn-form-item-info',
                      'fieldLabel' => $this->_['label_entire_filename'],
                      'name'       => 'image_entire_filename',
                      'maxLength'  => 255,
                      'anchor'     => '100%',
                      'readOnly'   => true,
                      'allowBlank' => true),
                array('xtype'      => 'textfield',
                      'itemCls'    => 'mn-form-item-info',
                      'fieldLabel' => $this->_['label_entire_url'],
                      'labelTip'   => $this->_['tip_entire_url'],
                      'name'       => 'image_entire_url',
                      'maxLength'  => 255,
                      'anchor'     => '100%',
                      'readOnly'   => true,
                      'allowBlank' => true)
            );
        // если состояние формы "вставка"
        } else {
            $tabImageItems = array(
                array('xtype'      => 'fieldset',
                      'labelWidth' => 70,
                      'title'      => $this->_['title_fieldset_img'],
                      'autoHeight' => true,
                      'items'      => array(
                          array('xtype'      => 'mn-field-upload',
                                'emptyText'  => $this->_['text_empty'],
                                'name'       => 'image',
                                'buttonText' => $this->_['text_btn_upload'],
                                'anchor'     => '100%',
                                'buttonCfg'  => array('width' => 70)
                          ),
                          array('xtype' => 'label', 'html' => sprintf($this->_['note_img'], $this->config->getFromCms('Site', 'FILES/EXT/IMAGES'))),
                      )
                )
            );
        }
        // вкладка "изображение"
        $tabImage = array(
            'iconSrc'     => $this->resourcePath . 'icon-tab-img.png',
            'title'       => $this->_['title_tab_img'],
            'layout'      => 'form',
            'labelWidth'  => 85,
            'baseCls'     => 'mn-form-tab-body',
            'defaultType' => 'textfield',
            'items'       => array($tabImageItems)
        );

        // вкладка "текст"
        $tabText = array(
            'title'      => $this->_['title_tab_text'],
            'iconSrc'    => $this->resourcePath . 'icon-tab-text-def.png',
            'layout'     => 'fit',
            'bodyStyle'  => 'padding:1px;',
            'items'      => array(
                array('xtype'      => 'htmleditor',
                      'hideLabel'  => true,
                      'name'       => 'image_text',
                      'anchor'     => '100% 100%',
                      'allowBlank' => true)
            )
        );

        // если едент. слайдера задаётся из вне
        if ($sliderId = $this->uri->getVar('parent', false)) {
            $tabText['items'][] = array(
                'xtype' => 'hidden',
                'name'  => 'slider_id',
                'value' => $sliderId
            );
        }

        // вкладки окна
        $tabs = array(
            array('xtype'             => 'tabpanel',
                  'layoutOnTabChange' => true,
                  'activeTab'         => 0,
                  'style'             => 'padding:3px',
                  'enableTabScroll'   => true,
                  'anchor'            => '100%',
                  'height'            => $this->isUpdate ? 347 : 300,
                  'defaults'          => array('autoScroll' => true),
                  'items'             => array($tabAttr, $tabText, $tabImage))
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($tabs);
        $form->url = $this->componentUrl . 'profile/';
        $form->fileUpload = true;

        parent::getInterface();
    }
}
?>