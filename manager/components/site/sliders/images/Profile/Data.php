<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные для интерфейса профиля изображения"
 * Пакет контроллеров "Изображения слайдера"
 * Группа пакетов     "Слайдеры"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SSliderImages_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('File');
Gear::controller('Profile/Data');

/**
 * Данные для интерфейса профиля изображения
 * 
 * @category   Gear
 * @package    GController_SSliderImages_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SSliderImages_Profile_Data extends GController_Profile_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array(
        'slider_id', 'image_index', 'image_entire_filename', 'image_entire_resolution',
        'image_entire_filesize', 'image_date', 'image_visible', 'image_title', 'image_title_1', 
        'image_text', 'image_url'
    );

    /**
     * Первичный ключ таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'image_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_slider_images';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ssliders_grid';

    /**
     * Доступные расширения файлов для загрузки
     *
     * @var array
     */
    protected $_upload = array('jpg', 'jpeg', 'png', 'gif');

    /**
     * Идентификатор слайдера
     *
     * @var integer
     */
    protected $_sliderId;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // если едент. слайдера задаётся из вне
        if ($sliderId = $this->input->get('slider_id', false))
            $this->_sliderId = $sliderId;
        // если едент. слайдера из списка
        else
            $this->_sliderId = $this->store->get('record', 0, 'gcontroller_ssliderimages_grid');
        // каталог загрузки изображений
        $this->_uploadPath = '../' .  $this->config->getFromCms('Site', 'DIR/SLIDERS');
    }

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_profile_update'], $record['image_entire_filename']);
    }

    /**
     * Событие наступает после успешного удаления данных
     * 
     * @return void
     */
    protected function dataDeleteComplete()
    {
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Событие наступает после успешного обновления данных
     * 
     * @param array $data сформированные данные полученные после запроса к таблице
     * @return void
     */
    protected function dataUpdateComplete($data = array())
    {
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Событие наступает после успешного добавления данных
     * 
     * @param array $data сформированные данные полученные после запроса к таблице
     * @return void
     */
    protected function dataInsertComplete($data = array())
    {
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        // изображения слайдера
        $query = new GDb_Query();
        $sql = 'SELECT * FROM `site_slider_images` WHERE `image_id` IN (' . $this->uri->id. ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        while (!$query->eof()) {
            $image = $query->next();
            // если есть изображение
            if ($image['image_entire_filename'])
                GFile::delete($this->_uploadPath . $image['image_entire_filename'], false);
        }
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        Gear::library('File');

        // дата изменения изображения
        $params['image_date'] = date('Y-m-d H:i:s');
        // если состояние формы "правка"
        if ($this->isUpdate) return;
        // идент. слайдера
        $params['slider_id'] = $this->_sliderId;
        // идент. название файла
        $fileId = uniqid();
        // попытка загрузить изображение
        // если нет изображения для загрузки
        if (!$this->input->hasFile())
            throw new GException('Adding data', 'File was not loaded');
        if (($file = $this->input->file->get('image')) === false)
            throw new GException('Adding data', 'File was not loaded');
        $uploadExt = explode(',', strtolower($this->config->getFromCms('Site', 'FILES/EXT/IMAGES')));
        // определение имени файла
        $fileExt = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
        $filename = $fileId . '.' . $fileExt;
        // загрузка изображения
        GFile::upload($file, $this->_uploadPath . $filename, $uploadExt);
        $img = GFactory::getClass('image', 'image', $this->_uploadPath . $filename);
        // обновляем поля изображения
        $params['image_entire_filename'] = $filename;
        $params['image_entire_filesize'] = GFile::fileSize($file['size']);
        $params['image_entire_resolution'] = $img->getSizeStr();
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        // ресурсы изображения
        $record['image_entire_url'] = 'http://' . $_SERVER['SERVER_NAME'] . $this->config->getFromCms('Site', 'DIR/SLIDERS') . $record['image_entire_filename'];

        return $record;
    }
}
?>