<?php
/**
 * Gear Manager
 *
 * Контроллер         "Развёрнутый список о записи изображения"
 * Пакет контроллеров "Список слайдеров"
 * Группа пакетов     "Слайдеры"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SSliders_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Expand');

/**
 * Развёрнутый список о записи изображения
 * 
 * @category   Gear
 * @package    GController_SSliders_Grid
 * @subpackage Expand
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SSliders_Grid_Expand extends GController_Grid_Expand
{
    /**
     * Вывод данных в интерфейс
     * 
     * @return void
     */
    protected function dataExpand()
    {
        parent::dataAccessView();

        $data = $this->getAttributes();
        $data .= $this->getImages();
        $this->response->data = $data;
    }

    /**
     * Возращает атрибуты записи
     * 
     * @return string
     */
    protected function getAttributes()
    {
        $data = '';
        $query = new GDb_Query();
        $sql = 'SELECT * FROM `site_sliders` WHERE `slider_id`=' . $this->uri->id;
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        $this->_title = $rec['slider_name'];

        return '';
    }

    /**
     * Возращает изображения записи
     * 
     * @return string
     */
    protected function getImages()
    {
        $data = '';
        $query = new GDb_Query();
        $sql = 'SELECT * FROM `site_slider_images` WHERE `slider_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        while (!$query->eof()) {
            $img = $query->next();
            $data .= '<span mn:bar="download,picture,widget,rename'
                   . '" mn:text="' . ($img['image_title'] ? $img['image_title'] : $this->_['label_image_title'] . ' ' . $img['image_index'])
                   . (!$img['image_visible'] ? '" mn:hidden="true' : '')
                   . '" mn:rename="site/sliders/images/' . ROUTER_DELIMITER . 'rename/interface/' . $img['image_id']
                   . '" mn:widget="site/sliders/images/' . ROUTER_DELIMITER . 'profile/interface/' . $img['image_id'] 
                   . '" mn:image-thumb="/data/sliders/' . $img['image_entire_filename']
                   . '" mn:image-full="/data/sliders/' . $img['image_entire_filename']
                   . '" class="mn-img mn-img-xxl fl"></span>';
        }
        if ($data) {
            $data = '<div class="mn-row-header">' . sprintf($this->_['title_images'], $query->getCountRecords(), $this->_title) . '</div>'
                  . '<div class="mn-row-body border">' . $data . '<div class="wrap"></div></div>';
        }

        return $data;
    }
}
?>