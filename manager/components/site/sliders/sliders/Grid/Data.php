<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка слайдеров"
 * Пакет контроллеров "Список слайдеров"
 * Группа пакетов     "Слайдеры"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SSliders_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('File');
Gear::controller('Grid/Data');

/**
 * Данные списка слайдеров
 * 
 * @category   Gear
 * @package    GController_SSliders_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SSliders_Grid_Data extends GController_Grid_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'slider_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_sliders';

    /**
     * Каталог к изображениям слайдеров
     *
     * @var string
     */
    public $pathData = 'data/sliders/';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // каталог загрузки изображений
        $this->_uploadPath = '../' . $this->pathData;

        // язык сайта по умолчанию
        $languageId = $this->config->getFromCms('Site', 'LANGUAGE/ID');
        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS `s`.*, `si`.`image_count`, `p`.`page_header` '
          . 'FROM `site_sliders` `s` '
          . 'LEFT JOIN (SELECT `slider_id`, COUNT(`slider_id`) `image_count` '
          . 'FROM `site_slider_images` GROUP BY `slider_id`) `si` USING (`slider_id`) '
            // статья
          . 'LEFT JOIN `site_articles` `a` USING (`article_id`) '
          . 'LEFT JOIN `site_pages` `p` ON `p`.`article_id`=`a`.`article_id` AND `p`.`language_id`=' . $languageId
          . ' WHERE 1 %filter ORDER BY %sort LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // идент. слайдера
            'slider_id' => array('type' => 'integer'),
            // заметка статьи
            'page_header' => array('type' => 'string'),
            // порядковый номер
            'slider_index' => array('type' => 'integer'),
            // название
            'slider_name' => array('type' => 'string'),
            // кол. изображений
            'image_count' => array('type' => 'integer')
        );
    }

    /**
     * Удаление изображений слайдера
     * 
     * @return void
     */
    protected function imagesDelete($sliderId)
    {
        $query = new GDb_Query();
        // изображения слайдера
        $sql = 'SELECT * FROM `site_slider_images` WHERE `slider_id` IN (' . $sliderId . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        while (!$query->eof()) {
            $image = $query->next();
            // если есть изображение
            if ($image['image_entire_filename'])
                GFile::delete($this->_uploadPath . $image['image_entire_filename']);
        }
        // удаление записей таблицы "site_slider_images" (изображения слайдера)
        $sql = 'DELETE FROM `site_slider_images` WHERE `slider_id` IN (' . $sliderId . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_slider_images') === false)
            throw new GSqlException();
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        $query = new GDb_Query();
        // удаление изображений слайдеров
        TDir::clear($this->_uploadPath);
        // удаление записей таблицы "site_slider_images" (изображения слайдера)
        if ($query->clear('site_slider_images') === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_slider_images') === false)
            throw new GSqlException();
        // удаление записей таблицы "site_sliders" (слайдеры)
        if ($query->clear('site_sliders') === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_sliders') === false)
            throw new GSqlException();
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Событие наступает после успешного удаления данных
     * 
     * @return void
     */
    protected function dataDeleteComplete()
    {
        // соединение с базой данных (т.к. для лога ранее возможно было другое соединение)
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();

        // удаление изображений слайдера
        $this->imagesDelete($this->uri->id);
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }
}
?>