<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'    => 'Sliders',
    'rowmenu_edit'  => 'Edit',
    'rowmenu_image' => 'Images',
    // панель управления
    'title_buttongroup_edit'   => 'Edit',
    'title_buttongroup_cols'   => 'Columns',
    'title_buttongroup_filter' => 'Filter',
    'msg_btn_clear'            => 'Do you really want to delete all records <span class="mn-msg-delete"> '
                                . '("Sliders", "Images")</span> ?',
    // столбцы
    'header_gallery_index'      => '№',
    'tooltip_slider_index '     => 'Index of slider',
    'header_article_note'       => 'Article',
    'tooltip_article_note'      => 'Article note',
    'header_slider_name'        => 'Name',
    'header_slider_description' => 'Description',
    'header_image_count'        => 'Images'
);
?>