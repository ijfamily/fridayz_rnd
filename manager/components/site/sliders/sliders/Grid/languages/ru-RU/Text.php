<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'    => 'Слайдеры',
    'tooltip_grid'  => 'список слайдов в слайдере',
    'rowmenu_edit'  => 'Редактировать',
    'rowmenu_image' => 'Слайды',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete"> '
                                . '("Слайдеры", "Слайды")</span> ?',
    // столбцы
    'header_slider_index'   => '№',
    'tooltip_slider_index'  => 'Порядковый номер слайдера',
    'header_article_note'   => 'Cтатья',
    'tooltip_article_note'  => 'Название статьи где размещен слайдер',
    'header_slider_name'    => 'Название',
    'header_image_count'    => 'Слайдов',
    // детали
    'title_images' => 'Слайды (<b>%s</b>): <b>%s</b>',
    // развёрнутая запись
    'label_image_title' => 'Слайд',
    'label_image_count' => 'Слайдов '
);
?>