<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные для интерфейса профиля слайдера"
 * Пакет контроллеров "Профиль слайдера"
 * Группа пакетов     "Слайдеры"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SSliders_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные для интерфейса профиля слайдера
 * 
 * @category   Gear
 * @package    GController_SSliders_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SSliders_Profile_Data extends GController_Profile_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array('slider_index', 'slider_name', 'article_id');

    /**
     * Первичный ключ таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'slider_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_sliders';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ssliders_grid';

    /**
     * Путь к изображениям слайдера
     *
     * @var string
     */
    public $pathData = 'data/sliders/';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // каталог загрузки изображений
        $this->_uploadPath = '../' . $this->pathData;
    }

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_profile_update'], $record['slider_name']);
    }

    /**
     * Удаление изображений слайдера
     * 
     * @return void
     */
    protected function imagesDelete($sliderId)
    {
        $query = new GDb_Query();
        // изображения слайдера
        $sql = 'SELECT * FROM `site_slider_images` WHERE `slider_id`=' . $sliderId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        while (!$query->eof()) {
            $image = $query->next();
            // если есть изображение
            if ($image['image_entire_filename'])
                GFile::delete($this->_uploadPath . $image['image_entire_filename']);
        }
        // удаление записей таблицы "site_slider_images" (изображения слайдера)
        $sql = 'DELETE FROM `site_slider_images` WHERE `slider_id`=' . $sliderId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_slider_images') === false)
            throw new GSqlException();
    }

    /**
     * Событие наступает после успешного удаления данных
     * 
     * @return void
     */
    protected function dataDeleteComplete()
    {
        // соединение с базой данных (т.к. для лога ранее возможно было другое соединение)
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();

        // удаление изображений слайдера
        $this->imagesDelete($this->uri->id);
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function recordPreprocessing($record)
    {
        $setTo = array();
        $query = new GDb_Query();
        // если выбрана статья
        if (!empty($record['article_id'])) {
            // язык сайта по умолчанию
            $languageId = $this->config->getFromCms('Site', 'LANGUAGE/ID');
            // статьи
            $sql = 'SELECT `page_header` FROM `site_pages` WHERE `language_id`=' . $languageId .' AND `article_id`=' . $record['article_id'];
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            if (!empty($rec))
                $setTo[] = array('xtype' => 'combo', 'id' => 'fldArticleId', 'value' => $rec['page_header']);
        }

        // установка полей
        if ($setTo)
            $this->response->add('setTo', $setTo);

        return $record;
    }
}
?>