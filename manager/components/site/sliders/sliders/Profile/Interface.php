<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля слайдера"
 * Пакет контроллеров "Профиль слайдера"
 * Группа пакетов     "Слайдеры"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SSliders_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля слайдера
 * 
 * @category   Gear
 * @package    GController_SSliders_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SSliders_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        $data = array('max' => 1);

        parent::getDataInterface();

        $query = new GDb_Query();
        // если состояние формы "insert"
        if ($this->isInsert) {
            $sql = 'SELECT MAX(`slider_index`) `max` FROM `site_sliders`';
            if (($record = $query->getRecord($sql)) === false)
                throw new GSqlException();
            $data['max'] = $record['max'] + 1;
            return $data;
        }

        return $data;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'titleEllipsis' => 45,
                  'gridId'        => 'gcontroller_ssliders_grid',
                  'width'         => 400,
                  'autoHeight'    => true,
                  'stateful'      => false,
                  'buttonsAdd'    => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-site-sliders',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype'      => 'spinnerfield',
                  'itemCls'    => 'mn-form-item-quiet',
                  'fieldLabel' => $this->_['label_slider_index'],
                  'labelTip'   => $this->_['tip_slider_index'],
                  'name'       => 'slider_index',
                  'value'      => $data['max'],
                  'width'      => 70,
                  'allowBlank' => true,
                  'emptyText'  => 1),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_slider_name'],
                  'name'       => 'slider_name',
                  'maxLength'  => 255,
                  'anchor'     => '100%',
                  'allowBlank' => false),
            array('xtype'      => 'mn-field-combo',
                  'fieldLabel' => $this->_['label_article_name'],
                  'labelTip'   => $this->_['tip_article_name'],
                  'id'         => 'fldArticleId',
                  'name'       => 'article_id',
                  'editable'   => true,
                  'checkDirty' => false,
                  'anchor'     => '100%',
                  'hiddenName' => 'article_id',
                  'allowBlank' => true,
                  'store'      => array(
                      'xtype' => 'jsonstore',
                      'url'   => $this->componentUrl . 'combo/trigger/?name=articles'
                  )
            )
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($items);
        $form->url = $this->componentUrl . 'profile/';

        parent::getInterface();
    }
}
?>