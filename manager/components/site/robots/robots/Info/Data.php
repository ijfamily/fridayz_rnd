<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля бота"
 * Пакет контроллеров "Профиль бота"
 * Группа пакетов     "Сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SBots_Info
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');;

/**
 * Данные профиля бота
 * 
 * @category   Gear
 * @package    GController_SBots_Info
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SBots_Info_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array('bot_name', 'bot_details', 'bot_uri', 'bot_date', 'bot_ipaddress', 'bot_access');

    /**
     * Первичный ключ таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'bot_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_bots';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sbots_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record array fields
     * @return string
     */
    protected function getTitle($record)
    {
        $settings = $this->session->get('user/settings');

        return sprintf($this->_['title_profile_info'],
               date($settings['format/datetime'], strtotime($record['bot_date'])));
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        $settings = $this->session->get('user/settings');
        $record['bot_date'] = date($settings['format/datetime'], strtotime($record['bot_date']));
        $record['bot_access'] = $this->_['data_boolean'][$record['bot_access']];
        $record['bot_uri'] = '<a target="_blank" href="' . $record['bot_uri'] . '">http://' . $_SERVER['HTTP_HOST'] . $record['bot_uri'] . '</a>';

        return $record;
    }
}
?>