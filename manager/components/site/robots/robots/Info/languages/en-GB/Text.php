<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_info' => 'Details "%s"',
    // поля формы
    'label_bot_name'      => 'Name',
    'label_bot_details'   => 'Details',
    'label_bot_uri'       => 'URI',
    'label_bot_date'      => 'Date',
    'label_bot_ipaddress' => 'IP Address',
    'label_bot_access'    => 'Access',
    // типы
    'data_boolean' => array('No', 'Yes')
);
?>