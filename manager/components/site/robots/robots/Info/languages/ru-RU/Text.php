<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_info' => 'Детали записи "%s"',
    // поля формы
    'label_bot_name'      => 'Название',
    'label_bot_details'   => 'Детально',
    'label_bot_uri'       => 'Адрес',
    'label_bot_date'      => 'Дата',
    'label_bot_ipaddress' => 'IP адрес',
    'label_bot_access'    => 'Допущен',
    // типы
    'data_boolean' => array('Нет', 'Да')
);
?>