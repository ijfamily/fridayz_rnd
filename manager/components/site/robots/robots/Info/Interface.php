<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля бота"
 * Пакет контроллеров "Профиль бота"
 * Группа пакетов     "Сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SBots_Info
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля бота
 * 
 * @category   Gear
 * @package    GController_SBots_Info
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SBots_Info_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('titleEllipsis' => 70,
                  'gridId'        => 'gcontroller_sbots_grid',
                  'width'         => 600,
                  'autoHeight'    => true,
                  'state'         => 'info',
                  'stateful'      => false)
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
           array('xtype'     => 'displayfield',
                 'fieldLabel' => $this->_['label_bot_date'],
                 'fieldClass' => 'mn-field-value-info',
                 'name'       => 'bot_date',
                 'readOnly'   => true,
                 'width'      => 140,
                 'allowBlank' => true),
           array('xtype'     => 'displayfield',
                 'fieldLabel' => $this->_['label_bot_name'],
                 'fieldClass' => 'mn-field-value-info',
                 'name'       => 'bot_name',
                 'readOnly'   => true,
                 'anchor'     => '100%',
                 'allowBlank' => true),
           array('xtype'     => 'displayfield',
                 'fieldLabel' => $this->_['label_bot_details'],
                 'fieldClass' => 'mn-field-value-info',
                 'name'       => 'bot_details',
                 'readOnly'   => true,
                 'anchor'     => '100%',
                 'allowBlank' => true),
           array('xtype'     => 'displayfield',
                 'fieldLabel' => $this->_['label_bot_uri'],
                 'fieldClass' => 'mn-field-value-info',
                 'name'       => 'bot_uri',
                 'readOnly'   => true,
                 'anchor'     => '100%',
                 'allowBlank' => true),
            array('xtype'     => 'displayfield',
                 'fieldLabel' => $this->_['label_bot_ipaddress'],
                 'fieldClass' => 'mn-field-value-info',
                 'name'       => 'bot_ipaddress',
                 'readOnly'   => true,
                 'width'      => 140,
                 'allowBlank' => true),
            array('xtype'     => 'displayfield',
                 'fieldLabel' => $this->_['label_bot_access'],
                 'fieldClass' => 'mn-field-value-info',
                 'name'       => 'bot_access',
                 'readOnly'   => true,
                 'width'      => 140,
                 'allowBlank' => true)
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($items);
        $form->url = $this->componentUrl . 'info/';

        parent::getInterface();
    }
}
?>