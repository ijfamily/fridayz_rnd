<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск ботов"
 * Пакет контроллеров "Боты"
 * Группа пакетов     "Сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SBots_Search
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск ботов
 * 
 * @category   Gear
 * @package    GController_SBots_Search
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SBots_Search_Data extends GController_Search_Data
{
    /**
     * дентификатор компонента (списка) к которому применяется поиск
     *
     * @var string
     */
    public $gridId = 'gcontroller_sbots_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sbots_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор
     * Используется для инициализации атрибутов контроллер не переданного в конструктор
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'bot_name', 'label' => $this->_['header_bot_name']),
            array('dataIndex' => 'bot_details', 'label' => $this->_['header_bot_details']),
            array('dataIndex' => 'bot_uri', 'label' => $this->_['header_bot_uri']),
            array('dataIndex' => 'bot_ipaddress', 'label' => $this->_['header_bot_ipaddress']),
            array('dataIndex' => 'bot_access', 'label' => $this->_['header_bot_access']),
        );
    }

    /**
     * Применение фильтра
     * 
     * @return void
     */
    protected function dataAccept()
    {
        if ($this->_sender == 'toolbar') {
            if ($this->input->get('action') == 'search') {
                $tlbFilter = '';
                // если попытка сбросить фильтр
                if (!$this->input->isEmptyPost(array('domain'))) {
                    $tlbFilter = array(
                        'domain' => $this->input->get('domain')
                    );
                }
                $this->store->set('tlbFilter', $tlbFilter);
                return;
            }
        }

        parent::dataAccept();
    }
}
?>