<?php
/**
 * Gear Manager
 *
 * Контроллер         "Триггер дерева"
 * Пакет контроллеров "Боты"
 * Группа пакетов     "Сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SBots_Tree
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Tree/Trigger');

/**
 * Триггер дерева
 * 
 * @category   Gear
 * @package    GController_SBots_Tree
 * @subpackage Trigger
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SBots_Tree_Trigger extends GController_Tree_Trigger
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sbots_grid';

    /**
     * Возращает список ip адресов
     *
     * @return array
     */
    protected function getByIp()
    {
        $data = array();
        $query = new GDb_Query();
        $sql = 'SELECT *, COUNT(*) `count` FROM `site_bots` GROUP BY `bot_ipaddress`';
        if ($query->execute($sql) !== true)
            throw new GSqlException();
        while (!$query->eof()) {
            $rec = $query->next();
            $data[] = array(
                'text'    => $rec['bot_ipaddress'] . ' <b>(' . $rec['count'] . ')</b>',
                'qtip'    => $rec['bot_name'],
                'leaf'    => true,
                'iconCls' => 'icon-folder-find',
                'value'   => $rec['bot_ipaddress']
            );
        }

        return $data;
    }

    /**
     * Возращает список ip адресов
     *
     * @return array
     */
    protected function getByBots()
    {
        $data = array();
        $query = new GDb_Query();
        $sql = 'SELECT *, COUNT(*) `count` FROM `site_bots` GROUP BY `bot_name`';
        if ($query->execute($sql) !== true)
            throw new GSqlException();
        while (!$query->eof()) {
            $rec = $query->next();
            $data[] = array(
                'text'    => $rec['bot_name'] . ' <b>(' . $rec['count'] . ')</b>',
                'qtip'    => $rec['bot_name'],
                'leaf'    => true,
                'iconCls' => 'icon-folder-find',
                'value'   => $rec['bot_name']
            );
        }

        return $data;
    }

    /**
     * Вывод данных в интерфейс компонента
     * 
     * @param  string $name название триггера
     * @param  string $node id выбранного узла
     * @return void
     */
    protected function dataView($name, $node)
    {
        parent::dataView($name, $node);

        // триггер
        switch ($name) {
            // быстрый фильтр списка
            case 'gridFilter':
                // id выбранного узла
                switch ($node) {
                    // по ip адресу
                    case 'byIp': $this->response->data = $this->getByIp(); break;

                    // по боту
                    case 'byBo': $this->response->data = $this->getByBots(); break;
                }
        }
    }
}
?>