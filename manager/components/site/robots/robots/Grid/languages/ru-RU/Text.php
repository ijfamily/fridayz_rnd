<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Роботы',
    'tooltip_grid' => 'список роботов и проиндексированных страниц сайта',
    'rowmenu_info' => 'Информация о записи',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">("Роботы")</span> ?',
    'text_btn_config'          => 'Настройка',
    'tooltip_btn_config'       => 'Настройка посещаемости сайта роботами',
    'label_domain'             => 'Сайт',
    // столбцы
    'header_site_name'     => 'Сайт',
    'header_bot_name'      => 'Название',
    'header_bot_details'   => 'Детально',
    'header_bot_uri'       => 'Адрес',
    'header_bot_date'      => 'Дата',
    'header_bot_ipaddress' => 'IP адрес',
    'header_bot_access'    => 'Допущен',
    // тип
    'data_boolean' => array('нет', 'да'),
    // быстрый фильтр
    'text_all_records' => 'Все роботы',
    'text_by_date'     => 'По дате индексации',
    'text_by_day'      => 'За день',
    'text_by_week'     => 'За неделю',
    'text_by_month'    => 'За месяц',
    'text_by_bot'      => 'По роботу',
    'text_by_ip'       => 'По IP адресу'
);
?>