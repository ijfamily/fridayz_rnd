<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'     => 'Bots',
    'rowmenu_info'   => 'Details',
    // панель управления
    'title_buttongroup_edit'   => 'Edit',
    'title_buttongroup_cols'   => 'Coumns',
    'title_buttongroup_filter' => 'Filter',
    'msg_btn_clear'            => 'Do you really want to delete all records <span class="mn-msg-delete">("Bots")</span> ?',
    // столбцы
    'header_bot_name'      => 'Name',
    'header_bot_details'   => 'Details',
    'header_bot_uri'       => 'Address',
    'header_bot_date'      => 'Date',
    'header_bot_ipaddress' => 'IP address',
    'header_bot_access'    => 'Access'
);
?>