<?php
/**
 * Gear Manager
 *
 * Контроллер         "Развёрнутая запись бота"
 * Пакет контроллеров "Боты"
 * Группа пакетов     "Сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SBots_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Expand');

/**
 * Развёрнутая запись бота
 * 
 * @category   Gear
 * @package    GController_SBots_Grid
 * @subpackage Expand
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SBots_Grid_Expand extends GController_Grid_Expand
{
    /**
     * Вывод данных в интерфейс
     * 
     * @return void
     */
    protected function dataExpand()
    {
        parent::dataAccessView();

        $this->response->data = $this->getAttributes();
    }

    /**
     * Возращает атрибуты записи
     * 
     * @return string
     */
    protected function getAttributes()
    {
        $data = '';
        $settings = $this->session->get('user/settings');
        $query = new GDb_Query();
        // бот
        $sql = 'SELECT * FROM `site_bots` WHERE `bot_id`=' . $this->uri->id;
        if (($bot = $query->getRecord($sql)) === false)
            throw new GSqlException();
        // бот
        $data = '<div><fieldset class="fixed" style="width:100%"><ul>';
        if (!empty($bot['bot_date']))
            $data .= '<li><label>' . $this->_['header_bot_date'] . ':</label> ' . date($settings['format/datetime'], strtotime($bot['bot_date'])) . '</li>';
        $data .= '<li><label>' . $this->_['header_bot_name'] . ':</label> ' . $bot['bot_name'] . '</li>';
        $data .= '<li><label>' . $this->_['header_bot_details'] . ':</label> ' . $bot['bot_details'] . '</li>';
        $data .= '<li><label>' . $this->_['header_bot_uri'] . ':</label> <a target="_blank" href="' . $bot['bot_uri'] . '">http://' . $_SERVER['HTTP_HOST'] . $bot['bot_uri'] . '</a></li>';
        $data .= '<li><label>' . $this->_['header_bot_ipaddress'] . ':</label> ' . $bot['bot_ipaddress'] . '</li>';
        $data .= '<li><label>' . $this->_['header_bot_access'] . ':</label> ' . $this->_['data_boolean'][(int) $bot['bot_access']] . '</li>';
        $data .= '</ul></fieldset>';

        $data = '<div class="mn-row-body border">' . $data . '<div class="wrap"></div></div>';

        return $data;
    }
}
?>