<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка ботов"
 * Пакет контроллеров "Боты"
 * Группа пакетов     "Сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SBots_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс списка ботов
 * 
 * @category   Gear
 * @package    GController_SBots_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SBots_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'bot_id';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'bot_date';

    /**
     * Использовать быстрый фильтр
     *
     * @var boolean
     */
    public $useSlidePanel = true;

    /**
     * Возращает список доменов
     * 
     * @return array
     */
    protected function getDomains()
    {
        $arr = $this->config->getFromCms('Domains');
        $list = array();
        if ($arr) {
            $list[] = array('Все', -1);
            $list[] = array($this->config->getFromCms('Site', 'NAME', ''), 0);
            $domains = $arr['domains'];
            foreach ($arr['indexes'] as $key => $value) {
                $list[] = array($value[1], $key);
            } 
        }

        return $list;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // название сайта
            array('name' => 'domain_id', 'type' => 'string'),
            // название
            array('name' => 'bot_name', 'type' => 'string'),
            // дедали
            array('name' => 'bot_details', 'type' => 'string'),
            // адрес
            array('name' => 'bot_uri', 'type' => 'string'),
            // дата
            array('name' => 'bot_date', 'type' => 'string'),
            // ip адрес
            array('name' => 'bot_ipaddress', 'type' => 'string'),
            // доступ
            array('name' => 'bot_access', 'type' => 'integer')
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $settings = $this->session->get('user/settings');
        $this->columns = array(
            array('xtype'     => 'expandercolumn',
                  'url'       => $this->componentUrl . 'grid/expand/',
                  'typeCt'    => 'row'),
            array('xtype'     => 'numbercolumn'),
            array('xtype'     => 'rowmenu'),
            array('dataIndex' => 'domain_id',
                  'header'    => $this->_['header_site_name'],
                  'width'     => 90,
                  'hidden'    => !$this->config->getFromCms('Site', 'OUTCONTROL'),
                  'sortable'  => true),
            array('xtype'     => 'datetimecolumn',
                  'frmDate'   => $settings['format/date'],
                  'frmTime'   => $settings['format/time'],
                  'dataIndex'     => 'bot_date',
                  'dateTimeIndex' => 'bot_date',
                  'header'    => '<em class="mn-grid-hd-details"></em>'
                               . $this->_['header_bot_date'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'date')),
            array('dataIndex' => 'bot_name',
                  'header'    => $this->_['header_bot_name'],
                  'width'     => 150,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'bot_details',
                  'header'    => $this->_['header_bot_details'],
                  'width'     => 230,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'bot_uri',
                  'header'    => $this->_['header_bot_uri'],
                  'width'     => 200,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'bot_ipaddress',
                  'header'    => $this->_['header_bot_ipaddress'],
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('xtype'     => 'booleancolumn',
                  'dataIndex' => 'bot_access',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-visible.png" align="absmiddle">',
                  'tooltip'   => $this->_['header_bot_access'],
                  'align'     => 'center',
                  'width'     => 45,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false))
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_grid'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_grid'],
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'profileUrl'    => $this->componentUrl . 'profile/')
        );

         // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "edit" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' => $this->classId));
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'      => 'mn-btn-clear-data',
                  'msgConfirm' => $this->_['msg_btn_clear'],
                  'gridId'     => $this->classId,
                  'isN'        => true)
        );
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка настройки" (ExtJS class "Manager.button.Widget")
        $group->items->add(
            array('xtype'   => 'mn-btn-widget',
                  'width'   => 60,
                  'text'    => $this->_['text_btn_config'],
                  'tooltip' => $this->_['tooltip_btn_config'],
                  'icon'    => $this->resourcePath . 'icon-btn-config.png',
                  'url'     => $this->componentUri . '../config/' . ROUTER_DELIMITER . 'profile/interface/')
        );
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = 'component-site-robots';
        $this->_cmp->tbar->items->add($group);

        // управление сайтами на других доменах
        if ($this->config->getFromCms('Site', 'OUTCONTROL')) {
            $domains = $this->getDomains();
            if ($domains) {
                // группа кнопок "фильтр" (ExtJS class "Ext.ButtonGroup")
                $group = new Ext_ButtonGroup_Filter(
                    array('title'  => $this->_['title_buttongroup_filter'],
                          'gridId' => $this->classId)
                );
                // кнопка "справка" (ExtJS class "Manager.button.Help")
                $btn = &$group->items->get(0);
                $btn['gridId'] = $this->classId;
                $btn['url'] = $this->componentUrl . 'search/data/';
                // кнопка "поиск" (ExtJS class "Manager.button.Search")
                $filter = $this->store->get('tlbFilter', array());
                $group->items->add(array(
                    'xtype'       => 'form',
                    'labelWidth'  => 35,
                    'bodyStyle'   => 'border:1px solid transparent;background-color:transparent',
                    'frame'       => false,
                    'bodyBorder ' => false,
                    'items'       => array(
                        array('xtype'         => 'combo',
                              'fieldLabel'    => $this->_['label_domain'],
                              'name'          => 'domain',
                              'checkDirty'    => false,
                              'editable'      => false,
                              'width'         => 140,
                              'typeAhead'     => false,
                              'triggerAction' => 'all',
                              'mode'          => 'local',
                              'store'         => array(
                                  'xtype'  => 'arraystore',
                                  'fields' => array('list', 'value'),
                                  'data'   => $domains
                              ),
                              'value'         => empty($filter) ? '' : $filter['domain'],
                              'hiddenName'    => 'domain',
                              'valueField'    => 'value',
                              'displayField'  => 'list',
                              'allowBlank'    => false)
                    )
                ));
                $this->_cmp->tbar->items->add($group);
            }
        }

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $cellInfo =
            '<div class="mn-grid-cell-tooltip-tl">{bot_date}</div>'
          . '<div class="mn-grid-cell-tooltip">'
          . '<em>' . $this->_['header_bot_name'] . '</em>: <b>{bot_name}</b><br>'
          . '<em>' . $this->_['header_bot_details'] . '</em>: <b>{bot_details}</b><br>'
          . '<em>' . $this->_['header_bot_uri'] . '</em>: <b>{bot_uri}</b><br>'
          . '<em>' . $this->_['header_bot_ipaddress'] . '</em>: <b>{bot_ipaddress}</b><br>'
          . '<em>' . $this->_['header_bot_access'] . '</em>: '
          . '<tpl if="bot_access == 0"><b>' . $this->_['data_boolean'][0] . '</b>;</tpl>'
          . '<tpl if="bot_access == 1"><b>' . $this->_['data_boolean'][1] . '</b>;</tpl><br>'
          . '</div>';
        $this->_cmp->cellTips = array(
            array('field' => 'bot_date', 'tpl' => $cellInfo),
            array('field' => 'bot_name', 'tpl' => '{bot_name}'),
            array('field' => 'bot_details', 'tpl' => '{bot_details}'),
            array('field' => 'bot_uri', 'tpl' => '{bot_uri}')
        );

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $items = array(
            array('text' => $this->_['rowmenu_info'],
                  'icon' => $this->resourcePath . 'icon-item-info.png',
                  'url'  => $this->componentUrl . 'info/interface/')
        );
        $this->_cmp->rowMenu = array('xtype' => 'mn-grid-rowmenu', 'items' => $items);

        // быстрый фильтр списка (ExtJs class "Manager.tree.GridFilter")
        $this->_slidePanel->cls = 'mn-tree-gridfilter';
        $this->_slidePanel->width = 250;
        $this->_slidePanel->initRoot = array(
            'text'     => 'Filter',
            'id'       => 'byRoot',
            'expanded' => true,
            'children' => array(
                array('text'     => $this->_['text_all_records'],
                      'value'    => 'all',
                      'expanded' => true,
                      'leaf'     => false,
                      'children' => array(
                        array('text'     => $this->_['text_by_date'],
                              'id'       => 'byDt',
                              'leaf'     => false,
                              'expanded' => true,
                              'children' => array(
                                  array('text'     => $this->_['text_by_day'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 'day'),
                                  array('text'     => $this->_['text_by_week'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true, 
                                        'value'    => 'week'),
                                  array('text'     => $this->_['text_by_month'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 'month')
                              )
                        ),
                        array('text'    => $this->_['text_by_bot'],
                              'id'      => 'byBo',
                              'leaf'    => false),
                        array('text'    => $this->_['text_by_ip'],
                              'id'      => 'byIp',
                              'leaf'    => false)
                    )
                )
            )
        );

        parent::getInterface();
    }
}
?>