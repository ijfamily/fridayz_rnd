<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка ботов"
 * Пакет контроллеров "Боты"
 * Группа пакетов     "Сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SBots_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные списка ботов
 * 
 * @category   Gear
 * @package    GController_SBots_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SBots_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'bot_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_bots';

    /**
     * Значки ботов
     *
     * @var string
     */
    protected $_bots = array(
        'Yandex'    => array('src' => 'icon-bot-yandex.png', 'color' => '#ED3030'),
        'Googlebot' => array('src' => 'icon-bot-google.png', 'color' => '#018DFF')
    );

    /**
     * Управления сайтами на других доменах
     *
     * @var boolean
     */
    public $isOutControl = false;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // управление сайтами на других доменах
        $this->isOutControl = $this->config->getFromCms('Site', 'OUTCONTROL');
        if ($this->isOutControl)
            $this->domains = $this->config->getFromCms('Domains', 'indexes');
        $this->domains[0] = array($_SERVER['SERVER_NAME'], $this->config->getFromCms('Site', 'NAME'));

        // SQL запрос
        $this->sql = 'SELECT SQL_CALC_FOUND_ROWS * FROM `site_bots` WHERE 1 ';
        // фильтр из панели инструментов
        if ($this->isOutControl) {
            $filter = $this->store->get('tlbFilter', false);
            if ($filter) {
                $domainId = (int) $filter['domain'];
                if ($domainId >= 0)
                    $this->sql .= ' AND `domain_id`=' . $domainId;
            }
        }
        $this->sql .= '%filter %fastfilter ORDER BY %sort LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // название сайта
            'domain_id' => array('type' => 'string'),
            // название
            'bot_name' => array('type' => 'string'),
            // дедали
            'bot_details' => array('type' => 'string'),
            // адрес
            'bot_uri' => array('type' => 'string'),
            // дата
            'bot_date' => array('type' => 'string'),
            // ip адрес
            'bot_ipaddress' => array('type' => 'string'),
            // доступ
            'bot_access' => array('type' => 'integer')
        );
    }

    /**
     * Возращает sql запрос для быстрого фильтра
     * (для подстановки "%fastfilter" в $sql)
     * 
     * @param string $key идендификатор поля
     * @param string $value значение
     * @return string
     */
    protected function getTreeFilter($key, $value)
    {
        // идендификатор поля
        switch ($key) {
            // по дате правки
            case 'byDt':
                $toDate = date('Y-m-d');
                switch ($value) {
                    case 'day':
                        return ' AND (`bot_date` BETWEEN "' . $toDate . '" AND "' . $toDate . '"'
                             . ' OR `bot_date` BETWEEN "' . $toDate . '" AND "' . $toDate . '")';

                    case 'week':
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') - 7, date('Y')));
                        return ' AND (`bot_date` BETWEEN "' . $fromDate . '" AND "' . $toDate . '"'
                             . ' OR `bot_date` BETWEEN "' . $fromDate . '" AND "' . $toDate . '")';

                    case 'month':
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, date('m') - 1, date('d'), date('Y')));
                        return ' AND (`bot_date` BETWEEN "' . $fromDate . '" AND "' . $toDate . '"'
                             . ' OR `bot_date` BETWEEN "' . $fromDate . '" AND "' . $toDate . '")';
                }
                break;

            // по ip адресу
            case 'byIp':
                return ' AND `bot_ipaddress`="' . $value . '" ';

            // по боту
            case 'byBo':
                return ' AND `bot_name`="' . $value . '" ';
        }

        return '';
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        $query = new GDb_Query();
        // удаление записей таблицы "site_bots" (роботы)
        if ($query->clear('site_bots') === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_bots') === false)
            throw new GSqlException();
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        $domain = $_SERVER['SERVER_NAME'];
        // сайты на других доменах
        if (isset($this->domains[$record['domain_id']])) {
            $domain = $this->domains[$record['domain_id']][0];
            $record['domain_id'] = $this->domains[$record['domain_id']][1];
        } else
            $record['domain_id'] = '';

        if (isset($this->_bots[$record['bot_name']]))
            $record['bot_name'] = '<span style="color:' . $this->_bots[$record['bot_name']]['color'] . '">'
                                . '<img style="margin-right:5px;" src="' . $this->resourcePath . $this->_bots[$record['bot_name']]['src'] . '" align="absmiddle">'
                                . $record['bot_name'] . '</span>';

        return $record;
    }
}
?>