<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля настройки посещаемости сайта роботами"
 * Пакет контроллеров "Настройка посещаемости сайта роботами"
 * Группа пакетов     "Настройки"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SConfRobots_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

define('_INC', 1);

Gear::controller('Profile/Data');

/**
 * Возращает шаблон настройки посещаемости сайта роботами
 * 
 * @params string $tpl данные в шаблоне
 */
function getTplSiteConfig($tpl)
{
    return <<<TEXT
<?php
/**
 * Gear CMS
 *
 * Настройки посещаемости сайта роботами (создан: {$tpl['date']})
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 *
 * @category   Gear
 * @package    Config
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    \$Id: Robots.php {$tpl['date']} Gear Magic \$
 */

return array(
    // максимально количество записей в журнале
    'LIMIT' => {$tpl['robots_limit']},
    // список роботов
    'LIST'  => array({$tpl['robots_list']})
);
?>
TEXT;
}

/**
 * Данные профиля настройки посещаемости сайта роботами
 * 
 * @category   Gear
 * @package    GController_SConfRobots_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SRobotsConfig_Profile_Data extends GController_Profile_Data
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_srobotsconfig_profile';

    /**
     * Файл настроек роботов
     *
     * @var string
     */
    public $filename = 'Robots.php';

    /**
     * Массив полей таблицы ($_tableName)
     *
     * @var array
     */
    public $fields = array('robots_list', 'robots_limit');

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return $this->_['profile_title_update'];
    }

    /**
     * Обновление настроеек сайта
     * 
     * @return void
     */
    protected function fileUpdate($params)
    {
        // запись для CMS
        if (file_put_contents('../' . PATH_CONFIG_CMS . $this->filename, getTplSiteConfig($params), FILE_TEXT) === false)
            throw new GException('Error', sprintf($this->_['msg_create_file'], $this->filename));
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        if (empty($params['robots_list']))
            $tpl = '';
        else {
            $list = json_decode($params['robots_list'], true);
            $count = sizeof($list);
            $tpl = '';
            for ($i = 0; $i < $count; $i++) {
                $alias = addslashes(trim($list[$i]['alias']));
                $name = addslashes(trim($list[$i]['name']));
                if (empty($name))
                    $name = $alias;
                if (empty($alias) && empty($name)) continue;
                $access = $list[$i]['access'] == 'true' ? 'true' : 'false';
                $tpl .= "        '$alias' => array('name' => '$name', 'access' => $access)";
                if ($i < $count - 1)
                    $tpl .= ",\n";
            }
            if ($tpl)
                $tpl = "\n$tpl\n    ";
        }
        $params['robots_list'] = $tpl;
        $params['date'] = date('Y-m-d H:i:s');
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        parent::dataAccessUpdate();

        // массив полей с их соответствующими значениями
        if (empty($params))
            $params = $this->input->getBy($this->fields);
        // проверки входных данных
        $this->isDataCorrect($params);
        // проверка существования записи с одинаковыми значениями
        $this->isDataExist($params);
        // использование системных полей в запросе SQL
        if ($this->isSysFields) {
            $params['sys_date_update'] = date('Y-m-d');
            $params['sys_time_update'] = date('H:i:s');
            $params['sys_user_update'] = $this->session->get('user_id');
        }
        // обновить файл конфигурации
        $this->fileUpdate($params);
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($params, 'method "PUT"');
        }
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        parent::dataAccessView();

        $setTo = array();

        $filename = '../' . PATH_CONFIG_CMS . $this->filename;
        // если файл настроек сайта не существует
        if (!file_exists($filename))
            throw new GException('Error', sprintf($this->_['msg_file_exist'], $filename));

        // настройки посещаемости роботами
        $settings = include($filename);
        $list = array();
        if (isset($settings['LIST'])) {
            foreach ($settings['LIST'] as $alias => $item) {
                $list[] = array('alias' => $alias, 'name' => $item['name'], 'access' => $item['access']);
            }
        }

        $this->response->add('setTo', array(
            array('id' => 'robots_list', 'value' => json_encode($list)),
            array('id' => 'robots_limit', 'value' => $settings['LIMIT'])
        ));
    }
}
?>