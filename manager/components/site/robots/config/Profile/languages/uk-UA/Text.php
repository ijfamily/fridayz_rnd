<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_update' => 'Налаштування системи',
    // поля формы
    'title_tab_interface'  => 'Персоналізація',
    'title_tab_control'    => 'Управління',
    'label_footer'         => 'Налаштування набудуть чинності тільки після виконаних змін',
    'label_type_shortcuts' => 'Вид',
    'title_shortcuts'      => 'Ярлики робочого столу',
    'label_desktop_view'   => 'Вигляд робочого столу',
    'label_view'           => 'Вид',
    'label_theme'          => 'Тема',
    'label_separator_debug' => 'Налагодження',
    'label_debug_js_core'   => 'налагодження ядра системи (JavaScript)',
    'label_debug_js'        => 'налагодження віджетів і плагінів системи (JavaScript)',
    'label_debug_php'       => 'налагодження компонентів системи (PHP)',
    'label_debug_php_errors' => 'висновок помилок (PHP)',
    'label_debug_info'       => '<div class="mn-field-comment">Для налагодження PHP сценаріїв встановіть для Mozilla Firefox розширення <a target="blank" href="http://www.firephp.org/">FirePHP</a>, <br>'
                             . 'для налагодження JavaScript - <a target="blank" href="http://www.getfirebug.com/">Firebug</a>',
    'label_separator_msg'     => 'Повідомлення',
    'label_receive_mails'     => 'отримувати системні листи від користувачів',
    'label_receive_messages'  => 'отримувати системні повідомлення від користувачів',
    'label_receive_js_errors' => 'отримувати повідомлення про помилки JavaScript',
    'label_sound'             => 'Звук',
    'label_separator_grid'    => 'Управління списком',
    'label_grid_columns'      => 'скинути положення стовпців',
    'label_grid_bg_use'       => 'використовувати в тлі списку зображення',
    'label_grid_bg_color'     => 'колір фону списку (якщо немає зображення)',
    'title_tab_date'          => 'Дата и время',
    'label_separator_date'    => 'Шаблон даты',
    'fieldset_title_day'      => 'День',
    'label_char_d'            => 'Символ <b>"d"</b>',
    'value_char_d'            => 'Day of the month, 2 digits with leading zeros<br>example: 01 to 31',
    'label_char_D'            => 'Символ <b>"D"</b>',
    'value_char_D'            => 'A textual representation of a day, three letters<br>example: Mon through Sun',
    'label_char_j'            => 'Символ <b>"j"</b>',
    'value_char_j'            => 'Day of the month without leading zeros<br>example: 1 to 31',
    'label_char_L'            => 'Символ <b>"L"</b>',
    'value_char_L'            => 'A full textual representation of the day of the week<br>example: Sunday through Saturday',
    'label_char_N'            => 'Символ <b>"N"</b>',
    'value_char_N'            => 'ISO-8601 numeric representation of the day of the week<br>example: 1 (for Monday) through 7 (for Sunday)',
    'label_char_S'            => 'Символ <b>"S"</b>',
    'value_char_S'            => 'English ordinal suffix for the day of the month, 2 characters<br>example: st, nd, rd or th. Works well with j ',
    'label_char_W'            => 'Символ <b>"W"</b>',
    'value_char_W'            => 'Numeric representation of the day of the week<br>example: 0 (for Sunday) through 6 (for Saturday)',
    'label_char_z'            => 'Символ <b>"z"</b>',
    'value_char_z'            => 'The day of the year (starting from 0)<br>example: 0 through 365',
    'fieldset_title_week'     => 'Неделя',
    'label_char_W'            => 'Символ <b>"W"</b>',
    'value_char_W'            => 'ISO-8601 week number of year, weeks starting on Monday<br>example: 42 (the 42nd week in the year)',
    'fieldset_title_month'    => 'Месяц',
    'label_char_F'            => 'Символ <b>"F"</b>',
    'value_char_F'            => 'A full textual representation of a month, such as January or March<br>example: January through December',
    'label_char_m'            => 'Символ <b>"m"</b>',
    'value_char_m'            => 'Numeric representation of a month, with leading zeros<br>example: 01 through 12',
    'label_char_M'            => 'Символ <b>"M"</b>',
    'value_char_M'            => 'A short textual representation of a month, three letters<br>example: Jan through Dec',
    'label_char_n'            => 'Символ <b>"n"</b>',
    'value_char_n'            => 'Numeric representation of a month, without leading zeros<br>example: 1 through 12',
    'label_char_t'            => 'Символ <b>"t"</b>',
    'value_char_t'            => 'Number of days in the given month<br>example: 28 through 31',
    'fieldset_title_year'     => 'Год',
    'label_char_L'            => 'Символ <b>"L"</b>',
    'value_char_L'            => 'Whether it\'s a leap year<br>example: 1 if it is a leap year, 0 otherwise.',
    'label_char_O'            => 'Символ <b>"O"</b>',
    'value_char_O'            => 'ISO-8601 year number. This has the same value as Y, except that if the ISO week number (W) belongs to the previous or next year, that year is used instead. <br>example: 1999 or 2003',
    'label_char_Y'            => 'Символ <b>"Y"</b>',
    'value_char_Y'            => 'A full numeric representation of a year, 4 digits<br>example: 1999 or 2003',
    'label_char_y'            => 'Символ <b>"y"</b>',
    'value_char_y'            => 'A two digit representation of a year<br>example: 99 or 03',
    'fieldset_title_time'     => 'Время',
    'label_char_a'            => 'Символ <b>"a"</b>',
    'value_char_a'            => 'Lowercase Ante meridiem and Post meridiem<br>example: am or pm',
    'label_char_A'            => 'Символ <b>"A"</b>',
    'value_char_A'            => 'Uppercase Ante meridiem and Post meridiem<br>example: AM or PM',
    'label_char_B'            => 'Символ <b>"B"</b>',
    'value_char_B'            => 'Swatch Internet time<br>example: 000 through 999',
    'label_char_g'            => 'Символ <b>"g"</b>',
    'value_char_g'            => '12-hour format of an hour without leading zeros<br>example: 1 through 12',
    'label_char_G'            => 'Символ <b>"G"</b>',
    'value_char_G'            => '24-hour format of an hour without leading zeros<br>example: 0 through 23',
    'label_char_h'            => 'Символ <b>"h"</b>',
    'value_char_h'            => '12-hour format of an hour with leading zeros<br>example: 01 through 12',
    'label_char_H'            => 'Символ <b>"H"</b>',
    'value_char_H'            => '24-hour format of an hour with leading zeros<br>example: 00 through 23',
    'label_char_i'            => 'Символ <b>"i"</b>',
    'value_char_i'            => 'Minutes with leading zeros<br>example: 00 to 59',
    'label_char_s'            => 'Символ <b>"s"</b>',
    'value_char_s'            => 'Seconds, with leading zeros<br>example: 00 through 59',
    'label_char_u'            => 'Символ <b>"u"</b>',
    'value_char_u'            => 'Microseconds<br>example: 54321',
    'label_format_date'       => 'Формат даты',
    'label_format_time'       => 'Формат времени',
    'label_separator_time'    => 'Шаблон времени',
    'label_format_datetime'   => 'Формат даты и времени',
    // типы
    'data_shortcuts' => array(array(1, 'Ярлики'), array(2, 'Компоненти')),
    'data_php_errors' => array(
        array(0, 'не виводити'),
        array(30719, 'все'),
        array(7, 'помилки, попередження'),
        array(15, 'помилки, попередження, повідомлення'),
        array(30711, 'всі окрім повідомлень')
    )
);
?>