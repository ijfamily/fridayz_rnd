<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля настройки посещаемости сайта роботами"
 * Пакет контроллеров "Настройка посещаемости сайта роботами"
 * Группа пакетов     "Настройки"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SConfRobots_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля настройки посещаемости сайта роботами
 * 
 * @category   Gear
 * @package    GController_SConfRobots_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SRobotsConfig_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // Ext_Window (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'           => $this->_['title_profile_update'],
                  'titleEllipsis'   => 40,
                  'width'           => 640,
                  'autoHeight'      => true,
                  'resizable'       => false,
                  'btnDeleteHidden' => true,
                  'stateful'        => false,
                  'buttonsAdd'      => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-site-robots-config',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );
        $this->_cmp->state = 'update';

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->url = $this->componentUrl . 'profile/';
        $form->labelWidth = 270;
        $form->defaults = array('labelSeparator' => '');
        $form->items->addItems(
            array(
                array('xtype' => 'label',
                      'html'  => $this->_['html_desc']),
                array('xtype'     => 'mn-field-grid-editor',
                      'id'        => 'robots_list',
                      'name'      => 'robots_list',
                      'hideLabel' => true,
                      'anchor'    => '100%',
                      'height'    => 250,
                      'checkDirty' => false,
                      'fields'    => array('alias', 'name', 'access'),
                      'columns'   => array(
                          array('header'    => $this->_['header_alias'],
                                'dataIndex' => 'alias',
                                'width'     => 150,
                                'sortable'  => true),
                          array('header'    => $this->_['header_name'],
                                'dataIndex' => 'name',
                                'width'     => 200,
                                'sortable'  => true),
                          array('header'    => $this->_['header_access'],
                                'dataIndex' => 'access',
                                'width'     => 200,
                                'sortable'  => true),
                      )
                ),
                array('xtype'      => 'numberfield',
                      'fieldLabel' => $this->_['label_robots_limit'],
                      'id'         => 'robots_limit',
                      'name'       => 'robots_limit',
                      'checkDirty' => false,
                      'value'      => 10000,
                      'width'      => 70,
                      'allowBlank' => true,
                      'emptyText'  => 0)
            )
        );

        parent::getInterface();
    }
}
?>