<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: index.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // сообщения
    'Upload data'            => 'Загрузка данных',
    'Is successful uploaded' => 'загрузка выполнена успешно!',
    'File is empty'          => 'не выбран файл для загрузки',
    'File exist %s'          => 'Файл с названием "%s" уже существует на сервере!',
    'Delete file'            => 'Удаление файла',
    'Is successful deleted'  => 'удаление файла выполнена успешно!',
    'File name is emtpy'     => 'Неуказано название файла!',
    'File type is empty'     => 'Неуказан тип файла!',
);
?>