<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные редактора статьи"
 * Пакет контроллеров "Редактор статьи"
 * Группа пакетов     "Статьи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SArticles_Editor
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('File');
Gear::controller('Profile/Data');

/**
 * Данные для интерфейса профиля статьи
 * 
 * @category   Gear
 * @package    GController_SArticles_Editor
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SArticles_Editor_Data extends GController_Profile_Data
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sarticles_grid';

    /**
     * Каталог изображений
     *
     * @var string
     */
    public $pathImages = 'data/images/';

    /**
     * Каталог документов
     *
     * @var string
     */
    public $pathDocs = 'data/docs/';

    /**
     * Название загруженного файла
     *
     * @var string
     */
    protected $_filename = '';

    /**
     * Тип загруженного файла
     *
     * @var string
     */
    protected $_type = '';

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        Gear::library('String');

        // если нет файла для загрузки
        if (!$this->input->hasFile())
            throw new GException($this->_['Upload data'], $this->_['File is empty']);
        if (($file = $this->input->file->get('file_name')) === false)
            throw new GException($this->_['Upload data'], $this->_['File is empty']);
        if (($this->_type = $this->input->get('file_type', false)) === false)
            throw new GException($this->_['Upload data'], $this->_['File type is empty']);
        // если выбран флажок "Сгенерировать название файла"
        $isGen = $this->input->get('file_generate', 0);
        // определение имени файла
        if ($isGen)
            $this->_filename = $this->input->file->createName('file_name', '', true, 'uniqid');
        else {
            $fileExt = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
            $fileId = GString::toUrl(pathinfo($file['name'], PATHINFO_FILENAME),  $this->config->getFromCms('Site', 'LANGUAGE/ALIAS'));
            $fileId = strtolower($fileId);
            $this->_filename = $fileId . '.' . $fileExt;
        }
        // проверка существования файла
        switch ($this->_type) {
            // изображение
            case 'image':
                $pathToFile = '../' . $this->pathImages;
                break;

            // документ
            case 'document':
                $pathToFile = '../' . $this->pathDocs;
                break;

            default:
                throw new GException($this->_['Upload data'], $this->_['File type is empty']);
        }
        // сгенерированное название папки файлов
        $folderId = $this->store->get('folder', 'gcontroller_sarticles_grid');
        // если папка создана ранее
        $pathToFile = $pathToFile . $folderId . '/';
        if (!file_exists($pathToFile)) {
            if (mkdir($pathToFile, 0700) === false)
                throw new GException($this->_['Upload data'], sprintf($this->_['File exist %s'], $pathToFile));
        }
        $filename = $pathToFile . $this->_filename;
        if (file_exists($filename))
            throw new GException($this->_['Upload data'], sprintf($this->_['File exist %s'], $this->_filename));
        // загрузка файла
        GFile::upload($file, $filename);
    }

    /**
     * Вставка данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataInsert($params = array())
    {
        $this->dataAccessInsert();

        $this->response->setMsgResult($this->_['Upload data'], $this->_['Is successful uploaded'], true);
        // проверки входных данных
        $this->isDataCorrect($params);
        // сгенерированное название папки файлов
        $folderId = $this->store->get('folder', 'gcontroller_sarticles_grid');
        switch ($this->_type) {
            // изображение
            case 'image': $url = '/' . $this->pathImages . $folderId . '/' . $this->_filename; break;

            // документ
            case 'document': $url = '/' . $this->pathDocs . $folderId . '/' . $this->_filename; break;
        }
        $this->response->set('url', $url);
        $this->response->set('filename', $this->_filename);
    }

    /**
     * Удаление данных
     * 
     * @return void
     */
    protected function dataDelete()
    {
        parent::dataAccessDelete();

        $this->response->setMsgResult($this->_['Delete file'], $this->_['Is successful deleted'], true);

        // сгенерированное название папки файлов
        $folderId = $this->store->get('folder', 'gcontroller_sarticles_profile');

        // если не указан файл
        if (!($name = $this->input->get('file_name', false)))
            throw new GException('Error', $this->_['File name is emtpy']);
        $name = pathinfo($name, PATHINFO_BASENAME);
        // если не указан тип файла
        if (!($type = $this->input->get('file_type', false)))
            throw new GException('Error', $this->_['File type is emtpy']);
        switch ($type) {
            case 'image': $path = $this->pathImages; break;
            case 'document': $path = $this->pathDocs; break;
            default: $path = '';
        }
        if (empty($path))
            throw new GException('Error', $this->_['File type is emtpy']);

        // удаление файла
        GFile::delete('../' . $path . $folderId . '/' . $name);
    }
}
?>