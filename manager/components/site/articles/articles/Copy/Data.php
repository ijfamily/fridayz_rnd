<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные интерфейса копии статьи"
 * Пакет контроллеров "Копия статьи"
 * Группа пакетов     "Статьи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SArticles_Copy
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные интерфейса копии статьи
 * 
 * @category   Gear
 * @package    GController_SArticles_Copy
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SArticles_Copy_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array('article_uri', 'article_note', 'page_header');

    /**
     * Первичное поле таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'article_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_articles';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sarticles_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        if (empty($record['page_header']))
            $title = $record['article_uri'];
        else
            $title = $record['page_header'];

        return sprintf($this->_['title_profile_update'], $title);
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        $sql = 'SELECT `a`.*, `p`.`page_header` '
             . 'FROM `site_articles` `a` LEFT JOIN `site_pages` `p` ON `a`.`article_id`=`p`.`article_id` AND `p`.`language_id`=' . $this->config->getFromCms('Site', 'LANGUAGE/ID')
             . ' WHERE `a`.`article_id`=' . $this->uri->id;

        parent::dataView($sql);
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        $this->dataAccessUpdate();

        $query = new GDb_Query();
        // вабранная статья
        $sql = 'SELECT * FROM `site_articles`WHERE `article_id`=' . $this->uri->id;
        // обновление данных статьи
        if (($article = $query->getRecord($sql)) === false)
            throw GSqlException();

        $article['article_note'] = $this->input->get('article_note');
        $article['article_uri'] = $this->input->get('article_uri');
        $article['sys_date_update'] = null;
        $article['sys_time_update'] = null;
        $article['sys_date_insert'] = date('Y-m-d');
        $article['sys_time_insert'] = date('H:i:s');
        $article['sys_user_insert'] = $this->session->get('user_id');
        $article['published_date'] = $article['sys_date_insert'];
        $article['published_time'] = $article['sys_time_insert'];
        $article['published_user'] = $article['sys_user_insert'];
        unset($article['article_id']);
        unset($article['article_folder']);

        $table = new GDb_Table('site_articles', 'article_id');
        // добавить статью
        if ($table->insert($article) === false)
            throw new GSqlException();

        // идинд. добавленной статьи
        $articleId = $table->getLastInsertId();
        // создание копий локализаци
        $table = new GDb_Table('site_pages', 'page_id');
        $sql = 'SELECT * FROM `site_pages` WHERE `article_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw GSqlException();
        while (!$query->eof()) {
            $page = $query->next();
            $page['article_id'] = $articleId;
            $page['page_header'] = $this->_['copy'] . $page['page_header'];
            unset($page['page_id']);
            // добавить локализацию
            if ($table->insert($page) === false)
                throw new GSqlException();
        }
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        $record['article_note'] = $this->_['copy'] . $record['page_header'];
        $record['article_uri'] = 'copy-' . $record['article_uri'];

        return $record;
    }
}
?>