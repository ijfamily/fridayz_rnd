<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс копии статьи"
 * Пакет контроллеров "Копия статьи"
 * Группа пакетов     "Статьи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SArticles_Copy
 * @copyright  Copyright (c) 2013-2016 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс копии статьи
 * 
 * @category   Gear
 * @package    GController_SArticles_Copy
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SArticles_Copy_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => '',
                  'id'            => $this->classId,
                  'titleEllipsis' => 100,
                  'gridId'        => 'gcontroller_sarticles_grid',
                  'width'         => 780,
                  'autoHeight'    => true,
                  'resizable'     => false,
                  'btnDeleteHidden' => true,
                  'btnUpdateText'   => $this->_['text_btn_update'],
                  'stateful'        => false)
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_article_note'],
                  'labelTip'   => $this->_['tip_article_note'],
                  'name'       => 'article_note',
                  'checkDirty' => false,
                  'maxLength'  => 255,
                  'width'      => 651,
                  'itemCls'    => 'mn-form-item-quiet',
                  'allowBlank' => true),
            array('xtype'      => 'fieldset',
                  'anchor'     => '100%',
                  'labelWidth' => 62,
                  'title'      => $this->_['title_fieldset_address'],
                  'autoHeight' => true,
                  'cls'        => 'mn-container-clean',
                  'layout'     => 'column',
                  'items'      => array(
                      array('layout' => 'form',
                            'width'  => 685,
                            'items'  => array(
                                  array('xtype'      => 'textfield',
                                        'id'         => 'fldArticleUriCopy',
                                        'fieldLabel' => $this->_['label_article_uri'],
                                        'labelTip'   => $this->_['tip_article_uri'],
                                        'name'       => 'article_uri',
                                        'checkDirty' => false,
                                        'blankText'  => $this->_['blank_article_uri'],
                                        'maxLength'  => 255,
                                        'anchor'     => '100%',
                                        'allowBlank' => true)
                              )
                        ),
                        array('layout'    => 'form',
                              'width'     => 33,
                              'bodyStyle' => 'margin-left:3px;',
                              'items'     => array(
                                  array('xtype'   => 'mn-btn-genurl',
                                        'type'    => 1,
                                        'fldAddress' => 'fldArticleUriCopy',
                                        //'tooltip' => $this->_['tip_button_genurl'],
                                        'icon'    => $this->resourcePath . 'icon-btn-gear.png',
                                        'url'     => $this->componentUrl . '../' . ROUTER_DELIMITER . 'generator/data/',
                                        /*'msgSelectHeader' => sprintf($this->_['msg_select_header'], $languageName),
                                        'msgSelectNode'   => $this->_['msg_select_node']*/)
                              )
                        )
                  )
            )
        );

        // Ext_Form_DataProfile (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->labelWidth = 73;
        $form->url = $this->componentUrl . 'copy/';
        $form->items->add($items);

        parent::getInterface();
    }
}
?>