<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_update'     => 'Creating a copy of an article "%s"',
    // поля формы
    'text_btn_update'          => 'Copy',
    'label_article_note'       => 'Note',
    'tip_article_note'         => 'Displayed only in the system of administration in the form of notes to the user',
    'empty_article_note'       => 'A copy of the article',
    'label_particle_note'      => 'Parent',
    'tip_particle_note'        => 'Used to create maps and site structure',
    'html_particle'            => 'Necessary for the proper construction of the site structure<br/><br/>',
    'title_fieldset_particles' => 'The new position within the structure of articles',
    'title_fieldset_address'   => 'The new address of article',
    'label_article_uri'        => 'URL resource',
    'tip_article_uri'          => 'This is part of the URL address of a kind "http://simple.com/a/b/c/d.html", where URI - "/a/b/c/d.html". If the main page - that "/"',
    'blank_article_uri'        => 'This field is required if the path is not specified, the value must be "/"',
    'copy'                     => 'Copy'
);
?>