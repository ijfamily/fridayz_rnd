<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_update'     => 'Создание копии статьи "%s"',
    // поля формы
    'tip_button_genurl'      => 'Генерация ЧПУ URL статьи',
    'msg_select_header'      => 'Заполните поле &quot;адрес статьи&quot;!',
    'msg_select_node'        => 'Выберите &quot;категорию&quot; статьи!',
    'text_btn_update'        => 'Копировать',
    'label_article_note'     => 'Заметка<hb></hb>',
    'tip_article_note'       => 'Отображается только в системе администрирования в виде заметки пользователю',
    'empty_article_note'     => 'Копия статьи',
    'title_fieldset_address' => 'Новый ЧПУ URL статьи',
    'label_article_uri'      => 'адрес<hb></hb>',
    'tip_article_uri'        => 'Это часть URL адреса вида "http://simple.com/a/b/c/d.html", где URI - "/a/b/c/d.html". Если главная страница - то "/"',
    'blank_article_uri'      => 'Это поле обязательно для заполнения, если путь не указан, значение должно быть "/"',
    'copy'                   => 'Копия - '
);
?>