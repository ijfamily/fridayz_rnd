<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_update'     => 'Створення копії статті "%s"',
    // поля формы
    'text_btn_update'          => 'Копіювати',
    'label_article_note'       => 'Примітка',
    'tip_article_note'         => 'Відображається лише в системі адміністрування у вигляді замітки користувачеві',
    'empty_article_note'       => 'Копія статті',
    'label_particle_note'      => 'Предок',
    'tip_particle_note'        => 'Використовується для формування карти і структури сайту',
    'html_particle'            => 'Необхідно для правильної побудови структури сайту<br/><br/>',
    'title_fieldset_particles' => 'Нове положення в структурі статей',
    'title_fieldset_address'   => 'Нова адреса статті',
    'label_article_uri'        => 'URI ресурс',
    'tip_article_uri'          => 'Це частина URL адреси виду "http://simple.com/a/b/c/d.html", де URI - "/a/b/c/d.html". Якщо головна сторінка - то "/"',
    'blank_article_uri'        => 'Це поле обов, якщо шлях не вказаний, значення має бути "/"',
    'copy'                     => 'Копія'
);
?>