<?php
/**
 * Gear Manager
 *
 * Контроллер         "Просмотр статьи сайта"
 * Пакет контроллеров "Статьи"
 * Группа пакетов     "Статьи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Distributed under the Lesser General Public License (LGPL)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    GController_SArticles_View
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('String');
Gear::library('Article');
Gear::ext('Container/Panel');
Gear::controller('Interface');

/**
 * Просмотр статьи сайта
 * 
 * @category   Gear
 * @package    GController_SArticles_View
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SArticles_View_Interface extends GController_Interface
{
    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // панель (ExtJS class "Ext.Panel")
        $this->_cmp = new Ext_Panel(
            array('id'        => strtolower(__CLASS__),
                  'closable'  => true,
                  'component' => array('container' => 'content-tab', 'destroy' => true))
        );
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // соединение с базой данных
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();
        // выбранная статья
        $query = new GDb_Query();
        $sql = 'SELECT * FROM `site_articles` WHERE `article_id`=' . $this->uri->id;
        if (($article = $query->getRecord($sql)) === false)
            throw new GSqlException();
        // если есть категория
        $catUri = '';
        if (!empty($article['category_id'])) {
            $sql = 'SELECT * FROM `site_categories` WHERE `category_id`=' . $article['category_id'];
            if (($cat = $query->getRecord($sql)) === false)
                throw new GSqlException();
            $catUri = $cat['category_uri'];
        }

        // управление сайтами на других доменах
        $domains = array();
        if ($this->config->getFromCms('Site', 'OUTCONTROL'))
            $domains = $this->config->getFromCms('Domains', 'indexes');
        $domains[0] = array($this->config->getFromCms('Site', 'HOST'), $this->config->getFromCms('Site', 'NAME'));
        if (isset($domains[$article['domain_id']]))
            $host = $domains[$article['domain_id']][0];
        else
            $host = $_SERVER['SERVER_NAME'];

        $url = GArticle::getUrl($host, $article['type_id'], $article['article_id'], $article['article_uri'], $catUri, $this->config->getFromCms('Site', 'SEF'));

        // панель (ExtJS class "Ext.Panel")
        $this->_cmp->iconSrc = $this->resourcePath . 'icon.png';
        $this->_cmp->title = sprintf($this->_['title'], GString::ellipsis($article['article_uri'], 20));
        $this->_cmp->tabTip = $article['article_uri'];
        $this->_cmp->items->add(array('xtype' => 'mn-iframe', 'url' =>  $url));

        parent::getInterface();
    }
}
?>