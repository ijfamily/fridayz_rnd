<?php
/**
 * Gear Manager
 *
 * Контроллер         "Изменение профиля записи статьи"
 * Пакет контроллеров "Профиль статьи"
 * Группа пакетов     "Статьи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SArticles_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Field.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Field');

/**
 * Изменение профиля записи статьи
 * 
 * @category   Gear
 * @package    GController_SArticles_Profile
 * @subpackage Field
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Field.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SArticles_Profile_Field extends GController_Profile_Field
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array('published', 'article_archive', 'article_rss', 'article_map', 'article_caching');

    /**
     * Первичный ключ таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'article_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_articles';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sarticles_grid';

    /**
     * Каталог кэша
     *
     * @var string
     */
    public $pathCache = '../cache/';

    /**
     * Удаление кэша
     * 
     * @return void
     */
    protected function cacheClear($id)
    {
        Gear::library('File');

        $query = new GDb_Query();
        $sql = "SELECT * FROM `site_cache` WHERE `cache_generator`='article' AND `cache_generator_id`=$id";
        if ($query->execute($sql) === false)
            throw new GSqlException();
        while (!$query->eof()) {
            $cache = $query->next();
            // если есть кэш
            if ($cache['cache_filename']) {
                GFile::delete($this->pathCache . $cache['cache_filename'], false);
            }
        }
        $sql = "DELETE FROM `site_cache` WHERE `cache_generator`='article' AND `cache_generator_id`=$id";
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Событие наступает после успешного обновления данных
     * 
     * @param array $data сформированные данные полученные после запроса к таблице
     * @return void
     */
    protected function dataUpdateComplete($data = array())
    {
        if (isset($data['article_caching'])) {
            if ($data['article_caching'] == 0) {
                $this->cacheClear($this->uri->id);
            }
        }
    }
}
?>