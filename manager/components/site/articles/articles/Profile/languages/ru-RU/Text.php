<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: index.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Создание записи "Статья"',
    'title_profile_update' => 'Изменение записи "%s"',
    'text_wnd_copy'        => 'Копировать',
    'tooltip_wnd_copy'     => 'Создание копии статьи',
    'text_btn_help'        => 'Справка',
    // поля формы
    // вкладка "атрибуты статьи"
    'label_domain'           => 'Сайт',
    'title_node_site'        => 'Сайт',
    'title_tab_articles'     => 'Статья - %s',
    'title_tab_attributes'   => 'Атрибуты',
    'tip_button_genurl'      => 'Генерация ЧПУ URL статьи',
    'msg_select_header'      => 'Заполните поле &quot;адрес статьи&quot;!',
    'msg_select_node'        => 'Выберите &quot;категорию&quot; статьи!',
    'label_article_note'     => 'Заметка<hb></hb>',
    'tip_article_note'       => 'Отображается только в системе администрирования в виде заметки пользователю',
    'label_category_name'    => 'Категория<hb></hb>',
    'tip_category_name'      => 'Используется для логического разделения на "Статьи" и "Новости" (указывайте "Статьи", иначе статья будет отображаться в компоненте "Новости")',
    'label_access_name'      => 'Доступ<hb></hb>',
    'tip_access_name'        => 'Используется для разделения прав между пользователями (в том случаи если статьи необходимо отображать только авторизованным пользователям)',
    'title_fieldset_address' => 'ЧПУ URL статьи',
    'label_article_uri'      => 'адрес<hb></hb>',
    'tip_article_uri'        => 'Это часть URL адреса вида "http://yourhost.com/a/b/c/d.html", где путь - "/a/b/c". Если главная страница - то "/"',
    'blank_article_uri'      => 'Это поле обязательно для заполнения, если путь не указан, значение должно быть "/"',
    'title_fieldset_part'    => 'Положение в структуре статей',
    'label_part_name'        => 'Предок',
    'label_article_tree'     => 'в структуре',
    'tip_article_tree'       => 'Если выбран флажок  "В структуре" - статья будет отображаться в дереве сайта',
    'tip_part_name'          => 'Используется для формирования карты и структуры сайта',
    'title_fieldset_public'  => 'Дата публикации статьи',
    'title_fieldset_params'  => 'Дополнительные параметры',
    'label_published_date'   => 'Дата',
    'label_published_time'   => 'Время',
    'label_article_rec'      => 'Рекомендуемая',
    'tip_article_rec'        => 'Используется как дополнительная фильтрация статей по этому полю, если этот вид статей задействован на сайте',
    'label_article_rss'      => 'В ленте RSS<hb></hb>',
    'tip_article_rss'        => 'Используется для создания ленты RSS',
    'label_article_map'      => 'Карта<hb></hb>',
    'tip_article_map'        => 'Будет отображаться на странице карты сайта',
    'label_article_header'   => 'Загаловок<hb></hb>',
    'tip_article_header'     => 'Отображение загаловка статьи на странице',
    'label_article_archive'  => 'Архив<hb></hb>',
    'tip_article_archive'    => 'Если статья находится в архиве, будет доступна пользователю, но не будет отображаться на сайте',
    'label_article_hidden'   => 'Скрытая<hb></hb>',
    'tip_article_hidden'     => 'Если статья скрытая, будет не доступна пользователю и не будет отображаться на сайте',
    'label_article_sitemap'  => 'Карта сайта<hb></hb>',
    'tip_article_sitemap'    => 'Используется для отображения статьи на карте сайта (если она задействована)',
    'label_article_search'   => 'Поиск<hb></hb>',
    'tip_article_search'     => 'Используется для поиска статьи (если поиск задействован на сайте)',
    'label_article_print'    => 'Печать<hb></hb>',
    'tip_article_print'      => 'Если выставлен флаг - при клике на значке "печать" (если предусмотрено в шаблоне статьи) открывается всплывающее окно с текстом статьи для печати',
    'label_article_index'    => 'Порядок<hb></hb>',
    'tip_article_index'      => 'Порядковый номер (если используется в списках)',
    'label_article_visits'   => 'Посещений<hb></hb>',
    'tip_article_visits'     => 'Количество посещений страницы пользователями',
    'title_fieldset_sm'      => 'Карта сайта (протокол Sitemap)',
    'label_sm_priority'      => 'Приоритетность<hb></hb>',
    'tip_sm_priority'        => 'Приоритетность URL относительно других URL на Вашем сайте. Допустимый диапазон значений — от 0,0 до 1,0.',
    'label_sm_changefreq'    => 'Частота<hb></hb>',
    'tip_sm_changefreq'      => 'Это значение предоставляет общую информацию для поисковых систем и может не соответствовать точно частоте сканирования этой страницы.',
    'title_fieldset_cache'   => 'Кэш страницы (создан: %s)',
    'label_article_caching'  => 'Кэширование<hb></hb>',
    'tip_article_caching'    => 'Используется для кэширования всей страницы сайта',
    'label_template_name'    => 'Шаблон<hb></hb>',
    'tip_template_name'      => 'Шаблон отображения статьи',
    // кэш страницы
    'msg_cache_delete'     => 'Вы действительно желаете удалить кэш страницы?',
    'text_cache_delete'    => 'Удалить кэш',
    'tip_cache_delete'     => 'Удаление кэша статьи',
    'title_deleting_cache' => 'Удаление кэша статьи',
    'msg_deleted_cache'    => 'Все файлы кэша статьи были успешно удалены!',
    'label_fld_date'    => 'Дата создания',
    'label_fld_file'    => 'Файл на сервере',
    'label_fld_address' => 'URL адрес страницы',
    'label_published'      => 'Опубликовать статью на сайте',
    'label_published_feed' => 'Опубликовать статью на ленте<hb></hb>',
    'tip_published_feed'   => 'Публикация статьи на одной из лент (новостей)',
    'label_published_main' => 'Опубликовать статью на главной<hb></hb>',
    'tip_published_main'   => 'Публикация статьи на главной странице сайта (на одном из компонентов главной страницы)',
    'label_type_name'      => 'Тип страницы',
    'title_fieldset_announce' => 'Дата анонса статьи или события',
    // вкладка "seo статьи"
    'title_fieldset_meta' => 'Метатеги статьи',
    'title_tab_seo'     => 'SEO',
    'title_fieldset_r'  => 'Роботы',
    'label_page_meta_r' => 'Роботы<hb></hb>',
    'tip_page_meta_r'   => 'Формирует информацию о гипертекстовых документах, которая поступает к роботам поисковых систем. 
                            Значения тега могут быть следующими: Index (страница должна быть проиндексирована), Noindex (документ
                            не индексируется), Follow (гиперссылки на странице отслеживаются), Nofollow (гиперссылки не прослеживаются),
                            All (включает значения index и follow, включен по умолчанию), None (включает значения noindex и nofollow).',
    'label_page_meta_v' => 'Время и интервал<hb></hb>',
    'tip_page_meta_v'   => 'Время и интервал посещения поискового робота. Позволяет управлять частотой индексации документа в поисковой системе.',
    'label_page_meta_m' => 'Состояние<hb></hb>',
    'tip_page_meta_m'   => 'Значение «Static» отмечает, что системе нет необходимости индексировать документ в дальнейшем,
                            «Dynamic» позволяет регулярно индексировать Интернет-страницу',
    'title_fieldset_a'  => 'Авторское право',
    'label_page_meta_a' => 'Автор<hb></hb>',
    'tip_page_meta_a'   => 'Идентификация автора или принадлежности документа. Содержит имя автора Интернет-страницы, в том случае, 
                            если сайт принадлежит какой-либо организации, целесообразнее использовать поле «Организация».',
    'label_page_meta_c' => 'Организация<hb></hb>',
    'tip_page_meta_c'   => 'Идентификация принадлежности документа к какой-либо организации',
    'label_page_meta_k' => 'Ключевые слова<hb></hb><br><small>(keywords)</small>',
    'tip_page_meta_k'   => 'Поисковые системы используют для того, чтобы определить релевантность ссылки. При формировании данного тега 
                            необходимо использовать только те слова, которые содержатся в самом документе. Использование тех слов, которых 
                            нет на странице, не рекомендуется. Рекомендованное количество слов в данном теге — не более десяти.',
    'label_page_meta_d' => 'Описание<hb></hb><br><small>(description)</small>',
    'tip_page_meta_d'   => 'Используется поисковыми системами для индексации, а также при создании аннотации в выдаче по запросу
                            При отсутствии тега поисковые системы выдают в аннотации первую строку документа или отрывок, содержащий ключевые слова.
                            Отображается после ссылки при поиске страниц в поисковике.',
    'label_page_title'  => 'Загаловок<hb></hb><br><small>(title)</small>',
    'tip_page_title'    => 'Отображается в названии вкладки браузера',
    // вкладка "страница"
    'title_tab_page'        => 'Страница',
    'label_page_header'     => 'Заголовок<hb></hb>',
    'tip_page_header'       => 'Отображается в заголовке статьи',
    'label_page_menu'       => 'Меню<hb></hb>',
    'tip_page_menu'         => 'Название пункта меню (только для динамического меню)',
    'label_page_breadcrumb' => 'Цепочка<hb></hb>',
    'tip_page_breadcrumb'   => 'Название в "цепочке" статей (если "цепочка" задействована)',
    'label_page_tags' => 'Теги<hb></hb>',
    'tip_page_tags'   => 'используются для быстрой фильтрации списка статей (записываются через пробел)',
    'label_page_html_short' => 'Текст<hb></hb>',
    'tip_page_html_short'   => 'Отображается в списке статей ввиде краткого описания',
    'title_fieldset_shorttext' => 'Краткое описание статьи',
    'label_page_image'        => 'Изображение',
    'title_fieldset_widgets'  => 'Виджеты статьи (скрипты и таблицы стилей)',
    'label_page_html_widgets' => 'Текст<hb></hb>',
    'tip_page_html_widgets'   => 'Скрипты (Java Script) и таблицы стилей (CSS) подключаемых из внешних сайтов',
    // владка "текст"
    'title_tab_article'   => 'Текст',
    'msg_article_landing' => 'Статья содержит Landing блоки и представлена в виде Landing страницы, поэтому текст статьи невозможно изменить. Если Вы желаете вносить правки в статью через текстовый редактор, тогда Вам необходимо удалить Landing блоки в компоненте "Landing страницы".',
    // тип
    'data_template_default' => 'Статья',
    'data_access_default'   => 'Полный',
    'default_parent_name'   => 'Сайт',
    'data_type_default'     => 'Статья',
    'data_priorities' => array(
        array('0.0'), array('0.1'), array('0.2'), array('0.3'), array('0.4'), array('0.5'), array('0.6'),
        array('0.7'), array('0.8'), array('0.9'), array('1.0')
    ),
    'data_changefreq' => array(
        array('всегда', 'a'), array('почасовая', 'h'), array('ежедневно', 'd'), array('еженедельно', 'w'), array('ежемесячно', 'm'),
        array('ежегодно', 'y'), array('никогда', 'n')
    ),
    'data_changefreq_a' => array('a' => 'всегда', 'h' => 'почасовая', 'd' => 'ежедневно', 'w' => 'еженедельно', 'm' => 'ежемесячно', 'y' => 'ежегодно', 'n' => 'никогда'),
    'data_robots' => array(
        array('all'), array('index, follow'), array('noindex, follow'), array('index, nofollow'), array('noindex, nofollow'),
        array('none')
    ),
    'data_revisit' => array(
        array('1 день', 1), array('2 дня', 2), array('3 дня', 3), array('4 дня', 4), array('5 дней', 5), array('6 дней', 6), array('7 дней', 7)
    ),
    'data_doc' => array(array('Static'), array(' Dynamic')),
    'data_depends' => array('"Статьи" (%s)', '"Галереи" (%s)')
);
?>