<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные для интерфейса профиля статьи"
 * Пакет контроллеров "Профиль статьи"
 * Группа пакетов     "Статьи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SArticles_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');
Gear::library('Article');
Gear::library('Component');

/**
 * Данные для интерфейса профиля статьи
 * 
 * @category   Gear
 * @package    GController_SArticles_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SArticles_Profile_Data extends GController_Profile_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array(
        'domain_id', 'article_folder', 'category_id', 'access_id', 'template_id', 'type_id', 'article_form', 'article_index', 'article_visits', 'article_note', 'article_uri', 
        'article_caching', 'article_sitemap', 'article_search', 'article_archive', 'article_print',
        'article_rss', 'article_map', 'article_header', 'article_landing', 'sitemap_changefreq', 'sitemap_priority', 'published',
        'published_main', 'published_feed', 'published_date', 'published_time', 'published_user',
        'announce_date', 'announce_time'
    );

    /**
     * Первичный ключ таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'article_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_articles';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sarticles_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        if (empty($record['page_header']))
            $title = $record['article_uri'];
        else
            $title = $record['page_header'];

        return sprintf($this->_['title_profile_update'], $title);
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        $sql = 'SELECT `a`.*, `p`.`page_header` '
             . 'FROM `site_articles` `a` LEFT JOIN `site_pages` `p` ON `a`.`article_id`=`p`.`article_id` AND `p`.`language_id`=' . $this->config->getFromCms('Site', 'LANGUAGE/ID')
             . ' WHERE `a`.`article_id`=' . $this->uri->id;

        parent::dataView($sql);
    }

    /**
     * Добавление в журнал действий пользователей
     * (вывод данных)
     * 
     * @param array $params параметры журнала
     * @return void
     */
    protected function logView($params = array())
    {
        if (isset($params['log_query_params']['page_html']))
            $params['log_query_params']['page_html'] = '[HTML]';
        if (isset($params['page_html_plain']['page_html']))
            $params['log_query_params']['page_html_plain'] = '[TEXT]';

        parent::logView($params);
    }

    /**
     * Добавление в журнал действий пользователей
     * (вставка данных)
     * 
     * @param array $params параметры журнала
     * @return void
     */
    protected function logInsert($params = array())
    {
        if (isset($params['log_query_params']['page_html']))
            $params['log_query_params']['page_html'] = '[HTML]';
        if (isset($params['page_html_plain']['page_html']))
            $params['log_query_params']['page_html_plain'] = '[TEXT]';

        parent::logInsert($params);
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        // удаление данны статьи
        GArticle::delete($this->uri->id);
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        // категория статьи
        if (isset($params['category_id']))
            if ($params['category_id'] == -1)
                $params['category_id'] = null;
        // дата публикации статьи
        $date = $this->input->getDate('published_date', 'Y-m-d', false);
        if (!$date)
            $params['published_date'] = date('Y-m-d');
        else
            $params['published_date'] = $date;
        // время публикации статьи
        $time = $this->input->get('published_time', false);
        if (!$time)
            $params['published_time'] = date('H:i:s');
        // кто опубликовал
        $params['published_user'] = $this->session->get('user_id');
        // дата анонса статьи
        $params['announce_date'] = $this->input->getDate('announce_date', 'Y-m-d', false);
        // хэш uri
        $uri = $this->input->get('article_uri',  false);
        if ($uri !== false)
            $params['article_uri_hash'] = md5($uri);
    }

    /**
     * Вставка текста статьи
     * 
     * @param  integer $articleId идент. статьи
     * @return array 
     */
    protected function textInsert($articleId)
    {
        $ln = $this->input->get('ln', false);
        if (empty($ln)) return;

        Gear::library('String');

        $table = new GDb_Table('site_pages', 'page_id');
        // допустимые поля
        $fields = array(
            'page_html', 'page_html_plain', 'page_html_length', 'page_html_short', 'page_html_widgets', 'page_breadcrumb',
            'page_title', 'page_header', 'page_menu', 'page_rss', 'page_image', 'page_meta_keywords', 'page_meta_description', 'page_meta_robots',
            'page_meta_author', 'page_meta_copyright', 'page_meta_revisit', 'page_meta_document', 'page_tags'
        );
        $componentAttrs = $this->store->get('components');
        // список доступных языков
        $list = $this->config->getFromCms('Site', 'LANGUAGES');
        // вкладки с языками
        foreach ($list as $alias => $lang) {
            $languageId = $lang['id'];
            // если есть язык
            if (isset($ln[$languageId])) {
                $item = $ln[$languageId];
                $data = array('article_id' => $articleId, 'language_id' => $languageId);
                for ($j = 0; $j < sizeof($fields); $j++) {
                    if (isset($item[$fields[$j]]))
                        $data[$fields[$j]] = $item[$fields[$j]];
                }
                // если поля не заполнены
                if (empty($data['page_meta_robots']))
                    $data['page_meta_robots'] = 'index, nofollow';
                if (empty($data['page_meta_author']))
                    $data['page_meta_author'] = $_SESSION['profile/name'];
                // длина текста статьи
                if (empty($data['page_html']))
                    $data['page_html_length'] = 0;
                else {
                    $text = strip_tags($data['page_html']);
                    $text = preg_replace("/\{image[0-9]\}/", '', $text);
                    $data['page_html_plain'] = trim($text);
                    $data['page_html_length'] = mb_strlen($data['page_html'], 'UTF-8');
                    // поиск динамических компонентов в статье
                    if (mb_strpos($data['page_html'], '<component', 0, 'UTF-8') !== false) {
                        $data['page_parsing'] = 1;
                        // обновление атрибутов компонентов в статье
                        if (isset($componentAttrs[$languageId])) {
                            $cmps = &$componentAttrs[$languageId];
                            // проверка существования компонентов в статье
                            foreach ($cmps as $id => $attr) {
                                if (mb_strpos($data['page_html'], $id, 0, 'UTF-8') === false)
                                    unset($cmps[$id]);
                            }
                            if (empty($cmps))
                                $data['component_attributes'] = null;
                            else {
                                // сохраняем атрибуты компонента
                                GComponent::saveToConfig($cmps);
                                $data['component_attributes'] = json_encode($cmps);
                            }
                        }
                    } else
                        $data['page_parsing'] = 0;
                }
                // если есть теги
                if (isset($item['page_tags'])) {
                    $data['page_tags'] = trim($item['page_tags']);
                    if (empty($data['page_tags']))
                        $data['page_tags_en'] = '';
                    else
                        $data['page_tags_en'] = GString::convert($data['page_tags'], 'ru');
                }
                // добавить запись
                if ($table->insert($data) === false)
                    throw new GSqlException();
            }
        }
    }

    /**
     * Обновление текста статьи
     * 
     * @return void
     */
    protected function textUpdate()
    {
        $ln = $this->input->get('ln', false);
        if (empty($ln)) return;

        Gear::library('String');

        $table = new GDb_Table('site_pages', 'page_id');
        // допустимые поля
        $fields = array(
            'page_html', 'page_html_plain', 'page_html_length', 'page_html_short', 'page_html_widgets', 'page_breadcrumb',
            'page_title', 'page_header', 'page_menu', 'page_rss', 'page_image', 'page_meta_keywords', 'page_meta_description', 'page_meta_robots',
            'page_meta_author', 'page_meta_copyright', 'page_meta_revisit', 'page_meta_document', 'page_tags', 'page_tags_en'
        );
        $componentAttrs = $this->store->get('components');
        // список доступных языков
        $list = $this->config->getFromCms('Site', 'LANGUAGES');
        // вкладки с языками
        foreach ($list as $alias => $lang) {
        //for ($i = 0; $i < $count; $i++) {
            $languageId = $lang['id'];
            // если есть язык
            if (isset($ln[$languageId])) {
                $item = $ln[$languageId];
                // если есть только идент.
                if (sizeof($item) == 1) continue;
                $data = array();
                for ($j = 0; $j < sizeof($fields); $j++) {
                    if (isset($item[$fields[$j]]))
                        $data[$fields[$j]] = $item[$fields[$j]];
                }
                // длина текста статьи
                if (isset($data['page_html'])) {
                    if (empty($data['page_html'])) {
                        $data['page_html_length'] = 0;
                        $data['page_html_plain'] = '';
                        $data['page_parsing'] = 0;
                    } else {
                        $text = strip_tags($data['page_html']);
                        $text = preg_replace("/\{image[0-9]\}/", '', $text);
                        $data['page_html_plain'] = trim($text);
                        $data['page_html_length'] = mb_strlen($data['page_html'], 'UTF-8');
                        // поиск динамических компонентов в статье
                        if (mb_strpos($data['page_html'], '<component', 0, 'UTF-8') !== false) {
                            $data['page_parsing'] = 1;
                            // обновление атрибутов компонентов в статье
                            if (isset($componentAttrs[$languageId])) {
                                $cmps = &$componentAttrs[$languageId];
                                // сохраняем атрибуты компонента
                                GComponent::saveToConfig($cmps);
                                // проверка существования компонентов в статье
                                foreach ($cmps as $id => $attr) {
                                    if (mb_strpos($data['page_html'], $id, 0, 'UTF-8') === false)
                                        unset($cmps[$id]);
                                }
                                if (empty($cmps))
                                    $data['component_attributes'] = null;
                                else
                                    $data['component_attributes'] = json_encode($cmps);
                                // чтобы не засорять сессию если компонент был удален
                                $this->store->set('components', $componentAttrs);
                            }
                        } else
                            $data['page_parsing'] = 0;
                    }
                }
                // если есть теги
                if (isset($item['page_tags'])) {
                    $data['page_tags'] = trim($item['page_tags']);
                    if (empty($data['page_tags']))
                        $data['page_tags_en'] = '';
                    else
                        $data['page_tags_en'] = GString::convert($data['page_tags'], 'ru');
                }
                // обновить запись
                if ($table->update($data, $item['page_id']) === false)
                    throw new GSqlException();
            }
        }
    }

    /**
     * Событие наступает после успешного обновления данных
     * 
     * @param array $data сформированные данные полученные после запроса к таблице
     * @return void
     */
    protected function dataUpdateComplete($data = array())
    {
        // соединение с базой данных (т.к. для лога ранее возможно было другое соединение)
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();

        // обновление текста статьи
        $this->textUpdate($this->_recordId);
        // удалить кэш
        GArticle::deleteCache($this->uri->id);
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Событие наступает после успешного добавления данных
     * 
     * @param array $data сформированные данные полученные после запроса к таблице
     * @return void
     */
    protected function dataInsertComplete($data = array())
    {
        // соединение с базой данных (т.к. для лога ранее возможно было другое соединение)
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();

        // вставка текста статьи
        $this->textInsert($this->_recordId);
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Событие наступает после успешного удаления данных
     * 
     * @return void
     */
    protected function dataDeleteComplete()
    {
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        if ($record['sitemap_changefreq'])
            if (isset($this->_['data_changefreq_a'][$record['sitemap_changefreq']]))
                $record['sitemap_changefreq'] = $this->_['data_changefreq_a'][$record['sitemap_changefreq']];

        $query = new GDb_Query();
        // категории статьи
        if (!empty($record['category_id'])) {
            $sql = 'SELECT * FROM `site_categories` WHERE `category_id`=' . $record['category_id'];
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            if (!empty($rec))
                $setTo[] = array('xtype' => 'mn-field-combo', 'id' => 'fldCategory', 'text' => $rec['category_name'], 'value' => $record['category_id']);
            unset($record['category_id']);
        }

        // тип статьи
        if (!empty($record['type_id'])) {
            $sql = 'SELECT * FROM `site_article_types` WHERE `type_id`=' . $record['type_id'];
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            if (!empty($rec))
                $setTo[] = array('xtype' => 'mn-field-combo', 'id' => 'fldType', 'text' => $rec['type_name'], 'value' => $record['type_id']);
        }

        // доступ к статье
        if (!empty($record['access_id'])) {
            $sql = 'SELECT * FROM `site_article_access` WHERE `access_id`=' . $record['access_id'];
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            if (!empty($rec))
                $setTo[] = array('xtype' => 'mn-field-combo', 'id' => 'fldAccess', 'text' => $rec['access_name'], 'value' => $record['access_id']);
        }

        // шаблон статьи
        if (!empty($record['template_id'])) {
            $sql = 'SELECT * FROM `site_templates` WHERE `template_id`=' . $record['template_id'];
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            if (!empty($rec))
                $setTo[] = array('xtype' => 'mn-field-combo', 'id' => 'fldTemplate', 'text' => $rec['template_name'], 'value' => $record['template_id']);
        }

        // установка полей
        $this->response->add('setTo', $setTo);

        return $record;
    }
}
?>