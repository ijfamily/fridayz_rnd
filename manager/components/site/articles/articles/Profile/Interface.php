<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля статьи"
 * Пакет контроллеров "Профиль статьи"
 * Группа пакетов     "Статьи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Distributed under the Lesser General Public License (LGPL)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    GController_SArticles_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');
Gear::library('Site');

/**
 * Интерфейс профиля статьи
 * 
 * @category   Gear
 * @package    GController_SArticles_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SArticles_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sarticles_grid';

    /**
     * Возращает список компонентов для редактора
     * 
     * @return array
     */
    protected function getEditorComponents()
    {
        if ($items = $this->store->get('editorComponents', false))
            return $items;

        // соединение с базой данных
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();

        $items = GSite::getEditorComponents1();
        $this->store->set('editorComponents', $items);

        return $items;
    }

    /**
     * Возращает интерфейс кэша
     * 
     * @param object $query указатель на запрос
     * @return mixed
     */
    protected function getCacheInterface($query = null)
    {
        if (is_null($query))
            $query = new GDb_Query();
        // данные кэша страницы
        $sql = 'SELECT * FROM `site_cache` WHERE `cache_generator`=\'article\' AND `cache_generator_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $items = array();
        while (!$query->eof()) {
            $cache = $query->next();
            $items[] = array(
                'xtype'      => 'displayfield',
                'fieldLabel' => $this->_['label_fld_date'],
                'anchor'     => '100%',
                'fieldClass' => 'mn-field-value-info',
                'value'      => date('d-m-Y H:i:s', strtotime($cache['cache_date']))
            );
            $items[] = array(
                'xtype'      => 'displayfield',
                'fieldLabel' => $this->_['label_fld_file'],
                'anchor'     => '100%',
                'value'      => '<a target="blank" href="/cache/' . $cache['cache_filename'] . '">' . $cache['cache_filename'] . '</a>'
            );
            $items[] = array(
                'xtype'      => 'displayfield',
                'fieldLabel' => $this->_['label_fld_address'],
                'anchor'     => '100%',
                'value'      => '<a target="blank" href="' . $cache['cache_uri'] . '">' . $cache['cache_uri'] . '</a>'
            );
            $items[] = array('xtype' => 'mn-field-separator');
        }
        $fs = array();
        if ($items) {
            $items[] = array(
                'xtype'       => 'mn-btn-resetcache',
                'msgConfirm'  => $this->_['msg_cache_delete'],
                'text'        => $this->_['text_cache_delete'],
                'tooltip'     => $this->_['tip_cache_delete'],
                'icon'        => $this->resourcePath . 'icon-cache-delete.png',
                'ctId'        => 'fs-cache',
                'width'       => 107,
                'url'         => $this->componentUrl . 'profile/cache/' . $this->uri->id
            );
            $fs = array(
                'xtype'       => 'fieldset',
                'id'          => 'fs-cache',
                'width'       => 760,
                'title'       => sprintf($this->_['title_fieldset_cache'], date('d-m-Y H:i:s', strtotime($cache['cache_date']))),
                'collapsible' => true,
                'collapsed'   => true,
                'labelWidth'  => 125,
                'items'       => $items
            );
        }

        return $fs;
    }

    /**
     * Возращает интерфейса полей с текстом
     * 
     * @return array
     */
    protected function getTextInterface($article)
    {
        $values = array();

        // если состоянеи формы "правка"
        if ($this->isUpdate) {
            $query = new GDb_Query();
            // обновление текста в статье
            $sql = 'SELECT * FROM `site_pages` WHERE `article_id`=' . $this->uri->id;
            if ($query->execute($sql) === false)
                throw new GSqlException();
            $components = array();
            while (!$query->eof()) {
                $item = $query->next();
                if (!isset($values[$item['language_id']]))
                    $values[$item['language_id']] = array();
                $values[$item['language_id']]['page_id'] = $item['page_id'];
                $values[$item['language_id']]['page_html'] = $item['page_html'];
                $values[$item['language_id']]['page_html_short'] = $item['page_html_short'];
                $values[$item['language_id']]['page_html_widgets'] = $item['page_html_widgets'];
                $values[$item['language_id']]['page_breadcrumb'] = $item['page_breadcrumb'];
                $values[$item['language_id']]['page_tags'] = $item['page_tags'];
                $values[$item['language_id']]['page_title'] = $item['page_title'];
                $values[$item['language_id']]['page_header'] = $item['page_header'];
                $values[$item['language_id']]['page_menu'] = $item['page_menu'];
                $values[$item['language_id']]['page_image'] = $item['page_image'];
                $values[$item['language_id']]['page_meta_keywords'] = $item['page_meta_keywords'];
                $values[$item['language_id']]['page_meta_description'] = $item['page_meta_description'];
                $values[$item['language_id']]['page_meta_robots'] = $item['page_meta_robots'];
                $values[$item['language_id']]['page_meta_author'] = $item['page_meta_author'];
                $values[$item['language_id']]['page_meta_copyright'] = $item['page_meta_copyright'];
                $values[$item['language_id']]['page_meta_revisit'] = $item['page_meta_revisit'];
                $values[$item['language_id']]['page_meta_document'] = $item['page_meta_document'];
                // если нет компонентов в статьи
                if (empty($item['component_attributes']))
                    $components[$item['language_id']] = array();
                else {
                    $components[$item['language_id']] = json_decode($item['component_attributes'], true);
                    switch (json_last_error()) {
                        case JSON_ERROR_DEPTH:
                            throw new GException('Data processing error', 'Maximum stack depth exceeded');
                            break;

                        case JSON_ERROR_CTRL_CHAR:
                            throw new GException('Data processing error', 'Unexpected control character found');
                            break;

                        case JSON_ERROR_SYNTAX:
                        case 2:
                            throw new GException('Data processing error', 'Syntax error, malformed JSON');
                            break;

                        case JSON_ERROR_NONE:
                            break;
                    }
                }
            }
            $this->store->set('components', $components);
        // если состоянеи формы "вставка"
        } else {
            $components = array();
            // список доступных языков
            $list = $this->config->getFromCms('Site', 'LANGUAGES');
            foreach ($list as $key => $item) {
                $components[$item['id']] = array();
            }
            $this->store->set('components', $components);
        }

        $did = $this->config->getFromCms('Site', 'LANGUAGE/ID');
        // список доступных языков
        $list = $this->config->getFromCms('Site', 'LANGUAGES');
        $editorComponents = $this->getEditorComponents();
        // вкладки с языками
        $tabs[] = array();
        foreach ($list as $key => $item) {
            //$lid = $lid;
            $lid = $item['id'];
            $metaRobots = isset($values[$lid]['page_meta_robots']) ? $values[$lid]['page_meta_robots'] : 'index, nofollow';
            $metaAuthor = isset($values[$lid]['page_meta_author']) ? $values[$lid]['page_meta_author'] :  $_SESSION['profile/name'];
            // владка "SEO статьи"
            $tabSeo = array(
                'title'       => $this->_['title_tab_seo'],
                'iconSrc'     => $this->resourcePath . 'icon-tab-seo.png',
                'layout'      => 'form',
                'labelWidth'  => 85,
                'autoScroll'  => true,
                'baseCls'     => 'mn-form-tab-body',
                'defaultType' => 'textfield',
                'items'       => array(
                    array('xtype'      => 'fieldset',
                          'width'      => 760,
                          'labelWidth' => 120,
                          'collapsible' => true,
                          'collapsed'   => true,
                          'title'      => $this->_['title_fieldset_r'] . ' (' . $metaRobots . ')',
                          'autoHeight' => true,
                          'items'      => array(
                              array('xtype'         => 'combo',
                                    'fieldLabel'    => $this->_['label_page_meta_r'],
                                    'labelTip'      => $this->_['tip_page_meta_r'],
                                    'id'            => 'ln[' . $lid . '][page_meta_robots]',
                                    'name'          => 'ln[' . $lid . '][page_meta_robots]',
                                    'value'         => $metaRobots,
                                    'editable'      => true,
                                    'maxLength'     => 100,
                                    'width'         => 180,
                                    'typeAhead'     => false,
                                    'triggerAction' => 'all',
                                    'mode'          => 'local',
                                    'store'         => array(
                                        'xtype'  => 'arraystore',
                                        'fields' => array('list'),
                                        'data'   => $this->_['data_robots']
                                    ),
                                    'hiddenName'    => 'ln[' . $lid . '][page_meta_robots]',
                                    'valueField'    => 'list',
                                    'displayField'  => 'list',
                                    'allowBlank'    => true),
                              array('xtype'         => 'combo',
                                    'itemCls'       => 'mn-form-item-quiet',
                                    'fieldLabel'    => $this->_['label_page_meta_v'],
                                    'labelTip'      => $this->_['tip_page_meta_v'],
                                    'id'            => 'ln[' . $lid . '][page_meta_revisit]',
                                    'name'          => 'ln[' . $lid . '][page_meta_revisit]',
                                    'value'         => isset($values[$lid]['page_meta_revisit']) ? $values[$lid]['page_meta_revisit'] : '',
                                    'editable'      => true,
                                    'maxLength'     => 10,
                                    'width'         => 180,
                                    'typeAhead'     => false,
                                    'triggerAction' => 'all',
                                    'mode'          => 'local',
                                    'store'         => array(
                                        'xtype'  => 'arraystore',
                                        'fields' => array('list', 'value'),
                                        'data'   => $this->_['data_revisit']
                                    ),
                                    'hiddenName'    => 'ln[' . $lid . '][page_meta_revisit]',
                                    'valueField'    => 'value',
                                    'displayField'  => 'list',
                                    'allowBlank'    => true),
                              array('xtype'         => 'combo',
                                    'itemCls'       => 'mn-form-item-quiet',
                                    'fieldLabel'    => $this->_['label_page_meta_m'],
                                    'labelTip'      => $this->_['tip_page_meta_m'],
                                    'id'            => 'ln[' . $lid . '][page_meta_document]',
                                    'name'          => 'ln[' . $lid . '][page_meta_document]',
                                    'value'         => isset($values[$lid]['page_meta_document']) ? $values[$lid]['page_meta_document'] : '',
                                    'editable'      => true,
                                    'maxLength'     => 100,
                                    'width'         => 180,
                                    'typeAhead'     => false,
                                    'triggerAction' => 'all',
                                    'mode'          => 'local',
                                    'store'         => array(
                                        'xtype'  => 'arraystore',
                                        'fields' => array('list'),
                                        'data'   => $this->_['data_doc']
                                    ),
                                    'hiddenName'    => 'ln[' . $lid . '][page_meta_document]',
                                    'valueField'    => 'list',
                                    'displayField'  => 'list',
                                    'allowBlank'    => true),
                          )
                    ),
                    array('xtype'      => 'fieldset',
                          'width'      => 760,
                          'labelWidth' => 120,
                          'title'      => $this->_['title_fieldset_a'] . ' (' . $metaAuthor . ')',
                          'collapsible' => true,
                          'collapsed'   => true,
                          'autoHeight' => true,
                          'items'      => array(
                              array('xtype'      => 'textfield',
                                    'itemCls'       => 'mn-form-item-quiet',
                                    'fieldLabel' => $this->_['label_page_meta_a'],
                                    'labelTip'   => $this->_['tip_page_meta_a'],
                                    'id'         => 'ln[' . $lid . '][page_meta_author]',
                                    'name'       => 'ln[' . $lid . '][page_meta_author]',
                                    'value'      => $metaAuthor,
                                    'emptyText'  => $_SESSION['profile/name'],
                                    'maxLength'  => 100,
                                    'width'      => 252,
                                    'allowBlank' => true),
                              array('xtype'      => 'textfield',
                                    'itemCls'       => 'mn-form-item-quiet',
                                    'fieldLabel' => $this->_['label_page_meta_c'],
                                    'labelTip'   => $this->_['tip_page_meta_c'],
                                    'id'         => 'ln[' . $lid . '][page_meta_copyright]',
                                    'name'       => 'ln[' . $lid . '][page_meta_copyright]',
                                    'value'      => isset($values[$lid]['page_meta_copyright']) ? $values[$lid]['page_meta_copyright'] : '',
                                    'maxLength'  => 255,
                                    'width'      => 252,
                                    'allowBlank' => true),
                          )
                    ),
                    array('xtype'      => 'fieldset',
                          'width'      => 760,
                          'labelWidth' => 120,
                          'title'      => $this->_['title_fieldset_meta'],
                          'autoHeight' => true,
                          'items'      => array(
                                array('xtype'      => 'textfield',
                                      'fieldLabel' => $this->_['label_page_title'],
                                      'labelTip'   => $this->_['tip_page_title'],
                                      'itemCls'    => 'mn-form-item-quiet',
                                      'id'         => 'ln[' . $lid . '][page_title]',
                                      'name'       => 'ln[' . $lid . '][page_title]',
                                      'value'      => isset($values[$lid]['page_title']) ? $values[$lid]['page_title'] : '',
                                      'maxLength'  => 255,
                                      'anchor'     => '100%',
                                      'allowBlank' => true),
                                array('xtype'      => 'textarea',
                                      'itemCls'    => 'mn-form-item-quiet',
                                      'fieldLabel' => $this->_['label_page_meta_k'],
                                      'labelTip'   => $this->_['tip_page_meta_k'],
                                      'id'         => 'ln[' . $lid . '][page_meta_keywords]',
                                      'name'       => 'ln[' . $lid . '][page_meta_keywords]',
                                      'value'      => isset($values[$lid]['page_meta_keywords']) ? $values[$lid]['page_meta_keywords'] : '',
                                      'anchor'     => '100%',
                                      'height'     => 150,
                                      'allowBlank' => true),
                                array('xtype'      => 'textarea',
                                      'itemCls'    => 'mn-form-item-quiet',
                                      'fieldLabel' => $this->_['label_page_meta_d'],
                                      'labelTip'   => $this->_['tip_page_meta_d'],
                                      'id'         => 'ln[' . $lid . '][page_meta_description]',
                                      'name'       => 'ln[' . $lid . '][page_meta_description]',
                                      'value'      => isset($values[$lid]['page_meta_description']) ? $values[$lid]['page_meta_description'] : '',
                                      'anchor'     => '100%',
                                      'height'     => 150,
                                      'allowBlank' => true)
                          )
                    )
                )
            );
            // вкладка "страница"
            $tabPage = array(
                'title'       => $this->_['title_tab_page'],
                'iconSrc'     => $this->resourcePath . 'icon-tab-page.png',
                'layout'      => 'form',
                'labelWidth'  => 100,
                'autoScroll'  => true,
                'baseCls'     => 'mn-form-tab-body',
                'items'       => array(
                    array('xtype' => 'hidden',
                          'id'    => 'ln[' . $lid . '][page_id]',
                          'name'  => 'ln[' . $lid . '][page_id]',
                          'value' => isset($values[$lid]['page_id']) ? $values[$lid]['page_id'] : ''),
                    array('xtype'      => 'textfield',
                          'id'         => $did == $lid ? 'fldPageHeader' : '',
                          'fieldLabel' => $this->_['label_page_header'],
                          'labelTip'   => $this->_['tip_page_header'],
                          'name'       => 'ln[' . $lid . '][page_header]',
                          'value'      => isset($values[$lid]['page_header']) ? $values[$lid]['page_header'] : '',
                          'maxLength'  => 255,
                          'width'      => 645,
                          'allowBlank' => !($did == $lid)),
                    array('xtype'      => 'textfield',
                          'fieldLabel' => $this->_['label_page_menu'],
                          'labelTip'   => $this->_['tip_page_menu'],
                          'itemCls'    => 'mn-form-item-quiet',
                          'id'         => 'ln[' . $lid . '][page_menu]',
                          'name'       => 'ln[' . $lid . '][page_menu]',
                          'value'      => isset($values[$lid]['page_menu']) ? $values[$lid]['page_menu'] : '',
                          'maxLength'  => 255,
                          'width'      => 645,
                          'allowBlank' => true),
                    array('xtype'      => 'textfield',
                          'fieldLabel' => $this->_['label_page_breadcrumb'],
                          'labelTip'   => $this->_['tip_page_breadcrumb'],
                          'itemCls'    => 'mn-form-item-quiet',
                          'id'         => 'ln[' . $lid . '][page_breadcrumb]',
                          'name'       => 'ln[' . $lid . '][page_breadcrumb]',
                          'value'      => isset($values[$lid]['page_breadcrumb']) ? $values[$lid]['page_breadcrumb'] : '',
                          'maxLength'  => 255,
                          'width'      => 645,
                          'allowBlank' => true),
                    array('xtype'      => 'textfield',
                          'fieldLabel' => $this->_['label_page_tags'],
                          'labelTip'   => $this->_['tip_page_tags'],
                          'itemCls'    => 'mn-form-item-quiet',
                          'id'         => 'ln[' . $lid . '][page_tags]',
                          'name'       => 'ln[' . $lid . '][page_tags]',
                          'value'      => isset($values[$lid]['page_tags']) ? $values[$lid]['page_tags'] : '',
                          'maxLength'  => 255,
                          'width'      => 645,
                          'allowBlank' => true),
                    array('xtype'      => 'fieldset',
                          'width'      => 760,
                          'title'      => $this->_['title_fieldset_shorttext'],
                          'labelWidth' => 90,
                          'autoHeight' => true,
                          'items'      => array(
                              array('xtype'           => 'mn-field-browse',
                                    'params'          => 'view=images',
                                    'triggerWdgSetTo' => true,
                                    'triggerWdgUrl'   => 'site/media/' . ROUTER_DELIMITER .'dialog/interface/',
                                    'fieldLabel' => $this->_['label_page_image'],
                                    'itemCls'    => 'mn-form-item-quiet',
                                    'id'         => 'page_image' . $lid,
                                    'name'       => 'ln[' . $lid . '][page_image]',
                                    'value'      => isset($values[$lid]['page_image']) ? $values[$lid]['page_image'] : '',
                                    'anchor'     => '100%',
                                    'allowBlank' => true),
                              array('xtype'      => 'textarea',
                                    'fieldLabel' => $this->_['label_page_html_short'],
                                    'labelTip'   => $this->_['tip_page_html_short'],
                                    'itemCls'    => 'mn-form-item-quiet',
                                    'id'         => 'ln[' . $lid . '][page_html_short]',
                                    'name'       => 'ln[' . $lid . '][page_html_short]',
                                    'value'      => isset($values[$lid]['page_html_short']) ? $values[$lid]['page_html_short'] : '',
                                    'anchor'     => '100%',
                                    'height'     => 140,
                                    'allowBlank' => true)
                          )
                    ),
                    array('xtype'      => 'fieldset',
                          'width'      => 760,
                          'title'      => $this->_['title_fieldset_widgets'],
                          'labelWidth' => 90,
                          'autoHeight' => true,
                          'items'      => array(
                              array('xtype'      => 'textarea',
                                    'fieldLabel' => $this->_['label_page_html_widgets'],
                                    'labelTip'   => $this->_['tip_page_html_widgets'],
                                    'itemCls'    => 'mn-form-item-quiet',
                                    'id'         => 'ln[' . $lid . '][page_html_widgets]',
                                    'name'       => 'ln[' . $lid . '][page_html_widgets]',
                                    'value'      => isset($values[$lid]['page_html_widgets']) ? $values[$lid]['page_html_widgets'] : '',
                                    'anchor'     => '100%',
                                    'height'     => 120,
                                    'allowBlank' => true)
                          )
                    )
                )
            );
            // если страница лендинг
            if ($article['article_landing']) {
                // вкладка "текст"
                $tabArticle = array(
                    'title'   => $this->_['title_tab_article'],
                    'iconSrc' => $this->resourcePath . 'icon-tab-article.png',
                    'layout'  => 'anchor',
                    'style'   => 'padding:7px;font-size:13px',
                    'html'    => $this->_['msg_article_landing']
                );
            } else {
                // вкладка "текст"
                $tabArticle = array(
                    'title'        => $this->_['title_tab_article'],
                    'iconSrc'      => $this->resourcePath . 'icon-tab-article.png',
                    'layout'       => 'anchor',
                    'style'        => 'padding:1px',
                    'bodyCssClass' => 'mn-tinymce',
                    'items'        => array(
                        array('xtype'      => 'mn-tinymce',
                              'id'         => 'htmleditor-' . $lid,
                              'hideLabel'  => true,
                              'name'       => 'ln[' . $lid . '][page_html]',
                              'value'      => isset($values[$lid]['page_html']) ? $values[$lid]['page_html'] : '',
                              'anchor'     => '100% 100%',
                              'allowBlank' => /*!($did == $lid)*/true,
                              'tinyMCEConfig' => array(
                                  'languageId'     => $this->config->getFromCms('Site', 'LANGUAGE/ID'),
                                  'language'       => $this->config->getFromCms('Site', 'LANGUAGE/ALIAS'),
                                  'uploadUrl'      => $this->uri->scriptName . '/' . $this->componentUrl . 'editor/data/',
                                  'componentIcons' => PATH_COMPONENT . 'site/articles/articles/Component/resources/icons/',
                                  'components'     => $editorComponents,
                                  'keep_styles' => false,
                                  'allow_html_in_named_anchor' => true,
                                  'allow_conditional_comments' => true,
                                  'protected' => array(),
                                  'cleanup' => false,
                                  'verify_html' => false,
                                  'cleanup_on_startup' => false,
                                  'extended_valid_elements' => '*[*]',
                                  'schema' => 'html5',
                                  'preformatted' => true,
                                    'cleanup_on_startup' => false,
                                    'trim_span_elements' => false,
                                    'verify_html' => false,
                                    'cleanup' => false,
                                    'convert_urls' => false,
                                    'paste_auto_cleanup_on_paste' => false,
                                    'paste_block_drop' => false,
                                    'paste_remove_spans' => false,
                                    'paste_strip_class_attributes' => false,
                                    'paste_retain_style_properties' => "",
                                    'inline_styles' => false
                              )
                        )
                    )
                );
            }

            $tabs[] = array(
                'xtype'             => 'tabpanel',
                'title'             => sprintf($this->_['title_tab_articles'], $item['title']),
                'iconSrc'           => $this->resourcePath . ($did == $lid ? 'icon-tab-text-def.png' : 'icon-tab-text.png'),
                'tabPosition'       => 'bottom',
                'layoutOnTabChange' => true,
                'activeTab'         => 0,
                'anchor'            => '100%',
                'autoScroll'        => false,
                'items'             => array($tabPage, $tabSeo, $tabArticle)
            );
        }

        return $tabs;
    }

    /**
     * Возращает список доменов
     * 
     * @return array
     */
    protected function getDomains()
    {
        $arr = $this->config->getFromCms('Domains');
        $list = array();
        if ($arr) {
            $list[] = array($this->config->getFromCms('Site', 'NAME', ''), 0);
            foreach ($arr['indexes'] as $key => $value) {
                $list[] = array($value[1], $key);
            } 
        }

        return $list;
    }

    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        $data = array('article_landing' => 0, 'cache' => array(), 'folder' => '');

        // если состояние формы "вставка"
        if ($this->isInsert) {
            // все загружаемые документы и изображения привязываются к номеру папки статьи
            $data['folder'] = time();

            return $data;
        }

        parent::getDataInterface();

        $query = new GDb_Query();
        // выбранная статья, категория статьи, доступ к статье
        $sql = 'SELECT * FROM `site_articles` WHERE `article_id`=' . (int)$this->uri->id;
        if (($data = $query->getRecord($sql)) === false)
            throw new GSqlException();

        $data['cache'] = $this->getCacheInterface($query);
        if (empty($data['article_folder'])) {
            $data['article_folder'] = time();
        }
        $data['folder'] = $data['article_folder'];

        return $data;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();
        $this->store->set('folder', $data['folder']);

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'id'            => $this->classId,
                  'titleEllipsis' => 120,
                  'gridId'        => 'gcontroller_sarticles_grid',
                  'width'         => 900,
                  'height'        => 620,
                  'resizable'     => false,
                  'stateful'      => false,
                  'maximizable'   => true,
                  'buttonsAdd'      => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-site-articles',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );

        // если состояние формы "update"
        if ($this->isUpdate) {
            $this->_cmp->buttonsAdd = array(
                array('xtype'   => 'mn-btn-widget-form',
                      'text'    => $this->_['text_btn_help'],
                      'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-site-articles',
                      'iconCls' => 'icon-item-info'),
                array('xtype'       => 'mn-btn-widget-form',
                      'style'       => array('margin-right' => 10),
                      'text'        => $this->_['text_wnd_copy'],
                      'tooltip'     => $this->_['tooltip_wnd_copy'],
                      'icon'        => $this->resourcePath . 'icon-wnd-copy.png',
                      'windowId'    => strtolower(__CLASS__),
                      'width'       => 97,
                      'closeWindow' => true,
                      'url'         => ROUTER_SCRIPT . $this->componentUrl . '../' . ROUTER_DELIMITER . 'copy/interface/' . $this->uri->id)
            );
        }

        // поля формы (ExtJS class "Ext.Panel")
        $langs = $this->config->getFromCms('Site', 'LANGUAGES');
        $lang = $langs[$this->config->getFromCms('Site', 'LANGUAGE')];
        // поля вкладки "атрибуты статьи"
        $itemUri = array(
            'xtype'      => 'fieldset',
            'width'      => 760,
            'labelWidth' => 62,
            'title'      => $this->_['title_fieldset_address'],
            'autoHeight' => true,
            'cls'        => 'mn-container-clean',
            'layout'     => 'column',
            'items'      => array(
                  array('layout' => 'form',
                        'width'  => 685,
                        'items'  => array(
                              array('xtype'      => 'textfield',
                                    'id'         => 'fldArticleUri',
                                    'fieldLabel' => $this->_['label_article_uri'],
                                    'labelTip'   => $this->_['tip_article_uri'],
                                    'name'       => 'article_uri',
                                    'blankText'  => $this->_['blank_article_uri'],
                                    'maxLength'  => 255,
                                    'anchor'     => '100%',
                                    'allowBlank' => true,
                                    'value'      => 'index.html')
                          )
                    ),
                    array('layout'    => 'form',
                          'width'     => 33,
                          'bodyStyle' => 'margin-left:3px;',
                          'items'     => array(
                              array('xtype'   => 'mn-btn-genurl',
                                    'type'    => 1,
                                    'tooltip' => $this->_['tip_button_genurl'],
                                    'icon'    => $this->resourcePath . 'icon-btn-gear.png',
                                    'url'     => $this->componentUrl . '../' . ROUTER_DELIMITER . 'generator/data/',
                                    'msgSelectHeader' => sprintf($this->_['msg_select_header'], $lang['title']),
                                    'msgSelectNode'   => $this->_['msg_select_node'])
                    )
              )
            )
        );

        $tabAttrItems = array(
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_article_note'],
                  'labelTip'   => $this->_['tip_article_note'],
                  'name'       => 'article_note',
                  'maxLength'  => 255,
                  'width'      => 657,
                  'itemCls'    => 'mn-form-item-quiet',
                  'allowBlank' => true),
            array('xtype'         => 'combo',
                  'fieldLabel'    => $this->_['label_domain'],
                  'itemCls'       => 'mn-form-item-quiet',
                  'name'          => 'domain_id',
                  'hidden'        => !$this->config->getFromCms('Site', 'OUTCONTROL'),
                  'value'         => 0,
                  'editable'      => false,
                  'maxLength'     => 100,
                  'width'         => 200,
                  'typeAhead'     => false,
                  'triggerAction' => 'all',
                  'mode'          => 'local',
                  'store'         => array(
                      'xtype'  => 'arraystore',
                      'fields' => array('display', 'value'),
                      'data'   => $this->getDomains()
                  ),
                  'hiddenName'    => 'domain_id',
                  'valueField'    => 'value',
                  'displayField'  => 'display',
                  'allowBlank'    => true),
            array('xtype'      => 'mn-field-combo',
                  'id'         => 'fldTemplate',
                  'tpl'        => 
                      '<tpl for="."><div ext:qtip="{name}" class="x-combo-list-item">'
                    . '<img width="16px" height="16px" align="absmiddle" src="' 
                    . $this->path . '/../../../templates/Combo/resources/icons/{data}.png" style="margin-right:5px">{name}</div></tpl>',
                  'fieldLabel' => $this->_['label_template_name'],
                  'labelTip'   => $this->_['tip_template_name'],
                  'value'      => $this->isInsert ? $this->_['data_template_default'] : '',
                  'hiddenValue' => $this->isInsert ? 6 : '',
                  'name'       => 'template_id',
                  'editable'   => false,
                  'width'      => 200,
                  'hiddenName' => 'template_id',
                  'allowBlank' => false,
                  'store'      => array(
                      'xtype' => 'jsonstore',
                      'url'   => $this->componentUrl . 'combo/trigger/?name=templates'
                  )
            ),
              array('xtype'      => 'mn-field-combo',
                    'id'         => 'fldType',
                    'itemCls'    => 'mn-form-item-quiet',
                    'fieldLabel' => $this->_['label_type_name'],
                    'value'      => $this->isInsert ? $this->_['data_type_default'] : '',
                    'hiddenValue' => $this->isInsert ? 1 : '',
                    'name'       => 'type_id',
                    'resetable'  => false,
                    'editable'   => false,
                    'width'      => 200,
                    'hiddenName' => 'type_id',
                    'allowBlank' => true,
                    'store'      => array(
                        'xtype' => 'jsonstore',
                        'url'   => $this->componentUrl . 'combo/trigger/?name=types'
                    )
              ),
            array('xtype'      => 'mn-field-combo-tree',
                  'id'         => 'fldCategory',
                  'itemCls'    => 'mn-form-item-quiet',
                  'fieldLabel' => $this->_['label_category_name'],
                  'labelTip'   => $this->_['tip_category_name'],
                  'width'      => 400,
                  'name'       => 'category_id',
                  'hiddenName' => 'category_id',
                  'resetable'  => false,
                  'treeWidth'  => 400,
                  'treeRoot'   => array('id' => 1, 'expanded' => true, 'expandable' => true),
                  'allowBlank' => true,
                  'store'      => array(
                      'xtype' => 'jsonstore',
                      'url'   => $this->componentUrl . 'combo/nodes/'
                   )
            ),
            $itemUri,
            array('xtype'      => 'fieldset',
                  'width'      => 760,
                  'title'      => $this->_['title_fieldset_public'],
                  'autoHeight' => true,
                  'layout'     => 'column',
                  'cls'        => 'mn-container-clean',
                  'items'      => array(
                      array('layout'     => 'form',
                            'width'      => 185,
                            'labelWidth' => 62,
                            'items'      => array(
                                array('xtype'      => 'datefield',
                                      'itemCls'    => 'mn-form-item-quiet',
                                      'fieldLabel' => $this->_['label_published_date'],
                                      'format'     => 'd-m-Y',
                                      'name'       => 'published_date',
                                      'checkDirty' => false,
                                      'width'      => 95,
                                      'allowBlank' => true)
                            )
                      ),
                      array('layout'     => 'form',
                            'labelWidth' => 55,
                            'items'      => array(
                                array('xtype'      => 'textfield',
                                      'itemCls'    => 'mn-form-item-quiet',
                                      'fieldLabel' => $this->_['label_published_time'],
                                      'name'       => 'published_time',
                                      'checkDirty' => false,
                                      'maxLength'  => 8,
                                      'width'      => 70,
                                      'allowBlank' => true)
                            )
                      )
                  )
            ),
            array('xtype'      => 'fieldset',
                  'width'      => 760,
                  'title'      => $this->_['title_fieldset_announce'],
                  'autoHeight' => true,
                  'layout'     => 'column',
                  'cls'        => 'mn-container-clean',
                  'items'      => array(
                      array('layout'     => 'form',
                            'width'      => 185,
                            'labelWidth' => 62,
                            'items'      => array(
                                array('xtype'      => 'datefield',
                                      'itemCls'    => 'mn-form-item-quiet',
                                      'fieldLabel' => $this->_['label_published_date'],
                                      'format'     => 'd-m-Y',
                                      'name'       => 'announce_date',
                                      'checkDirty' => false,
                                      'width'      => 95,
                                      'allowBlank' => true)
                            )
                      ),
                      array('layout'     => 'form',
                            'labelWidth' => 55,
                            'items'      => array(
                                array('xtype'      => 'textfield',
                                      'itemCls'    => 'mn-form-item-quiet',
                                      'fieldLabel' => $this->_['label_published_time'],
                                      'name'       => 'announce_time',
                                      'checkDirty' => false,
                                      'maxLength'  => 8,
                                      'width'      => 70,
                                      'allowBlank' => true)
                            )
                      )
                  )
            ),
            array('xtype'      => 'fieldset',
                  'width'      => 760,
                  'labelWidth' => 100,
                  'title'      => $this->_['title_fieldset_params'],
                  'autoHeight' => true,
                  'collapsible' => true,
                  'collapsed'  => true,
                  'items'      => array(
                      array('xtype'      => 'numberfield',
                            'itemCls'    => 'mn-form-item-quiet',
                            'fieldLabel' => $this->_['label_article_index'],
                            'labelTip'   => $this->_['tip_article_index'],
                            'name'       => 'article_index',
                            'width'      => 70,
                            'allowBlank' => true,
                            'emptyText'  => 1),
                      array('xtype'      => 'numberfield',
                            'itemCls'    => 'mn-form-item-quiet',
                            'fieldLabel' => $this->_['label_article_visits'],
                            'labelTip'   => $this->_['tip_article_visits'],
                            'name'       => 'article_visits',
                            'width'      => 70,
                            'allowBlank' => true,
                            'emptyText'  => 0),
                      array('xtype'      => 'mn-field-combo',
                            'id'         => 'fldAccess',
                            'itemCls'    => 'mn-form-item-quiet',
                            'tpl'        => 
                                  '<tpl for="."><div ext:qtip="{name}" class="x-combo-list-item">'
                                . '<img width="16px" height="16px" align="absmiddle" src="' 
                                . $this->path . '/../../articles/Combo/resources/icons/icon-access-{id}.png" style="margin-right:5px">{name}</div></tpl>',
                            'fieldLabel' => $this->_['label_access_name'],
                            'labelTip'   => $this->_['tip_access_name'],
                            'value'      => $this->isInsert ? $this->_['data_access_default'] : '',
                            'hiddenValue' => $this->isInsert ? 1 : '',
                            'name'       => 'access_id',
                            'resetable'  => false,
                            'editable'   => false,
                            'width'      => 200,
                            'hiddenName' => 'access_id',
                            'allowBlank' => false,
                            'store'      => array(
                                'xtype' => 'jsonstore',
                                'url'   => $this->componentUrl . 'combo/trigger/?name=access'
                            )
                      ),
                      array('xtype'  => 'container',
                            'layout' => 'column',
                            'cls'    => 'mn-container-clean',
                            'items'  => array(
                                array('layout' => 'form',
                                      'width'  => 150,
                                      'items'  => array(
                                          array('xtype'      => 'mn-field-chbox',
                                                'itemCls'    => 'mn-form-item-quiet',
                                                'fieldLabel' => $this->_['label_article_rss'],
                                                'labelTip'   => $this->_['tip_article_rss'],
                                                'default'    => $this->isUpdate ? null : 1,
                                                'name'       => 'article_rss'),
                                          array('xtype'      => 'mn-field-chbox',
                                                'itemCls'    => 'mn-form-item-quiet',
                                                'fieldLabel' => $this->_['label_article_archive'],
                                                'labelTip'   => $this->_['tip_article_archive'],
                                                'name'       => 'article_archive')
                                      )
                                ),
                                array('layout'      => 'form',
                                      'width'       => 120,
                                      'labelWidth'  => 55,
                                      'items'       => array(
                                          array('xtype'      => 'mn-field-chbox',
                                                'itemCls'    => 'mn-form-item-quiet',
                                                'fieldLabel' => $this->_['label_article_search'],
                                                'labelTip'   => $this->_['tip_article_search'],
                                                'default'    => $this->isUpdate ? null : 1,
                                                'name'       => 'article_search'),
                                          array('xtype'      => 'mn-field-chbox',
                                                'itemCls'    => 'mn-form-item-quiet',
                                                'fieldLabel' => $this->_['label_article_print'],
                                                'labelTip'   => $this->_['tip_article_print'],
                                                'default'    => $this->isUpdate ? null : 0,
                                                'name'       => 'article_print')
                                      )
                                ),
                                array('layout'      => 'form',
                                      'width'       => 120,
                                      'labelWidth'  => 69,
                                      'items'       => array(
                                          array('xtype'      => 'mn-field-chbox',
                                                'itemCls'    => 'mn-form-item-quiet',
                                                'fieldLabel' => $this->_['label_article_map'],
                                                'labelTip'   => $this->_['tip_article_map'],
                                                'default'    => $this->isUpdate ? null : 0,
                                                'name'       => 'article_map'),
                                          array('xtype'      => 'mn-field-chbox',
                                                'itemCls'    => 'mn-form-item-quiet',
                                                'fieldLabel' => $this->_['label_article_header'],
                                                'labelTip'   => $this->_['tip_article_header'],
                                                'default'    => $this->isUpdate ? null : 1,
                                                'name'       => 'article_header')
                                      )
                                ),
                                array('layout'      => 'form',
                                      'width'       => 150,
                                      'labelWidth'  => 98,
                                          'items'       => array(
                                              array('xtype'      => 'mn-field-chbox',
                                                    'itemCls'    => 'mn-form-item-quiet',
                                                    'fieldLabel' => $this->_['label_article_caching'],
                                                    'labelTip'   => $this->_['tip_article_caching'],
                                                    'name'       => 'article_caching')
                                      )
                                )
                            )
                      )
                  )
            ),
            array('xtype'      => 'fieldset',
                  'labelWidth' => 108,
                  'width'      => 760,
                  'title'      => $this->_['title_fieldset_sm'],
                  'autoHeight' => true,
                  'collapsible' => true,
                  'collapsed'  => true,
                  'items'      => array(
                      array('xtype'      => 'mn-field-chbox',
                            'itemCls'    => 'mn-form-item-quiet',
                            'fieldLabel' => $this->_['label_article_sitemap'],
                            'labelTip'   => $this->_['tip_article_sitemap'],
                            'default'    => $this->isUpdate ? null : 1,
                            'name'       => 'article_sitemap'),
                      array('xtype'         => 'combo',
                            'itemCls'       => 'mn-form-item-quiet',
                            'fieldLabel'    => $this->_['label_sm_priority'],
                            'labelTip'      => $this->_['tip_sm_priority'],
                            'name'          => 'sitemap_priority',
                            'editable'      => true,
                            'maxLength'     => 100,
                            'width'         => 100,
                            'typeAhead'     => false,
                            'triggerAction' => 'all',
                            'mode'          => 'local',
                            'value'         => '0.0',
                            'store'         => array(
                                'xtype'  => 'arraystore',
                                'fields' => array('list'),
                                'data'   => $this->_['data_priorities']
                            ),
                            'hiddenName'    => 'sitemap_priority',
                            'valueField'    => 'list',
                            'displayField'  => 'list',
                            'allowBlank'    => true),
                      array('xtype'         => 'combo',
                            'itemCls'       => 'mn-form-item-quiet',
                            'fieldLabel'    => $this->_['label_sm_changefreq'],
                            'labelTip'      => $this->_['tip_sm_changefreq'],
                            'name'          => 'sitemap_changefreq',
                            'editable'      => false,
                            'maxLength'     => 100,
                            'width'         => 100,
                            'typeAhead'     => false,
                            'triggerAction' => 'all',
                            'mode'          => 'local',
                            'store'         => array(
                                'xtype'  => 'arraystore',
                                'fields' => array('list', 'value'),
                                'data'   => $this->_['data_changefreq']
                            ),
                            'hiddenName'    => 'sitemap_changefreq',
                            'valueField'    => 'value',
                            'displayField'  => 'list',
                            'allowBlank'    => true)
                  )
            )
        );
        // если есть кэш
        if ($data['cache']) {
            $tabAttrItems[] = $data['cache'];
        }
        $tabAttrItems[] =  array(
              'xtype'      => 'container',
              'width'      => 760,
              'layout'     => 'form',
              'labelWidth' => 203,
              'items'      => array(
                  array('xtype'      => 'mn-field-chbox',
                        'fieldLabel' => $this->_['label_published'],
                        'default'    => $this->isUpdate ? null : 1,
                        'name'       => 'published'),
                  array('xtype'      => 'mn-field-chbox',
                        'fieldLabel' => $this->_['label_published_feed'],
                        'labelTip'   => $this->_['tip_published_feed'],
                        'default'    => $this->isUpdate ? null : 1,
                        'name'       => 'published_feed'),
                  array('xtype'      => 'mn-field-chbox',
                        'fieldLabel' => $this->_['label_published_main'],
                        'labelTip'   => $this->_['tip_published_main'],
                        'default'    => $this->isUpdate ? null : 1,
                        'name'       => 'published_main')
              )
        );
        // вкладка "атрибуты"
        $tabAttr = array(
            'title'       => $this->_['title_tab_attributes'],
            'layout'      => 'form',
            'labelWidth'  => 90,
            'iconSrc'     => $this->resourcePath . 'icon-tab-attr.png',
            'baseCls'     => 'mn-form-tab-body',
            'defaultType' => 'textfield',
            'items'       => array($tabAttrItems)
        );

        // вкладки окна
        $tabs = array(
            array('xtype' => 'hidden',
                  'name'  => 'article_folder',
                  'value' => $data['folder']),
            array('xtype'             => 'tabpanel',
                  'layoutOnTabChange' => true,
                  'activeTab'         => 0,
                  'style'             => 'padding:3px',
                  'enableTabScroll'   => true,
                  'anchor'            => '100% 100%',
                  'defaults'          => array('autoScroll' => true),
                  'bodyStyle'         => 'background-color:#ECECEC',
                  'items'             => array($tabAttr, $this->getTextInterface($data)))
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($tabs);
        $form->url = $this->componentUrl . 'profile/';

        parent::getInterface();
    }
}
?>