<?php
/**
 * Gear Manager
 *
 * Контроллер         "Триггер дерева"
 * Пакет контроллеров "Триггер дерева"
 * Группа пакетов     "Список статей"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Distributed under the Lesser General Public License (LGPL)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    GController_SArticles_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Tree/Trigger');

/**
 * Триггер дерева
 * 
 * @category   Gear
 * @package    GController_SArticles_Tree
 * @subpackage Trigger
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SArticles_Tree_Trigger extends GController_Tree_Trigger
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sarticles_grid';

    /**
     * Возращает список категорий
     *
     * @return array
     */
    protected function getCategories()
    {
        $data = array();
        $query = new GDb_Query();
        $sql = 'SELECT `c`.*, COUNT(*) `count` FROM `site_articles` `a` JOIN `site_categories` `c` USING(`category_id`) GROUP BY `category_id`';
        if ($query->execute($sql) !== true)
            throw new GSqlException();
        while (!$query->eof()) {
            $rec = $query->next();
            $data[] = array(
                'text'    => $rec['category_name'] . ' <b>(' . $rec['count'] . ')</b>',
                'qtip'    => $rec['category_name'],
                'leaf'    => true,
                'iconCls' => 'icon-folder-find',
                'value'   => $rec['category_id']
            );
        }

        return $data;
    }

    /**
     * Возращает список прав доступа
     *
     * @return array
     */
    protected function getAccess()
    {
        $data = array();
        $query = new GDb_Query();
        $sql = 'SELECT `c`.*, COUNT(*) `count` FROM `site_articles` `a` JOIN `site_article_access` `c` USING(`access_id`) GROUP BY `access_id`';
        if ($query->execute($sql) !== true)
            throw new GSqlException();
        while (!$query->eof()) {
            $rec = $query->next();
            $data[] = array(
                'text'    => $rec['access_name'] . ' <b>(' . $rec['count'] . ')</b>',
                'qtip'    => $rec['access_name'],
                'leaf'    => true,
                'iconCls' => 'icon-folder-find',
                'value'   => $rec['access_id']
            );
        }

        return $data;
    }

    /**
     * Возращает список шаблонов
     *
     * @return array
     */
    protected function getTemplates()
    {
        $data = array();
        $query = new GDb_Query();
        $sql = 'SELECT `t`.*, COUNT(*) `count` FROM `site_articles` `a` JOIN `site_templates` `t` USING(`template_id`) GROUP BY `template_id`';
        if ($query->execute($sql) !== true)
            throw new GSqlException();
        while (!$query->eof()) {
            $rec = $query->next();
            $data[] = array(
                'text'    => $rec['template_name'] . ' <b>(' . $rec['count'] . ')</b>',
                'qtip'    => $rec['template_name'],
                'leaf'    => true,
                'iconCls' => 'icon-folder-find',
                'value'   => $rec['template_id']
            );
        }

        return $data;
    }

    /**
     * Возращает список авторов
     *
     * @return array
     */
    protected function getAuthors()
    {
        $data = array();
        $query = new GDb_Query();
        $sql = 'SELECT `u`.*, `p`.`profile_name`, COUNT(*) `count` FROM `site_articles` `a` '
             . 'JOIN `gear_users` `u` ON `a`.`sys_user_update`=`u`.`user_id` OR `a`.`sys_user_insert`=`u`.`user_id` '
             . 'JOIN `gear_user_profiles` `p` USING(`user_id`) GROUP BY `u`.`user_id`';
        if ($query->execute($sql) !== true)
            throw new GSqlException();
        while (!$query->eof()) {
            $rec = $query->next();
            $data[] = array(
                'text'    => $rec['profile_name'] . ' <b>(' . $rec['count'] . ')</b>',
                'qtip'    => $rec['profile_name'],
                'leaf'    => true,
                'iconCls' => 'icon-folder-find',
                'value'   => $rec['user_id']
            );
        }

        return $data;
    }

    /**
     * Возращает виды страниц
     *
     * @return array
     */
    protected function getTypeView()
    {
        $data = array();
        $query = new GDb_Query();
        $sql = 'SELECT COUNT(*) `count` FROM `site_articles` WHERE `article_landing`=1 GROUP BY `article_landing`';
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        if (!empty($rec))
            $data[] = array(
                'text'    => $this->_['landing page'] . ' <b>(' . $rec['count'] . ')</b>',
                'qtip'    => $this->_['landing page'],
                'leaf'    => true,
                'iconCls' => 'icon-folder-find',
                'value'   => 1
            );
        $sql = 'SELECT COUNT(*) `count` FROM `site_articles` WHERE `article_landing`=0 GROUP BY `article_landing`';
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        $data[] = array(
            'text'    => $this->_['page'] . ' <b>(' . $rec['count'] . ')</b>',
            'qtip'    => $this->_['page'],
            'leaf'    => true,
            'iconCls' => 'icon-folder-find',
            'value'   => 0
        );

        return $data;
    }

    /**
     * Возращает все теги статей
     *
     * @return array
     */
    protected function getTags()
    {
        $data = array();
        $query = new GDb_Query();
        $sql = 'SELECT * FROM `site_tags`';
        if ($query->execute($sql) !== true)
            throw new GSqlException();
        while (!$query->eof()) {
            $rec = $query->next();
            $data[] = array(
                'text'    => '#' . $rec['tag_name'],
                'qtip'    => $rec['tag_title'],
                'leaf'    => true,
                'iconCls' => 'icon-folder-find',
                'value'   => $rec['tag_name']
            );
        }

        return $data;
    }

    /**
     * Вывод данных в интерфейс компонента
     * 
     * @param  string $name название триггера
     * @param  string $node id выбранного узла
     * @return void
     */
    protected function dataView($name, $node)
    {
        parent::dataView($name, $node);

        // триггер
        switch ($name) {
            // быстрый фильтр списка
            case 'gridFilter':
                // id выбранного узла
                switch ($node) {
                    // по категории
                    case 'byCt': $this->response->data = $this->getCategories(); break;

                    // по правам доступа
                    case 'byAc': $this->response->data = $this->getAccess(); break;

                    // по шаблону
                    case 'byTm': $this->response->data = $this->getTemplates(); break;

                    // по автору статьи
                    case 'byAu': $this->response->data = $this->getAuthors(); break;

                    // по виду статьи
                    case 'byTv': $this->response->data = $this->getTypeView(); break;

                    // по тегу
                    case 'byTg': $this->response->data = $this->getTags(); break;
                }
        }
    }
}
?>