<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'        => 'Статьи',
    'tooltip_grid'      => 'список статей сайта',
    'rowmenu_edit'      => 'Редактировать',
    'rowmenu_view'      => 'Просмотр страницы',
    'rowmenu_pictures'  => 'Изображения',
    'rowmenu_documents' => 'Документы',
    'rowmenu_video'     => 'Видеозаписи',
    'rowmenu_audio'     => 'Аудиозаписи',
    'rowmenu_comments'  => 'Комментарии',
    'rowmenu_landing'   => 'Целевая страница (landing page)',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">("Статьи", "Изображения", '
                                . '"Документы", "Комментарии")</span> ?',
    'text_btn_copy'            => 'Копировать',
    'tooltip_btn_copy'         => 'Создание копии статьи',
    'warning_btn_copy'         => 'Для создания копии необходимо выделить запись!',
    'warningr_btn_copy'        => 'Необходимо выделить только одну запись!',
    'text_btn_log'             => 'История',
    'tooltip_btn_log'          => 'История запросов пользователей системы',
    'label_domain'             => 'Сайт',
    // столбцы
    'header_site_name'         => 'Сайт',
    'header_article_index'     => '№',
    'tooltip_article_index'    => 'Порядковый номер статьи в списке',
    'header_published_date'    => 'Дата публикации',
    'tooltip_published_date'   => 'Дата публикации (правка) статьи',
    'header_article_note'      => 'Заметка',
    'header_page_header'       => 'Заголовок',
    'tooltip_page_header'      => 'Заголовок статьи',
    'header_category_name'     => 'Категория',
    'tooltip_category_name'    => 'Категория статьи',
    'header_page_tags'         => 'Теги',
    'header_article_uri'       => 'ЧПУ URL статьи',
    'tooltip_article_uri'      => 'Человекопонятный URL (единообразный локатор ресурса) адрес статьи',
    'tooltip_published'        => 'Публикация статьи',
    'header_article_archive'   => 'Архив',
    'tooltip_article_archive'  => 'Статья в архиве',
    'header_article_rss'       => 'RSS',
    'tooltip_article_rss'      => 'Отображается в RSS ленте',
    'tooltip_article_visits'   => 'Количество посещений пользователями',
    'header_article_sitemap'   => 'Карта',
    'tooltip_article_sitemap'  => 'Отображается на карте сайта (Sitemap протокол)',
    'header_article_caching'   => 'Кэш',
    'tooltip_article_caching'  => 'Кэширования страницы',
    'header_access_name'       => 'Доступ',
    'tooltip_access_name'      => 'Права доступа к статье',
    'header_article_parsing'   => 'Анализ',
    'tooltip_article_parsing'  => 'Присутствие в статье динамических компонентов',
    'header_template_name'     => 'Шаблон',
    'tooltip_template_name'    => 'Шаблон отображения статьи',
    'header_robots'            => 'Индексация',
    'tooltip_robots'           => 'Мета &quot;robots&quot; на странице сайта',
    'header_article_landing'   => 'Лендинг страница',
    'tooltip_article_landing'  => 'Статья представлена в виде целевой страницы (landing page)',
    'tooltip_article_link'     => 'Просмотр сраницы сайта',
    'header_article_folder'    => 'Каталог',
    'tooltip_article_folder'   => 'Название каталога где находятся файлы (изображения, документы) статьи',
    // развернутая запись
    'title_fieldset_common'  => 'Атрибуты',
    'label_published_date'   => 'Дата публикации',
    'label_article_note'     => 'Заметка',
    'label_page_header'      => 'Заголовок',
    'label_category_name'    => 'Категория',
    'label_access_name'      => 'Права доступа',
    'label_template_name'    => 'Шаблон отображения статьи',
    'label_article_uri'      => 'ЧПУ URL статьи',
    'label_article_uri_hash' => 'Хэш ЧПУ URL статьи',
    'label_language'         => ' (язык - %s)',
    // вкладка "Дополнительные параметры"
    'title_fieldset_add'      => 'Дополнительные параметры',
    'label_article_index'     => 'Порядковый номер в списке',
    'label_type_name'         => 'Тип статьи',
    'label_article_rss'       => 'В ленте RSS',
    'label_article_archive'   => 'Статья в архиве',
    'label_article_search'    => 'Участвует в поиске',
    'label_article_hidden'    => 'Видимость на сайте',
    'label_article_print'     => 'Вывод статьи для печати',
    'label_article_header'    => 'Выводить заголовок статьи',
    'label_article_caching'   => 'Кэширование страницы',
    'label_article_parsing'   => 'Присутствие динам-х компонентов',
    'label_article_nodes'     => 'Количество потомков',
    // вкладка "Карта сайта (протокол Sitemap)"
    'title_fieldset_sitemap'   => 'Карта сайта (протокол Sitemap)',
    'label_article_sitemap'    => 'Отображается на карте сайта',
    'label_sitemap_priority'   => 'Приоритетность',
    'label_sitemap_changefreq' => 'Частота',
    // вкладка "SEO статьи"
    'title_fieldset_seo' => 'SEO статьи',
    'label_page_meta_r'  => 'Роботы (robots)',
    'label_page_meta_v'  => 'Время и интервал посещения поискового робота (meta revisit)',
    'label_page_meta_m'  => 'Состояние (meta document)',
    'label_page_meta_a'  => 'Автор (author)',
    'label_page_meta_c'  => 'Организация (copyright)',
    'label_page_meta_k'  => 'Ключевые слова (keywords)',
    'label_page_meta_d'  => 'Описание (description)',
    // вкладка "страница"
    'title_fieldset_page'   => 'Страница',
    'label_page_header'     => 'Заголовок',
    'label_page_menu'       => 'В меню',
    'label_page_breadcrumb' => 'В цепочке',
    'label_page_html_short' => 'Описание (сокр.)',
    // вкладка "кэш страницы"
    'title_fieldset_cache' => 'Кэш страницы',
    'label_fld_date'       => 'Дата',
    'label_fld_file'       => 'Файл',
    'label_fld_address'    => 'Адрес',
    // вкладка "текст"
    'title_fieldset_text' => 'Текст статьи',
    // быстрый фильтр
    'text_all_records' => 'Все статьи',
    'text_by_date'     => 'По дате правки статьи',
    'text_by_date_pub' => 'По дате публикации статьи',
    'text_by_day'      => 'За день',
    'text_by_week'     => 'За неделю',
    'text_by_month'    => 'За месяц',
    'text_by_author'   => 'По автору статьи',
    'text_by_category' => 'По категории',
    'text_by_tag'      => 'По тегу',
    'text_by_visible'  => 'По публикации',
    'text_by_archive'  => 'По расположению в архиве',
    'text_by_rss'      => 'По расположению в RSS ленте',
    'text_by_sitemap'  => 'По отображению на Sitemap',
    'text_by_cache'    => 'По кэшированию',
    'text_by_access'   => 'По правам доступа',
    'text_by_search'   => 'По возможности поиска',
    'text_by_header'   => 'По отображению загаловка',
    'text_by_template' => 'По шаблону',
    'text_by_yes'      => 'Да',
    'text_by_no'       => 'Нет',
    'text_by_view'     => 'По виду страниц',
    // тип
    'data_unknow'  => '[неизвестно]',
    'data_boolean' => array('нет', 'да'),
    'data_priorities' => array(
        array('0.0'), array('0.1'), array('0.2'), array('0.3'), array('0.4'), array('0.5'), array('0.6'),
        array('0.7'), array('0.8'), array('0.9'), array('1.0')
    ),
    'data_changefreq' => array(
        array('всегда', 'a'), array('почасовая', 'h'), array('ежедневно', 'd'), array('еженедельно', 'w'), array('ежемесячно', 'm'),
        array('ежегодно', 'y'), array('никогда', 'n')
    ),
    'data_changefreq_a' => array('a' => 'всегда', 'h' => 'почасовая', 'd' => 'ежедневно', 'w' => 'еженедельно', 'm' => 'ежемесячно', 'y' => 'ежегодно', 'n' => 'никогда'),
    'data_robots' => array(
        array('all'), array('index, follow'), array('noindex, follow'), array('index, nofollow'), array('noindex, nofollow'),
        array('none')
    ),
    'data_revisit' => array(
        array('1 день', 1), array('2 дня', 2), array('3 дня', 3), array('4 дня', 4), array('5 дней', 5), array('6 дней', 6), array('7 дней', 7)
    ),
    'data_doc' => array(array('Static'), array(' Dynamic')),
    'data_depends' => array('"Статьи" (%s)', '"Галереи" (%s)')
);
?>