<?php
/**
 * Gear Manager
 *
 * Контроллер         "Развёрнутая запись статьи"
 * Пакет контроллеров "Статьи"
 * Группа пакетов     "Статьи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SArticles_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Expand');

/**
 * Развёрнутая запись книги
 * 
 * @category   Gear
 * @package    GController_SArticles_Grid
 * @subpackage Expand
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SArticles_Grid_Expand extends GController_Grid_Expand
{
    /**
     * Вывод данных в интерфейс
     * 
     * @return void
     */
    protected function dataExpand()
    {
        parent::dataAccessView();

        $data = $this->getAttributes();
        $this->response->data = $data;
    }

    /**
     * Возращает атрибуты записи
     * 
     * @return string
     */
    protected function getAttributes()
    {
        $data = '';
        $languageId = $this->config->getFromCms('Site', 'LANGUAGE/ID');
        $query = new GDb_Query();
        // статья
        $sql = 
            'SELECT SQL_CALC_FOUND_ROWS `t`.`template_name`, `t`.`template_icon`, `p`.`page_header`, `p`.`page_meta_robots`, `a`.*, `ac`.`category_name`, `as`.`access_name` '
          . 'FROM `site_articles` `a` '
            // страницы
          . 'LEFT JOIN `site_pages` `p` ON `p`.`article_id`=`a`.`article_id` AND `p`.`language_id`=' . $languageId . ' '
            // категории статей
          . 'LEFT JOIN `site_categories` `ac` ON `ac`.`category_id`=`a`.`category_id` '
            // доступ к статьям
          . 'LEFT JOIN `site_article_access` `as` USING (`access_id`) '
            // шаблоны статей
          . 'LEFT JOIN `site_templates` `t` ON `t`.`template_id`=`a`.`template_id` '
          . 'WHERE `a`.`article_id`=' . $this->uri->id;
        if (($article = $query->getRecord($sql)) === false)
            throw new GSqlException();
        // страница
        $sql = 'SELECT * FROM `site_pages` WHERE `article_id`=' . $article['article_id'] . ' AND `language_id`=' . $languageId;
        if (($page = $query->getRecord($sql)) === false)
            throw new GSqlException();
        // статья
        $data = '<div><fieldset class="fixed" style="width:33%"><label>' . $this->_['title_fieldset_common'] . '</label><ul>';
        //if ($article['article_date'])
           // $data .= '<li><label>' . $this->_['label_article_date'] . ':</label> ' . date('d-m-Y H:i:s', strtotime($article['article_date'])) . '</li>';
        $data .= '<li><label>' . $this->_['label_article_note'] . ':</label> ' . $article['article_note'] . '</li>';
        $data .= '<li><label>' . $this->_['label_template_name'] . ':</label> ' . $article['template_name'] . '</li>';
        $data .= '<li><label>' . $this->_['label_category_name'] . ':</label> ' . $article['category_name'] . '</li>';
        $data .= '<li><label>' . $this->_['label_article_uri'] . ':</label> ' . $article['article_uri'] . '</li>';
        $data .= '<li><label>' . $this->_['label_article_uri_hash'] . ':</label> ' . $article['article_uri_hash'] . '</li>';
        if ($article['published_date'])
            $data .= '<li><label>' . $this->_['label_published_date'] . ':</label> ' . date('d-m-Y', strtotime($article['published_date'])) . ' ' . $article['published_time'] . '</li>';
        $data .= '</ul></fieldset>';
        // дополнительные параметры
        $data .= '<fieldset class="fixed" style="width:33%"><label>' . $this->_['title_fieldset_add'] . '</label><ul>';
        $data .= '<li><label>' . $this->_['label_article_index'] . ':</label> ' . $article['article_index'] . '</li>';
        //$data .= '<li><label>' . $this->_['label_type_name'] . ':</label> ' . $article['type_name'] . '</li>';
        $data .= '<li><label>' . $this->_['label_access_name'] . ':</label> ' . $article['access_name'] . '</li>';
        $data .= '<li><label>' . $this->_['label_article_rss'] . ':</label> ' . $this->_['data_boolean'][(int) $article['article_rss']] . '</li>';
        $data .= '<li><label>' . $this->_['label_article_archive'] . ':</label> ' . $this->_['data_boolean'][(int) $article['article_archive']] . '</li>';
        $data .= '<li><label>' . $this->_['label_article_search'] . ':</label> ' . $this->_['data_boolean'][(int) $article['article_search']] . '</li>';
        $data .= '<li><label>' . $this->_['label_article_print'] . ':</label> ' . $this->_['data_boolean'][(int) $article['article_print']] . '</li>';
        $data .= '<li><label>' . $this->_['label_article_header'] . ':</label> ' . $this->_['data_boolean'][(int) $article['article_header']] . '</li>';
        $data .= '<li><label>' . $this->_['label_article_caching'] . ':</label> ' . $this->_['data_boolean'][(int) $article['article_caching']] . '</li>';
        $data .= '<li><label>' . $this->_['label_article_parsing'] . ':</label> ' . $this->_['data_boolean'][(int) empty($page['page_parsing'])] . '</li>';
        $data .= '</ul></fieldset>';
        // карта сайта (протокол Sitemap)
        $data .= '<fieldset class="fixed" style="width:33%"><label>' . $this->_['title_fieldset_sitemap'] . '</label><ul>';
        $data .= '<li><label>' . $this->_['label_article_sitemap'] . ':</label> ' . $this->_['data_boolean'][(int) $article['article_sitemap']] . '</li>';
        $data .= '<li><label>' . $this->_['label_sitemap_priority'] . ':</label> ' . ($article['sitemap_priority'] ? $article['sitemap_priority'] : $this->_['data_unknow']) . '</li>';
        $data .= '<li><label>' . $this->_['label_sitemap_changefreq'] . ':</label> ' . ($article['sitemap_changefreq'] ? $article['sitemap_changefreq'] : $this->_['data_unknow']) . '</li>';
        $data .= '</ul></fieldset></div>';
        if ($page) {
            $lid = $this->config->getFromCms('Site', 'LANGUAGE');
            $langs = $this->config->getFromCms('Site', 'LANGUAGES');
            $language = $langs[$lid]['title'];
            // страница
            $data .= '<fieldset style="width:100%"><label>' . $this->_['title_fieldset_page'] . sprintf($this->_['label_language'], $language) . '</label><ul>';
            $data .= '<li><label>' . $this->_['label_page_header'] . ':</label> ' . (!empty($page['page_header']) ? $page['page_header'] : '') . '</li>';
            $data .= '<li><label>' . $this->_['label_page_menu'] . ':</label> ' . (!empty($page['page_menu']) ? $page['page_menu'] : $this->_['data_unknow']). '</li>';
            $data .= '<li><label>' . $this->_['label_page_breadcrumb'] . ':</label> ' . (!empty($page['page_breadcrumb']) ? $page['page_breadcrumb'] : $this->_['data_unknow']) . '</li>';
            $data .= '</ul></fieldset>';
            // SEO статьи
            $data .= '<fieldset style="width:100%"><label>' . $this->_['title_fieldset_seo'] . sprintf($this->_['label_language'], $language) . '</label><ul>';
            $data .= '<li><label>' . $this->_['label_page_meta_r'] . ':</label> ' . (!empty($page['page_meta_robots']) ? $page['page_meta_robots'] : $this->_['data_unknow']). '</li>';
            $data .= '<li><label>' . $this->_['label_page_meta_v'] . ':</label> ' . (!empty($page['page_meta_revisit']) ? $page['page_meta_revisit'] : $this->_['data_unknow']) . '</li>';
            $data .= '<li><label>' . $this->_['label_page_meta_m'] . ':</label> ' . (!empty($page['page_meta_document']) ? $page['page_meta_document'] : $this->_['data_unknow']) . '</li>';
            $data .= '<li><label>' . $this->_['label_page_meta_a'] . ':</label> ' . (!empty($page['page_meta_author']) ? $page['page_meta_author'] : $this->_['data_unknow']) . '</li>';
            $data .= '<li><label>' . $this->_['label_page_meta_c'] . ':</label> ' . (!empty($page['page_meta_copyright']) ? $page['page_meta_copyright'] : $this->_['data_unknow']) . '</li>';
            $data .= '<li><label>' . $this->_['label_page_meta_k'] . ':</label> ' . (!empty($page['page_meta_keywords']) ? $page['page_meta_keywords'] : '') . '</li>';
            $data .= '<li><label>' . $this->_['label_page_meta_d'] . ':</label> ' . (!empty($page['page_meta_description']) ? $page['page_meta_description'] : '') . '</li>';
            $data .= '</ul></fieldset>';
        }
        // кэш
        $sql = 'SELECT * FROM `site_cache` WHERE `cache_generator`=\'article\' AND `cache_generator_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->getCountRecords())
        $data .= '<fieldset class="fixed"><label>' . $this->_['title_fieldset_cache'] . '</label><ul>';
        while (!$query->eof()) {
            $cache = $query->next();
            $data .= '<li><label>' . $this->_['label_fld_date'] . ':</label> ' . date('d-m-Y H:i:s', strtotime($cache['cache_date'])) . '</li>';
            $data .= '<li><label>' . $this->_['label_fld_file'] . ':</label> <a target="blank" href="/cache/' . $cache['cache_filename'] . '">' . $cache['cache_filename'] . '</a></li>';
            $data .= '<li><label>' . $this->_['label_fld_address'] . ':</label> <a target="blank" href="' . $cache['cache_uri'] . '">http://' . $_SERVER['HTTP_HOST'] . $cache['cache_uri'] . '</a></li>';
        }
         $data .= '</ul></fieldset>';

        $data .= '<div class="wrap"></div>';
        $data = '<div class="mn-row-body border">' . $data . '<div class="wrap"></div></div>';

        return $data;
    }
}
?>