<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка статей сайта"
 * Пакет контроллеров "Статьи"
 * Группа пакетов     "Статьи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SArticles_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::ns('Controller/Grid/Interface');
Gear::ns('User/User');
Gear::ns('User/Access');

/**
 * Интерфейс списка статей сайта
 * 
 * @category   Gear
 * @package    GController_SArticles_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SArticles_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * (для обработки записей таблицы)
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'article_id';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'article_id';

    /**
     * Использовать быстрый фильтр
     *
     * @var boolean
     */
    public $useSlidePanel = true;

    /**
     * Возращает список доменов
     * 
     * @return array
     */
    protected function getDomains()
    {
        $arr = $this->config->getFromCms('Domains');
        $list = array();
        if ($arr) {
            $list[] = array('Все', -1);
            $list[] = array($this->config->getFromCms('Site', 'NAME', ''), 0);
            $domains = $arr['domains'];
            foreach ($arr['indexes'] as $key => $value) {
                $list[] = array($value[1], $key);
            } 
        }

        return $list;
    }

    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        parent::getDataInterface();

/*
        $cats =  GUser_Access::forData('site_categories_access', 'group', 'category_id');
        if ($cats)
            $cats = implode(',', $cats);*/
        $this->store->set('access.categories', array());
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        $data = $this->getDataInterface();
    
        // поля списка  (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // название сайта
            array('name' => 'domain_id', 'type' => 'string'),
            // идент. статьи
            array('name' => 'article_id', 'type' => 'integer'),
            array('name' => 'article_url', 'type' => 'string'),
            // порядковый номер в списке
            array('name' => 'article_index', 'type' => 'integer'),
            // дата изменения или создания статьи
            array('name' => 'published_date', 'type' => 'string'),
            array('name' => 'published_time', 'type' => 'string'),
            // заметка статьи
            array('name' => 'article_note', 'type' => 'string'),
            // загаловок статьи
            array('name' => 'page_header', 'type' => 'string'),
            // категория статьи
            array('name' => 'category_name', 'type' => 'string'),
            array('name' => 'category_uri', 'type' => 'string'),
            // URI статьи
            array('name' => 'article_uri', 'type' => 'string'),
            // если статья в архиве
            array('name' => 'article_archive', 'type' => 'integer'),
            // если RSS лента включает статью
            array('name' => 'article_rss', 'type' => 'integer'),
            // количество визитов
            array('name' => 'article_visits', 'type' => 'integer'),
            // статья опубликована
            array('name' => 'published', 'type' => 'integer'),
            // если статья присутствует на карте сайта
            array('name' => 'article_sitemap', 'type' => 'integer'),
            array('name' => 'article_map', 'type' => 'integer'),
            array('name' => 'article_landing', 'type' => 'integer'),
            // кэширование страницы сайта
            array('name' => 'article_caching', 'type' => 'integer'),
            // категория доступа к статье
            array('name' => 'access_name', 'type' => 'string'),
            // анализ статьи на наличие динамических компонентов
            array('name' => 'article_parsing', 'type' => 'integer'),
            // шаблон статьи
            array('name' => 'template_icon', 'type' => 'string'),
            array('name' => 'template_filename', 'type' => 'string'),
            // индексация
            array('name' => 'page_meta_robots', 'type' => 'string'),
            array('name' => 'page_meta_robots_s', 'type' => 'string'),
            // папка статьи
            array('name' => 'article_folder', 'type' => 'string'),
            // ссылка на страницу сайта
            array('name' => 'article_link', 'type' => 'string'),
            // теги
            array('name' => 'page_tags',  'type' => 'string')
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $settings = $this->session->get('user/settings');
        $this->columns = array(
            array('xtype'     => 'expandercolumn',
                  'url'       => $this->componentUrl . 'grid/expand/',
                  'typeCt'    => 'row'),
            array('xtype'     => 'numbercolumn'),
            array('xtype'     => 'rowmenu'),
            array('dataIndex' => 'domain_id',
                  'header'    => $this->_['header_site_name'],
                  'width'     => 90,
                  'hidden'    => !$this->config->getFromCms('Site', 'OUTCONTROL'),
                  'sortable'  => true),
            array('dataIndex' => 'article_index',
                  'hidden'    => true,
                  'header'    => $this->_['header_article_index'],
                  'tooltip'   => $this->_['tooltip_article_index'],
                  'width'     => 35,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('xtype'     => 'datetimecolumn',
                  'frmDate'   => $settings['format/date'],
                  'frmTime'   => $settings['format/time'],
                  'dataIndex' => 'published_date',
                  'timeIndex' => 'published_time',
                  'header'    => $this->_['header_published_date'],
                  'tooltip'   => $this->_['tooltip_published_date'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'date')),
            array('dataIndex' => 'article_note',
                  'hidden'    => true,
                  'header'    => $this->_['header_article_note'],
                  'width'     => 150,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'page_header',
                  'header'    => $this->_['header_page_header'],
                  'header'    => '<em class="mn-grid-hd-details"></em>' . $this->_['header_page_header'],
                  'tooltip'   => $this->_['tooltip_page_header'],
                  'width'     => 160,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'category_name',
                  'header'    => $this->_['header_category_name'],
                  'tooltip'    => $this->_['tooltip_category_name'],
                  'width'     => 110,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'page_tags',
                  'header'    => $this->_['header_page_tags'],
                  'width'     => 110,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'article_link',
                  'align'     => 'center',
                  'header'    => '&nbsp;',
                  'tooltip'   => $this->_['tooltip_article_link'],
                  'fixed'     => true,
                  'hideable'  => false,
                  'width'     => 25,
                  'sortable'  => false,
                  'menuDisabled' => true),
            array('dataIndex' => 'article_uri',
                  'header'    => $this->_['header_article_uri'],
                  'tooltip'   => $this->_['tooltip_article_uri'],
                  'width'     => 180,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'article_folder',
                  'header'    => '<img align="absmiddle" src="' . $this->resourcePath . 'icon-hd-folder.png"> ' . $this->_['header_article_folder'],
                  'tooltip'   => $this->_['tooltip_article_folder'],
                  'hidden'    => true,
                  'width'     => 85,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'article_visits',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-visits.png" align="absmiddle">',
                  'tooltip'   => $this->_['tooltip_article_visits'],
                  'width'     => 40,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('xtype'     => 'booleancolumn',
                  'align'     => 'center',
                  'cls'       => 'mn-bg-color-gray2',
                  'dataIndex' => 'published',
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-visible.png" align="absmiddle">',
                  'width'     => 40,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean')),
            array('xtype'     => 'booleancolumn',
                  'align'     => 'center',
                  'dataIndex' => 'article_archive',
                  'hidden'    => true,
                  'cls'       => 'mn-bg-color-gray2',
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath. 'icon-hd-archive.png" align="absmiddle">',
                  'tooltip'   => $this->_['tooltip_article_archive'],
                  'width'     => 40,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('xtype'     => 'booleancolumn',
                  'align'     => 'center',
                  'dataIndex' => 'article_rss',
                  'hidden'    => true,
                  'cls'       => 'mn-bg-color-gray2',
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-rss.png" align="absmiddle">',
                  'tooltip'   => $this->_['tooltip_article_rss'],
                  'width'     => 40,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('xtype'     => 'booleancolumn',
                  'align'     => 'center',
                  'dataIndex' => 'article_map',
                  'hidden'    => true,
                  'cls'       => 'mn-bg-color-gray2',
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-sitemap.png" align="absmiddle">',
                  'tooltip'   => $this->_['tooltip_article_sitemap'],
                  'width'     => 40,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean')),
            array('xtype'     => 'booleancolumn',
                  'align'     => 'center',
                  'cls'       => 'mn-bg-color-gray2',
                  'dataIndex' => 'article_caching',
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-cache.png" align="absmiddle">',
                  'tooltip'   => $this->_['tooltip_article_caching'],
                  'width'     => 40,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean')),
            array('align'     => 'center',
                  'dataIndex' => 'access_name',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-access.png" align="absmiddle">',
                  'tooltip'   => $this->_['tooltip_access_name'],
                  'width'     => 40,
                  'sortable'  => true),
            array('xtype'     => 'booleancolumn',
                  'align'     => 'center',
                  'dataIndex' => 'article_landing',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-landing.png" align="absmiddle">',
                  'tooltip'   => $this->_['tooltip_article_landing'],
                  'width'     => 40,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean')),
            array('xtype'     => 'booleancolumn',
                  'hidden'    => true,
                  'align'     => 'center',
                  'dataIndex' => 'article_parsing',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-parsing.png" align="absmiddle">',
                  'tooltip'   => $this->_['tooltip_article_parsing'],
                  'width'     => 40,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'template_icon',
                  'header'    => '&nbsp;',
                  'tooltip'   => $this->_['tooltip_template_name'],
                  'fixed'     => true,
                  'hideable'  => false,
                  'width'     => 25,
                  'sortable'  => false,
                  'menuDisabled' => true),
            array('dataIndex' => 'page_meta_robots',
                  'header'    => $this->_['header_robots'],
                  'tooltip'   => $this->_['tooltip_robots'],
                  'width'     => 115,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string'))
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_grid'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_grid'],
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'profileUrl'    => $this->componentUrl . 'profile/')
        );

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "edit" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "добавить" (ExtJS class "Manager.button.InsertData")
        $group->items->add(array('xtype' => 'mn-btn-insert-data', 'gridId' => $this->classId));
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' => $this->classId));
        /*
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "история" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'   => 'mn-btn-widget',
                  'icon'    => $this->resourcePath . 'icon-btn-log.png',
                  'text'    => $this->_['text_btn_log'],
                  'tooltip' => $this->_['tooltip_btn_log'],
                  'width'   => 60,
                  'url'     => $this->componentUri . '../log/' . ROUTER_DELIMITER . 'grid/interface/')
        );
        */
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'      => 'mn-btn-clear-data',
                  'msgConfirm' => $this->_['msg_btn_clear'],
                  'gridId'     => $this->classId,
                  'isN'        => true)
        );
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = 'component-site-articles';
        $this->_cmp->tbar->items->add($group);

        // управление сайтами на других доменах
        if ($this->config->getFromCms('Site', 'OUTCONTROL')) {
            $domains = $this->getDomains();
            if ($domains) {
                // группа кнопок "фильтр" (ExtJS class "Ext.ButtonGroup")
                $group = new Ext_ButtonGroup_Filter(
                    array('title'  => $this->_['title_buttongroup_filter'],
                          'gridId' => $this->classId)
                );
                // кнопка "справка" (ExtJS class "Manager.button.Help")
                $btn = &$group->items->get(0);
                $btn['gridId'] = $this->classId;
                $btn['url'] = $this->componentUrl . 'search/data/';
                // кнопка "поиск" (ExtJS class "Manager.button.Search")
                $filter = $this->store->get('tlbFilter', array());
                $group->items->add(array(
                    'xtype'       => 'form',
                    'labelWidth'  => 35,
                    'bodyStyle'   => 'border:1px solid transparent;background-color:transparent',
                    'frame'       => false,
                    'bodyBorder ' => false,
                    'items'       => array(
                        array('xtype'         => 'combo',
                              'fieldLabel'    => $this->_['label_domain'],
                              'name'          => 'domain',
                              'checkDirty'    => false,
                              'editable'      => false,
                              'width'         => 140,
                              'typeAhead'     => false,
                              'triggerAction' => 'all',
                              'mode'          => 'local',
                              'store'         => array(
                                  'xtype'  => 'arraystore',
                                  'fields' => array('list', 'value'),
                                  'data'   => $domains
                              ),
                              'value'         => empty($filter) ? '' : $filter['domain'],
                              'hiddenName'    => 'domain',
                              'valueField'    => 'value',
                              'displayField'  => 'list',
                              'allowBlank'    => false)
                    )
                ));
                $this->_cmp->tbar->items->add($group);
            }
        }

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $cellInfo =
            '<div class="mn-grid-cell-tooltip-tl">{page_header}</div>'
          . '<div class="mn-grid-cell-tooltip">'
          . '<em>' . $this->_['header_published_date'] . '</em>: <b>{published_date}</b><br>'
          . '<em>' . $this->_['header_article_index'] . '</em>: <b>{article_index}</b><br>'
          . '<em>' . $this->_['header_article_note'] . '</em>: <b>{article_note}</b><br>'
          . '<em>' . $this->_['header_category_name'] . '</em>: <b>{category_name}</b><br>'
          . '<em>' . $this->_['header_article_uri'] . '</em>: <b>{article_uri}</b><br>'
          . '<em>' . $this->_['header_robots'] . '</em>: <b>{page_meta_robots_s}</b><br>'
          . '<em>' . $this->_['tooltip_published'] . '</em>: '
          . '<tpl if="published == 0"><b>' . $this->_['data_boolean'][0] . '</b>;</tpl>'
          . '<tpl if="published == 1"><b>' . $this->_['data_boolean'][1] . '</b>;</tpl><br>'
          . '<em>' . $this->_['tooltip_article_archive'] . '</em>: '
          . '<tpl if="article_archive == 0"><b>' . $this->_['data_boolean'][0] . '</b>;</tpl>'
          . '<tpl if="article_archive == 1"><b>' . $this->_['data_boolean'][1] . '</b>;</tpl><br>'
          . '<em>' . $this->_['tooltip_article_rss'] . '</em>: '
          . '<tpl if="article_rss == 0"><b>' . $this->_['data_boolean'][0] . '</b>;</tpl>'
          . '<tpl if="article_rss == 1"><b>' . $this->_['data_boolean'][1] . '</b>;</tpl><br>'
          . '<em>' . $this->_['tooltip_article_sitemap'] . '</em>: '
          . '<tpl if="article_sitemap == 0"><b>' . $this->_['data_boolean'][0] . '</b></tpl>'
          . '<tpl if="article_sitemap == 1"><b>' . $this->_['data_boolean'][1] . '</b></tpl><br>'
          . '<em>' . $this->_['tooltip_article_caching'] . '</em>: '
          . '<tpl if="article_caching == 0"><b>' . $this->_['data_boolean'][0] . '</b></tpl>'
          . '<tpl if="article_caching == 1"><b>' . $this->_['data_boolean'][1] . '</b></tpl><br>'
          . '<em>' . $this->_['tooltip_article_parsing'] . '</em>: '
          . '<tpl if="article_parsing == 0"><b>' . $this->_['data_boolean'][0] . '</b>;</tpl>'
          . '<tpl if="article_parsing == 1"><b>' . $this->_['data_boolean'][1] . '</b>;</tpl><br>'
          . '</div>';
         $this->_cmp->cellTips = array(
            array('field' => 'article_index', 'tpl' => '{article_index}'),
            array('field' => 'article_note', 'tpl' => '{article_note}'),
            array('field' => 'page_header', 'tpl' => $cellInfo),
            array('field' => 'category_name', 'tpl' => '{category_name} ({category_uri})'),
            array('field' => 'article_uri', 'tpl' => '{article_url}'),
            array('field' => 'article_folder', 'tpl' => '<tpl if="article_folder">"' . $this->config->getFromCms('Site', 'DIR/DOCS') . '{article_folder}/"<br>"' . $this->config->getFromCms('Site', 'DIR/IMAGES') . '{article_folder}/"</tpl>')
        );

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $items = array(
            array('text'  => $this->_['rowmenu_edit'],
                  'icon'  => $this->resourcePath . 'icon-item-edit.png',
                  'url'   => $this->componentUrl . 'profile/interface/'),
            array('xtype' => 'menuseparator'),
            array('text'  => $this->_['rowmenu_landing'],
                  'icon'  => $this->resourcePath . 'icon-hd-landing.png',
                  'url'   => $this->componentUrl . '../../../landing/pages/' . ROUTER_DELIMITER . 'grid/interface/'),
            array('text'  => $this->_['rowmenu_view'],
                  'icon'  => $this->resourcePath . 'icon-item-link.png',
                  'url'   => $this->componentUrl . 'view/interface/'),
        );
        $this->_cmp->rowMenu = array('xtype' => 'mn-grid-rowmenu', 'items' => $items);

        // быстрый фильтр списка (ExtJs class "Manager.tree.GridFilter")
        $this->_slidePanel->cls = 'mn-tree-gridfilter';
        $this->_slidePanel->width = 250;
        $this->_slidePanel->initRoot = array(
            'text'     => 'Filter',
            'id'       => 'byRoot',
            'expanded' => true,
            'children' => array(
                array('text'     => $this->_['text_all_records'],
                      'value'    => 'all',
                      'expanded' => true,
                      'leaf'     => false,
                      'children' => array(
                        array('text'     => $this->_['text_by_date'],
                              'id'       => 'byDt',
                              'leaf'     => false,
                              'expanded' => true,
                              'children' => array(
                                  array('text'     => $this->_['text_by_day'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 'day'),
                                  array('text'     => $this->_['text_by_week'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true, 
                                        'value'    => 'week'),
                                  array('text'     => $this->_['text_by_month'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 'month')
                              )
                        ),
                        array('text'     => $this->_['text_by_date_pub'],
                              'id'       => 'byPb',
                              'leaf'     => false,
                              'expanded' => true,
                              'children' => array(
                                  array('text'     => $this->_['text_by_day'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 'day'),
                                  array('text'     => $this->_['text_by_week'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true, 
                                        'value'    => 'week'),
                                  array('text'     => $this->_['text_by_month'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 'month')
                              )
                        ),
                        array('text'    => $this->_['text_by_author'],
                              'id'      => 'byAu',
                              'leaf'    => false),
                        array('text'    => $this->_['text_by_category'],
                              'id'      => 'byCt',
                              'leaf'    => false),
                        array('text'    => $this->_['text_by_tag'],
                              'id'      => 'byTg',
                              'leaf'    => false),
                        array('text'     => $this->_['text_by_visible'],
                              'id'      => 'byHd',
                              'leaf'     => false,
                              'expanded' => false,
                              'children' => array(
                                  array('text'     => $this->_['text_by_yes'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 0),
                                  array('text'     => $this->_['text_by_no'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 1)
                              )
                        ),
                        array('text'     => $this->_['text_by_archive'],
                              'id'      => 'byAr',
                              'leaf'     => false,
                              'expanded' => false,
                              'children' => array(
                                  array('text'     => $this->_['text_by_yes'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 1),
                                  array('text'     => $this->_['text_by_no'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 0)
                              )
                        ),
                        array('text'     => $this->_['text_by_rss'],
                              'id'      => 'byRs',
                              'leaf'     => false,
                              'expanded' => false,
                              'children' => array(
                                  array('text'     => $this->_['text_by_yes'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 1),
                                  array('text'     => $this->_['text_by_no'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 0)
                              )
                        ),
                        array('text'     => $this->_['text_by_sitemap'],
                              'id'      => 'bySm',
                              'leaf'     => false,
                              'expanded' => false,
                              'children' => array(
                                  array('text'     => $this->_['text_by_yes'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 1),
                                  array('text'     => $this->_['text_by_no'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 0)
                              )
                        ),
                        array('text'     => $this->_['text_by_cache'],
                              'id'      => 'byCa',
                              'leaf'     => false,
                              'expanded' => false,
                              'children' => array(
                                  array('text'     => $this->_['text_by_yes'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 1),
                                  array('text'     => $this->_['text_by_no'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 0)
                              )
                        ),
                        array('text'     => $this->_['text_by_search'],
                              'id'      => 'bySe',
                              'leaf'     => false,
                              'expanded' => false,
                              'children' => array(
                                  array('text'     => $this->_['text_by_yes'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 1),
                                  array('text'     => $this->_['text_by_no'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 0)
                              )
                        ),
                        array('text'     => $this->_['text_by_header'],
                              'id'      => 'byHe',
                              'leaf'     => false,
                              'expanded' => false,
                              'children' => array(
                                  array('text'     => $this->_['text_by_yes'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 1),
                                  array('text'     => $this->_['text_by_no'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 0)
                              )
                        ),
                        array('text'    => $this->_['text_by_access'],
                              'id'      => 'byAc',
                              'leaf'    => false),
                        array('text'    => $this->_['text_by_template'],
                              'id'      => 'byTm',
                              'leaf'    => false),
                        array('text'    => $this->_['text_by_view'],
                              'id'      => 'byTv',
                              'leaf'    => false),
                    )
                )
            )
        );

        parent::getInterface();
    }
}
?>