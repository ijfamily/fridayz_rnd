<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка статей сайта"
 * Пакет контроллеров "Статьи"
 * Группа пакетов     "Статьи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SArticles_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::ns('Controller/Grid/Data');
Gear::library('Article');

/**
 * Данные списка статей сайта
 * 
 * @category   Gear
 * @package    GController_SArticles_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SArticles_Grid_Data extends GController_Grid_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * (для обработки записей таблицы)
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'article_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_articles';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sarticles_grid';

    /**
     * Домены
     *
     * @var array
     */
    public $domains = array();

    /**
     * Управления сайтами на других доменах
     *
     * @var boolean
     */
    public $isOutControl = false;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // управление сайтами на других доменах
        $this->isOutControl = $this->config->getFromCms('Site', 'OUTCONTROL');
        if ($this->isOutControl)
            $this->domains = $this->config->getFromCms('Domains', 'indexes');
        $this->domains[0] = array($_SERVER['SERVER_NAME'], $this->config->getFromCms('Site', 'NAME'));

        // если сайт использует ЧПУ
        $this->sef = $this->config->getFromCms('Site', 'SEF');
        $languageId = $this->config->getFromCms('Site', 'LANGUAGE/ID');
        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS `t`.`template_name`, `t`.`template_icon`, `p`.`page_header`, `p`.`page_tags`, `p`.`page_meta_robots`, `a`.*, `ac`.`category_name`, `as`.`access_name`, `ac`.`category_uri` '
          . 'FROM `site_articles` `a` '
            // страницы
          . 'LEFT JOIN `site_pages` `p` ON `p`.`article_id`=`a`.`article_id` AND `p`.`language_id`=' . $languageId . ' '
            // категории статей
          . 'LEFT JOIN `site_categories` `ac` ON `ac`.`category_id`=`a`.`category_id` '
            // доступ к статьям
          . 'LEFT JOIN `site_article_access` `as` USING (`access_id`) '
            // шаблоны статей
          . 'LEFT JOIN `site_templates` `t` ON `t`.`template_id`=`a`.`template_id` WHERE 1 ';
        // фильтр из панели инструментов
        if ($this->isOutControl) {
            $filter = $this->store->get('tlbFilter', false);
            if ($filter) {
                $domainId = (int) $filter['domain'];
                if ($domainId >= 0)
                    $this->sql .= ' AND `a`.`domain_id`=' . $domainId;
            }
        }
        // ограничения  на доступ к категориям
        if ($cats = $this->store->get('access.categories'))
            $this->sql .= ' AND `a`.`category_id` IN (' . $cats . ') ';
        $this->sql .= '%filter %fastfilter ORDER BY %sort LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // название сайта
            'domain_id' => array('type' => 'string', 'as' => 'a.domain_id'),
            // системные поля
            'sys_date_update' => array('type' => 'string', 'as' => 'a.sys_date_update'),
            'sys_date_insert' => array('type' => 'string', 'as' => 'a.sys_date_insert'),
            // идент. статьи
            'article_id' => array('type' => 'integer'),
            'article_url' => array('type' => 'string'),
            // порядковый номер в списке
            'article_index' => array('type' => 'integer'),
            // дата изменения или создания статьи
            'published_date' => array('type' => 'string'),
            'published_time' => array('type' => 'string'),
            // заметка статьи
            'article_note' => array('type' => 'string'),
            // загаловок статьи
            'page_header' => array('type' => 'string'),
            // категория статьи
            'category_name' => array('type' => 'string'),
            'category_uri'  => array('type' => 'string'),
            'category_id'   => array('type' => 'integer'),
            // URI статьи
            'article_uri' => array('type' => 'string'),
            // если статья в архиве
            'article_archive' => array('type' => 'integer'),
            // если RSS лента включает статью
            'article_rss' => array('type' => 'integer'),
            // количество визитов
            'article_visits' => array('type' => 'integer'),
            // статья опубликована
            'published' => array('type' => 'integer'),
            // если статья присутствует на карте сайта
            'article_sitemap' => array('type' => 'integer'),
            'article_map' => array('type' => 'integer'),
            'article_landing' => array('type' => 'integer'),
            // кэширование страницы сайта
            'article_caching' => array('type' => 'integer'),
            // категория доступа к статье
            'access_name' => array('type' => 'string'),
            // инд. доступа к статье
            'access_id' => array('type' => 'string'),
            // анализ статьи на наличие динамических компонентов
            'article_parsing' => array('type' => 'integer'),
            // шаблона статьи
            'template_icon' => array('type' => 'string'),
            'template_filename' => array('type' => 'string'),
            // индексация
            'page_meta_robots' => array('type' => 'string'),
            'page_meta_robots_s' => array('type' => 'string'),
            // вид статьи
            'type_id' => array('type' => 'integer'),
            // папка статьи
            'article_folder' => array('type' => 'string'),
            // ссылка на страницу сайта
            'article_link' => array('type' => 'string'),
            // теги
            'page_tags' => array('type' => 'string')
        );
    }

    /**
     * Возращает sql запрос для быстрого фильтра
     * (для подстановки "%fastfilter" в $sql)
     * 
     * @param string $key идендификатор поля
     * @param string $value значение
     * @return string
     */
    protected function getTreeFilter($key, $value)
    {
        // идендификатор поля
        switch ($key) {
            // по дате правки статьи
            case 'byDt':
                $toDate = date('Y-m-d');
                switch ($value) {
                    case 'day':
                        return ' AND (`a`.`sys_date_insert` BETWEEN "' . $toDate . '" AND "' . $toDate . '"'
                             . ' OR `a`.`sys_date_update` BETWEEN "' . $toDate . '" AND "' . $toDate . '")';

                    case 'week':
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') - 7, date('Y')));
                        return ' AND (`a`.`sys_date_insert` BETWEEN "' . $fromDate . '" AND "' . $toDate . '"'
                             . ' OR `a`.`sys_date_update` BETWEEN "' . $fromDate . '" AND "' . $toDate . '")';

                    case 'month':
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, date('m') - 1, date('d'), date('Y')));
                        return ' AND (`a`.`sys_date_insert` BETWEEN "' . $fromDate . '" AND "' . $toDate . '"'
                             . ' OR `a`.`sys_date_update` BETWEEN "' . $fromDate . '" AND "' . $toDate . '")';
                }
                break;

            // по дате публикации статьи
            case 'byPb':
                $toDate = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') + 1, date('Y')));
                switch ($value) {
                    case 'day':
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') - 1, date('Y')));
                        return ' AND (`a`.`published_date` BETWEEN "' . $fromDate . '" AND "' . $toDate . '")';

                    case 'week':
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') - 7, date('Y')));
                        return ' AND `a`.`published_date` BETWEEN "' . $fromDate . '" AND "' . $toDate . '"';

                    case 'month':
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, date('m') - 1, date('d'), date('Y')));
                        return ' AND `a`.`published_date` BETWEEN "' . $fromDate . '" AND "' . $toDate . '"';
                }
                break;

            // по автору статьи
            case 'byAu':
                return ' AND (`a`.`sys_user_update`=' . (int) $value . ' OR `a`.`sys_user_insert`=' . (int) $value . ')';

            // по категории
            case 'byCt':
                return ' AND `a`.`category_id`="' . (int) $value . '" ';

            // по расположению в архиве
            case 'byAr':
                return ' AND `article_archive`=' . (int) $value;

            // по расположению в rss ленте
            case 'byRs':
                return ' AND `article_rss`=' . (int) $value;

            // по отображению на Sitemap
            case 'bySm':
                return ' AND `article_map`=' . (int) $value;

            // по кэшированию
            case 'byCa':
                return ' AND `article_caching`=' . (int) $value;

            // по возможности поиска
            case 'bySe':
                return ' AND `article_search`=' . (int) $value;

            // по отображению загаловка
            case 'byHe':
                return ' AND `article_header`=' . (int) $value;

            // по правам доступа
            case 'byAc':
                return ' AND `access_id`=' . (int) $value;

            // по шаблону
            case 'byTm':
                return ' AND `a`.`template_id`=' . (int) $value;

            // по виду страниц
            case 'byTv':
                return ' AND `a`.`article_landing`=' . (int) $value;

            // по тегу
            case 'byTg':
                return ' AND MATCH (`p`.`page_tags`) AGAINST (\'*' . $value . '*\' in boolean mode) ';
        }

        return '';
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        // удаление данных всех статей
        GArticle::deleteAll();

        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        // удаление статей
        GArticle::delete($this->uri->id);
    }

    /**
     * Событие наступает после успешного удаления данных
     * 
     * @return void
     */
    protected function dataDeleteComplete()
    {
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        $domain = $_SERVER['SERVER_NAME'];
        // сайты на других доменах
        if (isset($this->domains[$record['domain_id']])) {
            $domain = $this->domains[$record['domain_id']][0];
            $record['domain_id'] = $this->domains[$record['domain_id']][1];
        } else
            $record['domain_id'] = '';

        // если есть обозначение
        if (!empty($record['template_icon']))
            $record['template_icon'] = '<img src="' . $this->path . '/../../../templates/Combo/resources/icons/' . $record['template_icon'] . '.png" />';
        // доступ к статье
        $record['access_name'] = '<img src="' . $this->path . '/../Combo/resources/icons/icon-access-' . $record['access_id'] . '.png">';
        // индексация страницы сайта
        if (!empty($record['page_meta_robots'])) {
            $index = $record['page_meta_robots'];
            if ($index == 'index, follow' || $index == 'index, nofollow' || $index == 'all')
                $icon = 'green';
            else
                $icon = 'red';
            $record['page_meta_robots_s'] = $record['page_meta_robots'];
            $record['page_meta_robots'] = '<img src="' . $this->resourcePath . 'icon-index-' . $icon . '.png" align="absmiddle"> ' . $index;
        }
        // если статья не опубликована
        if (empty($record['published']))
            $record['rowCls'] = 'mn-row-notpublished';
        $url = GArticle::getUrl($domain, $record['type_id'], $record['article_id'], $record['article_uri'], $record['category_uri'], $this->sef);
        $record['article_url'] = $url;
        if ($record['domain_id'])
            $record['article_link'] = '<a href="'. $url . '?preview" target="_blank" title="' . $this->_['tooltip_article_link'] . '"><img src="' . $this->resourcePath . 'icon-item-link.png"></a> ';
        else
            $record['article_link'] = '';

        return $record;
    }
}
?>