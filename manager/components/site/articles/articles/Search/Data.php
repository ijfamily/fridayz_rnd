<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск статьи сайта"
 * Пакет контроллеров "Статьи"
 * Группа пакетов     "Статьи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Distributed under the Lesser General Public License (LGPL)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    GController_SArticles_Search
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск статьи сайта
 * 
 * @category   Gear
 * @package    GController_SArticles_Search
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SArticles_Search_Data extends GController_Search_Data
{
    /**
     * дентификатор компонента (списка) к которому применяется поиск
     *
     * @var string
     */
    public $gridId = 'gcontroller_sarticles_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sarticles_grid';

    /**
     * Идентификатор компонента
     *
     * @var string
     */
    protected $_gridId = 'gcontroller_sarticles_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор
     * Используется для инициализации атрибутов контроллер не переданного в конструктор
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'article_index', 'label' => $this->_['header_article_index']),
            array('dataIndex' => 'article_date', 'label' => $this->_['header_article_date']),
            array('dataIndex' => 'article_note', 'label' => $this->_['header_article_note']),
            array('dataIndex' => 'page_header', 'label' => $this->_['header_page_header']),
            array('dataIndex' => 'category_name', 'label' => $this->_['header_category_name']),
            array('dataIndex' => 'article_uri', 'label' => $this->_['header_article_uri']),
            array('dataIndex' => 'article_published', 'label' => $this->_['header_article_published']),
            array('dataIndex' => 'article_archive', 'label' => $this->_['header_article_archive']),
            array('dataIndex' => 'article_rss', 'label' => $this->_['header_article_rss']),
            array('dataIndex' => 'article_sitemap', 'label' => $this->_['header_article_sitemap']),
            array('dataIndex' => 'article_caching', 'label' => $this->_['header_article_caching']),
            array('dataIndex' => 'access_name', 'label' => $this->_['header_access_name']),
            array('dataIndex' => 'article_parsing', 'label' => $this->_['header_article_parsing']),
            array('dataIndex' => 'page_meta_robots', 'label' => $this->_['header_page_meta_robots'])
        );
    }

    /**
     * Применение фильтра
     * 
     * @return void
     */
    protected function dataAccept()
    {
        if ($this->_sender == 'toolbar') {
            if ($this->input->get('action') == 'search') {
                $tlbFilter = '';
                // если попытка сбросить фильтр
                if (!$this->input->isEmptyPost(array('domain'))) {
                    $tlbFilter = array(
                        'domain' => $this->input->get('domain')
                    );
                }
                $this->store->set('tlbFilter', $tlbFilter);
                return;
            }
        }

        parent::dataAccept();
    }
}
?>