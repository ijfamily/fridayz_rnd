<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Search in the list "Articles"',
    // поля
    'header_article_index'   => '№',
    'header_article_note'    => 'Note',
    'header_particle_note'   => 'Note (p)',
    'header_page_header'     => 'Header',
    'header_category_name'   => 'Category',
    'header_article_uri'     => 'URI address',
    'header_access_name'     => 'Access',
    'header_article_archive' => 'Archive',
    'header_article_rss'     => 'RSS',
    'header_article_parsing' => 'Analysis',
    'header_article_sitemap' => 'Map',
    'header_article_caching' => 'Cache',
    'header_article_lang'    => 'Language',
    'header_article_nodes'   => 'Articles',
    'header_article_nodes'   => 'Number of children in Article',
    'header_article_tags_i'  => 'The number of images in the article',
    'header_article_tags_g'  => 'The number of galleries in the article',
    'header_article_tags_d'  => 'The number of documents in paper',
    'header_article_tags_v'  => 'The number of video files to the article',
    'header_article_tags_a'  => 'The number of audio files in an article',
    'header_article_tags_t'  => 'The number of tags in the article',
);
?>