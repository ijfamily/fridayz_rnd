<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Поиск в списке "Статьи"',
    // поля
    'header_article_index'   => 'Порядковый номер в списке',
    'header_article_date'    => 'Дата публикации',
    'header_article_note'    => 'Заметка',
    'header_page_header'     => 'Заголовок',
    'header_category_name'   => 'Категория',
    'header_article_uri'     => 'ЧПУ URL статьи',
    'header_article_published' => 'Публикация статьи',
    'header_article_archive'   => 'Статьи в архиве',
    'header_article_rss'      => 'Отображение в RSS ленте',
    'header_article_sitemap'  => 'Отображение на карте сайта',
    'header_article_caching'  => 'Кэширование страницы',
    'header_access_name'      => 'Правар доступа к статье',
    'header_article_parsing'  => 'Динамические компоненты в статье',
    'header_page_meta_robots' => 'Идексация'
);
?>