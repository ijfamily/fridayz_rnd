<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Пошук в списку "Статті"',
    // поля
    'header_article_index'   => '№',
    'header_article_note'    => 'Примітка',
    'header_particle_note'   => 'Примітка (п)',
    'header_page_header'     => 'Заголовок',
    'header_category_name'   => 'Категорія',
    'header_article_uri'     => 'URI ресурса',
    'header_access_name'     => 'Доступ',
    'header_article_archive' => 'Архів',
    'header_article_rss'     => 'RSS',
    'header_article_parsing' => 'Аналіз',
    'header_article_sitemap' => 'Карта',
    'header_article_caching' => 'Кеш',
    'header_article_lang'    => 'Мова',
    'header_article_nodes'   => 'Статей',
    'header_article_nodes'   => 'Кількість нащадків у статті',
    'header_article_tags_i'  => 'Кількість зображень в статті',
    'header_article_tags_g'  => 'Кількість галерей у статті',
    'header_article_tags_d'  => 'Кількість документів у статті',
    'header_article_tags_v'  => 'Кількість відеофайлів у статті',
    'header_article_tags_a'  => 'Кількість аудіофайлів у статті',
    'header_article_tags_t'  => 'Кількість тегів у статті',
);
?>