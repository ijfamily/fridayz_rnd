<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: index.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    'Correctly selected the type of request' => 'Неправильно выбран типа запроса!',
    'You did not fill the field' => 'Вы не заполнили поле!'
);
?>