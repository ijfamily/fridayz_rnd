<?php
/**
 * Gear Manager
 *
 * Контроллер         "Обработка URL строки"
 * Пакет контроллеров "Статьи"
 * Группа пакетов     "Статьи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SArticles_Request
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Data');

/**
 * Обработка URL строки
 * 
 * @category   Gear
 * @package    GController_SArticles_Request
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SArticles_Generator_Data extends GController_Data
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sarticles_grid';

    /**
     * Генерация URL
     * 
     * @return string
     */
    protected function generateUrl()
    {
        parent::dataAccessView();

        $url = $this->input->get('header', '');
        if (empty($url))
            throw new GException('Data processing error', $this->_['You did not fill the field']);

        Gear::library('String');
        $alias = $this->config->getFromCms('Site', 'LANGUAGE/ALIAS');

        $list = explode('.', $url);
        if (sizeof($list) == 2)
            $url = GString::toUrl($list[0], $alias) . '.' . $list[1];
        else
            $url = GString::toUrl($url, $alias) . '.html';

        $this->response->data = array('text' => $url);
    }

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        if ($this->uri->action == 'data')
            // метод запроса
            switch ($this->uri->method) {
                // метод "POST"
                case 'POST': $this->generateUrl(); return;
            }

        throw new GException(
            'Error processing controller',
            'Unable to initialize method "%s" controller',
            $this->uri->method,
            true,
            array('icon'      => 'icon-msg-error',
                  'buttons'   => array('ok' => 'OK', 'yes' => GText::_('Send error report', 'exception')),
                  'btnAction' => 'send report')
        );
    }
}
?>