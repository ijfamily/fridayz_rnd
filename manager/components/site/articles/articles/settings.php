<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Статьи"
 * Группа пакетов     "Статьи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SArticles
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 * 
 * Установка компонента
 * 
 * Название: Статьи
 * Описание: Статьи сайта
 * ID класса: gcontroller_sarticles_grid
 * Группа: Сайт / Статьи
 * Очищать: нет
 * Ресурс
 *    Пакет: site/articles/articles/
 *    Контроллер: site/articles/articles/Grid/
 *    Интерфейс: grid/interface/
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске: нет
 */

return array(
    // {S}- модуль "Site" {} - пакет контроллеров "Articles" -> SArticles
    'clsPrefix' => 'SArticles',
    // выбрать базу данных из настроек конфига
    'conName'   => 'site',
    // использорвать язык
    'language'  => 'ru-RU'
);
?>