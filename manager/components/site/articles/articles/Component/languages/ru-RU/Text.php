<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: index.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile'        => 'Свойства компонента "%s"',
    'title_profile_insert' => 'Добавление компонента "%s" в статью',
    'title_profile_update' => 'Изменение свойств компонента "%s"',
    // сообщения
    'title_adding_component'    => 'Компонент',
    'title_updating_property'   => 'Компонент',
    'msg_is_successful_append'  => 'Успешно выполнена вставка компонента в статью!',
    'msg_is_successful_updated' => 'Успешно выполнено изменение свойств компонента в статье!',
    'msg_empty_properties'      => 'Нет свойств у компонента',
    'msg_no_page'               => 'Нет страницы с указанным языком'
);
?>