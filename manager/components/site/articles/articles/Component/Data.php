<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные интерфейса свойств компонента"
 * Пакет контроллеров "Профиль свойств компонента"
 * Группа пакетов     "Статьи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SArticles_Component
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные интерфейса свойств компонента
 * 
 * @category   Gear
 * @package    GController_SArticles_Component
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SArticles_Component_Data extends GController_Profile_Data
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sarticles_grid';

    /**
     * Идент. компонента
     *
     * @var string
     */
    protected $_componentId = '';

    /**
     * Идент. языка
     *
     * @var string
     */
    protected $_languageId = 0;

    /**
     * Идент. текстового редактора
     *
     * @var string
     */
    protected $_editorId = '';

    /**
     * Идент. компонента
     *
     * @var array
     */
    protected $_component = '';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // идент. компонента
        $this->_componentId = $this->store->get('componentId');
        // идент. языка
        $this->_languageId = $this->store->get('language');
        // идент. текстового редактора
        $this->_editorId = $this->store->get('editor');
        // компонент
        $this->_component = $this->store->get('component');
    }

    /**
     * Возращает компоненты статьи
     * 
     * @param integer $articleId идент. статьи
     * @return array
     */
    protected function getComponentInterface()
    {
        return '&nbsp;&nbsp;<component id="' . $this->_component['id'] . '" class="' . $this->_component['cmp_class'] . '" path="' . $this->_component['cmp_path'] . '" editable="false">'
             . '<button type="button" title="Свойства компонента" target="control"></button><button type="button" title="Удалить компонент" target="close"></button>'
             . '<label>' . $this->_component['cmp_note'] . '</label></component>&nbsp;&nbsp;';
    }

    /**
     * Возращает текст для выпадающего списка
     * 
     * @param string $field название поля
     * @param mixed $value значение
     * @return string
     */
    protected function getComboText($field, $value, $alias, $query)
    {
        if ($field == 'data-id' && $alias == 'galleries') {
            $sql = "SELECT * FROM `site_gallery` WHERE `gallery_id`='$value' ORDER BY `gallery_name`";
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            else
                return array('id' => 'fldDataId', 'xtype' => 'mn-field-combo', 'value' => $value, 'text' => $rec['gallery_name']);
        } else
        if ($field == 'data-id' && $alias == 'slider') {
            $sql = "SELECT * FROM `site_sliders` WHERE `slider_id`='$value' ORDER BY `slider_name`";
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            else
                return array('id' => 'fldDataId', 'xtype' => 'mn-field-combo', 'value' => $value, 'text' => $rec['slider_name']);
        } else
        if ($field == 'template') {
            $sql = "SELECT * FROM `site_templates` WHERE `template_filename`='$value' ORDER BY `template_name`";
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            else
                return array('id' => 'fldTemplate', 'xtype' => 'mn-field-combo', 'value' => $value, 'text' => $rec['template_name']);
        }

        return false;
    }

    /**
     * Событие наступает после успешного добавления данных
     * 
     * @param array $data сформированные данные полученные после запроса к таблице
     * @return void
     */
    protected function dataInsertComplete($data = array())
    {
        $this->response->add('setTo', array(array('id' => $this->_editorId, 'func' => 'insertContent', 'args' => $this->getComponentInterface())));
    }

    /**
     * Вставка данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataInsert($params = array())
    {
        $this->dataAccessInsert();

        $this->response->setMsgResult($this->_['title_adding_component'], $this->_['msg_is_successful_append'], true);
        $props = $this->input->get('properties');
        if (empty($props))
            throw new GException('Warning', 'To execute a query, you must change data!');

        // компоненты для всех языков (если ранне были добавлены при вызове профиля статьи, ввиде array())
        $components = $this->store->get('components');
        // если не были созданы ранее 
        if (empty($components)) {
            $languages = $this->config->getFromCms('Site', 'LANGUAGES');
            foreach ($languages as $alias => $item) {
                $components[$item['id']] = array();
            }
        }
        // если все таки нет страницы с указанным языком
        if (!isset($components[$this->_languageId]))
            throw new GException('Warning', $this->_['msg_no_page']);
        // компоненты статьи
        $this->_component['id'] = sprintf($this->_component['cmp_dom_id'], uniqid());
        $components[$this->_languageId][$this->_component['id']] = $props;
        // обновить компоненты статьи
        $this->store->set('components', $components);

        $this->dataInsertComplete($params);
    }

    /**
     * Событие наступает после успешного обновления данных
     * 
     * @param array $data сформированные данные полученные после запроса к таблице
     * @return void
     */
    protected function dataUpdateComplete($data = array())
    {
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(
            array('id' => 'site', 'func' => 'reload'),
            // чтобы текст редактора мог сохраниться при измении только атрибутов компонентов в статье
            array('id' => $this->_editorId, 'func' => 'setDirty')
        ));
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        parent::dataAccessUpdate();

        $this->response->setMsgResult($this->_['title_updating_property'], $this->_['msg_is_successful_updated'], true);
        $props = $this->input->get('properties');
        if (empty($props))
            throw new GException('Warning', 'To execute a query, you must change data!');
        // компоненты для всех языков
        $languages = $this->store->get('components');
        // если нет страницы с указанным языком
        if (!isset($languages[$this->_languageId]))
            throw new GException('Warning', $this->_['msg_no_page']);
        $languages[$this->_languageId][$this->_componentId] = $props;
        // обновить компоненты статьи
        $this->store->set('components', $languages);

        $this->dataUpdateComplete($params);
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        parent::dataAccessView();

        // компоненты для всех языков
        $languages = $this->store->get('components');
        // если нет страницы с указанным языком
        if (!isset($languages[$this->_languageId]))
            throw new GException('Warning', $this->_['msg_no_page']);
        
        if (isset($languages[$this->_languageId][$this->_componentId]))
            $props = $languages[$this->_languageId][$this->_componentId];
        else
            $props = array();
        // свойства компонента
        $setTo = array();
        $alias = $this->_component['cmp_alias'];
        $query = new GDb_Query();
        foreach($props as $name => $value) {
            if ($value) {
                $to = $this->getComboText($name, $value, $alias, $query);
                $setTo[] = $to;
            }
            $this->_data['properties[' . $name . ']'] = $value;
        }
        // результат
        $this->response->data = $this->recordPreprocessing($this->_data);
        // загаловок
        $this->response->data['title'] = sprintf($this->_['title_profile_update'], $this->_component['cmp_name']);

        // установка полей
        if ($setTo)
            $this->response->add('setTo', $setTo);
    }

}
?>