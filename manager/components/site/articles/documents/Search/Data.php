<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск документов в статье сайта"
 * Пакет контроллеров "Документы статьи"
 * Группа пакетов     "Статьи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SArticleDocuments_Search
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск документов в статье сайта
 * 
 * @category   Gear
 * @package    GController_SArticleDocuments_Search
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SArticleDocuments_Search_Data extends GController_Search_Data
{
    /**
     * дентификатор компонента (списка) к которому применяется поиск
     *
     * @var string
     */
    public $gridId = 'gcontroller_sarticledocuments_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sarticles_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор
     * Используется для инициализации атрибутов контроллер не переданного в конструктор
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'document_index', 'label' => $this->_['header_document_index']),
            array('dataIndex' => 'document_title', 'label' => $this->_['header_document_title']),
            array('dataIndex' => 'document_visible', 'label' => $this->_['header_document_visible']),
            array('dataIndex' => 'document_filename', 'label' => $this->_['header_document_filename']),
            array('dataIndex' => 'document_type', 'label' => $this->_['header_document_type'])
        );
    }
}
?>