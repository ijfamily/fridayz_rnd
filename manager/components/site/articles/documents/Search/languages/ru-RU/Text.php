<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Поиск в списке "Документы"',
    // поля
    'header_document_index'     => '№',
    'header_document_filename'  => 'Файл документа',
    'header_document_type'      => 'Тип',
    'header_document_filesize'  => 'Размер',
    'header_document_title'     => 'Загаловок',
    'header_document_visible'   => 'Показывать'
);
?>