<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные для интерфейса профиля документа"
 * Пакет контроллеров "Документы статьи"
 * Группа пакетов     "Статьи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SArticleDocuments_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('File');
Gear::controller('Profile/Data');

/**
 * Данные для интерфейса профиля документа
 * 
 * @category   Gear
 * @package    GController_SArticleDocuments_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SArticleDocuments_Profile_Data extends GController_Profile_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array(
        'article_id', 'document_index', 'document_filename', 'document_filesize', 'document_type', 'document_original',
        'document_download', 'document_download_set', 'document_download_tpl', 'document_date',
        'document_visible'
    );

    /**
     * Первичный ключ таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'document_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_documents';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sarticles_grid';

    /**
     * Доступные расширения файлов для загрузки
     *
     * @var array
     */
    protected $_upload = array('doc', 'docx', 'xls', 'xlsx', 'pdf', 'djv', 'zip', 'rar', '7z');

    /**
     * Каталог данных сайта
     *
     * @var string
     */
    public $pathData = '../data/docs/';

    /**
     * Идентификатор статьи
     *
     * @var integer
     */
    protected $_articleId;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // идент. статьи
        $this->_articleId =$this->store->get('record', 0, 'gcontroller_sarticledocuments_grid');
    }

    /**
     * Вставка текста документа
     * 
     * @param  integer $documentId идент. изображения
     * @return void 
     */
    protected function textInsert($documentId)
    {
        $ln = $this->input->get('ln', false);
        if (empty($ln)) return;

        $table = new GDb_Table('site_documents_l', 'document_lid');
        // допустимые поля
        $fields = array('document_title');
        // список доступных языков
        $list = $this->config->getFromCms('Site', 'LANGUAGES');
        // вкладки с языками
        foreach ($list as $key => $item) {
        //for ($i = 0; $i < $count; $i++) {
            // если есть язык
            if (isset($ln[$item['id']])) {
                $item = $ln[$item['id']];
                $data = array('document_id' => $documentId, 'language_id' => $item['id']);
                for ($j = 0; $j < sizeof($fields); $j++) {
                    if (isset($item[$fields[$j]]))
                        $data[$fields[$j]] = $item[$fields[$j]];
                }
                if ($table->insert($data) === false)
                    throw new GSqlException();
            }
        }
    }

    /**
     * Обновление текста документа
     * 
     * @return void
     */
    protected function textUpdate()
    {
        $ln = $this->input->get('ln', false);
        if (empty($ln)) return;

        $table = new GDb_Table('site_documents_l', 'document_lid');
        // допустимые поля
        $fields = array('document_title');
        // список доступных языков
        $list = $this->config->getFromCms('Site', 'LANGUAGES');
        // вкладки с языками
        foreach ($list as $key => $item) {
        //for ($i = 0; $i < $count; $i++) {
            // если есть язык
            if (isset($ln[$item['id']])) {
                $item = $ln[$item['id']];
                // если есть только идент.
                if (sizeof($item) == 1) continue;
                $data = array();
                for ($j = 0; $j < sizeof($fields); $j++) {
                    if (isset($item[$fields[$j]]))
                        $data[$fields[$j]] = $item[$fields[$j]];
                }
                // обновить запись
                if ($table->update($data, $item['document_lid']) === false)
                    throw new GSqlException();
            }
        }
    }

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_profile_update'], $record['document_filename']);
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        // массив полей таблицы ($tableName)
        $this->fields = array(
            'document_index', 'document_date', 'document_visible', 'document_download',
            'document_download_set', 'document_download_tpl'
        );

        parent::dataUpdate($params);
    }

    /**
     * Событие наступает после успешного обновления данных
     * 
     * @param array $data сформированные данные полученные после запроса к таблице
     * @return void
     */
    protected function dataUpdateComplete($data = array())
    {
        // соединение с базой данных (т.к. для лога ранее возможно было другое соединение)
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();

        // обновление текста изображения
        $this->textUpdate($this->_recordId);
    }

    /**
     * Событие наступает после успешного добавления данных
     * 
     * @param array $data сформированные данные полученные после запроса к таблице
     * @return void
     */
    protected function dataInsertComplete($data = array())
    {
        // соединение с базой данных (т.к. для лога ранее возможно было другое соединение)
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();

        // вставка текста изображения
        $this->textInsert($this->_recordId);
        // обновление счётчика статьи
        $query = new GDb_Query();
        $sql = 'UPDATE `site_articles` SET `article_tags_d`='
             . '(SELECT COUNT(*) FROM `site_documents` '
             . 'WHERE `article_id`=' . $this->_articleId . ') WHERE `article_id`=' . $this->_articleId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Событие наступает после успешного удаления данных
     * 
     * @return void
     */
    protected function dataDeleteComplete()
    {
        // соединение с базой данных (т.к. для лога ранее возможно было другое соединение)
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();

        $query = new GDb_Query();
        // обновление счётчика статьи
        $sql = 'UPDATE `site_articles` SET `article_tags_d`='
             . '(SELECT COUNT(*) FROM `site_documents` '
             . 'WHERE `article_id`=' . $this->_articleId . ') WHERE `article_id`=' . $this->_articleId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        // документы статьи
        $query = new GDb_Query();
        $sql = 'SELECT * FROM `site_documents` WHERE `document_id` IN (' . $this->uri->id. ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        while (!$query->eof()) {
            $doc = $query->next();
            // если есть документ
            if ($doc['document_filename'])
                GFile::delete($this->pathData . $doc['document_filename'], false);
        }
        // удаление документов статьи
        $sql = 'DELETE FROM `site_documents_l` WHERE `document_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_documents_l') === false)
            throw new GSqlException();
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        // дата изменения статьи
        $params['document_date'] = date('Y-m-d H:i:s');
        // если состояние формы "правка"
        if ($this->isUpdate) return;
        // идент. статьи
        $params['article_id'] = $this->_articleId;
        // идент. название файла
        $fileId = uniqid();
        // если нет документа для загрузки
        if (($file = $this->input->file->get('doc')) === false)
            throw new GException('Adding data', 'File was not loaded');
        // определение имени файла
        $fileExt = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
        $filename = $fileId . '.' . $fileExt;
        // загрузка документа
        GFile::upload($file, $this->pathData . $filename, $this->_upload);
        // обновляем поля документа
        $params['document_filename'] = $filename;
        $params['document_original'] = $file['name'];
        $params['document_type'] = $fileExt;
        $params['document_filesize'] = $file['size'];
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        // ресурсы документа
        $url = 'http://' . $_SERVER['SERVER_NAME'] . '/data/docs/' . $record['document_filename'];
        $record['document_url'] = '<a href="' . $url . '">' . $url . '</a>';
        // размер документа
        if (!empty($record['document_filesize']))
            $record['document_filesize'] = GFile::fileSize($record['document_filesize']);
        // типа файла
        $record['document_type'] = GFile::getType($record['document_type']);

        return $record;
    }
}
?>