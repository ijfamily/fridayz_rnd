<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Создание записи "Документ"',
    'title_profile_update' => 'Изменение записи "%s"',
    // поля формы
    'label_document_index'    => 'Индекс',
    'tip_document_index'      => 
        'Порядковый номер (для галереи - упорядоченность списка; для статьи - формирования псевдонима; для каждой '
      . 'категории свой порядок)',
    'label_document_visible'  => 'Показывать',
    'tip_document_visible'    => 'Показывать документ',
    'label_document_archive'  => 'Архив',
    'tip_document_archive'    => 'Документ в архиве (не будет доступно)',
    'title_fieldset_download' => 'Счётчик загрузки документа',
    'label_download_set'      => 'Счётчик',
    'tip_download_set'        => 'Счетать загрузки документа',
    'label_download_tpl'      => 'Шаблон',
    'tip_download_tpl'        => 'Использовать шаблон для ссылки вида: "ссылка, 200 Кb, загрузок 7"',
    'label_document_title'    => 'Загаловок',
    'tip_document_title'      => 'Отображается в названии ссылки на документ в статье',
    'title_fieldset_doc'      => 'Файл (".DOC", ".DOCX", ".XLS", ".XLSX", ".PDF", ".DJV", ".ZIP", ".7z", ".RAR")',
    'text_empty'              => 'Выберите файл ...',
    'text_btn_upload'         => 'Выбрать',
    'title_fieldset_adoc'     => 'Атрибуты документа',
    'label_document_filename' => 'Файл',
    'label_document_oname'    => 'Файл (о)',
    'tip_document_oname'      => 'Оригинальное название файла',
    'label_document_download' => 'Загрузок',
    'tip_document_download'   => 'Количество загрузок файла пользователями',
    'label_document_uri'      => 'Русурс URI',
    'tip_document_uri'        => 
        'Это символьная строка, позволяющая идентифицировать какой-либо ресурс: документ, изображение, файл, службу, '
      . 'ящик электронной почты и т. д.',
    'label_document_url'      => 'Русурс URL',
    'tip_document_url'        => 
        'Eдинообразный локатор (определитель местонахождения) ресурса. Это стандартизированный способ записи адреса '
      . 'ресурса в сети Интернет.',
    'label_document_type'     => 'Тип',
    'tip_document_type'       => 'Тип файла (".DOC", ".DOCX", ".XLS", ".XLSX", ".PDF", ".DJV", ".ZIP", "*.7z", ".RAR")',
    'label_document_filesize' => 'Размер',
    'tip_document_filesize'   => 'Размер файла',
    'label_date_insert'       => 'Создан',
    'label_date_update'       => 'Изменён'
);
?>