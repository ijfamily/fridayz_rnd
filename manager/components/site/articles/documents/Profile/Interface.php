<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля документа"
 * Пакет контроллеров "Документы статьи"
 * Группа пакетов     "Статьи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SArticleDocuments_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля документа
 * 
 * @category   Gear
 * @package    GController_SArticleDocuments_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SArticleDocuments_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Возращает интерфейса полей с текстом
     * 
     * @return array
     */
    protected function getTextInterface()
    {
        // соединение с базой данных
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();

        $values = array();

        // если состоянеи формы "правка"
        if ($this->isUpdate) {
            $query = new GDb_Query();
            // обновление текста в картинках
            $sql = 'SELECT * FROM `site_documents_l` WHERE `document_id`=' . $this->uri->id;
            if ($query->execute($sql) === false)
                throw new GSqlException();
            while (!$query->eof()) {
                $item = $query->next();
                if (!isset($values[$item['language_id']]))
                    $values[$item['language_id']] = array();
                $values[$item['language_id']]['document_lid'] = $item['document_lid'];
                $values[$item['language_id']]['document_title'] = $item['document_title'];
            }
        }

        $did = $this->config->getFromCms('Site', 'LANGUAGE/ID');
        // список доступных языков
        $list = $this->config->getFromCms('Site', 'LANGUAGES');
        // вкладки с языками
        $tabs[] = array();
        foreach ($list as $key => $item) {
        //for ($i = 0; $i < $count; $i++) {
            $lid = $item['id'];
            $tabs[] = array(
                'title'      => $list[$i]['language_name'],
                'layout'     => 'form',
                'baseCls'    => 'mn-form-tab-body',
                'iconSrc'    => $this->resourcePath . ($did == $lid ? 'icon-tab-text-def.png' : 'icon-tab-text.png'),
                'labelWidth' => 78,
                'items'   => array(
                    array('xtype' => 'hidden',
                          'id'    => 'ln[' . $lid . '][document_lid]',
                          'name'  => 'ln[' . $lid . '][document_lid]',
                          'value' => isset($values[$lid]['document_lid']) ? $values[$lid]['document_lid'] : ''),
                    array('xtype'      => 'textfield',
                          'itemCls'    => 'mn-form-item-quiet',
                          'fieldLabel' => $this->_['label_document_title'],
                          'labelTip'   => $this->_['tip_document_title'],
                          'id'         => 'ln[' . $lid . '][document_title]',
                          'name'       => 'ln[' . $lid . '][document_title]',
                          'value'      => isset($values[$lid]['document_title']) ? $values[$lid]['document_title'] : '',
                          'maxLength'  => 255,
                          'anchor'     => '100%',
                          'allowBlank' => !($did == $lid)),
                )
            );
        }

        return array(
            'xtype'             => 'tabpanel',
            'tabPosition'       => 'bottom',
            'layoutOnTabChange' => true,
            'activeTab'         => 0,
            'anchor'            => '100%',
            'height'            => 70,
            'items'             => array($tabs)
        );
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'id'            => $this->classId,
                  'titleEllipsis' => 80,
                  'gridId'        => 'gcontroller_sarticledocuments_grid',
                  'width'         => 505,
                  'autoHeight'    => true,
                  'resizable'     => false,
                  'stateful'      => false)
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype'      => 'spinnerfield',
                  'itemCls'    => 'mn-form-item-quiet',
                  'fieldLabel' => $this->_['label_document_index'],
                  'labelTip'   => $this->_['tip_document_index'],
                  'name'       => 'document_index',
                  'resetable'  => false,
                  'width'      => 70,
                  'allowBlank' => true,
                  'emptyText'  => 1),
            array('xtype'      => 'mn-field-chbox',
                  'itemCls'    => 'mn-form-item-quiet',
                  'fieldLabel' => $this->_['label_document_visible'],
                  'labelTip'   => $this->_['tip_document_visible'],
                  'default'    => $this->isUpdate ? null : 1,
                  'name'       => 'document_visible'),
            array('xtype'      => 'fieldset',
                  'labelWidth' => 80,
                  'title'      => $this->_['title_fieldset_download'],
                  'autoHeight' => true,
                  'items'      => array(
                      array('xtype'      => 'mn-field-chbox',
                            'itemCls'    => 'mn-form-item-quiet',
                            'fieldLabel' => $this->_['label_download_set'],
                            'labelTip'   => $this->_['tip_download_set'],
                            'default'    => 0,
                            'name'       => 'document_download_set'),
                      array('xtype'      => 'mn-field-chbox',
                            'itemCls'    => 'mn-form-item-quiet',
                            'fieldLabel' => $this->_['label_download_tpl'],
                            'labelTip'   => $this->_['tip_download_tpl'],
                            'default'    => 0,
                            'name'       => 'document_download_tpl'),
                      array('xtype'      => 'spinnerfield',
                            'itemCls'    => 'mn-form-item-quiet',
                            'fieldLabel' => $this->_['label_document_download'],
                            'labelTip'   => $this->_['tip_document_download'],
                            'name'       => 'document_download',
                            'width'      => 70,
                            'allowBlank' => true,
                            'emptyText'  => 0)
                  )
            )
        );
        // если состояние формы "вставка"
        if ($this->isInsert) {
            $items[] = array(
                'xtype'      => 'fieldset',
                'labelWidth' => 70,
                'title'      => $this->_['title_fieldset_doc'],
                'autoHeight' => true,
                'items'      => array(
                    array('xtype'      => 'mn-field-upload',
                          'emptyText'  => $this->_['text_empty'],
                          'name'       => 'doc',
                          'buttonText' => $this->_['text_btn_upload'],
                          'anchor'     => '100%',
                          'buttonCfg'  => array('width' => 70)
                    )
                )
            );
        } else {
            $items[] = array(
                array('xtype'      => 'fieldset',
                      'labelWidth' => 80,
                      'title'      => $this->_['title_fieldset_adoc'],
                      'autoHeight' => true,
                      'items'      => array(
                          array('xtype'      => 'displayfield',
                                'fieldLabel' => $this->_['label_document_url'],
                                'labelTip'   => $this->_['tip_document_url'],
                                'id'         => 'fldDocumentUrl',
                                'name'       => 'document_url'),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_document_filename'],
                                'name'       => 'document_filename'),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_document_oname'],
                                'labelTip'   => $this->_['tip_document_oname'],
                                'name'       => 'document_original'),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_document_type'],
                                'labelTip'   => $this->_['tip_document_type'],
                                'name'       => 'document_type'),
                          array('xtype'      => 'displayfield',
                                'fieldClass' => 'mn-field-value-info',
                                'fieldLabel' => $this->_['label_document_filesize'],
                                'labelTip'   => $this->_['tip_document_filesize'],
                                'name'       => 'document_filesize')
                      )
                )
            );
        }
        $items[] = $this->getTextInterface();

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($items);
        $form->url = $this->componentUrl . 'profile/';
        $form->fileUpload = true;

        parent::getInterface();
    }
}
?>