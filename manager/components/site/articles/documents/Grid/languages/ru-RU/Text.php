<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'        => 'Документы статьи "%s"',
    'rowmenu_edit'      => 'Редактировать',
    'rowmenu_file'      => 'Переименовать',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">("Документы")</span> ?',
    // столбцы
    'header_document_index'     => '№',
    'tooltip_document_index'    => 'Порядковый номер документа',
    'header_document_alias'     => 'Псевдоним',
    'tooltip_document_alias'    => 'Псевдоним документа в статье',
    'header_document_filename'  => 'Файл документа',
    'tooltip_document_filename' => 'Файл документа',
    'header_document_original'  => 'Файл (з)',
    'tooltip_document_original' => 'Файл загруженого документа',
    'header_document_type'      => 'Тип',
    'tooltip_document_type'     => 'Тип файла',
    'header_document_download'  => 'Загрузок',
    'header_document_filesize'  => 'Размер',
    'tooltip_document_filesize' => 'Размер файла документа',
    'header_document_title'     => 'Загаловок',
    'tooltip_document_title'    => 'Текст для описания документа',
    'header_document_visible'   => 'Показывать',
    'tooltip_document_visible'  => 'Показывать документ',
    'header_document_archive'   => 'Архив',
    'tooltip_document_archive'  => 'Документ в архиве',
    'header_languages_name'     => 'Язык'
);
?>