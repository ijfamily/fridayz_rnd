<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'        => 'Article documents "%s"',
    'rowmenu_edit'      => 'Edit',
    'rowmenu_file'      => 'Rename',
    // панель управления
    'title_buttongroup_edit'   => 'Edit',
    'title_buttongroup_cols'   => 'Columns',
    'title_buttongroup_filter' => 'Filter',
    'msg_btn_clear'            => 'Do you really want to delete all entries <span class="mn-msg-delete">("Documents")</span> ?',
    // столбцы
    'header_document_index'     => '№',
    'tooltip_document_index'    => 'Index of the document',
    'header_document_alias'     => 'Alias',
    'tooltip_document_alias'    => 'Alias ​​document in article',
    'header_document_filename'  => 'File (d)',
    'tooltip_document_filename' => 'Dcouemnt file',
    'header_document_original'  => 'File (u)',
    'tooltip_document_original' => 'File uploaded document',
    'header_document_type'      => 'Type',
    'tooltip_document_type'     => 'Type file',
    'header_document_download'  => 'Download',
    'header_document_filesize'  => 'Size',
    'tooltip_document_filesize' => 'Document file size',
    'header_document_title'     => 'Title',
    'tooltip_document_title'    => 'Text to describe the document',
    'header_document_visible'   => 'Visible',
    'tooltip_document_visible'  => 'Show document',
    'header_document_archive'   => 'Archive',
    'tooltip_document_archive'  => 'Document in the archive',
    'header_languages_name'     => 'Language'
);
?>