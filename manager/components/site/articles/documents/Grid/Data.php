<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные интерфейса списка документов статьи"
 * Пакет контроллеров "Документы статьи"
 * Группа пакетов     "Статьи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SArticleDocuments_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('File');
Gear::controller('Grid/Data');

/**
 * Данные интерфейса списка изображений статьи
 * 
 * @category   Gear
 * @package    GController_SArticleDocuments_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SArticleDocuments_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'document_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_documents';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sarticles_grid';

    /**
     * Каталог данных сайта
     *
     * @var string
     */
    public $pathData = '../data/docs/';

    /**
     * Идентификатор статьи
     *
     * @var integer
     */
    protected $_articleId;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        $this->_articleId = $this->store->get('record', 0, 'gcontroller_sarticledocuments_grid');
        $languageId = $this->config->getFromCms('Site', 'LANGUAGE/ID');
        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS * FROM ('
          . 'SELECT `d`.*, `dl`.`document_title` '
          . 'FROM `site_documents` `d` '
            // текст документа
          . 'LEFT JOIN `site_documents_l` `dl` ON `dl`.`document_id`=`d`.`document_id` AND `dl`.`language_id`=' . $languageId . ' '
           .'WHERE `d`.`article_id`=' . $this->_recordId . ') `table` '
          . 'WHERE 1 %filter '
          . 'ORDER BY %sort '
          . 'LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // индекс изображения
            'document_index' => array('type' => 'string'),
            // псевдоним документ
            'document_alias' => array('type' => 'string'),
            // тип документ
            'document_type' => array('type' => 'string'),
            // файл документ
            'document_filename' => array('type' => 'string'),
            // размер файла документ
            'document_filesize' => array('type' => 'string'),
            // загаловок документа
            'document_title' => array('type' => 'string'),
            // показывать документ
            'document_visible' => array('type' => 'integer')
        );
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        $query = new GDb_Query();
        // удаление записей таблицы "site_documents_l" (текста документа)
        $sql = 'DELETE `dl` FROM `site_documents_l` `dl`, `site_documents` `d` '
             . 'WHERE `dl`.`document_id`=`d`.`document_id` AND `d`.`article_id`=' . $this->_articleId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_documents_l') === false)
            throw new GSqlException();
        // удаление файлов документов
        $sql = 'SELECT * FROM `site_documents` WHERE `article_id`=' . $this->_articleId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        while (!$query->eof()) {
            $doc = $query->next();
            // если есть документ
            if ($doc['document_filename'])
                GFile::delete($this->pathData . $doc['document_filename']);
        }
        // удаление записей таблицы "site_documents" (удаление документов)
        $sql = 'DELETE FROM `site_documents` WHERE `article_id`=' . $this->_articleId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_documents') === false)
            throw new GSqlException();
        // обновление счётчика статьи
        $sql = 'UPDATE `site_articles` SET `article_tags_d`=0 WHERE `article_id`=' . $this->_articleId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Событие наступает после успешного удаления данных
     * 
     * @return void
     */
    protected function dataDeleteComplete()
    {
        // соединение с базой данных (т.к. для лога ранее возможно было другое соединение)
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();

        $query = new GDb_Query();
        // обновление счётчика статьи
        $sql = 'UPDATE `site_articles` SET `article_tags_d`='
             . '(SELECT COUNT(*) FROM `site_documents` '
             . 'WHERE `article_id`=' . $this->_articleId . ') WHERE `article_id`=' . $this->_articleId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        // документы статьи
        $query = new GDb_Query();
        $sql = 'SELECT * FROM `site_documents` WHERE `document_id` IN (' . $this->uri->id. ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        while (!$query->eof()) {
            $image = $query->next();
            // если есть документ
            if ($image['document_filename'])
                GFile::delete($this->pathData . $image['document_filename']);
        }
        // удаление документов статьи
        $sql = 'DELETE FROM `site_documents_l` WHERE `document_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_documents_l') === false)
            throw new GSqlException();
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        // если файл документа не существует
        if (!file_exists($this->pathData . $record['document_filename']))
            $record['document_filename'] = '<span style="color:#eeacaf">' . $record['document_filename'] . '</span>';
        // если размер файла документа не определен
        if (!empty($record['document_filesize']))
            $record['document_filesize'] = GFile::fileSize($record['document_filesize']);
        // псевдоним документа
        if (empty($record['document_index']))
            $record['document_alias'] = '';
        else
            $record['document_alias'] = '{doc' . $record['document_index'] . '}';

        return $record;
    }
}
?>