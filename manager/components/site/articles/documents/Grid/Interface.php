<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка документов статьи"
 * Пакет контроллеров "Документы статьи"
 * Группа пакетов     "Статьи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SArticleDocuments_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс списка документов статьи
 * 
 * @category   Gear
 * @package    GController_SArticleDocuments_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SArticleDocuments_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'document_id';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'document_index';

    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        parent::getDataInterface();

        $languageId = $this->config->getFromCms('Site', 'LANGUAGE/ID');
        $query = new GDb_Query();
        // выбранная статья
        $sql = 'SELECT `article_note`, `page_header` '
             . 'FROM `site_articles` `a` LEFT JOIN `site_pages` `p` ON `a`.`article_id`=`p`.`article_id` AND `p`.`language_id`=' . $languageId
             . ' WHERE `a`.`article_id`=' . (int)$this->uri->id;
        if (($record = $query->getRecord($sql)) === false)
            throw new GSqlException();
        if (empty($record['page_header']))
            $title = $record['article_note'];
        else
            $title = $record['page_header'];
        $data['title'] = sprintf($this->_['title_grid'], $title);

        return $data;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // индекс изображения
            array('name' => 'document_index', 'type' => 'string'),
            // псевдоним документ
            array('name' => 'document_alias', 'type' => 'string'),
            // тип документ
            array('name' => 'document_type', 'type' => 'string'),
            // файл документ
            array('name' => 'document_filename', 'type' => 'string'),
            // размер файла документ
            array('name' => 'document_filesize', 'type' => 'string'),
            // загаловок документа
            array('name' => 'document_title', 'type' => 'string'),
            // показывать документ
            array('name' => 'document_visible', 'type' => 'integer')
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $settings = $this->session->get('user/settings');
        $this->columns = array(
            array('xtype'     => 'numbercolumn'),
            array('xtype'     => 'rowmenu'),
            array('dataIndex' => 'document_index',
                  'header'    => $this->_['header_document_index'],
                  'tooltip'   => $this->_['tooltip_document_index'],
                  'width'     => 45,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'document_alias',
                  'header'    => $this->_['header_document_alias'],
                  'width'     => 100,
                  'sortable'  => false,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'document_title',
                  'header'    => $this->_['header_document_title'],
                  'tooltip'   => $this->_['tooltip_document_title'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'document_filename',
                  'header'    => '<em class="mn-grid-hd-details"></em>' . $this->_['header_document_filename'],
                  'tooltip'   =>$this->_['tooltip_document_filename'],
                  'width'     => 135,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'document_type',
                  'header'    =>$this->_['header_document_type'],
                  'tooltip'   =>$this->_['tooltip_document_type'],
                  'width'     => 70,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'document_filesize',
                  'header'    =>$this->_['header_document_filesize'],
                  'tooltip'   =>$this->_['tooltip_document_filesize'],
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('xtype'     => 'booleancolumn',
                  'align'     => 'center',
                  'cls'       => 'mn-bg-color-gray2',
                  'dataIndex' => 'document_visible',
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-visible.png">',
                  'tooltip'   => $this->_['tooltip_document_visible'],
                  'width'     => 40,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean'))
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $data['title'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'profileUrl'    => $this->componentUrl . 'profile/')
        );

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "edit" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "добавить" (ExtJS class "Manager.button.InsertData")
        $group->items->add(array('xtype' => 'mn-btn-insert-data', 'gridId' => $this->classId));
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' => $this->classId));
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'      => 'mn-btn-clear-data',
                  'msgConfirm' => $this->_['msg_btn_clear'],
                  'gridId'     => $this->classId,
                  'isN'        => true)
        );
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = $this->classId;
        $this->_cmp->tbar->items->add($group);

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $cellInfo =
            '<div class="mn-grid-cell-tooltip-tl">{document_filename}</div>'
          . '<div class="mn-grid-cell-tooltip">'
          . '<em>' . $this->_['header_document_alias'] . '</em>: <b>{document_alias}</b><br>'
          . '<em>' . $this->_['header_document_title'] . '</em>: <b>{document_title}</b><br>'
          . '<em>' . $this->_['header_document_type'] . '</em>: <b>{document_type}</b><br>'
          . '<em>' . $this->_['header_document_filesize'] . '</em>: <b>{document_filesize}</b>'
          . '</div>';
        $this->_cmp->cellTips = array(
            array('field' => 'document_alias', 'tpl' => '{document_alias}'),
            array('field' => 'document_title', 'tpl' => '{document_title}'),
            array('field' => 'document_filename', 'tpl' => $cellInfo)
        );

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $items = array(
            array('text'    => $this->_['rowmenu_edit'],
                  'iconCls' => 'icon-form-edit',
                  'url'     => $this->componentUrl . 'profile/interface/'),
            array('xtype'   => 'menuseparator'),
            array('text'    => $this->_['rowmenu_file'],
                  'icon'    => $this->resourcePath . 'icon-rename.png',
                  'url'     => $this->componentUrl . 'rename/interface/'),
        );
        $this->_cmp->rowMenu = array('xtype' => 'mn-grid-rowmenu', 'items' => $items);

        parent::getInterface();
    }
}
?>