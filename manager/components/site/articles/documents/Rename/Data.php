<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные интерфейса изменения имени файла документа"
 * Пакет контроллеров "Документы статьи"
 * Группа пакетов     "Статьи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SArticleDocuments_Rename
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные интерфейса изменения имени файла документа
 * 
 * @category   Gear
 * @package    GController_SArticleDocuments_Rename
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SArticleDocuments_Rename_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array('document_filename');

    /**
     * Первичный ключ таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'document_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_documents';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sarticles_grid';

    /**
     * Путь к документам
     *
     * @var string
     */
    public $pathData = '../data/docs/';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['profile_title_update'], $record['document_filename']);
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        $this->dataAccessUpdate();

        if ($this->input->isEmpty('PUT'))
            throw new GException('Warning', 'To execute a query, you must change data!');
        // массив полей с их соответствующими значениями
        if (empty($params))
            $params = $this->input->getBy($this->fields);
        // проверки входных данных
        $this->isDataCorrect($params);
        // проверка существования записи с одинаковыми значениями
        $this->isDataExist($params);
        $params = array();
        // переименовывание файла
        if ($this->input->get('document_filename_new', false)) {
            if ($this->input->get('document_filename') != $this->input->put('document_filename_new')) {
                $from = $this->pathData . $this->input->get('document_filename');
                $to = $this->pathData . $this->input->get('document_filename_new');
                if (rename($from, $to) === false)
                    throw new GException('Error', 'Unable to move file!');
                $params['document_filename'] = $this->input->get('document_filename_new');
            }
        }
        // если нет данных для изменений
        if (empty($params))
            throw new GException('Warning', 'To execute a query, you must change data!');

        $table = new GDb_Table($this->tableName, $this->idProperty);
        if ($table->update($params, $this->uri->id) === false)
            throw new GSqlException();
        // запись в журнал действий пользователя
        $this->logUpdate(
            array('log_query'        => $table->query->getSQL(),
                  'log_error'        => $this->response->success === false ? $table->getError() : null,
                  'log_query_params' => $params)
        );
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        if ($this->input->get('document_filename', false) === false)
            throw new GException('Warning', 'To execute a query, you must change data!');
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        $record['document_filename_new'] = $record['document_filename'];

        return $record;
    }
}
?>