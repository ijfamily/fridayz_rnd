<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс изменения имени файла документа"
 * Пакет контроллеров "Документы статьи"
 * Группа пакетов     "Статьи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SArticleDocuments_Rename
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс изменения имени документа
 * 
 * @category   Gear
 * @package    GController_SArticleDocuments_Rename
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SArticleDocuments_Rename_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'           => '',
                  'titleEllipsis'   => 54,
                  'gridId'          => 'gcontroller_sarticledocuments_grid',
                  'width'           => 430,
                  'autoHeight'      => true,
                  'btnDeleteHidden' => true,
                  'stateful'        => false)
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype'      => 'fieldset',
                  'labelWidth' => 100,
                  'title'      => $this->_['title_document'],
                  'autoHeight' => true,
                  'items'      => array(
                      array('xtype'         => 'textfield',
                            'fieldLabel'    => $this->_['label_document_filename'],
                            'itemCls'       => 'mn-form-item-info',
                            'name'          => 'document_filename',
                            'checkDirty'    => false,
                            'maxLength'     => 255,
                            'anchor'        => '100%',
                            'readOnly'      => true,
                            'allowBlank'    => true),
                      array('xtype'         => 'textfield',
                            'fieldLabel'    => $this->_['label_document_filename_new'],
                            'name'          => 'document_filename_new',
                            'checkDirty'    => false,
                            'maxLength'     => 255,
                            'anchor'        => '100%',
                            'allowBlank'    => true)
                  )
            )
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($items);
        $form->labelWidth = 85;
        $form->url = $this->componentUrl . 'rename/';

        parent::getInterface();
    }
}
?>