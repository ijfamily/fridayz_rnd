<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные интерфейса списка комментариев статьи"
 * Пакет контроллеров "Комментарии статьи"
 * Группа пакетов     "Статьи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SArticleComments_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные интерфейса списка комментариев статьи
 * 
 * @category   Gear
 * @package    GController_SArticleComments_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SArticleComments_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'comment_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_comments';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sarticles_grid';

    /**
     * Идентификатор статьи
     *
     * @var integer
     */
    protected $_articleId;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        $this->_articleId = $this->store->get('record', 0, 'gcontroller_sarticlecomments_grid');
        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS * FROM `site_comments` '
           .'WHERE `article_id`=' . $this->_recordId . ' %filter '
          . 'ORDER BY %sort LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // дата
            'comment_date' => array('type' => 'string'),
            // автор
            'comment_author_name' => array('type' => 'string'),
            // e-mail
            'comment_author_email' => array('type' => 'string'),
            // ip адрес
            'comment_ip' => array('type' => 'string'),
            // текст
            'comment_text' => array('type' => 'string')
        );
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        $query = new GDb_Query();
        // удаление записей таблицы "site_comments" (комментарии)
        $sql = 'DELETE FROM `site_comments` WHERE `article_id`=' . $this->_articleId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_comments') === false)
            throw new GSqlException();
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        if (!empty($record['comment_date']))
            $record['comment_date'] = date('d-m-Y H:i:s', strtotime($record['comment_date']));
        if (mb_strlen($record['comment_text'], 'UTF-8') > 40)
            $record['comment_text'] = mb_substr($record['comment_text'], 0, 40, 'UTF-8') . ' ...';

        return $record;
    }
}
?>