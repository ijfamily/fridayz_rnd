<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка комментариев статьи"
 * Пакет контроллеров "Комментарии статьи"
 * Группа пакетов     "Статьи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SArticleComments_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс списка комментариев статьи
 * 
 * @category   Gear
 * @package    GController_SArticleComments_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SArticleComments_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'comment_id';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'comment_date';

    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        parent::getDataInterface();

        $languageId = $this->config->getFromCms('Site', 'LANGUAGE/ID');
        $query = new GDb_Query();
        // выбранная статья
        $sql = 'SELECT `article_note`, `page_header` '
             . 'FROM `site_articles` `a` LEFT JOIN `site_pages` `p` ON `a`.`article_id`=`p`.`article_id` AND `p`.`language_id`=' . $languageId
             . ' WHERE `a`.`article_id`=' . (int)$this->uri->id;
        if (($record = $query->getRecord($sql)) === false)
            throw new GSqlException();
        if (empty($record['page_header']))
            $title = $record['article_note'];
        else
            $title = $record['page_header'];
        $data['title'] = sprintf($this->_['title_grid'], $title);

        return $data;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // дата
            array('name' => 'comment_date', 'type' => 'string'),
            // автор
            array('name' => 'comment_author_name', 'type' => 'string'),
            // e-mail
            array('name' => 'comment_author_email', 'type' => 'string'),
            // ip адрес
            array('name' => 'comment_ip', 'type' => 'string'),
            // текст
            array('name' => 'comment_text', 'type' => 'string')
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $this->columns = array(
            array('xtype'     => 'numbercolumn'),
            array('xtype'     => 'rowmenu'),
            array('dataIndex' => 'comment_date',
                  'header'    => '<em class="mn-grid-hd-details"></em>' . $this->_['header_comment_date'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'comment_author_name',
                  'header'    => $this->_['header_comment_author_name'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'comment_author_email',
                  'header'    => $this->_['header_comment_author_email'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'comment_ip',
                  'header'    => $this->_['header_comment_ip'],
                  'width'     => 110,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'comment_text',
                  'header'    => $this->_['header_comment_text'],
                  'width'     => 200,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false))
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $data['title'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'profileUrl'    => $this->componentUrl . 'profile/')
        );

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "edit" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' => $this->classId));
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'      => 'mn-btn-clear-data',
                  'msgConfirm' => $this->_['msg_btn_clear'],
                  'gridId'     => $this->classId,
                  'isN'        => true)
        );
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = $this->classId;
        $this->_cmp->tbar->items->add($group);

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $cellInfo =
            '<div class="mn-grid-cell-tooltip-tl">{comment_date}</div>'
          . '<div class="mn-grid-cell-tooltip">'
          . '<em>' . $this->_['header_comment_author_name'] . '</em>: <b>{comment_author_name}</b><br>'
          . '<em>' . $this->_['header_comment_author_email'] . '</em>: <b>{comment_author_email}</b><br>'
          . '<em>' . $this->_['header_comment_ip'] . '</em>: <b>{comment_ip}</b>'
          . '</div>';
        $this->_cmp->cellTips = array(
            array('field' => 'comment_date', 'tpl' => $cellInfo),
            array('field' => 'comment_text', 'tpl' => '{comment_text}'),
            array('field' => 'comment_ip', 'tpl' => '{comment_ip}'),
            array('field' => 'comment_author_email', 'tpl' => '{comment_author_email}'),
            array('field' => 'comment_author_name', 'tpl' => '{comment_author_name}')
        );

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $items = array(
            array('text'    => $this->_['rowmenu_info'],
                  'icon'    => $this->resourcePath . 'icon-detail.png',
                  'url'     => $this->componentUrl . 'info/interface/')
        );
        $this->_cmp->rowMenu = array('xtype' => 'mn-grid-rowmenu', 'items' => $items);

        parent::getInterface();
    }
}
?>