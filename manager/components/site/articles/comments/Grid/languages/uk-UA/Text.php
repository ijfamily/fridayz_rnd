<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Distributed under the Lesser General Public License (LGPL)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2014 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Text.php 2014-06-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Коментарі статті "%s"',
    'rowmenu_info' => 'Деталі',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Стовпці',
    'title_buttongroup_filter' => 'Фільтр',
    'msg_btn_clear'            => 'Ви дійсно бажаєте видалити всі записи <span class="mn-msg-delete">("Коментарі")</span> ?',
    // столбцы
    'header_comment_date'         => 'Дата',
    'header_comment_author_name'  => 'Автор',
    'header_comment_author_email' => 'Email',
    'header_comment_ip'           => 'IP адреса',
    'header_comment_text'         => 'Текст'
);
?>