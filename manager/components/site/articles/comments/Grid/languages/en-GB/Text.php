<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Distributed under the Lesser General Public License (LGPL)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2014 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Text.php 2014-06-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Article comments "%s"',
    'rowmenu_info' => 'Details',
    // панель управления
    'title_buttongroup_edit'   => 'Edit',
    'title_buttongroup_cols'   => 'Columns',
    'title_buttongroup_filter' => 'Filter',
    'msg_btn_clear'            => 'Do you really want to delete all entries <span class="mn-msg-delete">("Comments")</span> ?',
    // столбцы
    'header_comment_date'         => 'Date',
    'header_comment_author_name'  => 'Author',
    'header_comment_author_email' => 'Email',
    'header_comment_ip'           => 'IP address',
    'header_comment_text'         => 'Text'
);
?>