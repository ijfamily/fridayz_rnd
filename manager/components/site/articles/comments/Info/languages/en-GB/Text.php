<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'info_title' => 'Details "%s"',
    // поля формы
    'label_comment_date'         => 'Date',
    'label_comment_author_name'  => 'Author',
    'label_comment_author_email' => 'Email',
    'label_comment_ip'           => 'IP address',
    'label_comment_text'         => 'Text'
);
?>