<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля комментария"
 * Пакет контроллеров "Комментарии статьи"
 * Группа пакетов     "Статьи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SArticleComments_Info
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля комментария
 * 
 * @category   Gear
 * @package    GController_SArticleComments_Info
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SArticleComments_Info_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('titleEllipsis' => 40,
                  'gridId'        => 'gcontroller_sarticlecomments_grid',
                  'width'         => 400,
                  'autoHeight'    => true,
                  'state'         => 'info',
                  'stateful'      => false)
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_comment_date'],
                  'name'       => 'comment_date',
                  'width'      => 140,
                  'allowBlank' => true),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_comment_author_name'],
                  'name'       => 'comment_author_name',
                  'width'      => 140,
                  'allowBlank' => true),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_comment_ip'],
                  'name'       => 'comment_ip',
                  'width'      => 140,
                  'allowBlank' => true),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_comment_author_email'],
                  'name'       => 'comment_author_email',
                  'width'      => 298,
                  'allowBlank' => true),
            array('xtype'      => 'textarea',
                  'hideLabel' => true,
                  'name'       => 'comment_text',
                  'width'      => 374,
                  'height'     => 125,
                  'allowBlank' => true)
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($items);
        $form->url = $this->componentUrl . 'info/';

        parent::getInterface();
    }
}
?>