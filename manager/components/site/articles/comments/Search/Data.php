<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск комментариев в статье сайта"
 * Пакет контроллеров "Комментарии статьи"
 * Группа пакетов     "Статьи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SArticleComments_Search
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск комментариев в статье сайта
 * 
 * @category   Gear
 * @package    GController_SArticleComments_Search
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SArticleComments_Search_Data extends GController_Search_Data
{
    /**
     * дентификатор компонента (списка) к которому применяется поиск
     *
     * @var string
     */
    public $gridId = 'gcontroller_sarticlecomments_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sarticles_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор
     * Используется для инициализации атрибутов контроллер не переданного в конструктор
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'comment_date', 'label' => $this->_['header_comment_date']),
            array('dataIndex' => 'comment_author_name', 'label' => $this->_['header_comment_author_name']),
            array('dataIndex' => 'comment_author_email', 'label' => $this->_['header_comment_author_email']),
            array('dataIndex' => 'comment_ip', 'label' => $this->_['header_comment_ip']),
            array('dataIndex' => 'comment_text', 'label' => $this->_['header_comment_text'])
        );
    }
}
?>