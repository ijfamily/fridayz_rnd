<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные для интерфейса профиля кэша"
 * Пакет контроллеров "Профиль кэша"
 * Группа пакетов     "Кэш"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SCache_Info
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные для интерфейса профиля кэша
 * 
 * @category   Gear
 * @package    GController_SCache_Info
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SCache_Info_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($_tableName)
     *
     * @var array
     */
    public $fields = array(
        'cache_generator_id', 'cache_generator', 'cache_agent', 'cache_date', 'cache_filename', 'cache_uri',
        'cache_ipaddress', 'cache_note'
    );

    /**
     * Первичный ключ таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'cache_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_cache';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_scache_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_info'], $record['cache_uri']);
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        $record['cache_date'] = date('d-m-Y H:i:s', strtotime($record['cache_date']));

        return $record;
    }
}
?>