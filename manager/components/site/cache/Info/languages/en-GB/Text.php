<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_info' => 'Details record "%s"',
    // поля формы
    'label_cache_date'         => 'Data',
    'label_cache_filename'     => 'File',
    'label_cache_uri'          => 'URI resource',
    'label_cache_note'         => 'Note',
    'label_cache_generator'    => 'Generator',
    'label_cache_generator_id' => 'ID generator',
    'label_cache_agent'        => 'Agent',
    'label_cache_ipaddress'    => 'IP address'
);
?>