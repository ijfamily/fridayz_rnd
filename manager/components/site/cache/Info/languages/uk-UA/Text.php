<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_info' => 'Деталі запису "%s"',
    // поля формы
    'label_cache_date'         => 'Дата',
    'label_cache_filename'     => 'Файл',
    'label_cache_uri'          => 'URI ресурс',
    'label_cache_note'         => 'Примітка',
    'label_cache_generator'    => 'Генератор',
    'label_cache_generator_id' => 'ID генератору',
    'label_cache_agent'        => 'Агент',
    'label_cache_ipaddress'    => 'IP адреса'
);
?>