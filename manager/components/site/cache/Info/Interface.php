<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля кэша"
 * Пакет контроллеров "Профиль кэша"
 * Группа пакетов     "Кэш"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SCache_Info
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля кэша
 * 
 * @category   Gear
 * @package    GController_SCache_Info
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SCache_Info_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('titleEllipsis' => 40,
                  'gridId'        => 'gcontroller_scache_grid',
                  'width'         => 388,
                  'autoHeight'    => true,
                  'state'         => 'info',
                  'stateful'      => false)
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_cache_date'],
                  'name'       => 'cache_date',
                  'width'      => 130,
                  'readOnly'   => true,
                  'allowBlank' => false),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_cache_filename'],
                  'name'       => 'cache_filename',
                  'anchor'     => '100%',
                  'readOnly'   => true,
                  'allowBlank' => false),
             array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_cache_uri'],
                  'name'       => 'cache_uri',
                  'anchor'     => '100%',
                  'readOnly'   => true,
                  'allowBlank' => false),
             array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_cache_note'],
                  'name'       => 'cache_note',
                  'anchor'     => '100%',
                  'readOnly'   => true,
                  'allowBlank' => false),
             array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_cache_generator'],
                  'name'       => 'cache_generator',
                  'width'      => 100,
                  'readOnly'   => true,
                  'allowBlank' => false),
             array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_cache_generator_id'],
                  'name'       => 'cache_generator_id',
                  'width'      => 100,
                  'readOnly'   => true,
                  'allowBlank' => false),
             array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_cache_agent'],
                  'name'       => 'cache_agent',
                  'anchor'     => '100%',
                  'readOnly'   => true,
                  'allowBlank' => false),
             array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_cache_ipaddress'],
                  'name'       => 'cache_ipaddress',
                  'width'      => 100,
                  'readOnly'   => true,
                  'allowBlank' => false)
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->labelWidth = 95;
        $form->items->add($items);
        $form->url = $this->componentUrl . 'info/';

        parent::getInterface();
    }
}
?>