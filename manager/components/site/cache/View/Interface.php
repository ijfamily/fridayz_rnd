<?php
/**
 * Gear Manager
 *
 * Контроллер         "Просмотр кэша статьи"
 * Пакет контроллеров "Кэш"
 * Группа пакетов     "Кэш"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SCache_View
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Interface');
Gear::ext('Container/Panel');

/**
 * Просмотр кэша сайта
 * 
 * @category   Gear
 * @package    GController_SCache_View
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SCache_View_Interface extends GController_Interface
{
    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // Ext_Panel (ExtJS class "Ext.Panel")
        $this->_cmp = new Ext_Panel(
            array('id'        => strtolower(__CLASS__),
                  'closable'  => true,
                  'component' => array('container' => 'content-tab', 'destroy' => true))
        );
    }

    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        parent::getDataInterface();

        // выбранная статья
        $query = new GDb_Query();
        $sql = 'SELECT * FROM `site_cache` WHERE `cache_id`=' . $this->uri->id;
        if (($data = $query->getRecord($sql)) === false)
            throw new GSqlException();

        return $data;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительные данные для создания интерфейса
        $data = $this->getDataInterface();

        // Ext_Panel (ExtJS class "Ext.Panel")
        $this->_cmp->iconSrc = $this->resourcePath . 'icon.png';
        $this->_cmp->iconTpl = $this->resourcePath . 'shortcut.png';
        $this->_cmp->title = sprintf($this->_['title'], $data['cache_uri']);
        $this->_cmp->titleTpl = $this->_['tooltip'];
        $this->_cmp->tabTip = $data['cache_uri'];
        $this->_cmp->items->add(array('xtype' => 'mn-iframe', 'url' => 'http://' . $_SERVER['HTTP_HOST'] . '/cache/' .$data['cache_filename']));
/*
        $html = $this->_component;
        $html->setProperty('iconSrc', $this->_request->controllerPath . '/resources/icon.png');
        $html->setProperty('title', sprintf($this->_['title'], $data['cache_uri']));
        $html->setProperty('tabTip', $data['cache_uri']);
        $html->items->add(array('xtype' => 'mn-iframe', 'url' =>  $url = 'http://' . $_SERVER['HTTP_HOST'] . '/cache/' .$data['cache_filename']));
*/
        parent::getInterface();
    }
}
?>