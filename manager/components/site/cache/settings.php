<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Кэш"
 * Группа пакетов     "Кэш"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SCache
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: setttings.php 2016-01-01 12:00:00 Gear Magic $
 *
* Установка компонента
 * 
 * Название: Кэш
 * Описание: Кэш
 * Меню:
 * ID класса: gcontroller_scache_grid
 * Модуль: Сайт
 * Группа: Сайт
 * Очищать: нет
 * Ресурс
 *    Компонент: site/cache/
 *    Контроллер: site/cache/Grid/
 *    Интерфейс: grid/interface/
 *    Меню:
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске компонентов: нет
 *    В главном меню: нет
 */

return array(
    // {S}- модуль "Site" {} - пакет контроллеров "Cache" -> SCache
    'clsPrefix' => 'SCache',
    // выбрать базу данных из настроек конфига
    'conName'   => 'site',
    // использовать язык
    'language'  => 'ru-RU'
);
?>