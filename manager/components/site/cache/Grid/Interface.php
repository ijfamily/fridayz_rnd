<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка кэшируемых страниц"
 * Пакет контроллеров "Список кэшируемых страниц"
 * Группа пакетов     "Кэш"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SCache_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс списка кэшируемых страниц
 * 
 * @category   Gear
 * @package    GController_SCache_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SCache_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'cache_id';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'cache_date';

    /**
     * Использовать быстрый фильтр
     *
     * @var boolean
     */
    public $useSlidePanel = true;

    /**
     * Возращает список доменов
     * 
     * @return array
     */
    protected function getDomains()
    {
        $arr = $this->config->getFromCms('Domains');
        $list = array();
        if ($arr) {
            $list[] = array('Все', -1);
            $list[] = array($this->config->getFromCms('Site', 'NAME', ''), 0);
            $domains = $arr['domains'];
            foreach ($arr['indexes'] as $key => $value) {
                $list[] = array($value[1], $key);
            } 
        }

        return $list;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // название сайта
            array('name' => 'cache_domain_id', 'type' => 'string'),
            // идент. генератора
            array('name' => 'cache_generator_id', 'type' => 'string'),
            // генератор кэша
            array('name' => 'cache_generator', 'type' => 'string'),
            // агент участвующий в кэшировании
            array('name' => 'cache_agent', 'type' => 'string'),
            // дата создания кэша
            array('name' => 'cache_date', 'type' => 'string'),
            // название файла кэша
            array('name' => 'cache_filename', 'type' => 'string'),
            array('name' => 'goto_url', 'type' => 'string'),
            // URI ресурс
            array('name' => 'cache_uri', 'type' => 'string'),
            // ip адрес с которого была выполнено кэширование
            array('name' => 'cache_ipaddress', 'type' => 'string'),
            // заметка о контенте кэша
            array('name' => 'cache_note', 'type' => 'string')
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $this->columns = array(
            array('xtype'     => 'expandercolumn',
                  'url'       => $this->componentUrl . 'grid/expand/',
                  'typeCt'    => 'row'),
            array('xtype'     => 'numbercolumn'),
            array('xtype'     => 'rowmenu'),
            array('dataIndex' => 'cache_domain_id',
                  'header'    => $this->_['header_site_name'],
                  'width'     => 90,
                  'hidden'    => !$this->config->getFromCms('Site', 'OUTCONTROL'),
                  'sortable'  => true),
            array('dataIndex' => 'cache_date',
                  'header'    => '<em class="mn-grid-hd-details"></em>' . $this->_['header_cache_date'],
                  'tooltip'   => $this->_['tooltip_cache_date'],
                  'width'     => 130,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'goto_url',
                  'header'    => '&nbsp;',
                  'fixed'     => true,
                  'hideable'  => false,
                  'width'     => 25,
                  'sortable'  => false,
                  'menuDisabled' => true),
            array('dataIndex' => 'cache_filename',
                  'header'    => $this->_['header_cache_filename'],
                  'tooltip'   => $this->_['tooltip_cache_filename'],
                  'width'     => 230,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'cache_uri',
                  'header'    => $this->_['header_cache_uri'],
                  'width'     => 200,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'cache_note',
                  'header'    => $this->_['header_cache_note'],
                  'width'     => 200,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'cache_generator',
                  'hidden'    => false,
                  'header'    => $this->_['header_cache_generator'],
                  'tooltip'   => $this->_['tooltip_cache_generator'],
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'cache_generator_id',
                  'hidden'    => false,
                  'header'    => $this->_['header_cache_generator_id'],
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'cache_agent',
                  'hidden'    => true,
                  'header'    => $this->_['header_cache_agent'],
                  'tooltip'   => $this->_['tooltip_cache_agent'],
                  'width'     => 200,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'cache_ipaddress',
                  'hidden'    => true,
                  'header'    => $this->_['header_cache_ipaddress'],
                  'width'     => 90,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string'))
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_grid'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_grid'],
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'profileUrl'    => $this->componentUrl . 'profile/')
        );

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "edit" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' => $this->classId));
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'      => 'mn-btn-clear-data',
                  'msgConfirm' => $this->_['msg_btn_clear'],
                  'gridId'     => $this->classId,
                  'isN'        => true)
        );
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = 'component-site-cache';
        $this->_cmp->tbar->items->add($group);

        // управление сайтами на других доменах
        if ($this->config->getFromCms('Site', 'OUTCONTROL')) {
            $domains = $this->getDomains();
            if ($domains) {
                // группа кнопок "фильтр" (ExtJS class "Ext.ButtonGroup")
                $group = new Ext_ButtonGroup_Filter(
                    array('title'  => $this->_['title_buttongroup_filter'],
                          'gridId' => $this->classId)
                );
                // кнопка "справка" (ExtJS class "Manager.button.Help")
                $btn = &$group->items->get(0);
                $btn['gridId'] = $this->classId;
                $btn['url'] = $this->componentUrl . 'search/data/';
                // кнопка "поиск" (ExtJS class "Manager.button.Search")
                $filter = $this->store->get('tlbFilter', array());
                $group->items->add(array(
                    'xtype'       => 'form',
                    'labelWidth'  => 35,
                    'bodyStyle'   => 'border:1px solid transparent;background-color:transparent',
                    'frame'       => false,
                    'bodyBorder ' => false,
                    'items'       => array(
                        array('xtype'         => 'combo',
                              'fieldLabel'    => $this->_['label_domain'],
                              'name'          => 'domain',
                              'checkDirty'    => false,
                              'editable'      => false,
                              'width'         => 140,
                              'typeAhead'     => false,
                              'triggerAction' => 'all',
                              'mode'          => 'local',
                              'store'         => array(
                                  'xtype'  => 'arraystore',
                                  'fields' => array('list', 'value'),
                                  'data'   => $domains
                              ),
                              'value'         => empty($filter) ? '' : $filter['domain'],
                              'hiddenName'    => 'domain',
                              'valueField'    => 'value',
                              'displayField'  => 'list',
                              'allowBlank'    => false)
                    )
                ));
                $this->_cmp->tbar->items->add($group);
            }
        }

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $this->_cmp->cellTips = array(
            array('field' => 'cache_date',
                  'tpl'   =>
                      '<div class="mn-grid-cell-tooltip-tl">{cache_date}</div>'
                    . $this->_['header_cache_filename'] . ': <b>{cache_filename}</b><br>'
                    . $this->_['header_cache_uri'] . ': <b>{cache_uri}</b><br>'
                    . $this->_['header_cache_note'] . ': <b>{cache_note}</b><br>'
                    . $this->_['header_cache_generator'] . ': <b>{cache_generator}</b><br>'
                    . $this->_['header_cache_generator_id'] . ': <b>{cache_generator_id}</b><br>'
                    . $this->_['header_cache_agent'] . ': <b>{cache_agent}</b><br>'
                    . $this->_['header_cache_ipaddress'] . ': <b>{cache_ipaddress}</b>'
            ),
            array('field' => 'cache_filename', 'tpl' => '{cache_filename}'),
            array('field' => 'cache_uri', 'tpl' => '{cache_uri}'),
            array('field' => 'cache_note', 'tpl' => '{cache_note}'),
            array('field' => 'cache_generator', 'tpl' => '{cache_generator}'),
            array('field' => 'cache_agent', 'tpl' => '{cache_agent}'),
            array('field' => 'cache_ipaddress', 'tpl' => '{cache_ipaddress}')
        );

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $items = array(
            array('text'    => $this->_['rowmenu_info'],
                  'icon'    => $this->resourcePath . 'icon-item-info.png',
                  'url'     => $this->componentUrl . 'info/interface/'),
            array('xtype'   => 'menuseparator'),
            array('text'    => $this->_['rowmenu_view'],
                  'iconCls' => 'icon-item-html',
                  'url'     => $this->componentUrl . 'view/interface/')
        );
        $this->_cmp->rowMenu = array('xtype' => 'mn-grid-rowmenu', 'items' => $items);

        // быстрый фильтр списка (ExtJs class "Manager.tree.GridFilter")
        $this->_slidePanel->cls = 'mn-tree-gridfilter';
        $this->_slidePanel->width = 250;
        $this->_slidePanel->initRoot = array(
            'text'     => 'Filter',
            'id'       => 'byRoot',
            'expanded' => true,
            'children' => array(
                array('text'     => $this->_['text_all_records'],
                      'value'    => 'all',
                      'expanded' => true,
                      'leaf'     => false,
                      'children' => array(
                        array('text'     => $this->_['text_by_date'],
                              'id'       => 'byDt',
                              'leaf'     => false,
                              'expanded' => true,
                              'children' => array(
                                  array('text'     => $this->_['text_by_day'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 'day'),
                                  array('text'     => $this->_['text_by_week'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true, 
                                        'value'    => 'week'),
                                  array('text'     => $this->_['text_by_month'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 'month')
                              )
                        ),
                        array('text' => $this->_['text_by_gen'],
                              'id'   => 'byGn',
                              'leaf' => false)
                    )
                )
            )
        );

        parent::getInterface();
    }
}
?>