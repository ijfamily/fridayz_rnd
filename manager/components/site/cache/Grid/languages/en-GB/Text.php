<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Cache',
    'rowmenu_edit' => 'Details',
    'rowmenu_view' => 'View',
    // панель управления
    'title_buttongroup_edit'   => 'Edit',
    'title_buttongroup_cols'   => 'Columns',
    'title_buttongroup_filter' => 'Filter',
    'msg_btn_clear'            => 'Do you really want to delete all records <span class="mn-msg-delete"> '
                                . '("Cache")</span> ?',
    // столбцы
    'header_cache_date'         => 'Data',
    'tooltip_cache_date'        => 'Date of created cache',
    'header_cache_filename'     => 'File',
    'tooltip_cache_filename'    => 'Cache file',
    'header_cache_uri'          => 'URI resource',
    'header_cache_note'         => 'Note',
    'header_cache_generator'    => 'Generator',
    'tooltip_cache_generator'   => 'Initiator of the cache',
    'header_cache_generator_id' => 'ID generator',
    'header_cache_agent'        => 'Agent',
    'tooltip_cache_agent'       => 'Agent initiator cache',
    'header_cache_ipaddress'    => 'IP address'
);
?>