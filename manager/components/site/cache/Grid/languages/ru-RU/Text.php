<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Кэш сайта',
    'rowmenu_info' => 'Информация о записи',
    'rowmenu_view' => 'Просмотр кэша',
    'tooltip_grid' => 'список файлов в кэше',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">("Кэш")</span> ?',
    'label_domain'             => 'Сайт',
    // столбцы
    'header_site_name'          => 'Сайт',
    'title_goto'                => 'Просмотр страницы кэша',
    'header_cache_date'         => 'Дата',
    'tooltip_cache_date'        => 'Дата создания кэша',
    'header_cache_filename'     => 'Файл',
    'tooltip_cache_filename'    => 'Файл кэша',
    'header_cache_uri'          => 'URI ресурс',
    'header_cache_note'         => 'Заметка',
    'header_cache_generator'    => 'Генератор',
    'tooltip_cache_generator'   => 'Инициатор создания кэша',
    'header_cache_generator_id' => 'ID генератора',
    'header_cache_agent'        => 'Агент',
    'tooltip_cache_agent'       => 'Агент инициатора кэша',
    'header_cache_ipaddress'    => 'IP адрес',
    // сообщения
    'msg_successfully_clear' => 'Успешно выполнена очистка всего кэша!',
    // развернутая запись
    'title_fieldset_common'    => 'О кэше страницы',
    'label_cache_date'         => 'Дата создания',
    'label_cache_filename'     => 'Файл',
    'label_cache_uri'          => 'URI ресурс',
    'label_cache_note'         => 'Заметка',
    'title_fieldset_generator' => 'Генератор кэша',
    'label_cache_generator'    => 'Название',
    'label_cache_generator_id' => 'ID генератора',
    'label_cache_agent'        => 'Агент',
    'label_cache_ipaddress'    => 'IP адрес',
    // быстрый фильтр
    'text_all_records' => 'Все записи',
    'text_by_date'     => 'По дате',
    'text_by_day'      => 'За день',
    'text_by_week'     => 'За неделю',
    'text_by_month'    => 'За месяц',
    'text_by_gen'      => 'По генератору',
);
?>