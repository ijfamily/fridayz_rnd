<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Кеш',
    'rowmenu_edit' => 'Редагувати',
    'rowmenu_view' => 'Перегляд кеша',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Стовпці',
    'title_buttongroup_filter' => 'Фільтр',
    'msg_btn_clear'            => 'Ви дійсно бажаєте видалити всі записи <span class="mn-msg-delete"> '
                                . '("Кеш")</span> ?',
    // столбцы
    'header_cache_date'         => 'Дата',
    'tooltip_cache_date'        => 'Дата створення кешу',
    'header_cache_filename'     => 'Файл',
    'tooltip_cache_filename'    => 'Файл кешу',
    'header_cache_uri'          => 'URI ресурс',
    'header_cache_note'         => 'Примітка',
    'header_cache_generator'    => 'Генератор',
    'tooltip_cache_generator'   => 'Ініціатор створення кешу',
    'header_cache_generator_id' => 'ID генератора',
    'header_cache_agent'        => 'Агент',
    'tooltip_cache_agent'       => 'Агент ініціатора кешу',
    'header_cache_ipaddress'    => 'IP адреса'
);
?>