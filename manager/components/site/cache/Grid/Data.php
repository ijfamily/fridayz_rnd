<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка списка кэшируемых страниц"
 * Пакет контроллеров "Список кэшируемых страниц"
 * Группа пакетов     "Кэш"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SCache_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('File');
Gear::controller('Grid/Data');

/**
 * Данные списка списка кэшируемых страниц
 * 
 * @category   Gear
 * @package    GController_SCache_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SCache_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'cache_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_cache';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'cache_date';

    /**
     * Каталог кэша
     *
     * @var string
     */
    public $pathCache = '';

    /**
     * Домены
     *
     * @var array
     */
    public $domains = array();

    /**
     * Управления сайтами на других доменах
     *
     * @var boolean
     */
    public $isOutControl = false;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        $this->pathCache = DOCUMENT_ROOT . $this->config->getFromCms('Site', 'CACHE/DIR');

        // управление сайтами на других доменах
        $this->isOutControl = $this->config->getFromCms('Site', 'OUTCONTROL');
        if ($this->isOutControl)
            $this->domains = $this->config->getFromCms('Domains', 'indexes');
        $this->domains[0] = array($_SERVER['SERVER_NAME'], $this->config->getFromCms('Site', 'NAME'));

        // SQL запрос
        $this->sql = 'SELECT SQL_CALC_FOUND_ROWS * FROM `site_cache` WHERE 1 ';
        // фильтр из панели инструментов
        if ($this->isOutControl) {
            $filter = $this->store->get('tlbFilter', false);
            if ($filter) {
                $domainId = (int) $filter['domain'];
                if ($domainId >= 0)
                    $this->sql .= ' AND `cache_domain_id`=' . $domainId;
            }
        }
        $this->sql .= '%filter %fastfilter ORDER BY %sort LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // название сайта
            'cache_domain_id' => array('type' => 'string'),
            // идент. генератора
            'cache_generator_id' => array('type' => 'string'),
            // генератор кэша
            'cache_generator' => array('type' => 'string'),
            // агент участвующий в кэшировании
            'cache_agent' => array('type' => 'string'),
            // дата создания кэша
            'cache_date' => array('type' => 'string'),
            // название файла кэша
            'cache_filename' => array('type' => 'string'),
            'goto_url' => array('type' => 'string'),
            // URI ресурс
            'cache_uri' => array('type' => 'string'),
            // ip адрес с которого была выполнено кэширование
            'cache_ipaddress' => array('type' => 'string'),
            // заметка о контенте кэша
            'cache_note' => array('type' => 'string')
        );
    }

    /**
     * Возращает sql запрос для быстрого фильтра
     * (для подстановки "%fastfilter" в $sql)
     * 
     * @param string $key идендификатор поля
     * @param string $value значение
     * @return string
     */
    protected function getTreeFilter($key, $value)
    {
        // идендификатор поля
        switch ($key) {
            // по дате
            case 'byDt':
                $toDate = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') + 1, date('Y')));
                switch ($value) {
                    case 'day':
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') - 1, date('Y')));
                        return ' AND `cache_date` BETWEEN "' . $fromDate . '" AND "' . $toDate . '"';

                    case 'week':
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') - 7, date('Y')));
                        return ' AND (`cache_date` BETWEEN "' . $fromDate . '" AND "' . $toDate . '")';

                    case 'month':
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, date('m') - 1, date('d'), date('Y')));
                        return ' AND `cache_date` BETWEEN "' . $fromDate . '" AND "' . $toDate . '"';
                }
                break;

            // по генератору
            case 'byGn':
                return ' AND `cache_generator`="' . $value . '" ';
        }

        return '';
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataAccessClear();

        $this->response->setMsgResult('Deleting data', $this->_['msg_successfully_clear'], true);
        // очистить каталога кэша
        GDir::clear($this->pathCache, array('index.html', 'config'));
        $table = new GDb_Table('site_cache', 'cache_id');
        if ($table->clear() === false)
            throw new GSqlException();
    }

    /**
     * Удаление данных
     * 
     * @return void
     */
    protected function dataDelete()
    {
        $field = $this->uri->getVar('field');
        if ($field == 'generator')
            $this->idProperty = 'cache_generator_id';

        parent::dataDelete();
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        $field = $this->uri->getVar('field');
        // кэш статьи
        $query = new GDb_Query();
        if ($field == 'generator')
            $sql = 'SELECT * FROM `site_cache` WHERE `cache_generator_id`=' . $this->uri->id;
        else
            $sql = 'SELECT * FROM `site_cache` WHERE `cache_id` IN (' . $this->uri->id. ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        while (!$query->eof()) {
            $cache = $query->next();
            // если есть кэш
            if ($cache['cache_filename'])
                GFile::delete($this->pathCache . $cache['cache_filename']);
        }
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        $domain = $_SERVER['SERVER_NAME'];
        // сайты на других доменах
        if (isset($this->domains[$record['cache_domain_id']])) {
            $domain = $this->domains[$record['cache_domain_id']][0];
            $record['cache_domain_id'] = $this->domains[$record['cache_domain_id']][1];
        } else
            $record['cache_domain_id'] = '';

        // если файл кэша не существует
        if (!empty($record['cache_filename']))
            if (!file_exists($this->pathCache . $record['cache_filename']))
                $record['cache_filename'] = '<span style="color:#eeacaf">' . $record['cache_filename'] . '</span>';
            else {
                if ($record['cache_domain_id'])
                    $record['goto_url'] = 
                        '<a target="blank" href="http://' . $domain . '/' . $this->config->getFromCms('Site', 'CACHE/DIR') . $record['cache_filename'] . '" align="absmidle">'
                      . '<img src="' . $this->resourcePath . 'icon-goto.png" title="' . $this->_['title_goto'] . '"></a>';
                else
                    $record['goto_url'] = '';
            }
        $record['cache_date'] = date('d-m-Y H:i:s', strtotime($record['cache_date']));

        return $record;
    }

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        // метод запроса
        switch ($this->uri->method) {
            // метод "GET"
            case 'GET':
                // тип действия
                if ($this->uri->action == 'data' && isset($_GET['clear'])) {
                    $this->dataClear();
                    return;
                }
                break;
        }

        parent::init();
    }
}
?>