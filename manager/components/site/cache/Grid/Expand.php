<?php
/**
 * Gear Manager
 *
 * Контроллер         "Развёрнутая запись кэшируемых страниц"
 * Пакет контроллеров "Список кэшируемых страниц"
 * Группа пакетов     "Кэш"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SCache_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Expand');

/**
 * Развёрнутая запись книги
 * 
 * @category   Gear
 * @package    GController_SCache_Grid
 * @subpackage Expand
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SCache_Grid_Expand extends GController_Grid_Expand
{
    /**
     * Вывод данных в интерфейс
     * 
     * @return void
     */
    protected function dataExpand()
    {
        parent::dataAccessView();

        $data = $this->getAttributes();
        $this->response->data = $data;
    }

    /**
     * Возращает атрибуты записи
     * 
     * @return string
     */
    protected function getAttributes()
    {
        $data = '';
        $query = new GDb_Query();
        $sql = 
            'SELECT * FROM `site_cache` WHERE `cache_id`=' . $this->uri->id;
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        // кэш
        $data = '<fieldset class="fixed" style="width:45%"><label>' . $this->_['title_fieldset_common'] . '</label><ul>';
        $data .= '<li><label>' . $this->_['label_cache_date'] . ':</label> ' . date('d-m-Y H:i:s', strtotime($rec['cache_date'])) . '</li>';
        $data .= '<li><label>' . $this->_['label_cache_filename'] . ':</label> <a target="blank" href="/cache/' . $rec['cache_filename'] . '">' . $rec['cache_filename'] . '</a></li>';
        $data .= '<li><label>' . $this->_['label_cache_uri'] . ':</label> <a target="blank" href="' . $rec['cache_uri'] . '">' . $rec['cache_uri'] . '</a></li>';
        $data .= '<li><label>' . $this->_['label_cache_note'] . ':</label> ' . $rec['cache_note'] . '</li>';
        $data .= '</ul></fieldset>';
        $data .= '<fieldset class="fixed" style="width:45%"><label>' . $this->_['title_fieldset_generator'] . '</label><ul>';
        $data .= '<li><label>' . $this->_['label_cache_generator'] . ':</label> ' . $rec['cache_generator'] . '</li>';
        $data .= '<li><label>' . $this->_['label_cache_generator_id'] . ':</label> ' . $rec['cache_generator_id'] . '</li>';
        $data .= '<li><label>' . $this->_['label_cache_agent'] . ':</label> ' . $rec['cache_agent'] . '</li>';
        $data .= '<li><label>' . $this->_['label_cache_ipaddress'] . ':</label> ' . $rec['cache_ipaddress'] . '</li>';
        $data .= '</ul></fieldset>';

        $data .= '<div class="wrap"></div>';
        $data = '<div class="mn-row-body border">' . $data . '<div class="wrap"></div></div>';

        return $data;
    }
}
?>