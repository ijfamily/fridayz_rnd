<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск кэша"
 * Пакет контроллеров "Кэш"
 * Группа пакетов     "Кэш"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SCache_Search
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск кэша
 * 
 * @category   Gear
 * @package    GController_SCache_Search
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SCache_Search_Data extends GController_Search_Data
{
    /**
     * дентификатор компонента (списка) к которому применяется поиск
     *
     * @var string
     */
    public $gridId = 'gcontroller_scache_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_scache_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор
     * Используется для инициализации атрибутов контроллер не переданного в конструктор
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'cache_date', 'label' => $this->_['header_cache_date']),
            array('dataIndex' => 'cache_filename', 'label' => $this->_['header_cache_filename']),
            array('dataIndex' => 'cache_uri', 'label' => $this->_['header_cache_uri']),
            array('dataIndex' => 'cache_note', 'label' => $this->_['header_cache_note']),
            array('dataIndex' => 'cache_generator', 'label' => $this->_['header_cache_generator']),
            array('dataIndex' => 'cache_generator_id', 'label' => $this->_['header_cache_generator_id']),
            array('dataIndex' => 'cache_agent', 'label' => $this->_['header_cache_agent']),
            array('dataIndex' => 'cache_ipaddress', 'label' => $this->_['header_cache_ipaddress'])
        );
    }

    /**
     * Применение фильтра
     * 
     * @return void
     */
    protected function dataAccept()
    {
        if ($this->_sender == 'toolbar') {
            if ($this->input->get('action') == 'search') {
                $tlbFilter = '';
                // если попытка сбросить фильтр
                if (!$this->input->isEmptyPost(array('domain'))) {
                    $tlbFilter = array(
                        'domain' => $this->input->get('domain')
                    );
                }
                $this->store->set('tlbFilter', $tlbFilter);
                return;
            }
        }

        parent::dataAccept();
    }
}
?>