<?php
/**
 * Gear Manager
 *
 * Контроллер         "Изменение профиля записи баннера"
 * Пакет контроллеров "Профиль баннера"
 * Группа пакетов     "Баннера"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SBanners_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Field.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Field');

/**
 * Изменение профиля записи баннера
 * 
 * @category   Gear
 * @package    GController_SBanners_Profile
 * @subpackage Field
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Field.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SBanners_Profile_Field extends GController_Profile_Field
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array('banner_visible');

    /**
     * Первичный ключ таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'banner_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_banners';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sbanners_grid';
}
?>