<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные для интерфейса профиля баннера"
 * Пакет контроллеров "Профиль баннера"
 * Группа пакетов     "Баннера"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SBanners_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные для интерфейса профиля баннера
 * 
 * @category   Gear
 * @package    GController_SBanners_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SBanners_Profile_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array(
        'banner_type', 'banner_selector', 'banner_name', 'banner_image', 'banner_url', 'banner_code', 'banner_visible',
        'banner_bg_color', 'banner_bg_repeat', 'banner_bg_fix', 'banner_bg_posx_type', 'banner_bg_posx', 'banner_bg_posy_type',
        'banner_bg_posy', 'banners_exceptions', 'banner_template', 'banner_template_data'
    );

    /**
     * Первичный ключ таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'banner_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_banners';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sbanners_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_profile_update'], $record['banner_name']);
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        $type = $this->input->get('banner_type');
        switch ($type) {
            // баннер по шаблону
            case 4:
                $data = $this->input->getBy(array('banner1_link', 'banner1_image', 'banner1_blank', 'banner2_link', 'banner2_image', 'banner2_blank'));
                if (!empty($data))
                    $params['banner_template_data'] = json_encode($data);
                break;
        }
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        // данные шаблона
        if (!empty($record['banner_template_data'])) {
            $data = json_decode($record['banner_template_data'], true);
            $data = array_merge($record, $data);
        } else
            $data = $record;

        $setTo = array();
        // шаблон баннера
        $query = new GDb_Query();
        if (!empty($record['banner_template'])) {
            $sql = 'SELECT * FROM `site_templates` WHERE `template_filename`=' . $query->escapeStr($record['banner_template']);
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            if (!empty($rec))
                $setTo[] = array('xtype' => 'mn-field-combo', 'id' => 'fldTemplate', 'text' => $rec['template_name'], 'value' => $record['banner_template']);
        }

        // установка полей
        $this->response->add('setTo', $setTo);

        return $data;
    }
}
?>