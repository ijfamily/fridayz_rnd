<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => array('Создание записи "фоновая реклама"', 'Создание записи "Баннер"', 'Создание записи "Код баннера"', 'Создание записи "Шаблон баннера"'),
    'title_profile_update' => 'Изменение записи "%s"',
    'text_btn_help'        => 'Справка',
    // поля формы
    'label_banner_name'     => 'Название<hb></hb>',
    'tip_banner_name'       => 'Название доступно только вам',
    'label_banner_selector' => 'ID элемента<hb></hb>',
    'tip_banner_selector'   => 'ID элемента, к которому применяется фон или вставляется код баннера',
    'label_banner_type'     => 'Тип',
    'label_banner_url'      => 'Ссылка<hb></hb>',
    'tip_banner_url'        => 'Ссылка баннера (атрибут href)',
    'label_banner_visible'  => 'Видимый',
    'label_banner_image'    => 'Изображение<hb></hb>',
    'tip_banner_image'      => 'Изображение баннера',
    'label_banner_visible'  => 'Видимый',
    'title_fieldset_code'   => 'Код баннера',
    'label_banner_bg_color' => 'Цвет фона<hb></hb>',
    'title_fieldset_bg'     => 'Изображение фона',
    'tip_banner_bg_color'   => 'укажите 16-ричный код, пример #FFFFFF',
    'label_banner_bg_fix'   => 'Зафиксировать<hb></hb>',
    'tip_banner_bg_fix'     => 'Зафиксировать изображение фона',
    'title_fieldset_x'      => 'Смещение изображения фона по оси X',
    'title_fieldset_y'      => 'Смещение изображения фона по оси Y',
    'label_offset'          => 'Вид смещения',
    'label_offset_value'    => 'Смещение (%, пкс.)',
    'label_bg_repeat'       => 'Повтор',
    'title_fieldset_except' => 'Исключения при которых баннер не показывается',
    'title_fieldset_banner' => 'Баннер',
    'label_banner_link'     => 'Ресурс<hb></hb>',
    'tip_banner_link'       => 'Переход при клике на баннер',
    'label_banner_image'    => 'Изображение',
    'label_template_name'   => 'Шаблон<hb></hb>',
    'tip_template_name'     => 'Шаблон баннера',
    'label_banner_blank'    => 'Переход<hb></hb>',
    'tip_banner_blank'      => 'Переход на новую страницу',
    // тип
    'data_repeat' => array(array('no-repeat', 'Не повторять'), array('repeat-x', 'Повторять по оси X'), array('repeat-y', 'Повторять по оси Y'), array('repeat', 'Повторять по оси XY')),
    'data_offset_x' => array(array('value', 'По значению'), array('center', 'По центу'), array('left', 'Прижать слева'), array('right', 'Прижать справа')),
    'data_offset_y' => array(array('value', 'По значению'), array('center', 'По центу'), array('top', 'Прижать сверху'), array('bottom', 'Прижать снизу'))
);
?>