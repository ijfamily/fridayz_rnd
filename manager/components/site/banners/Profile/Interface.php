<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля баннера"
 * Пакет контроллеров "Профиль баннера"
 * Группа пакетов     "Баннера"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SBanners_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля баннера
 * 
 * @category   Gear
 * @package    GController_SBanners_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SBanners_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса для шаблона баннера
     * 
     * @return void
     */
    protected function getBannerInterface4()
    {
        return array(
            array('xtype' => 'hidden',
                  'name'  => 'banner_type',
                  'value' => 4),
            array('xtype'      => 'mn-field-combo',
                  'id'         => 'fldTemplate',
                  'fieldLabel' => $this->_['label_template_name'],
                  'labelTip'   => $this->_['tip_template_name'],
                  'name'       => 'banner_template',
                  'editable'   => false,
                  'width'      => 200,
                  'hiddenName' => 'banner_template',
                  'allowBlank' => false,
                  'store'      => array(
                      'xtype' => 'jsonstore',
                      'url'   => $this->componentUrl . 'combo/trigger/?name=templates'
                  )
            ),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_banner_name'],
                  'labelTip'   => $this->_['tip_banner_name'],
                  'name'       => 'banner_name',
                  'maxLength'  => 255,
                  'anchor'     => '100%',
                  'allowBlank' => false),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_banner_selector'],
                  'labelTip'   => $this->_['tip_banner_selector'],
                  'name'       => 'banner_selector',
                  'maxLength'  => 255,
                  'width'      => 300,
                  'allowBlank' => false),
            array('xtype'      => 'mn-field-chbox',
                  'fieldLabel' => $this->_['label_banner_visible'],
                  'name'       => 'banner_visible'),
            array('xtype'      => 'fieldset',
                  'anchor'     => '100%',
                  'title'      => $this->_['title_fieldset_banner'] . ' №1',
                  'labelWidth' => 90,
                  'autoHeight' => true,
                  'items'      => array(
                      array('xtype'      => 'textfield',
                            'fieldLabel' => $this->_['label_banner_link'],
                            'labelTip'   => $this->_['tip_banner_link'],
                            'name'       => 'banner1_link',
                            'checkDirty' => false,
                            'anchor'     => '100%',
                            'allowBlank' => false),
                      array('xtype'           => 'mn-field-browse',
                            'params'          => 'view=banners',
                            'triggerWdgSetTo' => true,
                            'triggerWdgUrl'   => 'site/media/' . ROUTER_DELIMITER .'dialog/interface/',
                            'fieldLabel' => $this->_['label_banner_image'],
                            'name'       => 'banner1_image',
                            'checkDirty' => false,
                            'anchor'     => '100%',
                            'allowBlank' => false),
                      array('xtype'      => 'mn-field-chbox',
                            'fieldLabel' => $this->_['label_banner_blank'],
                            'labelTip'   => $this->_['tip_banner_blank'],
                            'name'       => 'banner1_blank'),
                  )
            ),
            array('xtype'      => 'fieldset',
                  'anchor'     => '100%',
                  'title'      => $this->_['title_fieldset_banner'] . ' №2',
                  'labelWidth' => 90,
                  'autoHeight' => true,
                  'items'      => array(
                      array('xtype'      => 'textfield',
                            'fieldLabel' => $this->_['label_banner_link'],
                            'labelTip'   => $this->_['tip_banner_link'],
                            'name'       => 'banner2_link',
                            'checkDirty' => false,
                            'anchor'     => '100%',
                            'allowBlank' => true),
                      array('xtype'           => 'mn-field-browse',
                            'params'          => 'view=banners',
                            'triggerWdgSetTo' => true,
                            'triggerWdgUrl'   => 'site/media/' . ROUTER_DELIMITER .'dialog/interface/',
                            'fieldLabel' => $this->_['label_banner_image'],
                            'name'       => 'banner2_image',
                            'checkDirty' => false,
                            'anchor'     => '100%',
                            'allowBlank' => true),
                      array('xtype'      => 'mn-field-chbox',
                            'fieldLabel' => $this->_['label_banner_blank'],
                            'labelTip'   => $this->_['tip_banner_blank'],
                            'name'       => 'banner2_blank'),
                  )
            ),
        );
    }

    /**
     * Инициализация интерфейса для кода баннера
     * 
     * @return void
     */
    protected function getBannerInterface3()
    {
        return array(
            array('xtype' => 'hidden',
                  'name'  => 'banner_type',
                  'value' => 3),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_banner_name'],
                  'labelTip'   => $this->_['tip_banner_name'],
                  'name'       => 'banner_name',
                  'maxLength'  => 255,
                  'anchor'     => '100%',
                  'allowBlank' => false),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_banner_selector'],
                  'labelTip'   => $this->_['tip_banner_selector'],
                  'name'       => 'banner_selector',
                  'maxLength'  => 255,
                  'width'      => 300,
                  'allowBlank' => false),
            array('xtype'      => 'mn-field-chbox',
                  'fieldLabel' => $this->_['label_banner_visible'],
                  'name'       => 'banner_visible'),
            array('xtype'      => 'fieldset',
                  'anchor'     => '100%',
                  'title'      => $this->_['title_fieldset_code'],
                  'autoHeight' => true,
                  'items'      => array(
                      array('xtype'      => 'textarea',
                            'hideLabel'  => true,
                            'name'       => 'banner_code',
                            'anchor'     => '100%',
                            'height'     => 200,
                            'allowBlank' => true)
                  )
            ),
            array('xtype'      => 'fieldset',
                  'anchor'     => '100%',
                  'title'      => $this->_['title_fieldset_except'],
                  'autoHeight' => true,
                  'items'      => array(
                      array('xtype'      => 'textarea',
                            'hideLabel'  => true,
                            'name'       => 'banners_exceptions',
                            'anchor'     => '100%',
                            'height'     => 70,
                            'allowBlank' => true)
                  )
            )
        );
    }

    /**
     * Инициализация интерфейса для баннера
     * 
     * @return void
     */
    protected function getBannerInterface2()
    {
        return array(
            array('xtype' => 'hidden',
                  'name'  => 'banner_type',
                  'value' => 2),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_banner_name'],
                  'labelTip'   => $this->_['tip_banner_name'],
                  'name'       => 'banner_name',
                  'maxLength'  => 255,
                  'anchor'     => '100%',
                  'allowBlank' => false),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_banner_selector'],
                  'labelTip'   => $this->_['tip_banner_selector'],
                  'name'       => 'banner_selector',
                  'maxLength'  => 255,
                  'width'      => 300,
                  'allowBlank' => false),
            array('xtype'           => 'mn-field-browse',
                  'triggerWdgSetTo' => true,
                  'triggerWdgUrl'   => 'site/media/' . ROUTER_DELIMITER .'dialog/interface/',
                  'fieldLabel' => $this->_['label_banner_image'],
                  'labelTip'   => $this->_['tip_banner_image'],
                  'id'         => 'banner_image',
                  'name'       => 'banner_image',
                  'anchor'     => '100%',
                  'allowBlank' => false),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_banner_url'],
                  'labelTip'   => $this->_['tip_banner_url'],
                  'name'       => 'banner_url',
                  'maxLength'  => 255,
                  'anchor'     => '100%',
                  'allowBlank' => true),
            array('xtype'      => 'mn-field-chbox',
                  'fieldLabel' => $this->_['label_banner_visible'],
                  'value'      => 1,
                  'name'       => 'banner_visible'),
            array('xtype'      => 'fieldset',
                  'anchor'     => '100%',
                  'title'      => $this->_['title_fieldset_except'],
                  'autoHeight' => true,
                  'items'      => array(
                      array('xtype'      => 'textarea',
                            'hideLabel'  => true,
                            'name'       => 'banners_exceptions',
                            'anchor'     => '100%',
                            'height'     => 70,
                            'allowBlank' => true)
                  )
            )
        );
    }

    /**
     * Инициализация интерфейса для фоновой рекламы
     * 
     * @return void
     */
    protected function getBannerInterface1()
    {
        return array(
            array('xtype' => 'hidden',
                  'name'  => 'banner_type',
                  'value' => 1),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_banner_name'],
                  'labelTip'   => $this->_['tip_banner_name'],
                  'name'       => 'banner_name',
                  'maxLength'  => 255,
                  'anchor'     => '100%',
                  'allowBlank' => false),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_banner_selector'],
                  'labelTip'   => $this->_['tip_banner_selector'],
                  'name'       => 'banner_selector',
                  'maxLength'  => 255,
                  'width'      => 300,
                  'allowBlank' => false),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_banner_url'],
                  'labelTip'   => $this->_['tip_banner_url'],
                  'name'       => 'banner_url',
                  'maxLength'  => 255,
                  'anchor'     => '100%',
                  'allowBlank' => true),
            array('xtype'      => 'mn-field-chbox',
                  'fieldLabel' => $this->_['label_banner_visible'],
                  'value'      => 1,
                  'name'       => 'banner_visible'),
            array('xtype'      => 'fieldset',
                  'anchor'     => '100%',
                  'title'      => $this->_['title_fieldset_bg'],
                  'labelWidth' => 100,
                  'autoHeight' => true,
                  'items'      => array(
                        array('xtype'           => 'mn-field-browse',
                              'triggerWdgSetTo' => true,
                              'triggerWdgUrl'   => 'site/media/' . ROUTER_DELIMITER .'dialog/interface/',
                              'fieldLabel' => $this->_['label_banner_image'],
                              'labelTip'   => $this->_['tip_banner_image'],
                              'id'         => 'banner_image',
                              'name'       => 'banner_image',
                              'anchor'     => '100%',
                              'allowBlank' => false),
                        array('xtype'      => 'textfield',
                              'fieldLabel' => $this->_['label_banner_bg_color'],
                              'labelTip'   => $this->_['tip_banner_bg_color'],
                              'name'       => 'banner_bg_color',
                              'maxLength'  => 7,
                              'width'      => 150,
                              'allowBlank' => true),
                        array('xtype'         => 'combo',
                              'fieldLabel'    => $this->_['label_bg_repeat'],
                              'name'          => 'banner_bg_repeat',
                              'value'         => 'no-repeat',
                              'editable'      => false,
                              'width'         => 150,
                              'typeAhead'     => false,
                              'triggerAction' => 'all',
                              'mode'          => 'local',
                              'store'         => array(
                                  'xtype'  => 'arraystore',
                                  'fields' => array('value', 'display'),
                                  'data'   => $this->_['data_repeat']
                              ),
                              'hiddenName'    => 'banner_bg_repeat',
                              'valueField'    => 'value',
                              'displayField'  => 'display',
                              'allowBlank'    => true),
                        array('xtype'      => 'mn-field-chbox',
                              'fieldLabel' => $this->_['label_banner_bg_fix'],
                              'labelTip'   => $this->_['tip_banner_bg_fix'],
                              'value'      => 1,
                              'name'       => 'banner_bg_fix')
                  )
            ),
            array('xtype'      => 'fieldset',
                  'anchor'     => '100%',
                  'title'      => $this->_['title_fieldset_x'],
                  'labelWidth' => 140,
                  'autoHeight' => true,
                  'items'      => array(
                      array('xtype'         => 'combo',
                            'fieldLabel'    => $this->_['label_offset'],
                            'name'          => 'banner_bg_posx_type',
                            'value'         => 'center',
                            'editable'      => false,
                            'width'         => 150,
                            'typeAhead'     => false,
                            'triggerAction' => 'all',
                            'mode'          => 'local',
                            'store'         => array(
                                'xtype'  => 'arraystore',
                                'fields' => array('value', 'display'),
                                'data'   => $this->_['data_offset_x']
                            ),
                            'hiddenName'    => 'banner_bg_posx_type',
                            'valueField'    => 'value',
                            'displayField'  => 'display',
                            'allowBlank'    => true),
                        array('xtype'      => 'textfield',
                              'fieldLabel' => $this->_['label_offset_value'],
                              'name'       => 'banner_bg_posx',
                              'maxLength'  => 15,
                              'width'      => 150,
                              'allowBlank' => true),
                  )
            ),
            array('xtype'      => 'fieldset',
                  'anchor'     => '100%',
                  'title'      => $this->_['title_fieldset_y'],
                  'labelWidth' => 140,
                  'autoHeight' => true,
                  'items'      => array(
                      array('xtype'         => 'combo',
                            'fieldLabel'    => $this->_['label_offset'],
                            'name'          => 'banner_bg_posy_type',
                            'value'         => 'top',
                            'editable'      => false,
                            'width'         => 150,
                            'typeAhead'     => false,
                            'triggerAction' => 'all',
                            'mode'          => 'local',
                            'store'         => array(
                                'xtype'  => 'arraystore',
                                'fields' => array('value', 'display'),
                                'data'   => $this->_['data_offset_y']
                            ),
                            'hiddenName'    => 'banner_bg_posy_type',
                            'valueField'    => 'value',
                            'displayField'  => 'display',
                            'allowBlank'    => true),
                        array('xtype'      => 'textfield',
                              'fieldLabel' => $this->_['label_offset_value'],
                              'name'       => 'banner_bg_posy',
                              'maxLength'  => 15,
                              'width'      => 150,
                              'allowBlank' => true),
                  )
            ),
            array('xtype'      => 'fieldset',
                  'anchor'     => '100%',
                  'title'      => $this->_['title_fieldset_except'],
                  'autoHeight' => true,
                  'items'      => array(
                      array('xtype'      => 'textarea',
                            'hideLabel'  => true,
                            'name'       => 'banners_exceptions',
                            'anchor'     => '100%',
                            'height'     => 70,
                            'allowBlank' => true)
                  )
            )
        );
    }

    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        $data = array('type' => 0);

        // если состояние формы "вставка"
        if ($this->isInsert) {
            $data['type'] = (int) $this->uri->getVar('type');
            if (empty($data['type']))
                $data['type'] = 1;

            return $data;
        }

        parent::getDataInterface();

        $query = new GDb_Query();
        // выбранный баннер
        $sql = 'SELECT * FROM `site_banners` WHERE `banner_id`=' . (int)$this->uri->id;
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        $data['type'] = (int) $rec['banner_type'];
        if (!$data['type'])
            $data['type'] = 1;

        return $data;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'][$data['type'] - 1],
                  'id'            => $this->classId,
                  'titleEllipsis' => 80,
                  'gridId'        => 'gcontroller_sbanners_grid',
                  'width'         => 600,
                  'autoHeight'    => true,
                  'stateful'      => false,
                  'buttonsAdd'    => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-site-banners',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        switch ($data['type']) {
            // фоновая реклама
            case 1:
                $items = $this->getBannerInterface1();
                $form->labelWidth = 110;
                break;

            // баннер
            case 2:
                $items = $this->getBannerInterface2();
                $form->labelWidth = 95;
                break;

            // код баннера
            case 3:
                $items = $this->getBannerInterface3();
                $form->labelWidth = 85;
                break;

            // шаблон баннера
            case 4:
                $items = $this->getBannerInterface4();
                $form->labelWidth = 85;
                break;

            default:
                $items = array();
        }
        $form->items->add($items);
        $form->url = $this->componentUrl . 'profile/';
        $form->labelWidth = 100;


        parent::getInterface();
    }
}
?>