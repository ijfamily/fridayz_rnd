<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Баннера"
 * Группа пакетов     "Баннера"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SBanners
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 * 
 * Установка компонента
 * 
 * Название: Баннера
 * Описание: Баннера на страницах сайта
 * Меню: Беннер
 * ID класса: gcontroller_sbanners_grid
 * Модуль: Сайт
 * Группа: Сайт
 * Очищать: нет
 * Статистика компонента: нет
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске компонентов: нет
 *    В главном меню: нет
 * Ресурс
 *    Компонент: site/banners/
 *    Контроллер: site/banners/Grid/
 *    Интерфейс: grid/interface/
 *    Меню:
 */

return array(
    // {S}- модуль "Site" {Banners} - пакет контроллеров "Site banners" -> SBanners
    'clsPrefix' => 'SBanners',
    // использовать язык
    'language'  => 'ru-RU'
);
?>