<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка баннеров"
 * Пакет контроллеров "Баннера"
 * Группа пакетов     "Сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SBanners_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные списка баннеров
 * 
 * @category   Gear
 * @package    GController_SBanners_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SBanners_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'banner_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_banners';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 'SELECT SQL_CALC_FOUND_ROWS * FROM `site_banners` WHERE 1 %filter ORDER BY %sort LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // название
            'banner_name' => array('type' => 'string'),
            // селектор
            'banner_selector' => array('type' => 'string'),
            // тип
            'banner_type' => array('type' => 'string'),
            // переход при клике
            'banner_url' => array('type' => 'string'),
            // видимый
            'banner_visible' => array('type' => 'integer')
        );
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        Gear::library('File');

        parent::dataClear();

        GDir::clear(DOCUMENT_ROOT .  $this->config->getFromCms('Site', 'DIR/BANNERS') . '/', array('index.html'));

         $query = new GDb_Query();
        // удаление записей таблицы "site_banners"
        if ($query->clear('site_banners') === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_banners') === false)
            throw new GSqlException();
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        if (isset($this->_['data_type'][$record['banner_type'] - 1]))
            $record['banner_type'] = $this->_['data_type'][$record['banner_type'] - 1];

        return $record;
    }
}
?>