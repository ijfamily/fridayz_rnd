<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Баннера',
    'rowmenu_edit' => 'Редактировать',
    'tooltip_grid' => 'баннера на страницах сайта',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">("Баннера")</span> ?',
    'text_btn_add'             => 'Добавить',
    'tooltip_btn_add'          => 'Вставка новой записи',
    'text_item_banner1'        => 'Фоновая реклама',
    'text_item_banner2'        => 'Баннер',
    'text_item_banner3'        => 'Код баннера',
    'text_item_banner4'        => 'Шаблон баннера',
    // столбцы
    'header_banner_name'     => 'Название',
    'header_banner_selector' => 'ID элемента',
    'header_banner_type'     => 'Тип',
    'header_banner_visible'  => 'Видимый',
    // тип
    'data_type' => array('фоновая реклама', 'баннер', 'код баннера', 'шаблон баннера')
);
?>