<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка баннеров"
 * Пакет контроллеров "Баннера"
 * Группа пакетов     "Сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SBanners_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс списка баннеров
 * 
 * @category   Gear
 * @package    GController_SBanners_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SBanners_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'banner_id';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'banner_name';

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // название
            array('name' => 'banner_name', 'type' => 'string'),
            // селектор
            array('name' => 'banner_selector', 'type' => 'string'),
            // тип
            array('name' => 'banner_type', 'type' => 'string'),
            // видимый
            array('name' => 'banner_visible', 'type' => 'integer')
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $settings = $this->session->get('user/settings');
        $this->columns = array(
            array('xtype'     => 'numbercolumn'),
            array('xtype'     => 'rowmenu'),
            array('dataIndex' => 'banner_name',
                  'header'    => '<em class="mn-grid-hd-details"></em>'
                               . $this->_['header_banner_name'],
                  'width'     => 250,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'banner_selector',
                  'header'    => $this->_['header_banner_selector'],
                  'width'     => 150,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'banner_type',
                  'header'    => $this->_['header_banner_type'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('xtype'     => 'booleancolumn',
                  'align'     => 'center',
                  'cls'       => 'mn-bg-color-gray2',
                  'dataIndex' => 'banner_visible',
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-visible.png" align="absmiddle">',
                  'width'     => 40,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean'))
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_grid'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_grid'],
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'profileUrl'    => $this->componentUrl . 'profile/')
        );

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "edit" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "добавить" (ExtJS class "Manager.button.InsertData")
        $group->items->add(
            array('xtype'   => 'mn-btn-widgetsplit',
                  'text'    => $this->_['text_btn_add'],
                  'tooltip' => $this->_['tooltip_btn_add'],
                  'iconCls' => 'icon-btn-insert',
                  'itemId'  => 'item-banner1',
                  'menu'    => array(
                      array('iconCls' => 'icon-form-add',
                            'id'      => 'item-banner1',
                            'text'    => $this->_['text_item_banner1'],
                            'url'     => $this->componentUrl . 'profile/interface/?type=1'),
                      array('iconCls' => 'icon-form-add',
                            'id'      => 'item-banner2',
                            'text'    => $this->_['text_item_banner2'],
                            'url'     => $this->componentUrl . 'profile/interface/?type=2'),
                      array('iconCls' => 'icon-form-add',
                            'id'      => 'item-banner3',
                            'text'    => $this->_['text_item_banner3'],
                            'url'     => $this->componentUrl . 'profile/interface/?type=3'),
                      array('iconCls' => 'icon-form-add',
                            'id'      => 'item-banner4',
                            'text'    => $this->_['text_item_banner4'],
                            'url'     => $this->componentUrl . 'profile/interface/?type=4'),
                  )
            )
        );

        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' => $this->classId));
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'      => 'mn-btn-clear-data',
                  'msgConfirm' => $this->_['msg_btn_clear'],
                  'gridId'     => $this->classId,
                  'isN'        => true)
        );
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = 'component-site-banners';
        $this->_cmp->tbar->items->add($group);

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $cellInfo =
            '<div class="mn-grid-cell-tooltip-tl">{banner_name}</div>'
          . '<div class="mn-grid-cell-tooltip">'
          . '<em>' . $this->_['header_banner_selector'] . '</em>: <b>{banner_selector}</b><br>'
          . '<em>' . $this->_['header_banner_type'] . '</em>: <b>{banner_type}</b><br>'
          . '</div>';
        $this->_cmp->cellTips = array(
            array('field' => 'banner_name', 'tpl' => $cellInfo),
            array('field' => 'banner_selector', 'tpl' => '{banner_selector}'),
            array('field' => 'banner_url', 'tpl' => '{banner_url}')
        );

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $items = array(
            array('text'    => $this->_['rowmenu_edit'],
                  'iconCls' => 'icon-form-edit',
                  'url'     => $this->componentUrl . 'profile/interface/')
        );
        $this->_cmp->rowMenu = array('xtype' => 'mn-grid-rowmenu', 'items' => $items);

        parent::getInterface();
    }
}
?>