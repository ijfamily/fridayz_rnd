<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Блоки лендинг страниц"
 * Группа пакетов     "Справочники"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SLandingBlocks
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Блоки лендинг страниц
 * Описание: Справочник блоков лендинг страниц
 * Меню:
 * ID класса: gcontroller_slandingblocks_grid
 * Модуль: Сайт
 * Группа: Сайт / Справочники
 * Очищать: нет
 * Ресурс
 *    Компонент: site/references/lblocks/
 *    Контроллер: site/references/lblocks/Grid/
 *    Интерфейс: grid/interface/
 *    Меню:
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске компонентов: нет
 *    В главном меню: нет
 */

return array(
    // {S}- модуль "Site" {R} - пакет контроллеров "Landing blocks" -> SLandingBlocks
    'clsPrefix' => 'SLandingBlocks',
    // использовать язык
    'language'  => 'ru-RU'
);
?>