<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка блоков"
 * Пакет контроллеров "Список блоков лендинг страниц"
 * Группа пакетов     "Справочники"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SLandingBlocks_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные списка блоков
 * 
 * @category   Gear
 * @package    GController_SLandingBlocks_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SLandingBlocks_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'block_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_landing_blocks';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'block_name';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 'SELECT SQL_CALC_FOUND_ROWS * FROM `site_landing_blocks` WHERE 1 %filter ORDER BY %sort LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // название
            'block_name' => array('type' => 'string'),
            // описание
            'block_description' => array('type' => 'string'),
            // изображение
            'block_image' => array('type' => 'string'),
        );
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        $query = new GDb_Query();
        // удаление записей таблицы "site_landing_blocks" (блоки лендинга)
        if ($query->clear('site_landing_blocks') === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_landing_blocks') === false)
            throw new GSqlException();
        // удаление записей таблицы "site_landing_pages" (страницы лендинга)
        if ($query->clear('site_landing_pages') === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_landing_pages') === false)
            throw new GSqlException();
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        $query = new GDb_Query();
        // удаление блоков из страницы лендинга
        $sql = 'DELETE FROM `site_landing_pages` WHERE `block_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }
}
?>