<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля блока"
 * Пакет контроллеров "Профиль блока"
 * Группа пакетов     "Справочники"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SLandingBlocks_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля блока
 * 
 * @category   Gear
 * @package    GController_SLandingBlocks_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SLandingBlocks_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'titleEllipsis' => 70,
                  'gridId'        => 'gcontroller_slandingblocks_grid',
                  'width'         => 500,
                  'autoHeight'    => true,
                  'stateful'      => false)
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_block_name'],
                  'name'       => 'block_name',
                  'anchor'     => '100%',
                  'maxLength'  => 255,
                  'allowBlank' => false),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_block_description'],
                  'name'       => 'block_description',
                  'anchor'     => '100%',
                  'allowBlank' => true),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_block_image'],
                  'name'       => 'block_image',
                  'anchor'     => '100%',
                  'maxLength'  => 255,
                  'allowBlank' => true),
            array('xtype'      => 'fieldset',
                  'labelWidth' => 90,
                  'title'      => $this->_['title_fieldset_tag'],
                  'autoHeight' => true,
                  'items'      => array(
                      array('xtype'      => 'textfield',
                            'fieldLabel' => $this->_['label_block_tag'],
                            'name'       => 'block_tag',
                            'width'      => 200,
                            'maxLength'  => 255,
                            'allowBlank' => true),
                      array('xtype'      => 'textfield',
                            'fieldLabel' => $this->_['label_block_tag_css'],
                            'name'       => 'block_tag_css',
                            'width'      => 200,
                            'maxLength'  => 255,
                            'allowBlank' => true)
                  )
            )
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($items);
        $form->url = $this->componentUrl . 'profile/';
        $form->labelWidth = 100;

        parent::getInterface();
    }
}
?>