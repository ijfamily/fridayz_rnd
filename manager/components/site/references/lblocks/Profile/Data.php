<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля блока"
 * Пакет контроллеров "Профиль блока"
 * Группа пакетов     "Справочники"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SLandingBlocks_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные профиля блока
 * 
 * @category   Gear
 * @package    GController_SLandingBlocks_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SLandingBlocks_Profile_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($_tableName)
     *
     * @var array
     */
    public $fields = array('block_name', 'block_description', 'block_image');

    /**
     * Первичный ключ таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'block_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_landing_blocks';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_slandingblocks_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_profile_update'], $record['block_name']);
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        $query = new GDb_Query();
        // удаление блоков из страницы лендинга
        $sql = 'DELETE FROM `site_landing_pages` WHERE `block_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }
}
?>