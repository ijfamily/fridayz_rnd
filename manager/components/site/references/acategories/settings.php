<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Категории статей"
 * Группа пакетов     "Справочники"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SArticleCategories
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Категории статей
 * Описание: Справочники категорий статей
 * Меню:
 * ID класса: gcontroller_srarticlecategories_grid
 * Модуль: Сайт
 * Группа: Сайт / Справочники
 * Очищать: нет
 * Ресурс
 *    Компонент: site/references/acategories/
 *    Контроллер: site/references/acategories/Grid/
 *    Интерфейс: grid/interface/
 *    Меню:
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске компонентов: нет
 *    В главном меню: нет
 */

return array(
    // {S}- модуль "Site" {R} - пакет контроллеров "Articles categories" -> SRArticleCategories
    'clsPrefix' => 'SRArticleCategories',
    // выбрать базу данных из настроек конфига
    'conName'   => 'site',
    // использовать язык
    'language'  => 'ru-RU'
);
?>