<?php
/**
 * Gear Manager
 *
 * Контроллер         "Триггер выпадающего дерева"
 * Пакет контроллеров "Группы компонентов"
 * Группа пакетов     "Справочники"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SRArticleCategories_Combo
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Nodes.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Combo/Nodes');

/**
 * Триггер выпадающего дерева
 * 
 * @category   Gear
 * @package    GController_SRArticleCategories_Combo
 * @subpackage Nodes
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Nodes.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SRArticleCategories_Combo_Nodes extends GController_Combo_NNodes
{
    /**
     * Префикс полей для запросов в nested set
     *
     * @var string
     */
    public $fieldPrefix = 'category';

    /**
     * Поле для сортовки в запросе
     *
     * @var string
     */
    public $orderBy = '';

    /**
     * Первичное поле
     *
     * @var string
     */
    public $idProperty = 'category_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_categories';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_srarticlecategories_grid';

    /**
     * Предварительная обработка узла дерева перед формированием массива записей JSON
     * 
     * @params array $node узел дерева
     * @params array $record запись
     * @return array
     */
    protected function nodesPreprocessing($node, $record)
    {
        $node['text'] = $record['category_name'];
        if ($record['category_right'] - $record['category_left'] == 1)
            $node['icon'] = $this->resourcePath . 'icon-category.png';

        return $node;
    }
}
?>