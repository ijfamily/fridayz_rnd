<?php
/**
 * Gear Manager
 *
 * Контроллер         "Обработка URL строки"
 * Пакет контроллеров "Категории статьи"
 * Группа пакетов     "Категории статьи"
 * Модуль             "Справочники"
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SRArticleCategories_Generator
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Data');

/**
 * Обработка URL строки
 * 
 * @category   Gear
 * @package    GController_SRArticleCategories_Generator
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SRArticleCategories_Generator_Data extends GController_Data
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_srarticlecategories_grid';

    /**
     * Генерация URL
     * 
     * @return string
     */
    protected function generateUrl()
    {
        parent::dataAccessView();

        $treeId = $this->input->get('tree', 0);
        if ($treeId) {
        $query = new GDb_Query();
            $sql = 'SELECT * FROM `site_categories` WHERE `category_id`=' . $treeId;
            if (($cat = $query->getRecord($sql)) === false)
                throw new GSqlException();
            $path = $cat['category_uri'];
        } else
            $path = '';

        $url = $this->input->get('header', '');
        if (empty($url))
            throw new GException('Data processing error', $this->_['You did not fill the field']);

        if (substr($url, -1) != '/') {
            Gear::library('String');
            $alias = $this->config->getFromCms('Site', 'LANGUAGE/ALIAS');

            $url = $path . GString::toUrl($url, $alias) . '/';
        }

        $this->response->data = array('text' => $url);
    }

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        if ($this->uri->action == 'data')
            // метод запроса
            switch ($this->uri->method) {
                // метод "POST"
                case 'POST': $this->generateUrl(); return;
            }

        throw new GException(
            'Error processing controller',
            'Unable to initialize method "%s" controller',
            $this->uri->method,
            true,
            array('icon'      => 'icon-msg-error',
                  'buttons'   => array('ok' => 'OK', 'yes' => GText::_('Send error report', 'exception')),
                  'btnAction' => 'send report')
        );
    }
}
?>