<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск категорий статьи"
 * Пакет контроллеров "Категории статьи"
 * Группа пакетов     "Справочники"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SRArticleCategories_Search
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск категорий статьи
 * 
 * @category   Gear
 * @package    GController_SRArticleCategories_Search
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SRArticleCategories_Search_Data extends GController_Search_Data
{
    /**
     * Идентификатор компонента (списка) к которому применяется поиск
     *
     * @var string
     */
    public $gridId = 'gcontroller_srarticlecategories_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_srarticlecategories_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор
     * Используется для инициализации атрибутов контроллер не переданного в конструктор
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'category_name',  'as' => 'c.category_name', 'label' => $this->_['header_category_name']),
            array('dataIndex' => 'pcategory_name', 'as' => 'cp.category_name', 'label' => $this->_['header_pcategory_name']),
            array('dataIndex' => 'category_uri', 'as' => 'c.category_uri', 'label' => $this->_['header_category_uri']),
            array('dataIndex' => 'category_visible', 'as' => 'c.category_visible', 'label' => $this->_['header_category_visible']),
        );
    }

    /**
     * Применение фильтра
     * 
     * @return void
     */
    protected function dataAccept()
    {
        if ($this->_sender == 'toolbar') {
            if ($this->input->get('action') == 'search') {
                $tlbFilter = '';
                // если попытка сбросить фильтр
                if (!$this->input->isEmptyPost(array('domain'))) {
                    $tlbFilter = array(
                        'domain' => $this->input->get('domain')
                    );
                }
                $this->store->set('tlbFilter', $tlbFilter);
                return;
            }
        }

        parent::dataAccept();
    }
}
?>