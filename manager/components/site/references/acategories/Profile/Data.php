<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные для интерфейса профиля категории статьи"
 * Пакет контроллеров "Профиль категории статьи"
 * Группа пакетов     "Справочники"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SRArticleCategories_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные для интерфейса профиля категории статьи
 * 
 * @category   Gear
 * @package    GController_SRArticleCategories_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SRArticleCategories_Profile_Data extends GController_Profile_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Префикс полей для запросов в nested set
     *
     * @var string
     */
   public $fieldPrefix = 'category';

    /**
     * Массив полей таблицы ($_tableName)
     *
     * @var array
     */
    public $fields = array(
        'domain_id', 'category_name', 'category_breadcrumb', 'category_uri', 'category_image', 'category_uri_hash', 'category_visible', 'category_left', 'category_right', 'category_level',
        'list', 'list_limit', 'list_order', 'list_sort', 'list_type', 'list_class', 'list_path', 'list_template', 'list_subcategory', 'list_caching',
        'list_calendar', 'page_title', 'page_meta_keywords', 'page_meta_description', 'page_meta_robots', 'page_meta_author', 'page_meta_copyright', 
        'page_meta_revisit', 'page_meta_document'
    );

    /**
     * Первичный ключ таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'category_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_categories';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_srarticlecategories_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_profile_update'], $record['category_name']);
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        // хэш uri
        $uri = $this->input->get('category_uri',  false);
        if ($uri !== false)
            $params['category_uri_hash'] = md5($uri);
    }

    /**
     * Вставка данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataInsert($params = array())
    {
        parent::dataAccessInsert();

        if ($this->input->isEmpty('POST'))
            throw new GException('Warning', 'To execute a query, you must change data!');
        // массив полей с их соответствующими значениями
        if (empty($params))
            $params = $this->input->getBy($this->fields);
        // проверки входных данных
        $this->isDataCorrect($params);
        // проверка существования записи с одинаковыми значениями
        $this->isDataExist($params);
        // использование системных полей в запросе SQL
        if ($this->isSysFields) {
            $params['sys_date_insert'] = date('Y-m-d');
            $params['sys_time_insert'] = date('H:i:s');
            $params['sys_user_insert'] = $this->session->get('user_id');
        }

        Gear::library('Db/Drivers/MySQL/Tree/Tree');

        // указатель на базу данных
        $db = GFactory::getDb();
        $tree = new GDb_Tree($this->tableName, $this->fieldPrefix, $db->getHandle());
        $parentNode = $this->input->get('category_id');
        // добавление записи
        $this->_recordId = $tree->insert($parentNode, '', $params);
        if (!empty($tree->ERRORS_MES)) {
            $error = implode(',', $tree->ERRORS_MES);
            throw new GException('Data processing error', 'Query error (Internal Server Error)', $error, false);
        }

        // запись в журнал действий пользователя
        $this->logInsert(
            array('log_query'        => '[node sql]',
                  'log_error'        => null,
                  'log_query_params' => $params)
        );
    }

    /**
     * Удаление данных
     * 
     * @return void
     */
    protected function dataDelete()
    {
        parent::dataAccessDelete();

        // проверка существования зависимых записей
        $this->isDataDependent();
        // определяем откуда id брать
        if ($this->uri->id)
            $id = $this->uri->id;
        else {
            $input = GFactory::getInput();
            $id = $input->get('id');
        }
        // если не указан $id
        if (empty($id))
            throw new GException('Error', 'Unable to delete records!');

        $query = new GDb_Query();
        // выбранная запись
        $sql = 'SELECT * FROM `site_categories` WHERE `category_id`=' . $id;
        if (($node = $query->getRecord($sql)) === false)
            throw new GSqlException();

        // обновление всех компонентов в группах
        $sql = 'UPDATE `site_articles` `a`, (SELECT `category_id` FROM `site_categories` '
             . 'WHERE `category_left`>=' . $node['category_left'] . ' AND `category_right`<=' . $node['category_right'] . ') `c` '
             . 'SET `a`.`category_id`=NULL WHERE `a`.`category_id`=`c`.`category_id`';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        Gear::library('Db/Drivers/MySQL/Tree/Tree');

        // указатель на базу данных
        $db = GFactory::getDb();
        $tree = new GDb_Tree($this->tableName, $this->fieldPrefix, $db->getHandle());
        // удаление ветвей дерева
        $tree->DeleteAll($id, '');
        if (!empty($tree->ERRORS_MES)) {
            $error = implode(',', $tree->ERRORS_MES);
            throw new GException('Data processing error', 'Query error (Internal Server Error)', $error);
        }
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        Gear::library('Db/Drivers/MySQL/Tree/Tree');

        parent::dataAccessUpdate();

        $table = new GDb_Table($this->tableName, $this->idProperty, $this->fields);
        if ($this->input->isEmpty('PUT'))
            throw new GException('Warning', 'To execute a query, you must change data!');
        // массив полей с их соответствующими значениями
        if (empty($params))
            $params = $this->input->getBy($this->fields);
        // проверки входных данных
        $this->isDataCorrect($params);
        // проверка существования записи с одинаковыми значениями
        $this->isDataExist($params);
        // использование системных полей в запросе SQL
        if ($this->isSysFields) {
            $params['sys_date_update'] = date('Y-m-d');
            $params['sys_time_update'] = date('H:i:s');
            $params['sys_user_update'] = $this->session->get('user_id');
        }
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($params, 'method "PUT"');
        }
        if ($table->update($params, $this->uri->id) === false)
            throw new GSqlException();
        // предок до изменения категории
        $beforeParentId = $this->store->get('parentCategory', false);
        // предок после изменения категории
        $afterParentId = $this->input->get('category_id', false);
        // смена узла дерева
        if ($afterParentId != $beforeParentId && $beforeParentId !== false) {
            $tree = new GDb_Tree($this->tableName, 'category', GFactory::getDb()->getHandle());
            $tree->moveAll($this->uri->id, $afterParentId);
            if (!empty($tree->ERRORS_MES))
                throw new GSqlException();
        }
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($table->query->getSQL(), 'query');
        }
        // запись в журнал действий пользователя
        $this->logUpdate(
            array('log_query'        => $table->query->getSQL(),
                  'log_error'        => $this->response->success === false ? $table->getError() : null,
                  'log_query_params' => $params)
        );

        $this->dataUpdateComplete($params);
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        $query = new GDb_Query();
        // предок категории
        $sql = 'SELECT * FROM `site_categories` '
             . "WHERE {$record['category_left']} > `category_left` AND {$record['category_right']} < `category_right` AND "
             . "{$record['category_level']} = `category_level` + 1";
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        if (!empty($rec)) {
            $this->store->set('parentCategory', $rec['category_id']);
            $setTo[] = array('xtype' => 'mn-field-combo', 'id' => 'fldParentCategory', 'text' => $rec['category_name'], 'value' => $rec['category_id']);
        }

        // шаблон статьи
        if (!empty($record['list_template'])) {
            $sql = 'SELECT * FROM `site_templates` WHERE `template_filename`=\'' . $record['list_template'] . '\'';
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            if (!empty($rec))
                $setTo[] = array('xtype' => 'mn-field-combo', 'id' => 'fldTemplate', 'text' => $rec['template_name'], 'value' => $record['list_template']);
        }
        // если список задействован
        if ($record['list']) {
            $setTo[] = array('id' => 'fldSetList', 'func' => 'setTitle', 'args' => array($this->_['title_fieldset_list_active']));
            $setTo[] = array('id' => 'fldSetList', 'func' => 'expand', 'args' => array(true));
        }

        // установка полей
        $this->response->add('setTo', $setTo);

        return $record;
    }
}
?>