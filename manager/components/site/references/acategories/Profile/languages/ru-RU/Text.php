<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Создание записи "Категория статьи"',
    'title_profile_update' => 'Изменение записи "%s"',
    // поля формы
    // вкладка "категория"
    'label_domain'            => 'Сайт',
    'tip_button_genurl'       => 'Генерация ЧПУ URL статьи',
    'msg_select_header'       => 'Заполните поле &quot;адрес статьи&quot;!',
    'msg_select_node'         => 'Выберите &quot;категорию&quot; статьи!',
    'title_tab_category'      => 'Категория',
    'label_category_name'     => 'Название',
    'label_category_breadcrumb' => 'Цепочка<hb></hb>',
    'tip_category_breadcrumb' => 'Название в "цепочке" статей (если "цепочка" задействована)',
    'title_fieldset_category' => 'Положение в дереве категорий',
    'label_pcategory_name'    => 'предок',
    'title_fieldset_address'  => 'ЧПУ URL категории',
    'label_category_uri'      => 'адрес<hb></hb>',
    'tip_category_uri'        => 'Адрес имеет вид "/category/subcategory-1/subcategory-2/"',
    'blank_category_uri'      => '/category/',
    'label_category_visible'  => 'Показывать',
    'msg_fill_address'        => 'Заполните поле "адрес"',
    'title_fieldset_list'     => 'Список в категории',
    'title_fieldset_list_active' => 'Список в категории (задействован)',
    'label_template_name'     => 'Шаблон<hb></hb>',
    'tip_template_name'      => 'Шаблон вывода элементов списка',
    'label_list_order'       => 'Порядок сортировки<hb></hb>',
    'tip_list_order'         => 'Порядок сортировки элементов в списке',
    'label_list_sort'        => 'Критерий сортировки<hb></hb>',
    'tip_list_sort'          => 'Критерий сортировки элементов в списке',
    'label_list_type'        => 'Выводить по виду<hb></hb>',
    'tip_list_type'          => 'Выводить элементы списка по виду',
    'label_list_limit'       => 'Записей на странице<hb></hb>',
    'tip_list_limit'         => 'Количество записей выводимых на 1-й странице, оставьте 0 или пустым, если хотите использовать настройки по умолчаню',
    'label_list_subcategory' => 'В подкатегориях<hb></hb>',
    'tip_list_subcategory'   => 'Выводить элементы списка опубликованные в подкатегориях',
    'label_list'             => 'Задействовать список',
    'label_list_class'       => 'Вид списка',
    'label_list_caching'     => 'Кэширование<hb></hb>',
    'tip_list_caching'       => 'Кэширование списка обеспечивает быстрый доступ при помощи создания статической страницы на сервере',
    'label_calendar'         => 'Подключить календарь',
    'label_category_image'   => 'Изображ.<hb></hb>',
    'tip_category_image'     => 'Изображение категории',
    // кэш страницы
    'title_fieldset_cache' => 'Кэш страницы (создан: %s)',
    'msg_cache_delete'     => 'Вы действительно желаете удалить кэш страницы?',
    'text_cache_delete'    => 'Удалить кэш',
    'tip_cache_delete'     => 'Удаление кэша категории статьи',
    'title_deleting_cache' => 'Удаление кэша категории статьи',
    'msg_deleted_cache'    => 'Все файлы кэша категории статьи были успешно удалены!',
    'label_fld_date'    => 'Дата создания',
    'label_fld_file'    => 'Файл на сервере',
    'label_fld_address' => 'URL адрес страницы',
    // вкладка "seo категории"
    'title_fieldset_meta' => 'Метатеги статьи',
    'title_tab_seo'     => 'SEO',
    'title_fieldset_r'  => 'Роботы',
    'label_page_meta_r' => 'Роботы<hb></hb>',
    'tip_page_meta_r'   => 'Формирует информацию о гипертекстовых документах, которая поступает к роботам поисковых систем. 
                            Значения тега могут быть следующими: Index (страница должна быть проиндексирована), Noindex (документ
                            не индексируется), Follow (гиперссылки на странице отслеживаются), Nofollow (гиперссылки не прослеживаются),
                            All (включает значения index и follow, включен по умолчанию), None (включает значения noindex и nofollow).',
    'label_page_meta_v' => 'Время и интервал<hb></hb>',
    'tip_page_meta_v'   => 'Время и интервал посещения поискового робота. Позволяет управлять частотой индексации документа в поисковой системе.',
    'label_page_meta_m' => 'Состояние<hb></hb>',
    'tip_page_meta_m'   => 'Значение «Static» отмечает, что системе нет необходимости индексировать документ в дальнейшем,
                            «Dynamic» позволяет регулярно индексировать Интернет-страницу',
    'title_fieldset_a'  => 'Авторское право',
    'label_page_meta_a' => 'Автор<hb></hb>',
    'tip_page_meta_a'   => 'Идентификация автора или принадлежности документа. Содержит имя автора Интернет-страницы, в том случае, 
                            если сайт принадлежит какой-либо организации, целесообразнее использовать поле «Организация».',
    'label_page_meta_c' => 'Организация<hb></hb>',
    'tip_page_meta_c'   => 'Идентификация принадлежности документа к какой-либо организации',
    'label_page_meta_k' => 'Ключевые слова<hb></hb><br><small>(keywords)</small>',
    'tip_page_meta_k'   => 'Поисковые системы используют для того, чтобы определить релевантность ссылки. При формировании данного тега 
                            необходимо использовать только те слова, которые содержатся в самом документе. Использование тех слов, которых 
                            нет на странице, не рекомендуется. Рекомендованное количество слов в данном теге — не более десяти.',
    'label_page_meta_d' => 'Описание<hb></hb><br><small>(description)</small>',
    'tip_page_meta_d'   => 'Используется поисковыми системами для индексации, а также при создании аннотации в выдаче по запросу
                            При отсутствии тега поисковые системы выдают в аннотации первую строку документа или отрывок, содержащий ключевые слова.
                            Отображается после ссылки при поиске страниц в поисковике.',
    'label_page_title'  => 'Загаловок<hb></hb><br><small>(title)</small>',
    'tip_page_title'    => 'Отображается в названии вкладки браузера',
    // тип
    'data_priorities' => array(
        array('0.0'), array('0.1'), array('0.2'), array('0.3'), array('0.4'), array('0.5'), array('0.6'),
        array('0.7'), array('0.8'), array('0.9'), array('1.0')
    ),
    'data_changefreq' => array(
        array('всегда', 'a'), array('почасовая', 'h'), array('ежедневно', 'd'), array('еженедельно', 'w'), array('ежемесячно', 'm'),
        array('ежегодно', 'y'), array('никогда', 'n')
    ),
    'data_changefreq_a' => array('a' => 'всегда', 'h' => 'почасовая', 'd' => 'ежедневно', 'w' => 'еженедельно', 'm' => 'ежемесячно', 'y' => 'ежегодно', 'n' => 'никогда'),
    'data_robots' => array(
        array('all'), array('index, follow'), array('noindex, follow'), array('index, nofollow'), array('noindex, nofollow'),
        array('none')
    ),
    'data_revisit' => array(
        array('1 день', 1), array('2 дня', 2), array('3 дня', 3), array('4 дня', 4), array('5 дней', 5), array('6 дней', 6), array('7 дней', 7)
    ),
    'data_doc' => array(array('Static'), array(' Dynamic')),
    'data_order' => array(array('По убыванию', 'd'), array('По возрастанию', 'a')),
    'data_sort' => array(array('По алфавиту', 'header'), array('По дате публикации', 'date')),
    'data_type' => array(array('Все', 0), array('Статья', 1), array('Новость', 2)),
    'data_class' => array(/*array('Статьи категории', 'ListArticles'), array('Подкатегории в категории', 'ListCategories'), array('Фотоальбомы', 'Albums')*/)
);
?>