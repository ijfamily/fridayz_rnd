<?php
/**
 * Gear Manager
 *
 * Контроллер         "Сброс кэша категории"
 * Пакет контроллеров "Профиль категории"
 * Группа пакетов     "Спрвочники"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SRArticleCategories_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Cache.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Сброс кэша статьи
 * 
 * @category   Gear
 * @package    GController_SRArticleCategories_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Cache.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SRArticleCategories_Profile_Cache extends GController_Data
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sarticles_grid';

    /**
     * Каталог кэша сайта
     *
     * @var string
     */
    public $pathData = '../cache/';

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        if ($this->uri->action == 'cache')
            // метод запроса
            switch ($this->uri->method) {
                // метод "CLEAR"
                case 'CLEAR': $this->dataClear(); return;
            }

        parent::init();
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataAccessClear();

        Gear::library('File');

        $query = new GDb_Query();
        // данные кэша страницы
        $sql = 'SELECT * FROM `site_cache` '
             . 'WHERE `cache_generator`=\'category\' AND `cache_generator_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $items = array();
        while (!$query->eof()) {
            $cache = $query->next();
            // если есть кэш
            if ($cache['cache_filename']) {
                GFile::delete($this->pathData . $cache['cache_filename']);
            }
        }
        // удаление записей кэша
        $sql = 'DELETE FROM `site_cache` '
             . 'WHERE `cache_generator`=\'category\' AND `cache_generator_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $this->response->setMsgResult($this->_['title_deleting_cache'], $this->_['msg_deleted_cache'], true);
    }
}
?>