<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля категории статьиа"
 * Пакет контроллеров "Профиль категории статьи"
 * Группа пакетов     "Справочники"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SRArticleCategories_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля категории статьи
 * 
 * @category   Gear
 * @package    GController_SRArticleCategories_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SRArticleCategories_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Возращает список доменов
     * 
     * @return array
     */
    protected function getDomains()
    {
        $arr = $this->config->getFromCms('Domains');
        $list = array();
        if ($arr) {
            $list[] = array($this->config->getFromCms('Site', 'NAME', ''), 0);
            foreach ($arr['indexes'] as $key => $value) {
                $list[] = array($value[1], $key);
            } 
        }

        return $list;
    }

    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        $data = array('cache' => array(), 'class' => array());
        
        $list = $this->config->getFromCms('Site', 'LIST/TYPES');
        foreach ($list as $class => $item) {
            $data['class'][] = array($item['name'], $class);
        }
        //$data['class']

        // если состояние формы "вставка"
        if ($this->isInsert) return $data;

        parent::getDataInterface();

        $query = new GDb_Query();
        // данные кэша страницы
        $sql = 'SELECT * FROM `site_cache` WHERE `cache_generator`=\'category\' AND `cache_generator_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $items = array();
        while (!$query->eof()) {
            $cache = $query->next();
            $items[] = array(
                'xtype'      => 'displayfield',
                'fieldLabel' => $this->_['label_fld_date'],
                'anchor'     => '100%',
                'fieldClass' => 'mn-field-value-info',
                'value'      => date('d-m-Y H:i:s', strtotime($cache['cache_date']))
            );
            $items[] = array(
                'xtype'      => 'displayfield',
                'fieldLabel' => $this->_['label_fld_file'],
                'anchor'     => '100%',
                'value'      => '<a target="blank" href="/cache/' . $cache['cache_filename'] . '">' . $cache['cache_filename'] . '</a>'
            );
            $items[] = array(
                'xtype'      => 'displayfield',
                'fieldLabel' => $this->_['label_fld_address'],
                'anchor'     => '100%',
                'value'      => '<a target="blank" href="' . $cache['cache_uri'] . '">' . $cache['cache_uri'] . '</a>'
            );
            $items[] = array('xtype' => 'mn-field-separator');
        }
        $fs = array();
        if ($items) {
            $items[] = array(
                'xtype'       => 'mn-btn-resetcache',
                'msgConfirm'  => $this->_['msg_cache_delete'],
                'text'        => $this->_['text_cache_delete'],
                'tooltip'     => $this->_['tip_cache_delete'],
                'icon'        => $this->resourcePath . 'icon-cache-delete.png',
                'ctId'        => 'fs-cache',
                'width'       => 107,
                'url'         => $this->componentUrl . 'profile/cache/' . $this->uri->id
            );
            $fs = array(
                'xtype'       => 'fieldset',
                'id'          => 'fs-cache',
                'width'       => 680,
                'title'       => sprintf($this->_['title_fieldset_cache'], date('d-m-Y H:i:s', strtotime($cache['cache_date']))),
                'collapsible' => true,
                'collapsed'   => true,
                'labelWidth'  => 125,
                'items'       => $items
            );
        }
        $data['cache'] = $fs;

        return $data;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'titleEllipsis' => 80,
                  'gridId'        => 'gcontroller_srarticlecategories_grid',
                  'width'         => 750,
                  'height'        => 595,
                  'resizable'     => false,
                  'stateful'      => false)
        );

        // поля формы (ExtJS class "Ext.Panel")
        $tabCategoryItems = array(
            array('xtype'         => 'combo',
                  'fieldLabel'    => $this->_['label_domain'],
                  'itemCls'       => 'mn-form-item-quiet',
                  'name'          => 'domain_id',
                  'hidden'        => !$this->config->getFromCms('Site', 'OUTCONTROL'),
                  'value'         => 0,
                  'editable'      => false,
                  'maxLength'     => 100,
                  'width'         => 200,
                  'typeAhead'     => false,
                  'triggerAction' => 'all',
                  'mode'          => 'local',
                  'store'         => array(
                      'xtype'  => 'arraystore',
                      'fields' => array('display', 'value'),
                      'data'   => $this->getDomains()
                  ),
                  'hiddenName'    => 'domain_id',
                  'valueField'    => 'value',
                  'displayField'  => 'display',
                  'allowBlank'    => true),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_category_name'],
                  'name'       => 'category_name',
                  'maxLength'  => 255,
                  'width'      => 580,
                  'allowBlank' => false),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_category_breadcrumb'],
                  'labelTip'   => $this->_['tip_category_breadcrumb'],
                  'name'       => 'category_breadcrumb',
                  'itemCls'    => 'mn-form-item-quiet',
                  'maxLength'  => 255,
                  'width'      => 580,
                  'allowBlank' => true),
          array('xtype'           => 'mn-field-browse',
                'params'          => 'view=categories',
                'triggerWdgSetTo' => true,
                'triggerWdgUrl'   => 'site/media/' . ROUTER_DELIMITER .'dialog/interface/',
                'fieldLabel' => $this->_['label_category_image'],
                'labelTip'   => $this->_['tip_category_image'],
                'itemCls'    => 'mn-form-item-quiet',
                'id'         => 'fldCategoryImage',
                'name'       => 'category_image',
                'value'      => '',
                'width'      => 580,
                'allowBlank' => true),

             array('xtype'      => 'fieldset',
                   'labelWidth' => 60,
                   'width'      => 680,
                   'title'      => $this->_['title_fieldset_address'],
                   'autoHeight' => true,
                   'cls'        => 'mn-container-clean',
                   'layout'     => 'column',
                   'items'      => array(
                       array('layout' => 'form',
                             'width'  => 605,
                             'items'  => array(
                                   array('xtype'      => 'textfield',
                                         'id'         => 'fldCategoryUri',
                                         'fieldLabel' => $this->_['label_category_uri'],
                                         'labelTip'   => $this->_['tip_category_uri'],
                                         'name'       => 'category_uri',
                                         'blankText'  => $this->_['blank_category_uri'],
                                         'maxLength'  => 255,
                                         'anchor'     => '100%',
                                         'allowBlank' => false)
                               )
                         ),
                         array('layout'    => 'form',
                               'width'     => 33,
                               'bodyStyle' => 'margin-left:3px;',
                               'items'     => array(
                                   array('xtype'   => 'mn-btn-genurl',
                                         'type'    => 1,
                                         'fldAddress' => 'fldCategoryUri',
                                         'fldTree' => 'fldParentCategory',
                                         'tooltip' => $this->_['tip_button_genurl'],
                                         'icon'    => $this->resourcePath . 'icon-btn-gear.png',
                                         'url'     => $this->componentUrl . '../' . ROUTER_DELIMITER . 'generator/data/',
                                         'msgSelectHeader' => $this->_['msg_fill_address'])
                               )
                         )
                   )
             ),
            array('xtype'      => 'fieldset',
                  'labelWidth' => 60,
                  'title'      => $this->_['title_fieldset_category'],
                  'width'      => 680,
                  'autoHeight' => true,
                  'items'      => array(
                      array('xtype'      => 'mn-field-combo-tree',
                            'id'         => 'fldParentCategory',
                            'fieldLabel' => $this->_['label_pcategory_name'],
                            'resetable'  => false,
                            'width'      => 400,
                            'name'       => 'category_id',
                            'hiddenName' => 'category_id',
                            'treeWidth'  => 400,
                            'treeRoot'   => array('id' => 'root', 'expanded' => true, 'expandable' => true),
                            'allowBlank' => false,
                            'store'      => array(
                                'xtype' => 'jsonstore',
                                'url'   => $this->componentUrl . 'combo/nodes/'
                            )
                      )
                  )
             ),
            array('xtype'       => 'fieldset',
                  'id'          => 'fldSetList',
                  'labelWidth'  => 150,
                  'title'       => $this->_['title_fieldset_list'],
                  'width'       => 680,
                  'autoHeight'  => true,
                  'collapsible' => true,
                  'collapsed'   => true,
                  'items'       => array(
                      array('xtype'      => 'numberfield',
                            'itemCls'    => 'mn-form-item-quiet',
                            'fieldLabel' => $this->_['label_list_limit'],
                            'labelTip'   => $this->_['tip_list_limit'],
                            'name'       => 'list_limit',
                            'checkDirty' => false,
                            'value'      => 0,
                            'width'      => 70,
                            'allowBlank' => true,
                            'emptyText'  => 0),
                      array('xtype'         => 'combo',
                            'itemCls'       => 'mn-form-item-quiet',
                            'fieldLabel'    => $this->_['label_list_order'],
                            'labelTip'      => $this->_['tip_list_order'],
                            'name'          => 'list_order',
                            'checkDirty'    => false,
                            'value'         => 'a',
                            'editable'      => false,
                            'width'         => 157,
                            'typeAhead'     => false,
                            'triggerAction' => 'all',
                            'mode'          => 'local',
                            'store'         => array(
                                'xtype'  => 'arraystore',
                                'fields' => array('list', 'value'),
                                'data'   => $this->_['data_order']
                            ),
                            'hiddenName'    => 'list_order',
                            'valueField'    => 'value',
                            'displayField'  => 'list',
                            'allowBlank'    => true),
                      array('xtype'         => 'combo',
                            'itemCls'       => 'mn-form-item-quiet',
                            'fieldLabel'    => $this->_['label_list_sort'],
                            'labelTip'      => $this->_['tip_list_sort'],
                            'name'          => 'list_sort',
                            'value'         => 'date',
                            'checkDirty'    => false,
                            'editable'      => false,
                            'width'         => 157,
                            'typeAhead'     => false,
                            'triggerAction' => 'all',
                            'mode'          => 'local',
                            'store'         => array(
                                'xtype'  => 'arraystore',
                                'fields' => array('list', 'value'),
                                'data'   => $this->_['data_sort']
                            ),
                            'hiddenName'    => 'list_sort',
                            'valueField'    => 'value',
                            'displayField'  => 'list',
                            'allowBlank'    => true),
                      array('xtype'         => 'combo',
                            'itemCls'       => 'mn-form-item-quiet',
                            'fieldLabel'    => $this->_['label_list_type'],
                            'labelTip'      => $this->_['tip_list_type'],
                            'name'          => 'list_type',
                            'checkDirty'    => false,
                            'value'         => 0,
                            'editable'      => false,
                            'width'         => 157,
                            'typeAhead'     => false,
                            'triggerAction' => 'all',
                            'mode'          => 'local',
                            'store'         => array(
                                'xtype'  => 'arraystore',
                                'fields' => array('list', 'value'),
                                'data'   => $this->_['data_type']
                            ),
                            'hiddenName'    => 'list_type',
                            'valueField'    => 'value',
                            'displayField'  => 'list',
                            'allowBlank'    => true),
                      array('xtype'         => 'combo',
                            'itemCls'       => 'mn-form-item-quiet',
                            'fieldLabel'    => $this->_['label_list_class'],
                            'name'          => 'list_class',
                            'checkDirty'    => false,
                            'value'         => 'ListArticles',
                            'editable'      => false,
                            'width'         => 270,
                            'typeAhead'     => false,
                            'triggerAction' => 'all',
                            'mode'          => 'local',
                            'store'         => array(
                                'xtype'  => 'arraystore',
                                'fields' => array('list', 'value'),
                                'data'   => $data['class']
                            ),
                            'hiddenName'    => 'list_class',
                            'valueField'    => 'value',
                            'displayField'  => 'list',
                            'allowBlank'    => true),
                      array('xtype'      => 'mn-field-combo',
                            'id'         => 'fldTemplate',
                            'tpl'        => 
                                '<tpl for="."><div ext:qtip="{name}" class="x-combo-list-item">'
                                . '<img width="16px" height="16px" align="absmiddle" src="' 
                                . $this->path . '/../../../templates/Combo/resources/icons/{data}.png" style="margin-right:5px">{name}</div></tpl>',
                            'fieldLabel' => $this->_['label_template_name'],
                            'labelTip'   => $this->_['tip_template_name'],
                            'name'       => 'list_template',
                            'checkDirty' => false,
                            'editable'   => false,
                            'width'      => 270,
                            'pageSize'   => 50,
                            'hiddenName' => 'list_template',
                            'allowBlank' => true,
                            'store'      => array(
                                'xtype' => 'jsonstore',
                                'url'   => $this->componentUrl . 'combo/trigger/?name=templates'
                            )
                        ),
                        array('xtype'      => 'mn-field-chbox',
                              'fieldLabel' => $this->_['label_list_subcategory'],
                              'labelTip'   => $this->_['tip_list_subcategory'],
                              'itemCls'    => 'mn-form-item-quiet',
                              'default'    => $this->isUpdate ? null : 0,
                              'checkDirty' => false,
                              'name'       => 'list_subcategory'),
                        array('xtype'      => 'mn-field-chbox',
                              'fieldLabel' => $this->_['label_list_caching'],
                              'labelTip'   => $this->_['tip_list_caching'],
                              'itemCls'    => 'mn-form-item-quiet',
                              'default'    => $this->isUpdate ? null : 0,
                              'checkDirty' => false,
                              'name'       => 'list_caching'),
                        array('xtype'      => 'mn-field-chbox',
                              'fieldLabel' => $this->_['label_calendar'],
                              'default'    => $this->isUpdate ? null : 0,
                              'checkDirty' => false,
                              'name'       => 'list_calendar'),
                        array('xtype'      => 'mn-field-chbox',
                              'fieldLabel' => $this->_['label_list'],
                              'default'    => $this->isUpdate ? null : 0,
                              'checkDirty' => false,
                              'name'       => 'list')
                  )
            )
        );
        // если есть кэш
        if ($data['cache'])
            $tabCategoryItems[] = $data['cache'];
        $tabCategoryItems[] = array(
            'xtype'      => 'mn-field-chbox',
            'fieldLabel' => $this->_['label_category_visible'],
            'default'    => $this->isUpdate ? null : 1,
            'name'       => 'category_visible'
        );
        // вкладка "категория"
        $tabCategory = array(
            'title'       => $this->_['title_tab_category'],
            'layout'      => 'form',
            'baseCls'     => 'mn-form-tab-body',
            'defaultType' => 'textfield',
            'items'       => array($tabCategoryItems)
        );

        // поля вкладки "SEO категории"
        $tabSeoItems = array(
            array('xtype'       => 'fieldset',
                  'width'       => 660,
                  'labelWidth'  => 120,
                  'collapsible' => true,
                  'collapsed'   => true,
                  'title'       => $this->_['title_fieldset_r'],
                  'autoHeight'  => true,
                  'items'       => array(
                      array('xtype'         => 'combo',
                            'fieldLabel'    => $this->_['label_page_meta_r'],
                            'labelTip'      => $this->_['tip_page_meta_r'],
                            'name'          => 'page_meta_robots',
                            'editable'      => true,
                            'checkDirty'    => false,
                            'maxLength'     => 100,
                            'width'         => 180,
                            'typeAhead'     => false,
                            'triggerAction' => 'all',
                            'mode'          => 'local',
                            'value'         => 'index, nofollow',
                            'store'         => array(
                                'xtype'  => 'arraystore',
                                'fields' => array('list'),
                                'data'   => $this->_['data_robots']
                            ),
                            'hiddenName'    => 'page_meta_robots',
                            'valueField'    => 'list',
                            'displayField'  => 'list',
                            'allowBlank'    => true),
                      array('xtype'         => 'combo',
                            'itemCls'       => 'mn-form-item-quiet',
                            'fieldLabel'    => $this->_['label_page_meta_v'],
                            'labelTip'      => $this->_['tip_page_meta_v'],
                            'name'          => 'page_meta_revisit',
                            'editable'      => true,
                            'maxLength'     => 10,
                            'width'         => 180,
                            'typeAhead'     => false,
                            'triggerAction' => 'all',
                            'mode'          => 'local',
                            'value'         => '',
                            'store'         => array(
                                'xtype'  => 'arraystore',
                                'fields' => array('list', 'value'),
                                'data'   => $this->_['data_revisit']
                            ),
                            'hiddenName'    => 'page_meta_revisit',
                            'valueField'    => 'value',
                            'displayField'  => 'list',
                            'allowBlank'    => true),
                      array('xtype'         => 'combo',
                            'itemCls'       => 'mn-form-item-quiet',
                            'fieldLabel'    => $this->_['label_page_meta_m'],
                            'labelTip'      => $this->_['tip_page_meta_m'],
                            'name'          => 'page_meta_document',
                            'editable'      => false,
                            'maxLength'     => 10,
                            'width'         => 180,
                            'typeAhead'     => false,
                            'triggerAction' => 'all',
                            'mode'          => 'local',
                            'value'         => '',
                            'store'         => array(
                                'xtype'  => 'arraystore',
                                'fields' => array('list'),
                                'data'   => $this->_['data_doc']
                            ),
                            'hiddenName'    => 'page_meta_document',
                            'valueField'    => 'list',
                            'displayField'  => 'list',
                            'allowBlank'    => true)
                  )
            ),
            array('xtype'       => 'fieldset',
                  'width'       => 660,
                  'labelWidth'  => 120,
                  'title'       => $this->_['title_fieldset_a'],
                  'collapsible' => true,
                  'collapsed'   => true,
                  'autoHeight'  => true,
                  'items'       => array(
                      array('xtype'      => 'textfield',
                            'itemCls'       => 'mn-form-item-quiet',
                            'fieldLabel' => $this->_['label_page_meta_a'],
                            'labelTip'   => $this->_['tip_page_meta_a'],
                            'name'       => 'page_meta_author',
                            'emptyText'  => $_SESSION['profile/name'],
                            'maxLength'  => 100,
                            'width'      => 252,
                            'allowBlank' => true),
                      array('xtype'      => 'textfield',
                            'itemCls'       => 'mn-form-item-quiet',
                            'fieldLabel' => $this->_['label_page_meta_c'],
                            'labelTip'   => $this->_['tip_page_meta_c'],
                            'name'       => 'page_meta_copyright',
                            'maxLength'  => 255,
                            'width'      => 252,
                            'allowBlank' => true),
                  )
            ),
            array('xtype'      => 'fieldset',
                  'width'      => 660,
                  'labelWidth' => 120,
                  'title'      => $this->_['title_fieldset_meta'],
                  'autoHeight' => true,
                  'items'      => array(
                        array('xtype'      => 'textfield',
                              'fieldLabel' => $this->_['label_page_title'],
                              'labelTip'   => $this->_['tip_page_title'],
                              'itemCls'    => 'mn-form-item-quiet',
                              'name'       => 'page_title',
                              'maxLength'  => 255,
                              'anchor'     => '100%',
                              'allowBlank' => true),
                        array('xtype'      => 'textarea',
                              'itemCls'    => 'mn-form-item-quiet',
                              'fieldLabel' => $this->_['label_page_meta_k'],
                              'labelTip'   => $this->_['tip_page_meta_k'],
                              'name'       => 'page_meta_keywords',
                              'anchor'     => '100%',
                              'height'     => 150,
                              'allowBlank' => true),
                        array('xtype'      => 'textarea',
                              'itemCls'    => 'mn-form-item-quiet',
                              'fieldLabel' => $this->_['label_page_meta_d'],
                              'labelTip'   => $this->_['tip_page_meta_d'],
                              'name'       => 'page_meta_description',
                              'anchor'     => '100%',
                              'height'     => 150,
                              'allowBlank' => true)
                  )
            )
        );
        // владка "SEO категории"
        $tabSeo = array(
            'title'       => $this->_['title_tab_seo'],
            'iconSrc'     => $this->resourcePath . 'icon-tab-seo.png',
            'layout'      => 'form',
            'labelWidth'  => 85,
            'baseCls'     => 'mn-form-tab-body',
            'defaultType' => 'textfield',
            'items'       => array($tabSeoItems)
        );

        // вкладки окна
        $tabs = array(
            array('xtype'             => 'tabpanel',
                  'layoutOnTabChange' => true,
                  'activeTab'         => 0,
                  'style'             => 'padding:3px',
                  'enableTabScroll'   => true,
                  'anchor'            => '100% 100%',
                  'defaults'          => array('autoScroll' => true),
                  'items'             => array($tabCategory, $tabSeo))
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($tabs);
        $form->url = $this->componentUrl . 'profile/';

        parent::getInterface();
    }
}
?>