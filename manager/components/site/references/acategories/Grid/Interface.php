<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка категорий статей"
 * Пакет контроллеров "Список категорий статей"
 * Группа пакетов     "Справочники"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SRArticleCategories_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс списка категорий статей
 * 
 * @category   Gear
 * @package    GController_SRArticleCategories_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SRArticleCategories_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'category_id';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'category_left';

    /**
     * Возращает список доменов
     * 
     * @return array
     */
    protected function getDomains()
    {
        $arr = $this->config->getFromCms('Domains');
        $list = array();
        if ($arr) {
            $list[] = array('Все', -1);
            $list[] = array($this->config->getFromCms('Site', 'NAME', ''), 0);
            $domains = $arr['domains'];
            foreach ($arr['indexes'] as $key => $value) {
                $list[] = array($value[1], $key);
            } 
        }

        return $list;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // название сайта
            array('name' => 'domain_id', 'type' => 'string'),
            // название
            array('name' => 'category_name', 'type' => 'string'),
            array('name' => 'category_title', 'type' => 'string'),
            // название предка
            array('name' => 'pcategory_name', 'type' => 'string'),
            // покахывать
            array('name' => 'category_visible', 'type' => 'integer'),
            // адрес
            array('name' => 'category_uri', 'type' => 'string'),
            array('name' => 'category_url', 'type' => 'string'),
            array('name' => 'goto_url', 'type' => 'string'),
            // задействован список
            array('name' => 'list', 'type' => 'integer'),
            // индексация
            array('name' => 'page_meta_robots', 'type' => 'string'),
            array('name' => 'page_meta_robots_s', 'type' => 'string')
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $this->columns = array(
            array('xtype'     => 'numbercolumn'),
            array('xtype'     => 'rowmenu'),
            array('dataIndex' => 'domain_id',
                  'header'    => $this->_['header_site_name'],
                  'width'     => 90,
                  'hidden'    => !$this->config->getFromCms('Site', 'OUTCONTROL'),
                  'sortable'  => true),
            array('dataIndex' => 'category_name',
                  'header'    => '<em class="mn-grid-hd-details"></em>' . $this->_['header_category_name'],
                  'width'     => 300,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'pcategory_name',
                  'header'    => $this->_['header_pcategory_name'],
                  'width'     => 160,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'goto_url',
                  'align'     => 'center',
                  'header'    => '&nbsp;',
                  'tooltip'   => $this->_['tooltip_goto_url'],
                  'fixed'     => true,
                  'hideable'  => false,
                  'width'     => 25,
                  'sortable'  => false,
                  'menuDisabled' => true),
            array('dataIndex' => 'category_uri',
                  'header'    => $this->_['header_category_uri'],
                  'width'     => 210,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('xtype'     => 'booleancolumn',
                  'align'     => 'center',
                  'cls'       => 'mn-bg-color-gray2',
                  'dataIndex' => 'category_visible',
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-visible.png" align="absmiddle">',
                  'tooltip'   => $this->_['tooltip_category_visible'],
                  'align'     => 'center',
                  'width'     => 45,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean')),
            array('xtype'     => 'booleancolumn',
                  'align'     => 'center',
                  'cls'       => 'mn-bg-color-gray2',
                  'dataIndex' => 'list',
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-list.png" align="absmiddle">',
                  'tooltip'   => $this->_['tooltip_list'],
                  'align'     => 'center',
                  'width'     => 45,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean')),
            array('dataIndex' => 'page_meta_robots',
                  'header'    => $this->_['header_robots'],
                  'tooltip'   => $this->_['tooltip_robots'],
                  'width'     => 115,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string'))
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_grid'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_grid'],
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'profileUrl'    => $this->componentUrl . 'profile/',
                  'sm'            => array('singleSelect' => true))
        );

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "edit" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "добавить" (ExtJS class "Manager.button.InsertData")
        $group->items->add(array('xtype' => 'mn-btn-insert-data', 'gridId' => $this->classId));
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' => $this->classId));
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'      => 'mn-btn-clear-data',
                  'msgConfirm' => $this->_['msg_btn_clear'],
                  'gridId'     => $this->classId,
                  'isN'        => true)
        );
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = 'component-site-categories';
        $this->_cmp->tbar->items->add($group);

        // управление сайтами на других доменах
        if ($this->config->getFromCms('Site', 'OUTCONTROL')) {
            $domains = $this->getDomains();
            if ($domains) {
                // группа кнопок "фильтр" (ExtJS class "Ext.ButtonGroup")
                $group = new Ext_ButtonGroup_Filter(
                    array('title'  => $this->_['title_buttongroup_filter'],
                          'gridId' => $this->classId)
                );
                // кнопка "справка" (ExtJS class "Manager.button.Help")
                $btn = &$group->items->get(0);
                $btn['gridId'] = $this->classId;
                $btn['url'] = $this->componentUrl . 'search/data/';
                // кнопка "поиск" (ExtJS class "Manager.button.Search")
                $filter = $this->store->get('tlbFilter', array());
                $group->items->add(array(
                    'xtype'       => 'form',
                    'labelWidth'  => 35,
                    'bodyStyle'   => 'border:1px solid transparent;background-color:transparent',
                    'frame'       => false,
                    'bodyBorder ' => false,
                    'items'       => array(
                        array('xtype'         => 'combo',
                              'fieldLabel'    => $this->_['label_domain'],
                              'name'          => 'domain',
                              'checkDirty'    => false,
                              'editable'      => false,
                              'width'         => 140,
                              'typeAhead'     => false,
                              'triggerAction' => 'all',
                              'mode'          => 'local',
                              'store'         => array(
                                  'xtype'  => 'arraystore',
                                  'fields' => array('list', 'value'),
                                  'data'   => $domains
                              ),
                              'value'         => empty($filter) ? '' : $filter['domain'],
                              'hiddenName'    => 'domain',
                              'valueField'    => 'value',
                              'displayField'  => 'list',
                              'allowBlank'    => false)
                    )
                ));
                $this->_cmp->tbar->items->add($group);
            }
        }

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $cellInfo =
            '<div class="mn-grid-cell-tooltip-tl">{category_title}</div>'
          . '<div class="mn-grid-cell-tooltip">'
          . '<em>' . $this->_['header_category_brd'] . '</em>: <b>{category_breadcrumb}</b><br>'
          . '<em>' . $this->_['header_pcategory_name'] . '</em>: <b>{pcategory_name}</b><br>'
          . '<em>' . $this->_['header_category_uri'] . '</em>: <b>{category_uri}</b><br>'
          . '<em>' . $this->_['tooltip_category_visible'] . '</em>: '
          . '<tpl if="category_visible == 0"><b>' . $this->_['data_boolean'][0] . '</b></tpl>'
          . '<tpl if="category_visible == 1"><b>' . $this->_['data_boolean'][1] . '</b></tpl><br>'
          . '</div>';
        $this->_cmp->cellTips = array(
            array('field' => 'category_name', 'tpl' => $cellInfo),
            array('field' => 'pcategory_name', 'tpl' => '{pcategory_name}'),
            array('field' => 'category_uri', 'tpl' => '{category_url}'),
            array('field' => 'category_breadcrumb', 'tpl' => '{category_breadcrumb}')
        );

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $items = array(
            array('text'    => $this->_['rowmenu_edit'],
                  'iconCls' => 'icon-form-edit',
                  'url'     => $this->componentUrl . 'profile/interface/')
        );
        $this->_cmp->rowMenu = array('xtype' => 'mn-grid-rowmenu', 'items' => $items);

        parent::getInterface();
    }
}
?>