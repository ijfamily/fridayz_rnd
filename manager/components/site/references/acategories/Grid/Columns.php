<?php
/**
 * Gear Manager
 *
 * Контроллер         "Столбцы списка категорий статей"
 * Пакет контроллеров "Список категорий статей"
 * Группа пакетов     "Справочники"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SRArticleCategories_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Columns.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * @see GController_Grid_Columns
 */
require_once('Gear/Controllers/Grid/Columns.php');

/**
 * Столбцы списка категорий статей
 * 
 * @category   Gear
 * @package    GController_SRArticleCategories_Grid
 * @subpackage Columns
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Columns.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SRArticleCategories_Grid_Columns extends GController_Grid_Columns
{}
?>