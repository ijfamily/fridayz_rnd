<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка категорий статей"
 * Пакет контроллеров "Список категорий статей"
 * Группа пакетов     "Справочники"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SRArticleCategories_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные списка категорий статей
 * 
 * @category   Gear
 * @package    GController_SRArticleCategories_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SRArticleCategories_Grid_Data extends GController_Grid_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * (для обработки записей таблицы)
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Префикс полей для запросов в nested set
     *
     * @var string
     */
   public $fieldPrefix = 'category';

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'category_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_categories';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'category_name';

    /**
     * Домены
     *
     * @var array
     */
    public $domains = array();

    /**
     * Управления сайтами на других доменах
     *
     * @var boolean
     */
    public $isOutControl = false;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // управление сайтами на других доменах
        $this->isOutControl = $this->config->getFromCms('Site', 'OUTCONTROL');
        if ($this->isOutControl)
            $this->domains = $this->config->getFromCms('Domains', 'indexes');
        $this->domains[0] = array($_SERVER['SERVER_NAME'], $this->config->getFromCms('Site', 'NAME'));

        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS `c`.*, `cp`.`category_name` `pcategory_name`, `cp`.`category_left` `pcategory_left` '
          . 'FROM `site_categories` `c`, `site_categories` `cp` '
          . 'WHERE `c`.`category_left` > `cp`.`category_left` AND `cp`.`category_right` > `c`.`category_right` AND '
          . '`cp`.`category_level` = `c`.`category_level`-1 ';
        // фильтр из панели инструментов
        $filter = $this->store->get('tlbFilter', false);
        if ($this->isOutControl) {
            if ($filter) {
                $domainId = (int) $filter['domain'];
                if ($domainId >= 0)
                    $this->sql .= ' AND `c`.`domain_id`=' . $domainId;
            }
        }
        $this->sql .= '%filter ORDER BY %sort LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // название сайта
            'domain_id' => array('type' => 'string'),
            // название
            'category_name' => array('type' => 'string', 'as' => 'c.category_name'),
            'category_title' => array('type' => 'string'),
            // название предка
            'pcategory_name' => array('type' => 'string', 'as' => 'cp.category_name'),
            // показывать
            'category_visible' => array('type' => 'integer'),
            // адрес
            'category_uri' => array('type' => 'string', 'as' => 'c.category_uri'),
            'category_url' => array('type' => 'string'),
            'goto_url' => array('type' => 'string'),
            // атрибуты дерева
            'pcategory_left' => array('type' => 'integer'),
            'category_level' => array('type' => 'integer'),
            'category_left' => array('type' => 'integer'),
            'category_right' => array('type' => 'integer'),
            // задействован список
            'list' => array('type' => 'integer'),
            // индексация
            'page_meta_robots' => array('type' => 'string'),
            'page_meta_robots_s' => array('type' => 'string')
        );
    }

    /**
     * Возращает поле сортировик
     * 
     * @return string
     */
    protected function getSortField()
    {
        $s = parent::getSortField();
        if ($s != 'category_left')
            $s = 'category_left ASC ,' . $s;

        return $s;
    }

    /**
     * Удаление данных
     * 
     * @return void
     */
    protected function dataDelete()
    {
        parent::dataAccessDelete();

        // проверка существования зависимых записей
        $this->isDataDependent();
        // определяем откуда id брать
        if ($this->uri->id)
            $id = $this->uri->id;
        else {
            $input = GFactory::getInput();
            $id = $input->get('id');
        }
        // если не указан $id
        if (empty($id))
            throw new GException('Error', 'Unable to delete records!');

        $query = new GDb_Query();
        // выбранная запись
        $sql = 'SELECT * FROM `site_categories` WHERE `category_id`=' . $id;
        if (($node = $query->getRecord($sql)) === false)
            throw new GSqlException();

        // обновление всех статей
        $sql = 'UPDATE `site_articles` `a`, (SELECT `category_id` FROM `site_categories` '
             . 'WHERE `category_left`>=' . $node['category_left'] . ' AND `category_right`<=' . $node['category_right'] . ') `c` '
             . 'SET `a`.`category_id`=NULL WHERE `a`.`category_id`=`c`.`category_id`';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        Gear::library('Db/Drivers/MySQL/Tree/Tree');

        // указатель на базу данных
        $db = GFactory::getDb();
        $tree = new GDb_Tree($this->tableName, $this->fieldPrefix, $db->getHandle());
        // удаление ветвей дерева
        $tree->DeleteAll($id, '');
        if (!empty($tree->ERRORS_MES)) {
            $error = implode(',', $tree->ERRORS_MES);
            throw new GException('Data processing error', 'Query error (Internal Server Error)', $error);
        }
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        $query = new GDb_Query();
        // обновление всех статей
        $sql = 'UPDATE `site_articles` `a` SET `category_id`=NULL';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление записей таблицы "site_categories"
        if ($query->clear('site_categories') === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_categories') === false)
            throw new GSqlException();

        // cброс данных к первоначальному виду
        $table = new GDb_Table('site_categories', 'category_id');
        $table->insert(
            array('category_id'     => 1,
                  'category_left'   => 1,
                  'category_right'  => 2,
                  'category_level'  => 1,
                  'category_name'   => $this->_['text_root_category'],
                  'sys_user_insert' => 1,
                  'sys_record'      => 1,
                  'sys_date_insert' => date('Y-m-d'),
                  'sys_time_insert' => date('H:i:s'))
        );
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        $domain = $_SERVER['SERVER_NAME'];
        // сайты на других доменах
        if (isset($this->domains[$record['domain_id']])) {
            $domain = $this->domains[$record['domain_id']][0];
            $record['domain_id'] = $this->domains[$record['domain_id']][1];
        } else
            $record['domain_id'] = '';

        $isParent = ($record['category_right'] - $record['category_left']) > 1;

        $record['category_title'] = $record['category_name'];
        $record['category_name'] = '<span' . ($isParent ? ' class="mn-tree-row-collapsed"' : '') . ' style="padding-left:' . (($record['category_level'] - 1) * 11) . 'px"></span>' . $record['category_name'];
        if ($record['category_left'] - 2 < 2)
            $record['category_step'] = 1;
        else
            $record['category_step'] = ($record['category_left'] - 2) / 2;
        // если категории не отображаются
        if (empty($record['category_visible']))
            $record['rowCls'] = 'mn-row-notpublished';
        // индексация страницы сайта
        if ($record['list']) {
            if (!empty($record['page_meta_robots'])) {
                $index = $record['page_meta_robots'];
                if ($index == 'index, follow' || $index == 'index, nofollow' || $index == 'all')
                    $icon = 'green';
                else
                    $icon = 'red';
                $record['page_meta_robots_s'] = $record['page_meta_robots'];
                $record['page_meta_robots'] = '<img src="' . $this->resourcePath . 'icon-index-' . $icon . '.png" align="absmiddle"> ' . $index;
            }
        } else
            $record['page_meta_robots'] = '';
        // подсказки для ЧПУ URL категорий
        $url = 'http://' . $domain . '/';
        if ($record['category_uri'] != '/')
            $url .= $record['category_uri'];
        $record['category_url'] = $url;
        if ($record['domain_id'])
            $record['goto_url'] = '<a href="'. $url . '" target="_blank" title="' . $this->_['tooltip_goto_url'] . '"><img src="' . $this->resourcePath . 'icon-goto.png"></a>';
        else
            $record['goto_url'] = '';

        return $record;
    }
}
?>