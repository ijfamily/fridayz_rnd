<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'    => 'Категории статей',
    'tooltip_grid'  => 'список категорий статей сайта',
    'rowmenu_edit'  => 'Редактировать',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">("Категории статей")</span> ?',
    // столбцы
    'header_site_name'         => 'Сайт',
    'header_category_name'     => 'Название',
    'header_pcategory_name'    => 'Название "предка"',
    'header_category_brd'      => 'В цепочке',
    'header_category_uri'      => 'ЧПУ URL категории',
    'tooltip_list'             => 'Задействован список в категории',
    'tooltip_category_visible' => 'Показывать категорию',
    'text_root_category'       => 'Основная категория',
    'header_robots'            => 'Индексация',
    'tooltip_robots'           => 'Мета &quot;robots&quot; на странице сайта (используется если в категории задействован список)',
    'label_domain'             => 'Сайт',
    'tooltip_goto_url'         => 'Просмотр категории сайта',
    // тип данных
    'data_boolean' => array('нет', 'да')
);
?>