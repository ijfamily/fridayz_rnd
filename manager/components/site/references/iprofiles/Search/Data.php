<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск профилей изображений"
 * Пакет контроллеров "Профиля изображений"
 * Группа пакетов     "Справочники"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SRImageProfiles_Search
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * @see GController_Search_Data
 */
require_once('Gear/Controllers/Search/Data.php');

/**
 * Категорий изображений
 * 
 * @category   Gear
 * @package    GController_SRImageProfiles_Search
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SRImageProfiles_Search_Data extends GController_Search_Data
{
    /**
     * Идентификатор компонента
     *
     * @var string
     */
    public $gridId = 'gcontroller_srimageprofiles_grid';

    /**
     * Конструктор
     * 
     * @return void
     */
    public function __construct()
    {
        // идентификатор доступа к компоненту
        self::$_accessId = 'gcontroller_srimageprofiles_grid';

        parent::__construct();
    }

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор
     * Используется для инициализации атрибутов контроллер не переданного в конструктор
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'profile_name', 'label' => $this->_['header_profile_name']),
        );
    }
}
?>