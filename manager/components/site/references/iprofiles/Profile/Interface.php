<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля изображения"
 * Пакет контроллеров "Профиль изображений"
 * Группа пакетов     "Справочники"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SRImageProfiles_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля изображений
 * 
 * @category   Gear
 * @package    GController_SRImageProfiles_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SRImageProfiles_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'titleEllipsis' => 45,
                  'gridId'        => 'gcontroller_srimageprofiles_grid',
                  'width'         => 420,
                  'autoHeight'    => true,
                  'stateful'      => false)
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype'      => 'textarea',
                  'fieldLabel' => $this->_['label_profile_name'],
                  'name'       => 'profile_name',
                  'anchor'     => '100%',
                  'height'     => 60,
                  'allowBlank' => false),
            array('xtype'      => 'textarea',
                  'fieldLabel' => $this->_['label_profile_action'],
                  'name'       => 'profile_action',
                  'anchor'     => '100%',
                  'height'     => 60,
                  'allowBlank' => false)
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($items);
        $form->url = $this->componentUrl . 'profile/';

        parent::getInterface();
    }
}
?>