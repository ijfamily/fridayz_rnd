<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Профили изображений"
 * Группа пакетов     "Справочники"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SRImageProfiles
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Профили изображений
 * Описание: Справочники профилей изображений
 * Меню:
 * ID класса: gcontroller_srimageprofiles_grid
 * Модуль: Сайт
 * Группа: Сайт / Справочники
 * Очищать: нет
 * Ресурс
 *    Компонент: site/references/iprofiles/
 *    Контроллер: site/references/iprofiles/Grid/
 *    Интерфейс: grid/interface/
 *    Меню:
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске компонентов: нет
 *    В главном меню: нет
 */

return array(
    // {S}- модуль "Site" {R} - пакет контроллеров "Images profiles" -> SRImageProfiles
    'clsPrefix' => 'SRImageProfiles',
    // выбрать базу данных из настроек конфига
    'conName'   => 'site',
    // использовать язык
    'language'  => 'ru-RU'
);
?>