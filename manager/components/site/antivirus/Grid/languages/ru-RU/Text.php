<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Антивирус',
    'tooltip_grid' => 'проверка папок и файлов скрипта на наличие подозрительных файлов, а также отслеживание несанкционированных изменений в файлах',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'text_btn_snapshot'    => 'Снимок',
    'tooltip_btn_snapshot' => 'Создание снимка файлов',
    // столбцы
    'header_dirname'  => 'Каталог',
    'header_basename' => 'Имя файла',
    'header_perms'    => 'Права доступа',
    'header_size'     => 'Размер',
    'header_date'     => 'Дата'
);
?>