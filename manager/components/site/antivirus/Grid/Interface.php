<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка"
 * Пакет контроллеров "Антивирус"
 * Группа пакетов     "Сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SAntivirus_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс списка
 * 
 * @category   Gear
 * @package    GController_SAntivirus_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SAntivirus_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('name' => 'dirname', 'type' => 'string'),
            array('name' => 'icon', 'type' => 'string'),
            array('name' => 'basename', 'type' => 'string'),
            array('name' => 'perms', 'type' => 'string'),
            array('name' => 'size', 'type' => 'string'),
            array('name' => 'date', 'type' => 'string'),
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $this->columns = array(
            array('xtype'     => 'numbercolumn'),
            array('dataIndex' => 'dirname',
                  'header'    => $this->_['header_dirname'],
                  'align'     => 'right',
                  'width'     => 400),
            array('dataIndex' => 'icon',
                  'align'     => 'center',
                  'header'    => '&nbsp;',
                  'fixed'     => true,
                  'hideable'  => false,
                  'width'     => 25,
                  'sortable'  => false,
                  'menuDisabled' => true),
            array('dataIndex' => 'basename',
                  'header'    => '<em class="mn-grid-hd-details"></em>' . $this->_['header_basename'],
                  'width'     => 200),
            array('dataIndex' => 'perms',
                  'header'    => $this->_['header_perms'],
                  'width'     => 130),
            array('dataIndex' => 'size',
                  'header'    => $this->_['header_size'],
                  'width'     => 130),
            array('dataIndex' => 'date',
                  'header'    => $this->_['header_date'],
                  'width'     => 130),
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_grid'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_grid'],
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'profileUrl'    => $this->componentUrl . 'profile/')
        );

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "edit" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' => $this->classId));
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'   => 'mn-btn-widget',
                  'width'   => 60,
                  'text'    => $this->_['text_btn_snapshot'],
                  'tooltip' => $this->_['tooltip_btn_snapshot'],
                  'icon'    => $this->resourcePath . 'icon-btn-snapshot.png',
                  'url'     => $this->componentUri . ROUTER_DELIMITER . 'snapshot/interface/')
        );
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 2)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = 'component-site-antivirus';
        $this->_cmp->tbar->items->add($group);

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $cellInfo =
            '<div class="mn-grid-cell-tooltip-tl">{basename}</div>'
          . '<div class="mn-grid-cell-tooltip">'
          . '<em>' . $this->_['header_dirname'] . '</em>: <b>{dirname}</b><br>'
          . '<em>' . $this->_['header_perms'] . '</em>: <b>{perms}</b><br>'
          . '<em>' . $this->_['header_size'] . '</em>: <b>{size}</b><br>'
          . '<em>' . $this->_['header_date'] . '</em>: <b>{date}</b><br>'
          . '</div>';
        $this->_cmp->cellTips = array(
            array('field' => 'dirname', 'tpl' => '{dirname}'),
            array('field' => 'basename', 'tpl' => $cellInfo),
            array('field' => 'perms', 'tpl' => '{perms}'),
        );
        $this->_cmp->progressLoad = true;

        parent::getInterface();
    }
}
?>