<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка"
 * Пакет контроллеров "Антивирус"
 * Группа пакетов     "Сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SAntivirus_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('File');
Gear::controller('Grid/Data');

/**
 * Данные списка
 * 
 * @category   Gear
 * @package    GController_SAntivirus_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SAntivirus_Grid_Data extends GController_Grid_Data
{
    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        parent::dataAccessView();

        GSnapshot::fromFile($this->componentPath . 'Snapshot/resources/data/');
        $items = GSnapshot::getList();

        foreach ($items as $index => $item) {
            if (GSnapshot::check($item['filename'])) continue;

            // права доступа
            $perms = fileperms($item['filename']);
            if ($perms !== false)
                $perms = GFile::filePermsDigit($perms) . ' ' . GFile::filePermsInfo($perms);
            else
                $perms = '';
            // размер файла
            if ($item['is-dir'])
                $size = '';
            else
                $size = GFile::getFileSize($item['filename']);
            // дата правки
            $date = filemtime($item['filename']);
            if ($date)
                $date = date("d-m-Y H:i:s", $date);
            else
                $date = '';
            if ($item['is-dir']) {
                $item['basename'] = '';
                $item['icon'] = '<img src="' . $this->resourcePath . 'icon-folder.png">';
            } else {
                $item['icon'] = '<img src="' . $this->resourcePath . 'icon-document.png">';
            }
            $item['dirname'] = str_replace(array('/'), ' <span style="color:#898989">&raquo;</span> ', $item['dirname']);

            $this->_data[] = array(
                'dirname'  => mb_convert_encoding($item['dirname'], 'UTF-8', 'WINDOWS-1251'),
                'basename' => mb_convert_encoding($item['basename'], 'UTF-8', 'WINDOWS-1251'),
                'icon'     => $item['icon'],
                'perms'    => $perms,
                'size'     => $size,
                'date'     => $date
            );
        }
        $this->response->setMsgResult('ddd',
                                      'dddddddddddds',
                                      true);


        //$this->response->success = true;
        $this->response->data = $this->_data;
    }
}
?>