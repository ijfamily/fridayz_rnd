<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Антивирус"
 * Группа пакетов     "Антивирус"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SAntivirus
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: setttings.php 2016-01-01 12:00:00 Gear Magic $
 */


/**
 * Класс обработки снимков файлов
 * 
 * @category   Gear
 * @package    GController_SAntivirus
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: setttings.php 2016-01-01 12:00:00 Gear Magic $
 */
class GSnapshot
{
    /**
     * Название файла снимка
     *
     * @var string
     */
    public static $filename = 'snapshot.dat';

    /**
     * Ключ для формирования снимка
     *
     * @var string
     */
    public static $key = '5b19114dde4c9c4cf9fbab2de36a51e9';

    /**
     * Список полученнй из fromFile, toFile
     *
     * @var array
     */
    public static $data = array();

    /**
     * Проверка существования снимка
     * 
     * @return boolean
     */
    public static function check($filename)
    {
        $code = md5(self::$key . $filename);

        return isset(self::$data[$code]);
    }

    /**
     * Чтение снимка из файла
     * 
     * @return mixed
     */
    public static function fromFile($path)
    {
        try {
            // если файл не существует
            if (!file_exists($path . self::$filename))
                throw new GException('Data processing error', 'File "%s" does not exist', $path . self::$filename);
            // не возможно прочитать файл
            if (($text = file_get_contents($path . self::$filename, true)) === false)
                throw new GException('Data processing error', 'Can`t open file "%s" for reading', $path . self::$filename);
            // unserialize
            if (($data = unserialize($text)) === false)
                throw new GException('Data processing error', 'Can`t open file "%s" for reading', $path . self::$filename . ' (unserialize)');
        } catch (GException $e) {}

        return self::$data = $data;
    }

    /**
     * Запись снимка в файл
     * 
     * @return mixed
     */
    public static function toFile($path)
    {
        try {
            // если каталог не существует
            if (!file_exists($path)) {
                if (!mkdir($path))
                    throw new GException('Data processing error', 'The directory "%s" can not be opened', $path);
            }

            $items = self::getList();
    
            foreach ($items as $index => $item) {
                self::$data[md5(self::$key . $item['filename'])] = true;
            }

            if (file_put_contents($path . self::$filename, serialize(self::$data), FILE_TEXT) === false)
                throw new GException('Data processing error', 'Could not write the file to disk');
        } catch (GException $e) {}

        return self::$data;
    }

    /**
     * Возращает список файлов
     * 
     * @return array
     */
    public static function getList()
    {
        $items = array();

        GDir::getListFiles($items, DOCUMENT_ROOT, false);
        GDir::getListFiles($items, DOCUMENT_ROOT . 'application/', true);
        GDir::getListFiles($items, DOCUMENT_ROOT . 'manager/', true, array(DOCUMENT_ROOT . 'manager/data/'));
        GDir::getListFiles($items, DOCUMENT_ROOT . 'data/', false);

        return $items;
    }
}


/** 
 * Установка компонента
 * 
 * Название: Антивирус
 * Описание: Проверка папок и файлов скрипта на наличие подозрительных файлов, а также отслеживание несанкционированных изменений в файлах
 * Меню: Антивирус
 * ID класса: gcontroller_santivirus_grid
 * Модуль: Сайт
 * Группа: Сайт
 * Очищать: нет
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске компонентов: нет
 *    В главном меню: нет
 * Ресурс
 *    Компонент: site/antivirus/
 *    Контроллер: site/antivirus/Grid/
 *    Интерфейс: grid/interface/
 *    Меню:
 */

return array(
    // {S}- модуль "Site" {} - пакет контроллеров "Antivirus" -> SAntivirus
    'clsPrefix' => 'SAntivirus',
    // выбрать базу данных из настроек конфига
    'conName'   => 'site',
    // использовать язык
    'language'  => 'ru-RU'
);
?>