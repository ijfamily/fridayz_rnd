<?php
/**
 * Gear Manager
 *
 * Контроллер         "Создание снимка файлов"
 * Пакет контроллеров "Снимок файлов"
 * Группа пакетов     "Антивирус"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SAntivirus_Profile
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('File');
Gear::controller('Profile/Data');

/**
 * Создание снимка файлов
 * 
 * @category   Gear
 * @package    GController_SAntivirus_Profile
 * @subpackage Data
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SAntivirus_Snapshot_Data extends GController_Profile_Data
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_santivirus_grid';

    /**
     * Вставка данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataInsert($params = array())
    {
        $this->dataAccessInsert();

        GSnapshot::toFile($this->resourcePath . 'data/');

        $size = GFile::getFileSize($this->resourcePath . 'data/' . GSnapshot::$filename);

        $this->response->set('action', 'snapshot');
        $this->response->setMsgResult($this->_['title_dlg'],
                                      sprintf($this->_['msg_dlg'], sizeof(GSnapshot::$data), $size),
                                      true);

        // запись в журнал действий пользователя
        $this->logInsert(
            array('log_query'        => 'snapshot',
                  'log_error'        => null,
                  'log_query_params' => null)
        );

        $this->dataInsertComplete($params);
    }
}
?>