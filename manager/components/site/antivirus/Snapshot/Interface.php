<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля снимка"
 * Пакет контроллеров "Снимок файлов"
 * Группа пакетов     "Антивирус"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SAntivirus_Profile
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('File');
Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля снимка
 * 
 * @category   Gear
 * @package    GController_SAntivirus_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SAntivirus_Snapshot_Interface extends GController_Profile_Interface
{
    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        $filename = $this->resourcePath . 'data/' . GSnapshot::$filename;
        if (!file_exists($filename))
            return false;
        // размер файла
        $size = GFile::getFileSize($filename);
        // дата правки
        $date = filemtime($filename);
        if ($date)
            $date = date("d-m-Y H:i:s", $date);
        // права доступа
        $perms = fileperms($filename);
        if ($perms !== false)
            $perms = GFile::filePermsDigit($perms) . ' ' . GFile::filePermsInfo($perms);
        else
            $perms = '';

        return array('key' => GSnapshot::$key, 'name' => $filename, 'date' => $date, 'size' => $size, 'perms' => $perms);
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('id' => $this->classId,
                  'title'         => $this->_['title_profile_insert'],
                  'titleEllipsis' => 45,
                  'gridId'        => 'gcontroller_santivirus_grid',
                  'width'         => 470,
                  'autoHeight'    => true,
                  'state'         => 'none',
                  'stateful'      => false,
                  'buttonsAdd'    => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-site-antivirus',
                            'iconCls' => 'icon-item-info'),
                      array('xtype'       => 'mn-btn-form-update',
                            'text'        => $this->_['text_btn_snapshot'],
                            'icon'        => $this->resourcePath . 'icon-btn-snapshot.png',
                            'windowId'    => $this->classId,
                            'width'       => 97,
                            'closeWindow' => false)
                  )
            )
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype' => 'label', 'html' => $this->_['html_desc'])
        );
        if ($data) {
            $items[] = array(
                'xtype'      => 'fieldset',
                'title'      => $this->_['title_fs_snapshot'],
                'autoHeight' => true,
                'labelWidth' => 70,
                'defaults'   => array('xtype' => 'displayfield', 'fieldClass' => 'mn-field-value-info'),
                'items'      => array(
                    array('fieldLabel' => $this->_['label_key'], 'value' => $data['key']),
                    array('fieldLabel' => $this->_['label_name'], 'value' => $data['name']),
                    array('fieldLabel' => $this->_['label_date'], 'value' => $data['date']),
                    array('fieldLabel' => $this->_['label_size'], 'value' => $data['size']),
                    array('fieldLabel' => $this->_['label_perms'], 'value' => $data['perms'])
                )
            );
        } else {
            $items[] = array(
                'xtype'      => 'fieldset',
                'title'      => $this->_['title_fs_snapshot'],
                'autoHeight' => true,
                'labelWidth' => 70,
                'items'      => array(
                    array('xtype' =>'label', 'html' => $this->_['html_none']),
                )
            );
        }

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($items);
        $form->url = $this->componentUrl . 'snapshot/';
        $form->labelWidth = 95;

        parent::getInterface();
    }
}
?>