<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2013-2016 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Создание снимка файлов',
    'text_btn_help'        => 'Справка',
    'text_btn_snapshot'    => 'Сделать снимок ',
    'title_dlg'            =>'Cнимок файлов',
    'msg_dlg'              =>'Успешно создан cнимок файлов!<br><br>количество файлов: <b>%s</b> шт.<br>размер снимка: <b>%s</b>',
    // поля формы
    'title_fs_snapshot'    => 'Последний снимок',
    'html_desc'   => '<note>Для отслеживание посторонних изменений в файлах системы, необходимо создать снимок файлов (список всех файлов системы на текущей момент) </note>',
    'html_none'   => '<note>еще не разу не было сделано снимков</note>',
    'label_name'  => 'Название',
    'label_date'  => 'Дата',
    'label_size'  => 'Размер',
    'label_perms' => 'Доступ',
    'label_key'   => 'Ключ'
);
?>