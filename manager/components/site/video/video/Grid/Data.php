<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные интерфейса списка видеозаписей"
 * Пакет контроллеров "Список видеозаписей"
 * Группа пакетов     "Видеозаписи"
 * Модуль             "Сайт"
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SVideo_Grid
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные интерфейса списка видеозаписей
 * 
 * @category   Gear
 * @package    GController_SVideo_Grid
 * @subpackage Data
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SVideo_Grid_Data extends GController_Grid_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'video_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_video';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_svideo_grid';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql =
            'SELECT SQL_CALC_FOUND_ROWS `v`.*, `c`.`category_name` FROM `site_video` `v` '
          . 'LEFT JOIN `site_categories` `c` USING (`category_id`) '
          . 'WHERE 1 %filter ORDER BY %sort LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // тип видеозаписи
            'video_type' => array('type' => 'string'),
            // код видеозаписи
            'video_code' => array('type' => 'string'),
            // название
            'video_title' => array('type' => 'string'),
            // описание
            'video_text' => array('type' => 'string'),
            // размер
            'video_width' => array('type' => 'string'),
            'video_height' => array('type' => 'string'),
            'video_size' => array('type' => 'string'),
            // обложка
            'video_thumbnail' => array('type' => 'string'),
            // длительность
            'video_duration' => array('type' => 'string'),
            // теги
            'video_tags' => array('type' => 'string'),
            // дата публикации
            'published_date' => array('type' => 'string'),
            'published_time' => array('type' => 'string'),
            'published_user' => array('type' => 'integer'),
            // видеозапись опубликована
            'published' => array('type' => 'integer'),
            // переход по ссылке
            'goto_url' => array('type' => 'string'),
            // название категории
            'category_name' => array('type' => 'string')
        );
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        $query = new GDb_Query();
        // удаление изображений
        $sql = 'DELETE FROM `site_video`';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_video') === false)
            throw new GSqlException();
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Событие наступает после успешного удаления данных
     * 
     * @return void
     */
    protected function dataDeleteComplete()
    {
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }


    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        $v = $this->settings['video'];
        if (isset($v[$record['video_type']])) {
            $vr = $v[$record['video_type']];
            // переход на страницу
            $record['goto_url'] = '<a href="' . sprintf($vr['page'], $record['video_code']) . '" target="_blank" title="' . $this->_['tooltip_goto_url'] . '"><img src="' . $this->resourcePath . 'icon-goto.png"></a>';
            // видеохостинг
            $record['video_type'] = '<span style="color:' . $vr['color'] . '">'
                                . '<img style="margin-right:5px;" src="' . $this->resourcePath . $vr['icon'] . '" align="absmiddle">'
                                . '<a href="' . $vr['url'] . '" target="_blank" style="color:' . $vr['color'] . '">' . $record['video_type'] . '</a></span>';
        }
        // размер видео
        $record['video_size'] = $record['video_width'] . ' x ' . $record['video_height'];

        return $record;
    }
}
?>