<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка видеозаписей"
 * Пакет контроллеров "Список видеозаписей"
 * Группа пакетов     "Видеозаписи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SVideo_Grid
 * @copyright  Copyright (c) 2013-2016 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс списка видеозаписей
 * 
 * @category   Gear
 * @package    GController_SVideo_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SVideo_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'video_id';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_svideo_grid';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'video_id';

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // тип видеозаписи
            array('name' => 'video_type', 'type' => 'string'),
            // код видеозаписи
            array('name' => 'video_code', 'type' => 'string'),
            // название
            array('name' => 'video_title', 'type' => 'string'),
            // описание
            array('name' => 'video_text', 'type' => 'string'),
            // размер
            array('name' => 'video_size', 'type' => 'string'),
            // обложка
            array('name' => 'video_thumbnail', 'type' => 'string'),
            // длительность
            array('name' => 'video_duration', 'type' => 'string'),
            // теги
            array('name' => 'video_tags', 'type' => 'string'),
            // дата публикации
            array('name' => 'published_date', 'type' => 'string'),
            array('name' => 'published_time', 'type' => 'string'),
            array('name' => 'published_user', 'type' => 'integer'),
            // видеозапись опубликована
            array('name' => 'published', 'type' => 'integer'),
            // переход по ссылке
            array('name' => 'goto_url', 'type' => 'string'),
            // название категории
            array('name' => 'category_name', 'type' => 'string')
        );

        $settings = $this->session->get('user/settings');
        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $this->columns = array(
            array('xtype'     => 'expandercolumn',
                  'url'       => $this->componentUrl . 'grid/expand/',
                  'typeCt'    => 'row'),
            array('xtype'     => 'numbercolumn'),
            array('xtype'     => 'rowmenu'),
            array('xtype'     => 'datetimecolumn',
                  'dataIndex' => 'published_date',
                  'timeIndex' => 'published_time',
                  'userIndex' => 'published_user',
                  'frmDate'   => $settings['format/date'],
                  'frmTime'   => $settings['format/time'],
                  'header'    => $this->_['header_published'],
                  'isSystem'  => true,
                  'urlQuery'  => '?state=info',
                  'url'       => 'administration/users/contingent/' . ROUTER_DELIMITER . 'profile/interface/',
                  'width'     => 130,
                  'sortable'  => true,
                  'filter'    => array('type' => 'date')),
            array('dataIndex' => 'video_type',
                  'header'    => $this->_['header_video_type'],
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'video_title',
                  'header'    => '<em class="mn-grid-hd-details"></em>' . $this->_['header_video_name'],
                  'width'     => 250,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'video_text',
                  'header'    => $this->_['header_video_description'],
                  'width'     => 120,
                  'hidden'    => true,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'video_duration',
                  'header'    => '<img align="absmiddle" src="' . $this->resourcePath . 'icon-hd-time.png"> ' . $this->_['header_video_duration'],
                  'tooltip'   => $this->_['tooltip_video_duration'],
                  'width'     => 80,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'video_size',
                  'header'    => $this->_['header_video_size'],
                  'width'     => 100,
                  'sortable'  => false/*,
                  'filter'    => array('disabled' => true)*/),
            array('dataIndex' => 'video_tags',
                  'header'    => $this->_['header_video_tags'],
                  'width'     => 100,
                  'hidden'    => true,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'goto_url',
                  'align'     => 'center',
                  'header'    => '&nbsp;',
                  'tooltip'   => $this->_['tooltip_goto_url'],
                  'fixed'     => true,
                  'hideable'  => false,
                  'width'     => 25,
                  'sortable'  => false,
                  'menuDisabled' => true),
            array('dataIndex' => 'category_name',
                  'header'    => $this->_['header_category_name'],
                  'tooltip'   => $this->_['tooltip_category_name'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('xtype'     => 'booleancolumn',
                  'align'     => 'center',
                  'cls'       => 'mn-bg-color-gray2',
                  'dataIndex' => 'published',
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-visible.png" align="absmiddle">',
                  'width'     => 40,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean'))
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_grid'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_grid'],
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'profileUrl'    => $this->componentUrl . 'profile/')
        );

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "edit" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "добавить" (ExtJS class "Manager.button.InsertData")
        $group->items->add(array('xtype' => 'mn-btn-insert-data', 'gridId' => $this->classId));
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' => $this->classId));
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'      => 'mn-btn-clear-data',
                  'msgConfirm' => $this->_['msg_btn_clear'],
                  'gridId'     => $this->classId,
                  'isN'        => true)
        );
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = 'component-site-video';
        $this->_cmp->tbar->items->add($group);

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $cellView =
            '<div class="mn-grid-cell-tooltip-tl">{video_title}</div>'
          . '<div class="mn-grid-cell-tooltip">'
          . '<em>' . $this->_['header_category_name'] . '</em>: <b>{category_name}</b><br>'
          . '<div><em>' . $this->_['header_video_type'] . '</em>: <b>{video_type}</b></div>'
          . '<div><em>' . $this->_['header_video_size'] . '</em>: <b>{video_size}</b></div>'
          . '<div><em>' . $this->_['label_video_duration'] . '</em>: <b>{video_duration}</b></div>'
          . '<em>' . $this->_['header_video_description'] . '</em>: <b>{video_text}</b><br>'
          . '<em>' . $this->_['header_video_tags'] . '</em>: <b>{video_tags}</b>'
          . '</div>';
        $this->_cmp->cellTips = array(
            array('field' => 'video_type', 'tpl' => '{video_type}'),
            array('field' => 'video_title', 'tpl' => $cellView),
            array('field' => 'video_text', 'tpl' => '{video_text}'),
            array('field' => 'video_tags', 'tpl' => '{video_tags}'),
            array('field' => 'category_name', 'tpl' => '{category_name}'),
            array('field' => 'video_size', 'tpl' => '{video_size}')
        );

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $items = array(
            array('text'    => $this->_['rowmenu_edit'],
                  'iconCls' => 'icon-form-edit',
                  'url'     => $this->componentUrl . 'profile/interface/'),
        );
        $this->_cmp->rowMenu = array('xtype' => 'mn-grid-rowmenu', 'items' => $items);

        parent::getInterface();
    }
}
?>