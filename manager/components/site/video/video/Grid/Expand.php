<?php
/**
 * Gear Manager
 *
 * Контроллер         "Развёрнутая запись видеозаписи"
 * Пакет контроллеров "Список видеозаписей"
 * Группа пакетов     "Видеозаписи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SVideo_Grid
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Expand');

/**
 * Развёрнутая запись видеозаписи
 * 
 * @category   Gear
 * @package    GController_SVideo_Grid
 * @subpackage Expand
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SVideo_Grid_Expand extends GController_Grid_Expand
{
    /**
     * Вывод данных в интерфейс
     * 
     * @return void
     */
    protected function dataExpand()
    {
        parent::dataAccessView();

        $data = $this->getAttributes();
        $this->response->data = $data;
    }

    /**
     * Возращает атрибуты записи
     * 
     * @return string
     */
    protected function getAttributes()
    {
        $data = '';
        $query = new GDb_Query();
        // видеозапись
        $sql = 'SELECT * FROM `site_video` WHERE `video_id`=' . $this->uri->id;
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        // категория
        $cat = false;
        if ($rec['category_id']) {
            $sql = 'SELECT * FROM `site_categories` WHERE `category_id`=' . $rec['category_id'];
            if (($cat = $query->getRecord($sql)) === false)
                throw new GSqlException();
            if (empty($cat['category_id']))
                $cat = false;
        }
        // шаблон
        $data = '<fieldset class="fixed"><label>' . $this->_['title_fieldset_common'] . '</label><ul>';
        if ($cat)
            $data .= '<li><label>' . $this->_['label_video_category'] . ':</label> ' . $cat['category_name'] . '</li>';
        $data .= '<li><label>' . $this->_['label_video_code'] . ':</label> ' . $rec['video_code'] . '</li>';
        $data .= '<li><label>' . $this->_['label_video_name'] . ':</label> ' . $rec['video_title'] . '</li>';
        if ($rec['video_text'])
            $data .= '<li><label>' . $this->_['label_video_text'] . ':</label> ' . $rec['video_text'] . '</li>';
        if ($rec['video_tags'])
            $data .= '<li><label>' . $this->_['label_video_tags'] . ':</label> ' . $rec['video_tags'] . '</li>';
        $data .= '<li><label>' . $this->_['label_video_type'] . ':</label> ' . $rec['video_type'] . '</li>';
        $data .= '<li><label>' . $this->_['label_video_size'] . ':</label> ' . $rec['video_width'] . 'x' . $rec['video_height'] . '</li>';
        $data .= '<li><label>' . $this->_['label_video_duration'] . ':</label> ' . $rec['video_duration'] . '</li>';
        $data .= '<li><label>' . $this->_['label_video_thumbnail'] . ':</label> ' . $rec['video_thumbnail'] . '</li>';
        if ($rec['video_uploaded'])
            $data .= '<li><label>' . $this->_['label_video_uploaded'] . ':</label> ' . date('d-m-Y', strtotime($rec['video_uploaded'])) . '</li>';
        if ($rec['published_date'])
            $data .= '<li><label>' . $this->_['label_published_date'] . ':</label> ' . date('d-m-Y', strtotime($rec['published_date'])) . ' ' . $rec['published_time'] . '</li>';
        $data .= '<li><label>' . $this->_['label_published'] . ':</label> ' . $this->_['data_boolean'][(int) $rec['published']] . '</li>';
        $data .= '<li><label>' . $this->_['label_video_frame'] . ':</label> ' . $this->_['data_boolean'][(int) $rec['video_frame']] . '</li>';
        $data .= '</ul></fieldset>';
        $v = $this->settings['video'];
        if (isset($v[$rec['video_type']])) {
            $vr = $v[$rec['video_type']];
            $data .= '<div class="wrap"></div><div>';
            $data .= str_replace('%s', $rec['video_code'], $vr['frame']);
            $data .= '</div>';
        }
        $data = '<div class="mn-row-body border">' . $data . '<div class="wrap"></div></div>';

        return $data;
    }
}
?>