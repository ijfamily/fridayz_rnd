<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2013-2016 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Видеозаписи',
    'rowmenu_edit' => 'Редактировать',
    'tooltip_grid' => 'это Ваши видеозаписи, расположенные на видеохостинге Youtube, Vimeo и др.',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">("Видеозаписи")</span> ?',
    // столбцы
    'header_published'          => 'Дата публикации',
    'header_category_name'      => 'Категория',
    'tooltip_category_name'     => 'Категория видеозаписи',
    'header_video_description'  => 'Описание',
    'tooltip_video_description' => 'Описание видеозаписи',
    'header_video_name'         => 'Название',
    'header_video_duration'     => ', сек.',
    'tooltip_video_duration'    => 'Длительность видеозаписи  в секундах',
    'header_video_size'         => 'Размер, пкс.',
    'header_video_tags'         => 'Теги',
    'header_video_type'         => 'Видеохостинг',
    'tooltip_goto_url'          => 'Просмотр страницы с видеозаписью',
    // развёрнутая запись
    'label_video_uploaded' => 'Загружена видеозапись',
    'label_video_code'     => 'Код',
    'label_video_name'     => 'Название',
    'label_video_text'     => 'Описание',
    'label_video_tags'     => 'Теги',
    'label_video_duration' => 'Длительность видеозаписи, сек.',
    'label_video_size'     => 'Размер, пкс.',
    'label_video_type'     => 'Видеохостинг',
    'label_published_date' => 'Дата публикации',
    'label_published'      => 'Опубликована',
    'label_video_frame'    => 'Используется фрейм',
    'label_video_thumbnail' => 'Обложка',
    'label_video_category'  => 'Категория',
    // тип
    'data_boolean' => array('нет', 'да')
);
?>