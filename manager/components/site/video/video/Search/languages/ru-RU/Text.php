<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2013-2016 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Поиск в списке "Видеозаписи"',
    // поля
    'header_category_name'  => 'Категория',
    'header_video_text'     => 'Описание',
    'header_video_name'     => 'Название',
    'header_video_duration' => 'Длительность видеозаписи, сек.',
    'header_video_size'     => 'Размер, пкс.',
    'header_video_tags'     => 'Теги',
    'header_video_type'     => 'Видеохостинг'
);
?>