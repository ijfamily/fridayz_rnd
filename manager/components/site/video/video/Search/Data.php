<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск видеозаписи"
 * Пакет контроллеров "Список видеозаписей"
 * Группа пакетов     "Видеозаписи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SVideo_Search
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск видеозаписи
 * 
 * @category   Gear
 * @package    GController_SVideo_Search
 * @subpackage Interface
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SVideo_Search_Data extends GController_Search_Data
{
    /**
     * дентификатор компонента (списка) к которому применяется поиск
     *
     * @var string
     */
    public $gridId = 'gcontroller_svideo_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_svideo_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор
     * Используется для инициализации атрибутов контроллер не переданного в конструктор
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'category_name', 'label' => $this->_['header_category_name']),
            array('dataIndex' => 'video_text', 'label' => $this->_['header_video_text']),
            array('dataIndex' => 'video_name', 'label' => $this->_['header_video_name']),
            array('dataIndex' => 'video_duration', 'label' => $this->_['header_video_duration']),
            array('dataIndex' => 'video_size', 'label' => $this->_['header_video_size']),
            array('dataIndex' => 'video_tags', 'label' => $this->_['header_video_tags']),
            array('dataIndex' => 'video_type', 'label' => $this->_['header_video_type']),
        );
    }
}
?>