<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля видеозаписи"
 * Пакет контроллеров "Список видеозаписей"
 * Группа пакетов     "Видеозаписи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SVideo_Profile
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля видеозаписи
 * 
 * @category   Gear
 * @package    GController_SVideo_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SVideo_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'id'            => $this->classId,
                  'titleEllipsis' => 80,
                  'gridId'        => 'gcontroller_svideo_grid',
                  'width'         => 650,
                  'autoHeight'    => true,
                  'resizable'     => false,
                  'stateful'      => false,
                  'buttonsAdd'    => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-site-video',
                            'iconCls' => 'icon-item-info'),
                      array('xtype'      => 'mn-btn-senddata',
                            'icon'       => $this->resourcePath . 'icon-video.png',
                            'text'       => $this->_['text_btn_send'],
                            'windowId'   => $this->classId,
                            'url'        => $this->componentUrl . 'profile/info/')
                  ))
        );

        // тип видео
        $videoTypes = array();
        if (!empty($this->settings['video'])) {
            foreach ($this->settings['video'] as $type => $item)
                $videoTypes[] = array($type);
        }
        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
                array('xtype'      => 'textfield',
                      'id'         => 'fldVideoCode',
                      'fieldLabel' => $this->_['label_video_code'],
                      'name'       => 'video_code',
                      'maxLength'  => 255,
                      'width'      => 170,
                      'allowBlank' => false),
                array('xtype'         => 'combo',
                      'fieldLabel'    => $this->_['label_video_type'],
                      'id'            => 'fldVideoType',
                      'name'          => 'video_type',
                      'editable'      => false,
                      'maxLength'     => 100,
                      'width'         => 170,
                      'typeAhead'     => false,
                      'triggerAction' => 'all',
                      'mode'          => 'local',
                      'store'         => array(
                          'xtype'  => 'arraystore',
                          'fields' => array('list'),
                          'data'   => $videoTypes
                      ),
                      'hiddenName'    => 'video_type',
                      'valueField'    => 'list',
                      'displayField'  => 'list',
                      'allowBlank'    => false),
                array('xtype'      => 'textfield',
                      'id'         => 'fldVideoTitle',
                      'fieldLabel' => $this->_['label_video_title'],
                      'name'       => 'video_title',
                      'maxLength'  => 255,
                      'anchor'     => '100%',
                      'allowBlank' => false),
                array('xtype'      => 'textarea',
                      'id'         => 'fldVideoDescription',
                      'fieldLabel' => $this->_['label_video_description'],
                      'name'       => 'video_description',
                      'anchor'     => '100%',
                      'height'      => 40,
                      'allowBlank' => true),
                array('xtype'      => 'textarea',
                      'id'         => 'fldVideoTags',
                      'fieldLabel' => $this->_['label_video_tags'],
                      'name'       => 'video_tags',
                      'anchor'     => '100%',
                      'height'     => 70,
                      'allowBlank' => true),
                array('xtype'      => 'textfield',
                      'id'         => 'fldVideoThumbnail',
                      'fieldLabel' => $this->_['label_video_thumbnail'],
                      'name'       => 'video_thumbnail',
                      'maxLength'  => 255,
                      'anchor'     => '100%',
                      'allowBlank' => false),
                array('xtype'      => 'mn-field-combo-tree',
                      'id'         => 'fldCategory',
                      'fieldLabel' => $this->_['label_category_name'],
                      'labelTip'   => $this->_['tip_category_name'],
                      'width'      => 225,
                      'name'       => 'category_id',
                      'hiddenName' => 'category_id',
                      'resetable'  => false,
                      'treeWidth'  => 520,
                      'treeRoot'   => array('id' => 1, 'expanded' => true, 'expandable' => true),
                      'allowBlank' => false,
                      'store'      => array(
                          'xtype' => 'jsonstore',
                          'url'   => $this->componentUrl . 'combo/nodes/'
                       )
                ),
                array('xtype'      => 'fieldset',
                      'title'      => $this->_['title_fieldset_size'],
                      'labelWidth' => 90,
                      'autoHeight' => true,
                      'layout'     => 'column',
                      'cls'        => 'mn-container-clean',
                      'items'      => array(
                          array('layout'     => 'form',
                                'width'      => 250,
                                'labelWidth' => 90,
                                'items'      => array(
                                      array('xtype'      => 'numberfield',
                                            'id'         => 'fldVideoWidth',
                                            'fieldLabel' => $this->_['label_video_width'],
                                            'name'       => 'video_width',
                                            'maxLength'  => 255,
                                            'width'      => 100,
                                            'allowBlank' => true),
                                      array('xtype'      => 'numberfield',
                                            'id'         => 'fldVideoHeight',
                                            'fieldLabel' => $this->_['label_video_height'],
                                            'name'       => 'video_height',
                                            'maxLength'  => 100,
                                            'width'      => 100,
                                            'allowBlank' => true),
                                )
                          ),
                          array('layout'     => 'form',
                                'labelWidth' => 130,
                                'items'      => array(
                                    array('xtype'      => 'textfield',
                                          'id'         => 'fldVideoDuration',
                                          'fieldLabel' => $this->_['label_video_duration'],
                                          'name'       => 'video_duration',
                                          'maxLength'  => 255,
                                          'width'      => 100,
                                          'allowBlank' => true),
                                    array('xtype'      => 'textfield',
                                          'id'         => 'fldVideoUploaded',
                                          'fieldLabel' => $this->_['label_video_uploaded'],
                                          'name'       => 'video_uploaded',
                                          'maxLength'  => 20,
                                          'width'      => 140)
                                )
                          )
                      )
                ),
                array('xtype'      => 'fieldset',
                      'anchor'     => '100%',
                      'title'      => $this->_['title_fieldset_public'],
                      'autoHeight' => true,
                      'layout'     => 'column',
                      'cls'        => 'mn-container-clean',
                      'items'      => array(
                          array('layout'     => 'form',
                                'width'      => 185,
                                'labelWidth' => 62,
                                'items'      => array(
                                    array('xtype'      => 'datefield',
                                          'itemCls'    => 'mn-form-item-quiet',
                                          'fieldLabel' => $this->_['label_published_date'],
                                          'format'     => 'd-m-Y',
                                          'name'       => 'published_date',
                                          'checkDirty' => false,
                                          'width'      => 95,
                                          'allowBlank' => true)
                                )
                          ),
                          array('layout'     => 'form',
                                'labelWidth' => 55,
                                'items'      => array(
                                    array('xtype'      => 'textfield',
                                          'itemCls'    => 'mn-form-item-quiet',
                                          'fieldLabel' => $this->_['label_published_time'],
                                          'name'       => 'published_time',
                                          'checkDirty' => false,
                                          'maxLength'  => 8,
                                          'width'      => 70,
                                          'allowBlank' => true)
                                )
                          )
                      )
                ),
                array('xtype'      => 'mn-field-chbox',
                      'itemCls'    => 'mn-form-item-quiet',
                      'fieldLabel' => $this->_['label_frame'],
                      'labelTip'   => $this->_['tip_frame'],
                      'default'    => $this->isUpdate ? null : 0,
                      'name'       => 'video_frame'),
                array('xtype'      => 'mn-field-chbox',
                      'itemCls'    => 'mn-form-item-quiet',
                      'fieldLabel' => $this->_['label_published'],
                      'labelTip'   => $this->_['tip_published'],
                      'default'    => $this->isUpdate ? null : 1,
                      'name'       => 'published'),
                array('xtype'   => 'mn-form-image',
                      'id'      => 'fldVideoImage',
                      'cls'     => 'mn-form-image contain',
                      'default' => $this->resourcePath . 'bg-default.png',
                      'anchor'  => '100%',
                      'height'  => 200),
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($items);
        $form->labelWidth = 100;
        $form->url = $this->componentUrl . 'profile/';

        parent::getInterface();
    }
}
?>