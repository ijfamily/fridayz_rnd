<?php
/**
 * Gear Manager
 *
 * Контроллер         "Изменение профиля записи изображения"
 * Пакет контроллеров "Изображения альбома"
 * Группа пакетов     "Альбомы"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SVideo_Profile
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Field.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Field');

/**
 * Изменение профиля записи галереи
 * 
 * @category   Gear
 * @package    GController_SVideo_Profile
 * @subpackage Field
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Field.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SVideo_Profile_Field extends GController_Profile_Field
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array('published');

    /**
     * Первичный ключ таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'video_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_video';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_svideo_grid';
}
?>