<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные для интерфейса профиля видеозаписи"
 * Пакет контроллеров "Список видеозаписей"
 * Группа пакетов     "Видеозаписи"
 * Модуль             "Сайт"
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SVideo_Profile
 * @copyright  Copyright (c) 2013-2016 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные для интерфейса профиля видеозаписи
 * 
 * @category   Gear
 * @package    GController_SVideo_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SVideo_Profile_Data extends GController_Profile_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array(
        'category_id', 'video_type', 'video_code', 'video_title', 'video_text', 'video_uploaded', 'video_thumbnail', 'video_duration',
        'video_width', 'video_height', 'video_tags', 'video_script', 'published', 'published_date', 'published_time', 'published_user',
        'video_frame'
    );

    /**
     * Первичный ключ таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'video_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_video';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_svideo_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_profile_update'], $record['video_title']);
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        // категория
        if (isset($params['category_id']))
            if ($params['category_id'] == -1)
                $params['category_id'] = null;
        // дата публикации
        $date = $this->input->getDate('published_date', 'Y-m-d', false);
        if (!$date)
            $params['published_date'] = date('Y-m-d');
        else
            $params['published_date'] = $date;
        // время публикации
        $time = $this->input->get('published_time', false);
        if (!$time)
            $params['published_time'] = date('H:i:s');
        // кто опубликовал
        $params['published_user'] = $this->session->get('user_id');
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function recordPreprocessing($record)
    {
        $setTo = array();
        $query = new GDb_Query();

        // категории
        if (!empty($record['category_id'])) {
            $sql = 'SELECT * FROM `site_categories` WHERE `category_id`=' . $record['category_id'];
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            if (!empty($rec))
                $setTo[] = array('xtype' => 'mn-field-combo', 'id' => 'fldCategory', 'text' => $rec['category_name'], 'value' => $record['category_id']);
            unset($record['category_id']);
        }
        // обложка
        if (!empty($record['video_thumbnail'])) {
            $setTo[] = array('id' => 'fldVideoImage', 'func' => 'setSrc', 'args' => array($record['video_thumbnail']));
        }
        // установка полей
        if ($setTo)
            $this->response->add('setTo', $setTo);

        return $record;
    }
}
?>