<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные видеозаписи полученные от видеохостинга"
 * Пакет контроллеров "Профиль видеозаписи"
 * Группа пакетов     "Видеозапиис"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SVideo_Profile
 * @copyright  Copyright (c) 2013-2016 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Info.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные видеозаписи полученные от видеохостинга
 * 
 * @category   Gear
 * @package    GController_SVideo_Profile
 * @subpackage Info
 * @copyright  Copyright (c) 2013-2016 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Info.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SVideo_Profile_Info extends GController_Profile_Data
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_svideo_grid';

    /**
     * Возращает информацию о vimeo видео
     * 
     * @return void
     */
    protected function getVimeoInfo($code)
    {
        // запрос к видеохостингу
        $json = file_get_contents('http://vimeo.com/api/v2/video/' . $code . '.json');
        if ($json === false)
            throw new GException('Error', $this->_['msg_error_request']);

        $video = json_decode($json, true);
        try {
            switch (json_last_error()) {
                case JSON_ERROR_DEPTH:
                    throw new GException('Data processing error', 'Maximum stack depth exceeded');
                    break;

                case JSON_ERROR_CTRL_CHAR:
                    throw new GException('Data processing error', 'Unexpected control character found');
                    break;

                case JSON_ERROR_SYNTAX:
                case 2:
                    throw new GException('Data processing error', 'Syntax error, malformed JSON');
                    break;

                case JSON_ERROR_NONE:
                    break;
            }
        } catch(GException $e) {}
        // если нет видеозаписи
        if (empty($video))
            throw new GException('Error', $this->_['msg_no_data']);
        $res = $video[0];
        if (!empty($res['tags']))
            $tags = '#' . implode(' #', $res['tags']);
        else
            $tags = '';

        return array(
            'title'       => $res['title'],
            'description' => $res['description'],
            'uploaded'    => empty($res['upload_date']) ? '' : date('d-m-y H:i:s', strtotime($res['upload_date'])),
            'thumbnail'   => $res['thumbnail_large'],
            'image'       => $res['thumbnail_large'],
            'duration'    => $res['duration'],
            'width'       => $res['width'],
            'height'      => $res['height'],
            'tags'        => $tags,
        );
    }

    /**
     * Возращает информацию о youtube видео
     * 
     * @return void
     */
    protected function getYoutubeInfo($code)
    {
        // запрос к видеохостингу
        $json = file_get_contents('https://www.googleapis.com/youtube/v3/videos?id=' . $code . '&part=snippet&key=AIzaSyBRapI9y0VdAFpfF_ngh6hOXA7BQtHAz6g');
        GFactory::getDg()->log($json);
        if ($json === false)
            throw new GException('Error', $this->_['msg_error_request']);

        $video = json_decode($json, true);
        try {
            switch (json_last_error()) {
                case JSON_ERROR_DEPTH:
                    throw new GException('Data processing error', 'Maximum stack depth exceeded');
                    break;

                case JSON_ERROR_CTRL_CHAR:
                    throw new GException('Data processing error', 'Unexpected control character found');
                    break;

                case JSON_ERROR_SYNTAX:
                case 2:
                    throw new GException('Data processing error', 'Syntax error, malformed JSON');
                    break;

                case JSON_ERROR_NONE:
                    break;
            }
        } catch(GException $e) {}
        // если нет видеозаписи
        if (empty($video))
            throw new GException('Error', $this->_['msg_no_data']);
        $res = $video['items'][0]['snippet'];
        if (!empty($res['tags']))
            $tags = '#' . implode(' #', $res['tags']);
        else
            $tags = '';

        return array(
            'title'       => $res['title'],
            'description' => $res['description'],
            'uploaded'    => empty($res['publishedAt']) ? '' : date('d-m-y H:i:s', strtotime($res['publishedAt'])),
            'thumbnail'   => $res['thumbnails']['medium']['url'],
            'image'       => $res['thumbnails']['standard']['url'],
            'duration'    => 0,
            'width'       => $res['thumbnails']['standard']['width'],
            'height'      => $res['thumbnails']['standard']['height'],
            'tags'        => $tags
        );
    }

    /**
     * Обновление текста изображения
     * 
     * @return void
     */
    protected function dataInfo()
    {
        // все поля видеозаписи
        $fields = $this->input->get('fields', false);
        if (empty($fields))
            throw new GException('Error', $this->_['msg_empty_fields']);
        // видеохостинг
        if (empty($fields['video_type']) || empty($fields['video_code']))
            throw new GException('Error', $this->_['msg_error_request']);

        $info = array();
        switch ($fields['video_type']) {
            case 'vimeo': $info = $this->getVimeoInfo($fields['video_code']); break;

            case 'youtube': $info = $this->getYoutubeInfo($fields['video_code']); break;
        }
        if (empty($info))
            throw new GException('Error', $this->_['msg_no_data']);

        $setTo = array(
            array('id' => 'fldVideoTitle', 'value' => $info['title']),
            array('id' => 'fldVideoDescription', 'value' => $info['description']),
            array('id' => 'fldVideoUploaded', 'value' => $info['uploaded']),
            array('id' => 'fldVideoThumbnail', 'value' => $info['thumbnail']),
            array('id' => 'fldVideoImage', 'func' => 'setSrc', 'args' => array($info['image'])),
            array('id' => 'fldVideoDuration', 'value' => $info['duration']),
            array('id' => 'fldVideoWidth', 'value' => $info['width']),
            array('id' => 'fldVideoHeight', 'value' => $info['height']),
            array('id' => 'fldVideoTags', 'value' => $info['tags'])
        );

        // установка полей
        $this->response->add('setTo', $setTo);
    }

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        if ($this->uri->action == 'info')
            // метод запроса
            switch ($this->uri->method) {
                // метод "POST"
                case 'POST': $this->dataInfo(); return;
            }

        parent::init();
    }
}
?>