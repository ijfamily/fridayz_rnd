<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2013-2016 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Создание записи "Видеозапись"',
    'title_profile_update' => 'Изменение записи "%s"',
    'text_btn_help'        => 'Справка',
    'text_btn_send'        => ' Получить информацию о видео ',
    // поля формы
    'label_video_code'        => 'Код',
    'label_video_type'        => 'Видеохостинг',
    'label_video_title'       => 'Загаловок',
    'label_video_description' => 'Описание',
    'label_video_uploaded'    => 'Загружено',
    'label_video_thumbnail'   => 'Обложка',
    'title_fieldset_size'     => 'Атрибуты видеозаписи',
    'label_video_duration'    => 'Длительность, сек.',
    'label_video_width'       => 'Ширина, пкс.',
    'label_video_height'      => 'Высота, пкс.',
    'label_video_tags'        => 'Теги',
    'label_category_name'   => 'Категория<hb></hb>',
    'tip_category_name'     => 'Категорию необходимо указывать если нужен список видеозаписей',
    'title_fieldset_public' => 'Дата публикации видеозаписи',
    'label_published_date'  => 'Дата',
    'label_published_time'  => 'Время',
    'label_published'       => 'Опубликовать<hb></hb>',
    'tip_published'         => 'Опубликовать видеозапись в выбранной категории',
    'label_frame'           => 'Фрейм<hb></hb>',
    'tip_frame'             => 'Показывать видеозапись в фрейме',
    // сообщения
    'msg_error_request' => 'Невозможно получить данные по вашему запросу!',
    'msg_empty_fields'  => 'Не заполнены поля "Код видео" или "Тип видео"!',
    'msg_no_data'       => 'Нет данных по вашему запросу!'
);
?>