<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Видеозаписи"
 * Группа пакетов     "Видеозаписи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SVideo
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Видеозаписи
 * Описание: Видеозаписи
 * Меню: Видеозапись
 * ID класса: gcontroller_svideo_grid
 * Модуль: Сайт
 * Группа: Сайт
 * Очищать: нет
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске компонентов: нет
 *    В главном меню: нет
 * Ресурс
 *    Компонент: site/video/video/
 *    Контроллер: site/video/video/Grid/
 *    Интерфейс: grid/interface/
 *    Меню: profile/interface/
 */

return array(
    // {S}- модуль "Site" {} - пакет контроллеров "Video" -> SVideo
    'clsPrefix' => 'SVideo',
    // использовать язык
    'language'  => 'ru-RU',
    // видео
    'video' => array(
        'youtube' => array(
            'icon'  => 'icon-youtube.png',
            'color' => '#ED3030',
            'url'   => 'https://www.youtube.com/',
            'page'  => 'https://www.youtube.com/watch?v=%s',
            'frame' => '<iframe width="100%" height="360" src="//www.youtube.com/embed/%s?rel=0" frameborder="0"></iframe>'
        ),
        'vimeo'   => array(
            'icon'  => 'icon-vimeo.png',
            'color' => '#018DFF',
            'url'   => 'https://vimeo.com/',
            'page'  => 'https://vimeo.com/%s',
            'frame' => '<iframe width="100%" height="360" src="https://player.vimeo.com/video/%s" frameborder="0" webkitallowfullscreen="webkitallowfullscreen" mozallowfullscreen="mozallowfullscreen" allowfullscreen="allowfullscreen"></iframe>'
        )
    )
);
?>