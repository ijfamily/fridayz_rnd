<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск шаблона"
 * Пакет контроллеров "Шаблоны"
 * Группа пакетов     "Шаблоны"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_STemplates_Search
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск шаблолна
 * 
 * @category   Gear
 * @package    GController_STemplates_Search
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_STemplates_Search_Data extends GController_Search_Data
{
    /**
     * Идентификатор компонента (списка) к которому применяется поиск
     *
     * @var string
     */
    public $gridId = 'gcontroller_stemplates_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_stemplates_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор
     * Используется для инициализации атрибутов контроллер не переданного в конструктор
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'template_name', 'label' => $this->_['header_template_name']),
            array('dataIndex' => 'template_filename', 'label' => $this->_['header_template_filename']),
            array('dataIndex' => 'template_description', 'label' => $this->_['header_template_description'])
        );
    }

    /**
     * Применение фильтра
     * 
     * @return void
     */
    protected function dataAccept()
    {
        if ($this->_sender == 'toolbar') {
            if ($this->input->get('action') == 'search') {
                $tlbFilter = '';
                // если попытка сбросить фильтр
                if (!$this->input->isEmptyPost(array('theme', 'language'))) {
                    $tlbFilter = array(
                        'theme'    => $this->input->get('theme'),
                        'language' => $this->input->get('language')
                    );
                }
                $this->store->set('tlbFilter', $tlbFilter);
                return;
            }
        }

        parent::dataAccept();
    }
}
?>