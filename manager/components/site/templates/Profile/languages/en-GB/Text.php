<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Create record "Template"',
    'title_profile_update' => 'Update record "%s"',
    // поля формы
    'label_template_name'        => 'Name',
    'empty_template_name'        => 'New template',
    'label_template_description' => 'Description',
    'empty_template_description' => 'Template description',
    'label_template_filename'    => 'File',
    'empty_template_filename'    => 'template.tpl',
    'tip_template_filename'      => 'File with extensions (".TPL", ".XML")',
    // сообщения
    'msg_error_expension' => 'Created the template does not match the increase (".TPL", ".XML")'
);
?>