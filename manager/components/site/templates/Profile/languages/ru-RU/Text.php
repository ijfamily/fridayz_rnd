<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Создание записи "Шаблон"',
    'title_profile_update' => 'Изменение записи "%s"',
    'text_btn_help'        => 'Справка',
    // поля формы
    'label_category_name'        => 'Категория',
    'label_template_name'        => 'Название',
    'empty_template_name'        => 'Новый шаблон',
    'label_template_description' => 'Описание',
    'empty_template_description' => 'Описание шаблона',
    'label_template_filename'    => 'Файл',
    'empty_template_filename'    => 'template.tpl.php',
    'tip_template_filename'      => 'Файл с расширением (".TPL", ".XML", ".HTML", ".HTM", ".PHP")',
    'label_template_icon'        => 'Обозначение',
    // данные
    'data_category_default' => 'Шаблон статьи',
    // сообщения
    'msg_error_expension' => 'Неправильно указано расширение шаблона (".TPL", ".XML", ".HTML", ".HTM", ".PHP")'
);
?>