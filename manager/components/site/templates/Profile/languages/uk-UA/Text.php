<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Створення запису "Шаблон"',
    'title_profile_update' => 'Зміна запису "%s"',
    // поля формы
    'label_template_name'        => 'Название',
    'empty_template_name'        => 'Новий шаблон',
    'label_template_description' => 'Опис',
    'empty_template_description' => 'Опис шаблону',
    'label_template_filename'    => 'Файл',
    'empty_template_filename'    => 'template.tpl',
    'tip_template_filename'      => 'Файл з розширенням (".TPL", ".XML")',
    // сообщения
    'msg_error_expension' => 'Создаваеми шаблон, не відповідає розширенню (".TPL", ".XML")'
);
?>