<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные для интерфейса профиля шаблона"
 * Пакет контроллеров "Профиль шаблона"
 * Группа пакетов     "Шаблоны"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_STemplate_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные для интерфейса профиля шаблона
 * 
 * @category   Gear
 * @package    GController_STemplate_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_STemplates_Profile_Data extends GController_Profile_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array('template_name', 'template_description', 'template_filename', 'category_id', 'template_icon', 'template_theme');

    /**
     * Первичный ключ таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'template_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_templates';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_stemplates_grid';

    /**
     * Доступные расширения файлов для загрузки
     *
     * @var array
     */
    protected $_upload = array('xml', 'tpl', 'html', 'htm', 'php');

    /**
     * Список доступных языков
     *
     * @var array
     */
    protected $_langs;

    /**
     * Каталог c шаблонами
     *
     * @var string
     */
    public $pathData = '../application/templates';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        $this->_langs = $this->session->get('language/list', array(), 'site');
    }

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_profile_update'], $record['template_name']);
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function recordPreprocessing($record)
    {
        $setTo = array();
        $query = new GDb_Query();
        // категории
        $sql = 'SELECT * FROM `site_template_categories` WHERE `category_id`=' . $record['category_id'];
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        if (!empty($rec))
            $setTo[] = array('xtype' => 'combo', 'id' => 'fldCategoryId', 'value' => $rec['category_name']);
        $setTo[] = array('xtype' => 'combo', 'id' => 'fldTemplateIcon', 'value' => $record['template_icon']);

        // установка полей
        if ($setTo)
            $this->response->add('setTo', $setTo);

        return $record;
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        // если состояние формы "update"
        if ($this->isUpdate) return true;
        // определение расширения файла
        $fileExt = strtolower(pathinfo($params['template_filename'], PATHINFO_EXTENSION));
        if (!in_array($fileExt, $this->_upload))
            throw new GException('Error', $this->_['msg_error_expension']);
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        /*
        Gear::library('File');

        $count = count($this->_langs);
        // выбранные шаблоны
        $query = new GDb_Query();
        $sql = 'SELECT * FROM `site_templates` WHERE `template_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        while (!$query->eof()) {
            $tpl = $query->next();
            for ($i = 0; $i < $count; $i++) {
                // удаление шаблона
                GFile::delete($this->pathData . '/' . $this->_langs[$i]['language_alias'] . '/' . $tpl['template_filename'], false);
            }
        }
        */
    }
}
?>