<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля шаблона"
 * Пакет контроллеров "Профиль шаблона"
 * Группа пакетов     "Шаблоны"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_STemplate_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля шаблона
 * 
 * @category   Gear
 * @package    GController_STemplate_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_STemplates_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        $filter = $this->store->get('tlbFilter', false, 'gcontroller_stemplates_grid');

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'titleEllipsis' => 45,
                  'gridId'        => 'gcontroller_stemplates_grid',
                  'width'         => 420,
                  'autoHeight'    => true,
                  'stateful'      => false,
                  'buttonsAdd'    => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-site-templates',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_template_name'],
                  'emptyText'  => $this->_['empty_template_name'],
                  'name'       => 'template_name',
                  'maxLength'  => 255,
                  'anchor'     => '100%',
                  'allowBlank' => false),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_template_description'],
                  'emptyText'  => $this->_['empty_template_description'],
                  'name'       => 'template_description',
                  'maxLength'  => 255,
                  'anchor'     => '100%',
                  'allowBlank' => false),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_template_filename'],
                  'emptyText'  => $this->_['empty_template_filename'],
                  'labelTip'   => $this->_['tip_template_filename'],
                  'name'       => 'template_filename',
                  'maxLength'  => 255,
                  'anchor'     => '100%',
                  'allowBlank' => false),
            array('xtype'      => 'mn-field-combo',
                  'fieldLabel' => $this->_['label_category_name'],
                  'value'      => $this->isInsert ? $this->_['data_category_default'] : '',
                  'hiddenValue' => $this->isInsert ? 1 : '',
                  'id'         => 'fldCategoryId',
                  'name'       => 'category_id',
                  'editable'   => false,
                  'width'      => 200,
                  'hiddenName' => 'category_id',
                  'allowBlank' => false,
                  'store'      => array(
                      'xtype' => 'jsonstore',
                      'url'   => $this->componentUrl . 'combo/trigger/?name=categories'
                  )
            ),
            array('xtype'      => 'mn-field-combo',
                  'tpl'        => 
                      '<tpl for="."><div ext:qtip="{name}" class="x-combo-list-item">'
                    . '<img width="16px" height="16px" align="absmiddle" src="' 
                    . $this->path . '/../Combo/resources/icons/{name}.png" style="margin-right:5px">{name}</div></tpl>',
                  'fieldLabel' => $this->_['label_template_icon'],
                  'value'      => $this->isInsert ? 'icon-template-default' : '',
                  'id'         => 'fldTemplateIcon',
                  'name'       => 'template_icon',
                  'editable'   => false,
                  'width'      => 200,
                  'hiddenName' => 'template_icon',
                  'allowBlank' => false,
                  'store'      => array(
                      'xtype' => 'jsonstore',
                      'url'   => $this->componentUrl . 'combo/trigger/?name=icons'
                  )
            )
        );
        // если состояние формы "вставка"
        if ($this->isInsert) {
            $items[] = array(
                'xtype' => 'hidden',
                'name'  => 'template_theme',
                'value' => $filter['theme']
            );
        }

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($items);
        $form->url = $this->componentUrl . 'profile/';
        $form->labelWidth = 80;

        parent::getInterface();
    }
}
?>