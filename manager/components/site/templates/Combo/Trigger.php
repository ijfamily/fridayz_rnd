<?php
/**
 * Gear Manager
 *
 * Контроллер         "Триггер выпадающего списка для обработки данных шаблонов"
 * Пакет контроллеров "Шаблоны"
 * Группа пакетов     "Шаблоны"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_STemplates_Combo
 * @subpackage Trigger
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Combo/Trigger');

/**
 * Триггер выпадающего списка для обработки данных шаблонов
 * 
 * @category   Gear
 * @package    GController_STemplates_Combo
 * @subpackage Trigger
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_STemplates_Combo_Trigger extends GController_Combo_Trigger
{
    /**
     * Вывод логотипов групп пользователей
     * 
     * @return void
     */
    protected function queryIcons()
    {
        $dir = $this->resourcePath . 'icons/';
        if (($handle = opendir($dir)) === false) 
            throw new GException('Error', 'The directory can not be opened');
        $data = array();
        $count = 0;
        while (false !== ($file = readdir($handle))) {
            if ($file != '.' && $file != '..') {
                $name = basename($file, '.png'); 
                $data[] = array('id' => $name, 'name' => $name);
                $count++;
            }
        }
        $this->response->set('totalCount', $count);
        $this->response->success = true;
        $this->response->data = $data;
        closedir($handle);
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $triggerName название триггера
     * @return void
     */
    protected function dataView($triggerName)
    {
        parent::dataView($triggerName);

        // имя триггера
        switch ($triggerName) {
            // категории
            case 'categories': $this->query('site_template_categories', 'category_id', 'category_name'); break;

            // обозначения
            case 'icons': $this->queryIcons(); break;
        }
    }
}
?>