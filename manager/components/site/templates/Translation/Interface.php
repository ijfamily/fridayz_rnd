<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс локализации шаблона"
 * Пакет контроллеров "Локализация шаблона"
 * Группа пакетов     "Шаблоны"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_STemplates_Translation
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс локализации шаблона
 * 
 * @category   Gear
 * @package    GController_STemplates_Translation
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_STemplates_Translation_Interface extends GController_Translation_Interface
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_stemplates_grid';

    /**
     * Каталог шаблонов
     *
     * @var string
     */
    public $pathTemplate = '../application/templates/';

    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        parent::getDataInterface();

        $query = new GDb_Query();
        // данные о шаблоне
        $sql = 'SELECT * FROM `site_templates` WHERE `template_id`=' . $this->uri->id;
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        // определение языка
        $list =  $this->config->getFromCms('Site', 'LANGUAGES');
        // попытка чтения файла шаблона
        $filter = $this->store->get('tlbFilter');
        if ($filter['theme'] == 'common')
            $lang = '';
        else
            $lang = $list[$filter['language']]['alias'] . '/';
        $filename = $this->pathTemplate . $filter['theme'] . '/' . $lang . $rec['template_filename'];
        // если шаблон не существует
        if (!file_exists($filename))
            throw new GException('Error', 'Can not open file "%s" for writing', array($filename));
        $text = file_get_contents($filename);
        if ($text === false)
            throw new GException('Error', 'Can not open file "%s" for writing', array($filename));

        return array(
            'text'           => $text,
            'filename'       => $filename,
            'language_alias' => $list[$filter['language']]['alias'],
            'template_name'     => $rec['template_name'],
            'template_filename' => $rec['template_filename']
        );
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => sprintf($this->_['title_translation_update'], $data['language_alias'], $data['template_name']),
                  'titleEllipsis' => 80,
                  'gridId'        => 'gcontroller_stemplates_grid',
                  'width'         => 800,
                  'height'        => 510,
                  'resizable'     => true,
                  'stateful'      => false,
                  'maximizable'   => true,
                  'btnDeleteHidden' => true,
                  'iconCls'         => 'icon-form-translate')
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype' => 'hidden',
                  'name'  => 'language_alias',
                  'value' => $data['language_alias']),
            array('xtype' => 'hidden',
                  'name'  => 'template_filename',
                  'value' => $data['template_filename']),
            array('xtype'      => 'textfield',
                  'hideLabel'  => true,
                  'anchor'     => '100%',
                  'value'      => $data['filename'],
                  'readOnly'   => true,
                  'allowBlank' => true),
            array('xtype'      => 'textarea',
                  'hideLabel'  => true,
                  'name'       => 'template_text',
                  'anchor'     => '100% 100%',
                  'value'      => $data['text'],
                  'style'      => 'margin-bottom:27px',
                  'allowBlank' => true)
        );

        // Ext_Form_DataProfile (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($items);
        $form->url = $this->componentUrl . 'translation/';

        parent::getInterface();
    }
}
?>