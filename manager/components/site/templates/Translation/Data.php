<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные интерфейса локализации шаблона"
 * Пакет контроллеров "Локализация шаблона"
 * Группа пакетов     "Шаблоны"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_STemplates_Translation
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные интерфейса локализации шаблона
 * 
 * @category   Gear
 * @package    GController_STemplates_Translation
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_STemplates_Translation_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array('template_text', 'language_alias', 'template_filename');

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_stemplates_grid';

    /**
     * Каталог шаблонов
     *
     * @var string
     */
    public $pathTemplate = '../application/templates/';

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {}

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        parent::dataAccessUpdate();

        if ($this->input->isEmpty('PUT'))
            throw new GException('Warning', 'To execute a query, you must change data!');
        // массив полей с их соответствующими значениями
        if (empty($params))
            $params = $this->input->getBy($this->fields);
        // проверки входных данных
        $this->isDataCorrect($params);
        // попытка записи файла шаблона
        $filter = $this->store->get('tlbFilter');
        if ($filter['theme'] == 'common')
            $lang = '';
        else
            $lang = $params['language_alias'] . '/';
        $filename = $this->pathTemplate . $filter['theme'] . '/' . $lang . $params['template_filename'];
        // если шаблон не существует
        if (!file_exists($filename))
            throw new GException('Error', 'Can not open file "%s" for writing', array($filename));
        $text = file_put_contents($filename, $params['template_text']);
        if ($text === false)
            throw new GException('Error', 'Can not open file "%s" for writing', array($filename));
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        // если правка формы
        if ($this->isUpdate) {
            if (empty($params['template_text']))
                throw new GException('Warning', 'Incorrectly entered or selected from the fields: %s', array($this->_['label_template_text']));
            if (empty($params['language_alias']))
                throw new GException('Warning', 'Incorrectly entered or selected from the fields: %s', array($this->_['label_language_alias']));
            if (empty($params['template_filename']))
                throw new GException('Warning', 'Incorrectly entered or selected from the fields: %s', array($this->_['label_template_filename']));
            $params['template_filename'] = pathinfo($params['template_filename'], PATHINFO_BASENAME);
        }
    }
}
?>