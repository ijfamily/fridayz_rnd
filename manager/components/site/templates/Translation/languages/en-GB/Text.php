<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_translation_insert' => 'Creating a translator records (%s) "Article"',
    'title_translation_update' => 'Translating of the recording (%s) "%s"',
    'text_button_export'       => 'Export',
    'tooltip_button_export'    => 'Export article to Microsoft Office Word',
    // закладка "seo статьи"
    'title_tab_seo'     => 'SEO article',
    'title_fieldset_r'  => 'Robots',
    'label_page_meta_r' => 'Robots',
    'tip_page_meta_r'   => 'Generates information about the hypertext documents, which comes to search engine robots.
                            The tag values ​​can be as follows: Index (page needs to be indexed), Noindex (document
                            not indexed), Follow (hyperlinks on the page are tracked), Nofollow (hyperlinks are not traced)
                            All (including the index and follow, enabled by default), None (includes values ​​noindex and nofollow).',
    'label_page_meta_v' => 'Time and interval',
    'tip_page_meta_v'   => 'Time and interval visit crawler. Allows you to control the frequency of document indexing in search engines.',
    'label_page_meta_m' => 'State',
    'tip_page_meta_m'   => 'The value of «Static» notes that the system does not need to index the document in the future,
                            «Dynamic» will regularly indexed web page',
    'title_fieldset_a'  => 'Copyright',
    'label_page_meta_a' => 'Author',
    'tip_page_meta_a'   => 'The identification of the document author or accessories. Contains the name of a web page author, in the event that
                            if the site is owned by an organization, it is better to use the "Organization."',
    'label_page_meta_c' => 'Organization',
    'tip_page_meta_c'   => 'Identification of accessories document to any organization',
    'label_page_meta_g' => 'Generator',
    'tip_page_meta_g'   => 'The name of the program developer to edit web pages',
    'label_page_meta_k' => 'Keywords',
    'tip_page_meta_k'   => 'Search engines are used to determine the most relevant reference. In forming this tag
                            use only the words that are contained in the document itself. The use of those words, which
                            not on the page, it is not recommended. The recommended number of words in this tag - no more than ten.',
    'label_page_meta_d' => 'Description',
    'tip_page_meta_d'   => 'It is used by search engines to index, as well as creating annotations in the extradition request
                            if there is no tag, search engines give a summary or the first line of the document fragment containing the keywords.
                            Displayed after the link when you search in a search engine pages.',
    // закладка "страница"
    'title_tab_page'        => 'Page',
    'label_page_title'      => 'Title',
    'tip_page_title'        => 'Displays the name of the browser tabs',
    'label_page_header'     => 'Header',
    'tip_page_header'       => 'Displayed in the title',
    'label_page_menu'       => 'Menu',
    'tip_page_menu'         => 'The name of the menu item (for dynamic menu)',
    'label_page_breadcrumb' => 'Breadcrumbs',
    'tip_page_breadcrumb'   => 'he name in the "breadcrumbs" of articles (if the "breadcrumbs" is pressed)',
    'label_page_html_short' => 'Description (sht.)',
    'tip_page_html_short'   => 'Appears in the list of articles in the form of a brief description of',
    'label_page_rss'        => 'Description (short) for RSS feeds',
    'tip_page_rss'          => 'Displayed in RSS feed',
    // закладка "статья"
    'title_tab_article' => 'Article',
    // сообщения
    'msg_export_warning' => 'Article contains text for export to Microsoft Office Word or profile articles not selected flag "Print"',
    // тип
    'data_robots' => array(
        array('all'), array('index, follow'), array('noindex, follow'), array('index, nofollow'), array('noindex, nofollow'),
        array('none')
    ),
    'data_revisit' => array(
        array('1 day', 1), array('2 days', 2), array('3 days', 3), array('4 days', 4), array('5 days', 5), array('6 days', 6), array('7 days', 7)
    ),
    'data_doc' => array(array('Static'), array(' Dynamic')),
);
?>