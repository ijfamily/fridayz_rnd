<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_translation_insert' => 'Створення транслятора запису (%s) "Стаття"',
    'title_translation_update' => 'Транслювання запису (%s) "%s"',
    'text_button_export'       => 'Экспорт',
    'tooltip_button_export'    => 'Экспорт статті в Microsoft Office Word',
    // закладка "seo статьи"
    'title_tab_seo'     => 'SEO статті',
    'title_fieldset_r'  => 'Роботи',
    'label_page_meta_r' => 'Роботи',
    'tip_page_meta_r'   => 'Формує інформацію про гіпертекстових документах, яка надходить до роботів пошукових систем.
                            Значення тега можуть бути наступними: Index (сторінка повинна бути проіндексована), Noindex (документ
                            не індексується), Follow (гіперпосилання на сторінці відслідковуються), Nofollow (гіперпосилання не простежуються),
                            All (включає значення index і follow, включений за умовчанням), None (включає значення noindex і nofollow).',
    'label_page_meta_v' => 'Час і інтервал',
    'tip_page_meta_v'   => 'Час і інтервал відвідування пошукового робота. Дозволяє керувати частотою індексації документа в пошуковій системі.',
    'label_page_meta_m' => 'Стан',
    'tip_page_meta_m'   => 'Значення «Static» відзначає, що системі немає необхідності індексувати документ надалі,
                            «Dynamic» дозволяє регулярно індексувати Інтернет-сторінку',
    'title_fieldset_a'  => 'Авторське право',
    'label_page_meta_a' => 'Автор',
    'tip_page_meta_a'   => 'Ідентифікація автора або приналежності документа. Містить ім\'я автора Інтернет-сторінки, в тому випадку,
                            якщо сайт належить якої організації, доцільніше використовувати поле «Організація».',
    'label_page_meta_c' => 'Організація',
    'tip_page_meta_c'   => 'Ідентифікація приналежності документа до якої-небудь організації',
    'label_page_meta_g' => 'Генератор',
    'tip_page_meta_g'   => 'Назва програми розробника для редагування веб-сторінок',
    'label_page_meta_k' => 'Ключові слова',
    'tip_page_meta_k'   => 'Пошукові системи використовують для того, щоб визначити релевантність посилання. При формуванні даного тега
                            необхідно використовувати тільки ті слова, які містяться в самому документі. Використання тих слів, яких
                            немає на сторінці, не рекомендується. Рекомендована кількість слів у даному тезі - не більше десяти.',
    'label_page_meta_d' => 'Опис',
    'tip_page_meta_d'   => 'Використовується пошуковими системами для індексації, а також при створенні анотації у видачі за запитом
                            За відсутності тега пошукові системи видають в анотації перший рядок документа чи уривок, який містить ключові слова.
                            Відображається після заслання при пошуку сторінок в пошуковику.',
    // закладка "страница"
    'title_tab_page'        => 'Сторінка',
    'label_page_title'      => 'Напис',
    'tip_page_title'        => 'Відображається в назві вкладки браузера',
    'label_page_header'     => 'Тема',
    'tip_page_header'       => 'Відображається в заголовку статті',
    'label_page_menu'       => 'Меню',
    'tip_page_menu'         => 'Назва пункту меню (лише для динамічного меню)',
    'label_page_breadcrumb' => 'ланцюжок',
    'tip_page_breadcrumb'   => 'Назва в "ланцюжку" статей (якщо "ланцюжок" задіяна)',
    'label_page_html_short' => 'Опис (скор.)',
    'tip_page_html_short'   => 'Відображається у списку статей у вигляді короткого опису',
    'label_page_rss'        => 'Опис (скор.) для RSS стрічки',
    'tip_page_rss'          => 'Відображається в RSS стрічці',
    // закладка "статья"
    'title_tab_article' => 'Стаття',
    // сообщения
    'msg_export_warning' => 'Стаття не містить текст для експорту в Microsoft Office Word або в профілі статті не обраний прапор "Друк"',
    // тип
    'data_robots' => array(
        array('all'), array('index, follow'), array('noindex, follow'), array('index, nofollow'), array('noindex, nofollow'),
        array('none')
    ),
    'data_revisit' => array(
        array('1 день', 1), array('2 дня', 2), array('3 дня', 3), array('4 дня', 4), array('5 днів', 5), array('6 днів', 6), array('7 днів', 7)
    ),
    'data_doc' => array(array('Static'), array(' Dynamic')),
);?>