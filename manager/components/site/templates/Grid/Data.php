<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка шаблонов"
 * Пакет контроллеров "Шаблоны"
 * Группа пакетов     "Шаблоны"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_STemplates_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('File');
Gear::controller('Grid/Data');

/**
 * Данные списка шаблонов
 * 
 * @category   Gear
 * @package    GController_STemplates_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_STemplates_Grid_Data extends GController_Grid_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'template_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_templates';

    /**
     * Каталог c шаблонами
     *
     * @var string
     */
    public $pathData = '../application/templates';

    /**
     * Список доступных языков
     *
     * @var array
     */
    protected $_langs;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // список доступных языков
        $this->_langs = $this->config->getFromCms('Site', 'LANGUAGES');;
        // SQL запрос
        $this->sql = 'SELECT SQL_CALC_FOUND_ROWS * '
                   . 'FROM `site_templates` `t` JOIN `site_template_categories` `c` USING(`category_id`) ';
        // фильтр из панели инсирументов
        $filter = $this->store->get('tlbFilter', false);
        if ($filter)
            $this->sql .= "WHERE `template_theme`='" . $filter['theme'] . "' %filter %fastfilter ORDER BY %sort LIMIT %limit";
        else
            $this->sql .= 'WHERE 0>1';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // название шаблона
            'template_name' => array('type' => 'string'),
            // название категории
            'category_name' => array('type' => 'string'),
            // описание шаблона
            'template_description' => array('type' => 'string'),
            // название файла шаблона
            'template_filename' => array('type' => 'string'),
            // права доступа
            'file_perms' => array('type' => 'string'),
            // обозначение
            'template_icon' => array('type' => 'string')
        );
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        $query = new GDb_Query();
        // удаление записей таблицы "site_templates" (шаблоны)
        if ($query->clear('site_templates') === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_templates') === false)
            throw new GSqlException();
    }

    /**
     * Возращает sql запрос для быстрого фильтра
     * (для подстановки "%fastfilter" в $sql)
     * 
     * @param string $key идендификатор поля
     * @param string $value значение
     * @return string
     */
    protected function getTreeFilter($key, $value)
    {
        // идендификатор поля
        switch ($key) {
            // по дате
            case 'byDt':
                $toDate = date('Y-m-d');
                switch ($value) {
                    case 'day':
                        return ' AND (`t`.`sys_date_insert` BETWEEN "' . $toDate . '" AND "' . $toDate . '"'
                             . ' OR `t`.`sys_date_update` BETWEEN "' . $toDate . '" AND "' . $toDate . '")';

                    case 'week':
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d") - 7, date("Y")));
                        return ' AND (`t`.`sys_date_insert` BETWEEN "' . $fromDate . '" AND "' . $toDate . '"'
                             . ' OR `t`.`sys_date_update` BETWEEN "' . $fromDate . '" AND "' . $toDate . '")';

                    case 'month':
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, date("m")-1, date("d"), date("Y")));
                        return ' AND (`t`.`sys_date_insert` BETWEEN "' . $fromDate . '" AND "' . $toDate . '"'
                             . ' OR `t`.`sys_date_update` BETWEEN "' . $fromDate . '" AND "' . $toDate . '")';
                }
                break;

            // по категории
            case 'byCt':
                return ' AND `c`.`category_id`="' . (int) $value . '" ';
        }

        return '';
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        // если есть обозначение
        if (!empty($record['template_icon']))
            $record['template_icon'] = '<img src="' . $this->path . '/../Combo/resources/icons/' . $record['template_icon'] . '.png" />';
        $filter = $this->store->get('tlbFilter', false);
        if ($filter) {
            if ($filter['theme'] == 'common')
                $lang = '';
            else
                $lang = $this->_langs[$filter['language']]['alias'] . '/';
            $filename = $this->pathData . '/' . $filter['theme'] . '/' . $lang . $record['template_filename'];
            if (!file_exists($filename)) {
                $record['rowCls'] = 'mn-row-notpublished';
                $record['isRowMenu'] = false;
            } else {
                $perms = fileperms($filename);
                if ($perms !== false) {
                    $record['file_perms'] = GFile::filePermsDigit($perms) . ' ' . GFile::filePermsInfo($perms);
                }
            }
        }

        return $record;
    }
}
?>