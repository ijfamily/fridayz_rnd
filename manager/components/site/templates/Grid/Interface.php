<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка шаблонов"
 * Пакет контроллеров "Шаблоны"
 * Группа пакетов     "Шаблоны"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_STemplates_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс списка шаблонов
 * 
 * @category   Gear
 * @package    GController_STemplates_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
 
final class GController_STemplates_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'template_id';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'template_name';

    /**
     * Использовать быстрый фильтр
     *
     * @var boolean
     */
    public $useSlidePanel = true;

    /**
     * Возращает список шаблонов сайта
     * 
     * @return array
     */
    protected function getThemes()
    {
        $path = '../application/templates/';
        $items = array();
        if ($handle = opendir($path)) {
            while (false !== ($file = readdir($handle))) {
                // если файл
                if ($file != "." && $file != ".." && $file != 'system' && is_dir($path . $file)) {
                    $items[] = array($file, $file);
                }
            }
            closedir($handle);
        }

        return $items;
    }

    /**
     * Возращает список языков сайта
     * 
     * @return array
     */
    protected function getLanguages()
    {
        $items = array();
        $list = $this->config->getFromCms('Site', 'LANGUAGES');
        foreach ($list as $key => $item) {
            $items[] = array($key, $item['title']);
        }

        return $items;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // название шаблона
            array('name' => 'template_name', 'type' => 'string'),
            // название категории
            array('name' => 'category_name', 'type' => 'string'),
            // описание шаблона
            array('name' => 'template_description', 'type' => 'string'),
            // название файла шаблона
            array('name' => 'template_filename', 'type' => 'string'),
            // права доступа
            array('name' => 'file_perms', 'type' => 'string'),
            // обозначение
            array('name' => 'template_icon', 'type' => 'string')
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $this->columns = array(
            array('xtype'     => 'expandercolumn',
                  'url'       => $this->componentUrl . 'grid/expand/',
                  'typeCt'    => 'row'),
            array('xtype'     => 'numbercolumn'),
            array('xtype'     => 'rowmenu'),
            array('dataIndex' => 'template_name',
                  'header'    => '<em class="mn-grid-hd-details"></em>' . $this->_['header_template_name'],
                  'width'     => 200,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'category_name',
                  'header'    => $this->_['header_category_name'],
                  'width'     => 160,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'template_icon',
                  'header'    => '&nbsp;',
                  'fixed'     => true,
                  'hideable'  => false,
                  'width'     => 25,
                  'sortable'  => false,
                  'menuDisabled' => true),
            array('dataIndex' => 'template_filename',
                  'header'    => $this->_['header_template_filename'],
                  'width'     => 160,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'file_perms',
                  'header'    => $this->_['header_file_perms'],
                  'width'     => 115,
                  'sortable'  => false),
            array('dataIndex' => 'template_description',
                  'header'    => $this->_['header_template_description'],
                  'width'     => 200,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false))
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_grid'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_grid'],
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'profileUrl'    => $this->componentUrl . 'profile/')
        );

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "edit" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "добавить" (ExtJS class "Manager.button.InsertData")
        $group->items->add(array('xtype' => 'mn-btn-insert-data', 'gridId' => $this->classId));
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' => $this->classId));
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'      => 'mn-btn-clear-data',
                  'msgConfirm' => $this->_['msg_btn_clear'],
                  'gridId'     => $this->classId,
                  'isN'        => true)
        );
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = 'component-site-templates';
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "фильтр" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Filter(
            array('title'  => $this->_['title_buttongroup_filter'],
                  'gridId' => $this->classId)
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(0);
        $btn['gridId'] = $this->classId;
        $btn['url'] = $this->componentUrl . 'search/data/';
        //$this->_cmp->tbar->items->add($group);
        
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $filter = $this->store->get('tlbFilter', array());
        $group->items->add(array(
            'xtype'       => 'form',
            'labelWidth'  => 49,
            'bodyStyle'   => 'border:1px solid transparent;background-color:transparent',
            'frame'       => false,
            'bodyBorder ' => false,
            'items'       => array(
                array('xtype'         => 'combo',
                      'fieldLabel'    => $this->_['label_theme'],
                      'name'          => 'theme',
                      'checkDirty'    => false,
                      'editable'      => false,
                      'width'         => 120,
                      'typeAhead'     => false,
                      'triggerAction' => 'all',
                      'mode'          => 'local',
                      'store'         => array(
                          'xtype'  => 'arraystore',
                          'fields' => array('value', 'list'),
                          'data'   => $this->getThemes()
                      ),
                      'value'         => empty($filter) ? '' : $filter['theme'],
                      'hiddenName'    => 'theme',
                      'valueField'    => 'value',
                      'displayField'  => 'list',
                      'allowBlank'    => false),
                array('xtype'         => 'combo',
                      'fieldLabel'    => $this->_['label_language'],
                      'name'          => 'language',
                      'checkDirty'    => false,
                      'editable'      => false,
                      'width'         => 120,
                      'typeAhead'     => false,
                      'triggerAction' => 'all',
                      'mode'          => 'local',
                      'store'         => array(
                          'xtype'  => 'arraystore',
                          'fields' => array('value', 'list'),
                          'data'   => $this->getLanguages()
                      ),
                      'value'         => empty($filter) ? $this->config->getFromCms('LANGUAGE') : $filter['language'],
                      'hiddenName'    => 'language',
                      'valueField'    => 'value',
                      'displayField'  => 'list',
                      'allowBlank'    => false),
            )
        ));
        $this->_cmp->tbar->items->add($group);

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $cellInfo =
            '<div class="mn-grid-cell-tooltip-tl">{template_name}</div>'
          . '<div class="mn-grid-cell-tooltip">'
          . '<em>' . $this->_['header_template_filename'] . '</em>: <b>{template_filename}</b><br>'
          . '<em>' . $this->_['header_category_name'] . '</em>: <b>{category_name}</b><br>'
          . '<em>' . $this->_['header_template_description'] . '</em>: <b>{template_description}</b>'
          . '</div>';
        $this->_cmp->cellTips = array(
            array('field' => 'template_name', 'tpl' => $cellInfo),
            array('field' => 'category_name', 'tpl' => '{category_name}'),
            array('field' => 'template_filename', 'tpl' => '{template_filename}'),
            array('field' => 'template_description', 'tpl' => '{template_description}')
        );

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $items = array(
            array('text' => $this->_['rowmenu_edit'],
                  'icon' => $this->resourcePath . 'icon-item-edit.png',
                  'url'  => $this->componentUrl . 'profile/interface/'),
            array('xtype' => 'menuseparator'),
            array('text' => $this->_['rowmenu_text'],
                  'icon' => $this->resourcePath . 'icon-item-text.png',
                  'url'  => $this->componentUrl . 'translation/interface/'),
        );
        $this->_cmp->rowMenu = array('xtype' => 'mn-grid-rowmenu', 'items' => $items);

        // быстрый фильтр списка (ExtJs class "Manager.tree.GridFilter")
        $this->_slidePanel->cls = 'mn-tree-gridfilter';
        $this->_slidePanel->width = 250;
        $this->_slidePanel->initRoot = array(
            'text'     => 'Filter',
            'id'       => 'byRoot',
            'expanded' => true,
            'children' => array(
                array('text'     => $this->_['text_all_records'],
                      'value'    => 'all',
                      'expanded' => true,
                      'leaf'     => false,
                      'children' => array(
                        array('text'     => $this->_['text_by_date'],
                              'id'       => 'byDt',
                              'leaf'     => false,
                              'expanded' => true,
                              'children' => array(
                                  array('text'     => $this->_['text_by_day'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 'day'),
                                  array('text'     => $this->_['text_by_week'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true, 
                                        'value'    => 'week'),
                                  array('text'     => $this->_['text_by_month'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 'month')
                              )
                        ),
                        array('text'    => $this->_['text_by_category'],
                              'id'      => 'byCt',
                              'leaf'    => false),
                    )
                )
            )
        );

        parent::getInterface();
    }
}
?>