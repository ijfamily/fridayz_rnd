<?php
/**
 * Gear Manager
 *
 * Контроллер         "Развёрнутая запись шаблона"
 * Пакет контроллеров "Список шаблонов"
 * Группа пакетов     "Шаблоны"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_STemplates_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Expand');

/**
 * Развёрнутая запись шаблона
 * 
 * @category   Gear
 * @package    GController_STemplates_Grid
 * @subpackage Expand
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_STemplates_Grid_Expand extends GController_Grid_Expand
{
    /**
     * Каталог шаблонов
     *
     * @var string
     */
    public $pathTemplate = '../application/templates/';

    /**
     * Вывод данных в интерфейс
     * 
     * @return void
     */
    protected function dataExpand()
    {
        parent::dataAccessView();

        $data = $this->getAttributes();
        $this->response->data = $data;
    }

    /**
     * Возращает атрибуты записи
     * 
     * @return string
     */
    protected function getAttributes()
    {
        $data = '';
        $query = new GDb_Query();
        $sql = 
            'SELECT * FROM `site_templates` JOIN `site_template_categories` USING(`category_id`) WHERE `template_id`=' . $this->uri->id;
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        // шаблон
        $data = '<fieldset class="fixed"><label>' . $this->_['title_fieldset_common'] . '</label><ul>';
        $data .= '<li><label>' . $this->_['label_template_name'] . ':</label> ' . $rec['template_name'] . '</li>';
        $data .= '<li><label>' . $this->_['label_category_name'] . ':</label> ' . $rec['category_name'] . '</li>';
        $data .= '<li><label>' . $this->_['label_template_description'] . ':</label> ' . $rec['template_description'] . '</li>';
        $data .= '<li><label>' . $this->_['label_template_filename'] . ':</label> ' . $rec['template_filename'] . '</li>';
        $data .= '</ul></fieldset>';
        // языки
        $data .= '<fieldset class="fixed"><label>' . $this->_['title_fieldset_langs'] . '</label><ul>';
        $list = $this->config->getFromCms('Site', 'LANGUAGES');
        $filter = $this->store->get('tlbFilter');
        $data .= '<li><label>' . $list[$filter['language']]['title'] . '</label></li>';
        $data .= '</ul></fieldset>';
        // текст
        $data .= '<fieldset style="width:100%"><label>' . sprintf($this->_['title_fieldset_text'], $list[$filter['language']]['title']) . '</label>';
        // попытка чтения файла шаблона
        $filename = $this->pathTemplate . $filter['theme'] . '/' . $list[$filter['language']]['alias'] . '/' . $rec['template_filename'];
        // если шаблон не существует
        if (!file_exists($filename))
            $data .= '<div>' . $this->_['msg_template_not_exist'] . '</div>';
        else {
            $text = file_get_contents($filename);
            if ($text === false)
                $data .= '<div>' . $this->_['msg_template_error'] . '</div>';
            else
                $data .= '<div><textarea readonly="true" style="width:100%;height:400px;background-color:#E1EBFA;border:1px dashed #D0D0D0;font-size:12px;padding:5px;">' . htmlspecialchars ($text) . '</textarea></div>';
        }
        $data .= '</fieldset>';

        $data .= '<div class="wrap"></div>';
        $data = '<div class="mn-row-body border">' . $data . '<div class="wrap"></div></div>';

        return $data;
    }
}
?>