<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Шаблоны сайта',
    'rowmenu_edit' => 'Атрибуты шаблона',
    'rowmenu_text' => 'Текст шаблона',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'tooltip_grid'             => 'список шаблонов компонентов, страниц сайта',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">("Шаблоны сайта")</span> ?',
    'warningr_btn_copy'        => 'Необходимо выделить только одну запись!',
    'label_theme'              => 'Шаблон',
    'label_language'           => 'Язык',
    // столбцы
    'header_template_name'        => 'Название',
    'header_category_name'        => 'Категория',
    'header_template_description' => 'Описание',
    'header_template_filename'    => 'Файл',
    'header_languages_list'       => 'Язык',
    'header_file_perms'           => 'Права доступа',
    // развёрнутая запись
    'title_fieldset_common'      => 'О шаблоне',
    'label_template_name'        => 'Название',
    'label_category_name'        => 'Категория',
    'label_template_description' => 'Описание',
    'label_template_filename'    => 'Файл',
    'title_fieldset_langs'       => 'Доступные языки шаблона',
    'title_fieldset_text'        => 'Текст шаблона "%s"',
    // сообщения
    'msg_template_not_exist' => '[шаблон не существует]',
    'msg_template_error'     => '[ошибка чтения шаблона]',
    // быстрый фильтр
    'text_all_records' => 'Все шаблоны',
    'text_by_date'     => 'По дате',
    'text_by_day'      => 'За день',
    'text_by_week'     => 'За неделю',
    'text_by_month'    => 'За месяц',
    'text_by_year'     => 'По году',
    'text_by_category' => 'По категории'
);
?>