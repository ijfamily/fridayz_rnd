<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Рассылка писем',
    // поля формы
    'label_email_theme'    => 'Тема',
    'title_fieldset_text'  => 'Текст',
    'text_btn_send'        => 'Разослать',
    'tooltip_btn_send'     => 'Разослать письмо',
    // сообщения
    'title_mailing'        => 'Рассылка',
    'msg_success'          => 'Рассылка писем выполнена успешно!',
    'msg_send_message_err' => 'Невозможно отправить письмо "%s" (ошибка сервера)!',
    'msg_empty_mails'      => 'Нет почтовых адресов для рассылки!',
    'msg_exists_template'  => 'Шаблон письма для рассылки не существует!'
);
?>