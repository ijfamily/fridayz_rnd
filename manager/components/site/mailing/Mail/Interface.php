<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля рассылки"
 * Пакет контроллеров "Профиль рассылки"
 * Группа пакетов     "Сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SMailing_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля рассылки
 * 
 * @category   Gear
 * @package    GController_SMailing_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SMailing_Mail_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'titleEllipsis' => 45,
                  'id'            => $this->classId,
                  'gridId'        => 'gcontroller_smailing_grid',
                  'width'         => 600,
                  'autoHeight'    => true,
                  'state'         => 'none',
                  'resizable'     => false,
                  'stateful'      => false)
        );
        $this->_cmp->buttonsAdd = array(
            array('xtype'       => 'mn-btn-form-update',
                  'text'        => $this->_['text_btn_send'],
                  'tooltip'     => $this->_['tooltip_btn_send'],
                  'icon'        => $this->resourcePath . 'icon-btn-send.png',
                  'windowId'    => $this->classId,
                  'width'       => 97,
                  'closeWindow' => false)
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_email_theme'],
                  'name'       => 'email_theme',
                  'maxLength'  => 255,
                  'anchor'     => '100%',
                  'allowBlank' => false),
            array('xtype' => 'mn-field-separator', 'html' => $this->_['title_fieldset_text']),
            array('xtype'      => 'htmleditor',
                  'hideLabel'  => true,
                  'name'       => 'email_text',
                  'height'     => 300,
                  'anchor'     => '100%',
                  'allowBlank' => false)
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($items);
        $form->labelWidth = 50;
        $form->url = $this->componentUrl . 'mail/';

        parent::getInterface();
    }
}
?>