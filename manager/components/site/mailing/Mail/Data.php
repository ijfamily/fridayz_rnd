<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные для интерфейса профиля рассылки"
 * Пакет контроллеров "Профиль рассылки"
 * Группа пакетов     "Сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SMailing_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные для интерфейса профиля рассылки
 * 
 * @category   Gear
 * @package    GController_SMailing_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SMailing_Mail_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array('email_theme', 'email_text');

    /**
     * Первичный ключ таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'email_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_mailing';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_smailing_grid';

    /**
     * Шаблон письма
     *
     * @var string
     */
    public $mailTemplate = 'mailing_mail.tpl.php';

    /**
     * Вставка данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataInsert($params = array())
    {
        $this->dataAccessInsert();

        $this->response->setMsgResult($this->_['title_mailing'], $this->_['msg_success'], true);

        // файл шаблона
        $tpl = '../application/templates/default/ru/' . $this->mailTemplate;
        if (!file_exists($tpl))
            throw new GException('Error', $this->_['msg_exists_template']);
        $query = new GDb_Query();
        // список адресов
        $sql = 'SELECT * FROM `site_mailing` WHERE `email_enabled`=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        // если нет писем
        if ($query->getCountRecords() == 0)
            throw new GException('Error', $this->_['msg_empty_mails']);
        // шаблон письма
        $mailData = array(
            'type'     => 'template',
            'template' => $tpl,
            'from'     => GFactory::getConfig()->get('MAIL/HOST'),
            'subject'  => $this->input->get('email_theme', ''),
            'data'     => array('text' => $this->input->get('email_text'))
        );
        $table = new GDb_Table('site_mailing', 'email_id');
        // рассылка
        $mail = GFactory::getMail();
        while (!$query->eof()) {
            $rec = $query->next();
            $mailData['to'] = $rec['email_address'];
            if ($mail->send($mailData) == false)
                throw new GException('Error', sprintf($this->_['msg_send_message_err'], $rec['email_address']));
            // Обновить адреса
            if ($table->update(array('email_posted' => date('Y-m-d H:i:s')), $rec['email_id']) === false)
                throw new GSqlException();
        }
    }
}
?>