<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Рассылка',
    'tooltip_grid' => 'список e-mail адресов клиентов для рассылки',
    'rowmenu_edit' => 'Редактировать',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete"> '
                                . '("Рассылка")</span> ?',
    'text_btn_mail'            => 'Рассылка',
    'tooltip_btn_mail'         => 'Рассылка писем',
    // столбцы
    'header_email_address'  => 'E-mail адрес',
    'header_email_client'   => 'Имя клиента',
    'header_email_posted'   => 'Дата рассылки',
    'tooltip_email_posted'  => 'Дата последней рассылки',
    'tooltip_email_enabled' => 'Доступен для рассылки'
);
?>