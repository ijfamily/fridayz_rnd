<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Рассылка"
 * Группа пакетов     "Рассылка"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SMailing
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Рассылка
 * Описание: Рассылка писем
 * Меню:
 * ID класса: gcontroller_smailing_grid
 * Модуль: Сайт
 * Группа: Сайт
 * Очищать: нет
 * Ресурс
 *    Компонент: site/mailing/
 *    Контроллер: site/mailing/Grid/
 *    Интерфейс: grid/interface/
 *    Меню:
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске компонентов: нет
 *    В главном меню: нет
 */

return array(
    // {S}- модуль "Site" {R} - пакет контроллеров "Mailing" -> SMailing
    'clsPrefix' => 'SMailing',
    // использовать язык
    'language'  => 'ru-RU'
);
?>