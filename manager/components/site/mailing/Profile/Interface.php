<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля рассылки"
 * Пакет контроллеров "Профиль рассылки"
 * Группа пакетов     "Сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SMailing_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля рассылки
 * 
 * @category   Gear
 * @package    GController_SMailing_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SMailing_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'titleEllipsis' => 45,
                  'gridId'        => 'gcontroller_smailing_grid',
                  'width'         => 420,
                  'autoHeight'    => true,
                  'stateful'      => false,
                  'buttonsAdd'    => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-site-mailing',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_email_address'],
                  'name'       => 'email_address',
                  'maxLength'  => 255,
                  'anchor'     => '100%',
                  'allowBlank' => false),
            array('xtype'      => 'textfield',
                  'itemCls'    => 'mn-form-item-quiet',
                  'fieldLabel' => $this->_['label_email_client'],
                  'name'       => 'email_client',
                  'maxLength'  => 255,
                  'anchor'     => '100%',
                  'allowBlank' => true),
            array('xtype'      => 'mn-field-chbox',
                  'fieldLabel' => $this->_['label_email_enabled'],
                  'labelTip'   => $this->_['tip_email_enabled'],
                  'default'    => 0,
                  'name'       => 'email_enabled')
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($items);
        $form->labelWidth = 100;
        $form->url = $this->componentUrl . 'profile/';

        parent::getInterface();
    }
}
?>