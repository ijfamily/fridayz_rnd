<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные для интерфейса профиля рассылки"
 * Пакет контроллеров "Профиль рассылки"
 * Группа пакетов     "Сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SMailing_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные для интерфейса профиля рассылки
 * 
 * @category   Gear
 * @package    GController_SMailing_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SMailing_Profile_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array('email_address', 'email_client', 'email_enabled');

    /**
     * Первичный ключ таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'email_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_mailing';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_smailing_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_profile_update'], $record['email_address']);
    }

    /**
     * Проверка существования записи с одинаковыми значениями
     * 
     * @param  array $params array (field => value, ....)
     * @return void
     */
    protected function isDataExist($params)
    {
        if (isset($params['email_address'])) {
            $table = new GDb_Table($this->tableName, $this->idProperty);
            if ($table->isDataExist(array('email_address' => $params['email_address']), (int)$this->uri->id) === true)
                throw new GException('Warning', sprintf($this->_['msg_wrong_email_address'], $params['email_address']));
        }
    }
}
?>