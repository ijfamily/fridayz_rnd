<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные для интерфейса профиля СПАМа"
 * Пакет контроллеров "Профиль СПАМа"
 * Группа пакетов     "СПАМ"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SSpam_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные для интерфейса профиля СПАМа
 * 
 * @category   Gear
 * @package    GController_SSpam_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SSpam_Profile_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array('spam_ipaddress', 'spam_mask', 'spam_date');

    /**
     * Первичный ключ таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'spam_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_spam';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sspam_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_profile_update'], $record['spam_ipaddress']);
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        try {
            $params['spam_date'] = date('Y-m-d H:i:s');
            if (!empty($params['spam_ipaddress'])) {
                $ip = ip2long(trim($params['spam_ipaddress']));
                if ($ip == -1 || $ip === false)
                     throw new GException('Loading data', $this->_['msg_error_ip']);
                $params['spam_ipaddress'] = sprintf("%u", $ip);;
            }
            if (!empty($params['spam_mask'])) {
                $addr = trim($params['spam_mask']);
                if ($addr == '255.255.255.255') {
                    $params['spam_mask'] = 4294967295;
                    return;
                }
                $ip = ip2long($addr);
                if ($ip == -1 || $ip === false)
                     throw new GException('Loading data', $this->_['msg_error_ip']);
                $params['spam_mask'] = sprintf("%u", $ip);
            }
       } catch(GException $e) {}
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        $record['spam_ipaddress'] = long2ip($record['spam_ipaddress']);
        $record['spam_mask'] = long2ip($record['spam_mask']);

        return $record;
    }
}
?>