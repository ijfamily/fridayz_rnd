<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля СПАМа"
 * Пакет контроллеров "Профиль СПАМа"
 * Группа пакетов     "СПАМ"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SSpam_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля СПАМа
 * 
 * @category   Gear
 * @package    GController_SSpam_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SSpam_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'id'            => $this->classId,
                  'titleEllipsis' => 80,
                  'gridId'        => 'gcontroller_sspam_grid',
                  'width'         => 370,
                  'autoHeight'    => true,
                  'stateful'      => false,
                  'buttonsAdd'    => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-site-spam',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_spam_ipaddress'],
                  'value'      => $this->isUpdate ? '' : '192.168.126.1',
                  'name'       => 'spam_ipaddress',
                  'maxLength'  => 15,
                  'anchor'     => '100%',
                  'allowBlank' => false),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_spam_mask'],
                  'value'      => $this->isUpdate ? '' : '255.255.255.0',
                  'name'       => 'spam_mask',
                  'maxLength'  => 15,
                  'anchor'     => '100%',
                  'allowBlank' => false),
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($items);
        $form->url = $this->componentUrl . 'profile/';
        $form->labelWidth = 80;

        parent::getInterface();
    }
}
?>