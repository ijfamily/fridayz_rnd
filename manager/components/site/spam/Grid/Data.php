<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка СПАМа"
 * Пакет контроллеров "СПАМ"
 * Группа пакетов     "СПАМ"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SSpam_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные списка СПАМа
 * 
 * @category   Gear
 * @package    GController_SSpam_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SSpam_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'spam_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_spam';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS * '
          . 'FROM (SELECT `spam_id`, INET_NTOA(`spam_ipaddress`) `spam_ipaddress`, '
          . 'INET_NTOA(`spam_mask`) `spam_mask`, `spam_date` FROM `site_spam` ) `t`'
          . 'WHERE 1 %filter '
          . 'ORDER BY %sort '
          . 'LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // ip адрес
            'spam_ipaddress' => array('type' => 'string'),
            // маска
            'spam_mask' => array('type' => 'string'),
            // дата
            'spam_date' => array('type' => 'string'),
        );
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

         $query = new GDb_Query();
        // удаление записей таблицы "site_spam"
        if ($query->clear('site_spam') === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_spam') === false)
            throw new GSqlException();
    }
}
?>