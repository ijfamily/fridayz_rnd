<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля очистки данных"
 * Пакет контроллеров "Мастер очистки данных"
 * Группа пакетов     "Мастер очистки данных"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SConfig_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные профиля очистки данных
 * 
 * @category   Gear
 * @package    GController_SConfig_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SCleaner_Profile_Data extends GController_Profile_Data
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_scleaner_profile';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);


    }

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return $this->_['profile_title_update'];
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        parent::dataAccessView();
    }

    /**
     * Очистка 
     * 
     * @return void
     */
    protected function cleanCache()
    {
        Gear::library('File');

        // удаление изображений статьи
        GDir::clear('../cache/', array('index.html'), true);
    }

    /**
     * Очистка 
     * 
     * @return void
     */
    protected function cleanSysCache()
    {}

    /**
     * Очистка 
     * 
     * @return void
     */
    protected function cleanArticles()
    {
        // удаление комментариев статей
        if ($this->_query->clear('site_articles') === false)
            throw new GSqlException();
        if ($this->_query->dropAutoIncrement('site_articles') === false)
            throw new GSqlException();
        // удаление комментариев статей
        if ($this->_query->clear('site_pages') === false)
            throw new GSqlException();
        if ($this->_query->dropAutoIncrement('site_pages') === false)
            throw new GSqlException();
    }

    /**
     * Очистка 
     * 
     * @return void
     */
    protected function cleanArticleImages()
    {
        Gear::library('File');

        // удаление изображений статьи
        GDir::clear(DOCUMENT_ROOT . $this->config->getFromCms('Site', 'DIR/IMAGES'), array('index.html'), true);
    }

    /**
     * Очистка 
     * 
     * @return void
     */
    protected function cleanLandings()
    {
        if ($this->_query->clear('site_landing_pages') === false)
            throw new GSqlException();
        if ($this->_query->dropAutoIncrement('site_landing_pages') === false)
            throw new GSqlException();
    }

    /**
     * Очистка 
     * 
     * @return void
     */
    protected function cleanArticleCategories()
    {
        if ($this->_query->clear('site_categories') === false)
            throw new GSqlException();
        if ($this->_query->dropAutoIncrement('site_categories') === false)
            throw new GSqlException();
        // создание основной категории
        $sql = 'INSERT INTO `site_categories` (`category_name`, `category_left`, `category_right`, `category_level`, `category_visible`, `sys_record`) VALUES '
              . "('Основная категория', 1, 2, 1, 1, 1)";
        if ($this->_query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Очистка 
     * 
     * @return void
     */
    protected function cleanProfileImages()
    {
        if ($this->_query->clear('site_image_profiles') === false)
            throw new GSqlException();
        if ($this->_query->dropAutoIncrement('site_image_profiles') === false)
            throw new GSqlException();
    }

    /**
     * Очистка 
     * 
     * @return void
     */
    protected function cleanRobots()
    {
        if ($this->_query->clear('site_bots') === false)
            throw new GSqlException();
        if ($this->_query->dropAutoIncrement('site_bots') === false)
            throw new GSqlException();
    }

    /**
     * Очистка 
     * 
     * @return void
     */
    protected function cleanSpam()
    {
        if ($this->_query->clear('site_spam') === false)
            throw new GSqlException();
        if ($this->_query->dropAutoIncrement('site_spam') === false)
            throw new GSqlException();
    }

    /**
     * Очистка 
     * 
     * @return void
     */
    protected function cleanGalleries()
    {
        //
        if ($this->_query->clear('site_gallery') === false)
            throw new GSqlException();
        if ($this->_query->dropAutoIncrement('site_gallery') === false)
            throw new GSqlException();
        //
        if ($this->_query->clear('site_gallery_images') === false)
            throw new GSqlException();
        if ($this->_query->dropAutoIncrement('site_gallery_images') === false)
            throw new GSqlException();
        //
        if ($this->_query->clear('site_gallery_images_l') === false)
            throw new GSqlException();
        if ($this->_query->dropAutoIncrement('site_gallery_images_l') === false)
            throw new GSqlException();

        // удаление изображений статьи
        GDir::clear(DOCUMENT_ROOT . $this->config->getFromCms('Galleries', 'DIR'), array('index.html'), true);
    }

    /**
     * Очистка 
     * 
     * @return void
     */
    protected function cleanSliders()
    {
        //
        if ($this->_query->clear('site_sliders') === false)
            throw new GSqlException();
        if ($this->_query->dropAutoIncrement('site_sliders') === false)
            throw new GSqlException();
        //
        if ($this->_query->clear('site_slider_images') === false)
            throw new GSqlException();
        if ($this->_query->dropAutoIncrement('site_slider_images') === false)
            throw new GSqlException();

        // удаление изображений статьи
        GDir::clear(DOCUMENT_ROOT . $this->config->getFromCms('Site', 'DIR/SLIDERS'), array('index.html'), true);
    }

    /**
     * Очистка 
     * 
     * @return void
     */
    protected function cleanMenu()
    {
        // удаление записей меню
        if ($this->_query->clear('site_menu') === false)
            throw new GSqlException();
        if ($this->_query->dropAutoIncrement('site_menu') === false)
            throw new GSqlException();
        // удаление пунктов меню
        if ($this->_query->clear('site_menu_items') === false)
            throw new GSqlException();
        if ($this->_query->dropAutoIncrement('site_menu_items') === false)
            throw new GSqlException();
        // удаление локализации
        if ($this->_query->clear('site_menu_items_l') === false)
            throw new GSqlException();
        if ($this->_query->dropAutoIncrement('site_menu_items_l') === false)
            throw new GSqlException();
        //
        $sql = 'INSERT INTO `site_menu_items` (`item_index`, `item_visible`) VALUES (1, 1)';
        if ($this->_query->execute($sql) === false)
            throw new GSqlException();
        $sql = "INSERT INTO `site_menu_items_l` (`language_id`, `item_id`, `item_name`) VALUES (1, 1, 'Меню сайта')";
        if ($this->_query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Очистка 
     * 
     * @return void
     */
    protected function cleanFeedback()
    {
        if ($this->_query->clear('site_feedback') === false)
            throw new GSqlException();
        if ($this->_query->dropAutoIncrement('site_feedback') === false)
            throw new GSqlException();
    }

    /**
     * Очистка 
     * 
     * @return void
     */
    protected function cleanMailing()
    {
        if ($this->_query->clear('site_mailing') === false)
            throw new GSqlException();
        if ($this->_query->dropAutoIncrement('site_mailing') === false)
            throw new GSqlException();
    }

    /**
     * Очистка 
     * 
     * @return void
     */
    protected function cleanJournalAttends()
    {
        if ($this->_query->clear('gear_user_attends') === false)
            throw new GSqlException();
        if ($this->_query->dropAutoIncrement('gear_user_attends') === false)
            throw new GSqlException();
    }

    /**
     * Очистка 
     * 
     * @return void
     */
    protected function cleanJournalRestore()
    {
        if ($this->_query->clear('gear_user_restore') === false)
            throw new GSqlException();
        if ($this->_query->dropAutoIncrement('gear_user_restore') === false)
            throw new GSqlException();
    }

    /**
     * Очистка 
     * 
     * @return void
     */
    protected function cleanJournalEscapes()
    {
        if ($this->_query->clear('gear_user_escapes') === false)
            throw new GSqlException();
        if ($this->_query->dropAutoIncrement('gear_user_escapes') === false)
            throw new GSqlException();
    }

    /**
     * Очистка 
     * 
     * @return void
     */
    protected function cleanJournalLog()
    {
        if ($this->_query->clear('gear_log') === false)
            throw new GSqlException();
        if ($this->_query->dropAutoIncrement('gear_log') === false)
            throw new GSqlException();
    }

    /**
     * Очистка 
     * 
     * @return void
     */
    protected function cleanJournalQuery()
    {
        if ($this->_query->clear('gear_log_sql') === false)
            throw new GSqlException();
        if ($this->_query->dropAutoIncrement('gear_log_sql') === false)
            throw new GSqlException();
    }

    /**
     * Очистка 
     * 
     * @return void
     */
    protected function cleanJournalScripts()
    {
        if ($this->_query->clear('gear_log_script') === false)
            throw new GSqlException();
        if ($this->_query->dropAutoIncrement('gear_log_script') === false)
            throw new GSqlException();
    }

    /**
     * Очистка 
     * 
     * @return void
     */
    protected function cleanJournalAuth()
    {
        if ($this->_query->clear('gear_user_authentication') === false)
            throw new GSqlException();
        if ($this->_query->dropAutoIncrement('gear_user_authentication') === false)
            throw new GSqlException();
    }

    /**
     * Очистка 
     * 
     * @return void
     */
    protected function cleanPost()
    {
        if ($this->_query->clear('gear_user_mails') === false)
            throw new GSqlException();
        if ($this->_query->dropAutoIncrement('gear_user_mails') === false)
            throw new GSqlException();
    }

    /**
     * Очистка 
     * 
     * @return void
     */
    protected function cleanMessages()
    {
        if ($this->_query->clear('gear_user_messages') === false)
            throw new GSqlException();
        if ($this->_query->dropAutoIncrement('gear_user_messages') === false)
            throw new GSqlException();
    }

    /**
     * Очистка 
     * 
     * @return void
     */
    protected function cleanDocs()
    {
        if ($this->_query->clear('site_documents') === false)
            throw new GSqlException();
        if ($this->_query->dropAutoIncrement('site_documents') === false)
            throw new GSqlException();

        // удаление документов
        GDir::clear(DOCUMENT_ROOT . $this->config->getFromCms('Site', 'DIR/DOCS'), array('index.html'), true);

    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        parent::dataAccessUpdate();

        $this->response->set('action', 'delete');
        $this->response->setMsgResult('Deleting data', $this->_['msg_success'], true);

        $this->_query = new GDb_Query();
        $clean = $this->input->get('clean', array());
        if (empty($clean))
            throw new GException('Warning', 'To execute a query, you must change data!');
        $key = $this->input->get('key');
        if (empty($key))
            throw new GException('Warning', $this->_['msg_empty_key']);
        //if ($key != $this->config->getFrom('SITE', 'CMS/TOKEN'))
            //throw new GException('Warning', $this->_['msg_key_wrong']);

        foreach ($clean as $key => $value) {
            $func = 'clean' . $key;
            if (method_exists($this, $func))
                $this->$func();
        }
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($params, 'method "PUT"');
        }
    }
}
?>