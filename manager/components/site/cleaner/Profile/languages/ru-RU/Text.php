<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_update' => 'Мастер очистки данных',
    'text_btn_update'      => 'Очистить',
    'text_btn_help'        => 'Справка',
    // поля формы
    'label_fs_cache' => 'Кэш',
    'label_fs_references' => 'Справочники',
    'label_fs_articles' => 'Статьи',
    'label_fs_list' => 'Списки / журналы',
    'label_fs_components' => 'Компоненты сайта',
    'label_fs_mails' => 'Рассылка / письма',
    'label_fs_admin' => 'Администрирование',
    'label_cache' => 'Кэш',
    'label_sys_cache' => 'Системный кэш',
    'label_galleries' => 'Галереи изображений',
    'label_sliders' => 'Слайдеры',
    'label_articles' => 'Статьи',
    'label_articles_images' => 'Изображения статей',
    'label_landing' => 'Landing страницы',
    'label_menu' => 'Меню сайта',
    'label_ref_categories' => 'Справочник категорий статей',
    'label_ref_images' => 'Справочник профилей изображений',
    'label_feedback' => 'Данные обратной связи',
    'label_mails' => 'Данные рассылки',
    'label_docs' => 'Документы',
    'label_robots' => 'Журнал посещений сайта роботами',
    'label_spam' => 'Список заблокированных IP адресов для отправки сообщений',
    'label_journal_attends' => 'Журнал авторизации пользователей',
    'label_journal_restore' => 'Журнал восстановления учетных записей',
    'label_journal_escapes' => 'Журнал выхода пользователей',
    'label_journal_log' => 'Журнал действий пользователей',
    'label_journal_query' => 'Журнал ошибок запросов пользователей',
    'label_journal_scripts' => 'Журнал ошибок скриптов',
    'label_journal_auth' => 'Журнал аутентификации пользователей',
    'label_post' => 'Почта',
    'label_messages' => 'Сообщения',
    'label_key' => 'Ключ безопасности',
    // сообщения
    'msg_key_wrong' => 'Невозможно выполнить очистку данных (неверный ключ)!',
    'msg_empty_key' => 'Введите ключ!',
    'msg_success' => 'Успешно выполнена очистка данных!',
);
?>