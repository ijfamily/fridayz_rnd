<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля очистки данных"
 * Пакет контроллеров "Мастер очистки данных"
 * Группа пакетов     "Мастер очистки данных"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SConfig_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля очистки данных
 * 
 * @category   Gear
 * @package    GController_SConfig_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SCleaner_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // Ext_Window (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'           => $this->_['title_profile_update'],
                  'titleEllipsis'   => 40,
                  'width'           => 750,
                  'autoHeight'      => true,
                  'resizable'       => false,
                  'btnDeleteHidden' => true,
                  'stateful'        => false,
                  'btnUpdateIcon'   => 'icon-fbtn-setup',
                  'btnUpdateText'   => $this->_['text_btn_update'],
                  'buttonsAdd'    => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-site-cleaner',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );
        $this->_cmp->state = 'update';

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->url = $this->componentUrl . 'profile/';
        $form->labelWidth = 127;
        $form->items->addItems(array(
            array('xtype'  => 'container',
                  'layout' => 'column',
                  //'anchor' => '100% 100%',
                  'cls'    => 'mn-container-clean',
                  'style'  => 'background: url("' . $this->resourcePath. 'bg-interface.png") no-repeat center 10px;margin-bottom:20px;',
                  'items'  => array(
                      array('layout'      => 'form',
                            'columnWidth' => 0.32,
                            'items'       => array(
                                array('xtype' => 'mn-field-separator', 'html' => $this->_['label_fs_cache']),
                                array('xtype'      => 'checkbox',
                                      'boxLabel'   => $this->_['label_cache'],
                                      'hideLabel'  => true,
                                      'checkDirty' => false,
                                      'checked'    => true,
                                      'name'       => 'clean[Cache]'),
                                array('xtype'      => 'checkbox',
                                      'boxLabel'   => $this->_['label_sys_cache'],
                                      'hideLabel'  => true,
                                      'checkDirty' => false,
                                      'checked'    => true,
                                      'name'       => 'clean[SysCache]'),
                                array('xtype' => 'mn-field-separator', 'html' => $this->_['label_fs_articles']),
                                array('xtype'      => 'checkbox',
                                      'boxLabel'   => $this->_['label_articles'],
                                      'hideLabel'  => true,
                                      'checkDirty' => false,
                                      'checked'    => true,
                                      'name'       => 'clean[Articles]'),
                                array('xtype'      => 'checkbox',
                                      'boxLabel'   => $this->_['label_landing'],
                                      'hideLabel'  => true,
                                      'checkDirty' => false,
                                      'checked'    => true,
                                      'name'       => 'clean[Landings]'),
                                array('xtype'      => 'checkbox',
                                      'boxLabel'   => $this->_['label_articles_images'],
                                      'hideLabel'  => true,
                                      'checkDirty' => false,
                                      'checked'    => true,
                                      'name'       => 'clean[articleImages]'),
                                array('xtype'      => 'checkbox',
                                      'boxLabel'   => $this->_['label_docs'],
                                      'hideLabel'  => true,
                                      'checkDirty' => false,
                                      'checked'    => true,
                                      'name'       => 'clean[Docs]'),
                                array('xtype' => 'mn-field-separator', 'html' => $this->_['label_fs_references']),
                                array('xtype'      => 'checkbox',
                                      'boxLabel'   => $this->_['label_ref_categories'],
                                      'hideLabel'  => true,
                                      'checkDirty' => false,
                                      'checked'    => true,
                                      'name'       => 'clean[ArticleCategories]'),
                                array('xtype'      => 'checkbox',
                                      'boxLabel'   => $this->_['label_ref_images'],
                                      'hideLabel'  => true,
                                      'checkDirty' => false,
                                      'checked'    => true,
                                      'name'       => 'clean[ProfileImages]')
                                )
                            ),
                      array('layout'      => 'form',
                            'columnWidth' => 0.32,
                            'items'       => array(
                                array('xtype' => 'mn-field-separator', 'html' => $this->_['label_fs_list']),
                                array('xtype'      => 'checkbox',
                                      'boxLabel'   => $this->_['label_robots'],
                                      'hideLabel'  => true,
                                      'checkDirty' => false,
                                      'checked'    => true,
                                      'name'       => 'clean[Robots]'),
                                array('xtype'      => 'checkbox',
                                      'boxLabel'   => $this->_['label_spam'],
                                      'hideLabel'  => true,
                                      'checkDirty' => false,
                                      'checked'    => true,
                                      'name'       => 'clean[Spam]'),
                                array('xtype' => 'mn-field-separator', 'html' => $this->_['label_fs_components']),
                                array('xtype'      => 'checkbox',
                                      'boxLabel'   => $this->_['label_galleries'],
                                      'hideLabel'  => true,
                                      'checkDirty' => false,
                                      'checked'    => true,
                                      'name'       => 'clean[Galleries]'),
                                array('xtype'      => 'checkbox',
                                      'boxLabel'   => $this->_['label_sliders'],
                                      'hideLabel'  => true,
                                      'checkDirty' => false,
                                      'checked'    => true,
                                      'name'       => 'clean[Sliders]'),
                                array('xtype'      => 'checkbox',
                                      'boxLabel'   => $this->_['label_menu'],
                                      'hideLabel'  => true,
                                      'checkDirty' => false,
                                      'checked'    => true,
                                      'name'       => 'clean[Menu]'),
                                array('xtype' => 'mn-field-separator', 'html' => $this->_['label_fs_mails']),
                                array('xtype'      => 'checkbox',
                                      'boxLabel'   => $this->_['label_feedback'],
                                      'hideLabel'  => true,
                                      'checkDirty' => false,
                                      'checked'    => true,
                                      'name'       => 'clean[Feedback]'),
                                array('xtype'      => 'checkbox',
                                      'boxLabel'   => $this->_['label_mails'],
                                      'hideLabel'  => true,
                                      'checkDirty' => false,
                                      'checked'    => true,
                                      'name'       => 'clean[Mailing]')
                                )
                            ),
                      array('layout'      => 'form',
                            'columnWidth' => 0.32,
                            'items'       => array(
                                array('xtype' => 'mn-field-separator', 'html' => $this->_['label_fs_admin']),
                                array('xtype'      => 'checkbox',
                                      'boxLabel'   => $this->_['label_journal_attends'],
                                      'hideLabel'  => true,
                                      'checkDirty' => false,
                                      'checked'    => true,
                                      'name'       => 'clean[JournalAttends]'),
                                array('xtype'      => 'checkbox',
                                      'boxLabel'   => $this->_['label_journal_restore'],
                                      'hideLabel'  => true,
                                      'checkDirty' => false,
                                      'checked'    => true,
                                      'name'       => 'clean[JournalRestore]'),
                                array('xtype'      => 'checkbox',
                                      'boxLabel'   => $this->_['label_journal_escapes'],
                                      'hideLabel'  => true,
                                      'checkDirty' => false,
                                      'checked'    => true,
                                      'name'       => 'clean[JournalEscapes]'),
                                array('xtype'      => 'checkbox',
                                      'boxLabel'   => $this->_['label_journal_log'],
                                      'hideLabel'  => true,
                                      'checkDirty' => false,
                                      'checked'    => true,
                                      'name'       => 'clean[JournalLog]'),
                                array('xtype'      => 'checkbox',
                                      'boxLabel'   => $this->_['label_journal_query'],
                                      'hideLabel'  => true,
                                      'checkDirty' => false,
                                      'checked'    => true,
                                      'name'       => 'clean[JournalQuery]'),
                                array('xtype'      => 'checkbox',
                                      'boxLabel'   => $this->_['label_journal_scripts'],
                                      'hideLabel'  => true,
                                      'checkDirty' => false,
                                      'checked'    => true,
                                      'name'       => 'clean[JournalScripts]'),
                                array('xtype'      => 'checkbox',
                                      'boxLabel'   => $this->_['label_journal_auth'],
                                      'hideLabel'  => true,
                                      'checkDirty' => false,
                                      'checked'    => true,
                                      'name'       => 'clean[JournalAuth]'),
                                array('xtype'      => 'checkbox',
                                      'boxLabel'   => $this->_['label_post'],
                                      'hideLabel'  => true,
                                      'checkDirty' => false,
                                      'checked'    => true,
                                      'name'       => 'clean[Post]'),
                                array('xtype'      => 'checkbox',
                                      'boxLabel'   => $this->_['label_messages'],
                                      'hideLabel'  => true,
                                      'checkDirty' => false,
                                      'checked'    => true,
                                      'name'       => 'clean[Messages]'),
                                )
                        )
                  )
            ),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_key'],
                  'name'       => 'key',
                  'width'     => 250,
                  'allowBlank' => false),
        ));

        parent::getInterface();
    }
}
?>