<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Мастер очистки данных"
 * Группа пакетов     "Мастер очистки данных"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SCleaner
 * @copyright  Copyright (c) 2013-2016 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Мастер очистки данных
 * Описание: Мастер очистки данных
 * Меню: Мастер очистки данных
 * ID класса: gcontroller_scleaner_profile
 * Модуль: Сайт
 * Группа: Сайт
 * Очищать: нет
 * Статистика компонента: нет
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске компонентов: нет
 *    В главном меню: нет
 * Ресурс
 *    Компонент: site/cleaner/
 *    Контроллер: site/cleaner/Profile/
 *    Интерфейс: profile/interface/
 *    Меню:
 */

return array(
    // {S}- модуль "Site" {Cleaner} - пакет контроллеров "Site cleaner" -> SCleaner
    'clsPrefix' => 'SCleaner',
    // использорвать язык
    'language'  => 'ru-RU'
);
?>