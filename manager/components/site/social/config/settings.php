<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Настройка социальных сетей"
 * Группа пакетов     "Социальные сети"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SSocialConfig
 * @copyright  Copyright (c) 2013-2016 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Настройка социальных сетей
 * Описание: Настройка социальных сетей
 * Меню: Настройка социальных сетей
 * ID класса: gcontroller_ssocialconfig_profile
 * Модуль: Сайт
 * Группа: Сайт/ Настройки
 * Очищать: нет
 * Статистика компонента: нет 
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске компонентов: нет
 *    В главном меню: нет
 * Ресурс
 *    Компонент: site/social/config/
 *    Контроллер: site/social/config/Profile/
 *    Интерфейс: profile/interface/
 *    Меню:
 */

return array(
    // {S}- модуль "Site" {Galleries} - пакет контроллеров "Social networks config" -> SSocialConfig
    'clsPrefix' => 'SSocialConfig',
    // использовать язык
    'language'  => 'ru-RU'
);
?>