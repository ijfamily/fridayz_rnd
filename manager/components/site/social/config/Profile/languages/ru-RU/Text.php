<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_update' => 'Настройка социальных сетей',
    'text_btn_help'        => 'Справка',
    // поля
    'html_desc'   => '<header>Настройка авторизации пользователей с использованием социальных сетей</header>'
                   . '<note>Для полноценной работы данного вида авторизации, вам необходимо в социальной сети создать свое приложение для авторизации, включить поддержку авторизации в данном модуле, '
                   . 'и задать необходимые ключи вашего приложения в социальной сети.<br><br></note>',
    'header_alias'  => 'Псевдоним',
    'header_name'   => 'Название',
    'label_id'      => 'ID приложения<hb></hb>',
    'tip_id'        => 'ID приложения в сети %s',
    'label_key'     => 'Публичный ключ<hb></hb>',
    'tip_key'       => 'Публичный ключ в сети %s',
    'label_secret'  => 'Защищенный ключ<hb></hb>',
    'tip_secret'    => 'Защищенный ключ в сети %s',
    'label_enabled' => 'Включить авторизацию<hb></hb>',
    'tip_enabled'   => 'Включить авторизацию с использованием сети %s',
    'title_fieldset_provider' => 'Авторизация с использованием сети %s',
    // сообщения
    'msg_file_exist'     => 'Невозможно открыть файл настроек "%s"',
    'msg_empty_settings' => 'Файл "%s" не содержит настройки социальных сетей!'
);
?>