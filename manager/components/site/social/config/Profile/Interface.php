<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля настройки списка MIMES"
 * Пакет контроллеров "Настройка списка MIMES"
 * Группа пакетов     "Настройки"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SMimes_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
define('_INC', 1);
Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля настройки списка MIMES
 * 
 * @category   Gear
 * @package    GController_SMimes_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SSocialConfig_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Файл настроек роботов
     *
     * @var string
     */
    public $filename = 'Social.php';

    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        $filename = '../' . PATH_CONFIG_CMS . $this->filename;
        // если файл настроек сайта не существует
        if (!file_exists($filename))
            throw new GException('Error', sprintf($this->_['msg_file_exist'], $filename));

        // настройки доменов
        $settings = include($filename);
        $interface = array(
            array('xtype' => 'label', 'html' => $this->_['html_desc'])
        );
        if (empty($settings))
            throw new GException('Error', sprintf($this->_['msg_empty_settings'], $filename));

        if ($settings) {
            foreach ($settings as $alias => $provider) {
                if (!isset($provider['name']))
                    $provider['name'] = $alias;
                $item = array(
                    'xtype'      => 'fieldset',
                    'labelWidth'  => 147,
                    'width'       => 550,
                    'collapsible' => true,
                    'collapsed'   => !(int) $provider['enabled'],
                    'title'      => sprintf($this->_['title_fieldset_provider'], $provider['name']),
                    'autoHeight' => true,
                    'items'      => array(
                        array('xtype'      => 'hidden',
                              'name'       => 'providers[' . $alias . '][name]',
                              'value'      => $provider['name'],
                              'checkDirty' => false)
                    )
                );
                if (isset($provider['keys']['id']))
                    $item['items'][] = array(
                        'xtype'      => 'textfield',
                        'fieldLabel' => $this->_['label_id'],
                        'labelTip'   => sprintf($this->_['tip_id'], $provider['name']),
                        'name'       => 'providers[' . $alias . '][id]',
                        'value'      => $provider['keys']['id'],
                        'anchor'     => '100%',
                        'checkDirty' => false,
                        'allowBlank' => true
                    );
                if (isset($provider['keys']['secret']))
                    $item['items'][] = array(
                        'xtype'      => 'textfield',
                        'fieldLabel' => $this->_['label_secret'],
                        'labelTip'   => sprintf($this->_['tip_secret'], $provider['name']),
                        'name'       => 'providers[' . $alias . '][secret]',
                        'value'      => $provider['keys']['secret'],
                        'anchor'     => '100%',
                        'checkDirty' => false,
                        'allowBlank' => true
                    );
                if (isset($provider['keys']['key']))
                    $item['items'][] = array(
                        'xtype'      => 'textfield',
                        'fieldLabel' => $this->_['label_key'],
                        'labelTip'   => sprintf($this->_['tip_key'], $provider['name']),
                        'name'       => 'providers[' . $alias . '][key]',
                        'value'      => $provider['keys']['key'],
                        'anchor'     => '100%',
                        'checkDirty' => false,
                        'allowBlank' => true
                    );
                $item['items'][] = array(
                    'xtype'      => 'mn-field-chbox',
                    'fieldLabel' => $this->_['label_enabled'],
                    'labelTip'   => sprintf($this->_['tip_enabled'], $provider['name']),
                    'value'      => (int) $provider['enabled'],
                    'checkDirty' => false,
                    'name'       => 'providers[' . $alias . '][enabled]'
                );

                //GFactory::getDg()->log($provider);
                $interface[] = $item;
            }
        }

        return $interface;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // Ext_Window (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'           => $this->_['title_profile_update'],
                  'titleEllipsis'   => 40,
                  'width'           => 600,
                  'height'          => 500,
                  'resizable'       => false,
                  'btnDeleteHidden' => true,
                  'stateful'        => false,
                  'buttonsAdd'      => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-site-social-config',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );
        $this->_cmp->state = 'update';

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->url = $this->componentUrl . 'profile/';
        $form->autoScroll = true;

        $form->items->addItems(
            $data
        );
        /*
            array(
                array('xtype' => 'label',
                      'html'  => $this->_['html_desc']),
                array('xtype'     => 'mn-field-grid-editor',
                      'id'        => 'list_social',
                      'name'      => 'list_social',
                      'hideLabel' => true,
                      'anchor'    => '100%',
                      'height'    => 400,
                      'checkDirty' => false,
                      'fields'    => array('alias', 'name', 'id', 'key', 'secret', 'enabled'),
                      'columns'   => array(
                          array('header'    => $this->_['header_alias'],
                                'dataIndex' => 'alias',
                                'width'     => 120,
                                'sortable'  => true),
                          array('header'    => $this->_['header_name'],
                                'dataIndex' => 'name',
                                'width'     => 120,
                                'sortable'  => true),
                          array('header'    => $this->_['header_id'],
                                'editor' => array('xtype' =>'textfield','allowBlank' => true),
                                'dataIndex' => 'id',
                                'width'     => 100,
                                'sortable'  => true),
                          array('header'    => $this->_['header_key'],
                                'editor' => array('xtype' =>'textfield','allowBlank' => true),
                                'dataIndex' => 'key',
                                'width'     => 170,
                                'sortable'  => true),
                          array('header'    => $this->_['header_secret'],
                                'dataIndex' => 'secret',
                                'editor' => array('xtype' =>'textfield', 'allowBlank' => true),
                                'width'     => 170,
                                'sortable'  => true),
                          array('xtype'     => 'booleancolumn',
                                'header'    => $this->_['header_enabled'],
                                'tooltip'   => $this->_['tooltip_enabled'],
                                'editor'    => array('xtype' => 'checkbox'),
                                'dataIndex' => 'enabled',
                                'width'     => 80,
                                'sortable'  => true)
                      )
                )
            )
        );
        */

        parent::getInterface();
    }
}
?>