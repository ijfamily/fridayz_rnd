<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля настройки списка MIMES"
 * Пакет контроллеров "Настройка списка MIMES"
 * Группа пакетов     "Настройки"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SMimes_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

define('_INC', 1);

Gear::library('String');
Gear::controller('Profile/Data');

/**
 * Возращает шаблон настройки Социальных сетей
 * 
 * @params string $tpl данные в шаблоне
 */
function getTplSiteConfig($tpl)
{
    return <<<TEXT
<?php
/**
 * Gear CMS
 *
 * Настройка списка Социальных сетей (создан: {$tpl['date']})
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 *
 * @category   Gear
 * @package    Config
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    \$Id: Social.php {$tpl['date']} Gear Magic \$
 */

defined('_INC') or die;

return array(
    {$tpl['providers']}
);
?>
TEXT;
}

/**
 * Данные профиля настройки списка MIMES
 * 
 * @category   Gear
 * @package    GController_SMimes_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SSocialConfig_Profile_Data extends GController_Profile_Data
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ssocialconfig_profile';

    /**
     * Файл настроек роботов
     *
     * @var string
     */
    public $filename = 'Social.php';

    /**
     * Массив полей таблицы ($_tableName)
     *
     * @var array
     */
    public $fields = array('providers');

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return $this->_['profile_title_update'];
    }

    /**
     * Обновление настроеек сайта
     * 
     * @return void
     */
    protected function fileUpdate($params)
    {
        // запись для CMS
        if (file_put_contents('../' . PATH_CONFIG_CMS . $this->filename, getTplSiteConfig($params), FILE_TEXT) === false)
            throw new GException('Error', sprintf($this->_['msg_create_file'], $this->filename));
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {

        $params['date'] = date('Y-m-d H:i:s');
        $params['social'] = '';

        if (empty($params['providers'])) return true;
 
        $tpl = '';
        foreach ($params['providers'] as $alias => $item) {
            $tpl .= "'" . GString::toSimpleStr($alias) . "' => array(" . _n;
            $tpl .= _2t . "'enabled' => " . ($item['enabled'] ? 'true' : 'false') . "," . _n;
            $tpl .= _2t . "'name'    => '" . (isset($item['name']) ? $item['name'] : $alias) . "'," . _n;
            $tpl .= _2t . "'keys'    => array(";
            $keys = array();
            if (isset($item['id']))
                $keys[] = "'id' => '" . GString::toSimpleStr($item['id']) . "'";
            if (isset($item['key']))
                $keys[] = "'key' => '" . GString::toSimpleStr($item['key']) . "'";
            if (isset($item['secret']))
                $keys[] = "'secret' => '" . GString::toSimpleStr($item['secret']) . "'";
            if ($keys)
                $tpl .= implode(', ', $keys);
            $tpl .= ')'. _n;

            $tpl .= _t . ")," . _n_t;
        }
        $params['providers'] = $tpl;
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        parent::dataAccessUpdate();

        // массив полей с их соответствующими значениями
        if (empty($params))
            $params = $this->input->getBy($this->fields);
        // проверки входных данных
        $this->isDataCorrect($params);
        // проверка существования записи с одинаковыми значениями
        $this->isDataExist($params);
        // использование системных полей в запросе SQL
        if ($this->isSysFields) {
            $params['sys_date_update'] = date('Y-m-d');
            $params['sys_time_update'] = date('H:i:s');
            $params['sys_user_update'] = $this->session->get('user_id');
        }
        // обновить файл конфигурации
        $this->fileUpdate($params);
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($params, 'method "PUT"');
        }
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        parent::dataAccessView();
    }
}
?>