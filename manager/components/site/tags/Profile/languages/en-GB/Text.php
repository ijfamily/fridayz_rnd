<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2013-2016 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Create record "Gallery"',
    'title_profile_update' => 'Update record "%s"',
    // поля формы
    'label_gallery_index'       => 'Index',
    'tip_gallery_index'         => 'Index',
    'label_gallery_name'        => 'Name',
    'tip_gallery_name'          => 'Displayed only in the system of administration in the form of notes to the user',
    'label_gallery_description' => 'Description',
    'label_article_name'        => 'Article',
    'tip_article_name'          => 'Article note'
);
?>