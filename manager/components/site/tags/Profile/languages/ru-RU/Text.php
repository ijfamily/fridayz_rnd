<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2013-2016 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Создание записи "Тег облака"',
    'title_profile_update' => 'Изменение записи "%s"',
    'text_btn_help'        => 'Справка',
    // поля формы
    'label_tag_index' => 'Порядок<hb></hb>',
    'tip_tag_index'   => 'Порядковый номер',
    'label_tag_name'  => 'Название',
    'label_tag_title' => 'Описание<hb></hb>',
    'tip_tag_title'   => 'Описание',
    'label_tag_lat'   => 'Латиница<hb></hb>',
    'tip_tag_lat'     => 'Транслитерация название',
    'label_published' => 'Публикация',
);
?>