<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2013-2016 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Створення запису "Галерея"',
    'title_profile_update' => 'Зміна запису "%s"',
    // поля формы
    'label_gallery_index'       => 'Индекс',
    'tip_gallery_index'         => 'Порядковий номер (для формування псевдоніма у статті)',
    'label_gallery_name'        => 'Примітка',
    'tip_gallery_name'          => 'Відображається лише в системі адміністрування у вигляді замітки користувачеві',
    'label_gallery_description' => 'Опис',
    'label_article_name'        => 'Стаття',
    'tip_article_name'          => 'Примітка статті'
);
?>