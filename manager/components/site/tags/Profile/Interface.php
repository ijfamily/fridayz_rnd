<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля облака тега"
 * Пакет контроллеров "Профиль облака тега"
 * Группа пакетов     "Облако тегов"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SCloudeTags_Profile
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля облака тега
 * 
 * @category   Gear
 * @package    GController_SCloudeTags_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SCloudeTags_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        $data = array('max' => 1);

        parent::getDataInterface();

        $query = new GDb_Query();
        // если состояние формы "insert"
        if ($this->isInsert) {
            $sql = 'SELECT MAX(`tag_index`) `max` FROM `site_tags`';
            if (($record = $query->getRecord($sql)) === false)
                throw new GSqlException();
            $data['max'] = $record['max'] + 1;
            return $data;
        }

        return $data;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'titleEllipsis' => 45,
                  'gridId'        => 'gcontroller_scloudetags_grid',
                  'width'         => 470,
                  'autoHeight'    => true,
                  'stateful'      => false,
                  'buttonsAdd'    => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-site-tags',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype'      => 'spinnerfield',
                  'itemCls'    => 'mn-form-item-quiet',
                  'fieldLabel' => $this->_['label_tag_index'],
                  'labelTip'   => $this->_['tip_tag_index'],
                  'name'       => 'tag_index',
                  'value'      => $data['max'],
                  'width'      => 70,
                  'allowBlank' => true,
                  'emptyText'  => 1),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_tag_name'],
                  'name'       => 'tag_name',
                  'checkDirty' => false,
                  'maxLength'  => 255,
                  'anchor'     => '100%',
                  'allowBlank' => false),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_tag_title'],
                  'labelTip'   => $this->_['tip_tag_title'],
                  'name'       => 'tag_title',
                  'maxLength'  => 255,
                  'anchor'     => '100%',
                  'allowBlank' => false),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_tag_lat'],
                  'labelTip'   => $this->_['tip_tag_lat'],
                  'name'       => 'tag_lat',
                  'checkDirty' => false,
                  'maxLength'  => 255,
                  'anchor'     => '100%',
                  'allowBlank' => true),
            array('xtype'      => 'mn-field-chbox',
                  'fieldLabel' => $this->_['label_published'],
                  'default'    => $this->isUpdate ? null : 1,
                  'name'       => 'tag_published')
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($items);
        $form->url = $this->componentUrl . 'profile/';
        $form->labelWidth = 95;

        parent::getInterface();
    }
}
?>