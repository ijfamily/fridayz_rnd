<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Облако тегов"
 * Группа пакетов     "Облако тегов"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SCloudeTags
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Облако тегов
 * Описание: Облако тегов
 * Меню: Облако тегов
 * ID класса: gcontroller_scloudetags_grid
 * Модуль: Сайт
 * Группа: Сайт
 * Очищать: нет
 * Ресурс
 *    Компонент: site/tags/tags/
 *    Контроллер: site/tags/tags/Grid/
 *    Интерфейс: grid/interface/
 *    Меню:
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске компонентов: нет
 *    В главном меню: нет
 */

return array(
    // {S}- модуль "Site" {} - пакет контроллеров "Cloude tags" -> SCloudeTags
    'clsPrefix' => 'SCloudeTags',
    // использовать язык
    'language'  => 'ru-RU'
);
?>