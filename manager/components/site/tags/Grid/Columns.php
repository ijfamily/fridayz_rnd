<?php
/**
 * Gear Manager
 *
 * Контроллер         "Столбцы облака тегов"
 * Пакет контроллеров "Облако тегов"
 * Группа пакетов     "Облако тегов"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SCloudeTags_Grid
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Columns.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Columns');

/**
 * Столбцы списка облака тегов
 * 
 * @category   Gear
 * @package    GController_SCloudeTags_Grid
 * @subpackage Columns
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Columns.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SCloudeTags_Grid_Columns extends GController_Grid_Columns
{}
?>