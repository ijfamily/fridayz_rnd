<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка облака тегов"
 * Пакет контроллеров "Облако тегов"
 * Группа пакетов     "Облако тегов"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SCloudeTags_Grid
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные списка облака тегов
 * 
 * @category   Gear
 * @package    GController_SCloudeTags_Grid
 * @subpackage Data
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SCloudeTags_Grid_Data extends GController_Grid_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * (для обработки записей таблицы)
     * 
     * @var boolean
     */
    public $isSysFields = false;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'tag_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_tags';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 'SELECT SQL_CALC_FOUND_ROWS * FROM `site_tags` WHERE 1 %filter ORDER BY %sort LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // порядок
            'tag_index' => array('type' => 'integer'),
            // название
            'tag_name' => array('type' => 'string'),
            // загаловок
            'tag_title' => array('type' => 'string'),
            // в латинице
            'tag_lat' => array('type' => 'string'),
            // опубликован
            'tag_published' => array('type' => 'integer'),
            // переход по ссылке
            'tag_uri' => array('type' => 'string'),
            'goto_url' => array('type' => 'string')
        );
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        $query = new GDb_Query();
        // удаление записей таблицы "site_tags" (облако тегов)
        if ($query->clear('site_tags') === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_tags') === false)
            throw new GSqlException();
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        // подсказки для ЧПУ URL категорий
        $record['tag_uri'] = '/tag/' . $record['tag_name'] . '/';
        $record['goto_url'] = '<a href="'. $record['tag_uri'] . '" target="_blank" title="' . $this->_['tooltip_goto_url'] . '"><img src="' . $this->resourcePath . 'icon-goto.png"></a>';
        $record['tag_name'] = '#' . $record['tag_name'];

        return $record;
    }
}
?>