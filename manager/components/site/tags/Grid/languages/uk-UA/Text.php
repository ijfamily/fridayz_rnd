<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2013-2016 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'     => 'Галерея',
    'rowmenu_edit'   => 'Редагувати',
    'rowmenu_images' => 'Зображення',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'стовпці',
    'title_buttongroup_filter' => 'Фільтр',
    'msg_btn_clear'            => 'Ви дійсно бажаєте видалити всі записи <span class="mn-msg-delete">("Галерея", "Зображення")</span> ?',
    // столбцы
    'header_gallery_index'       => '№',
    'tooltip_gallery_index'      => 'Порядковий номер галереї',
    'header_article_note'        => 'Стаття',
    'tooltip_article_note'       => 'Примітка статті',
    'header_gallery_name'        => 'Назва',
    'header_gallery_description' => 'Опис',
    'header_images_count'        => 'Зображень',
    'tooltip_images_count'       => 'Зображень в галереї'
);
?>