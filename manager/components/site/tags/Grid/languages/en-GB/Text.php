<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
  * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2013-2016 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'     => 'Gallery',
    'rowmenu_edit'   => 'Edit',
    'rowmenu_images' => 'Images',
    // панель управления
    'title_buttongroup_edit'   => 'Edit',
    'title_buttongroup_cols'   => 'Coumns',
    'title_buttongroup_filter' => 'Filter',
    'msg_btn_clear'            => 'Do you really want to delete all records <span class="mn-msg-delete">("Gallery", "Images")</span> ?',
    // столбцы
    'header_gallery_index'      => '№',
    'tooltip_gallery_index'     => 'Index of galleries',
    'header_article_note'       => 'Article',
    'tooltip_article_note'      => 'Article note',
    'header_gallery_name'       => 'Name',
    'header_gallery_description' => 'Description',
    'header_images_count'       => 'Images',
    'tooltip_images_count'      => 'Images in gallery'
);
?>