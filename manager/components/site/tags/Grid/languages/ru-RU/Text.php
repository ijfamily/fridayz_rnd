<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2013-2016 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'    => 'Облако тегов',
    'rowmenu_edit'  => 'Редактировать',
    'tooltip_grid'  => 'облако тегов',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">("Облако тегов")</span> ?',
    // столбцы
    'header_tag_index'  => '№',
    'tooltip_tag_index' => 'Порядковый номер тега',
    'header_tag_name'   => 'Название',
    'header_tag_title'  => 'Описание',
    'header_tag_lat'    => 'Латиница',
    'tooltip_tag_lat'   => 'Транслитерация название',
    'tooltip_published' => 'Публикация тега',
    'header_tag_uri'    => 'ЧПУ URL тега',
    'tooltip_goto_url'  => 'Просмотр статей по выбранному тегу',
    // развёрнутая запись
    'label_tag_index' => 'Порядковый номер тега',
    'label_tag_name'  => 'Название',
    'label_tag_title' => 'Описание',
    'label_tag_lat'   => 'Транслитерация название',
    'label_published' => 'Публикация тега',
    // тип
    'data_boolean' => array('нет', 'да')
);
?>