<?php
/**
 * Gear Manager
 *
 * Контроллер         "Развёрнутая запись облака тегов"
 * Пакет контроллеров "Облако тегов"
 * Группа пакетов     "Облако тегов"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SCloudeTags_Grid
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Expand');

/**
 * Развёрнутая запись облака тегов
 * 
 * @category   Gear
 * @package    GController_SCloudeTags_Grid
 * @subpackage Expand
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SCloudeTags_Grid_Expand extends GController_Grid_Expand
{
    /**
     * Загаловок в списке
     * 
     * @return string
     */
    protected $_title = '';

    /**
     * Вывод данных в интерфейс
     * 
     * @return void
     */
    protected function dataExpand()
    {
        parent::dataAccessView();

        $data = $this->getAttributes();
        $this->response->data = $data;
    }

    /**
     * Возращает атрибуты записи
     * 
     * @return string
     */
    protected function getAttributes()
    {
        $data = '';
        $query = new GDb_Query();
        $sql = 'SELECT * FROM `site_tags` WHERE `tag_id`=' . $this->uri->id;
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();

        $data .= '<fieldset><ul><li><label>' . $this->_['label_tag_index'] . ':</label> ' . $rec['tag_index'] . '</li>';
        $data .= '<li><label>' . $this->_['label_tag_name'] . ':</label> ' . $rec['tag_name'] . '</li>';
        $data .= '<li><label>' . $this->_['label_tag_title'] . ':</label> ' . $rec['tag_title'] . '</li>';
        $data .= '<li><label>' . $this->_['label_tag_lat'] . ':</label> ' . $rec['tag_lat'] . '</li>';
        $data .= '<li><label>' . $this->_['label_published'] . ':</label> ' . $this->_['data_boolean'][(int) $rec['tag_published']] . '</li></ul></fieldset>';

        return '<div class="mn-row-body border">' . $data . '<div class="wrap"></div></div>';
    }
}
?>