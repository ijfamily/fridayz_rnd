<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск тега облака"
 * Пакет контроллеров "Облако тегов"
 * Группа пакетов     "Облако тегов"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SCloudeTags_Search
 * @copyright  Copyright (c) 2013-2015 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Data.php 2015-07-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск тега облака
 * 
 * @category   Gear
 * @package    GController_SCloudeTags_Search
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2015 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Data.php 2015-07-01 12:00:00 Gear Magic $
 */
final class GController_SCloudeTags_Search_Data extends GController_Search_Data
{
    /**
     * дентификатор компонента (списка) к которому применяется поиск
     *
     * @var string
     */
    public $gridId = 'gcontroller_scloudetags_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_scloudetags_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор
     * Используется для инициализации атрибутов контроллер не переданного в конструктор
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'tag_index', 'label' => $this->_['header_tag_index']),
            array('dataIndex' => 'tag_name', 'label' => $this->_['header_tag_name']),
            array('dataIndex' => 'tag_title', 'label' => $this->_['header_tag_title']),
            array('dataIndex' => 'tag_lat', 'label' => $this->_['header_tag_lat']),
            array('dataIndex' => 'tag_published', 'label' => $this->_['header_tag_published'])
        );
    }
}
?>