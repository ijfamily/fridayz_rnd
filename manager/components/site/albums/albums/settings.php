<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Альбомы"
 * Группа пакетов     "Альбомы"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SAlbums
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Альбомы
 * Описание: Альбомы изображений
 * Меню: Альбом изображения
 * ID класса: gcontroller_salbums_grid
 * Модуль: Сайт
 * Группа: Сайт
 * Очищать: нет
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске компонентов: нет
 *    В главном меню: нет
 * Ресурс
 *    Компонент: site/albums/albums/
 *    Контроллер: site/albums/albums/Grid/
 *    Интерфейс: grid/interface/
 *    Меню:
 */

return array(
    // {S}- модуль "Site" {} - пакет контроллеров "Albums" -> SAlbums
    'clsPrefix' => 'SAlbums',
    // использовать язык
    'language'  => 'ru-RU'
);
?>