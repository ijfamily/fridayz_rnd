<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2013-2016 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'    => 'Фотоальбомы',
    'rowmenu_edit'  => 'Редактировать',
    'rowmenu_image' => 'Изображения',
    'rowmenu_view'  => 'Просмотр изображений',
    'tooltip_grid'  => 'это Ваши фотографии, отредактированные и отсортированные, а также фото миниатюр для предварительного просмотра (превью или thumbnails) и html-страницы, на которых будут отображаться фотографии и комментарии к ним',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">("Альбомы", "Изображения")</span> ?',
    'text_btn_config'          => 'Настройка',
    'tooltip_btn_config'       => 'Настройка фотоальбома',
    // столбцы
    'header_published'          => 'Дата публикации',
    'header_album_index'        => '№',
    'tooltip_album_index'       => 'Порядковый номер альбома',
    'header_album_description'  => 'Описание',
    'tooltip_album_description' => 'Описание альбома',
    'header_album_name'         => 'Название',
    'header_images_count'       => 'Изображений',
    'tooltip_images_count'      => 'Изображений в альбоме',
    'header_category_name'      => 'Категория',
    'tooltip_category_name'     => 'Категория статьи',
    'tooltip_published'         => 'Публикация альбома',
    'header_album_folder'       => 'Каталог',
    'tooltip_album_folder'      => 'Название каталога, где находятся изображения альбомов',
    'header_file_perms'         => 'Права доступа',
    'header_album_uri'          => 'ЧПУ URL альбома',
    'tooltip_goto_url'          => 'Просмотр альбома сайта',
    // развёрнутая запись
    'label_image_title' => 'Изображение',
    'label_image_count' => 'Изображений: <b>%s</b>',
    // сообщения
    'msg_cant_delete' => 'Невозможно удалить альбом "%s", т.к. альбом не имеет каталога изображений',
    // тип
    'data_boolean' => array('нет', 'да')
);
?>