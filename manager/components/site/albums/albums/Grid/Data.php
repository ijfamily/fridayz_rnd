<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка альбомов сайта"
 * Пакет контроллеров "Альбомы"
 * Группа пакетов     "Альбомы"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SAlbums_Grid
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('String');
Gear::library('File');
Gear::controller('Grid/Data');

/**
 * Данные списка альбомов сайта
 * 
 * @category   Gear
 * @package    GController_SAlbums_Grid
 * @subpackage Data
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SAlbums_Grid_Data extends GController_Grid_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * (для обработки записей таблицы)
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'album_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_albums';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // язык сайта по умолчанию
        $languageId = $this->config->getFromCms('Site', 'LANGUAGE/ID');
        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS `g`.*, `i`.`images_count`, `c`.`category_name` FROM `site_albums` `g` '
            // join `site_album_images`
          . 'LEFT JOIN (SELECT `album_id`, COUNT(`album_id`) `images_count` '
          . 'FROM `site_album_images` GROUP BY `album_id`) `i` USING (`album_id`) '
          . 'LEFT JOIN `site_categories` `c` USING (`category_id`) '
          . ' WHERE 1 %filter ORDER BY %sort LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // идент. альбома
            'album_id' => array('type' => 'integer'),
            // дата публикации
            'published_date' => array('type' => 'string'),
            'published_time' => array('type' => 'string'),
            'published_user' => array('type' => 'integer'),
            // порядковый номер альбома
            'album_index' => array('type' => 'integer'),
            // название
            'album_name' => array('type' => 'string'),
            // описание
            'album_description' => array('type' => 'string'),
            // изображений в альбома
            'images_count' => array('type' => 'integer'),
            // каталог
            'album_folder' => array('type' => 'string'),
            // права доступа
            'file_perms' => array('type' => 'string'),
            // альбом опубликован
            'published' => array('type' => 'integer'),
            // название категории
            'category_name' => array('type' => 'string'),
            'album_uri' => array('type' => 'string'),
            // переход по ссылке
            'goto_url' => array('type' => 'string')
        );

        $this->albumsPath = DOCUMENT_ROOT .  $this->config->getFromCms('Albums', 'DIR');
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        $query = new GDb_Query();
        // удаление изображений галерей
        GDir::clear(DOCUMENT_ROOT .  $this->config->getFromCms('Albums', 'DIR'), array(), true);
        // удаление записей таблицы "site_album_images" (изображения альбома)
        if ($query->clear('site_album_images') === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_album_images') === false)
            throw new GSqlException();
        // удаление записей таблицы "site_album_images_l" (текст изображений)
        if ($query->clear('site_album_images_l') === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_album_images_l') === false)
            throw new GSqlException();
        // удаление записей таблицы "site_albums" (галерея)
        if ($query->clear('site_albums') === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_albums') === false)
            throw new GSqlException();
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Событие наступает после успешного удаления данных
     * 
     * @return void
     */
    protected function dataDeleteComplete()
    {
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        Gear::library('File');

        $query = new GDb_Query();
        $sql = 'SELECT * FROM `site_albums` WHERE `album_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $errors = $ids = array();
        while (!$query->eof()) {
            $item = $query->next();
            if (empty($item['album_folder']))
                $errors[] = $item['album_name'];
            else {
                $path = DOCUMENT_ROOT .  $this->config->getFromCms('Albums', 'DIR') . $item['album_folder'] . '/';
                if (file_exists($path))
                    GDir::remove($path);
                $ids[] = $item['album_id'];
            }
        }
        $ids = implode(',', $ids);
        // если есть что удалять
        if ($ids) {
            // если есть ошибки удаляем что есть
            if ($errors) {
                // удаление записей таблицы "site_albums" (галерея)
                $sql = 'DELETE FROM `site_albums` WHERE `album_id` IN (' . $ids . ')';
                if ($query->execute($sql) === false)
                    throw new GSqlException();
            }
            // удаление записей таблицы "site_album_images_l" (текст изображений)
            $sql = 'DELETE `il` FROM `site_album_images_l` `il`, `site_album_images` `l` '
                 . 'WHERE `il`.`image_id`=`l`.`image_id` AND `l`.`album_id` IN (' . $ids . ')';
            if ($query->execute($sql) === false)
                throw new GSqlException();
            // удаление записей таблицы "site_album_images" (изображения альбома)
            $sql = 'DELETE FROM `site_album_images` WHERE `album_id` IN (' . $ids . ')';
            if ($query->execute($sql) === false)
                throw new GSqlException();
        }
        // если были ошибки
        if ($errors)
            throw new GException('Error', sprintf($this->_['msg_cant_delete'], GString::ellipsis(implode(',', $errors), 200)));
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        // подсказки для ЧПУ URL категорий
        $record['album_uri'] = '/albums/' . $record['album_id'];
        $record['goto_url'] = '<a href="'. $record['album_uri'] . '/?preview" target="_blank" title="' . $this->_['tooltip_goto_url'] . '"><img src="' . $this->resourcePath . 'icon-goto.png"></a>';

        $filename = $this->albumsPath . $record['album_folder'];
        if (!file_exists($filename)) {
            $record['rowCls'] = 'mn-row-warning';
        } else {
            $perms = fileperms($filename);
            if ($perms !== false) {
                $record['file_perms'] = GFile::filePermsDigit($perms) . ' ' . GFile::filePermsInfo($perms);
            }
        }

        return $record;
    }
}
?>