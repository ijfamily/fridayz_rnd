<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск альбомов сайта"
 * Пакет контроллеров "Альбомы"
 * Группа пакетов     "Альбомы"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SAlbums_Search
 * @copyright  Copyright (c) 2013-2015 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Data.php 2015-07-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск альбомов сайта
 * 
 * @category   Gear
 * @package    GController_SAlbums_Search
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2015 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Data.php 2015-07-01 12:00:00 Gear Magic $
 */
final class GController_SAlbums_Search_Data extends GController_Search_Data
{
    /**
     * дентификатор компонента (списка) к которому применяется поиск
     *
     * @var string
     */
    public $gridId = 'gcontroller_salbums_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_salbums_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор
     * Используется для инициализации атрибутов контроллер не переданного в конструктор
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'album_index', 'label' => $this->_['header_album_index']),
            array('dataIndex' => 'album_name', 'label' => $this->_['header_album_name'])
        );
    }
}
?>