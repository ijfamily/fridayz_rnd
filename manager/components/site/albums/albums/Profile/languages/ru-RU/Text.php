<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2013-2016 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Создание записи "Фотоальбома"',
    'title_profile_update' => 'Изменение записи "%s"',
    'text_btn_help'        => 'Справка',
    // вкладка "атрибуты"
    'title_tab_attributes' => 'Атрибуты',
    'label_album_index' => 'Порядок<hb></hb>',
    'tip_album_index'   => 'Порядковый номер',
    'label_album_name'  => 'Название',
    'label_album_desc'  => 'Описание',
    'label_category_name'   => 'Категория<hb></hb>',
    'tip_category_name'     => 'Категорию необходимо указывать если нужен список фотоальбомов',
    'title_fieldset_public' => 'Дата публикации альбома',
    'label_published_date'  => 'Дата',
    'label_published_time'  => 'Время',
    'label_published'       => 'Опубликовать<hb></hb>',
    'tip_published'         => 'Опубликовать фотоальбом в выбранной категории',
    // вкладка "seo статьи"
    'title_fieldset_meta' => 'Метатеги статьи',
    'title_tab_seo'     => 'SEO',
    'title_fieldset_r'  => 'Роботы',
    'label_page_meta_r' => 'Роботы<hb></hb>',
    'tip_page_meta_r'   => 'Формирует информацию о гипертекстовых документах, которая поступает к роботам поисковых систем. 
                            Значения тега могут быть следующими: Index (страница должна быть проиндексирована), Noindex (документ
                            не индексируется), Follow (гиперссылки на странице отслеживаются), Nofollow (гиперссылки не прослеживаются),
                            All (включает значения index и follow, включен по умолчанию), None (включает значения noindex и nofollow).',
    'label_page_meta_v' => 'Время и интервал<hb></hb>',
    'tip_page_meta_v'   => 'Время и интервал посещения поискового робота. Позволяет управлять частотой индексации документа в поисковой системе.',
    'label_page_meta_m' => 'Состояние<hb></hb>',
    'tip_page_meta_m'   => 'Значение «Static» отмечает, что системе нет необходимости индексировать документ в дальнейшем,
                            «Dynamic» позволяет регулярно индексировать Интернет-страницу',
    'title_fieldset_a'  => 'Авторское право',
    'label_page_meta_a' => 'Автор<hb></hb>',
    'tip_page_meta_a'   => 'Идентификация автора или принадлежности документа. Содержит имя автора Интернет-страницы, в том случае, 
                            если сайт принадлежит какой-либо организации, целесообразнее использовать поле «Организация».',
    'label_page_meta_c' => 'Организация<hb></hb>',
    'tip_page_meta_c'   => 'Идентификация принадлежности документа к какой-либо организации',
    'label_page_meta_k' => 'Ключевые слова<hb></hb><br><small>(keywords)</small>',
    'tip_page_meta_k'   => 'Поисковые системы используют для того, чтобы определить релевантность ссылки. При формировании данного тега 
                            необходимо использовать только те слова, которые содержатся в самом документе. Использование тех слов, которых 
                            нет на странице, не рекомендуется. Рекомендованное количество слов в данном теге — не более десяти.',
    'label_page_meta_d' => 'Описание<hb></hb><br><small>(description)</small>',
    'tip_page_meta_d'   => 'Используется поисковыми системами для индексации, а также при создании аннотации в выдаче по запросу
                            При отсутствии тега поисковые системы выдают в аннотации первую строку документа или отрывок, содержащий ключевые слова.
                            Отображается после ссылки при поиске страниц в поисковике.',
    'label_page_title'  => 'Загаловок<hb></hb><br><small>(title)</small>',
    'tip_page_title'    => 'Отображается в названии вкладки браузера',
    // сообщения
    'msg_cant_mk_dir' => 'Невозможно создать альбом "%s"',
    'msg_cant_delete' => 'Невозможно удалить альбом "%s", т.к. альбом не имеет каталога изображений',
    // тип
    'data_priorities' => array(
        array('0.0'), array('0.1'), array('0.2'), array('0.3'), array('0.4'), array('0.5'), array('0.6'),
        array('0.7'), array('0.8'), array('0.9'), array('1.0')
    ),
    'data_changefreq' => array(
        array('всегда', 'a'), array('почасовая', 'h'), array('ежедневно', 'd'), array('еженедельно', 'w'), array('ежемесячно', 'm'),
        array('ежегодно', 'y'), array('никогда', 'n')
    ),
    'data_changefreq_a' => array('a' => 'всегда', 'h' => 'почасовая', 'd' => 'ежедневно', 'w' => 'еженедельно', 'm' => 'ежемесячно', 'y' => 'ежегодно', 'n' => 'никогда'),
    'data_robots' => array(
        array('all'), array('index, follow'), array('noindex, follow'), array('index, nofollow'), array('noindex, nofollow'),
        array('none')
    ),
    'data_revisit' => array(
        array('1 день', 1), array('2 дня', 2), array('3 дня', 3), array('4 дня', 4), array('5 дней', 5), array('6 дней', 6), array('7 дней', 7)
    ),
    'data_doc' => array(array('Static'), array(' Dynamic'))
);
?>