<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля альбома"
 * Пакет контроллеров "Профиль альбома"
 * Группа пакетов     "Альбомы"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SAlbums_Profile
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля альбома
 * 
 * @category   Gear
 * @package    GController_SAlbums_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SAlbums_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        $data = array('max' => 1);

        parent::getDataInterface();

        $query = new GDb_Query();
        // если состояние формы "insert"
        if ($this->isInsert) {
            $sql = 'SELECT MAX(`album_index`) `max` FROM `site_albums`';
            if (($record = $query->getRecord($sql)) === false)
                throw new GSqlException();
            $data['max'] = $record['max'] + 1;
            return $data;
        }

        return $data;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'titleEllipsis' => 45,
                  'gridId'        => 'gcontroller_salbums_grid',
                  'width'         => 650,
                  'autoHeight'    => true,
                  'stateful'      => false,
                  'buttonsAdd'    => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-site-albums',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );

        // поля формы (ExtJS class "Ext.Panel")
        // вкладка "атрибуты"
        $tabAttr = array(
            'iconSrc'     => $this->resourcePath . 'icon-tab-attr.png',
            'title'       => $this->_['title_tab_attributes'],
            'layout'      => 'form',
            'labelWidth'  => 97,
            'baseCls'     => 'mn-form-tab-body',
            'items'       => array(
                array('xtype'      => 'spinnerfield',
                      'itemCls'    => 'mn-form-item-quiet',
                      'fieldLabel' => $this->_['label_album_index'],
                      'labelTip'   => $this->_['tip_album_index'],
                      'name'       => 'album_index',
                      'value'      => $data['max'],
                      'width'      => 70,
                      'allowBlank' => true,
                      'emptyText'  => 1),
                array('xtype'      => 'textfield',
                      'fieldLabel' => $this->_['label_album_name'],
                      'name'       => 'album_name',
                      'maxLength'  => 255,
                      'anchor'     => '100%',
                      'allowBlank' => false),
                array('xtype'      => 'textarea',
                      'fieldLabel' => $this->_['label_album_desc'],
                      'itemCls'    => 'mn-form-item-quiet',
                      'name'       => 'album_description',
                      'anchor'     => '100%',
                      'height'     => 70,
                      'allowBlank' => true),
                array('xtype'      => 'fieldset',
                      'anchor'     => '100%',
                      'title'      => $this->_['title_fieldset_public'],
                      'autoHeight' => true,
                      'layout'     => 'column',
                      'cls'        => 'mn-container-clean',
                      'items'      => array(
                          array('layout'     => 'form',
                                'width'      => 185,
                                'labelWidth' => 62,
                                'items'      => array(
                                    array('xtype'      => 'datefield',
                                          'itemCls'    => 'mn-form-item-quiet',
                                          'fieldLabel' => $this->_['label_published_date'],
                                          'format'     => 'd-m-Y',
                                          'name'       => 'published_date',
                                          'checkDirty' => false,
                                          'width'      => 95,
                                          'allowBlank' => true)
                                )
                          ),
                          array('layout'     => 'form',
                                'labelWidth' => 55,
                                'items'      => array(
                                    array('xtype'      => 'textfield',
                                          'itemCls'    => 'mn-form-item-quiet',
                                          'fieldLabel' => $this->_['label_published_time'],
                                          'name'       => 'published_time',
                                          'checkDirty' => false,
                                          'maxLength'  => 8,
                                          'width'      => 70,
                                          'allowBlank' => true)
                                )
                          )
                      )
                ),
                array('xtype'      => 'mn-field-combo-tree',
                      'itemCls'    => 'mn-form-item-quiet',
                      'id'         => 'fldCategory',
                      'fieldLabel' => $this->_['label_category_name'],
                      'labelTip'   => $this->_['tip_category_name'],
                      'width'      => 225,
                      'name'       => 'category_id',
                      'hiddenName' => 'category_id',
                      'resetable'  => false,
                      'treeWidth'  => 400,
                      'treeRoot'   => array('id' => 1, 'expanded' => true, 'expandable' => true),
                      'allowBlank' => true,
                      'store'      => array(
                          'xtype' => 'jsonstore',
                          'url'   => $this->componentUrl . 'combo/nodes/'
                       )
                ),
                array('xtype'      => 'mn-field-chbox',
                      'itemCls'    => 'mn-form-item-quiet',
                      'fieldLabel' => $this->_['label_published'],
                      'labelTip'   => $this->_['tip_published'],
                      'default'    => $this->isUpdate ? null : 1,
                      'name'       => 'published')
            )
        );

        // владка "SEO статьи"
        $tabSeo = array(
            'title'       => $this->_['title_tab_seo'],
            'iconSrc'     => $this->resourcePath . 'icon-tab-seo.png',
            'layout'      => 'form',
            'labelWidth'  => 85,
            'autoScroll'  => true,
            'baseCls'     => 'mn-form-tab-body',
            'defaultType' => 'textfield',
            'items'       => array(
                array('xtype'      => 'fieldset',
                      'width'      => 580,
                      'labelWidth' => 120,
                      'collapsible' => true,
                      'collapsed'   => true,
                      'title'      => $this->_['title_fieldset_r'],
                      'autoHeight' => true,
                      'items'      => array(
                          array('xtype'         => 'combo',
                                'fieldLabel'    => $this->_['label_page_meta_r'],
                                'labelTip'      => $this->_['tip_page_meta_r'],
                                'id'            => 'page_meta_robots',
                                'name'          => 'page_meta_robots',
                                //'value'         => $metaRobots,
                                'editable'      => true,
                                'maxLength'     => 100,
                                'width'         => 180,
                                'typeAhead'     => false,
                                'triggerAction' => 'all',
                                'mode'          => 'local',
                                'store'         => array(
                                    'xtype'  => 'arraystore',
                                    'fields' => array('list'),
                                    'data'   => $this->_['data_robots']
                                ),
                                'hiddenName'    => 'page_meta_robots',
                                'valueField'    => 'list',
                                'displayField'  => 'list',
                                'allowBlank'    => true),
                          array('xtype'         => 'combo',
                                'itemCls'       => 'mn-form-item-quiet',
                                'fieldLabel'    => $this->_['label_page_meta_v'],
                                'labelTip'      => $this->_['tip_page_meta_v'],
                                'id'            => 'page_meta_revisit',
                                'name'          => 'page_meta_revisit',
                                'editable'      => true,
                                'maxLength'     => 10,
                                'width'         => 180,
                                'typeAhead'     => false,
                                'triggerAction' => 'all',
                                'mode'          => 'local',
                                'store'         => array(
                                    'xtype'  => 'arraystore',
                                    'fields' => array('list', 'value'),
                                    'data'   => $this->_['data_revisit']
                                ),
                                'hiddenName'    => 'page_meta_revisit',
                                'valueField'    => 'value',
                                'displayField'  => 'list',
                                'allowBlank'    => true),
                          array('xtype'         => 'combo',
                                'itemCls'       => 'mn-form-item-quiet',
                                'fieldLabel'    => $this->_['label_page_meta_m'],
                                'labelTip'      => $this->_['tip_page_meta_m'],
                                'id'            => 'page_meta_document',
                                'name'          => 'page_meta_document',
                                'editable'      => true,
                                'maxLength'     => 100,
                                'width'         => 180,
                                'typeAhead'     => false,
                                'triggerAction' => 'all',
                                'mode'          => 'local',
                                'store'         => array(
                                    'xtype'  => 'arraystore',
                                    'fields' => array('list'),
                                    'data'   => $this->_['data_doc']
                                ),
                                'hiddenName'    => 'page_meta_document',
                                'valueField'    => 'list',
                                'displayField'  => 'list',
                                'allowBlank'    => true),
                      )
                ),
                array('xtype'      => 'fieldset',
                      'width'      => 580,
                      'labelWidth' => 120,
                      'title'      => $this->_['title_fieldset_a'],
                      'collapsible' => true,
                      'collapsed'   => true,
                      'autoHeight' => true,
                      'items'      => array(
                          array('xtype'      => 'textfield',
                                'itemCls'       => 'mn-form-item-quiet',
                                'fieldLabel' => $this->_['label_page_meta_a'],
                                'labelTip'   => $this->_['tip_page_meta_a'],
                                'id'         => 'page_meta_author',
                                'name'       => 'page_meta_author',
                                //'value'      => $metaAuthor,
                                'emptyText'  => $_SESSION['profile/name'],
                                'maxLength'  => 100,
                                'width'      => 252,
                                'allowBlank' => true),
                          array('xtype'      => 'textfield',
                                'itemCls'       => 'mn-form-item-quiet',
                                'fieldLabel' => $this->_['label_page_meta_c'],
                                'labelTip'   => $this->_['tip_page_meta_c'],
                                'id'         => 'page_meta_copyright',
                                'name'       => 'page_meta_copyright',
                                'maxLength'  => 255,
                                'width'      => 252,
                                'allowBlank' => true),
                      )
                ),
                array('xtype'      => 'fieldset',
                      'width'      => 580,
                      'labelWidth' => 120,
                      'title'      => $this->_['title_fieldset_meta'],
                      'autoHeight' => true,
                      'items'      => array(
                            array('xtype'      => 'textarea',
                                  'itemCls'    => 'mn-form-item-quiet',
                                  'fieldLabel' => $this->_['label_page_meta_k'],
                                  'labelTip'   => $this->_['tip_page_meta_k'],
                                  'id'         => 'page_meta_keywords',
                                  'name'       => 'page_meta_keywords',
                                  'anchor'     => '100%',
                                  'height'     => 150,
                                  'allowBlank' => true),
                            array('xtype'      => 'textarea',
                                  'itemCls'    => 'mn-form-item-quiet',
                                  'fieldLabel' => $this->_['label_page_meta_d'],
                                  'labelTip'   => $this->_['tip_page_meta_d'],
                                  'id'         => 'page_meta_description',
                                  'name'       => 'page_meta_description',
                                  'anchor'     => '100%',
                                  'height'     => 150,
                                  'allowBlank' => true)
                      )
                )
            )
        );

        // вкладки окна
        $tabs = array(
            array('xtype'             => 'tabpanel',
                  'layoutOnTabChange' => true,
                  'activeTab'         => 0,
                  'style'             => 'padding:3px',
                  'anchor'            => '100%',
                  'height'            => 380,
                  'items'             => array($tabAttr, $tabSeo))
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($tabs);
        $form->url = $this->componentUrl . 'profile/';

        parent::getInterface();
    }
}
?>