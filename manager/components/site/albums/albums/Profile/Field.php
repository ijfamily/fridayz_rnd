<?php
/**
 * Gear Manager
 *
 * Контроллер         "Изменение профиля записи альбома"
 * Пакет контроллеров "Профиль альбома"
 * Группа пакетов     "Альбомы"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SAlbums_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Field.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Field');

/**
 * Изменение профиля записи альбома
 * 
 * @category   Gear
 * @package    GController_SAlbums_Profile
 * @subpackage Field
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Field.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SAlbums_Profile_Field extends GController_Profile_Field
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array('published');

    /**
     * Первичный ключ таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'album_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_albums';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_salbums_grid';

    /**
     * Удаление кэша
     * 
     * @return void
     */
    protected function cacheClear($id)
    {
        Gear::library('File');

        $query = new GDb_Query();
        $sql = "SELECT * FROM `site_cache` WHERE `cache_generator`='album' AND `cache_generator_id`=$id";
        if ($query->execute($sql) === false)
            throw new GSqlException();
        while (!$query->eof()) {
            $cache = $query->next();
            // если есть кэш
            if ($cache['cache_filename']) {
                $path = DOCUMENT_ROOT . $this->config->getFromCms('Site', 'CACHE/DIR');
                GFile::delete($path . $cache['cache_filename'], false);
            }
        }
        $sql = "DELETE FROM `site_cache` WHERE `cache_generator`='album' AND `cache_generator_id`=$id";
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Событие наступает после успешного обновления данных
     * 
     * @param array $data сформированные данные полученные после запроса к таблице
     * @return void
     */
/*
    protected function dataUpdateComplete($data = array())
    {
        if (isset($data['article_caching'])) {
            if ($data['article_caching'] == 0) {
                $this->cacheClear($this->uri->id);
            }
        }
    }
*/
}
?>