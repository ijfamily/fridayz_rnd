<?php
/**
 * Gear Manager
 *
 * Контроллер         "Триггер выпадающего списка для обработки данных альбома"
 * Пакет контроллеров "Альбомы"
 * Группа пакетов     "Альбомы"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SAlbums_Combo
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Combo/Trigger');

/**
 * Триггер выпадающего списка для обработки данных альбома
 * 
 * @category   Gear
 * @package    GController_SAlbums_Combo
 * @subpackage Trigger
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SAlbums_Combo_Trigger extends GController_Combo_Trigger
{
    /**
     * Возращает список статей
     * 
     * @return void
     */
    protected function queryArticles()
    {
        $languageId = $this->config->getFromCms('Site', 'LANGUAGE/ID');
        $table = new GDb_Table('site_articles', 'article_id');
        $table->setStart($this->_start);
        $table->setLimit($this->_limit);
        $sql = 'SELECT SQL_CALC_FOUND_ROWS `a`.`article_id`, `p`.`page_header` '
             . 'FROM `site_articles` `a` JOIN `site_pages` `p` ON `p`.`article_id`=`a`.`article_id` AND `p`.`language_id`=' . $languageId
             . ' WHERE `a`.`article_id`<>1 ORDER BY `page_header` LIMIT %limit';
        if ($table->query($sql) === false)
            throw new GSqlException();
        $data = array();
        while (!$table->query->eof()) {
            $record = $table->query->next();
            $data[] = array('id' => $record['article_id'], 'name' => $record['page_header']);
        }
        $this->response->set('totalCount', $table->query->getFoundRows());
        $this->response->success = true;
        $this->response->data = $data;
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $triggerName название триггера
     * @return void
     */
    protected function dataView($triggerName)
    {
        parent::dataView($triggerName);

        // имя триггера
        switch ($triggerName) {
            // статьи
            case 'articles': $this->queryArticles(); break;
        }
    }
}
?>