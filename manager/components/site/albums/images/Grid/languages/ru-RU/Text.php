<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2013-2016 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'        => 'Изображения фотоальбома "%s"',
    'rowmenu_edit'      => 'Редактировать',
    'rowmenu_file'      => 'Переименовать',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">("Изображения")</span> ?',
    'text_btn_aggregate'       => 'Наполнить',
    'tooltip_btn_aggregate'    => 'Из изображений в каталоге фотоальбома будут созданы миниатюры и добавлены в фотоальбом (предыдущие изображения будут удалены из списка)',
    'text_btn_uploader'        => 'Добавить',
    'tooltip_btn_uploader'     => 'Добавить изображения (перетаскивая их на форму) в каталог фотоальбома',
    'text_btn_view'            => 'Просмотр',
    'tooltip_btn_view'         => 'Просмотр изображений фотоальбома',
    // столбцы
    'header_image_index'              => '№',
    'tooltip_image_index'             => 'Порядковый номер изображения',
    'header_image_view'               => 'Изображение',
    'header_image_title'              => 'Заглавие',
    'tooltip_image_title'             => 'Используется для подписи изображения если  изображение отсутствует (атрибуты "alt", "title")',
    'header_image_visible'            => 'Показывать',
    'tooltip_image_visible'           => 'Показывать изображения',
    'tooltip_image_entire_link'       => 'Просмотр изображения',
    'header_image_entire_filename'    => 'Файл изображения',
    'tooltip_image_entire_filename'   => 'Файл изображения',
    'header_image_entire_resolution'  => 'Разрешение (и)',
    'tooltip_image_entire_resolution' => 'Разрешение изображения в пкс.',
    'header_image_entire_filesize'    => 'Размер (и)',
    'tooltip_image_entire_filesize'   => 'Размер файла изображения',
    'header_image_thumb_filename'     => 'Файл миниатюры',
    'tooltip_image_thumb_filename'    => 'Файл миниатюры',
    'header_image_thumb_resolution'   => 'Разрешение (м)',
    'tooltip_image_thumb_resolution'  => 'Разрешение миниатюры в пкс.',
    'header_image_thumb_filesize'     => 'Размер (м)',
    'tooltip_image_thumb_filesize'    => 'Размер файла миниатюры',
    'tooltip_image_cover'             => 'Обложка фотоальбома',
);
?>