<?php
/**
 * Gear Manager
 *
 * Контроллер         "Развёрнутая запись изображения"
 * Пакет контроллеров "Изображения альбома"
 * Группа пакетов     "Альбомы"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SAlbumImages_Grid
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Expand');

/**
 * Развёрнутая запись изображения
 * 
 * @category   Gear
 * @package    GController_SAlbumImages_Grid
 * @subpackage Expand
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SAlbumImages_Grid_Expand extends GController_Grid_Expand
{
    /**
     * Столбцы (id => field)
     *
     * @var array
     */
    public $columns = array('image-thumb' => 'image_thumb_filename', 'image-full' => 'image_entire_filename');

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_salbums_grid';

    /**
     * Возращает SQL запрос
     * 
     * @return string
     */
    protected function getQuery()
    {
        return 'SELECT * FROM `site_album_images` WHERE `image_id`=' . $this->uri->id;
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        $album = $this->store->get('album');
        $path = $this->config->getFromCms('Albums', 'DIR') . $album['album_folder'] . '/';
        if (!empty($record['image-full']))
            $record['image-full'] = '<span mn:bar="download,picture'
                                  . '" class="mn-img mn-img-xl'
                                  . '" mn:image-thumb="/' . $path . $record['image-full']
                                  . '" mn:image-full="/' . $path . $record['image-full'] . '"></span>';
        if (!empty($record['image-thumb']))
            $record['image-thumb'] = '<span mn:bar="download,picture'
                                   . '" class="mn-img mn-img-xl'
                                   . '" mn:image-thumb="/' . $path . $record['image-thumb']
                                   . '" mn:image-full="/' . $path . $record['image-thumb'] . '"></span>';

        return $record;
    }
}
?>