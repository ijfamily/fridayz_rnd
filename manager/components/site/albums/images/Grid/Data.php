<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные интерфейса списка изображений альбома"
 * Пакет контроллеров "Изображения альбома"
 * Группа пакетов     "Альбомы"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SAlbumImages_Grid
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('File');
Gear::controller('Grid/Data');

/**
 * Данные интерфейса списка изображений альбома
 * 
 * @category   Gear
 * @package    GController_SAlbumImages_Grid
 * @subpackage Data
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SAlbumImages_Grid_Data extends GController_Grid_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'image_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_album_images';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_salbums_grid';

    /**
     * Каталог изображений альбома
     *
     * @var string
     */
    public $_path = '';

    /**
     * Идентификатор галереи
     *
     * @var integer
     */
    protected $_albumId;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // идент. альбома
        $this->_albumId = $this->store->get('record', 0, 'gcontroller_salbums_grid');
        $this->_album = $this->store->get('album');
        // каталог изображений альбома
        $this->_path = DOCUMENT_ROOT . $this->config->getFromCms('Albums', 'DIR') . $this->_album['album_folder']. '/';
        // язык сайта по умолчанию
        $languageId = $this->config->getFromCms('Site', 'LANGUAGE/ID');
        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS `i`.*, `il`.`image_title` FROM `site_album_images` `i` '
          . 'LEFT JOIN `site_album_images_l` `il` ON `il`.`image_id`=`i`.`image_id` AND `il`.`language_id`=' . $languageId . ' '
           .'WHERE `i`.`album_id`=' . $this->_recordId . ' %filter ORDER BY %sort LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // индекс изображения
            'image_index' => array('type' => 'string'),
            // файл изображения
            'image_entire_filename' => array('type' => 'string'),
            // разрешение изображения
            'image_entire_resolution' => array('type' => 'string'),
            // размер файла изображения
            'image_entire_filesize' => array('type' => 'string'),
            // файл эскиза изображения
            'image_thumb_filename' => array('type' => 'string'),
            // разрешение эскиза изображения
            'image_thumb_resolution' => array('type' => 'string'),
            // размер файла эскиза изображения
            'image_thumb_filesize' => array('type' => 'string'),
            // заголовок изображения
            'image_title' => array('type' => 'string'),
            // показывать изображение
            'image_visible' => array('type' => 'integer'),
            // обложка изображения
            'image_cover' => array('type' => 'integer'),
            // обложка изображения
            'image_view' => array('type' => 'string'),
            'image_entire_link' => array('type' => 'string')

        );
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        $query = new GDb_Query();
        // удаление текста изображений
        $sql = 'DELETE `il` FROM `site_album_images_l` `il`, `site_album_images` `i` '
             . 'WHERE `il`.`image_id`=`i`.`image_id` AND `i`.`album_id`=' . $this->_albumId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_album_images_l') === false)
            throw new GSqlException();
        // удаление файлов изображений
        $sql = 'SELECT * FROM `site_albums` WHERE `album_id`=' . $this->_albumId;
        if (($album = $query->getRecord($sql)) === false)
            throw new GSqlException();
        if (!empty($album['album_folder'])) {
            // удаление изображений альбома
            GDir::clear(DOCUMENT_ROOT .  $this->config->getFromCms('Albums', 'DIR') . $album['album_folder'] . '/');
        }
        // удаление изображений
        $sql = 'DELETE FROM `site_album_images` WHERE `album_id`=' . $this->_albumId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_album_images') === false)
            throw new GSqlException();
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Событие наступает после успешного удаления данных
     * 
     * @return void
     */
    protected function dataDeleteComplete()
    {
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        $query = new GDb_Query();
        // изображения статьи
        $sql = 'SELECT `i`.*, `g`.`album_folder` FROM `site_album_images` `i` JOIN `site_albums` `g` USING(`album_id`) '
             . 'WHERE `i`.`image_id` IN (' . $this->uri->id. ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $path = DOCUMENT_ROOT . $this->config->getFromCms('Albums', 'DIR');
        while (!$query->eof()) {
            $image = $query->next();
            // если есть изображение
            if ($image['image_entire_filename'])
                GFile::delete($path . $image['album_folder'] . '/' . $image['image_entire_filename'], false);
            // если есть эскиз
            if ($image['image_thumb_filename'])
                GFile::delete($path . $image['album_folder'] . '/' . $image['image_thumb_filename'], false);
        }
        // удаление изображений статьи
        $sql = 'DELETE FROM `site_album_images_l` WHERE `image_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_album_images_l') === false)
            throw new GSqlException();
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        $record['image_view'] = '';
        // если файл эскиза не существует
        if (!empty($record['image_thumb_filename']))
            if (!file_exists($this->_path . $record['image_thumb_filename']))
                $record['image_thumb_filename'] = '<span style="color:#eeacaf">' . $record['image_thumb_filename'] . '</span>';
            else
                $record['image_view'] ='<div class="mn-img-preview" style="background-image: url(/' . $this->_path . $record['image_thumb_filename'] . ');"></div>';
        // если файл изображения не существует
        if (!empty($record['image_entire_filename']))
            if (!file_exists($this->_path . $record['image_entire_filename'])) {
                $record['image_entire_filename'] = '<span style="color:#eeacaf">' . $record['image_entire_filename'] . '</span>';
            } else {
                $record['image_entire_link'] = '<a href="/'. $this->_path . $record['image_entire_filename'] . '" target="_blank" title="' . $this->_['tooltip_image_entire_link'] . '"><img src="' . $this->resourcePath . 'icon-item-link.png"></a>';
            }

        return $record;
    }
}
?>