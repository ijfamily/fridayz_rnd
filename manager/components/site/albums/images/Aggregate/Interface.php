<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля наполнения фотоальбома"
 * Пакет контроллеров "Профиль наполнения фотоальбома"
 * Группа пакетов     "Сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SAlbumImages_Aggregate
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('File');
Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля наполнения фотоальбома
 * 
 * @category   Gear
 * @package    GController_SAlbumImages_Aggregate
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SAlbumImages_Aggregate_Interface extends GController_Profile_Interface
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_salbums_grid';

    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        parent::getDataInterface();

        $query = new GDb_Query();
        // список альбом
        $sql = 'SELECT * FROM `site_albums` WHERE `album_id`=' . $this->store->get('record', 0, 'gcontroller_salbums_grid');
        if (($album = $query->getRecord($sql)) === false)
            throw new GSqlException();
        if (empty($album))
            throw new GException($this->_['title_process'], $this->_['msg_empty_list']);
        $path = DOCUMENT_ROOT . $this->config->getFromCms('Albums', 'DIR') . $album['album_folder'] . '/';
        // если нет каталога
        if (!is_dir($path))
            throw new GException($this->_['title_process'], 'The directory "%s" can not be opened', $path);
        // если каталог не открывается
        $handle = @opendir($path);
        if ($handle === false)
            throw new GException($this->_['title_process'], 'The directory "%s" can not be opened', $path);
        $process = $data = array();
        $index = 0;
        while(false !== ($file = readdir($handle))) {
            if($file != '.' && $file != '..') {
                if (strpos($file, '_thumb') !== false)
                    throw new GException($this->_['title_process'], $this->_['msg_thumb_exist']);
                $size = GFile::getFileSize($path . $file);
                $data[] = array($index, $file, sprintf($this->_['label_desc'], $size), 0);
                $process[] = array($path . $file);
                $index++;
            }
        }
        $this->store->set('process', $process);
        $this->store->set('album', $album);

        return array('list' => $data, 'process' => $process, 'alubm' => $album);
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('id'            => $this->classId,
                  'title'         => $this->_['title_profile_insert'],
                  'titleEllipsis' => 45,
                  'gridId'        => 'gcontroller_salbums_grid',
                  'width'         => 500,
                  'height'        => 500,
                  'state'         => 'none',
                  'resizable'     => false,
                  'stateful'      => false,
                  'buttonsAdd'    => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-site-albums-aggregate',
                            'iconCls' => 'icon-item-info'),
                      array('xtype'       => 'mn-btn-form-update',
                            'id'          => 'btn-process',
                            'text'        => $this->_['text_btn_aggregate'],
                            'tooltip'     => $this->_['tooltip_btn_aggregate'],
                            'icon'        => $this->resourcePath . 'icon-btn-send.png',
                            'windowId'    => $this->classId,
                            'width'       => 97,
                            'disabled'    => empty($data['list']),
                            'closeWindow' => false)
                  ))
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype'  => 'mn-grid-process',
                  'id'     => 'grid-process',
                  'anchor' => '100% 100%',
                  'bodyCls' => '',
                  'data'   => $data['list'],
                  'url'    => $this->componentUrl . 'aggregate/data/'
            )
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($items);
        $form->labelWidth = 50;
        $form->url = $this->componentUrl . 'aggregate/';

        parent::getInterface();
    }
}
?>