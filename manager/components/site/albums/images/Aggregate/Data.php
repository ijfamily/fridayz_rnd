<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные наполнения фотоальбома"
 * Пакет контроллеров "Профиль наполнения фотоальбома"
 * Группа пакетов     "Сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SAlbumImages_Aggregate
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('String');
Gear::library('File');
Gear::controller('Grid/Process');

/**
 * Данные профиля наполнения фотоальбома
 * 
 * @category   Gear
 * @package    GController_SAlbumImages_Aggregate
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SAlbumImages_Aggregate_Data extends GController_Grid_Process
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_salbums_grid';

    /**
     * Идент. списка
     *
     * @var string
     */
    public $gridId = 'grid-process';

    /**
     * Фотоальбом
     *
     * @var mixed
     */
    protected $_album = false;

    /**
     * Путь к фотоальбому
     *
     * @var string
     */
    protected $_albumPath = '';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // если нет списка изображений
        if (empty($this->list))
            throw new GException($this->_['title_process'], $this->_['msg_empty_list']);
        // фотоальбом
        $this->_album = $this->store->get('album', false);
        if (empty($this->_album))
            throw new GException($this->_['title_process'], $this->_['msg_empty_album']);
        // путь к фотоальбому
        $this->_albumPath = DOCUMENT_ROOT . $this->config->getFromCms('Albums', 'DIR') . $this->_album['album_folder'] . '/';
    }

    /**
     * Начало обработки
     * 
     * @return void
     */
    protected function start()
    {
        parent::start();

        // удалить все записи
        $this->clear();
        // блокировать кнопку
        $this->response->add('setTo', array('id' => 'btn-process', 'func' => 'setDisabled', 'args' => array(true)), true);
        $this->response->setMsgResult($this->_['title_process'], $this->_['msg_process_start'], true);
    }

    /**
     * Конец обработки
     * 
     * @return void
     */
    protected function end()
    {
        // закрыть окно
        $this->response->add('setTo', array('id' => 'gcontroller_salbumimages_aggregate', 'func' => 'close'), true);
        // обновить список
        //$this->response->add('setTo', array('id' => 'gcontroller_salbumimages_grid', 'func' => 'reload'), true);
        $this->response->setMsgResult($this->_['title_process'], $this->_['msg_process_end'], true);
    }

    /**
     * Удалить все записи
     * 
     * @return void
     */
    protected function clear()
    {
        // соединение с базой данных
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();

        $query = new GDb_Query();
        // удаление текста изображений
        $sql = 'DELETE `il` FROM `site_album_images_l` `il`, `site_album_images` `i` '
             . 'WHERE `il`.`image_id`=`i`.`image_id` AND `i`.`album_id`=' . $this->_album['album_id'];
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_album_images_l') === false)
            throw new GSqlException();
        // удаление изображений
        $sql = 'DELETE FROM `site_album_images` WHERE `album_id`=' . $this->_album['album_id'];
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_album_images') === false)
            throw new GSqlException();
    }

    /**
     * Обработка изображения
     * 
     * @param string $filename имя файла
     * @return array
     */
    protected function updateImage($filename)
    {
        $result = array();

        // определение имени файла
        $tfileExt = $fileExt = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
        // идент. название файла
        if ($this->config->getFromCms('Albums', 'GENERATE/FILE/NAME'))
            $fileId = uniqid();
        else {
            $fileId = pathinfo($fileImage['name'], PATHINFO_FILENAME);
            $fileId = GString::toUrl($fileId,  $this->config->getFromCms('Site', 'LANGUAGE/ALIAS'));
        }
        $tfileId = $fileId . '_thumb';
        // сгенерировать название файла
        $nfilename = $this->_albumPath . $fileId . '.' . $fileExt;
        $tfilename = $this->_albumPath . $tfileId . '.' . $tfileExt;
        $result['image_entire_filename'] = $fileId . '.' . $fileExt;
        $result['image_thumb_filename'] = $tfileId . '.' . $tfileExt;

        // переименовать оригинал
        if (!rename($filename, $nfilename))
            throw new GException($this->_['title_process'], 'Unable to move file "%s"', $this->_albumPath);

        // изменить оригинал
        $width = (int) $this->config->getFromCms('Albums', 'IMAGE/ORIGINAL/WIDTH');
        $height = (int) $this->config->getFromCms('Albums', 'IMAGE/ORIGINAL/HEIGHT');
        $img = GFactory::getClass('Image', 'Image', $nfilename);
        $cmdImage = 'resize{"width": ' . $width . ', "height": ' . $height . '}';
        if (!$img->execute($cmdImage))
            throw new GException('Error', $this->_['msg_image_size']);
        $img->save($nfilename, $this->config->getFromCms('Albums', 'IMAGE/QUALITY', 0));
        $result['image_entire_resolution'] = $img->getSizeStr();

        // создать миниатюры
        $width = (int) $this->config->getFromCms('Albums', 'IMAGE/THUMB/WIDTH');
        $height = (int) $this->config->getFromCms('Albums', 'IMAGE/THUMB/HEIGHT');
        $img = GFactory::getClass('Image', 'Image', $nfilename);
        $cmdImage = 'resize{"width": ' . $width . ', "height": ' . $height . '}';
        if (!$img->execute($cmdImage))
            throw new GException('Error', $this->_['msg_error_create_thumb']);
        $img->save($tfilename);
        $result['image_thumb_resolution'] = $img->getSizeStr();
        $result['image_thumb_filesize'] = GFile::getFileSize($tfilename);

        $img = GFactory::getClass('Image', 'Image', $nfilename);
        // 
        if ($this->config->getFromCms('Albums', 'WATERMARK')) {
            $img->watermark(
                DOCUMENT_ROOT . $this->config->getFromCms('Site', 'WATERMARK/STAMP', ''),
                $this->config->getFromCms('Albums', 'WATERMARK/POSITION', '') 
            );
        }
        $img->save($nfilename, $this->config->getFromCms('Albums', 'IMAGE/QUALITY', 0));
        $result['image_entire_filesize'] = GFile::getFileSize($nfilename);

        return $result;
    }

    /**
     * Обработка элемента списка
     * 
     * @param array $item
     * @return void
     */
    protected function itemProcess($item = array())
    {
        // соединение с базой данных
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();

        $image = new GDb_Table('site_album_images_l', 'image_lid');
        $thumb = new GDb_Table('site_album_images', 'image_id');

        $data = $this->updateImage($item[0]);
        $data['album_id'] = $this->_album['album_id'];
        $data['image_index'] = $this->index + 1;
        $data['image_date'] = date('Y-m-d H:i:S');

        // добавить миниатюру
        if ($thumb->insert($data) === false)
            throw new GSqlException();
        // добавить изображение
        $data = array(
            'image_id'    => $thumb->getLastInsertId(),
            'language_id' => $this->config->getFromCms('Site', 'LANGUAGE/ID')
        );
        if ($image->insert($data) === false)
            throw new GSqlException();

        // выставить в списке ok
        $this->response->add('setTo', array('id' => $this->gridId, 'func' => 'setState', 'args' => array($this->index, 'ok')), true);
        $this->response->setMsgResult($this->_['title_process'], sprintf($this->_['msg_process'], $this->index + 1, sizeof($this->list)), true);
    }
}
?>