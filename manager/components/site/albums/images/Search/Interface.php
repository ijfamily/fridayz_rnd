<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск изображений в альбоме сайта"
 * Пакет контроллеров "Изображения альбома"
 * Группа пакетов     "Альбомы"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SAlbumImages_Search
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Interface');

/**
 * Поиск изображений в альбоме сайта
 * 
 * @category   Gear
 * @package    GController_SAlbumImages_Search
 * @subpackage Interface
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SAlbumImages_Search_Interface extends GController_Search_Interface
{
    /**
     * Идентификатор компонента
     *
     * @var string
     */
    public $gridId = 'gcontroller_salbums_grid';

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataSearch")
        $this->_cmp->setProps(
            array('title'       => $this->_['title_search'],
                  'iconSrc'     => $this->resourcePath . 'icon.png',
                  'gridId'      => $this->gridId,
                  'btnSearchId' => $this->gridId . '-bnt_search',
                  'url'         => $this->componentUrl . 'search/data/')
        );

        // виды полей для поиска записей
        $this->_cmp->editorFields = array(
          array('xtype' => 'numberfield'),
          array('xtype' => 'textfield'),
          array('xtype' => 'mn-field-combo-logic'),
          array('xtype' => 'mn-field-combo-logic'),
          array('xtype' => 'textfield'),
          array('xtype' => 'textfield'),
          array('xtype' => 'textfield'),
          array('xtype' => 'textfield'),
          array('xtype' => 'textfield'),
          array('xtype' => 'textfield'),
          array('xtype' => 'textfield'),
          array('xtype' => 'textfield'),
          array('xtype' => 'textfield')
        );

        parent::getInterface();
    }
}
?>