<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2013-2016 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Поиск в списке "Изображения альбома"',
    // поля
    'header_image_index'              => '№',
    'header_image_title'              => 'Заглавие',
    'header_image_visible'            => 'Показывать',
    'header_image_entire_filename'    => 'Файл изображения',
    'header_image_entire_resolution'  => 'Разрешение изображения',
    'header_image_entire_filesize'    => 'Размер изображения',
    'header_image_thumb_filename'     => 'Файл миниатюры',
    'header_image_thumb_resolution'   => 'Разрешение миниатюры',
    'header_image_thumb_filesize'     => 'Размер миниатюры'
);
?>