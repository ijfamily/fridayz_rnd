<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Изображения альбомов"
 * Группа пакетов     "Альбомы"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SAlbumImages
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // {S}- модуль "Site" {} - пакет контроллеров "Images of the albums" -> SAlbumImages
    'clsPrefix' => 'SAlbumImages',
    // использовать язык
    'language'  => 'ru-RU'
);
?>