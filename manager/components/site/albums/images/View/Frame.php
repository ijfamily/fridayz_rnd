<?php
/**
 * Gear Manager
 *
 * Контроллер         "Код фрейма просмотра"
 * Пакет контроллеров "Просмотр изображений"
 * Группа пакетов     "Фотоальбомы"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SAlbumImages_View
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Frame.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Frame');

/**
 * Код фрейма загрузчика
 * 
 * @category   Gear
 * @package    GController_SAlbumImages_View
 * @subpackage Frame
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Frame.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SAlbumImages_View_Frame extends GController_Frame
{
    /**
     * Возвращает дополнительные данные для формирования интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        parent::getDataInterface();

        $query = GFactory::getQuery();
        $sql = 'SELECT * FROM `site_albums` WHERE `album_id`=' . (int)$this->uri->id;
        if (($album = $query->getRecord($sql)) === false)
            throw new GSqlException();
        // если нет альбома
        if (empty($album))
            throw new GException('Warning', $this->_['msg_no_album']);
        $sql = 'SELECT * FROM `site_album_images` WHERE `album_id`=' . (int)$this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $images = array();
        $path = $this->config->getFromCms('Albums', 'DIR') . $album['album_folder'] . '/';
        while (!$query->eof()) {
            $img = $query->next();
            $images[] = array(
                'thumb'      => $img['image_thumb_filename'],
                'src'        => $img['image_entire_filename'],
                'resolution' => $img['image_entire_resolution'],
                'size'       => $img['image_entire_filesize'],
            );
        }
        // если нет изображений в альбоме
        if (sizeof($images) == 0)
            throw new GException('Warning', $this->_['msg_no_images']);

        return array(
            'count'  => sizeof($images),
            'album'  => $album['album_name'],
            'images' => $images,
            'path'   => $path
        );
    }

    /**
     * Вывод кода фрейма
     * 
     * @return void
     */
    public function frame($resPath = '')
    {
        $st = $this->session->get('user/settings');
        if (isset($st['theme']))
            $theme = $st['theme'];
        else
            $theme = 'default';
        $tpl = $this->getDataInterface();
?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <link href="/<?=PATH_APPLICATION;?>resources/themes/<?=$theme;?>/default.css" rel="stylesheet" />
        <link href="<?php echo $resPath;?>/lightgallery/css/lightgallery.css" rel="stylesheet" />
        <link href="<?php echo $resPath;?>/lightgallery/css/gallery.css" rel="stylesheet" />

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    </head>
    <body>
        <div class="header">
            <h1>Фотоальбом &laquo; <?php echo $tpl['album'];?> &raquo; / <?php echo $tpl['count'];?> шт.</h1>
        </div>
        <div id="gallery">
<?php
foreach ($tpl['images'] as $index => $img) {
    $src =  $tpl['path'] . $img['src'];
    $srcThumb = '/' . $tpl['path'] . $img['thumb'];
    $isExist = file_exists(DOCUMENT_ROOT . $src);
    if ($isExist) {
        $src = '/' . $src;
    } else {
        $src = $resPath . 'no-photo.png';
        $srcThumb = $resPath . 'no-photo-thumb.png';
    }

    echo '<a class="thumb', ($isExist ? '' : ' error'), '" href="', $src, '"  data-responsive="', $src, '" data-src="', $src, '" data-sub-html="', $tpl['album'], '">';
    echo '<div class="img"><img class="img-responsive" src="', $srcThumb, '"></div>';
    echo '<div class="info">';
    echo '<div class="title"><span>', $img['src'] , '</span></div>';
    echo '<div class="resolution">', $img['resolution'] , '</div>';
    echo '<div class="size">', $img['size'] , '</div>';
    echo '</div>';
    echo '</a>';
}
?>
        </div>

        <script type="text/javascript">$(document).ready(function(){ $('#gallery').lightGallery(); });</script>
        <script src="<?php echo $resPath;?>lightgallery/js/lightgallery.js"></script>
        <script src="<?php echo $resPath;?>lightgallery/js/lg-fullscreen.js"></script>
        <script src="<?php echo $resPath;?>lightgallery/js/lg-thumbnail.js"></script>
        <script src="<?php echo $resPath;?>lightgallery/js/lg-video.js"></script>
        <script src="<?php echo $resPath;?>lightgallery/js/lg-autoplay.js"></script>
        <script src="<?php echo $resPath;?>lightgallery/js/lg-zoom.js"></script>
        <script src="<?php echo $resPath;?>lightgallery/js/lg-hash.js"></script>
        <script src="<?php echo $resPath;?>lightgallery/js/lg-pager.js"></script>
        <script src="<?php echo $resPath;?>lightgallery/js/jquery.mousewheel.min.js"></script>
    </body>
</html>
<?php
    }
}
?>