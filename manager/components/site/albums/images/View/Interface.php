<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс просмотра изображений"
 * Пакет контроллеров "Просмотр изображений"
 * Группа пакетов     "Фотоальбомы"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SAlbumImages_View
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::ext('Container/Panel');
Gear::controller('Interface');

/**
 * Интерфейс просмотра изображений
 * 
 * @category   Gear
 * @package    GController_SAlbumImages_View
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SAlbumImages_View_Interface extends GController_Interface
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_salbums_grid';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // панель (ExtJS class "Ext.Panel")
        $component = array(
            'id'            => $this->classId,
            'closable'      => true,
            'component'     => array('container' => 'content-tab', 'destroy' => true),
        );
        $this->_cmp = new Ext_Panel($component);
    }

    /**
     * Возвращает дополнительные данные для формирования интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        parent::getDataInterface();

        $query = GFactory::getQuery();
        $sql = 'SELECT * FROM `site_albums` WHERE `album_id`=' . (int)$this->uri->id;
        if (($album = $query->getRecord($sql)) === false)
            throw new GSqlException();
        // если нет альбома
        if (empty($album))
            throw new GException('Warning', $this->_['msg_no_album']);
        $sql = 'SELECT COUNT(*) `count` FROM `site_album_images` WHERE `album_id`=' . (int)$this->uri->id;
        if (($images = $query->getRecord($sql)) === false)
            throw new GSqlException();
        // если нет изображений в альбоме
        if (!$images['count'])
            throw new GException('Warning', $this->_['msg_no_images']);

        return array('title' => sprintf($this->_['title_view'], $album['album_name']));
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $data['title'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_view'],
                  'titleEllipsis' => 40,
            )
        );
        $this->_cmp->items->add(array(
            'xtype' => 'mn-iframe',
            'url'   => '/' . PATH_APPLICATION . ROUTER_SCRIPT . $this->componentUrl . 'view/frame/' . $this->uri->id
        ));

        parent::getInterface();
    }
}
?>