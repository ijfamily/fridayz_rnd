<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_view'   => 'Просморт изображений фотоальбома "%s"',
    'tooltip_view' => 'просморт изображений фотоальбома',
    // сообщения
    'msg_no_album'  => 'Нет выбранного фотоальбома для просмотра!',
    'msg_no_images' => 'Нет изображений в фотоальбоме для просмотра!'
);
?>