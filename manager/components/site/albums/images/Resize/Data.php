<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные наполнения фотоальбома"
 * Пакет контроллеров "Профиль наполнения фотоальбома"
 * Группа пакетов     "Сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SAlbumImages_Aggregate
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('String');
Gear::library('File');
Gear::controller('Profile/Data');

/**
 * Данные профиля наполнения фотоальбома
 * 
 * @category   Gear
 * @package    GController_SAlbumImages_Aggregate
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SAlbumImages_Resize_Data extends GController_Profile_Data
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_salbums_grid';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // едент. галереи
        $this->_albumId = $this->store->get('record', 0, 'gcontroller_salbums_grid');;
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        parent::dataAccessView();

        $query = new GDb_Query();
        // список адресов
        $sql = 'SELECT * FROM `site_albums` WHERE `album_id`=' . $this->_albumId;
        if (($album = $query->getRecord($sql)) === false)
            throw new GSqlException();
        $this->_albumPath = DOCUMENT_ROOT . $this->config->getFromCms('Albums', 'DIR') . $album['album_folder'] . '/';

        if (!is_dir($this->_albumPath))
            throw new GException($this->_['title_aggregate'], 'The directory "%s" can not be opened', $this->_albumPath);
        $handle = @opendir($this->_albumPath);
        if ($handle === false)
            throw new GException($this->_['title_aggregate'], 'The directory "%s" can not be opened', $this->_albumPath);
        $images = $data = array();
        $index = 0;
        while(false !== ($file = readdir($handle))) {
            if($file != '.' && $file != '..') {
                $images[] = $this->_albumPath . $file;
                $data[] = array($index, $file, $file, 0);
                $index++;
            }
        }

       $this->response->add('setTo', array(
            array('id' => 'grid-process-resize', 'func' => 'loadData', 'args' => array($data)),
            //array('id' => 'grid-process-resize', 'func' => 'process', 'args' => array(0))
       ));
       
        
    }
}
?>