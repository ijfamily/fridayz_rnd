<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные для интерфейса профиля изображения"
 * Пакет контроллеров "Профиль загрузчика фотоальбома"
 * Группа пакетов     "Фотоальбомы"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SAlbumImages_Uploader
 * @copyright  Copyright (c) 2013-2016 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');
Gear::library('File');

/**
 * Данные для интерфейса профиля изображения
 * 
 * @category   Gear
 * @package    GController_SAlbumImages_Uploader
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SAlbumImages_Uploader_Data extends GController_Profile_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array(
        'album_id', 'image_index', 'image_entire_filename', 'image_entire_resolution',
        'image_entire_filesize', 'image_entire_style', 'image_thumb_filename', 'image_thumb_resolution',
         'image_thumb_filesize', 'image_date', 'image_visible', 'sys_date_insert',
        'sys_date_update'
    );

    /**
     * Первичный ключ таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'image_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_album_images';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_salbums_grid';

    /**
     * фотоальбом
     *
     * @var mixed
     */
    protected $_album = false;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // фотоальбом
        $this->_album = $this->store->get('album');
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param string $sql запрос SQL на вывод данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        $sql = 'SELECT `i`.*, `g`.`album_folder` FROM `site_albums` `g` JOIN `site_album_images` `i` USING(`album_id`) '
             . 'WHERE `i`.`image_id`=' . (int) $this->uri->id;

        parent::dataView($sql);
    }

    /**
     * Событие наступает после успешного добавления данных
     * 
     * @param array $data сформированные данные полученные после запроса к таблице
     * @return void
     */
    protected function dataInsertComplete($data = array())
    {
        // соединение с базой данных (т.к. для лога ранее возможно было другое соединение)
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();

        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(
            array('id' => 'site', 'func' => 'reload'),
            array('id' => 'frmUploader', 'func' => 'reload'),
            //array('id' => 'gcontroller_salbumimages_grid', 'func' => 'reload')
        ));
    }

    /**
     * Вставка данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataInsert($params = array())
    {
        $this->dataAccessInsert();

        $tblText = new GDb_Table('site_album_images_l', 'image_lid');
        // допустимые поля
        $fields = array('image_title', 'image_thumb_title', 'image_longdesc');
        // список доступных языков
        $langs = $this->config->getFromCms('Site', 'LANGUAGES');

        // список загружаемых изображений
        $list = $this->store->get('upload');
        // если списка нет
        if (empty($list))
            throw new GException('Warning', $this->_['msg_no_files']);
        // если фотоальбом ранее не выбран
        if (empty($this->_album))
            throw new GException($this->_['title_process'], $this->_['msg_empty_album']);

        $fromPath = DOCUMENT_ROOT . $this->config->getFromCms('Site', 'DIR/TEMP');
        //GFactory::getDg()->log($fromPath, '$fromPath');
        $toPath =  DOCUMENT_ROOT . $this->config->getFromCms('Albums', 'DIR') . $this->_album['album_folder'] . '/';
        //GFactory::getDg()->log($toPath, '$toPath');

        $tblImage = new GDb_Table($this->tableName, $this->idProperty, $this->fields);
        // добавления списка  изображений
        foreach ($list as $index => $item) {
            GFile::move($fromPath . $item['image_entire_filename'], $toPath . $item['image_entire_filename']);
            GFile::move($fromPath . $item['image_thumb_filename'], $toPath . $item['image_thumb_filename']);

            $image = array(
                'album_id'                => $this->_album['album_id'],
                'image_index'             => 1,
                'image_entire_filename'   => $item['image_entire_filename'],
                'image_entire_resolution' => $item['image_entire_resolution'],
                'image_entire_filesize'   => $item['image_entire_filesize'],
                'image_thumb_filename'    => $item['image_thumb_filename'],
                'image_thumb_resolution'  => $item['image_thumb_resolution'],
                'image_thumb_filesize'    => $item['image_thumb_filesize'],
                'image_date'              => date('Y-m-d H:i:s'),
                'image_visible'           => 1,
                'image_cover'             => 0,
                'sys_date_insert'         => date('Y-m-d'),
                'sys_time_insert'         => date('H:i:s'),
                'sys_user_insert'         => $this->session->get('user_id')
            );
            // вставка данных
            if ($tblImage->insert($image) === false)
                throw new GSqlException();

            //$this->_recordId = $tblImage->getLastInsertId();
        }

        // удаляем список для загрузки файлов
        $this->store->set('upload', array());

        $this->dataInsertComplete($params);
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        $params['image_date'] = date('Y-m-d H:i:s');
        // если состояние формы "правка"
        if ($this->isUpdate) return;

        $query = new GDb_Query();
        // фотоальбомы
        $sql = 'SELECT * FROM `site_albums` WHERE `album_id`=' . $this->_albumId;
        if (($this->_album = $query->getRecord($sql)) === false)
                throw new GSqlException();

        // идент. статьи
        $params['album_id'] = $this->_albumId;
        $fileImage = $fileThumb = false;
        $cmdImage = $cmdThumb = '';

        // если нет изображения для загрузки
        if (($fileImage = $this->input->file->get('image')) === false)
            throw new GException('Adding data', $this->_['msg_file_image_empty']);
        // определение имени файла
        $tfileExt = $fileExt = strtolower(pathinfo($fileImage['name'], PATHINFO_EXTENSION));
        // идент. название файла
        if ($this->config->getFromCms('Albums', 'GENERATE/FILE/NAME'))
            $fileId = uniqid();
        else {
            $fileId = pathinfo($fileImage['name'], PATHINFO_FILENAME);
            $fileId = GString::toUrl($fileId,  $this->config->getFromCms('Site', 'LANGUAGE/ALIAS'));
        }
        $tfileId = $fileId . '_thumb';
        // сгенерировать название файла
        $filename = $fileId . '.' . $fileExt;
        $tfilename = $tfileId . '.' . $tfileExt;


        $uploadPath = DOCUMENT_ROOT . $this->config->getFromCms('Albums', 'DIR') . $this->_album['album_folder'] . '/';
        $uploadExt = explode(',', strtolower($this->config->getFromCms('Site', 'FILES/EXT/IMAGES')));
        // проверка существования изображения
        if (file_exists($uploadPath . $filename))
            throw new GException('Error', sprintf($this->_['msg_file_exists'], $uploadPath . $filename));
        // проверка существования эскиза изображения
        if (file_exists($uploadPath . $tfilename))
            throw new GException('Error', sprintf($this->_['msg_file_exists'], $uploadPath . $tfilename));

        // загрузка изображения
        GFile::upload($fileImage, $uploadPath . $filename, $uploadExt);

        // изменить оригинал
        $width = (int) $this->config->getFromCms('Albums', 'IMAGE/ORIGINAL/WIDTH');
        $height = (int) $this->config->getFromCms('Albums', 'IMAGE/ORIGINAL/HEIGHT');
        $img = GFactory::getClass('Image', 'Image', $uploadPath . $filename);
        $cmdImage = 'resize{"width": ' . $width . ', "height": ' . $height . '}';
        if (!$img->execute($cmdImage))
            throw new GException('Error', $this->_['msg_image_size']);
        $img->save($uploadPath . $filename, $this->config->getFromCms('Albums', 'IMAGE/QUALITY', 0));

        // создать миниатюры
        $width = (int) $this->config->getFromCms('Albums', 'IMAGE/THUMB/WIDTH');
        $height = (int) $this->config->getFromCms('Albums', 'IMAGE/THUMB/HEIGHT');
        $img = GFactory::getClass('Image', 'Image', $uploadPath . $filename);
        $cmdImage = 'resize{"width": ' . $width . ', "height": ' . $height . '}';
        if ($img->execute($cmdImage)) {
            // изменение изображения
            $name = pathinfo($filename, PATHINFO_FILENAME) . '_thumb.' . pathinfo($filename, PATHINFO_EXTENSION);
            $img->save($uploadPath . $name);
        }
        // обновляем поля изображения
        $params['image_thumb_filename'] = $tfilename;
        $params['image_thumb_filesize'] = GFile::getFileSize($uploadPath . $tfilename);
        $params['image_thumb_resolution'] = $img->getSizeStr();

        // создание водянного знака
        $img = GFactory::getClass('Image', 'Image', $uploadPath . $filename);
        if ($this->config->getFromCms('Albums', 'WATERMARK')) {
            $img->watermark(
                DOCUMENT_ROOT . $this->config->getFromCms('Site', 'WATERMARK/STAMP', ''),
                $this->config->getFromCms('Albums', 'WATERMARK/POSITION', '') 
            );
        }
        $img->save($uploadPath . $filename, $this->config->getFromCms('Albums', 'IMAGE/QUALITY', 0));
        // обновляем поля изображения
        $params['image_entire_filename'] = $filename;
        $params['image_entire_filesize'] = GFile::getFileSize($uploadPath . $filename);
        $params['image_entire_resolution'] = $img->getSizeStr();

    }
}
?>