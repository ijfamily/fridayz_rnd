<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Загрузка изображений в фотоальбом',
    'text_btn_help'        => 'Справка',
    // сообщения
    'title_process'     => 'Наполнение фотоальбома изображениями',
    'msg_empty_album'   => 'Нет информации о фотоальбоме!',
    'msg_no_files'      => 'Нет изображений для загрузки!',
    'msg_thumb_exist'   => 'В каталоге альбома находятся миниатюры изображений (удалите их и повторите снова)!',
    'msg_empty_list'    => 'Невозможно выполнить наполнение фотоальбома (список изображений пуст)!',
    'msg_empty_album'   => 'Нет информации о фотоальбоме!',
    'msg_process_start' => 'Начался процесс обработки изображений!',
    'msg_process_end'   => 'Наполнение альбома выполнено!',
    'msg_process'       => 'Обработано <b>%s/%s</b> изображений',
    'msg_image_size'    => 'Невозможно изменить размеры изображения!',
    'msg_error_create_thumb' => 'Ошибка в создании миниатюры изображения!',
    'msg_index_process' => 'Неправильно указан порядок обработки данных!'
);
?>