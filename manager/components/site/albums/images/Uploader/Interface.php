<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля загрузчика"
 * Пакет контроллеров "Профиль загрузчика фотоальбома"
 * Группа пакетов     "Фотоальбомы"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SAlbumImages_Uploader
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля загрузчика
 * 
 * @category   Gear
 * @package    GController_SAlbumImages_Uploader
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SAlbumImages_Uploader_Interface extends GController_Profile_Interface
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_salbums_grid';

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // если список ранее был, уничтожаем его
        $this->store->set('upload', array());

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('id'            => $this->classId,
                  'title'         => $this->_['title_profile_insert'],
                  'titleEllipsis' => 45,
                  'gridId'        => 'gcontroller_salbums_grid',
                  'width'         => 720,
                  'height'        => 500,
                  'state'         => 'insert',
                  'resizable'     => false,
                  'stateful'      => false,
                  'buttonsAdd'    => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-site-albums-aggregate',
                            'iconCls' => 'icon-item-info')
                  ))
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype' => 'mn-iframe',
                  'id'    => 'frmUploader',
                  'url'   => ROUTER_SCRIPT . 'site/albums/images/' . ROUTER_DELIMITER . 'uploader/frame/')
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($items);
        $form->labelWidth = 50;
        $form->layout = 'fit';
        $form->url = $this->componentUrl . 'uploader/';

        parent::getInterface();
    }
}
?>