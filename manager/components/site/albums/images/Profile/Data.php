<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные для интерфейса профиля изображения"
 * Пакет контроллеров "Изображение альбома"
 * Группа пакетов     "Альбомы"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SAlbumImages_Profile
 * @copyright  Copyright (c) 2013-2016 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('String');
Gear::library('File');
Gear::controller('Profile/Data');

/**
 * Данные для интерфейса профиля изображения
 * 
 * @category   Gear
 * @package    GController_SAlbumImages_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SAlbumImages_Profile_Data extends GController_Profile_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array(
        'album_id', 'image_index', 'image_entire_filename', 'image_entire_resolution',
        'image_entire_filesize', 'image_entire_style', 'image_thumb_filename', 'image_thumb_resolution',
         'image_thumb_filesize', 'image_date', 'image_visible', 'sys_date_insert',
        'sys_date_update'
    );

    /**
     * Первичный ключ таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'image_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_album_images';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_salbums_grid';

    /**
     * Идентификатор галереи
     *
     * @var integer
     */
    protected $_albumId = 0;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // едент. галереи
        $this->_albumId = $this->input->get('album_id', 0);
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param string $sql запрос SQL на вывод данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        $sql = 'SELECT `i`.*, `g`.`album_folder` FROM `site_albums` `g` JOIN `site_album_images` `i` USING(`album_id`) '
             . 'WHERE `i`.`image_id`=' . (int) $this->uri->id;

        parent::dataView($sql);
    }

    /**
     * Вставка текста изображения
     * 
     * @param  integer $imageId идент. изображения
     * @return void 
     */
    protected function textInsert($imageId)
    {
        $ln = $this->input->get('ln', false);
        if (empty($ln)) return;

        $table = new GDb_Table('site_album_images_l', 'image_lid');
        // допустимые поля
        $fields = array('image_title', 'image_thumb_title', 'image_longdesc');
        // список доступных языков
        $langs = $this->config->getFromCms('Site', 'LANGUAGES');
        foreach ($langs as $alias => $lang) {
            // если есть язык
            if (isset($ln[$lang['id']])) {
                $item = $ln[$lang['id']];
                $data = array('image_id' => $imageId, 'language_id' => $lang['id']);
                for ($j = 0; $j < sizeof($fields); $j++) {
                    if (isset($item[$fields[$j]]))
                        $data[$fields[$j]] = $item[$fields[$j]];
                }
                if ($table->insert($data) === false)
                    throw new GSqlException();
            }
        }
    }

    /**
     * Обновление текста изображения
     * 
     * @return void
     */
    protected function textUpdate()
    {
        $ln = $this->input->get('ln', false);
        if (empty($ln)) return;

        $table = new GDb_Table('site_album_images_l', 'image_lid');
        // допустимые поля
        $fields = array('image_title', 'image_thumb_title', 'image_longdesc');
        // список доступных языков
        $langs = $this->config->getFromCms('Site', 'LANGUAGES');
        foreach ($langs as $alias => $lang) {
            // если есть язык
            if (isset($ln[$lang['id']])) {
                $item = $ln[$lang['id']];
                // если есть только идент.
                if (sizeof($item) == 1) continue;
                $data = array();
                for ($j = 0; $j < sizeof($fields); $j++) {
                    if (isset($item[$fields[$j]]))
                        $data[$fields[$j]] = $item[$fields[$j]];
                }
                // если запись ранее не была создана
                if (empty($item['image_lid'])) {
                    $data['image_id'] = $this->uri->id;
                    $data['language_id'] = $lang['id'];
                    if ($table->insert($data) === false)
                        throw new GSqlException();
                } else {
                    // обновить запись
                    if ($table->update($data, $item['image_lid']) === false)
                        throw new GSqlException();
                }
            }
        }
    }

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_profile_update'], $record['image_entire_filename']);
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        // массив полей таблицы ($tableName)
        $this->fields = array('image_index', 'image_date', 'image_visible');

        parent::dataUpdate($params);
    }

    /**
     * Событие наступает после успешного обновления данных
     * 
     * @param array $data сформированные данные полученные после запроса к таблице
     * @return void
     */
    protected function dataUpdateComplete($data = array())
    {
        // соединение с базой данных (т.к. для лога ранее возможно было другое соединение)
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();

        // обновление текста изображения
        $this->textUpdate($this->_recordId);
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Событие наступает после успешного добавления данных
     * 
     * @param array $data сформированные данные полученные после запроса к таблице
     * @return void
     */
    protected function dataInsertComplete($data = array())
    {
        // соединение с базой данных (т.к. для лога ранее возможно было другое соединение)
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();

        // вставка текста изображения
        $this->textInsert($this->_recordId);
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Событие наступает после успешного удаления данных
     * 
     * @return void
     */
    protected function dataDeleteComplete()
    {
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));

    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        $query = new GDb_Query();
        // изображения статьи
        $sql = 'SELECT `i`.*, `g`.`album_folder` FROM `site_album_images` `i` JOIN `site_albums` `g` USING(`album_id`) '
             . 'WHERE `i`.`image_id` IN (' . $this->uri->id. ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $path = DOCUMENT_ROOT . $this->config->getFromCms('Albums', 'DIR');
        while (!$query->eof()) {
            $image = $query->next();
            // если есть изображение
            if ($image['image_entire_filename'])
                GFile::delete($path . $image['album_folder'] . '/' . $image['image_entire_filename'], false);
            // если есть эскиз
            if ($image['image_thumb_filename'])
                GFile::delete($path . $image['album_folder'] . '/' . $image['image_thumb_filename'], false);
        }
        // удаление изображений статьи
        $sql = 'DELETE FROM `site_album_images_l` WHERE `image_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_album_images_l') === false)
            throw new GSqlException();
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
     /*
    protected function isDataCorrect(&$params)
    {
        $params['image_date'] = date('Y-m-d H:i:s');
        // если состояние формы "правка"
        if ($this->isUpdate) return;

        $query = new GDb_Query();
        // галерея
        $sql = 'SELECT * FROM `site_albums` WHERE `album_id`=' . $this->_albumId;
        if (($this->_album = $query->getRecord($sql)) === false)
                throw new GSqlException();

        // идент. статьи
        $params['album_id'] = $this->_albumId;
        $fileImage = $fileThumb = false;
        $cmdImage = $cmdThumb = '';

        // если выбран флажок "Изменение изображения после загрузки"
        if ($this->input->get('profile_image_create', 0)) {
            // но не выбран профиль изображения
            $cmdImage = $this->input->get('profile_image_id', 0);
            if (empty($cmdImage))
                throw new GException('Error', $this->_['msg_profile_image_empty']);
        }
        // если нет изображения для загрузки
        if (($fileImage = $this->input->file->get('image')) === false)
            throw new GException('Adding data', $this->_['msg_file_image_empty']);
        // определение имени файла
        $tfileExt = $fileExt = strtolower(pathinfo($fileImage['name'], PATHINFO_EXTENSION));
        // идент. название файла
        if ($this->input->get('image_name_create'))
            $fileId = uniqid();
        else {
            $fileId = pathinfo($fileImage['name'], PATHINFO_FILENAME);
            $fileId = GString::toUrl($fileId,  $this->config->getFromCms('Site', 'LANGUAGE/ALIAS'));
        }
        $tfileId = $fileId . '_thumb';
        // сгенерировать название файла
        $filename = $fileId . '.' . $fileExt;
        $tfilename = $tfileId . '.' . $tfileExt;

        // если выбран флажок "Создания эскиза из изображения"
        if ($this->input->get('profile_thumb_create', 0)) {
            // но не выбран профиль изображения
            $cmdThumb = $this->input->get('profile_thumb_id', 0);
            if (empty($cmdThumb))
                throw new GException('Error', $this->_['msg_profile_thumb_empty']);
        // если необходимо выбрать эскиз изображения
        } else {
            // если нет эскиза для загрузки
            if (($fileThumb = $this->input->file->get('thumb')) === false)
                throw new GException('Adding data', $this->_['msg_file_thumb_empty']);
            // определение имени файла
            $tfileExt = strtolower(pathinfo($fileThumb['name'], PATHINFO_EXTENSION));
            $tfileId = pathinfo($fileThumb['name'], PATHINFO_FILENAME);
            $tfileId = GString::toUrl($tfileId, $this->config->getFromCms('Site', 'LANGUAGE/ALIAS'));

            // сгенерировать название файла
            $tfilename = $tfileId . '.' . $tfileExt;
        }

        $uploadPath = DOCUMENT_ROOT . $this->config->getFromCms('Albums', 'DIR') . $this->_album['album_folder'] . '/';
        $uploadExt = explode(',', strtolower($this->config->getFromCms('Site', 'FILES/EXT/IMAGES')));
        // проверка существования изображения
        if (file_exists($uploadPath . $filename))
            throw new GException('Error', sprintf($this->_['msg_file_exists'], $uploadPath . $filename));
        // проверка существования эскиза изображения
        if (file_exists($uploadPath . $tfilename))
            throw new GException('Error', sprintf($this->_['msg_file_exists'], $uploadPath . $tfilename));

        // загрузка изображения
        GFile::upload($fileImage, $uploadPath . $filename, $uploadExt);
        $img = GFactory::getClass('Image', 'Image', $uploadPath . $filename);
        // если выбран профиль изображения
        if ($cmdImage) {
            if ($img->execute($cmdImage)) {
                // изменение изображения
                $img->save($uploadPath . $filename);
                // обновляем поля изображения
                $params['image_entire_filename'] = $filename;
                $params['image_entire_filesize'] = GFile::getFileSize($uploadPath . $filename);
                $params['image_entire_resolution'] = $img->getSizeStr();
            }
        } else {
            // обновляем поля изображения
            $params['image_entire_filename'] = $filename;
            $params['image_entire_filesize'] = GFile::fileSize($fileImage['size']);
            $params['image_entire_resolution'] = $img->getSizeStr();
        }

        // попытка загрузить эскиз
        // если выбран профиль изображения
        if ($cmdThumb) {
            $img = GFactory::getClass('Image', 'Image', $uploadPath . $filename);
            if ($img->execute($cmdThumb)) {
                // создание эскиза
                $img->save($uploadPath . $tfilename);
                // обновляем поля эскиза
                $params['image_thumb_filename'] = $tfilename;
                $params['image_thumb_filesize'] = GFile::getFileSize($uploadPath . $tfilename);
                $params['image_thumb_resolution'] = $img->getSizeStr();
            }
        // если эскиз из выбранного файла
        } else {
            // загрузка изображения
            GFile::upload($fileThumb, $uploadPath . $tfilename, $uploadExt);
            $img = GFactory::getClass('Image', 'Image', $uploadPath . $tfilename);
            // обновляем поля изображения
            $params['image_thumb_filename'] = $tfilename;
            $params['image_thumb_filesize'] = GFile::getFileSize($uploadPath . $tfilename);
            $params['image_thumb_resolution'] = $img->getSizeStr();
        }
    }
    */

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        $params['image_date'] = date('Y-m-d H:i:s');
        // если состояние формы "правка"
        if ($this->isUpdate) return;

        $query = new GDb_Query();
        // фотоальбомы
        $sql = 'SELECT * FROM `site_albums` WHERE `album_id`=' . $this->_albumId;
        if (($this->_album = $query->getRecord($sql)) === false)
                throw new GSqlException();

        // идент. статьи
        $params['album_id'] = $this->_albumId;
        $fileImage = $fileThumb = false;
        $cmdImage = $cmdThumb = '';

        // если нет изображения для загрузки
        if (!$this->input->hasFile())
            throw new GException('Adding data', $this->_['msg_file_image_empty']);
        if (($fileImage = $this->input->file->get('image')) === false)
            throw new GException('Adding data', $this->_['msg_file_image_empty']);
        // определение имени файла
        $tfileExt = $fileExt = strtolower(pathinfo($fileImage['name'], PATHINFO_EXTENSION));
        // идент. название файла
        if ($this->config->getFromCms('Albums', 'GENERATE/FILE/NAME'))
            $fileId = uniqid();
        else {
            $fileId = pathinfo($fileImage['name'], PATHINFO_FILENAME);
            $fileId = GString::toUrl($fileId,  $this->config->getFromCms('Site', 'LANGUAGE/ALIAS'));
        }
        $tfileId = $fileId . '_thumb';
        // сгенерировать название файла
        $filename = $fileId . '.' . $fileExt;
        $tfilename = $tfileId . '.' . $tfileExt;


        $uploadPath = DOCUMENT_ROOT . $this->config->getFromCms('Albums', 'DIR') . $this->_album['album_folder'] . '/';
        $uploadExt = explode(',', strtolower($this->config->getFromCms('Site', 'FILES/EXT/IMAGES')));
        // проверка существования изображения
        if (file_exists($uploadPath . $filename))
            throw new GException('Error', sprintf($this->_['msg_file_exists'], $uploadPath . $filename));
        // проверка существования эскиза изображения
        if (file_exists($uploadPath . $tfilename))
            throw new GException('Error', sprintf($this->_['msg_file_exists'], $uploadPath . $tfilename));

        // загрузка изображения
        GFile::upload($fileImage, $uploadPath . $filename, $uploadExt);

        // изменить оригинал
        $width = (int) $this->config->getFromCms('Albums', 'IMAGE/ORIGINAL/WIDTH');
        $height = (int) $this->config->getFromCms('Albums', 'IMAGE/ORIGINAL/HEIGHT');
        $img = GFactory::getClass('Image', 'Image', $uploadPath . $filename);
        $cmdImage = 'resize{"width": ' . $width . ', "height": ' . $height . '}';
        if (!$img->execute($cmdImage))
            throw new GException('Error', $this->_['msg_image_size']);
        $img->save($uploadPath . $filename, $this->config->getFromCms('Albums', 'IMAGE/QUALITY', 0));

        // создать миниатюры
        $width = (int) $this->config->getFromCms('Albums', 'IMAGE/THUMB/WIDTH');
        $height = (int) $this->config->getFromCms('Albums', 'IMAGE/THUMB/HEIGHT');
        $img = GFactory::getClass('Image', 'Image', $uploadPath . $filename);
        $cmdImage = 'resize{"width": ' . $width . ', "height": ' . $height . '}';
        if ($img->execute($cmdImage)) {
            // изменение изображения
            $name = pathinfo($filename, PATHINFO_FILENAME) . '_thumb.' . pathinfo($filename, PATHINFO_EXTENSION);
            $img->save($uploadPath . $name);
        }
        // обновляем поля изображения
        $params['image_thumb_filename'] = $tfilename;
        $params['image_thumb_filesize'] = GFile::getFileSize($uploadPath . $tfilename);
        $params['image_thumb_resolution'] = $img->getSizeStr();

        $img = GFactory::getClass('Image', 'Image', $uploadPath . $filename);
        // 
        if ($this->config->getFromCms('Albums', 'WATERMARK')) {
            $img->watermark(
                DOCUMENT_ROOT . $this->config->getFromCms('Site', 'WATERMARK/STAMP', ''),
                $this->config->getFromCms('Albums', 'WATERMARK/POSITION', '') 
            );
        }
        $img->save($uploadPath . $filename, $this->config->getFromCms('Albums', 'IMAGE/QUALITY', 0));
        // обновляем поля изображения
        $params['image_entire_filename'] = $filename;
        $params['image_entire_filesize'] = GFile::getFileSize($uploadPath . $filename);
        $params['image_entire_resolution'] = $img->getSizeStr();

    }

    /**
     * Предварительная обработка записи
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
       $path = '/' . $this->config->getFromCms('Albums', 'DIR') . $this->_data['album_folder'] . '/';
        // ресурсы изображения
        $record['image_entire_url'] = 'http://' . $_SERVER['SERVER_NAME'] . $path . $record['image_entire_filename'];
        // ресурсы эскиза
        if (empty($record['image_thumb_filename'])) {
            $record['image_thumb_url'] = '';
        } else {
            $record['image_thumb_url'] = 'http://' . $_SERVER['SERVER_NAME'] . $path . $record['image_thumb_filename'];
        }

        return $record;
    }
}
?>