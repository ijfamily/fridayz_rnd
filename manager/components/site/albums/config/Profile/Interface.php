<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля настроеек альбомов"
 * Пакет контроллеров "Настройки альбомов"
 * Группа пакетов     "Альбомы"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SAlbumsConfig_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля настроеек альбомов
 * 
 * @category   Gear
 * @package    GController_SAlbumsConfig_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SAlbumsConfig_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // Ext_Window (ExtJS class 'Manager.window.DataProfile')
        $this->_cmp->setProps(
            array('title'           => $this->_['title_profile_update'],
                  'titleEllipsis'   => 40,
                  'width'           => 640,
                  'autoHeight'      => true,
                  'resizable'       => false,
                  'btnDeleteHidden' => true,
                  'stateful'        => false,
                  'buttonsAdd'      => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-site-albums-config',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );
        $this->_cmp->state = 'update';

        $items = array(
            array('xtype' => 'label', 'html' => $this->_['html_desc']),
            array('xtype'      => 'fieldset',
                  'labelWidth' => 90,
                  'width'      => 592,
                  'title'      => $this->_['title_fs_catalog'],
                  'items'      => array(
                      array('xtype'      => 'textfield',
                            'fieldLabel' => $this->_['label_catalog'],
                            'width'      => '96%',
                            'checkDirty' => false,
                            'name'       => 'settings[DIR]')
                  )
            ),
            array('xtype'      => 'fieldset',
                  'title'      => $this->_['title_fieldset_original'],
                  'labelWidth' => 90,
                  'width'      => 592,
                  'items'      => array(
                      array('xtype'      => 'numberfield',
                            'fieldLabel' => $this->_['label_image_width'],
                            'width'      => 100,
                            'name'       => 'settings[IMAGE/ORIGINAL/WIDTH]',
                            'checkDirty' => false),
                      array('xtype'      => 'numberfield',
                            'fieldLabel' => $this->_['label_image_height'],
                            'width'      => 100,
                            'name'       => 'settings[IMAGE/ORIGINAL/HEIGHT]',
                            'checkDirty' => false),
                      array('xtype' => 'label', 'html' => $this->_['label_image_info']),
                  )
            ),
            array('xtype'      => 'fieldset',
                  'title'      => $this->_['title_fieldset_thumb'],
                  'labelWidth' => 90,
                  'width'      => 592,
                  'items'      => array(
                      array('xtype'      => 'numberfield',
                            'fieldLabel' => $this->_['label_image_width'],
                            'width'      => 100,
                            'name'       => 'settings[IMAGE/THUMB/WIDTH]',
                            'checkDirty' => false),
                      array('xtype'      => 'numberfield',
                            'fieldLabel' => $this->_['label_image_height'],
                            'width'      => 100,
                            'name'       => 'settings[IMAGE/THUMB/HEIGHT]',
                            'checkDirty' => false),
                      array('xtype' => 'label', 'html' => $this->_['label_image_info2'])
                  )
            ),
            array('xtype'      => 'fieldset',
                  'labelWidth' => 90,
                  'width'      => 592,
                  'title'      => $this->_['title_fs_watermaker'],
                  'items'      => array(
                      array('xtype'      => 'checkbox',
                            'fieldLabel' => $this->_['label_use'],
                            'checkDirty' => false,
                            'name'       => 'settings[WATERMARK]'),
                      array('xtype'         => 'combo',
                            'fieldLabel'    => $this->_['label_position'],
                            'name'          => 'settings[WATERMARK/POSITION]',
                            'checkDirty'    => false,
                            'editable'      => false,
                            'width'         => 180,
                            'typeAhead'     => false,
                            'triggerAction' => 'all',
                            'mode'          => 'local',
                            'store'         => array(
                                'xtype'  => 'arraystore',
                                'fields' => array('display', 'value'),
                                'data'   => $this->_['data_position']
                            ),
                            'hiddenName'    => 'settings[WATERMARK/POSITION]',
                            'valueField'    => 'value',
                            'displayField'  => 'display',
                            'allowBlank'    => true),

                  )
            ),
            array('xtype'      => 'checkbox',
                  'fieldLabel' => $this->_['label_generate'],
                  'checkDirty' => false,
                  'name'       => 'settings[GENERATE/FILE/NAME]'),
            array('xtype'      => 'numberfield',
                  'fieldLabel' => $this->_['label_image_quality'],
                  'width'      => 100,
                  'value'      => 100,
                  'name'       => 'settings[IMAGE/QUALITY]',
                  'checkDirty' => false),
            array('xtype' => 'label', 'html' => $this->_['info_image_quality']),
            array('xtype'      => 'numberfield',
                  'fieldLabel' => $this->_['label_list_limit'],
                  'width'      => 100,
                  'value'      => 100,
                  'name'       => 'settings[LIST/LIMIT]',
                  'checkDirty' => false)
        );


        // форма (ExtJS class 'Manager.form.DataProfile')
        $form = $this->_cmp->items->get(0);
        $form->url = $this->componentUrl . 'profile/';
        $form->labelWidth = 240;
        $form->items->addItems($items);
        $form->autoScroll = true;

        parent::getInterface();
    }
}
?>