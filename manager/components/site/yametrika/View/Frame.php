<?php
/**
 * Gear Manager
 *
 * Контроллер         "Код фрейма Яндекс.Метрика"
 * Пакет контроллеров "Просмотр Яндекс.Метрика"
 * Группа пакетов     "Яндекс.Метрика"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SYaMetrika_View
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Frame.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Frame');
Gear::library('File');
Gear::library('String');

// отключить вывод ошибок в JSON формате
GException::$toJson = false;

/**
 * Код фрейма Яндекс.Метрика
 * 
 * @category   Gear
 * @package    GController_SYaMetrika_View
 * @subpackage Frame
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Frame.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SYaMetrika_View_Frame extends GController_Frame
{
    /**
     * Вывод плагинов
     * 
     * @param string $resPath каталог ресурсов контроллера
     * @return void
     */
    public function plugins($resPath = '')
    {
        if (empty($this->settings['plugins'])) {
            echo '<em class="alert warning">' . $this->_['no plug-ins to display'] . '</em>';
            return;
        }
        foreach($this->settings['plugins'] as $index => $plugin) {
            $script = $this->path . '/plugins/' . $plugin . '.php';
            if (!file_exists($script)) {
                echo '<em class="alert error">' . $this->_['can not connect plugin'] . ' "', $plugin, '"</em>';
                continue;
            }
            require_once($script);
            $func = 'plugin_' . $plugin;
            if (!function_exists($func)) {
                echo '<em class="alert error">' . $this->_['no entry point to the plugin']. '"', $plugin, '"</em>';
                continue;
            }
            call_user_func($func, $this, $resPath);
        }
    }

    /**
     * Вывод кода фрейма
     * 
     * @param string $resPath каталог ресурсов сайта
     * @return void
     */
    public function frame($resPath = '')
    {
        $st = $this->session->get('user/settings');
        if (isset($st['theme']))
            $theme = $st['theme'];
        else
            $theme = 'default';
?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <link href="/<?=PATH_APPLICATION;?>resources/themes/<?=$theme;?>/manager.css" rel="stylesheet" />
        <link href="/<?=PATH_APPLICATION;?>resources/themes/<?=$theme;?>/default.css" rel="stylesheet" />
        <link href="<?=$resPath;?>/css/style.css" rel="stylesheet" />

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="/<?=PATH_APPLICATION;?>resources/js/vendors/amcharts/amcharts.js" type="text/javascript"></script>
        <script src="/<?=PATH_APPLICATION;?>resources/js/vendors/amcharts/serial.js" type="text/javascript"></script>
        <script src="/<?=PATH_APPLICATION;?>resources/js/vendors/amcharts/pie.js" type="text/javascript"></script>
        <script src="/<?=PATH_APPLICATION;?>resources/js/vendors/amcharts/themes/light.js" type="text/javascript"></script>
        <script src="/<?=PATH_APPLICATION;?>resources/js/vendors/amcharts/lang/ru.js" type="text/javascript"></script>
        <script src="<?=$resPath;?>/js/script.js"></script>
    </head>
    <body>
        <header>
            <h1><span>Я</span>ндекс.Метрика <a href="#" class="icon settings" title="Настройка" component-src="site/yametrika/config/~/profile/interface/"></a></h1>
            <div class="desc">Яндекс.Метрика от компании Яндекс — невидимый счётчик, отчёты обновляются каждые 5 минут. Анализирует рекламный трафик, конверсии, строит интерактивные карты путей пользователей по сайту. Доступны отчёты по полу и возрасту посетителей сайта. Предоставляет бесплатный мониторинг доступности сайта. Все отчёты доступны за произвольный период.</div>
        </header>
        <article>
            <div class="plugin loading" id="counters">Загрузка плагина "Список доступных счетчиков" ...</div>
            <div class="plugin loading" id="summary">Загрузка плагина "Отчет по посещаемости" ...</div>
            <div class="plugin loading" id="geo">Загрузка плагина "Отчет по странам мира" ...</div>
            <div class="plugin loading" id="phrases">Загрузка плагина "Отчет по поисковым фразам" ...</div>
            <div class="plugin loading" id="browsers">Загрузка плагина "Отчет по версии браузера" ...</div>

            <h2>Термины и определения</h2>
            <section class="terms">
                <img class="glyph" src="<?=$resPath;?>/images/icon-faq.png" />
                <strong>Виджет</strong>
                <p>графический модуль, который содержит краткую статистическую информацию о сайте и его посетителях.</p>
                <strong>Визит (сессия)</strong>
                <p>Последовательность действий одного посетителя на сайте (на одном счетчике). Визит завершен, если между действиями посетителя на 
                сайте прошло некоторое время. По умолчанию — 30 минут. Вы можете указать другое время с помощью опции тайм-аут визита.<br />
                Например, тайм-аут равен 30 минутам. Посетитель зашел на сайт и просмотрел несколько страниц — визит засчитан. 
                Затем оставил вкладку браузера открытой и отошел. Через 35 минут вернулся (период тайм-аута истек) и перешел на 
                другую страницу сайта. Этот просмотр считается новым визитом.<br />
                Переход из рекламных систем (например, Яндекс.Директ и др.) считается отдельным визитом. Даже если реклама повторно привела посетителя 
                (или их было несколько с одного компьютера) до того, как закончился тайм-аут визита, каждый клик по объявлению станет отдельным визитом. </p>
                <strong>Внешний переход</strong>
                <p>Переход, совершенный посетителем с вашего сайта на какой-либо другой сайт по внешней ссылке. </p>
                <strong>Внешняя ссылка</strong>
                <p>Ссылка, которая размещена на вашем сайте и ведет на сторонний сайт.</p>
                <strong>Время на сайте</strong>
                <p>Разница по времени между первым и последним событием в визите (к событиям относятся просмотры, переходы по внешним ссылкам, скачивания 
                файлов и достижения целей, включая вызов метода reachGoal).</p>
                <strong>Глубина просмотра</strong>
                <p>Количество просмотров страниц сайта в рамках одного визита. Является отношением общего числа просмотров к общему числу визитов за отчетный период.</p>
                <strong>Группировка</strong>
                <p>Атрибут действий посетителей, зафиксированных счетчиком (например, визит, просмотр, внешний переход, загрузка файла и пр.). По атрибуту группируются данные, переданные в отчет.</p>
                <strong>Достижение цели</strong>
                <p>Момент, в который посетитель совершает действие, являющееся целью.</p>
                <strong>Источник</strong>
                <p>Способ попадания посетителя на сайт: по рекламному объявлению, из результатов поиска, из социальной сети и др.</p>
                <strong>Конверсия</strong>
                <p>Доля целевых визитов в общем числе визитов.</p>
                <strong>Метрика</strong>
                <p>Числовая величина, которая рассчитывается на основе атрибута визита или просмотра. Эта величина может быть общей или средней. Например, просмотры 
                рассчитываются как сумма значений, глубина просмотра — как среднее значение.</p>
                <strong>Отказ</strong>
                <p>Визит считается отказом, если для него одновременно выполнены следующие условия:<br />
                1) за время визита зафиксировано не более одного просмотра страницы;<br />
                2) продолжительность визита меньше заданного для расчета отказов времени (по умолчанию 15 секунд);<br />
                3) не зафиксировано служебного события «неотказ».</p>
                <strong>Посетитель</strong>
                <p>Пользователь, который зашел на сайт в течение определенного промежутка времени. Посетитель считается уникальным, если обладает неповторяющимся 
                набором характеристик (IP-адрес, браузер, ОС, cookies и др.). В случае очистки cookies, переустановки браузера или переустановки операционной 
                системы, посетитель считается новым.</p>
                <strong>Просмотр (хит)</strong>
                <p>Загрузка страницы сайта при переходе посетителя на нее. К просмотрам также относятся перезагрузка страницы, обновление AJAX-сайтов, отправка данных с помощью метода hit.</p>
                <strong>Сегмент</strong>
                <p>Часть визитов или просмотров, выделенная по какому-либо формальному признаку (например, по количеству просмотренных страниц).</p>
                <strong>Счетчик</strong>
                <p>JavaScript-код, с помощью которого сервис собирает статистические данные. Также это учетная единица Яндекс.Метрики — контейнер, в который собирается вся информация о визитах на ваш сайт.</p>
                <strong>Целевой визит</strong>
                <p>Визит, в рамках которого произошло достижение цели.</p>
                <strong>Целевая метрика</strong>
                <p>Числовая величина, которая рассчитывается на основе атрибута целевого визита. Эта величина может быть общей или средней. Например, достижения цели рассчитываются как сумма значений, достижения на посетителя — как среднее значение.</p>
                <strong>Целевой отчет</strong>
                <p>Отображает данные по целевым визитам.</p>
                <strong>Цель</strong>
                <p>Действие посетителя, в котором заинтересован владелец сайта: просмотр конкретной страницы, нажатие на определенный элемент (например, кнопку «Оформление заказа») и т. п.</p>
            </section>
        </article>
    </body>
</html>
<?php
    }
}
?>