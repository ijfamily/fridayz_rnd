<?php
/**
 * Gear Manager
 *
 * Контроллер         "Вывод плагинов"
 * Пакет контроллеров "Яндекс.Метрика"
 * Группа пакетов     "Яндекс.Метрика"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SYaMetrika_View
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Plugin.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Frame');

// отключить вывод ошибок в JSON формате
GException::$toJson = false;

/**
 * Плагин Яндекс.Метрика
 * 
 * @category   Gear
 * @package    GController_SYaMetrika_View
 * @subpackage Plugin
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Plugin.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SYaMetrika_View_Plugin extends GController_Frame
{
    /**
     * Вывод кода фрейма
     * 
     * @param string $resPath каталог ресурсов сайта
     * @return void
     */
    public function frame($resPath = '')
    {
        $name = $this->uri->getVar('name');
        if (empty($name))
            die('<em class="alert warning">' . $this->_['incorrectly stated the name of the plugin'] . '</em>');
        if (empty($this->settings['plugins'])) {
            die('<em class="alert warning">' . $this->_['no plug-ins to display'] . '</em>');
            return;
        }
        $plugins = $this->settings['plugins'];
        if (!in_array($name, $plugins))
            die('<em class="alert warning">' . $this->_['incorrectly stated the name of the plugin'] . '</em>');
        $script = $this->path . '/plugins/' . $name . '.php';
        if (!file_exists($script)) {
            die('<em class="alert error">' . $this->_['can not connect plugin'] . ' "' . $name . '"</em>');
            continue;
        }

        require_once($script);

        $func = 'plugin_' . $name;
        if (!function_exists($func)) {
            die('<em class="alert error">' . $this->_['no entry point to the plugin']. '"' . $name . '"</em>');
        }
        call_user_func($func, $this, $resPath);

    }

    /**
     * Инициализация запроса
     * 
     * @return void
     */
    protected function init()
    {
        // метод запроса
        switch ($this->uri->method) {
            // метод "GET"
            case 'GET':
                // тип действия
                switch ($this->uri->action) {
                    // возращает интерфейс контроллера
                    case 'plugin':
                        $this->frame('/' . PATH_APPLICATION . $this->resourcePath);
                        return;
                }
                break;
        }

        parent::init();
    }
}
?>