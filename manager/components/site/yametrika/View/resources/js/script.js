var chartData = [];

function loadPlugin(name){
    var chart;
    $('#' + name + ' section').html('<div class="loading">Идёт обновление плагина, пожалуйста подождите ...</div>');
    $.ajax({
        url: '../plugin/?name=' + name,
        cache: false,
        success: function(data, status, xhr){
            $('#' + name).removeClass('loading').html(data);
            if (typeof chartData[name] != 'undefined')
                chart = AmCharts.makeChart('chart-' + name, chartData[name]);
        }
    });
    return false;
}

jQuery(document).ready(function($){
    $('a').click(function(){
        var src = $(this).attr('component-src');
        if (typeof src != 'undefined' && window.top.Site != 'undefined') {
            window.top.Site.load(src);
            return false;
        }
    });

    function loadPlugins(){
        var chart, plgName = plugins[indexPlg];
        $.ajax({
            url: '../plugin/?name=' + plgName,
            cache: false,
            success: function(data, status, xhr){
                $('#' + plgName).removeClass('loading').html(data);
                if (typeof chartData[plgName] != 'undefined')
                    chart = AmCharts.makeChart('chart-' + plgName, chartData[plgName]);
                if (plugins.length - 1 > indexPlg) {
                    indexPlg++
                    loadPlugins();
                }
            }
        });
    }

    var plugins = [], indexPlg = 0;
    $('.plugin').each(function(item, index) { plugins.push($(this).attr('id')); });

    loadPlugins();
});