<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_view'   => 'Яндекс.Метрика',
    'tooltip_view' => 'от компании Яндекс — невидимый счётчик, отчёты обновляются каждые 5 минут. Анализирует рекламный трафик, конверсии, строит интерактивные карты путей пользователей по сайту.',
    // сообщения
    'no plug-ins to display' => 'нет плагинов для отображения',
    'can not connect plugin' => 'невозможно подключить плагин',
    'no entry point to the plugin' => 'нет точки входа в плагин',
    'incorrectly stated the name of the plugin' => 'неправильно указано имя плагина',
);
?>