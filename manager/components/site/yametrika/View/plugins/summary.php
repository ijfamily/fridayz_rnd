<?php
/**
 * Gear Manager
 *
 * Плагин             "Отчет по посещаемости"
 * Пакет контроллеров "Плагин Яндекс.Метрика"
 * Группа пакетов     "Яндекс.Метрика"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Summary
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: summary.php 2016-01-01 12:00:00 Gear Magic $
 */



/**
 * Плагин вывода отчета по посещаемости
 * 
 * @param resource $frame указатель на класс контроллера фрейма
 * @param string $resPath каталог ресурсов контроллера
 * @return void
 */
function plugin_summary($frame, $resPath = '')
{
    $date = YaMetrika::getRangeDate($frame->config->getFromCms('YaMetrika', 'range.from'), $frame->config->getFromCms('YaMetrika', 'range.to'));
    $result = YaMetrika::getStatSummary(
        $frame->config->getFromCms('YaMetrika', 'counter.id'),
        $frame->config->getFromCms('YaMetrika', 'token'),
        $date['from'],
        $date['to']
    );
?>
<h2>Отчет по посещаемости <span>(c <?=date('d-m-Y', strtotime($date['from']));?> по <?=date('d-m-Y', strtotime($date['to']));?>)</span></h2>
<section>
    <div class="icons">
        <img src="<?=$resPath;?>/images/icon-summary.png" />
        <a href="#" class="icon refresh" title="Обновить" onclick="return loadPlugin('summary');"></a>
    </div>
    <?php if ($result === false) : ?>
    <em class="alert warning">невозможно получить доступ к настройкам метрики (неправильно сформирован запрос) или сервис не отвечает!</em>
    <?php else : ?>
    <table class="grid">
        <tr><td colspan="8"><div id="chart-summary" style="height: 300px; width: 100%;"></div></td></tr>
        <tr>
            <th>Дата</th>
            <th>Визиты</th>
            <th>Просмотры</th>
            <th>Посетители</th>
            <th>Новые посетители</th>
            <th>Отказы</th>
            <th>Глубина просмотра</th>
            <th>Среднее время в секундах,<br>проведенное на сайте посетителями</th>
        </tr>
<?php
$dataProvider = array();
foreach ($result['data'] as $cnt) :
    $date = substr_replace($cnt['date'], '-', 4, 0);
    $date = substr_replace($date, '-', 7, 0);
    $dataProvider[] = array('date' => $date, 'value' => $cnt['visits']);
?>
        <tr>
            <td><?=$date;?></td>
            <td><?=$cnt['visits'];?></td>
            <td><?=$cnt['page_views'];?></td>
            <td><?=$cnt['visitors'];?></td>
            <td><?=$cnt['new_visitors'];?></td>
            <td><?=$cnt['denial'];?></td>
            <td><?=$cnt['depth'];?></td>
            <td><?=$cnt['visit_time'];?></td>
        </tr>
<?php endforeach; ?>
        <tr>
            <th>Всего:</th>
            <th><?=$result['totals']['visits'];?></th>
            <th><?=$result['totals']['page_views'];?></th>
            <th><?=$result['totals']['visitors'];?></th>
            <th><?=$result['totals']['new_visitors'];?></th>
            <th><?=$result['totals']['denial'];?></th>
            <th><?=$result['totals']['depth'];?>а</th>
            <th><?=$result['totals']['visit_time'];?></th>
        </tr>
    </table>
    <script type="text/javascript">
        chartData['summary'] = {
            "type": "serial",
            "theme": "light",
            "language": "ru",
            "dataDateFormat": "YYYY-MM-DD",
            "dataProvider": <?=json_encode($dataProvider);?>,
            "graphs": [{
                "bullet": "round",
                "dashLength": 4,
                "valueField": "value"
            }],
            "chartCursor": { "cursorAlpha": 0, "zoomable":false, "valueZoomable":true },
            "categoryAxis": { "parseDates": true, "dashLength": 1, "minorGridEnabled": true },
            "categoryField": "date",
            "valueScrollbar": { }
        };
    </script>
    <?php endif; ?>
</section>
<?php
}
?>