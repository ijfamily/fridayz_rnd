<?php
/**
 * Gear Manager
 *
 * Плагин             "Отчет по версиям браузера"
 * Пакет контроллеров "Плагин Яндекс.Метрика"
 * Группа пакетов     "Яндекс.Метрика"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Browsers
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: browsers.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Плагин вывода отчетов по версии браузеру
 * 
 * @param resource $frame указатель на класс контроллера фрейма
 * @param string $resPath каталог ресурсов контроллера
 * @return void
 */
function plugin_browsers($frame, $resPath = '')
{
    $date = YaMetrika::getRangeDate($frame->config->getFromCms('YaMetrika', 'range.from'), $frame->config->getFromCms('YaMetrika', 'range.to'));
    $result = YaMetrika::getStatBrowsers(
        $frame->config->getFromCms('YaMetrika', 'counter.id'),
        $frame->config->getFromCms('YaMetrika', 'token'),
        $date['from'],
        $date['to']
    );
?>
<h2>Отчет по версиям браузера <span>(c <?=date('d-m-Y', strtotime($date['from']));?> по <?=date('d-m-Y', strtotime($date['to']));?>)</span></h2>
<section>
    <div class="icons">
        <img src="<?=$resPath;?>/images/icon-browsers.png" />
        <a href="#" class="icon refresh" title="Обновить" onclick="return loadPlugin('browsers');"></a>
    </div>
    <?php if ($result === false) : ?>
    <em class="alert warning">невозможно получить доступ к настройкам метрики (неправильно сформирован запрос) или сервис не отвечает!</em>
    <?php else : ?>
    <table class="grid">
        <tr><td colspan="7"><div id="chart-browsers" style="height: 450px; width: 100%;"></div></td></tr>
        <tr>
            <th>Название</th>
            <th>Версия</th>
            <th>Среднее время в сек.,<br>проведенное на сайте посетителями</th>
            <th>Просмотры</th>
            <th>Глубина просмотра</th>
            <th>Визиты</th>
            <th>Отказы</th>
        </tr>
<?php
$dataProvider = array();
foreach ($result['data'] as $cnt) :
    $dataProvider[] = array('name' => $cnt['name'] . ' ' . $cnt['version'], 'visit' => $cnt['visits']);
?>
        <tr>
            <td><?=$cnt['name'];?></td>
            <td><?=$cnt['version'];?></td>
            <td><?=$cnt['visit_time'];?></td>
            <td><?=$cnt['page_views'];?></td>
            <td><?=$cnt['depth'];?></td>
            <td><?=$cnt['visits'];?></td>
            <td><?=$cnt['denial'];?></td>
        </tr>
<?php endforeach; ?>
    </table>
    <script type="text/javascript">
        chartData['browsers'] = {
            "type": "pie",
            "theme": "light",
            "language": "ru",
            "dataProvider": <?=json_encode($dataProvider);?>,
                "titleField": "name",
                "valueField": "visit",
                "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
                "legend": {
                    "align": "center",
                    "markerType": "circle"
                }
        };
    </script>
    <?php endif; ?>
</section>
<?php
}
?>