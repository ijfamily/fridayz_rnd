<?php
/**
 * Gear Manager
 *
 * Плагин             "Отчет по поисковым фразам"
 * Пакет контроллеров "Плагин Яндекс.Метрика"
 * Группа пакетов     "Яндекс.Метрика"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Phrases
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: phrases.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Плагин вывода отчета по поисковым фразам
 * 
 * @param resource $frame указатель на класс контроллера фрейма
 * @param string $resPath каталог ресурсов контроллера
 * @return void
 */
function plugin_phrases($frame, $resPath = '')
{
    $date = YaMetrika::getRangeDate($frame->config->getFromCms('YaMetrika', 'range.from'), $frame->config->getFromCms('YaMetrika', 'range.to'));
    $result = YaMetrika::getStatPhrases(
        $frame->config->getFromCms('YaMetrika', 'counter.id'),
        $frame->config->getFromCms('YaMetrika', 'token'),
        $date['from'],
        $date['to']
    );
?>
<h2>Отчет по поисковым фразам <span>(c <?=date('d-m-Y', strtotime($date['from']));?> по <?=date('d-m-Y', strtotime($date['to']));?>)</span></h2>
<section>
    <div class="icons">
        <img src="<?=$resPath;?>/images/icon-phrases.png" />
        <a href="#" class="icon refresh" title="Обновить" onclick="return loadPlugin('phrases');"></a>
    </div>
    <?php if ($result === false) : ?>
    <em class="alert warning">невозможно получить доступ к настройкам метрики (неправильно сформирован запрос) или сервис не отвечает!</em>
    <?php else : ?>
    <table class="grid">
        <tr>
            <th>Фраза</th>
            <th>Среднее время в сек.,<br>проведенное на сайте посетителями</th>
            <th>Просмотры</th>
            <th>Глубина просмотра</th>
            <th>Визиты</th>
            <th>Отказы</th>
            <th>Ссылка на страницу выдачи,<br>с которой был переход</th>
        </tr>
<?php foreach ($result['data'] as $cnt) : ?>
        <tr>
            <td><?=$cnt['phrase'];?></td>
            <td><?=$cnt['visit_time'];?></td>
            <td><?=$cnt['page_views'];?></td>
            <td><?=$cnt['depth'];?></td>
            <td><?=$cnt['visits'];?></td>
            <td><?=$cnt['denial'];?></td>
            <td><?=isset($cnt['search_engines']) ? $cnt['search_engines'][0]['se_url'] : 'неизвестно';?></td>
        </tr>
<?php endforeach; ?>
    </table>
    <?php endif; ?>
</section>
<?php
}
?>