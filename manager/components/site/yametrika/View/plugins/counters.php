<?php
/**
 * Gear Manager
 *
 * Плагин             "Плагин вывода cписка всех доступных счётчиков"
 * Пакет контроллеров "Плагин Яндекс.Метрика"
 * Группа пакетов     "Яндекс.Метрика"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Counters
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: counters.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Плагин вывода cписка всех доступных счётчиков
 * 
 * @param resource $frame указатель на класс контроллера фрейма
 * @param string $resPath каталог ресурсов контроллера
 * @return void
 */
function plugin_counters($frame, $resPath = '')
{
    $counterId = $frame->config->getFromCms('YaMetrika', 'counter.id');
    $token = $frame->config->getFromCms('YaMetrika', 'token');
?>
<?php
$counters = YaMetrikaCounter::getCounters($token);
if ($counters) : ?>
<h2>Список доступных счетчиков</h2>
<div class="desc">список всех доступных счётчиков для логина владельца счетчика из Яндекс.Паспорта.</div>
<section>
    <div class="icons">
        <img src="<?=$resPath;?>/images/icon-counters.png" />
        <a href="#" class="icon refresh" title="Обновить" onclick="return loadPlugin('counters');"></a>
    </div>
    <table class="grid" style="width: auto;">
        <tr>
            <th>Идентификатор<br>счетчика</th>
            <th>Логин владельца счетчика<br>из Яндекс.Паспорта.</th>
            <th>Статус установки<br>кода счетчика</th>
            <th>Название счетчика</th>
            <th>Тип счетчика</th>
            <th>Уровень доступа<br>к счетчику</th>
            <th>Дата создания</th>
            <th>Полный домен сайта</th>
        </tr>
<?php
foreach ($counters as $cnt) :
    $cnt['code_status'] = YaMetrikaCounter::get($cnt['code_status'], 'statuses');
    $cnt['permission'] = YaMetrikaCounter::get($cnt['permission'], 'permissions');
    $cnt['type'] = YaMetrikaCounter::get($cnt['type'], 'types');
    $cnt['create_time'] = date('d-m-Y H:i:s', strtotime($cnt['create_time']));
?>
        <tr>
            <td><?=$counterId == $cnt['id'] ? '<img src="' . $resPath . '/images/icon-counter.png" align="absmiddle"> ' : '';?> <?=$cnt['id'];?></td>
            <td><?=$cnt['owner_login'];?></td>
            <td><?=$cnt['code_status'];?></td>
            <td><?=$cnt['name'];?></td>
            <td><?=$cnt['type'];?></td>
            <td><?=$cnt['permission'];?></td>
            <td><?=$cnt['create_time'];?></td>
            <td><?=$cnt['site'];?></td>
        </tr>
        <tr>
            <td colspan="4" valign="top">
                настройки счетчика:
                <ul>
                    <li>Асинхронный код счетчика: <? echo $cnt['code_options']['async'] ? '<em class="alert success">включено</em>' : '<em class="alert error">отключено</em>';?></li>
                    <li>Запись и анализ поведения посетителей сайта: <? echo $cnt['code_options']['visor'] ? '<em class="alert success">включено</em>' : '<em class="alert error">отключено</em>';?></li>
                    <li>Запрет отправки на индексацию страниц сайта: <? echo $cnt['code_options']['ut'] ? '<em class="alert success">включено</em>' : '<em class="alert error">отключено</em>';?></li>
                    <li>Отслеживание хеша в адресной строке браузера. Опция применима для AJAX-сайтов: <? echo $cnt['code_options']['track_hash'] ? '<em class="alert success">включено</em>' : '<em class="alert error">отключено</em>';?></li>
                    <li>Сбор статистики для работы отчета Карта кликов: <? echo $cnt['code_options']['clickmap'] ? '<em class="alert success">включено</em>' : '<em class="alert error">отключено</em>';?></li>
                </ul>
            </td>
            <td colspan="4" valign="top">
                настройки информера:
                <ul>
                    <li>Разрешение отображения информера: <? echo $cnt['code_options']['informer']['enabled'] ? '<em class="alert success">включено</em>' : '<em class="alert error">отключено</em>';?></li>
                    <li>Тип информера: <?=YaMetrikaInformer::get($cnt['code_options']['informer']['type'], 'types');?></li>
                    <li>Тип размера информера: <?=YaMetrikaInformer::get($cnt['code_options']['informer']['size'], 'sizes');?></li>
                    <li>Показатель, который будет отображаться на информере: <?=YaMetrikaInformer::get($cnt['code_options']['informer']['indicator'], 'indicators');?></li>
                    <li>Начальный (верхний) цвет информера в формате RRGGBBAA: <?=$cnt['code_options']['informer']['color_start'];?></li>
                    <li>Конечный (нижний) цвет информера в формате RRGGBBAA: <?=$cnt['code_options']['informer']['color_end'];?></li>
                    <li>Цвет текста на информере: <?=$cnt['code_options']['informer']['color_text'];?></li>
                    <li>Цвет стрелки на информере: <?=$cnt['code_options']['informer']['color_arrow'];?></li>
                </ul>
            </td>
        </tr>
<?php endforeach;?>
    </table>
</section>
<?php else : ?>
Нет счётчиков, Вам необходимо перейти по ссылке: https://oauth.yandex.ru/authorize?response_type=code&client_id=<?=$clientId?>
<?php return; endif;
}
?>