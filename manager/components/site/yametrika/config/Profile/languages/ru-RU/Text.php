<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_update' => 'Настройка Яндекс.Метрика',
    'text_btn_help'        => 'Справка',
    // поля
    'html_desc'               => '<header>Настройка компонента <span style="color:#ed1c24">Я</span>ндекс.Метрика</header>'
                               . '<note>Для настройки компонента метрики Яндекс необходимо заполнить следующие поля:</note>',
    'title_fs_account'    => 'Акаунт приложения',
    'lable_client_id'     => 'ID',
    'lable_client_secret' => 'Ключ',
    'label_token'         => 'Токен',
    'label_counter'       => 'ID счётчика',
    'title_fs_range'      => 'Просмотр за период',
    'label_range_from'    => 'От даты',
    'label_range_to'      => 'До даты',
    // сообщения
    'msg_file_exist' => 'Невозможно открыть файл настроек "%s"',
);
?>