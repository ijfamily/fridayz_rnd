<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля настроеек Яндекс.Метрика"
 * Пакет контроллеров "Настройки Яндекс.Метрика"
 * Группа пакетов     "Яндекс.Метрика"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SYaMetrikaConfig_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля настроеек Яндекс.Метрика
 * 
 * @category   Gear
 * @package    GController_SYaMetrikaConfig_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SYaMetrikaConfig_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // Ext_Window (ExtJS class 'Manager.window.DataProfile')
        $this->_cmp->setProps(
            array('title'           => $this->_['title_profile_update'],
                  'titleEllipsis'   => 40,
                  'width'           => 500,
                  'autoHeight'      => true,
                  'resizable'       => false,
                  'btnDeleteHidden' => true,
                  'stateful'        => false,
                  'buttonsAdd'      => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-site-yamatrika-config',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );
        $this->_cmp->state = 'update';

        $items = array(
            array('xtype' => 'label', 'html' => $this->_['html_desc']),
            array('xtype'      => 'fieldset',
                  'labelWidth' => 70,
                  'title'      => $this->_['title_fs_account'],
                  'items'      => array(
                      array('xtype'      => 'textfield',
                            'fieldLabel' => $this->_['lable_client_id'],
                            'anchor'      => '100%',
                            'checkDirty' => false,
                            'name'       => 'settings[client.id]'),
                      array('xtype'      => 'textfield',
                            'fieldLabel' => $this->_['lable_client_secret'],
                            'anchor'      => '100%',
                            'checkDirty' => false,
                            'name'       => 'settings[client.secret]'),
                  )
            ),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_token'],
                  'anchor'     => '100%',
                  'checkDirty' => false,
                  'name'       => 'settings[token]'),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_counter'],
                  'width'      => 200,
                  'checkDirty' => false,
                  'name'       => 'settings[counter.id]'),
            array('xtype'      => 'fieldset',
                  'anchor'     => '100%',
                  'title'      => $this->_['title_fs_range'],
                  'autoHeight' => true,
                  'layout'     => 'column',
                  'cls'        => 'mn-container-clean',
                  'items'      => array(
                      array('layout'     => 'form',
                            'width'      => 185,
                            'labelWidth' => 70,
                            'items'      => array(
                                array('xtype'      => 'datefield',
                                      'fieldLabel' => $this->_['label_range_from'],
                                      'format'     => 'd-m-Y',
                                      'name'       => 'settings[range.from]',
                                      'checkDirty' => false,
                                      'width'      => 95,
                                      'allowBlank' => true)
                            )
                      ),
                      array('layout'     => 'form',
                            'labelWidth' => 70,
                            'items'      => array(
                                array('xtype'      => 'datefield',
                                      'fieldLabel' => $this->_['label_range_to'],
                                      'format'     => 'd-m-Y',
                                      'name'       => 'settings[range.to]',
                                      'checkDirty' => false,
                                      'width'      => 95,
                                      'allowBlank' => true)
                            )
                      )
                  )
            ),
        );


        // форма (ExtJS class 'Manager.form.DataProfile')
        $form = $this->_cmp->items->get(0);
        $form->url = $this->componentUrl . 'profile/';
        $form->labelWidth = 80;
        $form->items->addItems($items);
        $form->autoScroll = true;

        parent::getInterface();
    }
}
?>