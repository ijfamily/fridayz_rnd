<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Настройка альбома изображений"
 * Группа пакетов     "Альбомы изображений"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SAlbumsConfig
 * @copyright  Copyright (c) 2013-2016 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Настройка альбома изображений 
 * Описание: Настройка альбома изображений 
 * Меню: Настройка альбома изображений
 * ID класса: gcontroller_salbumsconfig_profile
 * Модуль: Сайт
 * Группа: Сайт/ Настройки
 * Очищать: нет
 * Статистика компонента: нет 
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске компонентов: нет
 *    В главном меню: нет
 * Ресурс
 *    Компонент: site/albums/config/
 *    Контроллер: site/albums/config/Profile/
 *    Интерфейс: profile/interface/
 *    Меню: profile/interface/
 */

return array(
    // {S}- модуль "Site" {Albums} - пакет контроллеров "Albums config" -> SAlbumsConfig
    'clsPrefix' => 'SYaMetrikaConfig',
    // использовать язык
    'language'  => 'ru-RU'
);
?>