<?php
 //https://tech.yandex.ru/metrika/doc/ref/stat/traffic-summary-docpage/

class YaMetrikaInformer
{
    public static $types = array(
        'simple' => 'простой',
        'ext' => 'расширенный',
    );

    public static $sizes = array(
        1 => '80х15',
        2 => '80х31',
        3 => '88х31'
    );

    public static $indicators = array(
        'pageviews' => 'просмотры ',
        'visits' => 'визиты',
        'uniques' => 'посетители',
    );

    public static function get($value, $name)
    {
        $arr = self::$$name;
        if (isset($arr[$value]))
            return $arr[$value];

        return $value;
    }
}

class YaMetrikaCounter
{
    public static $types = array(
        'simple' => 'счетчик создан пользователем в Метрике',
        'partner ' => 'счетчик импортирован из РСЯ'
    );

    public static $permissions = array(
        'own'  => 'собственный счетчик пользователя',
        'view' => 'гостевой счетчик с уровнем доступа «только просмотр»',
        'edit' => 'гостевой счетчик с уровнем доступа «полный доступ»'
    );

    public static $statuses = array(
        'CS_ERR_CONNECT' => 'Не удалось проверить (ошибка соединения)',
        'CS_ERR_DUPLICATED' => 'Установлен более одного раза',
        'CS_ERR_HTML_CODE' => 'Установлен некорректно',
        'CS_ERR_OTHER_HTML_CODE' => 'Уже установлен другой счетчик',
        'CS_ERR_TIMEOUT' => 'Не удалось проверить (превышено время ожидания)',
        'CS_ERR_UNKNOWN' => 'Неизвестная ошибка',
        'CS_NEW_COUNTER' => 'Недавно создан',
        'CS_NA' => 'Не применим к данному счетчику',
        'CS_NOT_EVERYWHERE' => 'Установлен не на всех страницах',
        'CS_NOT_FOUND' => 'Не установлен',
        'CS_NOT_FOUND_HOME' => 'Не установлен на главной странице',
        'CS_NOT_FOUND_HOME_LOAD_DATA' => 'Не установлен на главной странице, но данные поступают',
        'CS_OBSOLETE' => 'Установлена устаревшая версия кода счетчика',
        'CS_OK' => 'Корректно установлен',
        'CS_OK_NO_DATA' => 'Установлен, но данные не поступают',
        'CS_WAIT_FOR_CHECKING' => 'Ожидает проверки наличия',
        'CS_WAIT_FOR_CHECKING_LOAD_DATA' => 'Ожидает проверки наличия, данные поступают'
    );

    public static function get($value, $name)
    {
        $arr = self::$$name;
        if (isset($arr[$value]))
            return $arr[$value];

        return $value;
    }

    public static function getCounters($token)
    {
        $result = @file_get_contents('https://api-metrika.yandex.ru/management/v1/counters?oauth_token=' . $token);
        if ($result === false) return false;

        $result = json_decode($result, true);

        return $result['counters'];
    }
}

class YaMetrika
{
    public static function getRangeDate($from = '', $to = '')
    {
        if (empty($from) && empty($to)) {
            $to = date('Y-m-d');
            $from = date('Y-m-d', strtotime('-7 days'));
        } else
        if (empty($from)) {
            $tou = strtotime($to);
            $fromu = mktime(0, 0, 0, date('m', $tou), date('d', $tou) - 7, date('Y', $tou));
            $to = date('Y-m-d', $tou);
            $from = date('Y-m-d', $fromu);
        } else
        if (empty($to)) {
            $to = date('Y-m-d');
            $from = date('Y-m-d', strtotime($from));
        }

        return array('to' => $to, 'from' => $from);
    }

    public static function getStatGeo($counterId, $token, $dateFrom, $dateTo)
    {
        $url = 'https://api-metrika.yandex.ru/stat/geo';
        $query = array(
          'id'          => $counterId,
          'date1'       => $dateFrom,
          'date2'       => $dateTo,
          'oauth_token' => $token,
          'pretty'      => 1
        );
        $query = urldecode(http_build_query($query));
        $result = file_get_contents($url . '.json?' . $query);

        if ($result === false) return false;

        $result = json_decode($result, true);
        $result['urlJson'] = $url . '.json?' . $query;
        $result['urlXml'] = $url . '.xml?' . $query;

        return $result;
    }

    public static function getStatPhrases($counterId, $token, $dateFrom, $dateTo)
    {
        $url = 'https://api-metrika.yandex.ru/stat/phrases';
        $query = array(
          'id'          => $counterId,
          'date1'       => $dateFrom,
          'date2'       => $dateTo,
          'oauth_token' => $token,
          'pretty'      => 1
        );
        $query = urldecode(http_build_query($query));
        $result = file_get_contents($url . '.json?' . $query);

        if ($result === false) return false;

        $result = json_decode($result, true);
        $result['urlJson'] = $url . '.json?' . $query;
        $result['urlXml'] = $url . '.xml?' . $query;

        return $result;
    }

    public static function getStatBrowsers($counterId, $token, $dateFrom, $dateTo)
    {
        $url = 'https://api-metrika.yandex.ru/stat/browsers';
        $query = array(
          'id'          => $counterId,
          'date1'       => $dateFrom,
          'date2'       => $dateTo,
          'oauth_token' => $token,
          'pretty'      => 1,
          'per_page'    => 20
        );
        $query = urldecode(http_build_query($query));
        $result = file_get_contents($url . '.json?' . $query);

        if ($result === false) return false;

        $result = json_decode($result, true);
        $result['urlJson'] = $url . '.json?' . $query;
        $result['urlXml'] = $url . '.xml?' . $query;

        return $result;
    }


    public static function getStatSummary($counterId, $token, $dateFrom, $dateTo)
    {
        $url = 'https://api-metrika.yandex.ru/stat/traffic/summary';
        $query = array(
          'id'          => $counterId,
          'date1'       => $dateFrom,
          'date2'       => $dateTo,
          'oauth_token' => $token,
          'pretty'      => 1,
          'per_page'    => 20
        );
        $query = urldecode(http_build_query($query));
        $result = file_get_contents($url . '.json?' . $query);

        if ($result === false) return false;

        $result = json_decode($result, true);
        $result['urlJson'] = $url . '.json?' . $query;
        $result['urlXml'] = $url . '.xml?' . $query;

        return $result;
    }

    public static function getToken($clientId, $clientSecret, $code)
    {
        // Формирование параметров (тела) POST-запроса с указанием кода подтверждения
        $query = array(
          'grant_type'    => 'authorization_code',
          'code'          => $code,
          'client_id'     => $clientId,
          'client_secret' => $clientSecret
        );
        $query = http_build_query($query);
    
        // Формирование заголовков POST-запроса
        $header = "Content-type: application/x-www-form-urlencoded";
    
        // Выполнение POST-запроса и вывод результата
        $opts = array(
            'http' => array(
                'method'  => 'POST',
                'header'  => $header,
                'content' => $query
            )
        );
        $context = stream_context_create($opts);
        $result = @file_get_contents('https://oauth.yandex.ru/token', false, $context);
        if ($result === false) return false;

        $result = json_decode($result);

        return $result['counters'];
    }
}
?>