<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Яндекс.Метрика"
 * Группа пакетов     "Яндекс.Метрика"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SYaMetrika
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Яндекс.Метрика
 * Описание: Яндекс.Метрика
 * Меню: Яндекс.Метрика
 * ID класса: gcontroller_syametrika_view
 * Модуль: Сайт
 * Группа: Сайт
 * Очищать: нет
 * Статистика компонента: нет
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске компонентов: нет
 *    В главном меню: нет
 * Ресурс
 *    Компонент: site/yametrika/
 *    Контроллер: site/yametrika/View/
 *    Интерфейс: view/interface/
 *    Меню:
 */

return array(
    // {S}- модуль "Site" {} - пакет контроллеров "Yandex Metrika" -> SYaMetrika
    'clsPrefix' => 'SYaMetrika',
    // использовать язык
    'language'  => 'ru-RU',
    // автозагрузка
    'autoload'  => array('YaMetrika'),
    // плагины
    'plugins'   => array('counters', 'summary', 'geo', 'phrases', 'browsers')
);
?>