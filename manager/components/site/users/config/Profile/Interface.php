<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс настройки пользователей сайта"
 * Пакет контроллеров "Настройки пользователей сайтаS"
 * Группа пакетов     "Пользователи сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SUsersConfig
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс настройки пользователей сайта
 * 
 * @category   Gear
 * @package    GController_SUsersConfig
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SUsersConfig_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // Ext_Window (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'           => $this->_['title_profile_update'],
                  'titleEllipsis'   => 40,
                  'width'           => 600,
                  'height'          => 500,
                  'resizable'       => false,
                  'btnDeleteHidden' => true,
                  'stateful'        => false,
                  'buttonsAdd'      => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-site-users-config',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );
        $this->_cmp->state = 'update';

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->url = $this->componentUrl . 'profile/';
        $form->autoScroll = true;
        $form->labelWidth = 280;

        $form->items->addItems(array(
            array('xtype' => 'label', 'html' => $this->_['html_desc']),
            array('xtype'      => 'checkbox',
                  'fieldLabel' => $this->_['label_restore'],
                  'checkDirty' => false,
                  'name'       => 'settings[RESTORE]'),
            array('xtype' => 'label', 'html' => $this->_['info_restore']),
            array('xtype'      => 'checkbox',
                  'fieldLabel' => $this->_['label_signin_access'],
                  'checkDirty' => false,
                  'name'       => 'settings[SIGNIN/ACCESS]'),
            array('xtype' => 'label', 'html' => $this->_['info_signin_access']),
            array('xtype'         => 'combo',
                  'fieldLabel'    => $this->_['label_signin_type'],
                  'name'          => 'settings[SIGNIN/TYPE]',
                  'checkDirty'    => false,
                  'editable'      => false,
                  'width'         => 170,
                  'typeAhead'     => false,
                  'triggerAction' => 'all',
                  'mode'          => 'local',
                  'value'         => 'login',
                  'store'         => array(
                      'xtype'  => 'arraystore',
                      'fields' => array('value', 'list'),
                      'data'   => $this->_['data_signtype']
                  ),
                  'hiddenName'    => 'settings[SIGNIN/TYPE]',
                  'valueField'    => 'value',
                  'displayField'  => 'list',
                  'allowBlank'    => true),
            array('xtype' => 'label', 'html' => $this->_['info_signin_type']),
            array('xtype'      => 'checkbox',
                  'fieldLabel' => $this->_['label_signup_confirm'],
                  'checkDirty' => false,
                  'name'       => 'settings[SIGNUP/CONFIRM]'),
            array('xtype' => 'label', 'html' => $this->_['info_signup_confirm']),
            array('xtype'      => 'checkbox',
                  'fieldLabel' => $this->_['label_signup_not'],
                  'checkDirty' => false,
                  'name'       => 'settings[SIGNUP/NOTIFICATION]'),
            array('xtype' => 'label', 'html' => $this->_['info_signup_not']),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_social_adapter'],
                  'checkDirty' => false,
                  'width'      => 170,
                  'allowBlank' => true,
                  'name'       => 'settings[SOCIAL/ADAPTER]'),
            array('xtype' => 'label', 'html' => $this->_['info_social_adapter']),
            array('xtype'      => 'checkbox',
                  'fieldLabel' => $this->_['label_social'],
                  'checkDirty' => false,
                  'name'       => 'settings[SIGNIN/SOCIAL]'),
            array('xtype' => 'label', 'html' => $this->_['info_social']),
            array('xtype'      => 'checkbox',
                  'fieldLabel' => $this->_['label_one_ip'],
                  'checkDirty' => false,
                  'name'       => 'settings[SIGNUP/ONE-IP]'),
            array('xtype' => 'label', 'html' => $this->_['info_one_ip']),
            array('xtype'      => 'checkbox',
                  'fieldLabel' => $this->_['label_signin_rules'],
                  'checkDirty' => false,
                  'name'       => 'settings[SIGNUP/RULES]'),
            array('xtype' => 'label', 'html' => $this->_['info_signin_rules']),
            array('xtype'      => 'checkbox',
                  'fieldLabel' => $this->_['label_signin_captcha'],
                  'checkDirty' => false,
                  'name'       => 'settings[SIGNIN/CAPTCHA]'),
            array('xtype' => 'label', 'html' => $this->_['info_signin_captcha']),
            array('xtype'      => 'checkbox',
                  'fieldLabel' => $this->_['label_signup_captcha'],
                  'checkDirty' => false,
                  'name'       => 'settings[SIGNUP/CAPTCHA]'),
            array('xtype' => 'label', 'html' => $this->_['info_signup_captcha']),
            array('xtype'      => 'numberfield',
                  'fieldLabel' => $this->_['label_signin_max'],
                  'checkDirty' => false,
                  'value'      => 0,
                  'width'      => 50,
                  'name'       => 'settings[SIGNUP/MAX]'),
            array('xtype' => 'label', 'html' => $this->_['info_signin_max']),


        ));

        parent::getInterface();
    }
}
?>