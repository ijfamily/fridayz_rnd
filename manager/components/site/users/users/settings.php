<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Пользователи сайта"
 * Группа пакетов     "Пользователи сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SUsers
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Пользователи сайта
 * Описание: Пользователи сайта
 * Меню: Пользователь сайта
 * ID класса: gcontroller_susers_grid
 * Модуль: Сайт
 * Группа: Сайт / Пользователи
 * Очищать: нет
 * Статистика компонента: нет 
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске компонентов: нет
 *    В главном меню: нет
 * Ресурс
 *    Компонент: site/users/users/
 *    Контроллер: site/users/users/Grid/
 *    Интерфейс: grid/interface/
 *    Меню: profile/interface/
 */

return array(
    // {S}- модуль "Site" {Users} - пакет контроллеров "Users" -> SUsers
    'clsPrefix' => 'SUsers',
    // использовать язык
    'language'  => 'ru-RU'
);
?>