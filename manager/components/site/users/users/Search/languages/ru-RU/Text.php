<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Поиск в списке "Пользователи сайта"',
    // поля
    'header_user_account_date'   => 'Зарегистрирован',
    'header_contact_address'     => 'Адрес',
    'header_contact_phone'       => 'Телефон',
    'header_contact_phone_a'     => 'Телефон (д)',
    'header_contact_email'       => 'E-mail',
    'header_user_account_ip'     => 'IP адрес',
    'header_user_confirm_date'   => 'Подтверждён',
    'header_user_confirm_code'   => 'Код подтверждения',
    'header_user_recovery_date'  => 'Восстановлен',
    'header_user_recovery_code'  => 'Код восстановления'
);
?>