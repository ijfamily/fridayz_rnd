<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск пользователей"
 * Пакет контроллеров "Поиск пользователей"
 * Группа пакетов     "Пользователи сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SUsers_Search
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data .php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск пользователей
 * 
 * @category   Gear
 * @package    GController_SUsers_Search
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data .php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SUsers_Search_Data extends GController_Search_Data
{
    /**
     * дентификатор компонента (списка) к которому применяется поиск
     *
     * @var string
     */
    public $gridId = 'gcontroller_susers_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_susers_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор,
     * используется для инициализации атрибутов контроллера без конструктора
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'user_account_date', 'label' => $this->_['header_user_account_date']),
            array('dataIndex' => 'contact_address', 'label' => $this->_['header_contact_address']),
            array('dataIndex' => 'contact_phone', 'label' => $this->_['header_contact_phone']),
            array('dataIndex' => 'contact_phone_a', 'label' => $this->_['header_contact_phone_a']),
            array('dataIndex' => 'contact_email', 'label' => $this->_['header_contact_email']),
            array('dataIndex' => 'account_ip', 'label' => $this->_['header_user_account_ip']),
            array('dataIndex' => 'confirm_date', 'label' => $this->_['header_user_confirm_date']),
            array('dataIndex' => 'confirm_code', 'label' => $this->_['header_user_confirm_code']),
            array('dataIndex' => 'recovery_date', 'label' => $this->_['header_user_recovery_date']),
            array('dataIndex' => 'user_recovery_code', 'label' => $this->_['header_user_recovery_code'])
        );
    }
}
?>