<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Создание записи "Пользователь"',
    'title_profile_update' => 'Изменение записи "%s"',
    'title_profile_info'   => 'Информация о пользователе "%s"',
    'text_btn_help'        => 'Справка',
    // поля формы
    'label_profile_first_name'   => 'Имя',
    'label_profile_last_name'    => 'Фамилия',
    'title_fieldset_contacts'    => 'Контакты',
    'label_contact_phone'        => 'Телефон',
    'label_contact_phone_a'      => 'Телефон (д)',
    'tip_contact_phone_a'        => 'Дополнительный телефон',
    'title_fieldset_address'     => 'Адрес',
    'label_street'               => 'Улица',
    'label_house'                => 'Дом №',
    'label_flat'                 => 'Квартира №',
    'title_fieldset_account'     => 'Учётная запись клиента (личный кабинет)',
    'label_contact_email'        => 'E-mail',
    'label_user_password'      => 'Пароль',
    'label_user_password-cfrm' => 'Пароль (потв.)',
    'label_user_account'       => 'Подключить',
    'tip_user_account'         => 'Разрешить клиенту доступ через учётную запись к личному кабинету',
);
?>