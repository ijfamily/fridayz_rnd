<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля пользователя сайта"
 * Пакет контроллеров "Профиль пользователя сайта"
 * Группа пакетов     "Пользователи сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SUsers_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data .php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные профиля пользователя сайта
 * 
 * @category   Gear
 * @package    GController_SUsers_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data .php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SUsers_Profile_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array(
        'user_confirm_date', 'user_password', 'user_password-cfrm', 'user_account', 'user_account_date', 'user_account_time', 'user_account_ip', 'user_name',
        'profile_first_name', 'profile_last_name', 'contact_address', 'contact_address_street', 'contact_address_house', 'contact_address_flat', 'contact_country', 
        'contact_region', 'contact_city', 'contact_phone', 'contact_phone_a', 'contact_email'
    );

    /**
     * Первичный ключ таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'user_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_users';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_susers_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        if ($this->state == 'info')
            return sprintf($this->_['title_profile_info'], $record['profile_first_name']);

        return sprintf($this->_['title_profile_update'], $record['profile_first_name']);
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        parent::dataView($sql);

        unset($this->response->data['user_password']);
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        $query = new GDb_Query();
        // удаление записей таблицы "site_user_networks" (авторизация через соц.сети)
        $sql = 'DELETE FROM `site_user_networks` WHERE `user_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_user_networks') === false)
            throw new GSqlException();
        // удаление записей таблицы "site_user_log" (журнал)
        $sql = 'DELETE FROM `site_user_log` WHERE `user_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_user_log') === false)
            throw new GSqlException();
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        // если состояние формы "insert"
        if ($this->isInsert) {
            if (!empty($params['user_account'])) {
                if (empty($params['user_password']))
                    throw new GException('Warning', GText::get('Incorrectly entered or selected from the fields: %s'), '"<b>' . $this->_['label_user_password'] . '</b>"');
                if (empty($params['user_password-cfrm']))
                    throw new GException('Warning', GText::get('Incorrectly entered or selected from the fields: %s'), '"<b>' . $this->_['label_user_password-cfrm'] . '</b>"');
                if (empty($params['contact_email']))
                    throw new GException('Warning', GText::get('Incorrectly entered or selected from the fields: %s'), '"<b>' . $this->_['label_contact_email'] . '</b>"');
                $params['user_account_date'] = date('Y-m-d');
                $params['user_account_time'] = date('H:i:s');
                $params['user_password'] = md5($params['user_password']);
            }
        }

        if (isset($params['user_password-cfrm']))
            unset($params['user_password-cfrm']);
    }
}
?>