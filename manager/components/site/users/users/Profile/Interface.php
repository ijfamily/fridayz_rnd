<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля пользователя сайта"
 * Пакет контроллеров "Профиль пользователя сайта"
 * Группа пакетов     "Пользователи сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SUsers_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface .php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля пользователя сайта
 * 
 * @category   Gear
 * @package    GController_SUsers_Profile
 * @subpackage Interface 
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface .php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SUsers_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'titleEllipsis' => 90,
                  'gridId'        => 'gcontroller_susers_grid',
                  'width'         => 470,
                  'autoHeight'    => true,
                  'resizable'     => false,
                  'stateful'      => false,
                  'buttonsAdd'    => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-site-users',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );

        // адрес доставки
        $fsAddress = array(
            'xtype'      => 'fieldset',
            'labelWidth' => 80,
            'title'      => $this->_['title_fieldset_address'],
            'autoHeight' => true,
            'items'      => array(
                array('xtype'      => 'textfield',
                      'itemCls'    => 'mn-form-item-quiet',
                      'fieldLabel' => $this->_['label_street'],
                      'anchor'     => '100%',
                      'name'       => 'contact_address_street',
                      'allowBlank' => true),
                array('xtype'      => 'textfield',
                      'itemCls'    => 'mn-form-item-quiet',
                      'fieldLabel' => $this->_['label_house'],
                      'width'      => 80,
                      'name'       => 'contact_address_house',
                      'allowBlank' => true),
                array('xtype'      => 'textfield',
                      'itemCls'    => 'mn-form-item-quiet',
                      'fieldLabel' => $this->_['label_flat'],
                      'width'      => 80,
                      'name'       => 'contact_address_flat',
                      'allowBlank' => true)
                )
        );
        // учётная запись
        $fsAccount = array(
            'xtype'      => 'fieldset',
            'labelWidth' => 95,
            'title'      => $this->_['title_fieldset_account'],
            'autoHeight' => true,
            'items'      => array(
                array('xtype'      => 'textfield',
                      'inputType'  => 'password',
                      'fieldLabel' => $this->_['label_user_password'],
                      'width'      => 200,
                      'name'       => 'user_password',
                      'id'         => 'user_password',
                      'maxLength'  => 32,
                      'allowBlank' => true),
                array('xtype'         => 'textfield',
                      'inputType'     => 'password',
                      'vtype'         => 'password',
                      'fieldLabel'    =>$this->_['label_user_password-cfrm'],
                      'name'          => 'user_password-cfrm',
                      'initialPassField' => 'user_password',
                      'width'         => 200,
                      'allowBlank'    => true),
                array('xtype'      => 'mn-field-chbox',
                      'fieldLabel' => $this->_['label_user_account'],
                      'labelTip'   => $this->_['tip_user_account'],
                      'name'       => 'user_account',
                      'default'    => null)
            )
        );
        // контакты
        $fsContacts = array(
            'xtype'      => 'fieldset',
            'labelWidth' => 86,
            'title'      => $this->_['title_fieldset_contacts'],
            'autoHeight' => true,
            'items'      => array(
                array('xtype'      => 'textfield',
                      'itemCls'    => 'mn-form-item-quiet',
                      'fieldLabel' => $this->_['label_contact_phone'],
                      'anchor'     => '100%',
                      'name'       => 'contact_phone',
                      'maxLength'  => 20,
                      'allowBlank' => true),
                array('xtype'      => 'textfield',
                      'itemCls'    => 'mn-form-item-quiet',
                      'fieldLabel' => $this->_['label_contact_phone_a'],
                      'labelTip'   => $this->_['tip_contact_phone_a'],
                      'anchor'     => '100%',
                      'name'       => 'contact_phone_a',
                      'maxLength'  => 20,
                     'allowBlank' => true),
                array('xtype'      => 'textfield',
                      'itemCls'    => 'mn-form-item-quiet',
                      'fieldLabel' => 'E-mail',
                      'anchor'     => '100%',
                      'name'       => 'contact_email',
                     'checkDirty' => false,
                     'maxLength'  => 100,
                     'allowBlank' => true)
                )
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_profile_first_name'],
                  'anchor'     => '100%',
                  'name'       => 'profile_first_name',
                  'maxLength'  => 50,
                  'allowBlank' => false),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_profile_last_name'],
                  'anchor'     => '100%',
                  'name'       => 'profile_last_name',
                  'maxLength'  => 50,
                  'allowBlank' => false),
            $fsContacts,
            $fsAddress,
            $fsAccount
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($items);
        $form->url = $this->componentUrl . 'profile/';
        $form->labelWidth = 97;

        parent::getInterface();
    }
}
?>