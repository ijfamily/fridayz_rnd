<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка пользователей сайта"
 * Пакет контроллеров "Список пользователей сайта"
 * Группа пакетов     "Пользователи сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SUsers_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные списка пользователей сайта
 * 
 * @category   Gear
 * @package    GController_SUsers_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SUsers_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'user_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_users';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 'SELECT SQL_CALC_FOUND_ROWS * FROM `site_users` WHERE 1 %filter ORDER BY %sort LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // доступ к личному кабинету
            'user_account' => array('type' => 'integer'),
            // пользователь
            'profile_first_name' => array('type' => 'string'),
            'profile_last_name' => array('type' => 'string'),
            // адрес
            'contact_address' => array('type' => 'string'),
            'contact_address_street' => array('type' => 'string'),
            'contact_address_house' => array('type' => 'string'),
            'contact_address_flat' => array('type' => 'string'),
            // телефон
            'contact_phone' => array('type' => 'string'),
            'contact_phone_a' => array('type' => 'string'),
            // e-mail
            'contact_email' => array('type' => 'string'),
            // дата регистрации
            'user_account_date' => array('type' => 'string'),
            'user_account_time' => array('type' => 'string'),
            'user_account_ip' => array('type' => 'string'),
            // подтверждение регистрации
            'user_confirm_date' => array('type' => 'string'),
            'user_confirm_code' => array('type' => 'string'),
        );
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        $query = new GDb_Query();
        // удаление записей таблицы "site_users" (пользователи)
        if ($query->clear('site_users') === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_users') === false)
            throw new GSqlException();
        // удаление записей таблицы "site_user_networks" (соц. сети)
        if ($query->clear('site_user_networks') === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_user_networks') === false)
            throw new GSqlException();
        // удаление записей таблицы "site_user_log" (журнал)
        if ($query->clear('site_user_log') === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_user_log') === false)
            throw new GSqlException();
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        $query = new GDb_Query();
        // удаление записей таблицы "site_user_networks" (авторизация через соц.сети)
        $sql = 'DELETE FROM `site_user_networks` WHERE `user_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_user_networks') === false)
            throw new GSqlException();
        // удаление записей таблицы "site_user_log" (журнал)
        $sql = 'DELETE FROM `site_user_log` WHERE `user_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_user_log') === false)
            throw new GSqlException();
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        // адрес
        $address = array();
        if ($record['contact_address_street'])
            $address[] = $record['contact_address_street'];
        if ($record['contact_address_house'])
            $address[] = $record['contact_address_house'];
        if ($record['contact_address_flat'])
            $address[] = $record['contact_address_flat'];
        if ($address) {
            $record['contact_address'] = implode(',', $address);
        }

        return $record;
    }
}
?>