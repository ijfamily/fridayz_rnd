<?php
/**
 * Gear Manager
 *
 * Контроллер         "Развёрнутая запись пользователя"
 * Пакет контроллеров "Список пользователей сайта"
 * Группа пакетов     "Пользователи сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SUsers_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand .php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Expand');

/**
 * Развёрнутая запись пользователя
 * 
 * @category   Gear
 * @package    GController_SUsers_Grid
 * @subpackage Expand
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand .php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SUsers_Grid_Expand extends GController_Grid_Expand
{
    /**
     * Вывод данных в интерфейс
     * 
     * @return void
     */
    protected function dataExpand()
    {
        parent::dataAccessView();

        $data = $this->getAttributes();
        $this->response->data = $data;
    }

    /**
     * Возращает атрибуты записи
     * 
     * @return string
     */
    protected function getAttributes()
    {
        $settings = $this->session->get('user/settings');

        $data = '';
        $query = new GDb_Query();
        $sql = 'SELECT * FROM `site_users` WHERE `user_id`=' . $this->uri->id;
        if (($user = $query->getRecord($sql)) === false)
            throw new GSqlException();
        // Клиент
        $data .= '<fieldset class="fixed" style="width:260px"><label>' . $this->_['title_fieldset_client'] . '</label><ul>';
        $data .= '<li><label>' . $this->_['label_user_name'] . ':</label> ' . $user['profile_first_name'] . ' ' . $user['profile_last_name'] . '</li>';
        $data .= '<li><label>' . $this->_['label_user_account'] . ':</label> ' . $this->_['data_boolean'][(int) $user['user_account']] . '</li>';
        if (!empty($user['user_account_date'])) {
            $date = $user['user_account_date'] . ' ' . $user['user_account_time'];
            $data .= '<li><label>' . $this->_['label_user_account_date'] . ':</label> ';
            $data .= date($settings['format/datetime'], strtotime($date)) . '</li>';
            $data .= '<li><label>' . $this->_['label_user_account_ip'] . ':</label> ' . $user['user_account_ip'] . '</li>';
        }
        $data .= '</ul></fieldset>';
        // Конакты
        $data .= '<fieldset class="fixed" style="width:260px"><label>' . $this->_['title_fieldset_contact'] . '</label><ul>';
        $data .= '<li><label>' . $this->_['label_contact_phone'] . ':</label> ' . ($user['contact_phone'] ? $user['contact_phone'] : $this->_['data_unknow']) . '</li>';
        $data .= '<li><label>' . $this->_['label_contact_phone_a'] . ':</label> ' . ($user['contact_phone_a'] ? $user['contact_phone_a'] : $this->_['data_unknow']) . '</li>';
        $data .= '<li><label>' . $this->_['label_contact_email'] . ':</label> ' . ($user['contact_email'] ? $user['contact_email'] : $this->_['data_unknow']) . '</li>';
        $data .= '</ul></fieldset>';
        // Адрес
        $data .= '<fieldset class="fixed" style="width:260px"><label>' . $this->_['title_fieldset_address'] . '</label><ul>';
        $data .= '<li><label>' . $this->_['label_city_name'] . ':</label> ' . ($user['contact_city'] ? $user['contact_city'] : $this->_['data_unknow']) . '</li>';
        $data .= '<li><label>' . $this->_['label_street'] . ':</label> ' . ($user['contact_address_street'] ? $user['contact_address_street'] : $this->_['data_unknow']) . '</li>';
        $data .= '<li><label>' . $this->_['label_house'] . ':</label> ' . ($user['contact_address_house'] ? $user['contact_address_house'] : $this->_['data_unknow']) . '</li>';
        $data .= '<li><label>' . $this->_['label_flat'] . ':</label> ' . ($user['contact_address_flat'] ? $user['contact_address_flat'] : $this->_['data_unknow']) . '</li>';
        $data .= '</ul></fieldset>';

        return '<div class="mn-row-body border">' . $data . '<div class="wrap"></div>';
    }
}
?>