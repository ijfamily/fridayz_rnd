<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка пользователей сайта"
 * Пакет контроллеров "Список пользователей сайта"
 * Группа пакетов     "Пользователи сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SUsers_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface .php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс списка пользователей сайта
 * 
 * @category   Gear
 * @package    GController_SUsers_Grid
 * @subpackage Interface 
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface .php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SUsers_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'user_id';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'user_id';

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // доступ к личному кабинету
            array('name' => 'user_account', 'type' => 'integer'),
            // пользователь
            array('name' => 'profile_first_name', 'type' => 'string'),
            array('name' => 'profile_last_name', 'type' => 'string'),
            // адрес
            array('name' => 'contact_address', 'type' => 'string'),
            array('name' => 'contact_address_street', 'type' => 'string'),
            array('name' => 'contact_address_house', 'type' => 'string'),
            array('name' => 'contact_address_flat', 'type' => 'string'),
            // телефон
            array('name' => 'contact_phone', 'type' => 'string'),
            array('name' => 'contact_phone_a', 'type' => 'string'),
            // e-mail
            array('name' => 'contact_email', 'type' => 'string'),
            // дата регистрации
            array('name' => 'user_account_date', 'type' => 'string'),
            array('name' => 'user_account_time', 'type' => 'string'),
            array('name' => 'user_account_ip', 'type' => 'string'),
            // подтверждение регистрации
            array('name' => 'user_confirm_date', 'type' => 'string'),
            array('name' => 'user_confirm_code', 'type' => 'string'),
            // восстановление аккаунта
            array('name' => 'user_recovery_date', 'type' => 'string'),
            array('name' => 'user_recovery_code', 'type' => 'string'),
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $settings = $this->session->get('user/settings');
        $this->columns = array(
            array('xtype'     => 'expandercolumn',
                  'url'       => $this->componentUrl . 'grid/expand/',
                  'typeCt'    => 'row'),
            array('xtype'     => 'numbercolumn'),
            array('xtype'     => 'rowmenu'),
            array('xtype'     => 'booleancolumn',
                  'dataIndex' => 'user_account',
                  'cls'       => 'mn-bg-color-gray2',
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-account.png">',
                  'tooltip'   => $this->_['tooltip_user_account'],
                  'width'     => 40,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'profile_first_name',
                  'header'    => '<em class="mn-grid-hd-details"></em>' . $this->_['header_profile_first_name'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'profile_last_name',
                  'header'    => $this->_['header_profile_last_name'],
                  'width'     => 170,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'contact_email',
                  'header'    => $this->_['header_contact_email'],
                  'width'     => 150,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'contact_address',
                  'header'    => $this->_['header_contact_address'],
                  'width'     => 170,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'contact_phone',
                  'header'    => $this->_['header_contact_phone'],
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'contact_phone_a',
                  'hidden'    => true,
                  'header'    => $this->_['header_contact_phone_a'],
                  'tooltip'   => $this->_['tooltip_contact_phone_a'],
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('xtype'     => 'datetimecolumn',
                  'dataIndex' => 'user_account_date',
                  'timeIndex' => 'user_account_time',
                  'frmDate'   => $settings['format/date'],
                  'frmTime'   => $settings['format/time'],
                  'header'    => '<em class="mn-grid-hd-details"></em>' . $this->_['header_user_account_date'],
                  'tooltip'   => $this->_['tooltip_user_account_date'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'date')),
            array('dataIndex' => 'user_account_ip',
                  'header'    => $this->_['header_user_account_ip'],
                  'tooltip'   => $this->_['tooltip_user_account_ip'],
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('xtype'     => 'datetimecolumn',
                  'dataIndex' => 'user_confirm_date',
                  'frmDate'   => $settings['format/datetime'],
                  'header'    => '<em class="mn-grid-hd-details"></em>' . $this->_['header_user_confirm_date'],
                  'tooltip'   => $this->_['tooltip_user_confirm_date'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'date')),
            array('dataIndex' => 'user_confirm_code',
                  'hidden'    => true,
                  'header'    => $this->_['header_user_confirm_code'],
                  'tooltip'   => $this->_['tooltip_user_confirm_code'],
                  'width'     => 150,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('xtype'     => 'datetimecolumn',
                  'hidden'    => true,
                  'dataIndex' => 'user_recovery_date',
                  'frmDate'   => $settings['format/datetime'],
                  'header'    => '<em class="mn-grid-hd-details"></em>' . $this->_['header_user_recovery_date'],
                  'tooltip'   => $this->_['tooltip_user_recovery_date'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'date')),
            array('dataIndex' => 'user_recovery_code',
                  'hidden'    => true,
                  'header'    => $this->_['header_user_recovery_code'],
                  'tooltip'   => $this->_['tooltip_user_recovery_code'],
                  'width'     => 150,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_grid'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_grid'],
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'profileUrl'    => $this->componentUrl . 'profile/')
        );

         // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "edit" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "добавить" (ExtJS class "Manager.button.InsertData")
        $group->items->add(array('xtype' => 'mn-btn-insert-data', 'gridId' => $this->classId));
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' => $this->classId));
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'      => 'mn-btn-clear-data',
                  'msgConfirm' => $this->_['msg_btn_clear'],
                  'gridId'     => $this->classId,
                  'isN'        => true)
        );
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка настройка" (ExtJS class "Manager.button.Widget")
        $group->items->add(
            array('xtype'   => 'mn-btn-widget',
                  'width'   => 60,
                  'text'    => $this->_['text_btn_config'],
                  'tooltip' => $this->_['tooltip_btn_config'],
                  'icon'    => $this->resourcePath . 'icon-btn-config.png',
                  'url'     => $this->componentUri . '../config/' . ROUTER_DELIMITER . 'profile/interface/')
        );
        // кнопка "журнал" (ExtJS class "Manager.button.Widget")
        $group->items->add(
            array('xtype'   => 'mn-btn-widget',
                  'width'   => 60,
                  'text'    => $this->_['text_btn_log'],
                  'tooltip' => $this->_['tooltip_btn_log'],
                  'icon'    => $this->resourcePath . 'icon-btn-log.png',
                  'url'     => $this->componentUri . '../log/' . ROUTER_DELIMITER . 'grid/interface/')
        );

        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = 'component-site-users';
        $this->_cmp->tbar->items->add($group);

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $cellInfo =
            '<div class="mn-grid-cell-tooltip-tl">{profile_first_name} {profile_last_name}</div>'
          . '<div class="mn-grid-cell-tooltip">'
          . '<em>' . $this->_['header_contact_email'] . '</em>: <b>{contact_email}</b><br>'
          . '<em>' . $this->_['header_contact_address'] . '</em>: <b>{contact_address}</b><br>'
          . '<em>' . $this->_['header_contact_phone'] . '</em>: <b>{contact_phone}</b><br>'
          . '<em>' . $this->_['header_contact_phone_a'] . '</em>: <b>{contact_phone_a}</b><br>'
          . '<em>' . $this->_['header_user_account_date'] . '</em>: <b>{user_account_date} {user_account_time}</b><br>'
          . '<em>' . $this->_['header_user_account_ip'] . '</em>: <b>{user_account_ip}</b><br>'
          . '<em>' . $this->_['header_user_confirm_date'] . '</em>: <b>{user_confirm_date}</b><br>'
          . '<em>' . $this->_['header_user_confirm_code'] . '</em>: <b>{user_confirm_code}</b><br>'
          . '<em>' . $this->_['header_user_recovery_date'] . '</em>: <b>{user_recovery_date}</b><br>'
          . '<em>' . $this->_['header_user_recovery_code'] . '</em>: <b>{user_recovery_code}</b><br>'
          . '<em>' . $this->_['tooltip_user_account'] . '</em>: '
          . '<tpl if="user_account == 0"><b>' . $this->_['data_boolean'][0] . '</b></tpl>'
          . '<tpl if="user_account == 1"><b>' . $this->_['data_boolean'][1] . '</b></tpl>'
          . '</div>';
        $cellAccount =
            '<div class="mn-grid-cell-tooltip-tl">{profile_first_name} {profile_last_name}</div>'
          . '<div class="mn-grid-cell-tooltip">'
          . '<em>' . $this->_['header_user_account_date'] . '</em>: <b>{user_account_date}</b><br>'
          . '<em>' . $this->_['header_user_account_ip'] . '</em>: <b>{user_account_ip}</b><br>'
          . '</div>';
        $cellConfirm =
            '<div class="mn-grid-cell-tooltip-tl">{profile_first_name} {profile_last_name}</div>'
          . '<div class="mn-grid-cell-tooltip">'
          . '<em>' . $this->_['header_user_confirm_date'] . '</em>: <b>{user_confirm_date}</b><br>'
          . '<em>' . $this->_['header_user_confirm_code'] . '</em>: <b>{user_confirm_code}</b><br>'
          . '</div>';
        $cellRecovery =
            '<div class="mn-grid-cell-tooltip-tl">{profile_first_name} {profile_last_name}</div>'
          . '<div class="mn-grid-cell-tooltip">'
          . '<em>' . $this->_['header_user_recovery_date'] . '</em>: <b>{user_recovery_date}</b><br>'
          . '<em>' . $this->_['header_user_recovery_code'] . '</em>: <b>{user_recovery_code}</b><br>'
          . '</div>';
        $this->_cmp->cellTips = array(
            array('field' => 'profile_first_name', 'tpl' => $cellInfo),
            array('field' => 'profile_last_name', 'tpl' => '{profile_last_name}'),
            array('field' => 'contact_address', 'tpl' => '{contact_address}'),
            array('field' => 'contact_phone', 'tpl' => '{contact_phone}'),
            array('field' => 'contact_phone_a', 'tpl' => '{contact_phone_a}'),
            array('field' => 'contact_email', 'tpl' => '{contact_email}'),
            array('field' => 'user_account_date', 'tpl' => $cellAccount),
            array('field' => 'user_confirm_date', 'tpl' => $cellConfirm),
            array('field' => 'user_recovery_date', 'tpl' => $cellRecovery),
        );

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $items = array(
            array('text'    => $this->_['rowmenu_edit'],
                  'iconCls' => 'icon-form-edit',
                  'url'     => $this->componentUrl . 'profile/interface/')
        );
        $this->_cmp->rowMenu = array('xtype' => 'mn-grid-rowmenu', 'items' => $items);

        parent::getInterface();
    }
}
?>