<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'       => 'Пользователи сайта',
    'tooltip_grid'     => 'список зарегистрированных пользователей на сайте',
    'rowmenu_edit'     => 'Редактировать',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">("Пользователи сайта")</span> ?',
    'text_btn_config'          => 'Настройки',
    'tooltip_btn_config'       => 'Настройки пользователей',
    'text_btn_log'             => 'Журнал',
    'tooltip_btn_log'          => 'Журнал действий пользователей',
    // столбцы
    'header_profile_first_name' => 'Имя',
    'header_profile_last_name'  => 'Фамилия',
    'header_user_account'    => 'Учётная запись',
    'tooltip_user_account'   => 'Доступ к личному кабинету',
    'header_user_account_date'   => 'Зарегистрирован',
    'tooltip_user_account_date'  => 'Дата регистрации клиента на сайте',
    'header_contact_address'     => 'Адрес',
    'header_contact_phone'       => 'Телефон',
    'header_contact_phone_a'     => 'Телефон (д)',
    'tooltip_contact_phone_a'    => 'Дополнительный телефон',
    'header_contact_email'       => 'E-mail',
    'header_user_account_ip'     => 'IP адрес',
    'tooltip_user_account_ip'    => 'IP адрес c которого была регистрация пользователя',
    'header_user_confirm_date'   => 'Подтверждён',
    'tooltip_user_confirm_date'  => 'Если пользователь подтверждает свою регистрацию через письмо отправленное на его e-mail',
    'header_user_confirm_code'   => 'Код подтверждения',
    'tooltip_user_confirm_code'  => 'Код отправленный на e-mail пользователю для подтверждения его регистрации',
    'header_user_recovery_date'  => 'Восстановлен',
    'tooltip_user_recovery_date' => 'Если пользователь восстанавливает свой аккаунт через письмо отправленное на его e-mail',
    'header_user_recovery_code'  => 'Код восстановления',
    'tooltip_user_recovery_code' => 'Код отправленный на e-mail пользователю для восстанавления его аккаунта',
    // развернутая запись
    'title_fieldset_client'   => 'Пользователь',
    'label_user_name'         => 'Ф.И.О.',
    'label_user_account'      => 'Доступ к аккаунту',
    'label_user_account_date' => 'Дата регистрации',
    'label_user_account_ip'   => 'IP адрес',

    'title_fieldset_contact'  => 'Конакты',
    'label_contact_phone'     => 'Телефон',
    'label_contact_phone_a'   => 'Телефон (д)',
    'label_contact_email'     => 'E-mail',
    'title_fieldset_address'  => 'Адрес',
    'label_city_name'         => 'Город',
    'label_street'            => 'Улица',
    'label_house'             => 'Дом',
    'label_flat'              => 'Квартира',
    // тип данных
    'data_boolean' => array('нет', 'да'),
    'data_unknow'  => '[неизвестно]'
);
?>