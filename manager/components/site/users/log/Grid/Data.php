<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные журнала пользователей сайта"
 * Пакет контроллеров "Журнал пользователей сайта"
 * Группа пакетов     "Пользователи сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SUsersLog_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные журнала пользователей сайта
 * 
 * @category   Gear
 * @package    GController_SUsersLog_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SUsersLog_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'log_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_user_log';

    /**
     * Обозначения
     *
     * @var array
     */
    public $icons = array(
        'signin' => 'icon-signin.png',
        'signup' => 'icon-signup.png',
        'signin and signup' => 'icon-signup.png',
        'delete' => 'icon-delete.png',
        'edit'   => 'icon-edit.png',
        'update' => 'icon-edit.png',
    );

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 'SELECT SQL_CALC_FOUND_ROWS * FROM `site_user_log` WHERE 1 %filter %fastfilter ORDER BY %sort LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // идент. пользователя
            'user_id' => array('type' => 'string'),
            // e-mail
            'log_email' => array('type' => 'string'),
            // дата
            'log_date' => array('type' => 'string'),
            // время
            'log_time' => array('type' => 'string'),
            // соц.сеть
            'log_network' => array('type' => 'string'),
            // действие
            'log_action' => array('type' => 'string'),
            // сообщение
            'log_text' => array('type' => 'string'),
            // ip адрес
            'log_ip' => array('type' => 'string'),
            // успех
            'log_success' => array('type' => 'integer'),
            // обозначение
            'log_icon' => array('type' => 'string')
        );
    }

    /**
     * Возращает sql запрос для быстрого фильтра
     * (для подстановки "%fastfilter" в $sql)
     * 
     * @param string $key идендификатор поля
     * @param string $value значение
     * @return string
     */
    protected function getTreeFilter($key, $value)
    {
        // идендификатор поля
        switch ($key) {
            // по дате правки
            case 'byDt':
                $toDate = date('Y-m-d');
                switch ($value) {
                    case 'day':
                        return ' AND (`bot_date` BETWEEN "' . $toDate . '" AND "' . $toDate . '"'
                             . ' OR `bot_date` BETWEEN "' . $toDate . '" AND "' . $toDate . '")';

                    case 'week':
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') - 7, date('Y')));
                        return ' AND (`bot_date` BETWEEN "' . $fromDate . '" AND "' . $toDate . '"'
                             . ' OR `bot_date` BETWEEN "' . $fromDate . '" AND "' . $toDate . '")';

                    case 'month':
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, date('m') - 1, date('d'), date('Y')));
                        return ' AND (`bot_date` BETWEEN "' . $fromDate . '" AND "' . $toDate . '"'
                             . ' OR `bot_date` BETWEEN "' . $fromDate . '" AND "' . $toDate . '")';
                }
                break;

            // по ip адресу
            case 'byIp':
                return ' AND `bot_ipaddress`="' . $value . '" ';

            // по боту
            case 'byBo':
                return ' AND `bot_name`="' . $value . '" ';
        }

        return '';
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        $query = new GDb_Query();
        // удаление записей таблицы "site_user_log" (журнал пользователей)
        if ($query->clear('site_user_log') === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_user_log') === false)
            throw new GSqlException();
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        // если есть обозначение
        $icon = 'icon-profile.png';
        if (isset($this->icons[$record['log_action']]))
            $icon= $this->icons[$record['log_action']];
        $record['log_icon'] = '<img src="' . $this->resourcePath . '/icons/' . $icon . '" />';
        // действие
        if (isset($this->_['data_action'][$record['log_action']]))
            $record['log_action'] = $this->_['data_action'][$record['log_action']];
        // если статья не опубликована
        if (empty($record['log_success']))
            $record['rowCls'] = 'mn-row-warning';

        return $record;
    }
}
?>