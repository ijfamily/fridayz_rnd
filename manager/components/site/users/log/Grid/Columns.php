<?php
/**
 * Gear Manager
 *
 * Контроллер         "Столбцы списка журнала пользователей сайта"
 * Пакет контроллеров "Журнал пользователей сайта"
 * Группа пакетов     "Пользователи сайта"
 * Модуль             "Сайт""
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SUsersLog_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Columns.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Columns');

/**
 * Столбцы списка журнала пользователей сайта
 * 
 * @category   Gear
 * @package    GController_SUsersLog_Grid
 * @subpackage Columns
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Columns.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SUsersLog_Grid_Columns extends GController_Grid_Columns
{}
?>