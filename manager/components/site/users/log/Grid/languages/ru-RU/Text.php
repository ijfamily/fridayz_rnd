<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Журнал пользователей',
    'tooltip_grid' => 'действия авторизированных пользователей на сайте',
    'rowmenu_info' => 'Информация о записи',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">("Журнал пользователей")</span> ?',
    'label_domain'             => 'Сайт',
    'text_btn_users'           => 'Пользователи',
    'tooltip_btn_users'        => 'Пользователи сайта',
    // столбцы
    'header_log_email'   => 'E-mail',
    'header_log_date'    => 'Дата',
    'header_log_network' => 'Социальная сеть',
    'header_log_action'  => 'Действие',
    'header_log_ip'      => 'IP адрес',
    'header_log_success' => 'Упех',
    'header_log_text'    => 'Сообщение',
    // тип
    'data_boolean' => array('нет', 'да'),
    'data_action'  => array(
        'edit'   => 'правка',
        'update' => 'правка',
        'delete' => 'удаление аккаунта',
        'signin' => 'авторизация',
        'signup'  => 'регистрация',
        'signout' => 'выход',
        'remind password'   => 'напомнить пароль',
        'recovery password' => 'восстановить пароль',
        'signin and signup' => 'авторизация с регистрацией',
    ),
    // быстрый фильтр
    'text_all_records' => 'Все роботы',
    'text_by_date'     => 'По дате индексации',
    'text_by_day'      => 'За день',
    'text_by_week'     => 'За неделю',
    'text_by_month'    => 'За месяц',
    'text_by_bot'      => 'По роботу',
    'text_by_ip'       => 'По IP адресу'
);
?>