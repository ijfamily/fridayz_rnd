<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс журнала пользователей сайта"
 * Пакет контроллеров "Журнал пользователей сайта"
 * Группа пакетов     "Пользователи сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SUsersLog_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс журнала пользователей сайта
 * 
 * @category   Gear
 * @package    GController_SUsersLog_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SUsersLog_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'log_id';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'log_date';

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // идент. пользователя
            array('name' => 'user_id', 'type' => 'integer'),
            // e-mail
            array('name' => 'log_email', 'type' => 'string'),
            // дата
            array('name' => 'log_date', 'type' => 'string'),
            // время
            array('name' => 'log_time', 'type' => 'string'),
            // соц.сеть
            array('name' => 'log_network', 'type' => 'string'),
            // действие
            array('name' => 'log_action', 'type' => 'string'),
            // сообщение
            array('name' => 'log_text', 'type' => 'string'),
            // ip адрес
            array('name' => 'log_ip', 'type' => 'string'),
            // успех
            array('name' => 'log_success', 'type' => 'integer'),
            // обозначение
            array('name' => 'log_icon', 'type' => 'string')
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $settings = $this->session->get('user/settings');
        $this->columns = array(
            /*
            array('xtype'     => 'expandercolumn',
                  'url'       => $this->componentUrl . 'grid/expand/',
                  'typeCt'    => 'row'),
            */
            array('xtype'     => 'numbercolumn'),
            //array('xtype'     => 'rowmenu'),
            array('xtype'     => 'datetimecolumn',
                  'dataIndex' => 'log_date',
                  'timeIndex' => 'log_time',
                  'userIndex' => 'user_id',
                  'frmDate'   => $settings['format/date'],
                  'frmTime'   => $settings['format/time'],
                  'header'    => $this->_['header_log_date'],
                  'isSystem'  => true,
                  'urlQuery'  => '?state=info',
                  'url'       => 'site/users/users/' . ROUTER_DELIMITER . 'profile/interface/',
                  'width'     => 130,
                  'sortable'  => true,
                  'filter'    => array('type' => 'date')),
            array('dataIndex' => 'log_email',
                  'header'    => $this->_['header_log_email'],
                  'width'     => 180,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'log_network',
                  'header'    => $this->_['header_log_network'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'log_icon',
                  'header'    => '&nbsp;',
                  'fixed'     => true,
                  'hideable'  => false,
                  'width'     => 25,
                  'sortable'  => false,
                  'menuDisabled' => true),
            array('dataIndex' => 'log_action',
                  'header'    => $this->_['header_log_action'],
                  'width'     => 170,
                  'sortable'  => true/*,
                  'filter'    => array('type' => 'string', 'disabled' => true)*/),
            array('dataIndex' => 'log_text',
                  'header'    => $this->_['header_log_text'],
                  'width'     => 170,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => true)),
            array('dataIndex' => 'log_ip',
                  'header'    => $this->_['header_log_ip'],
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('xtype'     => 'booleancolumn',
                  'dataIndex' => 'log_success',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-action.png" align="absmiddle">',
                  'tooltip'   => $this->_['header_log_success'],
                  'align'     => 'center',
                  'width'     => 45,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean', 'disabled' => false))
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_grid'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_grid'],
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'profileUrl'    => $this->componentUrl . 'profile/')
        );

         // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "edit" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' => $this->classId));
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'      => 'mn-btn-clear-data',
                  'msgConfirm' => $this->_['msg_btn_clear'],
                  'gridId'     => $this->classId,
                  'isN'        => true)
        );
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "пользователи" (ExtJS class "Manager.button.Widget")
        $group->items->add(
            array('xtype'   => 'mn-btn-widget',
                  'width'   => 60,
                  'text'    => $this->_['text_btn_users'],
                  'tooltip' => $this->_['tooltip_btn_users'],
                  'icon'    => $this->resourcePath . 'icon-btn-users.png',
                  'url'     => $this->componentUri . '../users/' . ROUTER_DELIMITER . 'grid/interface/')
        );
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = 'component-site-users-log';
        $this->_cmp->tbar->items->add($group);

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $cellInfo =
            '<div class="mn-grid-cell-tooltip-tl">{log_date} {log_time}</div>'
          . '<div class="mn-grid-cell-tooltip">'
          . '<em>' . $this->_['header_log_email'] . '</em>: <b>{log_email}</b><br>'
          . '<em>' . $this->_['header_log_network'] . '</em>: <b>{log_network}</b><br>'
          . '<em>' . $this->_['header_log_action'] . '</em>: <b>{log_action}</b><br>'
          . '<em>' . $this->_['header_log_ip'] . '</em>: <b>{log_ip}</b><br>'
          . '<em>' . $this->_['header_log_success'] . '</em>: '
          . '<tpl if="log_success == 0"><b>' . $this->_['data_boolean'][0] . '</b>;</tpl>'
          . '<tpl if="log_success == 1"><b>' . $this->_['data_boolean'][1] . '</b>;</tpl><br>'
          . '</div>';
        $this->_cmp->cellTips = array(
            array('field' => 'log_date', 'tpl' => $cellInfo),
            array('field' => 'log_email', 'tpl' => '{log_email}'),
            array('field' => 'log_network', 'tpl' => '{log_network}'),
            array('field' => 'log_action', 'tpl' => '{log_action}'),
            array('field' => 'log_text', 'tpl' => '{log_text}')
        );

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $items = array(
            array('text' => $this->_['rowmenu_info'],
                  'icon' => $this->resourcePath . 'icon-item-info.png',
                  'url'  => $this->componentUrl . 'info/interface/')
        );
        $this->_cmp->rowMenu = array('xtype' => 'mn-grid-rowmenu', 'items' => $items);

        // быстрый фильтр списка (ExtJs class "Manager.tree.GridFilter")
        $this->_slidePanel->cls = 'mn-tree-gridfilter';
        $this->_slidePanel->width = 250;
        $this->_slidePanel->initRoot = array(
            'text'     => 'Filter',
            'id'       => 'byRoot',
            'expanded' => true,
            'children' => array(
                array('text'     => $this->_['text_all_records'],
                      'value'    => 'all',
                      'expanded' => true,
                      'leaf'     => false,
                      'children' => array(
                        array('text'     => $this->_['text_by_date'],
                              'id'       => 'byDt',
                              'leaf'     => false,
                              'expanded' => true,
                              'children' => array(
                                  array('text'     => $this->_['text_by_day'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 'day'),
                                  array('text'     => $this->_['text_by_week'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true, 
                                        'value'    => 'week'),
                                  array('text'     => $this->_['text_by_month'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 'month')
                              )
                        ),
                        array('text'    => $this->_['text_by_bot'],
                              'id'      => 'byBo',
                              'leaf'    => false),
                        array('text'    => $this->_['text_by_ip'],
                              'id'      => 'byIp',
                              'leaf'    => false)
                    )
                )
            )
        );

        parent::getInterface();
    }
}
?>