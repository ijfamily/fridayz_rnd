<?php
/**
 * Terminal
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Distributed under the Lesser General Public License (LGPL)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Terminal
 * @package    Text
 * @copyright  Copyright (c) 2014-2015 <anton.tivonenko@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Text.php 2015-02-01 12:00:00 Anton Tivonenko $
 */

return array();
?>