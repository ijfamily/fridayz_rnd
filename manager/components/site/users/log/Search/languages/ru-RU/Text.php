<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Поиск в списке "Журналы пользователей"',
    // поля
    'header_log_email'   => 'E-mail',
    'header_log_date'    => 'Дата',
    'header_log_network' => 'Социальная сеть',
    'header_log_action'  => 'Действие',
    'header_log_ip'      => 'IP адрес',
    'header_log_success' => 'Упех'
);
?>