<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск в журнале пользователей сайта"
 * Пакет контроллеров "Поиск в журнале пользователей сайта"
 * Группа пакетов     "Пользователи сайт"
 * Модуль             "Сайт"
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SUsersLog_Search
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2015 <anton.tivonenko@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Interface.php 2015-02-01 12:00:00 Anton Tivonenko $
 */

Gear::controller('Search/Interface');

/**
 * Поиск в журнале пользователей сайта
 * 
 * @category   Gear
 * @package    GController_SUsersLog_Search
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2015 <anton.tivonenko@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Interface.php 2015-02-01 12:00:00 Anton Tivonenko $
 */
final class GController_SUsersLog_Search_Interface extends GController_Search_Interface
{
    /**
     * Идентификатор DOM компонента (списка)
     *
     * @var string
     */
    public $gridId = 'gcontroller_suserslog_grid';

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataSearch")
        $this->_cmp->setProps(
            array('title'       => $this->_['title_search'],
                  'iconSrc'     => $this->resourcePath . 'icon.png',
                  'gridId'      => $this->gridId,
                  'btnSearchId' => $this->gridId . '-bnt_search',
                  'url'         => $this->componentUrl . 'search/data/')
        );

        parent::getInterface();
    }
}
?>