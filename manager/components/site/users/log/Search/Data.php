<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск в журнале пользователей сайта"
 * Пакет контроллеров "Поиск в журнале пользователей сайта"
 * Группа пакетов     "Пользователи сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SUsersLog_Search
 * @subpackage Data
 * @copyright  Copyright (c) 2014-2015 <anton.tivonenko@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Data.php 2015-02-01 12:00:00 Anton Tivonenko $
 */

Gear::controller('Search/Data');

/**
 * Поиск в журнале пользователей сайта
 * 
 * @category   Gear
 * @package    GController_SUsersLog_Search
 * @subpackage Data
 * @copyright  Copyright (c) 2014-2015 <anton.tivonenko@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Data.php 2015-02-01 12:00:00 Anton Tivonenko $
 */
final class GController_SUsersLog_Search_Data extends GController_Search_Data
{
    /**
     * дентификатор компонента (списка) к которому применяется поиск
     *
     * @var string
     */
    public $gridId = 'gcontroller_suserslog_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_suserslog_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор,
     * используется для инициализации атрибутов контроллера без конструктора
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'log_email', 'label' => $this->_['header_log_email']),
            array('dataIndex' => 'log_network', 'label' => $this->_['header_log_network']),
            array('dataIndex' => 'log_action', 'label' => $this->_['header_log_action']),
            array('dataIndex' => 'log_ip', 'label' => $this->_['header_log_ip']),
            array('dataIndex' => 'log_success', 'label' => $this->_['header_log_success'])
        );
    }
}
?>