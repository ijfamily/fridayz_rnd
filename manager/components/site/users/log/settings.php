<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Журнал пользователей"
 * Группа пакетов     "Пользователи сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SUsersLog
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Журнал пользователей
 * Описание: Журнал пользователей
 * Меню: Журнал пользователей
 * ID класса: gcontroller_suserslog_grid
 * Модуль: Сайт
 * Группа: Сайт
 * Очищать: да
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске компонентов: нет
 *    В главном меню: нет
 * Ресурс
 *    Компонент: site/users/log/
 *    Контроллер: site/users/log/Grid/
 *    Интерфейс: grid/interface/
 *    Меню:
 */

return array(
    // {S}- модуль "Site" {} - пакет контроллеров "Users log" -> SUsersLog
    'clsPrefix' => 'SUsersLog',
    // использовать язык
    'language'  => 'ru-RU'
);
?>