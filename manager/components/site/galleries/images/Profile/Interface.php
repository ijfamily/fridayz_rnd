<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля изображения"
 * Пакет контроллеров "Изображение галереи"
 * Группа пакетов     "Галереи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SGalleryImages_Profile
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля изображения
 * 
 * @category   Gear
 * @package    GController_SGalleryImages_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SGalleryImages_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Идентификатор галереи
     *
     * @var integer
     */
    protected $_galleryId = 0;

    /**
     * Галерея
     *
     * @var mixed
     */
    protected $_gallery = false;

    /**
     * Если изобрежение добавляется извне
     *
     * @var boolean
     */
    protected $isInsertOutside = false;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        $this->_gallery = $this->_galleryId = false;
        $this->isInsertOutside = $this->isInsert && $this->uri->getVar('parent', false) !== false;
        // если едент. галереи задаётся из вне
        if ($this->isInsertOutside) {
            $this->_galleryId = $this->uri->getVar('parent', false);
        } else
            if ($this->isInsert) {
                $this->_galleryId = $this->store->get('record', 0, 'gcontroller_sgalleries_grid');
            }
    }

    /**
     * Возращает интерфейса полей с текстом
     * 
     * @return array
     */
    protected function getTextInterface()
    {
        $values = array();

        // если состояние формы "правка"
        if ($this->isUpdate) {
            $query = new GDb_Query();
            // обновление текста в картинках
            $sql = 'SELECT * FROM `site_gallery_images_l` WHERE `image_id`=' . $this->uri->id;
            if ($query->execute($sql) === false)
                throw new GSqlException();
            while (!$query->eof()) {
                $item = $query->next();
                if (!isset($values[$item['language_id']]))
                    $values[$item['language_id']] = array();
                $values[$item['language_id']]['image_lid'] = $item['image_lid'];
                $values[$item['language_id']]['image_title'] = $item['image_title'];
                $values[$item['language_id']]['image_thumb_title'] = $item['image_thumb_title'];
                $values[$item['language_id']]['image_longdesc'] = $item['image_longdesc'];
            }
        }

        // язык сайта по умолчанию
        $lang = $this->config->getFromCms('Site', 'LANGUAGE');
        $langs = $this->config->getFromCms('Site', 'LANGUAGES');
        $did = $langs[$lang]['id'];

        // вкладки с языками
        $tabs[] = array();
        foreach ($langs as $key => $lang) {
            $lid = $lang['id'];
            $tabs[] = array(
                'title'      => $lang['title'],
                'layout'     => 'form',
                'baseCls'    => 'mn-form-tab-body',
                'iconSrc'    => $this->resourcePath . ($did == $lid ? 'icon-tab-text-def.png' : 'icon-tab-text.png'),
                'labelWidth' => 90,
                'items'   => array(
                    array('xtype' => 'hidden',
                          'id'    => 'ln[' . $lid . '][image_lid]',
                          'name'  => 'ln[' . $lid . '][image_lid]',
                          'value' => isset($values[$lid]['image_lid']) ? $values[$lid]['image_lid'] : ''),
                    array('xtype'      => 'textfield',
                          'itemCls'    => 'mn-form-item-quiet',
                          'fieldLabel' => $this->_['label_image_title'],
                          'labelTip'   => $this->_['tip_image_title'],
                          'id'         => 'ln[' . $lid . '][image_title]',
                          'name'       => 'ln[' . $lid . '][image_title]',
                          'value'      => isset($values[$lid]['image_title']) ? $values[$lid]['image_title'] : '',
                          'maxLength'  => 255,
                          'anchor'     => '100%',
                          'allowBlank' => true),
                    array('xtype'      => 'textfield',
                          'itemCls'    => 'mn-form-item-quiet',
                          'fieldLabel' => $this->_['label_image_thumb_title'],
                          'labelTip'   => $this->_['tip_image_thumb_title'],
                          'id'         => 'ln[' . $lid . '][image_thumb_title]',
                          'name'       => 'ln[' . $lid . '][image_thumb_title]',
                          'value'      => isset($values[$lid]['image_thumb_title']) ? $values[$lid]['image_thumb_title'] : '',
                          'maxLength'  => 255,
                          'anchor'     => '100%',
                          'allowBlank' => true)
                )
            );
        }

        return array(
            'xtype'             => 'tabpanel',
            'tabPosition'       => 'bottom',
            'layoutOnTabChange' => true,
            'activeTab'         => 0,
            'anchor'            => '100%',
            'height'            => 285,
            'items'             => array($tabs)
        );
    }

    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        $data = array('max' => 1, 'image_entire_filename' => '', 'image_thumb_filename' => '');

        parent::getDataInterface();

        $query = new GDb_Query();
        // если добавление изображения извне
        if ($this->isInsertOutside || $this->isInsert) {
            $sql = 'SELECT * FROM `site_gallery` WHERE `gallery_id`=' . $this->_galleryId;
            if (($this->_gallery = $query->getRecord($sql)) === false)
                throw new GSqlException();
        }
        // если состояние формы "вставка"
        if ($this->isInsert) {
            $sql = 'SELECT MAX(`image_index`) `max` FROM `site_gallery_images` WHERE `gallery_id`=' . $this->_galleryId;
            if (($record = $query->getRecord($sql)) === false)
                throw new GSqlException();
            $data['max'] = $record['max'] + 1;

            return $data;
        }
        // если состояние формы "правка"
        if ($this->isUpdate) {
            // выбранное изображение
            $sql = 'SELECT * FROM `site_gallery_images` WHERE `image_id`=' . (int) $this->uri->id;
            if (($record = $query->getRecord($sql)) === false)
                throw new GSqlException();
            $data['image_entire_filename'] = $record['image_entire_filename'];
            $data['image_thumb_filename'] = $record['image_thumb_filename'];

            $this->_galleryId = $record['gallery_id'];
            $sql = 'SELECT * FROM `site_gallery` WHERE `gallery_id`=' . $this->_galleryId;
            if (($this->_gallery = $query->getRecord($sql)) === false)
                throw new GSqlException();
        }

        return $data;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'id'            => $this->classId,
                  'titleEllipsis' => 80,
                  'gridId'        => 'gcontroller_sgalleryimages_grid',
                  'width'         => 505,
                  'autoHeight'    => true,
                  'resizable'     => false,
                  'stateful'      => false)
        );

        // поля формы (ExtJS class "Ext.Panel")
        // поля вкладки "атрибуты"
        $tabAttrItems = array(
            array('xtype'      => 'spinnerfield',
                  'itemCls'    => 'mn-form-item-quiet',
                  'fieldLabel' => $this->_['label_image_index'],
                  'labelTip'   => $this->_['tip_image_index'],
                  'name'       => 'image_index',
                  'resetable'  => false,
                  'value'      => $data['max'],
                  'width'      => 70,
                  'allowBlank' => true,
                  'emptyText'  => 1),
            array('xtype'      => 'mn-field-chbox',
                  'itemCls'    => 'mn-form-item-quiet',
                  'fieldLabel' => $this->_['label_image_visible'],
                  'labelTip'   => $this->_['tip_image_visible'],
                  'default'    => $this->isUpdate ? null : 1,
                  'name'       => 'image_visible'),
            $this->getTextInterface()
        );
        // вкладка "атрибуты"
        $tabAttr = array(
            'iconSrc'     => $this->resourcePath . 'icon-tab-attr.png',
            'title'       => $this->_['title_tab_attributes'],
            'layout'      => 'form',
            'labelWidth'  => 95,
            'baseCls'     => 'mn-form-tab-body',
            'items'       => array($tabAttrItems)
        );

        // поля вкладки "изображение"
        $path = '/' . $this->config->getFromCms('Galleries', 'DIR') . $this->_gallery['gallery_folder'] . '/';
        // если состояние формы "правка"
        if ($this->isUpdate) {
            $tabImageItems = array(
                array('xtype'   => 'mn-form-image-view',
                      'image'   => array(
                          'src'    => $path . $data['image_entire_filename'],
                          'full'   => $path . $data['image_entire_filename'],
                          'title'  => $data['image_entire_filename'],
                          'width'  => 300,
                          'height' => 180,
                      ),
                      'default' => $path . 'list_none.jpg',
                      'buttons' => array(
                          'download',
                          'picture',
                          array('type'  => 'rename',
                                'title' => $this->_['title_bar_rename'],
                                'url'   => $this->componentUrl . 'rename/interface/' . $this->uri->id)
                      )
                ),
                array('xtype'      => 'textfield',
                      'itemCls'    => 'mn-form-item-info',
                      'fieldLabel' => $this->_['label_entire_filename'],
                      'name'       => 'image_entire_filename',
                      'maxLength'  => 255,
                      'anchor'     => '100%',
                      'readOnly'   => true,
                      'allowBlank' => true),
                array('xtype'      => 'textfield',
                      'itemCls'    => 'mn-form-item-info',
                      'fieldLabel' => $this->_['label_entire_url'],
                      'labelTip'   => $this->_['tip_entire_url'],
                      'name'       => 'image_entire_url',
                      'maxLength'  => 255,
                      'anchor'     => '100%',
                      'readOnly'   => true,
                      'allowBlank' => true)
            );
        // если состояние формы "вставка"
        } else {
            $tabImageItems = array(
                array('xtype'      => 'fieldset',
                      'labelWidth' => 184,
                      'title'      => $this->_['title_fieldset_img'],
                      'autoHeight' => true,
                      'items'      => array(
                          array('xtype'      => 'mn-field-upload',
                                'emptyText'  => $this->_['text_empty'],
                                'name'       => 'image',
                                'buttonText' => $this->_['text_btn_upload'],
                                'anchor'     => '100%',
                                'buttonCfg'  => array('width' => 70)
                          ),
                          array('xtype' => 'label', 'html' => sprintf($this->_['note_img'], $this->config->getFromCms('Site', 'FILES/EXT/IMAGES'))),
                      )
                )
            );
        }
        // вкладка "изображение"
        $tabImage = array(
            'iconSrc'     => $this->resourcePath . 'icon-tab-img.png',
            'title'       => $this->_['title_tab_img'],
            'layout'      => 'form',
            'labelWidth'  => 85,
            'baseCls'     => 'mn-form-tab-body',
            'defaultType' => 'textfield',
            'autoScroll'  => true,
            'items'       => array($tabImageItems)
        );

        // вкладки окна
        $tabs = array(
            array('xtype'             => 'tabpanel',
                  'layoutOnTabChange' => true,
                  'activeTab'         => 0,
                  'style'             => 'padding:3px',
                  'anchor'            => '100%',
                  'height'            => 380,
                  'items'             => array($tabAttr, $tabImage)),
            array('xtype' => 'hidden',
                  'name'  => 'gallery_id',
                  'value' => $this->_galleryId)
        );
        // если состояние формы "правка"
        if ($this->isUpdate) {
            // вкладка "эскиз изображения"
            $tabThumbItems = array(
                array('xtype'   => 'mn-form-image-view',
                      'image'   => array(
                          'src'    => $path . $data['image_thumb_filename'],
                          'full'   => $path . $data['image_thumb_filename'],
                          'title'  => $data['image_thumb_filename'],
                          'width'  => 300,
                          'height' => 180,
                      ),
                      'default' => $path . 'list_none.jpg',
                      'buttons' => array(
                          'download',
                          'picture',
                          array('type'  => 'rename',
                                'title' => $this->_['title_bar_rename'],
                                'url'   => $this->componentUrl . 'rename/interface/' . $this->uri->id)
                      )
                ),
                array('xtype'      => 'textfield',
                      'itemCls'    => 'mn-form-item-info',
                      'fieldLabel' => $this->_['label_entire_filename'],
                      'name'       => 'image_thumb_filename',
                      'maxLength'  => 255,
                      'anchor'     => '100%',
                      'readOnly'   => true,
                      'allowBlank' => true),
                array('xtype'      => 'textfield',
                      'itemCls'    => 'mn-form-item-info',
                      'fieldLabel' => $this->_['label_entire_url'],
                      'labelTip'   => $this->_['tip_entire_url'],
                      'name'       => 'image_thumb_url',
                      'maxLength'  => 255,
                      'anchor'     => '100%',
                      'readOnly'   => true,
                      'allowBlank' => true)
            );
            // вкладка "эскиз"
            $tabs[0]['items'][] = array(
                'iconSrc'     => $this->resourcePath . 'icon-tab-mimg.png',
                'title'       => $this->_['title_tab_thm'],
                'layout'      => 'form',
                'labelWidth'  => 85,
                'baseCls'     => 'mn-form-tab-body',
                'defaultType' => 'textfield',
                'items'       => array($tabThumbItems)
            );
        }

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($tabs);
        $form->url = $this->componentUrl . 'profile/';
        $form->fileUpload = true;

        parent::getInterface();
    }
}
?>