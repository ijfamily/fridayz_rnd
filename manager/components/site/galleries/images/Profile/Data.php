<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные для интерфейса профиля изображения"
 * Пакет контроллеров "Изображение галереи"
 * Группа пакетов     "Галереи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SGalleryImages_Profile
 * @copyright  Copyright (c) 2013-2016 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('String');
Gear::library('File');
Gear::controller('Profile/Data');

/**
 * Данные для интерфейса профиля изображения
 * 
 * @category   Gear
 * @package    GController_SGalleryImages_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SGalleryImages_Profile_Data extends GController_Profile_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array(
        'gallery_id', 'image_index', 'image_entire_filename', 'image_entire_resolution',
        'image_entire_filesize', 'image_entire_style', 'image_thumb_filename', 'image_thumb_resolution',
         'image_thumb_filesize', 'image_date', 'image_visible', 'sys_date_insert',
        'sys_date_update'
    );

    /**
     * Первичный ключ таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'image_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_gallery_images';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sgalleries_grid';

    /**
     * Идентификатор галереи
     *
     * @var integer
     */
    protected $_galleryId = 0;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // едент. галереи
        $this->_galleryId = $this->input->get('gallery_id', 0);
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param string $sql запрос SQL на вывод данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        $sql = 'SELECT `i`.*, `g`.`gallery_folder` FROM `site_gallery` `g` JOIN `site_gallery_images` `i` USING(`gallery_id`) '
             . 'WHERE `i`.`image_id`=' . (int) $this->uri->id;

        parent::dataView($sql);
    }

    /**
     * Вставка текста изображения
     * 
     * @param  integer $imageId идент. изображения
     * @return void 
     */
    protected function textInsert($imageId)
    {
        $ln = $this->input->get('ln', false);
        if (empty($ln)) return;

        $table = new GDb_Table('site_gallery_images_l', 'image_lid');
        // допустимые поля
        $fields = array('image_title', 'image_thumb_title', 'image_longdesc');
        // список доступных языков
        $langs = $this->config->getFromCms('Site', 'LANGUAGES');
        foreach ($langs as $alias => $lang) {
            // если есть язык
            if (isset($ln[$lang['id']])) {
                $item = $ln[$lang['id']];
                $data = array('image_id' => $imageId, 'language_id' => $lang['id']);
                for ($j = 0; $j < sizeof($fields); $j++) {
                    if (isset($item[$fields[$j]]))
                        $data[$fields[$j]] = $item[$fields[$j]];
                }
                if ($table->insert($data) === false)
                    throw new GSqlException();
            }
        }
    }

    /**
     * Обновление текста изображения
     * 
     * @return void
     */
    protected function textUpdate()
    {
        $ln = $this->input->get('ln', false);
        if (empty($ln)) return;

        $table = new GDb_Table('site_gallery_images_l', 'image_lid');
        // допустимые поля
        $fields = array('image_title', 'image_thumb_title', 'image_longdesc');
        // список доступных языков
        $langs = $this->config->getFromCms('Site', 'LANGUAGES');
        foreach ($langs as $alias => $lang) {
            // если есть язык
            if (isset($ln[$lang['id']])) {
                $item = $ln[$lang['id']];
                // если есть только идент.
                if (sizeof($item) == 1) continue;
                $data = array();
                for ($j = 0; $j < sizeof($fields); $j++) {
                    if (isset($item[$fields[$j]]))
                        $data[$fields[$j]] = $item[$fields[$j]];
                }
                // обновить запись
                if ($table->update($data, $item['image_lid']) === false)
                    throw new GSqlException();
            }
        }
    }

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_profile_update'], $record['image_entire_filename']);
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        // массив полей таблицы ($tableName)
        $this->fields = array('image_index', 'image_date', 'image_visible');

        parent::dataUpdate($params);
    }

    /**
     * Событие наступает после успешного обновления данных
     * 
     * @param array $data сформированные данные полученные после запроса к таблице
     * @return void
     */
    protected function dataUpdateComplete($data = array())
    {
        // соединение с базой данных (т.к. для лога ранее возможно было другое соединение)
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();

        // обновление текста изображения
        $this->textUpdate($this->_recordId);
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Событие наступает после успешного добавления данных
     * 
     * @param array $data сформированные данные полученные после запроса к таблице
     * @return void
     */
    protected function dataInsertComplete($data = array())
    {
        // соединение с базой данных (т.к. для лога ранее возможно было другое соединение)
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();

        // вставка текста изображения
        $this->textInsert($this->_recordId);
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Событие наступает после успешного удаления данных
     * 
     * @return void
     */
    protected function dataDeleteComplete()
    {
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));

    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        $query = new GDb_Query();
        // изображения статьи
        $sql = 'SELECT `i`.*, `g`.`gallery_folder` FROM `site_gallery_images` `i` JOIN `site_gallery` `g` USING(`gallery_id`) '
             . 'WHERE `i`.`image_id` IN (' . $this->uri->id. ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $path = DOCUMENT_ROOT . $this->config->getFromCms('Galleries', 'DIR');
        while (!$query->eof()) {
            $image = $query->next();
            // если есть изображение
            if ($image['image_entire_filename'])
                GFile::delete($path . $image['gallery_folder'] . '/' . $image['image_entire_filename'], false);
            // если есть эскиз
            if ($image['image_thumb_filename'])
                GFile::delete($path . $image['gallery_folder'] . '/' . $image['image_thumb_filename'], false);
        }
        // удаление изображений статьи
        $sql = 'DELETE FROM `site_gallery_images_l` WHERE `image_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_gallery_images_l') === false)
            throw new GSqlException();
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        $params['image_date'] = date('Y-m-d H:i:s');
        // если состояние формы "правка"
        if ($this->isUpdate) return;

        $query = new GDb_Query();
        // галерея
        $sql = 'SELECT * FROM `site_gallery` WHERE `gallery_id`=' . $this->_galleryId;
        if (($this->_gallery = $query->getRecord($sql)) === false)
                throw new GSqlException();

        // идент. статьи
        $params['gallery_id'] = $this->_galleryId;
        $fileImage = false;
        $cmdImage = $cmdThumb = '';

        // если нет изображения для загрузки
        if (($fileImage = $this->input->file->get('image')) === false)
            throw new GException('Adding data', $this->_['msg_file_image_empty']);
        // определение имени файла
        $tfileExt = $fileExt = strtolower(pathinfo($fileImage['name'], PATHINFO_EXTENSION));
        // идент. название файла
        if ($this->config->getFromCms('Galleries', 'GENERATE/FILE/NAME'))
            $fileId = uniqid();
        else {
            $fileId = pathinfo($fileImage['name'], PATHINFO_FILENAME);
            $fileId = GString::toUrl($fileId,  $this->config->getFromCms('Site', 'LANGUAGE/ALIAS'));
        }
        $tfileId = $fileId . '_thumb';
        // сгенерировать название файла
        $filename = $fileId . '.' . $fileExt;
        $tfilename = $tfileId . '.' . $tfileExt;

        // макрос для изменения оригинала изображения
        $cmdImage = $this->config->getFromCms('Galleries', 'IMAGE/ORIGINAL/MACRO');
        if (empty($cmdImage)) {
            $width = (int) $this->config->getFromCms('Galleries', 'IMAGE/ORIGINAL/WIDTH');
            $height = (int) $this->config->getFromCms('Galleries', 'IMAGE/ORIGINAL/HEIGHT');
            if ($width || $height)
                $cmdImage = 'resize{"width": ' . $width . ', "height": ' . $height . '}';
        } else {
            $cmdImage = stripslashes($cmdImage);
        }
        // макрос для изменения миниатюры изображения
        $cmdThumb = $this->config->getFromCms('Galleries', 'IMAGE/THUMB/MACRO');
        if (empty($cmdThumb)) {
            $width = (int) $this->config->getFromCms('Galleries', 'IMAGE/THUMB/WIDTH');
            $height = (int) $this->config->getFromCms('Galleries', 'IMAGE/THUMB/HEIGHT');
            if ($width || $height)
                $cmdThumb = 'resize{"width": ' . $width . ', "height": ' . $height . '}';
        } else {
            $cmdThumb = stripslashes($cmdThumb);
        }

        $uploadPath = DOCUMENT_ROOT . $this->config->getFromCms('Galleries', 'DIR') . $this->_gallery['gallery_folder'] . '/';
        $uploadExt = explode(',', strtolower($this->config->getFromCms('Site', 'FILES/EXT/IMAGES')));
        // проверка существования изображения
        if (file_exists($uploadPath . $filename))
            throw new GException('Error', sprintf($this->_['msg_file_exists'], $uploadPath . $filename));
        // проверка существования эскиза изображения
        if (file_exists($uploadPath . $tfilename))
            throw new GException('Error', sprintf($this->_['msg_file_exists'], $uploadPath . $tfilename));

        // загрузка изображения
        GFile::upload($fileImage, $uploadPath . $filename, $uploadExt);
        $img = GFactory::getClass('Image', 'Image', $uploadPath . $filename);

        // если есть макрос изображения
        if ($cmdImage) {
            if ($img->execute($cmdImage)) {
                // если используется водяной знак
                if ($this->config->getFromCms('Galleries', 'WATERMARK')) {
                    $img->watermark(
                        DOCUMENT_ROOT . $this->config->getFromCms('Site', 'WATERMARK/STAMP', ''),
                        $this->config->getFromCms('Galleries', 'WATERMARK/POSITION', '') 
                    );
                }
                // изменение изображения
                $img->save($uploadPath . $filename, $this->config->getFromCms('Galleries', 'IMAGE/QUALITY', 0));
                // обновляем поля изображения
                $params['image_entire_filename'] = $filename;
                $params['image_entire_filesize'] = GFile::getFileSize($uploadPath . $filename);
                $params['image_entire_resolution'] = $img->getSizeStr();
            }
        } else {
            // если используется водяной знак
            if ($this->config->getFromCms('Galleries', 'WATERMARK')) {
                $img->watermark(
                    DOCUMENT_ROOT . $this->config->getFromCms('Site', 'WATERMARK/STAMP', ''),
                    $this->config->getFromCms('Galleries', 'WATERMARK/POSITION', '') 
                );
                // изменение изображения
                $img->save($uploadPath . $filename, $this->config->getFromCms('Galleries', 'IMAGE/QUALITY', 0));
                // обновляем поля изображения
                $params['image_entire_filename'] = $filename;
                $params['image_entire_filesize'] = GFile::getFileSize($uploadPath . $filename);
                $params['image_entire_resolution'] = $img->getSizeStr();
            } else {
                // если качество изображения > 0%
                if ($this->config->getFromCms('Galleries', 'IMAGE/QUALITY', 0) > 0) {
                    // изменение изображения
                    $img->save($uploadPath . $filename, $this->config->getFromCms('Galleries', 'IMAGE/QUALITY', 0));
                    // обновляем поля изображения
                    $params['image_entire_filename'] = $filename;
                    $params['image_entire_filesize'] = GFile::getFileSize($uploadPath . $filename);
                    $params['image_entire_resolution'] = $img->getSizeStr();
                } else {
                    // обновляем поля изображения
                    $params['image_entire_filename'] = $filename;
                    $params['image_entire_filesize'] = GFile::fileSize($fileImage['size']);
                    $params['image_entire_resolution'] = $img->getSizeStr();
                }
            }
        }

        // если есть макрос миниатюры изображения
        if ($cmdThumb) {
            $img = GFactory::getClass('Image', 'Image', $uploadPath . $filename);
            if ($img->execute($cmdThumb)) {
                // создание миниатюры
                $img->save($uploadPath . $tfilename, $this->config->getFromCms('Galleries', 'IMAGE/QUALITY', 0));
                // обновляем поля миниатюры
                $params['image_thumb_filename'] = $tfilename;
                $params['image_thumb_filesize'] = GFile::getFileSize($uploadPath . $tfilename);
                $params['image_thumb_resolution'] = $img->getSizeStr();
            }
        }
    }

    /**
     * Предварительная обработка записи
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
       $path = $this->config->getFromCms('Galleries', 'DIR') . $this->_data['gallery_folder'] . '/';
        // ресурсы изображения
        $record['image_entire_url'] = $this->config->getFromCms('Site', 'HOME') . $path . $record['image_entire_filename'];
        // ресурсы эскиза
        if (empty($record['image_thumb_filename'])) {
            $record['image_thumb_url'] = '';
        } else {
            $record['image_thumb_url'] = $this->config->getFromCms('Site', 'HOME') . $path . $record['image_thumb_filename'];
        }

        return $record;
    }
}
?>