<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2013-2016 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Creating a record "Picture"',
    'title_profile_update' => 'Updating record "%s"',
    // поля формы
    // закладка "атрибуты"
    'title_tab_attributes' => 'Attributes',
    'label_image_index'    => 'Index',
    'tip_image_index'      => 
        'Serial number (for the gallery - ordering the list, for the article - the formation of a pseudonym; for each '
      . 'category of your order)',
    'label_category_name'  => 'Category',
    'tip_category_name'    => 'For each type of category images displayed differently in the text',
    'label_image_visible'  => 'Visible',
    'tip_image_visible'    => 'Show image',
    'label_image_archive'  => 'Archive',
    'tip_image_archive'    => 'Image in the archive (not available)',
    'label_image_longdesc' => 'URL',
    'label_image_title'    => 'Text',
    'tip_image_title'      => 
        'Used to sign the pictures if there is no image (attributes "alt", "title")',
    'tip_image_longdesc'   => 
        'Specifies the URL to navigate to the page where the detailed information about the image',
    'label_image_attr'     => 'Attributes',
    'tip_image_attr'       => 'Image attributes inline tag &lt;img&gt;',
    // закладка "изображение"
    'title_tab_img'           => 'Image',
    'title_fieldset_img'      => 'File (%s)',
    'text_empty'              => 'Browse ...',
    'text_btn_upload'         => 'Select',
    'title_fieldset_aimg'     => 'Image attributes',
    'label_entire_filename'   => 'File',
    'label_entire_uri'        => 'URI resource',
    'tip_entire_uri'          => 
        'A character string, which allows to identify any resource: document, image, file, service, '
      . 'email inbox, etc.',
    'label_entire_url'        => 'URL resource',
    'tip_entire_url'          => 
        'Uniform locator (determinant for the location) of the resource. It is a standardized method of recording addresses '
      . 'resource on the Internet.',
    'label_entire_resolution' => 'Resolution',
    'tip_entire_resolution'   => 'File resolution in pixels',
    'label_entire_type'       => 'Тип',
    'tip_entire_type'         => 'Image type ("JPEG", "PNG", "GIF")',
    'label_entire_filesize'   => 'Size',
    'tip_entire_filesize'     => 'File size',
    'label_date_insert'       => 'Created',
    'label_date_update'       => 'Updated',
    // закладка "эскиз"
    'title_tab_thm'      => 'Thumbnail',
    'title_fieldset_ath' => 'Thumbnail attributes',
);
?>