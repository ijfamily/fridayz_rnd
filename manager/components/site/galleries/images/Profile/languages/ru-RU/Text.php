<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2013-2016 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Создание записи "Изображение"',
    'title_profile_update' => 'Изменение записи "%s"',
    // поля формы
    // вкладка "атрибуты"
    'title_tab_attributes' => 'Атрибуты',
    'label_image_index'    => 'Порядок<hb></hb>',
    'tip_image_index'      => 'Порядковый номер в списке',
    'label_image_visible'  => 'Показывать<hb></hb>',
    'tip_image_visible'    => 'Показывать изображение',
    'label_image_longdesc' => 'URL адрес',
    'label_image_title'    => 'Название<hb></hb>',
    'tip_image_title'      => 
        'Название изображения (отображается заголовком на изображении и подставляется в атрибуты изображения &laquo;alt&raquo; и &laquo;title&raquo;")',
    'label_image_thumb_title'    => 'Название (м)<hb></hb>',
    'tip_image_thumb_title'      => 
        'Название миниатюры изображения (отображается заголовком на миниатюре изображения и подставляется в атрибуты изображения &laquo;alt&raquo; и &laquo;title&raquo;")',
    'title_bar_rename'     => 'Переименовать файл',
    // вкладка "изображение"
    'title_tab_img'           => 'Изображение',
    'title_fieldset_img'      => 'Файл изображения',
    'note_img'                => '<note>допустимые расширения файла &laquo;%s&raquo;</note>',
    'text_empty'              => 'Выберите файл ...',
    'text_btn_upload'         => 'Выбрать',
    'label_iname_create'      => 'Сгенерировать название файла',
    'label_entire_filename'   => 'Файл',
    'label_entire_uri'        => 'Русурс URI',
    'tip_entire_uri'          => 
        'Это символьная строка, позволяющая идентифицировать какой-либо ресурс: документ, изображение, файл, службу, '
      . 'ящик электронной почты и т. д.',
    'label_entire_url'        => 'Русурс URL',
    'tip_entire_url'          => 'Адрес изображения ',
//    'label_entire_resolution' => 'Разрешение',
//    'tip_entire_resolution'   => 'Разрешение файла в пикселях',
//    'label_entire_filesize'   => 'Размер',
//    'tip_entire_filesize'     => 'Размер файла',
//    'label_date_insert'       => 'Создан',
//    'label_date_update'       => 'Изменён',
    'title_fieldset_iprofile' => 'Изменение изображения после загрузки',
    // закладка "эскиз"
    'title_tab_thm'           => 'Миниатюра изображения',
    'title_fieldset_ath'      => 'Атрибуты миниатюры',
    'label_profile_image'     => 'Изменить',
    'label_thumb_create'      => 'Создать',
    'title_fieldset_tprofile' => 'Создание миниатюры из изображения',
    'text_select_image_size'  => 'выберите размер изображения',
    'text_select_thumb_size'  => 'выберите размер миниатюры',
    // сообщения
    'msg_profile_image_empty' => 'Не выбран профиль изображения для его преобразования!',
    'msg_file_image_empty'    => 'Не выбран файл изображения!',
    'msg_profile_thumb_empty' => 'Не выбран профиль изображения для создания миниатюры!',
    'msg_file_thumb_empty'    => 'Не выбран файл миниатюры!',
    'msg_file_exists'         => 'Файл изображения "%s" уже существует!',
);
?>