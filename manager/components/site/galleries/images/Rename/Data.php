<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные интерфейса изменения имени файла изображения"
 * Пакет контроллеров "Изображения галереи"
 * Группа пакетов     "Галереи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SGalleryImages_Rename
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные интерфейса изменения имени файла изображения
 * 
 * @category   Gear
 * @package    GController_SGalleryImages_Rename
 * @subpackage Data
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SGalleryImages_Rename_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($_tableName)
     *
     * @var array
     */
    public $fields = array('image_entire_filename', 'image_thumb_filename');

    /**
     * Первичный ключ таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'image_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_gallery_images';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sgalleries_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        if (!empty($record['image_thumb_filename']))
            return sprintf($this->_['profile_title_update'], $record['image_entire_filename'], $record['image_thumb_filename']);
        else
            return sprintf($this->_['profile_title_update1'], $record['image_entire_filename']);
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        $this->dataAccessUpdate();

        if ($this->input->isEmpty('PUT'))
            throw new GException('Warning', 'To execute a query, you must change data!');
        // массив полей с их соответствующими значениями
        if (empty($params))
            $params = $this->input->getBy($this->fields);
        // проверки входных данных
        $this->isDataCorrect($params);
        // проверка существования записи с одинаковыми значениями
        $this->isDataExist($params);
        $query = new GDb_Query();
        // изображение
        $sql = 'SELECT `i`.*, `g`.`gallery_folder` FROM `site_gallery` `g` JOIN `site_gallery_images` `i` USING(`gallery_id`) '
             . 'WHERE `i`.`image_id`=' . (int) $this->uri->id;
        if (($image = $query->getRecord($sql)) === false)
            throw new GSqlException();
        $path = DOCUMENT_ROOT . $this->config->getFromCms('Galleries', 'DIR') . $image['gallery_folder'] . '/';
        // переименовывание файла изображения
        if ($image['image_entire_filename'] != $this->input->get('image_entire_filename_new')) {
            $from = $path .  $image['image_entire_filename'];
            $to = $path . $this->input->get('image_entire_filename_new');
            if (rename($from, $to) === false)
                throw new GException('Error', 'Unable to move file!');
            $params['image_entire_filename'] = $this->input->get('image_entire_filename_new');
        }
        // переименовывание эскиза изображения
        if ($this->input->get('image_thumb_filename_new', false)) {
            if (!empty($image['image_thumb_filename']))
                if ($image['image_thumb_filename'] != $this->input->get('image_thumb_filename_new')) {
                    $from = $path . $image['image_thumb_filename'];
                    $to = $path . $this->input->get('image_thumb_filename_new');
                    if (rename($from, $to) === false)
                        throw new GException('Error', 'Unable to move file!');
                    $params['image_thumb_filename'] = $this->input->get('image_thumb_filename_new');
                }
        }
        // если нет данных для изменений
        if (empty($params))
            throw new GException('Warning', 'To execute a query, you must change data!');

        $table = new GDb_Table($this->tableName, $this->idProperty);
        if ($table->update($params, $this->uri->id) === false)
            throw new GSqlException();
        // запись в журнал действий пользователя
        $this->logUpdate(
            array('log_query'        => $table->query->getSQL(),
                  'log_error'        => $this->response->success === false ? $table->getError() : null,
                  'log_query_params' => $params)
        );
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        if ($this->input->get('image_entire_filename_new', false) === false)
            throw new GException('Warning', 'To execute a query, you must change data!');
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        $record['image_entire_filename_new'] = $record['image_entire_filename'];
        $record['image_thumb_filename_new'] = $record['image_thumb_filename'];

        return $record;
    }
}
?>