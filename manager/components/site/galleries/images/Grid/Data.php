<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные интерфейса списка изображений галереи"
 * Пакет контроллеров "Изображения галереи"
 * Группа пакетов     "Галереи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SGalleryImages_Grid
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('File');
Gear::controller('Grid/Data');

/**
 * Данные интерфейса списка изображений галереи
 * 
 * @category   Gear
 * @package    GController_SGalleryImages_Grid
 * @subpackage Data
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SGalleryImages_Grid_Data extends GController_Grid_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'image_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_gallery_images';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sgalleries_grid';

    /**
     * Каталог изображений галереи
     *
     * @var string
     */
    public $_path = '';

    /**
     * Идентификатор галереи
     *
     * @var integer
     */
    protected $_galleryId;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // идент. галереи
        $this->_galleryId = $this->store->get('record', 0, 'gcontroller_sgalleries_grid');
        $this->_gallery = $this->store->get('gallery');
        // каталог изображений галереи
        $this->_path = DOCUMENT_ROOT . $this->config->getFromCms('Galleries', 'DIR') . $this->_gallery['gallery_folder']. '/';
        // язык сайта по умолчанию
        $languageId = $this->config->getFromCms('Site', 'LANGUAGE/ID');
        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS `i`.*, `il`.`image_title` FROM `site_gallery_images` `i` '
          . 'LEFT JOIN `site_gallery_images_l` `il` ON `il`.`image_id`=`i`.`image_id` AND `il`.`language_id`=' . $languageId . ' '
           .'WHERE `i`.`gallery_id`=' . $this->_recordId . ' %filter ORDER BY %sort LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // индекс изображения
            'image_index' => array('type' => 'string'),
            // привью
            'image_view' => array('type' => 'string'),
            // файл изображения
            'image_entire_filename' => array('type' => 'string'),
            // разрешение изображения
            'image_entire_resolution' => array('type' => 'string'),
            // размер файла изображения
            'image_entire_filesize' => array('type' => 'string'),
            // файл эскиза изображения
            'image_thumb_filename' => array('type' => 'string'),
            // разрешение эскиза изображения
            'image_thumb_resolution' => array('type' => 'string'),
            // размер файла эскиза изображения
            'image_thumb_filesize' => array('type' => 'string'),
            // заголовок изображения
            'image_title' => array('type' => 'string'),
            // показывать изображение
            'image_visible' => array('type' => 'integer'),
        );
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        $query = new GDb_Query();
        // удаление текста изображений
        $sql = 'DELETE `il` FROM `site_gallery_images_l` `il`, `site_gallery_images` `i` '
             . 'WHERE `il`.`image_id`=`i`.`image_id` AND `i`.`gallery_id`=' . $this->_galleryId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_gallery_images_l') === false)
            throw new GSqlException();
        // удаление файлов изображений
        $sql = 'SELECT * FROM `site_gallery` WHERE `gallery_id`=' . $this->_galleryId;
        if (($gallery = $query->getRecord($sql)) === false)
            throw new GSqlException();
        if (!empty($gallery['gallery_folder'])) {
            // удаление изображений галереи
            GDir::clear(DOCUMENT_ROOT .  $this->config->getFromCms('Galleries', 'DIR') . $gallery['gallery_folder'] . '/');
        }
        // удаление изображений
        $sql = 'DELETE FROM `site_gallery_images` WHERE `gallery_id`=' . $this->_galleryId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_gallery_images') === false)
            throw new GSqlException();
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Событие наступает после успешного удаления данных
     * 
     * @return void
     */
    protected function dataDeleteComplete()
    {
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        $query = new GDb_Query();
        // изображения статьи
        $sql = 'SELECT `i`.*, `g`.`gallery_folder` FROM `site_gallery_images` `i` JOIN `site_gallery` `g` USING(`gallery_id`) '
             . 'WHERE `i`.`image_id` IN (' . $this->uri->id. ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $path = DOCUMENT_ROOT . $this->config->getFromCms('Galleries', 'DIR');
        while (!$query->eof()) {
            $image = $query->next();
            // если есть изображение
            if ($image['image_entire_filename'])
                GFile::delete($path . $image['gallery_folder'] . '/' . $image['image_entire_filename'], false);
            // если есть эскиз
            if ($image['image_thumb_filename'])
                GFile::delete($path . $image['gallery_folder'] . '/' . $image['image_thumb_filename'], false);
        }
        // удаление изображений статьи
        $sql = 'DELETE FROM `site_gallery_images_l` WHERE `image_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_gallery_images_l') === false)
            throw new GSqlException();
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        $record['image_view'] = '';
        // если файл эскиза не существует
        if (!empty($record['image_thumb_filename']))
            if (!file_exists($this->_path . $record['image_thumb_filename']))
                $record['image_thumb_filename'] = '<span style="color:#eeacaf">' . $record['image_thumb_filename'] . '</span>';
            else
                $record['image_view'] ='<div class="mn-img-preview" style="background-image: url(/' . $this->_path . $record['image_thumb_filename'] . ');"></div>';
        // если файл изображения не существует
        if (!empty($record['image_entire_filename']))
            if (!file_exists($this->_path . $record['image_entire_filename']))
                $record['image_entire_filename'] = '<span style="color:#eeacaf">' . $record['image_entire_filename'] . '</span>';

        return $record;
    }
}
?>