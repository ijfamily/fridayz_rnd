<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2013-2016 12:00:00 Gear Magic $
 */

return array(
    'title_root'          => 'Категории',
    'data_satellite_type' => array(
        array('id' => 0, 'name' => 'Не использовать'),
        array('id' => 1, 'name' => 'Сопутствующие товары'),
        array('id' => 2, 'name' => 'Из категории товаров')
    )
);
?>