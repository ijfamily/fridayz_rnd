<?php
/**
 * Gear Manager
 *
 * Контроллер         "Триггер выпадающего списка для обработки данных альбомов"
 * Пакет контроллеров "Галереи изображений"
 * Группа пакетов     "Галереи изображений"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SGalleryImages_Combo
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Combo/Trigger');

/**
 * Триггер выпадающего списка для обработки данных обложек
 * 
 * @category   Gear
 * @package    GController_SGalleryImages_Combo
 * @subpackage Trigger
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SGalleryImages_Combo_Trigger extends GController_Combo_Trigger
{
    /**
     * Вывод данных валюты
     * 
     * @return void
     */
    protected function queryProfiles()
    {
        $table = new GDb_Table('site_image_profiles', 'profile_id');
        $table->setStart($this->_start);
        $table->setLimit($this->_limit);
        $sql = 'SELECT SQL_CALC_FOUND_ROWS * '
             . 'FROM `site_image_profiles` '
             . 'WHERE ' . ($this->_query ? " `profile_name` LIKE '{$this->_query}%' " : '1 ')
             . 'ORDER BY `profile_name` ASC LIMIT %limit';
        $table->query($sql);
        $data = array();
        while (!$table->query->eof()) {
            $record = $table->query->next();
            $data[] = array(
                'id'   => $record['profile_action'],
                'name' => $record['profile_name']
            );
        }
        $this->response->set('totalCount', $table->query->getFoundRows());
        $this->response->success = true;
        $this->response->data = $data;
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $triggerName название триггера
     * @return void
     */
    protected function dataView($triggerName)
    {
        parent::dataView($triggerName);

        // имя триггера
        switch ($triggerName) {
            // профили изображений
            case 'profiles': $this->queryProfiles(); break;
        }
    }
}
?>