<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2013-2016 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Пошук у списку "Зображення"',
    // поля
    'header_image_index'              => '№',
    'header_image_title'              => 'Текст',
    'header_image_visible'            => 'Показувати',
    'header_image_archive'            => 'Архів',
    'header_image_entire_filename'    => 'Файл (з)',
    'header_image_entire_resolution'  => 'Дозвіл (з)',
    'header_image_entire_type'        => 'Тип (з)',
    'header_image_entire_filesize'    => 'Розмір (з)',
    'header_image_thumb_filename'     => 'Файл (е)',
    'header_image_thumb_resolution'   => 'Дозвіл (е)',
    'header_image_thumb_type'         => 'Тип (е)',
    'header_image_thumb_filesize'     => 'Розмір (е)',
    'header_languages_name'           => 'Мова'
);
?>