<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2013-2016 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Search in list "Images"',
    // поля
    'header_image_index'              => '№',
    'header_image_title'              => 'Text',
    'header_image_visible'            => 'Show',
    'header_image_archive'            => 'Archive',
    'header_image_entire_filename'    => 'File (i)',
    'header_image_entire_resolution'  => 'Resolution (i)',
    'header_image_entire_type'        => 'Type (i)',
    'header_image_entire_filesize'    => 'Size (i)',
    'header_image_thumb_filename'     => 'File (t)',
    'header_image_thumb_resolution'   => 'Resolution (t)',
    'header_image_thumb_type'         => 'Type (t)',
    'header_image_thumb_filesize'     => 'Size (t)',
    'header_languages_name'           => 'Language'
);
?>