<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_update' => 'Настройка фотогалереи',
    'text_btn_help'        => 'Справка',
    // поля
    'html_desc'               => '<header>Настройка компонента фотогалереи</header>'
                               . '<note>Для настройки компонента фотогалереи необходимо заполнить следующие поля:</note>',
    'title_fs_catalog'        => 'Каталог изображений галереи',
    'label_catalog'           => 'Каталог',
    'title_fs_watermaker'     => 'Водяной знак',
    'label_use'               => 'Использовать',
    'label_position'          => 'Положение',
    'label_image_width'       => 'Ширина, пкс',
    'label_image_height'      => 'Высота, пкс',
    'title_fieldset_original' => 'Размеры оригинального изображения',
    'title_fieldset_thumb'    => 'Размеры миниатюры изображения',
    'label_image_info'     => '<note>если размер загруженного изображения будет больше указанного , то изображение будет автоматически уменьшено до нужного размера, '
                        . 'иначе изображение будет с оригинальным размером. Укажите 0 по ширине и по высоте, чтобы изображение оставалось оригинальным. '
                        . 'В случаи если указан 0 по одной из сторон, то по другой стороне будет проводиться контроль оригинального изображения.</note>',
    'label_image_info1'   => '<note>если нет необходимости в создании средней копии изображения, укажите в размерах 0.</note>',
    'label_image_info2'   => '<note>если нет необходимости в создании миниатюры изображения, укажите в размерах 0.</note>',
    'label_image_quality' => 'Качество изображения, %',
    'info_image_quality'  => '<note>качество сжатия изображений в формате JPEG (от 0% до 30% - низкое, от 30% до 60% - среднее, от 60% до 80% - высокое, от 80% до 99% - очень высокое, 100% - найлучшее)</note>',
    'label_list_limit'    => 'Количество изображений списке, шт.',
    'info_list_limit'     => '<note>количество изображений в списке</note>',
    'label_generate'      => 'Генерация имени файла',
    'label_original_macro' => 'Макрос изменения оригинального изображения',
    'label_thumb_macro'    => 'Макрос изменения миниатюры изображения',
    // сообщения
    'msg_file_exist' => 'Невозможно открыть файл настроек "%s"',
    //типы
    'data_position' => array(
        array('Сверху слева', 'top-left'), array('Сверху по центру', 'top-center'), array('Сверху справа', 'top-right'),
        array('Снизу слева', 'bottom-left'), array('Снизу по центру', 'bottom-center'), array('Снизу справа', 'bottom-right')
    )
);
?>