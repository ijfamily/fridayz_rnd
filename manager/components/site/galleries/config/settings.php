<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Настройка галереи изображений"
 * Группа пакетов     "Галереи изображений"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SGalleriesConfig
 * @copyright  Copyright (c) 2013-2016 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Настройка галереи изображений
 * Описание: Настройка галереи изображений 
 * Меню: Настройка галереи изображений
 * ID класса: gcontroller_sgalleriesconfig_profile
 * Модуль: Сайт
 * Группа: Сайт/ Настройки
 * Очищать: нет
 * Статистика компонента: нет 
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске компонентов: нет
 *    В главном меню: нет
 * Ресурс
 *    Компонент: site/galleries/config/
 *    Контроллер: site/galleries/config/Profile/
 *    Интерфейс: profile/interface/
 *    Меню:
 */

return array(
    // {S}- модуль "Site" {Galleries} - пакет контроллеров "Galleries config" -> SGalleriesConfig
    'clsPrefix' => 'SGalleriesConfig',
    // использовать язык
    'language'  => 'ru-RU'
);
?>