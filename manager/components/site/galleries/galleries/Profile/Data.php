<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные для интерфейса профиля галереи"
 * Пакет контроллеров "Профиль галереи"
 * Группа пакетов     "Галереи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SGalleries_Profile
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные для интерфейса профиля статьи
 * 
 * @category   Gear
 * @package    GController_SGalleries_Profile
 * @subpackage Data
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SGalleries_Profile_Data extends GController_Profile_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * (для обработки записей таблицы)
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Массив полей таблицы ($_tableName)
     *
     * @var array
     */
    public $fields = array(
        'gallery_index', 'gallery_name', 'category_id', 'article_id', 'gallery_folder', 'published', 'published_date', 'published_time',
        'published_user', 'gallery_description'
    );

    /**
     * Первичный ключ таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'gallery_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_gallery';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sgalleries_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_profile_update'], $record['gallery_name']);
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        // состояние формы "вставка"
        if ($this->isInsert) {
            $folder = time();
            $path = DOCUMENT_ROOT .  $this->config->getFromCms('Galleries', 'DIR') . $folder . '/';
            if (!file_exists($path)) {
                if (@mkdir($path, 0700) === false)
                    throw new GException('Error', sprintf($this->_['msg_cant_mk_dir'], $path));
            }
            $params['gallery_folder'] = $folder;
        }
        // категория галереи
        if (isset($params['category_id']))
            if ($params['category_id'] == -1)
                $params['category_id'] = null;
        // дата публикации галереи
        $date = $this->input->getDate('published_date', 'Y-m-d', false);
        if (!$date)
            $params['published_date'] = date('Y-m-d');
        else
            $params['published_date'] = $date;
        // время публикации галереи
        $time = $this->input->get('published_time', false);
        if (!$time)
            $params['published_time'] = date('H:i:s');
        // кто опубликовал
        $params['published_user'] = $this->session->get('user_id');

    }

    /**
     * Событие наступает после успешного удаления данных
     * 
     * @return void
     */
    protected function dataDeleteComplete()
    {
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        Gear::library('File');

        $query = new GDb_Query();
        $sql = 'SELECT * FROM `site_gallery` WHERE `gallery_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $names = $ids = array();
        while (!$query->eof()) {
            $item = $query->next();
            if (empty($item['gallery_folder']))
                $names[] = $item['gallery_name'];
            else {
                $path = DOCUMENT_ROOT .  $this->config->getFromCms('Galleries', 'DIR') . $item['gallery_folder'] . '/';
                if (file_exists($path))
                    GDir::remove($path);
                $ids[] = $item['gallery_id'];
            }
        }
        $ids = implode(',', $ids);
        // если есть что удалять
        if ($ids) {
            // удаление локализации изображений
            $sql = 'DELETE `il` FROM `site_gallery_images_l` `il`, `site_gallery_images` `l` '
                 . 'WHERE `il`.`image_id`=`l`.`image_id` AND `l`.`gallery_id` IN (' . $ids . ')';
            if ($query->execute($sql) === false)
                throw new GSqlException();
            // удаление изображений галереи
            $sql = 'DELETE FROM `site_gallery_images` WHERE `gallery_id` IN (' . $ids . ')';
            if ($query->execute($sql) === false)
                throw new GSqlException();
        }
        // если были ошибки
        if ($names)
            throw new GException('Error', sprintf($this->_['msg_cant_delete'], implode(',', $names)));
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function recordPreprocessing($record)
    {
        $setTo = array();
        $query = new GDb_Query();
        // если выбрана статья
        if (!empty($record['article_id'])) {
            // язык сайта по умолчанию
            $languageId = $this->config->getFromCms('Site', 'LANGUAGE/ID');
            // статьи
            $sql = 'SELECT `page_header` FROM `site_pages` WHERE `language_id`=' . $languageId .' AND `article_id`=' . $record['article_id'];
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            if (!empty($rec))
                $setTo[] = array('xtype' => 'combo', 'id' => 'fldArticleId', 'value' => $rec['page_header']);
        }
        // категории галереи
        if (!empty($record['category_id'])) {
            $sql = 'SELECT * FROM `site_categories` WHERE `category_id`=' . $record['category_id'];
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            if (!empty($rec))
                $setTo[] = array('xtype' => 'mn-field-combo', 'id' => 'fldCategory', 'text' => $rec['category_name'], 'value' => $record['category_id']);
            unset($record['category_id']);
        }

        // установка полей
        if ($setTo)
            $this->response->add('setTo', $setTo);

        return $record;
    }
}
?>