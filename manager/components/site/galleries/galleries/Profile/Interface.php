<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля галереи"
 * Пакет контроллеров "Профиль галереи"
 * Группа пакетов     "Галереи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SGalleries_Profile
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля галереи
 * 
 * @category   Gear
 * @package    GController_SGalleries_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SGalleries_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        $data = array('max' => 1);

        parent::getDataInterface();

        $query = new GDb_Query();
        // если состояние формы "insert"
        if ($this->isInsert) {
            $sql = 'SELECT MAX(`gallery_index`) `max` FROM `site_gallery`';
            if (($record = $query->getRecord($sql)) === false)
                throw new GSqlException();
            $data['max'] = $record['max'] + 1;
            return $data;
        }

        return $data;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'titleEllipsis' => 45,
                  'gridId'        => 'gcontroller_sgalleries_grid',
                  'width'         => 470,
                  'autoHeight'    => true,
                  'stateful'      => false,
                  'buttonsAdd'    => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-site-galleries',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype'      => 'spinnerfield',
                  'itemCls'    => 'mn-form-item-quiet',
                  'fieldLabel' => $this->_['label_gallery_index'],
                  'labelTip'   => $this->_['tip_gallery_index'],
                  'name'       => 'gallery_index',
                  'value'      => $data['max'],
                  'width'      => 70,
                  'allowBlank' => true,
                  'emptyText'  => 1),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_gallery_name'],
                  'labelTip'   => $this->_['tip_gallery_name'],
                  'name'       => 'gallery_name',
                  'maxLength'  => 255,
                  'anchor'     => '100%',
                  'allowBlank' => false),
            array('xtype'      => 'textarea',
                  'fieldLabel' => $this->_['label_gallery_desc'],
                  'itemCls'    => 'mn-form-item-quiet',
                  'name'       => 'gallery_description',
                  'anchor'     => '100%',
                  'height'     => 70,
                  'allowBlank' => true),
            array('xtype'      => 'fieldset',
                  'anchor'     => '100%',
                  'title'      => $this->_['title_fieldset_public'],
                  'autoHeight' => true,
                  'layout'     => 'column',
                  'cls'        => 'mn-container-clean',
                  'items'      => array(
                      array('layout'     => 'form',
                            'width'      => 185,
                            'labelWidth' => 62,
                            'items'      => array(
                                array('xtype'      => 'datefield',
                                      'itemCls'    => 'mn-form-item-quiet',
                                      'fieldLabel' => $this->_['label_published_date'],
                                      'format'     => 'd-m-Y',
                                      'name'       => 'published_date',
                                      'checkDirty' => false,
                                      'width'      => 95,
                                      'allowBlank' => true)
                            )
                      ),
                      array('layout'     => 'form',
                            'labelWidth' => 55,
                            'items'      => array(
                                array('xtype'      => 'textfield',
                                      'itemCls'    => 'mn-form-item-quiet',
                                      'fieldLabel' => $this->_['label_published_time'],
                                      'name'       => 'published_time',
                                      'checkDirty' => false,
                                      'maxLength'  => 8,
                                      'width'      => 70,
                                      'allowBlank' => true)
                            )
                      )
                  )
            ),
            array('xtype'      => 'mn-field-combo',
                  'itemCls'    => 'mn-form-item-quiet',
                  'fieldLabel' => $this->_['label_article_note'],
                  'labelTip'   => $this->_['tip_article_note'],
                  'id'         => 'fldArticleId',
                  'name'       => 'article_id',
                  'checkDirty' => false,
                  'editable'   => true,
                  'anchor'     => '100%',
                  'hiddenName' => 'article_id',
                  'allowBlank' => true,
                  'store'      => array(
                      'xtype' => 'jsonstore',
                      'url'   => $this->componentUrl . 'combo/trigger/?name=articles'
                  )
            ),
            array('xtype'      => 'mn-field-combo-tree',
                  'itemCls'    => 'mn-form-item-quiet',
                  'id'         => 'fldCategory',
                  'fieldLabel' => $this->_['label_category_name'],
                  'labelTip'   => $this->_['tip_category_name'],
                  'anchor'     => '100%',
                  'name'       => 'category_id',
                  'hiddenName' => 'category_id',
                  'resetable'  => false,
                  'treeWidth'  => 400,
                  'treeRoot'   => array('id' => 1, 'expanded' => true, 'expandable' => true),
                  'allowBlank' => true,
                  'store'      => array(
                      'xtype' => 'jsonstore',
                      'url'   => $this->componentUrl . 'combo/nodes/'
                   )
            ),
            array('xtype'      => 'mn-field-chbox',
                  'itemCls'    => 'mn-form-item-quiet',
                  'fieldLabel' => $this->_['label_published'],
                  'labelTip'   => $this->_['tip_published'],
                  'default'    => $this->isUpdate ? null : 1,
                  'name'       => 'published')
            );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($items);
        $form->url = $this->componentUrl . 'profile/';
        $form->labelWidth = 95;

        parent::getInterface();
    }
}
?>