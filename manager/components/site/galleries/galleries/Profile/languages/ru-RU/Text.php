<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2013-2016 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Создание записи "Галерея"',
    'title_profile_update' => 'Изменение записи "%s"',
    'text_btn_help'        => 'Справка',
    // поля формы
    'label_gallery_index' => 'Порядок',
    'tip_gallery_index'   => 'Порядковый номер',
    'label_gallery_name'  => 'Название',
    'tip_gallery_name'    => 'Отображается только в системе администрирования',
    'label_article_note'  => 'Статья',
    'tip_article_note'    => 'Статья в которой будет размещена галерея',
    'label_gallery_desc'  => 'Описание',
    'label_category_name' => 'Категория<hb></hb>',
    'tip_category_name'   => 'Категорию необходимо указывать если нужен список галерей (фотоальбомов)',
    'title_fieldset_public' => 'Дата публикации галереи',
    'label_published_date'  => 'Дата',
    'label_published_time'  => 'Время',
    'label_published'       => 'Опубликовать<hb></hb>',
    'tip_published'         => 'Опубликовать галерею (фотоальбом) в выбранной категории',
    // сообщения
    'msg_cant_mk_dir' => 'Невозможно создать каталог "%s"',
    'msg_cant_delete' => 'Невозможно удалить галерею "%s", т.к. галерея не имеет каталога изображений',
);
?>