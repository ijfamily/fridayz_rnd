<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка галерей сайта"
 * Пакет контроллеров "Галереи"
 * Группа пакетов     "Галереи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SGalleries_Grid
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('String');
Gear::library('File');
Gear::controller('Grid/Data');

/**
 * Данные списка галерей сайта
 * 
 * @category   Gear
 * @package    GController_SGalleries_Grid
 * @subpackage Data
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SGalleries_Grid_Data extends GController_Grid_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * (для обработки записей таблицы)
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'gallery_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_gallery';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // язык сайта по умолчанию
        $languageId = $this->config->getFromCms('Site', 'LANGUAGE/ID');
        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS `g`.*, `i`.`images_count`, `p`.`page_header` '
          . 'FROM `site_gallery` `g` '
            // join `site_gallery_images`
          . 'LEFT JOIN (SELECT `gallery_id`, COUNT(`gallery_id`) `images_count` '
          . 'FROM `site_gallery_images` GROUP BY `gallery_id`) `i` USING (`gallery_id`) '
            // join `site_articles`
          . 'LEFT JOIN `site_articles` `a` USING (`article_id`) '
          . 'LEFT JOIN `site_pages` `p` ON `p`.`article_id`=`a`.`article_id` AND `p`.`language_id`=' . $languageId
          . ' WHERE 1 %filter ORDER BY %sort LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // идент. галереи
            'gallery_id' => array('type' => 'integer'),
            // дата публикации
            'published_date' => array('type' => 'string'),
            'published_time' => array('type' => 'string'),
            'published_user' => array('type' => 'integer'),
            // заметка статьи
            'page_header' => array('type' => 'string'),
            // порядковый номер галереи
            'gallery_index' => array('type' => 'integer'),
            // название
            'gallery_name' => array('type' => 'string'),
            // изображений в галереи
            'images_count' => array('type' => 'integer'),
            // каталог
            'gallery_folder' => array('type' => 'string'),
            // галерея опубликована
            'published' => array('type' => 'integer'),
        );
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        $query = new GDb_Query();
        // удаление изображений галерей
        GDir::clear(DOCUMENT_ROOT .  $this->config->getFromCms('Galleries', 'DIR'), array(), true);
        // удаление записей таблицы "site_gallery_images" (изображения галереи)
        if ($query->clear('site_gallery_images') === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_gallery_images') === false)
            throw new GSqlException();
        // удаление записей таблицы "site_gallery_images_l" (текст изображений)
        if ($query->clear('site_gallery_images_l') === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_gallery_images_l') === false)
            throw new GSqlException();
        // удаление записей таблицы "site_gallery" (галерея)
        if ($query->clear('site_gallery') === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_gallery') === false)
            throw new GSqlException();
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Событие наступает после успешного удаления данных
     * 
     * @return void
     */
    protected function dataDeleteComplete()
    {
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        Gear::library('File');

        $query = new GDb_Query();
        $sql = 'SELECT * FROM `site_gallery` WHERE `gallery_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $errors = $ids = array();
        while (!$query->eof()) {
            $item = $query->next();
            if (empty($item['gallery_folder']))
                $errors[] = $item['gallery_name'];
            else {
                $path = DOCUMENT_ROOT .  $this->config->getFromCms('Galleries', 'DIR') . $item['gallery_folder'] . '/';
                if (file_exists($path))
                    GDir::remove($path);
                $ids[] = $item['gallery_id'];
            }
        }
        $ids = implode(',', $ids);
        // если есть что удалять
        if ($ids) {
            // если есть ошибки удаляем что есть
            if ($errors) {
                // удаление записей таблицы "site_gallery" (галерея)
                $sql = 'DELETE FROM `site_gallery` WHERE `gallery_id` IN (' . $ids . ')';
                if ($query->execute($sql) === false)
                    throw new GSqlException();
            }
            // удаление записей таблицы "site_gallery_images_l" (текст изображений)
            $sql = 'DELETE `il` FROM `site_gallery_images_l` `il`, `site_gallery_images` `l` '
                 . 'WHERE `il`.`image_id`=`l`.`image_id` AND `l`.`gallery_id` IN (' . $ids . ')';
            if ($query->execute($sql) === false)
                throw new GSqlException();
            // удаление записей таблицы "site_gallery_images" (изображения галереи)
            $sql = 'DELETE FROM `site_gallery_images` WHERE `gallery_id` IN (' . $ids . ')';
            if ($query->execute($sql) === false)
                throw new GSqlException();
        }
        // если были ошибки
        if ($errors)
            throw new GException('Error', sprintf($this->_['msg_cant_delete'], GString::ellipsis(implode(',', $errors), 200)));
    }
}
?>