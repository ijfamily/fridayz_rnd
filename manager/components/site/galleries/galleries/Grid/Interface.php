<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка галерей сайта"
 * Пакет контроллеров "Галереи"
 * Группа пакетов     "Галереи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SGalleries_Grid
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс списка галерей сайта
 * 
 * @category   Gear
 * @package    GController_SGalleries_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SGalleries_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * (для обработки записей таблицы)
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'gallery_id';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'gallery_index';

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // заметка статьи
            array('name' => 'page_header', 'type' => 'string'),
            // дата публикации
            array('name' => 'published_date', 'type' => 'string'),
            array('name' => 'published_time', 'type' => 'string'),
            array('name' => 'published_user', 'type' => 'integer'),
            // порядковый номер галереи
            array('name' => 'gallery_index', 'type' => 'integer'),
            // название
            array('name' => 'gallery_name', 'type' => 'string'),
            // изображений в галереи
            array('name' => 'images_count', 'type' => 'integer'),
            // каталог
            array('name' => 'gallery_folder', 'type' => 'string'),
            // галерея опубликована
            array('name' => 'published', 'type' => 'integer')
        );

        $settings = $this->session->get('user/settings');
        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $this->columns = array(
            array('xtype'     => 'expandercolumn',
                  'url'       => $this->componentUrl . 'grid/expand/',
                  'typeCt'    => 'row'),
            array('xtype'     => 'numbercolumn'),
            array('xtype'     => 'rowmenu'),
            array('xtype'     => 'datetimecolumn',
                  'dataIndex' => 'published_date',
                  'timeIndex' => 'published_time',
                  'userIndex' => 'published_user',
                  'frmDate'   => $settings['format/date'],
                  'frmTime'   => $settings['format/time'],
                  'header'    => $this->_['header_published'],
                  'isSystem'  => true,
                  'urlQuery'  => '?state=info',
                  'url'       => 'administration/users/contingent/' . ROUTER_DELIMITER . 'profile/interface/',
                  'width'     => 130,
                  'sortable'  => true,
                  'filter'    => array('type' => 'date')),
            array('dataIndex' => 'gallery_index',
                  'header'    => $this->_['header_gallery_index'],
                  'tooltip'   => $this->_['tooltip_gallery_index'],
                  'width'     => 65,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'gallery_name',
                  'header'    => '<em class="mn-grid-hd-details"></em>'. $this->_['header_gallery_name'],
                  'width'     => 200,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'gallery_description',
                  'header'    => $this->_['header_gallery_description'],
                  'tooltip'   => $this->_['tooltip_gallery_description'],
                  'width'     => 150,
                  'hidden'     => true,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'page_header',
                  'header'    => $this->_['header_article_note'],
                  'tooltip'   => $this->_['tooltip_article_note'],
                  'width'     => 150,
                  'hidden'    => true,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'gallery_folder',
                  'header'    => '<img align="absmiddle" src="' . $this->resourcePath . 'icon-hd-folder.png"> ' . $this->_['header_gallery_folder'],
                  'tooltip'   => $this->_['tooltip_gallery_folder'],
                  'hidden'    => true,
                  'width'     => 85,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'images_count',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-image.png" align="absmiddle">',
                  'tooltip'   => $this->_['tooltip_images_count'],
                  'align'     => 'center',
                  'width'     => 45,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('xtype'     => 'booleancolumn',
                  'align'     => 'center',
                  'cls'       => 'mn-bg-color-gray2',
                  'dataIndex' => 'published',
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-visible.png" align="absmiddle">',
                  'width'     => 40,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean'))
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_grid'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_grid'],
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'profileUrl'    => $this->componentUrl . 'profile/')
        );

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "edit" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "добавить" (ExtJS class "Manager.button.InsertData")
        $group->items->add(array('xtype' => 'mn-btn-insert-data', 'gridId' => $this->classId));
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' => $this->classId));
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'      => 'mn-btn-clear-data',
                  'msgConfirm' => $this->_['msg_btn_clear'],
                  'gridId'     => $this->classId,
                  'isN'        => true)
        );
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка настройка" (ExtJS class "Manager.button.Widget")
        $group->items->add(
            array('xtype'   => 'mn-btn-widget',
                  'width'   => 60,
                  'text'    => $this->_['text_btn_config'],
                  'tooltip' => $this->_['tooltip_btn_config'],
                  'icon'    => $this->resourcePath . 'icon-btn-config.png',
                  'url'     => $this->componentUri . '../config/' . ROUTER_DELIMITER . 'profile/interface/1111')
        );
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = 'component-site-galleries';
        $this->_cmp->tbar->items->add($group);

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $cellInfo =
            '<div class="mn-grid-cell-tooltip-tl">{gallery_name}</div>'
          . '<div class="mn-grid-cell-tooltip">'
          . '<em>' . $this->_['header_gallery_index'] . '</em>: <b>{gallery_index}</b><br>'
          . '<em>' . $this->_['tooltip_images_count'] . '</em>: <b>{image_count}</b><br>'
          . '<em>' . $this->_['header_article_note'] . '</em>: <b>{page_header}</b><br>'
          . '</div>';
        $this->_cmp->cellTips = array(
            array('field' => 'gallery_name', 'tpl' => $cellInfo),
            array('field' => 'page_header', 'tpl' => '{page_header}'),
            array('field' => 'gallery_folder', 'tpl' => '<tpl if="gallery_folder">' . $this->config->getFromCms('Galleries', 'DIR') . '{gallery_folder}/</tpl>')
        );

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $items = array(
            array('text'    => $this->_['rowmenu_edit'],
                  'iconCls' => 'icon-form-edit',
                  'url'     => $this->componentUrl . 'profile/interface/'),
            array('xtype'   => 'menuseparator'),
            array('text'    => $this->_['rowmenu_image'],
                  'icon'    => $this->resourcePath . 'icon-hd-image.png',
                  'url'     => $this->componentUrl . '../../images/' . ROUTER_DELIMITER . 'grid/interface/'),
            array('text'    => $this->_['rowmenu_settings'],
                  'icon'    => $this->resourcePath . 'icon-hd-image.png',
                  'url'     => $this->componentUrl . '../../images/' . ROUTER_DELIMITER . 'grid/interface/')
        );
        $this->_cmp->rowMenu = array('xtype' => 'mn-grid-rowmenu', 'items' => $items);

        parent::getInterface();
    }
}
?>