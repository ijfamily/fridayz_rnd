<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2013-2016 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'    => 'Фотогалереи',
    'rowmenu_edit'  => 'Редактировать',
    'rowmenu_image' => 'Изображения',
    'rowmenu_settings' => 'Персональные настройки',
    'tooltip_grid'  => 'изображения галерей на страницах сайта',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">("Фотогалереи", "Изображения")</span> ?',
    'text_btn_config'          => 'Настройка',
    'tooltip_btn_config'       => 'Настройка фотогалереи',
    // столбцы
    'header_published'            => 'Дата публикации',
    'header_gallery_index'        => '№',
    'tooltip_gallery_index'       => 'Порядковый номер галереи',
    'header_article_note'         => 'Cтатья',
    'tooltip_article_note'        => 'Название статьи где размещена галерея',
    'header_gallery_name'         => 'Название',
    'header_gallery_description'  => 'Описание',
    'tooltip_gallery_description' => 'Описание альбома',
    'header_images_count'         => 'Изображений',
    'tooltip_images_count'        => 'Количество изображений в галереях',
    'tooltip_published'           => 'Публикация галереи',
    'header_gallery_folder'       => 'Каталог',
    'tooltip_gallery_folder'      => 'Название каталога, где находятся изображения',
    // развёрнутая запись
    'label_image_title' => 'Изображение',
    'label_image_count' => 'Изображений: <b>%s</b>',
    // сообщения
    'msg_cant_delete' => 'Невозможно удалить галерею "%s", т.к. галерея не имеет каталога изображений',
);
?>