<?php
/**
 * Gear Manager
 *
 * Контроллер         "Развёрнутая запись галереи"
 * Пакет контроллеров "Галереи"
 * Группа пакетов     "Галереи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SGalleries_Grid
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Expand');

/**
 * Развёрнутая запись галереи
 * 
 * @category   Gear
 * @package    GController_SGalleries_Grid
 * @subpackage Expand
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SGalleries_Grid_Expand extends GController_Grid_Expand
{
    /**
     * Загаловок в списке
     * 
     * @return string
     */
    protected $_title = '';

    /**
     * Вывод данных в интерфейс
     * 
     * @return void
     */
    protected function dataExpand()
    {
        parent::dataAccessView();

        $data = $this->getAttributes();
        $data .= $this->getImages();
        $this->response->data = $data;
    }

    /**
     * Возращает атрибуты записи
     * 
     * @return string
     */
    protected function getAttributes()
    {
        $data = '';
        $query = new GDb_Query();
        $sql = 'SELECT * FROM `site_gallery` WHERE `gallery_id`=' . $this->uri->id;
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        $this->_gallery = $rec;
        $this->_title = $rec['gallery_name'];

        return '';
    }

    /**
     * Возращает изображения записи
     * 
     * @return string
     */
    protected function getImages()
    {
        $data = '';
        // язык сайта по умолчанию
        $languageId = $this->config->getFromCms('Site', 'LANGUAGE/ID');
        $query = new GDb_Query();
        $sql = 'SELECT `l`.*, `gi`.* FROM `site_gallery_images` `gi` '
             . 'LEFT JOIN `site_gallery_images_l` `l` ON `l`.`image_id`=`gi`.`image_id` AND `l`.`language_id`=' . $languageId
             . ' WHERE `gi`.`gallery_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        while (!$query->eof()) {
            $img = $query->next();
            $entireFilename = $img['image_entire_filename'];
            $thumbFilename = $img['image_thumb_filename'];
            if (empty($thumbFilename))
                $thumbFilename = $entireFilename;
            $path = $this->config->getFromCms('Galleries', 'DIR') . $this->_gallery['gallery_folder'] . '/';
            $data .= '<span mn:bar="download,picture,widget,rename'
                   . '" mn:text="' . ($img['image_title'] ? $img['image_title'] : $this->_['label_image_title'] . ' ' . $img['image_index'])
                   . (!$img['image_visible'] ? '" mn:hidden="true' : '')
                   . '" mn:rename="site/galleries/images/' . ROUTER_DELIMITER . 'rename/interface/' . $img['image_id']
                   . '" mn:widget="site/galleries/images/' . ROUTER_DELIMITER . 'profile/interface/' . $img['image_id'] 
                   . '" mn:image-thumb="/' . $path . $thumbFilename
                   . '" mn:image-full="/' . $path . $entireFilename
                   . '" class="mn-img mn-img-xxl fl"></span>';
        }
        if ($data) {
            $data = '<div class="mn-row-header">' . sprintf($this->_['label_image_count'], $query->getCountRecords()) . '</div>'
                  . '<div class="mn-row-body border">' . $data . '<div class="wrap"></div></div>';
        }

        return $data;
    }


}
?>