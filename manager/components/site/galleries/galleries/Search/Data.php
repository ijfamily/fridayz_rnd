<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск галерей сайта"
 * Пакет контроллеров "Галереи"
 * Группа пакетов     "Галереи"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SGalleries_Search
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2015 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Data.php 2015-07-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск галерей сайта
 * 
 * @category   Gear
 * @package    GController_SGalleries_Search
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2015 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Data.php 2015-07-01 12:00:00 Gear Magic $
 */
final class GController_SGalleries_Search_Data extends GController_Search_Data
{
    /**
     * дентификатор компонента (списка) к которому применяется поиск
     *
     * @var string
     */
    public $gridId = 'gcontroller_sgalleries_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sgalleries_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор
     * Используется для инициализации атрибутов контроллер не переданного в конструктор
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'gallery_index', 'label' => $this->_['header_gallery_index']),
            array('dataIndex' => 'gallery_name', 'label' => $this->_['header_gallery_name']),
            array('dataIndex' => 'gallery_description', 'label' => $this->_['header_gallery_description']),
            array('dataIndex' => 'page_header', 'label' => $this->_['header_page_header']),
            array('dataIndex' => 'gallery_folder', 'label' => $this->_['header_gallery_folder']),
            array('dataIndex' => 'images_count', 'label' => $this->_['header_images_count'])
        );
    }
}
?>