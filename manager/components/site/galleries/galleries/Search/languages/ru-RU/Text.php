<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2013-2016 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Поиск в списке "Галерея"',
    // поля формы
    'header_gallery_index'       => '№',
    'header_gallery_name'        => 'Название',
    'header_gallery_description' => 'Описание',
    'header_page_header'    => 'Статья',
    'header_gallery_folder' => 'Каталог',
    'header_images_count'   => 'Количество изображений'
);
?>