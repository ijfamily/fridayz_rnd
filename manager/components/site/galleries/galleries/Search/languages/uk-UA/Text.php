<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2013-2016 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Пошук у списку "Галерея"',
    // поля формы
    'header_gallery_index'       => '№',
    'header_gallery_name'        => 'Назва',
    'header_article_note'        => 'Стаття',
    'header_gallery_description' => 'Опис',
    'header_images_count'        => 'Зображень'
);
?>