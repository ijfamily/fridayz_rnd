<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные интерфейса списка пунктов меню"
 * Пакет контроллеров "Список пунктов меню"
 * Группа пакетов     "Меню сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    GController_SMenuItems_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные интерфейса спискапунктов меню
 * 
 * @category   Gear
 * @package    GController_SMenuItems_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SMenuItems_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'item_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_menu_items';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_smenu_grid';

    /**
     * Идентификатор меню
     *
     * @var integer
     */
    protected $_menuId;

    /**
     * Домены
     *
     * @var array
     */
    public $domains = array();

    /**
     * Управления сайтами на других доменах
     *
     * @var boolean
     */
    public $isOutControl = false;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // управление сайтами на других доменах
        $this->isOutControl = $this->config->getFromCms('Site', 'OUTCONTROL');
        if ($this->isOutControl)
            $this->domains = $this->config->getFromCms('Domains', 'indexes');
        $this->domains[0] = array($_SERVER['SERVER_NAME'], $this->config->getFromCms('Site', 'NAME'));

        $this->_menuId = $this->store->get('record', 0, 'gcontroller_smenuitems_grid');
        $languageId = $this->config->getFromCms('Site', 'LANGUAGE/ID');
        // SQL запрос
        /*
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS `i`.*, `pi`.`parent_name`, `il`.*, `a`.`page_header` '
          . 'FROM `site_menu_items` `i` '
            // языки
          . 'JOIN `site_menu_items_l` `il` ON `il`.`item_id`=`i`.`item_id` AND `il`.`language_id`=' . $languageId
            // предок
          . ' LEFT JOIN (SELECT `il`.`item_name` `parent_name`, `i`.`item_id` FROM `site_menu_items` `i` JOIN `site_menu_items_l` `il` ON `il`.`item_id`=`i`.`item_id` AND `il`.`language_id`=' . $languageId . ') `pi` ON `i`.`item_parent_id`=`pi`.`item_id` '
            // статьи
          . ' LEFT JOIN (SELECT `p`.`page_header`, `p`.`article_id` '
          . 'FROM `site_articles` `a` JOIN `site_pages` `p` ON `a`.`article_id`=`p`.`article_id` AND `p`.`language_id`=' . $languageId . ') `a` USING (`article_id`) '
          . 'WHERE `i`.`menu_id`=' . $this->_menuId;
        */
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS `i`.*, `pi`.`parent_name`, `il`.* FROM `site_menu_items` `i` '
            // языки
          . 'JOIN `site_menu_items_l` `il` ON `il`.`item_id`=`i`.`item_id` AND `il`.`language_id`=' . $languageId
            // предок
          . ' LEFT JOIN (SELECT `il`.`item_name` `parent_name`, `i`.`item_id` FROM `site_menu_items` `i` JOIN `site_menu_items_l` `il` ON `il`.`item_id`=`i`.`item_id` AND `il`.`language_id`=' . $languageId . ') `pi` ON `i`.`item_parent_id`=`pi`.`item_id` '
          . 'WHERE `i`.`menu_id`=' . $this->_menuId;
        // фильтр из панели инструментов
        if ($this->isOutControl) {
            $filter = $this->store->get('tlbFilter', false);
            if ($filter) {
                $domainId = (int) $filter['domain'];
                if ($domainId >= 0)
                    $this->sql .= ' AND `i`.`domain_id`=' . $domainId;
            }
        }
        $this->sql .= ' %filter ORDER BY %sort LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // название сайта
            'domain_id' => array('type' => 'string'),
            // индекс меню
            'item_index' => array('type' => 'string'),
            // название меню
            'item_name' => array('type' => 'string'),
            // заметка статьи
            'page_header' => array('type' => 'string'),
            // url ссылки
            'tooltip_url' => array('type' => 'string'),
            'item_url' => array('type' => 'string'),
            'goto_url' => array('type' => 'string'),
            // предок меню
            'parent_name' => array('type' => 'string'),
            // видимость на сайте
            'item_visible' => array('type' => 'integer')
        );
    }

    /**
     * Событие наступает после успешного удаления данных
     * 
     * @return void
     */
    protected function dataDeleteComplete()
    {
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        // выбранное меню
        $query = new GDb_Query();
        // удаление записей таблицы "site_menu_items_l"
        $sql = 'DELETE `l` FROM `site_menu_items_l` `l`, `site_menu_items` `i` '
             . 'WHERE `l`.`item_id`=`i`.`item_id` AND `i`.`menu_id` IN (' . $this->_menuId . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        // удаление записей таблицы "site_menu_items"
        $sql = 'DELETE FROM `site_menu_items` WHERE `menu_id` IN (' . $this->_menuId . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        $query = new GDb_Query();
        // удаление пунктов меню "site_menu_items_l"
        $sql = 'DELETE FROM `site_menu_items_l` WHERE `item_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_menu_items_l') === false)
            throw new GSqlException();
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        $domain = $_SERVER['SERVER_NAME'];
        // сайты на других доменах
        if (isset($this->domains[$record['domain_id']])) {
            $domain = $this->domains[$record['domain_id']][0];
            $record['domain_id'] = $this->domains[$record['domain_id']][1];
        } else
            $record['domain_id'] = '';

        // подсказки для ЧПУ URL пунтка меню
        $url = 'http://' . $domain . '/';
        if ($record['item_url'] != '/')
            $url .= $record['item_url'];
        $record['tooltip_url'] = $url;
        if ($record['domain_id'])
            $record['goto_url'] = '<a href="'. $url . '" target="_blank" title="' . $this->_['tooltip_goto_url'] . '"><img src="' . $this->resourcePath . 'icon-goto.png"></a>';
        else
            $record['goto_url'] = '';

        return $record;
    }
}
?>