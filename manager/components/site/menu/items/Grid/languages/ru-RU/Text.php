<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Пункты меню',
    'tooltip_grid' => 'список пунктов меню "%s"',
    'rowmenu_edit' => 'Редактировать',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">("Меню сайта")</span> ?',
    'label_domain'             => 'Сайт',
    // столбцы
    'header_site_name'    => 'Сайт',
    'header_item_index'   => '№',
    'tooltip_item_index'  => 'Пордяковый номер статьи (для списка)',
    'header_item_name'    => 'Название',
    'header_pitem_name'   => 'Название (предка)',
    'tooltip_pitem_name'  => 'Название &quot;предка&quot; меню',
    'header_page_header'  => 'Статья',
    'tooltip_page_header' => 'Заметка статьи',
    'header_item_title'   => 'Подсказка',
    'tooltip_item_title'  => 'Всплывающая подсказка к тексту ссылки',
    'tooltip_goto_url'    => 'Просмотр страницы сайта',
    'header_item_url'     => 'ЧПУ URL адрес',
    'tooltip_item_url'    => 'ЧПУ URL адрес страницы',
    'tooltip_item_visible' => 'Видимость на сайте'
);
?>