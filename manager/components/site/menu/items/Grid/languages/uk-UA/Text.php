<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'        => 'Документи статті "%s"',
    'rowmenu_edit'      => 'Редагувати',
    'rowmenu_file'      => 'Перейменувати',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Стовпці',
    'title_buttongroup_filter' => 'Фільтр',
    'msg_btn_clear'            => 'Ви дійсно бажаєте видалити всі записи <span class="mn-msg-delete">("Документи")</span> ?',
    // столбцы
    'header_document_index'     => '№',
    'tooltip_document_index'    => 'Порядковий номер документа',
    'header_document_alias'     => 'Псевдоним',
    'tooltip_document_alias'    => 'Псевдоним документа у статті',
    'header_document_filename'  => 'Файл (д)',
    'tooltip_document_filename' => 'Файл документа',
    'header_document_original'  => 'Файл (з)',
    'tooltip_document_original' => 'Файл завантаженого документа',
    'header_document_type'      => 'Тип',
    'tooltip_document_type'     => 'Тип файла',
    'header_document_download'  => 'Завантажень',
    'header_document_filesize'  => 'Розмір',
    'tooltip_document_filesize' => 'Розмір файла документа',
    'header_document_title'     => 'Загаловок',
    'tooltip_document_title'    => 'Текст для опису документа',
    'header_document_visible'   => 'Показувати',
    'tooltip_document_visible'  => 'Показувати документ',
    'header_document_archive'   => 'Архів',
    'tooltip_document_archive'  => 'Документ в архіві',
    'header_languages_name'     => 'Мова'
);
?>