<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Search in the list "Documents"',
    // поля
    'header_document_index'     => '№',
    'header_document_filename'  => 'File (d)',
    'header_document_type'      => 'Type',
    'header_document_filesize'  => 'Size',
    'header_document_title'     => 'Title',
    'header_document_visible'   => 'Visible',
    'header_document_archive'   => 'Archive',
    'header_languages_name'     => 'Language'
);
?>