<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Пошук у списку "Документи"',
    // поля
    'header_document_index'     => '№',
    'header_document_filename'  => 'Файл (д)',
    'header_document_type'      => 'Тип',
    'header_document_filesize'  => 'Розмір',
    'header_document_title'     => 'Загаловок',
    'header_document_visible'   => 'Показувати',
    'header_document_archive'   => 'Архів',
    'header_languages_name'     => 'Мова'
);
?>