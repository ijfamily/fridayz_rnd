<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные интерфейса пункта меню"
 * Пакет контроллеров "Профиль пункта меню"
 * Группа пакетов     "Меню сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    GController_SMenuItems_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные интерфейса пункта меню
 * 
 * @category   Gear
 * @package    GController_SMenuItems_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SMenuItems_Profile_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array(
        'domain_id', 'item_parent_id', 'article_id', 'menu_id', 'item_index', 'item_visible'
    );

    /**
     * Первичный ключ таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'item_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_menu_items';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_smenu_grid';

    /**
     * Идентификатор меню
     *
     * @var integer
     */
    protected $_menuId;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // если едент. меню задаётся из вне
        if ($menuId = $this->input->get('menu_id', false))
            $this->_menuId = $menuId;
        // если едент. меню из списка
        else
            $this->_menuId =$this->store->get('record', 0, 'gcontroller_smenuitems_grid');
    }

    /**
     * Вставка текста документа
     * 
     * @param  integer $documentId идент. изображения
     * @return void 
     */
    protected function textInsert($itemId)
    {
        $ln = $this->input->get('ln', false);
        if (empty($ln)) return;

        $table = new GDb_Table('site_menu_items_l', 'item_lid');
        // допустимые поля
        $fields = array('item_name', 'item_url');
        // список доступных языков
        $list = $this->config->getFromCms('Site', 'LANGUAGES');
        // вкладки с языками
        foreach ($list as $key => $lang) {
            // если есть язык
            if (isset($ln[$lang['id']])) {
                $item = $ln[$lang['id']];
                $data = array('item_id' => $itemId, 'language_id' => $lang['id']);
                for ($j = 0; $j < sizeof($fields); $j++) {
                    if (isset($item[$fields[$j]]))
                        $data[$fields[$j]] = $item[$fields[$j]];
                }
                if ($table->insert($data) === false)
                    throw new GSqlException();
            }
        }
    }

    /**
     * Обновление текста документа
     * 
     * @return void
     */
    protected function textUpdate()
    {
        $ln = $this->input->get('ln', false);
        if (empty($ln)) return;

        $table = new GDb_Table('site_menu_items_l', 'item_lid');
        // допустимые поля
        $fields = array('item_name', 'item_url');
        // список доступных языков
        $list = $this->config->getFromCms('Site', 'LANGUAGES');
        // вкладки с языками
        foreach ($list as $key => $lang) {
            // если есть язык
            if (isset($ln[$lang['id']])) {
                $item = $ln[$lang['id']];
                // если есть только идент.
                if (sizeof($item) == 1) continue;
                $data = array();
                for ($j = 0; $j < sizeof($fields); $j++) {
                    if (isset($item[$fields[$j]]))
                        $data[$fields[$j]] = $item[$fields[$j]];
                }
                // обновить запись
                if ($table->update($data, $item['item_lid']) === false)
                    throw new GSqlException();
            }
        }
    }

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return $this->_['title_profile_update'];
    }

    /**
     * Событие наступает после успешного обновления данных
     * 
     * @param array $data сформированные данные полученные после запроса к таблице
     * @return void
     */
    protected function dataUpdateComplete($data = array())
    {
        // соединение с базой данных (т.к. для лога ранее возможно было другое соединение)
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();

        // обновление текста изображения
        $this->textUpdate($this->_recordId);
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Событие наступает после успешного обновления данных
     * 
     * @param array $data сформированные данные полученные после запроса к таблице
     * @return void
     */
    protected function dataInsertComplete($data = array())
    {
        // соединение с базой данных (т.к. для лога ранее возможно было другое соединение)
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();

        // обновление текста изображения
        $this->textInsert($this->_recordId);
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Событие наступает после успешного удаления данных
     * 
     * @return void
     */
    protected function dataDeleteComplete()
    {
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        $query = new GDb_Query();
        // удаление пунктов меню "site_menu_items_l"
        $sql = 'DELETE FROM `site_menu_items_l` WHERE `item_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_menu_items_l') === false)
            throw new GSqlException();
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        // если состояние формы "правка"
        if ($this->isUpdate) return;
        // идент. статьи
        $params['menu_id'] = $this->_menuId;
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        
        $setTo = array();
        $query = new GDb_Query();
        $languageId = $this->config->getFromCms('Site', 'LANGUAGE/ID');
        // статья
        if ((int) ($record['article_id']) > 0) {
            $sql = 'SELECT `p`.`page_header` FROM `site_articles` `a` '
                 . 'JOIN `site_pages` `p` ON `a`.`article_id`=`p`.`article_id` AND `p`.`language_id`=' . $languageId . ' AND `a`.`article_id`=' . $record['article_id'];
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            if (!empty($rec))
                $setTo[] = array('id' => 'fldArticle', 'xtype' => 'mn-field-combo', 'value' => $record['article_id'], 'text' => $rec['page_header']);
        }

        // предок меню
        if ((int) ($record['item_parent_id']) > 0) {
            
            $sql = 'SELECT `l`.`item_name` FROM `site_menu_items` `i` '
                 . 'JOIN `site_menu_items_l` `l` ON `l`.`item_id`=`i`.`item_id` AND `l`.`language_id`=' . $languageId . ' AND `i`.`item_id`=' . $record['item_parent_id'];
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            $record['item_parent_id'] = $rec['item_name'];
        }

        // установка полей
        if ($setTo)
            $this->response->add('setTo', $setTo);

        return $record;
    }
}
?>