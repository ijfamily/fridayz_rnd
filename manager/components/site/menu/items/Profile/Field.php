<?php
/**
 * Gear Manager
 *
 * Контроллер         "Изменение профиля пункта меню"
 * Пакет контроллеров "Профиль пункта меню"
 * Группа пакетов     "Меню сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    GController_SMenuItems_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Field.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Field');

/**
 * Изменение профиля пункта меню
 * 
 * @category   Gear
 * @package    GController_SMenuItems_Profile
 * @subpackage Field
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Field.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SMenuItems_Profile_Field extends GController_Profile_Field
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array('item_visible');

    /**
     * Первичный ключ таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'item_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_menu_items';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_smenu_grid';
}
?>