<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля пункта меню"
 * Пакет контроллеров "Профиль пункта меню"
 * Группа пакетов     "Меню сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    GController_SMenuItems_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля пункта меню
 * 
 * @category   Gear
 * @package    GController_SMenuItems_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SMenuItems_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Возращает интерфейса полей с текстом
     * 
     * @return array
     */
    protected function getTextInterface()
    {
        // соединение с базой данных
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();

        $values = array();

        // если состоянеи формы "правка"
        if ($this->isUpdate) {
            $query = new GDb_Query();
            // обновление текста в картинках
            $sql = 'SELECT * FROM `site_menu_items_l` WHERE `item_id`=' . $this->uri->id;
            if ($query->execute($sql) === false)
                throw new GSqlException();
            while (!$query->eof()) {
                $item = $query->next();
                if (!isset($values[$item['language_id']]))
                    $values[$item['language_id']] = array();
                $values[$item['language_id']]['item_lid'] = $item['item_lid'];
                $values[$item['language_id']]['item_name'] = $item['item_name'];
                $values[$item['language_id']]['item_url'] = $item['item_url'];
            }
        }

        $did = $this->config->getFromCms('Site', 'LANGUAGE/ID');
        // список доступных языков
        $list = $this->config->getFromCms('Site', 'LANGUAGES');
        // вкладки с языками
        $tabs[] = array();
        foreach ($list as $key => $lang) {
        //for ($i = 0; $i < $count; $i++) {
            $lid = $lang['id'];
            $tabs[] = array(
                'title'      => $lang['title'],
                'layout'     => 'form',
                'baseCls'    => 'mn-form-tab-body-c',
                'iconSrc'    => $this->resourcePath . ($did == $lid ? 'icon-tab-text-def.png' : 'icon-tab-text.png'),
                'labelWidth' => 81,
                'items'   => array(
                    array('xtype' => 'hidden',
                          'id'    => 'ln[' . $lid . '][item_lid]',
                          'name'  => 'ln[' . $lid . '][item_lid]',
                          'value' => isset($values[$lid]['item_lid']) ? $values[$lid]['item_lid'] : ''),
                    array('xtype'      => 'fieldset',
                          'labelWidth' => 70,
                          'title'      => $this->_['title_fieldset_link'],
                          'autoHeight' => true,
                          'items'      => array(
                              array('xtype'      => 'textfield',
                                    'itemCls'    => 'mn-form-item-quiet',
                                    'fieldLabel' => $this->_['label_item_url'],
                                    'labelTip'   => $this->_['tip_item_url'],
                                    'name'       => 'ln[' . $lid . '][item_url]',
                                    'value'      => isset($values[$lid]['item_url']) ? $values[$lid]['item_url'] : '',
                                    'maxLength'  => 255,
                                    'anchor'     => '100%',
                                    'allowBlank' => true)
                          )
                    ),
                    array('xtype'      => 'textfield',
                          'itemCls'    => 'mn-form-item-quiet',
                          'fieldLabel' => $this->_['label_item_name'],
                          'labelTip'   => $this->_['tip_item_name'],
                          'name'       => 'ln[' . $lid . '][item_name]',
                          'value'      => isset($values[$lid]['item_name']) ? $values[$lid]['item_name'] : '',
                          'maxLength'  => 255,
                          'anchor'     => '100%',
                          'allowBlank' => false)
                )
            );
        }

        return array(
            'xtype'             => 'tabpanel',
            'tabPosition'       => 'bottom',
            'layoutOnTabChange' => true,
            'activeTab'         => 0,
            'anchor'            => '100%',
            'height'            => 135,
            'items'             => array($tabs)
        );
    }

    /**
     * Возращает список доменов
     * 
     * @return array
     */
    protected function getDomains()
    {
        $arr = $this->config->getFromCms('Domains');
        $list = array();
        if ($arr) {
            $list[] = array($this->config->getFromCms('Site', 'NAME', ''), 0);
            foreach ($arr['indexes'] as $key => $value) {
                $list[] = array($value[1], $key);
            } 
        }

        return $list;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'id'            => $this->classId,
                  'titleEllipsis' => 80,
                  'gridId'        => 'gcontroller_smenuitems_grid',
                  'width'         => 505,
                  'autoHeight'    => true,
                  'resizable'     => false,
                  'stateful'      => false)
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype'      => 'spinnerfield',
                  'itemCls'    => 'mn-form-item-quiet',
                  'fieldLabel' => $this->_['label_item_index'],
                  'labelTip'   => $this->_['tip_item_index'],
                  'name'       => 'item_index',
                  'resetable'  => false,
                  'width'      => 70,
                  'allowBlank' => true,
                  'emptyText'  => 1),
            array('xtype'      => 'mn-field-chbox',
                  'itemCls'    => 'mn-form-item-quiet',
                  'fieldLabel' => $this->_['label_item_visible'],
                  'labelTip'   => $this->_['tip_item_visible'],
                  'default'    => $this->isUpdate ? null : 1,
                  'name'       => 'item_visible'),
            array('xtype'         => 'combo',
                  'fieldLabel'    => $this->_['label_domain'],
                  'itemCls'       => 'mn-form-item-quiet',
                  'name'          => 'domain_id',
                  'hidden'        => !$this->config->getFromCms('Site', 'OUTCONTROL'),
                  'value'         => 0,
                  'editable'      => false,
                  'maxLength'     => 100,
                  'width'         => 200,
                  'typeAhead'     => false,
                  'triggerAction' => 'all',
                  'mode'          => 'local',
                  'store'         => array(
                      'xtype'  => 'arraystore',
                      'fields' => array('display', 'value'),
                      'data'   => $this->getDomains()
                  ),
                  'hiddenName'    => 'domain_id',
                  'valueField'    => 'value',
                  'displayField'  => 'display',
                  'allowBlank'    => true),
            array('xtype'      => 'fieldset',
                  'labelWidth' => 75,
                  'title'      => $this->_['title_fieldset_pitem'],
                  'autoHeight' => true,
                  'items'      => array(
                      array('xtype'      => 'mn-field-combo-tree',
                            'fieldLabel' => $this->_['label_pitem_name'],
                            'width'      => 300,
                            'id'         => 'fldItemParent',
                            'name'       => 'item_parent_id',
                            'hiddenName' => 'item_parent_id',
                            'treeWidth'  => 400,
                            'treeRoot'   => array('id' => 'root', 'expanded' => false, 'expandable' => true),
                            //'value'      => $data['menu_id'],
                            'allowBlank' => true,
                            'store'      => array('xtype' => 'jsonstore', 'url' => $this->componentUrl . 'combo/nodes/'))
                  )
            ),
            /*
            array('xtype'      => 'fieldset',
                  'labelWidth' => 75,
                  'title'      => $this->_['title_fieldset_article'],
                  'autoHeight' => true,
                  'items'      => array(
                      array('xtype'      => 'mn-field-combo',
                            'itemCls'    => 'mn-form-item-quiet',
                            'fieldLabel' => $this->_['label_page_header'],
                            'checkDirty' => false,
                            'id'         => 'fldArticle',
                            'name'       => 'article_id',
                            'editable'   => true,
                            'width'      => 300,
                            'hiddenName' => 'article_id',
                            'allowBlank' => true,
                            'store'      => array(
                                'xtype' => 'jsonstore',
                                'url'   => $this->componentUrl . 'combo/trigger/?name=articles'
                            )
                    )
                  )
            ),*/
            $this->getTextInterface()
        );

        // если едент. меню задаётся из вне
        if ($menuId = $this->uri->getVar('parent', false)) {
            $items[] = array(
                'xtype' => 'hidden',
                'name'  => 'menu_id',
                'value' => $menuId
            );
        }

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($items);
        $form->url = $this->componentUrl . 'profile/';
        $form->labelWidth = 85;

        parent::getInterface();
    }
}
?>