<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Creating record "Document"',
    'title_profile_update' => 'Updating record "%s"',
    // поля формы
    'label_document_index'    => 'Index',
    'tip_document_index'      => 
        'Serial number (for the gallery - ordering the list, for the article - the formation of a pseudonym; for each '
      . 'category of your order)',
    'label_document_visible'  => 'Visible',
    'tip_document_visible'    => 'Show document',
    'label_document_archive'  => 'Archive',
    'title_fieldset_download' => 'Счётчик загрузки документа',
    'label_download_set'      => 'Счётчик',
    'tip_download_set'        => 'Счетать загрузки документа',
    'label_download_tpl'      => 'Шаблон',
    'tip_document_archive'    => 'Document in the archive (not available)',
    'label_document_title'    => 'Name',
    'tip_document_title'      => 'Used to sign the document in the article',
    'title_fieldset_doc'      => 'File ("doc", "docx", "xls", "xlsx", "pdf", "djv", "zip", "rar")',
    'text_empty'              => 'Select file ...',
    'text_btn_upload'         => 'Browse',
    'title_fieldset_adoc'     => 'Document attributes',
    'label_document_filename' => 'File',
    'label_document_oname'    => 'File (о)',
    'tip_document_oname'      => 'Original file name',
    'label_document_download' => 'Download',
    'tip_document_download'   => 'Total document donwload',
    'label_document_uri'      => 'URI resource',
    'tip_document_uri'        => 
        'A character string, which allows to identify any resource: document, image, file, service, '
      . 'email inbox, etc.',
    'label_document_url'      => 'Русурс URL',
    'tip_document_url'        => 
        'Uniform locator (determinant for the location) of the resource. It is a standardized method of recording addresses '
      . 'resource on the Internet.',
    'label_document_type'     => 'Type',
    'tip_document_type'       => 'File type ("DOC", "DOCX", "XLS", "XLSX", "PDF", "DJV", "ZIP", "RAR")',
    'label_document_filesize' => 'Size',
    'tip_document_filesize'   => 'File size',
    'label_date_insert'       => 'Created',
    'label_date_update'       => 'Updated'
);
?>