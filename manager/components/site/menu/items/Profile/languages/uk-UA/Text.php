<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Створення запису "Документ"',
    'title_profile_update' => 'Зміна запису "%s"',
    // поля формы
    'label_document_index'    => 'Індекс',
    'tip_document_index'      => 
        'Порядковий номер (для галереї - впорядкованість списку; для статті - формування псевдоніма; для кожної '
      . 'категорії свій порядок)',
    'label_document_visible'  => 'Показувати',
    'tip_document_visible'    => 'Показувати документ',
    'label_document_archive'  => 'Архів',
    'title_fieldset_download' => 'Счётчик загрузки документа',
    'label_download_set'      => 'Счётчик',
    'tip_download_set'        => 'Счетать загрузки документа',
    'label_download_tpl'      => 'Шаблон',
    'tip_document_archive'    => 'Документ в архіві (не буде доступно)',
    'label_document_title'    => 'Назва',
    'tip_document_title'      => 'Використовується для підпису документа в статті',
    'title_fieldset_doc'      => 'Файл ("doc", "docx", "xls", "xlsx", "pdf", "djv", "zip", "rar")',
    'text_empty'              => 'Виберіть файл ...',
    'text_btn_upload'         => 'Выбрати',
    'title_fieldset_adoc'     => 'Атрибути документа',
    'label_document_filename' => 'Файл',
    'label_document_oname'    => 'Файл (о)',
    'tip_document_oname'      => 'Оригінальна назва файлу',
    'label_document_download' => 'Завантажень',
    'tip_document_download'   => 'Кількість завантажень файлу користувачами',
    'label_document_uri'      => 'Русурс URI',
    'tip_document_uri'        => 
        'Це символьний рядок, що дозволяє ідентифікувати небудь ресурс: документ, зображення, файл, службу, '
      . 'ящик електронної пошти і т. д.',
    'label_document_url'      => 'Русурс URL',
    'tip_document_url'        => 
        'Eдінообразний локатор (визначник місцезнаходження) ресурсу. Це стандартизований спосіб запису адреси '
      . 'ресурсу в мережі Інтернет.',
    'label_document_type'     => 'Тип',
    'tip_document_type'       => 'Тип файлу ("DOC", "DOCX", "XLS", "XLSX", "PDF", "DJV", "ZIP", "RAR")',
    'label_document_filesize' => 'Розмір',
    'tip_document_filesize'   => 'Розмір файла',
    'label_date_insert'       => 'Створен',
    'label_date_update'       => 'Змінен'
);
?>