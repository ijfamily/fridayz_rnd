<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Создание записи "Пункт меню"',
    'title_profile_update' => 'Изменение пункта меню',
    // поля формы
    'label_domain'         => 'Сайт',
    'label_item_index'     => 'Порядок',
    'tip_item_index'       => 'Порядковый номер в списке',
    'label_item_name'      => 'Название',
    'title_fieldset_pitem' => 'Положение в структуре меню',
    'label_pitem_name'     => 'Предок',
    'tip_pitem_name'       => 'Предок пункт меню',
    'label_item_visible'   => 'Показывать',
    'tip_item_visible'     => 'Показывать пункт меню',
    'title_fieldset_link'  => 'Если пункт ссылка',
    'label_item_name'      => 'Название',
    'tip_item_name'        => 'Название пункта меню',
    'label_item_url'       => 'URL адрес',
    'tip_item_url'         => 'URL адрес ссылки',
    'title_fieldset_article' => 'Если пункт статья',
    'label_page_header'      => 'Статья'
);
?>