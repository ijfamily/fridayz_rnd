<?php
/**
 * Gear Manager
 *
 * Контроллер         "Триггер выпадающего списка для обработки данных"
 * Пакет контроллеров "Пункты меню сайта"
 * Группа пакетов     "Меню сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    GController_SMenuItems_Combo
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Combo/Trigger');

/**
 * Триггер выпадающего списка для обработки данных
 * 
 * @category   Gear
 * @package    GController_SMenuItems_Combo
 * @subpackage Trigger
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SMenuItems_Combo_Trigger extends GController_Combo_Trigger
{
    /**
     * Возращает категории статей
     * 
     * @return void
     */
    protected function queryArticles()
    {
        $languageId = $this->config->getFromCms('Site', 'LANGUAGE/ID');
        $table = new GDb_Table('site_categories', 'category_id');
        $table->setStart($this->_start);
        $table->setLimit($this->_limit);
        $sql = 'SELECT SQL_CALC_FOUND_ROWS `a`.`article_id`, `p`.`page_header` '
             . 'FROM `site_articles` `a` JOIN `site_pages` `p` ON `p`.`article_id`=`a`.`article_id` AND `p`.`language_id`=' . $languageId
             . ($this->_query ? " AND `p`.`page_header` LIKE '{$this->_query}%' " : '')
             . ' ORDER BY `page_header` LIMIT %limit';
        if ($table->query($sql) === false)
            throw new GSqlException();
        $data = array();
        while (!$table->query->eof()) {
            $record = $table->query->next();
            $data[] = array('id' => $record['article_id'], 'name' => $record['page_header']);
        }
        $this->response->set('totalCount', $table->query->getFoundRows());
        $this->response->success = true;
        $this->response->data = $data;
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $triggerName название триггера
     * @return void
     */
    protected function dataView($triggerName)
    {
        parent::dataView($triggerName);

        // имя триггера
        switch ($triggerName) {
            // статьи
            case 'articles': $this->queryArticles(); break;
        }
    }
}
?>