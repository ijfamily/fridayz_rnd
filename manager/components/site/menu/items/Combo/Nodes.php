<?php
/**
 * Gear Manager
 *
 * Контроллер         "Триггер выпадающего дерева для обработки данных"
 * Пакет контроллеров "Пункты меню сайта"
 * Группа пакетов     "Меню сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    GController_SMenu_Combo
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Nodes.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Combo/Nodes');

/**
 * Триггер выпадающего дерева для обработки данных
 * 
 * @category   Gear
 * @package    GController_SMenu_Combo
 * @subpackage Nodes
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Nodes.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SMenuItems_Combo_Nodes extends GController_Combo_Nodes
{
    /**
     * PПрефикс полей таблицы для формирования nested set
     *
     * @var string
     */
    public $fieldPrefix = 'item';

    /**
     * Поле используемое для сортировки данных
     *
     * @var string
     */
    public $orderBy = '';

    /**
     * Первичное поле таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'item_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_menu_items';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_smenu_grid';

    /**
     * Вызывается перед предварительной обработкой запроса при формировании записей
     * 
     * @param  array $prefix префикс полей
     * @param  array $id идинд. узла дерева
     * @return string
     */
    protected function queryPreprocessing($prefix, $id)
    {
        $languageId = $this->config->getFromCms('Site', 'LANGUAGE/ID');
        $menuId = $this->store->get('record', 0, 'gcontroller_smenuitems_grid');
        if ($id == 'root')
            $query = 'SELECT `l`.*, `a`.*, COUNT(`b`.`' . $this->idProperty . '`) `leaf` '
                   . 'FROM `' . $this->tableName . '` `a` '
                   . 'JOIN `site_menu_items_l` `l` ON `a`.`item_id`=`l`.`item_id` AND `l`.`language_id`=' . $languageId
                   . ' LEFT JOIN `' . $this->tableName . '` `b` ON `b`.`' . $prefix['parent_id'] . '`=`a`.`' . $this->idProperty . '` '
                   . 'WHERE `a`.`' . $prefix['parent_id'] . '` is NULL GROUP BY `a`.`' . $this->idProperty . '`';
        else
            $query = 'SELECT `l`.*, `a`.*, COUNT(`b`.`' . $this->idProperty . '`) `leaf` '
                   . 'FROM `' . $this->tableName . '` `a` '
                   . 'JOIN `site_menu_items_l` `l` ON `a`.`item_id`=`l`.`item_id` AND `l`.`language_id`=' . $languageId
                   . ' LEFT JOIN `' . $this->tableName . '` `b` ON `b`.`' . $prefix['parent_id'] . '`=`a`.`' . $this->idProperty . '` '
                   . 'WHERE `a`.`menu_id`=' . $menuId . ' AND `a`.`' . $prefix['parent_id'] . '` ' . ($id == 'root' ? 'is NULL' : '=' . $id)
                   . ' GROUP BY `a`.`' . $this->idProperty . '`';
        if ($this->orderBy) {
            if (isset($prefix[$this->orderBy]))
                $query .= ' ORDER BY `' . $prefix[$this->orderBy] . '`';
            else
                $query .= ' ORDER BY `' . $this->orderBy . '`';
        }

        return $query;
    }



    /**
     * Предварительная обработка узла дерева перед формированием массива записей JSON
     * 
     * @params array $node узел дерева
     * @params array $record запись
     * @return array
     */
    protected function nodesPreprocessing($node, $record)
    {
        $node['text'] = $record['item_name'];

        return $node;
    }
}
?>