<?php
/**
 * Gear Manager
 *
 * English (United Kingdom) package language
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    Translator
 * @copyright  Copyright (c) 2011-2012 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Translator.php 2012-12-01 12:00:00 Gear Magic $
 */

return array();
?>