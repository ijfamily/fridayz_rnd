<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск меню сайта"
 * Пакет контроллеров "Меню сайта"
 * Группа пакетов     "Меню сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    GController_SMenu_Search
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск меню сайта
 * 
 * @category   Gear
 * @package    GController_SMenu_Search
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SMenu_Search_Data extends GController_Search_Data
{
    /**
     * Идентификатор компонента
     *
     * @var string
     */
    public $gridId = 'gcontroller_smenu_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_smenu_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор
     * Используется для инициализации атрибутов контроллер не переданного в конструктор
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'menu_index', 'label' => $this->_['header_menu_index']),
            array('dataIndex' => 'menu_name', 'label' => $this->_['header_menu_name']),
            array('dataIndex' => 'menu_description', 'label' => $this->_['header_menu_description'])
        );
    }
}
?>