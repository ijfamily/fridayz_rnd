<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Пошук у списку "Меню сайту"',
    // поля
    'header_menu_index'   => '№',
    'header_menu_name'    => 'Назва',
    'header_pmenu_name'   => 'Назва(п)',
    'header_article_note' => 'Стаття',
    'header_menu_title'   => 'Підказка',
    'header_menu_url'     => 'URL адреса'
);
?>