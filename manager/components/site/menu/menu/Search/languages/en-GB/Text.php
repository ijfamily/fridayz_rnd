<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Search in the "Menu"',
    // поля
    'header_menu_index'   => '№',
    'header_menu_name'    => 'Name',
    'header_pmenu_name'   => 'Name (p)',
    'header_article_note' => 'Article',
    'header_menu_title'   => 'Tooltip',
    'header_menu_url'     => 'URL address'
);
?>