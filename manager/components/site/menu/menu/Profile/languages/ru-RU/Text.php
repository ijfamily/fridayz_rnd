<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Создание записи "Меню"',
    'title_profile_update' => 'Изменение записи "%s"',
    'text_btn_help'        => 'Справка',
    // поля формы
    'label_menu_index'       => 'Порядок',
    'tip_menu_index'         => 'Порядковый номер в списке',
    'label_menu_name'        => 'Название',
    'label_menu_description' => 'Описание'
);
?>