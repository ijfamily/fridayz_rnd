<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Creating a record "Menu"',
    'title_profile_update' => 'Change the record "%s"',
    // поля формы
    'label_menu_index'     => 'Index',
    'label_menu_name'      => 'Name',
    'label_article_note'   => 'Article',
    'title_fieldset_pmenu' => 'The position in the menu structure',
    'label_pmenu_name'     => 'Parent',
    'label_menu_hidden'    => 'Hidde',
    'tip_menu_hidden'      => 'Hidde menu',
    'title_fieldset_link'  => 'If the menu link',
    'label_menu_title'     => 'Tooltip',
    'tip_menu_title'       => 'Flyover to text links',
    'label_menu_url'       => 'URL address',
    'tip_menu_url'         => 'URL link address'
);
?>