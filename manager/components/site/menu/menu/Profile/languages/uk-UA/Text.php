<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Створення запису "Меню"',
    'title_profile_update' => 'Зміна запису "%s"',
    // поля формы
    'label_menu_index'     => 'Iндекс',
    'label_menu_name'      => 'Назва',
    'label_article_note'   => 'Стаття',
    'title_fieldset_pmenu' => 'Положення у структурі меню',
    'label_pmenu_name'     => 'Предок',
    'label_menu_hidden'    => 'Приховати',
    'tip_menu_hidden'      => 'Сховати меню',
    'title_fieldset_link'  => 'Якщо меню посилання',
    'label_menu_title'     => 'Підказка',
    'tip_menu_title'       => 'Підказка до тексту посилання',
    'label_menu_url'       => 'URL адреса',
    'tip_menu_url'         => 'URL адреса посилання'
);
?>