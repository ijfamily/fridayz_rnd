<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные для интерфейса профиля меню сайта"
 * Пакет контроллеров "Профиль меню сайта"
 * Группа пакетов     "Меню сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    GController_SMenu_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные для интерфейса профиля меню сайта
 * 
 * @category   Gear
 * @package    GController_SMenu_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SMenu_Profile_Data extends GController_Profile_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array('menu_index', 'menu_name', 'menu_description');

    /**
     * Первичный ключ таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'menu_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_menu';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_smenu_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_profile_update'], $record['menu_name']);
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        // выбранное меню
        $query = new GDb_Query();
        // удаление записей таблицы "site_menu_items_l"
        $sql = 'DELETE `l` FROM `site_menu_items_l` `l`, `site_menu_items` `i` '
             . 'WHERE `l`.`item_id`=`i`.`item_id` AND `i`.`menu_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        // удаление записей таблицы "site_menu_items"
        $sql = 'DELETE FROM `site_menu_items` WHERE `menu_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Событие наступает после успешного обновления данных
     * 
     * @param array $data сформированные данные полученные после запроса к таблице
     * @return void
     */
    protected function dataUpdateComplete($data = array())
    {
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Событие наступает после успешного удаления данных
     * 
     * @return void
     */
    protected function dataDeleteComplete()
    {
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }
}
?>