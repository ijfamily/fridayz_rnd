<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные для интерфейса списка меню"
 * Пакет контроллеров "Меню сайта"
 * Группа пакетов     "Меню сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    GController_SMenu_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные для интерфейса списка меню
 * 
 * @category   Gear
 * @package    GController_SMenu_Grid
 * @subpackage Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SMenu_Grid_Data extends GController_Grid_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'menu_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_menu';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_smenu_grid';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS `mi`.`menu_count`, `m`.* '
          . 'FROM `site_menu` `m` LEFT JOIN (SELECT `menu_id`, COUNT(`menu_id`) `menu_count` '
          . 'FROM `site_menu_items` GROUP BY `menu_id`) `mi` USING (`menu_id`) '
          . 'WHERE 1 %filter ORDER BY %sort LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // порядок меню
            'menu_index' => array('type' => 'string'),
            // название меню
            'menu_name' => array('type' => 'string'),
            // описание меню
            'menu_description' => array('type' => 'string'),
            // кол. пунктов меню
            'menu_count' => array('type' => 'integer'),
        );
    }

    /**
     * Сброс данных к первоначальному виду
     * 
     * @return void
     */
    protected function dataReset()
    {
        // пункты меню
        $table = new GDb_Table('site_menu_items', 'item_id');
        $table->insert(
            array('item_index'      => 1,
                  'item_visible'    => 1)
        );
        // пункты меню с языком
        $table = new GDb_Table('site_menu_items_l', 'item_lid');
        $table->insert(
            array('language_id' => $this->config->getFromCms('Site', 'LANGUAGE/ID'),
                  'item_id'     => $table->getLastInsertId(),
                  'item_name'   => 'Меню сайта')
        );
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        $query = new GDb_Query();
        // удаление записей таблицы "site_menu_items_l"
        if ($query->clear('site_menu_items_l') === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_menu_items_l') === false)
            throw new GSqlException();
        // удаление записей таблицы "site_menu_items"
        if ($query->clear('site_menu_items') === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_menu_items') === false)
            throw new GSqlException();
        // удаление записей таблицы "site_menu"
        if ($query->clear('site_menu') === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_menu') === false)
            throw new GSqlException();

        // сброс данных к первоначальному виду
        $this->dataReset();
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        // выбранное меню
        $query = new GDb_Query();
        // удаление записей таблицы "site_menu_items_l"
        $sql = 'DELETE `l` FROM `site_menu_items_l` `l`, `site_menu_items` `i` '
             . 'WHERE `l`.`item_id`=`i`.`item_id` AND `i`.`menu_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        // удаление записей таблицы "site_menu_items"
        $sql = 'DELETE FROM `site_menu_items` WHERE `menu_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Событие наступает после успешного удаления данных
     * 
     * @return void
     */
    protected function dataDeleteComplete()
    {
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }
}
?>