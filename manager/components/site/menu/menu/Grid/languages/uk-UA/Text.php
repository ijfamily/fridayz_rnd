<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Меню сайта',
    'rowmenu_edit' => 'Редагувати',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'стовпці',
    'title_buttongroup_filter' => 'Фільтр',
    'msg_btn_clear'            => 'Ви дійсно бажаєте видалити всі записи <span class="mn-msg-delete"> ("Меню сайту")</span> ?',
    'warningr_btn_copy'        => 'Необхідно виділити тільки один запис!',
    // столбцы
    'header_menu_index'    => '№',
    'tooltip_menu_index'   => 'Порядковий номер статті (для списку)',
    'header_menu_name'     => 'Назва',
    'header_pmenu_name'    => 'Назва (п)',
    'tooltip_pmenu_name'   => 'Назва &quot;предка&quot; меню',
    'header_article_note'  => 'Стаття',
    'tooltip_article_note' => 'Примітка статті',
    'header_menu_title'    => 'Підказка',
    'tooltip_menu_title'   => 'Підказка до тексту посилання',
    'header_menu_url'      => 'URL адреса',
    'tooltip_menu_url'     => 'URL адреса посилання меню'
);
?>