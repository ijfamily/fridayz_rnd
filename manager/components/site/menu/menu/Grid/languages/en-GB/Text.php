<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Site menu',
    'rowmenu_edit' => 'Edit',
    // панель управления
    'title_buttongroup_edit'   => 'Edit',
    'title_buttongroup_cols'   => 'Columns',
    'title_buttongroup_filter' => 'Filter',
    'msg_btn_clear'            => 'Do you really want to delete all records <span class="mn-msg-delete"> ("Menu")</span> ?',
    'warningr_btn_copy'        => 'You need to select only one record!',
    // столбцы
    'header_menu_index'    => '№',
    'tooltip_menu_index'   => 'The serial number of the article (for the list)',
    'header_menu_name'     => 'Name',
    'header_pmenu_name'    => 'Name (p)',
    'tooltip_pmenu_name'   => 'Name &quot;parent&quot; menu',
    'header_article_note'  => 'Article',
    'tooltip_article_note' => 'Note article',
    'header_menu_title'    => 'Hiny',
    'tooltip_menu_title'   => 'Tip tooltip to text links',
    'header_menu_url'      => 'URL address',
    'tooltip_menu_url'     => 'URL address menu links'
);
?>