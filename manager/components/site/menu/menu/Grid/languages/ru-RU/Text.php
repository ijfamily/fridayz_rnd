<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'    => 'Меню сайта',
    'tooltip_grid'  => 'доступные меню на сайте',
    'rowmenu_edit'  => 'Редактировать',
    'rowmenu_items' => 'Пункты меню',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">("Меню сайта")</span> ?',
    // столбцы
    'header_menu_index'       => '№',
    'tooltip_menu_index'      => 'Порядковый номер меню в списке',
    'header_menu_name'        => 'Название',
    'header_menu_description' => 'Описание',
    'header_menu_count'       => 'Пунктов в меню'
);
?>