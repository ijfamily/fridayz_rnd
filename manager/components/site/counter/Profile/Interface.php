<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля настройки счётчиков посещений"
 * Пакет контроллеров "Настройка счётчиков посещений"
 * Группа пакетов     "Настройка счётчиков посещений"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SCounterConfig_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля настройки счётчиков посещений
 * 
 * @category   Gear
 * @package    GController_SCounterConfig_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SCounterConfig_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // Ext_Window (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'           => $this->_['title_profile_update'],
                  'titleEllipsis'   => 40,
                  'width'           => 640,
                  'autoHeight'      => true,
                  'resizable'       => false,
                  'btnDeleteHidden' => true,
                  'stateful'        => false,
                  'buttonsAdd'      => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-site-counter-config',
                            'iconCls' => 'icon-item-info')
                  ))
        );
        $this->_cmp->state = 'update';

        // владка "Счётчик Яндекс.Метрика"
        $tabYandex = array(
            'title'       => $this->_['title_tab_yandex'],
            'iconSrc'     => $this->resourcePath . 'icon-tab-yandex.png',
            'layout'      => 'form',
            'labelWidth'  => 85,
            'autoScroll'  => true,
            'baseCls'     => 'mn-form-tab-body',
            'defaultType' => 'textfield',
            'items'       => array(
                array('xtype' => 'label', 'html' => $this->_['html_yandex']),
                array('xtype' => 'mn-field-separator', 'html' => $this->_['title_script']),
                array('xtype'      => 'textarea',
                      'itemCls'    => 'mn-form-item-quiet',
                      'hideLabel'  => true,
                      'name'       => 'yandex',
                      'anchor'     => '100%',
                      'checkDirty' => false,
                      'height'     => 215,
                      'allowBlank' => true)
            )
        );

        // владка "Счётчик Google Analytics"
        $tabGoogle = array(
            'title'       => $this->_['title_tab_google'],
            'iconSrc'     => $this->resourcePath . 'icon-tab-google.png',
            'layout'      => 'form',
            'labelWidth'  => 85,
            'autoScroll'  => true,
            'baseCls'     => 'mn-form-tab-body',
            'defaultType' => 'textfield',
            'items'       => array(
                array('xtype' => 'container', 'html' => $this->_['html_google']),
                array('xtype' => 'mn-field-separator', 'html' => $this->_['title_script']),
                array('xtype'      => 'textarea',
                      'itemCls'    => 'mn-form-item-quiet',
                      'hideLabel'  => true,
                      'name'       => 'google',
                      'checkDirty' => false,
                      'anchor'     => '100%',
                      'height'     => 254,
                      'allowBlank' => true)
            )
        );

        // владка "Другие счётчики"
        $tabOther = array(
            'title'       => $this->_['title_tab_other'],
            'iconSrc'     => $this->resourcePath . 'icon-tab-other.png',
            'layout'      => 'form',
            'labelWidth'  => 85,
            'autoScroll'  => true,
            'baseCls'     => 'mn-form-tab-body',
            'defaultType' => 'textfield',
            'items'       => array(
                array('xtype' => 'container', 'html' => $this->_['html_other']),
                array('xtype' => 'mn-field-separator', 'html' => $this->_['title_script']),
                array('xtype'      => 'textarea',
                      'itemCls'    => 'mn-form-item-quiet',
                      'hideLabel'  => true,
                      'name'       => 'other',
                      'checkDirty' => false,
                      'anchor'     => '100%',
                      'height'     => 240,
                      'allowBlank' => true)
            )
        );

        // вкладки окна
        $tabs = array(
            array('xtype'             => 'tabpanel',
                  'layoutOnTabChange' => true,
                  'activeTab'         => 0,
                  'style'             => 'padding:3px',
                  'anchor'            => '100%',
                  'height'            => 355,
                  'items'             => array($tabYandex, $tabGoogle, $tabOther))
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->url = $this->componentUrl . 'profile/';
        $form->items->add($tabs);

        parent::getInterface();
    }
}
?>