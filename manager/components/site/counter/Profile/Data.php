<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля настройки счётчиков посещений"
 * Пакет контроллеров "Настройка счётчиков посещений"
 * Группа пакетов     "Настройка счётчиков посещений"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SCounterConfig_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

define('_INC', 1);

Gear::controller('Profile/Data');

/**
 * Данные профиля настройки счётчиков посещений
 * 
 * @category   Gear
 * @package    GController_SCounterConfig_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SCounterConfig_Profile_Data extends GController_Profile_Data
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_scounterconfig_profile';

    /**
     * Массив полей
     *
     * @var array
     */
    public $fields = array('yandex', 'google', 'other');

    /**
     * Экран для парсинга счётчиков
     *
     * @var array
     */
    public $parse = array(
        'yandex' => array('<!-- Yandex.Metrika counter -->', '<!-- /Yandex.Metrika counter -->'),
        'google' => array('<!-- Google Analytics counter -->', '<!-- /Google Analytics counter -->'),
        'other'  => array('<!-- Other counter -->', '<!-- /Other counter -->')
    );

    /**
     * Файл шаблона
     *
     * @var string
     */
    public $filename = '../application/templates/common/counter.tpl';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return $this->_['profile_title_update'];
    }

    /**
     * Запись счетчиков в файл
     * 
     * @return void
     */
    protected function counterWrite($params)
    {
        $str = _n;
        foreach ($params as $key => $value) {
            if (isset($this->parse[$key]) && $value) {
                $str .= $this->parse[$key][0] . _n . $value . _n . $this->parse[$key][1] . _n;
            }
        }
        if (file_put_contents($this->filename, $str) === false)
            throw new GException('Error', 'Can not open file "%s" for writing', array($this->filename));
    }

    /**
     * Чтение счетчиков из файла
     * 
     * @return array
     */
    protected function counterRead()
    {
        mb_internal_encoding("UTF-8");

        // если файла нет
        if (!file_exists($this->filename))
            throw new GException('Error', 'Can not open file "%s" for writing', array($this->filename));
        // чтение файла
        if (($text = file_get_contents($this->filename)) === false)
            throw new GException('Error', 'Can not open file "%s" for writing', array($this->filename));

        $data = array();
        foreach ($this->parse as $key => $value) {
            $start = mb_strrpos($text, $value[0]) + mb_strlen($value[0]) + 1;
            $end = mb_strpos($text, $value[1]);
            if ($start !== false && $end !== false)
                $data[$key] = mb_substr($text, $start, $end - $start - 1);
        }
    
        return $data;
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        parent::dataAccessUpdate();

        // массив полей с их соответствующими значениями
        if (empty($params))
            $params = $this->input->getBy($this->fields);
        // проверки входных данных
        $this->isDataCorrect($params);
        // проверка существования записи с одинаковыми значениями
        $this->isDataExist($params);
        // использование системных полей в запросе SQL
        if ($this->isSysFields) {
            $params['sys_date_update'] = date('Y-m-d');
            $params['sys_time_update'] = date('H:i:s');
            $params['sys_user_update'] = $this->session->get('user_id');
        }
        // запись в файл счетчиков
        $this->counterWrite($params);
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($params, 'method "PUT"');
        }
        $this->response->setMsgResult($this->_['title_msg_result'], $this->_['title_msg_text'], true);
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        parent::dataAccessView();

        $this->response->data = $this->counterRead();
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($data, 'data');
        }
        // запись в журнал действий пользователя
        $this->logView(
            array('log_query'        => '[text]',
                  'log_error'        => $this->response->success === false ? 'read from file "' . $this->filename. '"' : null,
                  'log_query_params' => '')
        );
    }
}
?>