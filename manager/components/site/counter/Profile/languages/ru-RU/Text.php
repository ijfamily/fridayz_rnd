<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_update' => 'Счётчики посещений',
    'text_btn_help'        => 'Справка',
    // поля
    'title_tab_yandex' => 'Счётчик Яндекс.Метрика',
    'html_yandex'      =>'<note>Яндекс.Метрика от компании Яндекс — невидимый счётчик, отчёты обновляются каждые 5 минут. Анализирует рекламный трафик, конверсии, строит интерактивные карты путей пользователей по сайту. Доступны отчёты по полу и возрасту посетителей сайта. Предоставляет бесплатный мониторинг доступности сайта. Все отчёты доступны за произвольный период.</note>',
    'title_tab_google' => 'Счётчик Google Analytics ',
    'html_google'      =>'<note>Счётчик Google Analytics от компании Google, проводящий полный анализ аудитории и межсайтовых переходов</note>',
    'title_tab_other'  => 'Другие счётчики',
    'html_other'       => '<note>Здесь вы можете добавить другие авторитетные и популярные для счётчики, такие как: Рамблер (Рамблер-ТОП100), Spylog (Openstat), LiveInternet.ru, Рейтинг Mail.Ru и др.</note>',
    'title_script'     => 'Скрипт счётчика',
    // сообщения
    'title_msg_result' => 'Счётчики посещений',
    'title_msg_text'   => 'счётчики успешно добавлены на сайт'
);
?>