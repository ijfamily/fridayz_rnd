<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Счётчики посещений"
 * Группа пакетов     "Счётчики посещений"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SCounterConfig
 * @copyright  Copyright (c) 2013-2016 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Счётчики посещений
 * Описание: Счётчики посещений подсчитывает полученную информацию и предоставляет её в виде статистического отчёта
 * Меню: Счётчики посещений
 * ID класса: gcontroller_scounterconfig_profile
 * Модуль: Сайт
 * Группа: Сайт
 * Очищать: нет
 * Статистика компонента: нет 
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске компонентов: нет
 *    В главном меню: нет
 * Ресурс
 *    Компонент: site/counter/
 *    Контроллер: site/counter/Profile/
 *    Интерфейс: profile/interface/
 *    Меню:
 */

return array(
    // {S}- модуль "Site" {} - пакет контроллеров "Site counter config" -> SCounterConfig
    'clsPrefix' => 'SCounterConfig',
    // использовать язык
    'language'  => 'ru-RU'
);
?>