<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс просмотра ресурсов сайта"
 * Пакет контроллеров "Просмотр ресурсов сайта"
 * Группа пакетов     "Ресурсы сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SResources_View
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::ext('Container/Panel');
Gear::controller('Interface');

/**
 * Интерфейс просмотра ресурсов сайта
 * 
 * @category   Gear
 * @package    GController_SResources_View
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SResources_View_Interface extends GController_Interface
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_sresources_view';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // панель (ExtJS class "Ext.Panel")
        $component = array(
            'id'            => $this->classId,
            'closable'      => true,
            'component'     => array('container' => 'content-tab', 'destroy' => true),
        );
        $this->_cmp = new Ext_Panel($component);
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_view'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_view'],
                  'titleEllipsis' => 40,
            )
        );
        $this->_cmp->items->add(array(
            'xtype' => 'mn-iframe',
            'url'   => '/' . PATH_APPLICATION . ROUTER_SCRIPT . $this->componentUrl . 'view/frame/' . $this->uri->id
        ));

        parent::getInterface();
    }
}
?>