<?php
/**
 * Gear Manager
 *
 * Контроллер         "Код фрейма ресурсов сайта"
 * Пакет контроллеров "Просмотр ресурсов сайта"
 * Группа пакетов     "Ресурсы сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SResources_View
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Frame.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Frame');
Gear::library('File');
Gear::library('String');

// отключить вывод ошибок в JSON формате
GException::$toJson = false;

/**
 * Код фрейма загрузчика
 * 
 * @category   Gear
 * @package    GController_SResources_View
 * @subpackage Frame
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Frame.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SResources_View_Frame extends GController_Frame
{
    /**
     * Вывод плагинов
     * 
     * @param string $resPath каталог ресурсов контроллера
     * @return void
     */
    public function pugins($resPath = '')
    {
        if (empty($this->settings['plugins'])) {
            echo '<em class="error">нет плагинов для отображения</em>';
            return;
        }
        foreach($this->settings['plugins'] as $index => $plugin) {
            $script = $this->path . '/plugins/' . $plugin . '.php';
            if (!file_exists($script)) {
                echo '<em class="error">невозможно подключить плагин "', $plugin, '"</em>';
                continue;
            }
            require_once($script);
            $func = 'plugin_' . $plugin;
            if (!function_exists($func)) {
                echo '<em class="error">нет точки входа в плагин "', $plugin, '"</em>';
                continue;
            }
            call_user_func($func, $this, $resPath);
        }
    }

    /**
     * Вывод кода фрейма
     * 
     * @param string $resPath каталог ресурсов сайта
     * @return void
     */
    public function frame($resPath = '')
    {
        $_DOCUMENT_ROOT = './';

        // соединение с базой данных
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();
        $st = $this->session->get('user/settings');
        if (isset($st['theme']))
            $theme = $st['theme'];
        else
            $theme = 'default';
?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <link href="/<?php echo PATH_APPLICATION;?>resources/js/manager/resources/css/manager.css" rel="stylesheet" />
        <link href="/<?=PATH_APPLICATION;?>resources/themes/<?=$theme;?>/default.css" rel="stylesheet" />
        <link href="<?php echo $resPath;?>/css/style.css" rel="stylesheet" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="<?php echo $resPath;?>/js/script.js"></script>
    </head>
    <body>
        <header>
            <h1>Ресурсы сайта</h1>
        </header>
        <article>
            <?php $this->pugins($resPath); ?>
        </article>
    </body>
</html>
<?php
    }
}
?>