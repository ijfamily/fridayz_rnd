<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_view'   => 'Ресурсы сайта',
    'tooltip_view' => 'просморт изображений фотоальбома',
    // типы
    'data_nav' => array('none' => 'Отключить', 'bottom' => 'Внизу', 'top' => 'Наверху', 'both' => 'Внизу и наверху'),
    'data_order' => array('d' => 'По убыванию', 'a' => 'По возрастанию'),
    'data_sort' => array('header' => 'По алфавиту', 'date' => 'По дате публикации'),
    'data_records' => array('запись', 'записи', 'записей'),
    'data_bytes' => array('Б', 'Kб', 'Мб', 'Гб', 'Тб', 'Пб', 'ZB', 'YB')
);
?>