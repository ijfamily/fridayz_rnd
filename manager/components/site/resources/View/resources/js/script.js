jQuery(document).ready(function($){
    $('a').click(function(){
        var src = $(this).attr('component-src');
        if (typeof src != 'undefined' && window.top.Site != 'undefined') {
            window.top.Site.load(src);
            return false;
        }
    });
});