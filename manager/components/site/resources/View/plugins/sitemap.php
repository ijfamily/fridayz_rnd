<?php
/**
 * Gear Manager
 *
 * Плагин             "Информация о карте сайта"
 * Пакет контроллеров "Просмотр ресурсов сайта"
 * Группа пакетов     "Ресурсы сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Sitemap
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: sitemap.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Плагин вывода информарции о карте сайта
 * 
 * @param resource $frame указатель на класс контроллера фрейма
 * @param string $resPath каталог ресурсов контроллера
 * @return void
 */
function plugin_sitemap($frame, $resPath = '')
{
?>
<h2>Карта сайта / <a class="edit" href="#" component-src="site/config/site/~/profile/interface/">изменить</a></h2>
<div class="desc">Sitemaps — XML-файлы с информацией для поисковых систем (таких как Google, Яндекс, Bing, Поиск@Mail.Ru) о страницах 
веб-сайта, которые подлежат индексации. Sitemaps могут помочь поисковикам определить местонахождение страниц сайта, время их последнего 
обновления, частоту обновления и важность относительно других страниц сайта для того, чтобы поисковая машина смогла более разумно 
индексировать сайт.</div>
<section>
    <img class="glyph" src="<?php echo $resPath;?>/images/icon-sitemap.png" />
    <table class="grid">
        <tr><td width="250px"><label>Использовать карту сайта:</label></td><td>
        <?php $v = $frame->config->getFromCms('Site', 'SITEMAP'); echo $v ? '<em class="ok"></em>' : '<em class="alert error">отключено</em>';?>
        </td></tr>
        <tr><td><label>Количество статей:</label></td><td>
        <?php $v = $frame->config->getFromCms('Site', 'SITEMAP/COUNT'); echo $v ? $v : '<em class="alert warning">не указано (все статьи)</em>';?>
        </td></tr>
        <tr><td><label>Кэширование:</label></td><td>
        <?php $v = $frame->config->getFromCms('Site', 'SITEMAP/CACHING'); echo $v ? '<em class="ok"></em>' : '<em class="alert error">отключено</em>';?>
        </td></tr>
    </table>

    <h3>Для статей сайта</h3>
    <table class="grid">
        <tr><td width="500px">
            <label>Приоритетность:</label>
            <div class="note">приоритетность URL относительно других URL на Вашем сайте. Допустимый диапазон значений — от 0,0 до 1,0.</div>
        </td><td valign="top">
        <?php $v = $frame->config->getFromCms('Site', 'SITEMAP/ARTICLES/PRIORITY'); echo $v ? $v : '<em class="alert warning">не указана</em>';?>
        </td></tr>
        <tr><td>
            <label>Частота:</label>
            <div class="note">это значение предоставляет общую информацию для поисковых систем и может не соответствовать точно частоте сканирования этой страницы</div>
        </td><td valign="top">
        <?php $v = $frame->config->getFromCms('Site', 'SITEMAP/ARTICLES/CHANGEFREQ'); echo $v ? $v : '<em class="alert warning">не указана</em>';?>
        </td></tr>
    </table>

    <h3>Для новостей сайта</h3>
    <table class="grid">
        <tr><td width="500px">
            <label>Приоритетность:</label>
            <div class="note">приоритетность URL относительно других URL на Вашем сайте. Допустимый диапазон значений — от 0,0 до 1,0.</div>
        </td><td valign="top">
        <?php $v = $frame->config->getFromCms('Site', 'SITEMAP/NEWS/PRIORITY'); echo $v ? $v : '<em class="alert warning">не указана</em>';?>
        </td></tr>
        <tr><td>
            <label>Частота:</label>
            <div class="note">это значение предоставляет общую информацию для поисковых систем и может не соответствовать точно частоте сканирования этой страницы</div>
        </td><td valign="top">
        <?php $v = $frame->config->getFromCms('Site', 'SITEMAP/NEWS/CHANGEFREQ'); echo $v ? $v : '<em class="alert warning">не указана</em>';?>
        </td></tr>
    </table>

    <h3>Для категорий сайта</h3>
    <table class="grid">
        <tr><td width="500px">
            <label>Приоритетность:</label>
            <div class="note">приоритетность URL относительно других URL на Вашем сайте. Допустимый диапазон значений — от 0,0 до 1,0.</div>
        </td><td valign="top">
        <?php $v = $frame->config->getFromCms('Site', 'SITEMAP/CATEGORIES/PRIORITY'); echo $v ? $v : '<em class="alert warning">не указана</em>';?>
        </td></tr>
        <tr><td>
            <label>Частота:</label>
            <div class="note">это значение предоставляет общую информацию для поисковых систем и может не соответствовать точно частоте сканирования этой страницы</div>
        </td><td valign="top">
        <?php $v = $frame->config->getFromCms('Site', 'SITEMAP/CATEGORIES/CHANGEFREQ'); echo $v ? $v : '<em class="alert warning">не указана</em>';?>
        </td></tr>
    </table>
</section>
<?php
}
?>