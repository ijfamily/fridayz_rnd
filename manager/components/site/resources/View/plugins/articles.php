<?php
/**
 * Gear Manager
 *
 * Плагин             "Информации о статьях сайта"
 * Пакет контроллеров "Просмотр ресурсов сайта"
 * Группа пакетов     "Ресурсы сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Articles
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: articles.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Плагин вывода информации о статьях сайта
 * 
 * @param resource $frame указатель на класс контроллера фрейма
 * @param string $resPath каталог ресурсов контроллера
 * @return void
 */
function plugin_articles($frame, $resPath = '')
{
?>
<h2>Статьи сайта / <a class="view" href="#" component-src="site/articles/articles/~/grid/interface/">просмотреть</a></h2>
<section>
    <img class="glyph" src="<?php echo $resPath;?>/images/icon-article.png" />
    <table class="grid">
    <?php
    $query = GFactory::getQuery();

    echo '<tr><td>Главная страница сайта:</td><td>';
    $sql = "SELECT COUNT(*) `count` FROM `site_articles` WHERE `category_id` IS NULL AND `article_uri`='index.html'";
    if (($rec = $query->getRecord($sql)) === false)
        echo '<em class="alert error">невозможно определить запрос</em>';
    else
    if ($rec['count'] > 0)
        echo '<em class="ok"></em>';
    else
        echo '<em class="alert error">нет главной страницы</em>';
    echo '</td></tr>';

    echo '<tr><td>Без категорий:</td><td>';
    $sql = 'SELECT COUNT(*) `count` FROM `site_articles` WHERE `category_id` IS NULL';
    if (($rec = $query->getRecord($sql)) === false)
        echo '<em class="alert error">невозможно определить запрос</em>';
    else
        echo '<span class="up">', $rec['count'], '</span> ', GString::wordForm($rec['count'], $frame->_['data_records']);
    echo '</td></tr>';

    echo '<tr><td>Имеют скрытый загаловок:</td><td>';
    $sql = 'SELECT COUNT(*) `count` FROM `site_articles` WHERE `article_header`=0';
    if (($rec = $query->getRecord($sql)) === false)
        echo '<em class="alert error">невозможно определить запрос</em>';
    else
        echo '<span class="up">', $rec['count'], '</span> ', GString::wordForm($rec['count'], $frame->_['data_records']);
    echo '</td></tr>';

    echo '<tr><td>В архиве:</td><td>';
    $sql = 'SELECT COUNT(*) `count` FROM `site_articles` WHERE `article_archive`=1';
    if (($rec = $query->getRecord($sql)) === false)
        echo '<em class="alert error">невозможно определить запрос</em>';
    else
        echo '<span class="up">', $rec['count'], '</span> ', GString::wordForm($rec['count'], $frame->_['data_records']);
    echo '</td></tr>';

    echo '<tr><td>Главные в категории:</td><td>';
    $sql = "SELECT COUNT(*) `count` FROM `site_articles` WHERE `article_uri`='index.html'";
    if (($rec = $query->getRecord($sql)) === false)
        echo '<em class="alert error">невозможно определить запрос</em>';
    else
        echo '<span class="up">', $rec['count'], '</span> ', GString::wordForm($rec['count'], $frame->_['data_records']);
    echo '</td></tr>';

    echo '<tr><td>Опубликованных:</td><td>';
    $sql = 'SELECT COUNT(*) `count` FROM `site_articles` GROUP BY `published`';
    if (($rec = $query->getRecord($sql)) === false)
        echo '<em class="alert error">невозможно определить запрос</em>';
    else
        echo '<span class="up">', $rec['count'], '</span> ', GString::wordForm($rec['count'], $frame->_['data_records']);
    echo '</td></tr>';

    echo '<tr><td>Всего статей:</td><td>';
    $sql = 'SELECT COUNT(*) `count` FROM `site_articles`';
    if (($rec = $query->getRecord($sql)) === false)
        echo '<em class="alert error">невозможно определить запрос</em>';
    else
        echo '<span class="up">', $rec['count'], '</span> ', GString::wordForm($rec['count'], $frame->_['data_records']);
    echo '</td></tr>';
    ?>
    </table>
</section>

<h2>Список статей сайта / <a class="edit" href="#" component-src="site/config/site/~/profile/interface/">изменить</a></h2>
<section>
    <img class="glyph" src="<?php echo $resPath;?>/images/icon-article.png" />
    <table class="grid">
        <tr><td><label>Вывод архива статей за указанный день:</label></td><td>
        <?php $v = $frame->config->getFromCms('Site', 'LIST/ARCHIVE/DAY'); echo $v ? '<em class="ok"></em>' : '<em class="alert warning">не указано</em>';?>
        </td></tr>
        <tr><td><label>Вывод архива статей за указанный месяц:</label></td><td>
        <?php $v = $frame->config->getFromCms('Site', 'LIST/ARCHIVE/MONTH'); echo $v ? '<em class="ok"></em>' : '<em class="alert warning">не указано</em>';?>
        </td></tr>
        <tr><td><label>Вывод архива статей за указанный год:</label></td><td>
        <?php $v = $frame->config->getFromCms('Site', 'LIST/ARCHIVE/YEAR'); echo $v ? '<em class="ok"></em>' : '<em class="alert warning">не указано</em>';?>
        </td></tr>
    </table>

    <h3>Параметры списка</h3>
    <table class="grid">
        <tr><td><label>Шаблон:</label></td><td>
        <?php $v = $frame->config->getFromCms('Site', 'LIST/ARCHIVE/TEMPLATE'); echo $v ? $v : '<em class="alert warning">не указано</em>';?>
        </td></tr>
    </table>

    <h3>Параметры списка статей</h3>
    <table class="grid">
        <tr><td><label>Записей на странице:</label></td><td>
        <?php $v = $frame->config->getFromCms('Site', 'LIST/LIMIT'); echo $v ? $v : '<em class="alert warning">не указано</em>';?>
        </td></tr>
        <tr><td><label>Порядок сортировки:</label></td><td>
        <?php
        $v = $frame->config->getFromCms('Site', 'LIST/ORDER');
        echo $v ? $frame->_['data_order'][$v] : '<em class="alert warning">не указан</em>';
        ?>
        </td></tr>
        <tr><td><label>Критерий сортировки:</label></td><td>
        <?php
        $v = $frame->config->getFromCms('Site', 'LIST/SORT');
        echo $v ? $frame->_['data_sort'][$v] : '<em class="alert warning">не указан</em>';?>
        </td></tr>
        <tr><td><label>Шаблон:</label></td><td>
        <?php $v = $frame->config->getFromCms('Site', 'LIST/TEMPLATE'); echo $v ? $v : '<em class="alert warning">не указан</em>';?>
        </td></tr>
        <tr><td><label>Кэширование:</label></td><td>
        <?php $v = $frame->config->getFromCms('Site', 'LIST/CACHING'); echo $v ? '<em class="ok"></em>' : '<em class="alert error">отключено</em>';?>
        </td></tr>
        <tr><td><label>Формат даты:</label></td><td>
        <?php $v = $frame->config->getFromCms('Site', 'LIST/DATE/FORMAT'); echo $v ? $v : '<em class="alert warning">не указано</em>';?>
        </td></tr>
        <tr><td><label>Навигация в списке:</label></td><td>
        <?php
        $v = $frame->config->getFromCms('Site', 'LIST/NAVIGATION');
        echo $v ? $frame->_['data_nav'][$v] : '<em class="alert warning">не указана</em>';
        ?>
        </td></tr>
    </table>
</section>
<?php
}
?>