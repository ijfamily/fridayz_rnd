<?php
/**
 * Gear Manager
 *
 * Плагин             "Информация о сайте"
 * Пакет контроллеров "Просмотр ресурсов сайта"
 * Группа пакетов     "Ресурсы сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Common
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: common.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Плагин вывода общей информации о сайте
 * 
 * @param resource $frame указатель на класс контроллера фрейма
 * @param string $resPath каталог ресурсов контроллера
 * @return void
 */
function plugin_common($frame, $resPath = '')
{
?>
<h2>О сайте / <a class="edit" href="#" component-src="site/config/site/~/profile/interface/">изменить</a></h2>
<section>
    <img class="glyph" src="<?php echo $resPath;?>/images/icon-site.png" />
    <table class="grid">
        <tr><td width="190px">Название сайта:</td><td width="680px">
        <?php $v = $frame->config->getFromCms('Site', 'NAME'); echo $v ? $v : '<em class="alert error">не заполнено</em>';?>
        </td></tr>
        <tr><td valign="top">Название домена:</td><td>
        <?php $v = $frame->config->getFromCms('Site', 'HOST'); echo $v ? $v : '<em class="alert error">не заполнено</em>';?>
        </td></tr>
        <tr><td valign="top">Домашняя страница сайта:</td><td>
        <?php $v = $frame->config->getFromCms('Site', 'HOME'); echo $v ? '<a href="' . $v . '">' . $v . '</a>' : '<em class="alert error">не заполнено</em>';?>
        </td></tr>
        <tr><td valign="top">Шаблон загаловка страницы:</td><td>
        <?php $v = $frame->config->getFromCms('Site', 'TITLE'); echo $v ? $v . '</a>' : '<em class="alert error">не заполнено</em>';?>
        </td></tr>
        <tr><td valign="top">Описание:</td><td>
        <?php $v = $frame->config->getFromCms('Site', 'META/DESCRIPTION'); echo $v ? $v : '<em class="alert error">не заполнено</em>';?>
        </td></tr>
        <tr><td valign="top">Ключевые слова:</td><td>
        <?php $v = $frame->config->getFromCms('Site', 'META/KEYWORDS'); echo $v ? $v : '<em class="alert error">не заполнено</em>';?>
        </td></tr>
        <tr><td valign="top">Кодировка страниц сайта:</td><td>
        <?php $v = $frame->config->getFromCms('Site', 'CHARSET'); echo $v ? $v . '</a>' : '<em class="alert error">не заполнено</em>';?>
        </td></tr>
        <tr><td valign="top">Часовой пояс:</td><td>
        <?php $v = $frame->config->getFromCms('Site', 'TIMEZONE'); echo $v ? $v . '</a>' : '<em class="alert error">не заполнено</em>';?>
        </td></tr>
        <tr><td valign="top">Состояние сайта:</td><td>
        <?php echo $frame->config->getFromCms('Site', 'INACCESSIBLE') ? '<em class="error up">Не доступен (отключен в настройках)</em>' : '<span class="up">Доступен</span>';?>
        </td></tr>
    </table>
</section>

<h2>ЧПУ (человеко-понятный урл) / <a class="edit" href="#" component-src="site/config/site/~/profile/interface/">изменить</a></h2>
<div class="desc">название веб-адреса, содержащего читаемые слова, а не аббревиатуры или идентификаторы базы данных, написанные латинским алфавитом</div>
<section>
    <img class="glyph" src="<?php echo $resPath;?>/images/icon-sef.png" />
    <table class="grid">
        <tr><td width="190px">Использовать ЧПУ :</td><td width="680px">
        <?php echo $frame->config->getFromCms('Site', 'SEF') ? '<em class="ok"></em>' : '<em class="alert warning">отключено</em>';?>
        </td></tr>
        <tr><td valign="top">Вип ЧПУ :</td><td>
        <?php echo $frame->config->getFromCms('Site', 'SEF/TYPE');?>
        </td></tr>
    </table>
</section>

<h2>Управление нескольми доменами / <a class="edit" href="#" component-src="site/config/site/~/profile/interface/">изменить</a></h2>
<div class="desc">позволяет выводить контент сайта в зависимости от того, какой домен привязан к Вашему хостингу (несколько доменов - один хостинг) </div>
<section>
    <img class="glyph" src="<?php echo $resPath;?>/images/icon-domains.png" />
    <table class="grid">
        <tr><td width="190px">Управлять нескольми доменами:</td><td width="680px">
        <?php echo $frame->config->getFromCms('Site', 'OUTCONTROL') ? '<em class="ok"></em>' : '<em class="alert warning">отключено</em>';?>
        </td></tr>
    </table>
</section>

<h2>Безопасность / <a class="edit" href="#" component-src="site/config/site/~/profile/interface/">изменить</a></h2>
<section>
    <img class="glyph" src="<?php echo $resPath;?>/images/icon-shield.png" />
    <table class="grid">
        <tr><td>Ваш IP адрес:</td><td><?php echo $_SERVER['REMOTE_ADDR'];?></td><td></td></tr>
        <tr><td>Ключ безопасности:</td><td>
        <?php $v = $frame->config->getFromCms('Site', 'CMS/TOKEN'); echo $v ? $v : '<em class="alert error">не указан</em>';?>
        </td></tr>
        <tr><td>IP адрес вашего сервера:</td><td>
        <?php $v = $frame->config->getFromCms('Site', 'IP_ADDRESS'); echo $v ? $v : '<em class="alert error">не указан</em>';?>
        </td></tr>
        <tr><td>Разрешать AJAX запросы:</td><td>
        <?php $v = $frame->config->getFromCms('Site', 'AJAX'); echo $v ? '<em class="ok"></em>' : '<em class="alert warning">запрещено</em>';?>
        </td></tr>
    </table>
    <?php $v = $frame->config->getFromCms('Site', 'reCAPTCHA');?>
    <h3>Настройки reCAPTCHA</h3>
    <table class="grid">
        <tr><td width="550px">
            <label>Публичный ключ сервиса reCAPTCHA:</label>
            <div class="note">получить ключ вы можете по ссылке: http://www.google.com/recaptcha Внимание, настоятельно рекомендуется зарегистрироваться на сервисе и сгенерировать для своего сайта уникальную пару ключей, установив разрешение на использование только на своем домене. Использование стандартной пары ключей, не дает должного эффекта по защите от спам роботов</div>
            </td>
            <td valign="top"><?php echo $v['public_key'] ? $v['public_key'] : '<em class="alert warning">не указан</em>';?></td>
        </tr>
        <tr><td>
            <label>Секретный ключ сервиса reCAPTCHA:</label>
            <div class="note">получить ключ вы можете по ссылке: http://www.google.com/recaptcha Внимание, настоятельно рекомендуется зарегистрироваться на сервисе и сгенерировать для своего сайта уникальную пару ключей, установив разрешение на использование только на своем домене. Использование стандартной пары ключей, не дает должного эффекта по защите от спам роботов</div>
            </td>
            <td valign="top"><?php echo $v['private_key'] ? $v['private_key'] : '<em class="alert warning">не указан</em>';?></td>
        </tr>
        <tr><td>
            <label>Оформление reCAPTCHA:</label>
            <div class="note">одна из стандартных тем, которая будет использоваться для оформления reCAPTCHA.</div>
            </td>
            <td valign="top"><?php echo $v['theme'] ? $v['theme'] : '<em class="alert warning">не указана</em>';?></td>
        </tr>
    </table>
    <?php $v = $frame->config->getFromCms('Site', 'kCAPTCHA');?>
    <h3>Настройки kCAPTCHA</h3>
    <table class="grid">
        <tr><td width="350px"><label>Ширина (пкс):</label></td><td><?php echo $v['width'] ? $v['width'] : '<em class="alert warning">не указана</em>';?></td></tr>
        <tr><td><label>Высота (пкс):</label></td><td><?php echo $v['height'] ? $v['height'] : '<em class="alert warning">не указана</em>';?></td></tr>
        <tr><td><label>Количество символов (от,до):</label></td><td><?php echo $v['_length'] ? $v['_length'] : '<em class="alert warning">не указано</em>';?></td></tr>
        <tr><td><label>Символы в шрифте:</label></td><td><?php echo $v['alphabet'] ? $v['alphabet'] : '<em class="alert warning">не указаны</em>';?></td></tr>
        <tr><td><label>Символы в капче:</label></td><td><?php echo $v['allowedSymbols'] ? $v['allowedSymbols'] : '<em class="alert warning">не указаны</em>';?></td></tr>
        <tr><td><label>Символы вертикальной амплитуды флуктуаций:</label></td><td><?php echo $v['fluctuationAmplitude'] ? $v['fluctuationAmplitude'] : '<em class="alert error">не указаны</em>';?></td></tr>
        <tr><td><label>Присутствие шума на белом фоне:</label></td><td><?php echo $v['whiteNoiseDensity'] ? $v['whiteNoiseDensity'] : '<em class="alert warning">не указано</em>';?></td></tr>
        <tr><td><label>Присутствие шума на черном фоне:</label></td><td><?php echo $v['blackNoiseDensity'] ? $v['blackNoiseDensity'] : '<em class="alert warning">не указано</em>';?></td></tr>
        <tr><td><label>Тип изображения:</label></td><td><?php echo $v['type'] ? $v['type'] : '<em class="alert warning">не указан</em>';?></td></tr>
        <?php
            if ($v['backgroundColor'])
                $str = implode(',', $v['backgroundColor']);
            else
                $str = '';
        ?>
        <tr><td><label>Цвет фона в RGB:</label></td><td><?php echo $str ? $str : '<em class="alert warning">не указан</em>';?></td></tr>
        <tr><td><label>Предотвращения пробелов между символами:</label></td><td><?php echo $v['noSpaces'] ? $v['noSpaces'] : '<em class="alert warning">не указано</em>';?></td></tr>
        <tr><td><label>Надпись под капчей:</label></td><td><?php echo $v['showСredits'] ? $v['showСredits'] : '<em class="alert warning">не указана</em>';?></td></tr>
    </table>
</section>
<?php
}
?>