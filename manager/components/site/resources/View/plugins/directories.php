<?php
/**
 * Gear Manager
 *
 * Плагин             "Информация о каталогах сайте"
 * Пакет контроллеров "Просмотр ресурсов сайта"
 * Группа пакетов     "Ресурсы сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Directories
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: directories.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Плагин вывода информации о каталогах сайте
 * 
 * @param resource $frame указатель на класс контроллера фрейма
 * @param string $resPath каталог ресурсов контроллера
 * @return void
 */
function plugin_directories($frame, $resPath = '')
{
?>
<h2>Каталоги сайта / <a class="edit" href="#" component-src="site/config/site/~/profile/interface/">изменить</a></h2>
<section>
    <img class="glyph" src="<?php echo $resPath;?>/images/icon-dir.png" />
    <table class="grid">
        <tr><td>Корень сайта:</td><td><span class="icon folder-root"></span><?php echo getcwd(); ?></td></tr>
    </table>

    <h3>Контент сайта</h3>
    <table class="grid">
        <tr><td width="165px">Каталог категорий:</td><td><span class="icon folder"></span>
        <?php plugin_dir_site(DOCUMENT_ROOT . $frame->config->getFromCms('Site', 'DIR/CATEGORIES'));?>
        </tr>
        <tr><td>Каталог аудиофайлов:</td><td><span class="icon folder"></span>
        <?php plugin_dir_site(DOCUMENT_ROOT . $frame->config->getFromCms('Site', 'DIR/AUDIO'));?>
        </tr>
        <tr><td>Каталог видеофайлов:</td><td><span class="icon folder"></span>
        <?php plugin_dir_site(DOCUMENT_ROOT . $frame->config->getFromCms('Site', 'DIR/VIDEO'));?>
        </tr>
        <tr><td>Каталог документов:</td><td><span class="icon folder"></span>
        <?php plugin_dir_site(DOCUMENT_ROOT . $frame->config->getFromCms('Site', 'DIR/DOCS'));?>
        </tr>
        <tr><td>Каталог баннеров:</td><td><span class="icon folder"></span>
        <?php plugin_dir_site(DOCUMENT_ROOT . $frame->config->getFromCms('Site', 'DIR/BANNERS'));?>
        </tr>
        <tr><td>Каталог слайдеров:</td><td><span class="icon folder"></span>
        <?php plugin_dir_site(DOCUMENT_ROOT . $frame->config->getFromCms('Site', 'DIR/SLIDERS'));?>
        </tr>
        <tr><td>Каталог изображений:</td><td><span class="icon folder"></span>
        <?php plugin_dir_site(DOCUMENT_ROOT . $frame->config->getFromCms('Site', 'DIR/IMAGES'));?>
        </tr>
        </tr>
        <tr><td>Каталог фотоальбомов:</td><td><span class="icon folder"></span>
        <?php plugin_dir_site(DOCUMENT_ROOT . $frame->config->getFromCms('Albums', 'DIR'));?>
        </tr>
        <tr><td>Каталог фотогалерей:</td><td><span class="icon folder"></span>
        <?php plugin_dir_site(DOCUMENT_ROOT . $frame->config->getFromCms('Galleries', 'DIR'));?>
        </tr>
        <tr><td>Временный каталог:</td><td><span class="icon folder-temp"></span>
        <?php plugin_dir_site(DOCUMENT_ROOT . $frame->config->getFromCms('Site', 'DIR/TEMP'));?>
        </tr>
        <tr><td>Каталог данных:</td><td><span class="icon folder-data"></span>
        <?php plugin_dir_site(DOCUMENT_ROOT . $frame->config->getFromCms('Site', 'DIR/DATA'));?>
        </tr>
    </table>

    <h3>Настройки сайта</h3>
    <table class="grid">
        <tr><td width="155px">Каталог настроек:</td><td><span class="icon folder"></span>
        <?php plugin_dir_site(DOCUMENT_ROOT . 'application/config/');?>
        </tr>
    </table>

    <h3>Темы сайта</h3>
    <table class="grid">
        <tr><td width="155px">Каталог тем:</td><td><span class="icon folder"></span>
        <?php plugin_dir_site(DOCUMENT_ROOT . $frame->config->getFromCms('Site', 'DIR/THEMES'));?>
        </tr>
    </table>

    <h3>Константы панели управления сайтом</h3>
    <table class="grid">
        <tr><td>Корень сайта:</td><td><span class="icon sfolder-root"></span><?php echo DOCUMENT_ROOT;?></td></tr>
        <tr><td>Каталог приложения:</td><td><span class="icon sfolder"></span><?php echo PATH_APPLICATION;?></td></tr>
        <tr><td>Роутер запросов:</td><td><span class="icon sfolder"></span><?php echo ROUTER_SCRIPT;?></td></tr>
        <tr><td>Разделитель запросов роутера:</td><td><span class="icon sfolder"></span><?php echo ROUTER_DELIMITER;?></td></tr>
        <tr><td>Название ядра:</td><td><span class="icon sfolder"></span><?php echo CORE_NAME;?></td></tr>
        <tr><td>Каталог языков:</td><td><span class="icon sfolder"></span><?php echo PATH_LANGUAGE;?></td></tr>
        <tr><td>Каталог компонентов системы:</td><td><span class="icon sfolder"></span><?php echo PATH_COMPONENT;?></td></tr>
        <tr><td>Каталог настроек системы:</td><td><span class="icon sfolder"></span><?php echo PATH_CONFIG;?></td></tr>
    </table>
</section>
<?php
}

/**
 * Вывод информации о каталоге сайта
 * 
 * @param string $dir название каталога
 * @return void
 */
function plugin_dir_site($dir)
{
    echo realpath($dir), '</td>';
    if (!file_exists($dir)) {
        echo '<em class="alert error">каталог не существует</em></td>';
        return;
    }
    echo '<td>';
    if (($time = filemtime($dir)) !== false) {
        echo date('d-m-Y H:i:s', $time);
    } else {
        echo '<em class="alert error">невозможно определить дату изменения</em></td>';
        return;
    }
    echo '</td><td>';
    if (($perms = fileperms($dir)) === false) {
        echo '<em class="alert error">невозможно определить права доступа к каталогу</em></td>';
        return;
    } else
        echo GFile::filePermsDigit($perms), ' (', GFile::filePermsInfo($perms), ')';
    echo '<td>';
    $stat = GDir::getStat($dir);
    echo '<td>папок / файлов: <span class="up">', (int) $stat['dir'], ' / ', $stat['files'], '</span></td>';
    echo '<td><span class="icon data"></span> <span class="up">', GFile::fileSize($stat['size']), '</span></td>';
}
?>