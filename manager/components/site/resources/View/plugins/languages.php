<?php
/**
 * Gear Manager
 *
 * Плагин             "Информация о локализации сайта"
 * Пакет контроллеров "Просмотр ресурсов сайта"
 * Группа пакетов     "Ресурсы сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Languages
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: languages.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Плагин вывода информация о локализации сайта
 * 
 * @param resource $frame указатель на класс контроллера фрейма
 * @param string $resPath каталог ресурсов контроллера
 * @return void
 */
function plugin_languages($frame, $resPath = '')
{
?>
<h2>Локализация панели управления сайтом / <a class="view" href="#" component-src="system/settings/languages/~/grid/interface/">просмотреть</a></h2>
<section>
    <img class="glyph" src="<?php echo $resPath;?>/images/icon-language.png" />
    <table class="grid">
    <?php
    $query = GFactory::getQuery();
    $sql = "SELECT * FROM `gear_languages` WHERE `language_default`=1";
    if (($rec = $query->getRecord($sql)) === false)
        echo '<tr><td><label>Основной язык:</label></td><td><em class="alert error">невозможно определить запрос</em></td></tr>';
    else
    if (empty($rec))
        echo '<tr><td><label>Основной язык:</label></td><td><em class="alert error">неопределен язык</em></td></tr>';
    else
        echo '<tr><td><label>Основной язык:</label></td><td>', $rec['language_name'], '</td></tr>';
    ?>
    </table>

    <h3>Доступные языки</h3>
<?php
    $sql = "SELECT * FROM `gear_languages` ORDER BY `language_id` ASC";
    if (($rec = $query->getRecord($sql)) === false)
        echo '<em class="alert error">невозможно определить запрос</em>';
    else {
        if ($rec['count'] = 0)
            echo '<em class="alert error>нет доступных языков</em>';
        else {
?>
    <table class="grid">
<?php
    $default = array();
    while (!$query->eof()) {
        $rec = $query->next();
        if ($rec['language_default'])
            $default = $rec;
        echo '<tr><td width="110px">', $rec['language_name'], '</td><td>', $rec['language_alias'], '</td><td>', ($rec['language_enabled'] ? '<em class="ok"></em>' : '<em class="alert error">не доступен</em>'), '</td></tr>';
    }
?>
    </table>
<?php
        }
    }
?>
</section>

<h2>Локализация сайта / <a class="view" href="#" component-src="site/languages/~/grid/interface/">просмотреть</a></h2>
<section>
    <img class="glyph" src="<?php echo $resPath;?>/images/icon-language.png" />
    <table class="grid">
    <?php
    $langs = $frame->config->getFromCms('Site', 'LANGUAGES');
    $defaultId = $frame->config->getFromCms('Site', 'LANGUAGE/ID');
    $default = array();
    foreach($langs as $alias => $item) {
        if ($item['id'] == $defaultId)
            $default = $item;
    }
    if (empty($default))
        echo '<tr><td><label>Основной язык:</label></td><td><em class="alert error">неопределен язык</em></td></tr>';
    else
        echo '<tr><td><label>Основной язык:</label></td><td>', $default['title'], '</td></tr>';
    ?>
    </table>

    <h3>Доступные языки</h3>
<?php
    if (empty($langs))
        echo '<em class="alert error">нет доступных языков</em>';
?>
    <table class="grid">
<?php
    foreach($langs as $alias => $item) {
        echo '<tr><td width="110px">', $item['title'], '</td><td>', $alias, '</td><td><em class="ok"></em></td></tr>';
    }
?>
    </table>
</section>
<?php
}
?>