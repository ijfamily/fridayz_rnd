<?php
/**
 * Gear Manager
 *
 * Плагин             "Информация о настройках сайта"
 * Пакет контроллеров "Просмотр ресурсов сайта"
 * Группа пакетов     "Ресурсы сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Config
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: config.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Плагин вывода информации о настройках сайта
 * 
 * @param resource $frame указатель на класс контроллера фрейма
 * @param string $resPath каталог ресурсов контроллера
 * @return void
 */
function plugin_config($frame, $resPath = '')
{
?>
<h2>Сценарии настройки компонентов сайта</h2>
<section class="dir">
    <img class="glyph" src="<?php echo $resPath;?>/images/icon-config.png" />
    <table class="grid">
    <?php
    $dir = DOCUMENT_ROOT . '/application/config/';
    $src = array(
        'Albums.php'    => array('name' => 'Фотоальбом', 'src' => 'site/albums/config/~/profile/interface/'),
        'Domains.php'   => array('name' => 'Подключение нескольких доменов', 'src' => 'site/config/domains/~/profile/interface/'),
        'Forms.php'     => array('name' => 'Форма', 'src' => 'site/config/forms/~/profile/interface/'),
        'Galleries.php' => array('name' => 'Фотогалерея', 'src' => 'site/galleries/config/~/profile/interface/'),
        'Locking.php'   => array('name' => 'Блокировка IP адресов', 'src' => 'site/config/locking/~/profile/interface/'),
        'Mimes.php'     => array('name' => 'Список MIMES', 'src' => 'site/config/mimes/~/profile/interface/'),
        'Robots.php'    => array('name' => 'Поисковые роботы', 'src' => 'site/robots/config/~/profile/interface/'),
        'Router.php'    => array('name' => 'Маршрутизатор', 'src' => 'site/config/routing/~/profile/interface/'),
        'Site.php'      => array('name' => 'Сайт', 'src' => 'site/config/site/~/profile/interface/'),
        'Social.php'    => array('name' => 'Социальные сети', 'src' => 'site/social/config/~/profile/interface/'),
        'System.php'    => array('name' => 'Система', 'src' => ''),
        'Users.php'     => array('name' => 'Пользователи', 'src' => ''),
    );
    if (!file_exists($dir)) {
        echo '<tr><td><em class="alert error">каталог не существует</em></td></tr>';
        return;
    }
    $list = array();
    GDir::getListFiles($list, $dir);
    foreach ($list as $index => $file) {
        $filename = $file['dirname'];
        echo '<tr>';
        $exist = isset($src[$file['basename']]);
        if ($exist)
            echo '<td>', $src[$file['basename']]['name'], '</td>';
        else
            echo '<td><em class="alert error">неизвестный сценарий</em></td>';
        echo '<td><span class="icon config"></span> ', $file['basename'], '</td>';
        echo '<td>';
        if (($time = filemtime($filename)) !== false) {
            echo date('d-m-Y H:i:s', $time);
        } else {
            echo '<em class="alert error">невозможно определить дату изменения файла</em></td><tr>';
        }
        echo '</td><td>';
        if (($perms = fileperms($filename)) === false) {
            echo '<em class="alert error">невозможно определить права доступа к каталогу</em></td><tr>';
            return;
        } else
            echo GFile::filePermsDigit($perms), ' (', GFile::filePermsInfo($perms), ')';
        echo '</td>';
        echo '<td><span class="icon data"></span> <span class="up">', GFile::getFileSize($file['dirname']) , '</span></td><td>';
        if ($exist && $src[$file['basename']]['src'])
            echo '<a class="edit" href="#" component-src="' . $src[$file['basename']]['src'] . '">изменить</a>';
        echo '</td></tr>';
    }
    ?>
    </table>
</section>

<h2>Настройки компонентов сайта</h2>
<section>
    <img class="glyph" src="<?php echo $resPath;?>/images/icon-settings.png" />
    <?php
    $query = GFactory::getQuery();
    $sql = 'SELECT `c`.* '
         . 'FROM (SELECT `c`.*, `cl`.`controller_profile` '
         . 'FROM `gear_controllers` `c` JOIN `gear_controllers_l` `cl` '
         . 'ON `c`.`controller_id`=`cl`.`controller_id` AND `c`.`controller_menu_config`=1 AND '
         . '`c`.`controller_visible`=1  AND `cl`.`language_id`=' . $frame->language->get('id') . ') `c` '
         . ' ORDER BY `c`.`module_id`, `c`.`controller_profile` ASC';
    if ($query->execute($sql) === false)
        echo '<em class="alert error">невозможно выполнить запрос</em>';
    else {
        while (!$query->eof()) {
            $rec = $query->next();
            $src = $rec['controller_uri_pkg'] . ROUTER_DELIMITER . $rec['controller_uri_action'];
            $img = '/' . PATH_APPLICATION . PATH_COMPONENT . $rec['controller_uri'] . 'resources/shortcut.png';
            echo '<a class="box" href="#" component-src="', $src, '"><img width="32px" height="32px" src="', $img, '" align="absmiddle">' , $rec['controller_profile'], '</a>';
        }
    }
    ?>
    <div class="wrap"></div>
</section>
<?php
}
?>