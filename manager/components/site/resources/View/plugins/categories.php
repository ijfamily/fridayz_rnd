<?php
/**
 * Gear Manager
 *
 * Плагин             "Информации о категориях статьях сайта"
 * Пакет контроллеров "Просмотр ресурсов сайта"
 * Группа пакетов     "Ресурсы сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Categories
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: categories.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Плагин вывода информации о категориях статьях сайта
 * 
 * @param resource $frame указатель на класс контроллера фрейма
 * @param string $resPath каталог ресурсов контроллера
 * @return void
 */
function plugin_categories($frame, $resPath = '')
{
?>
<h2>Категории сайта / <a class="view" href="#" component-src="site/references/acategories/~/grid/interface/">просмотреть</a></h2>
<section>
    <img class="glyph" src="<?php echo $resPath;?>/images/icon-category.png" />
    <table class="grid">
    <?php
    $query = GFactory::getQuery();

    echo '<tr><td>Имеют список:</td><td>';
    $sql = 'SELECT COUNT(*) `count` FROM `site_categories` WHERE `list`=1';
    if (($rec = $query->getRecord($sql)) === false)
        echo '<em class="alert error">невозможно определить запрос</em>';
    else
         echo '<span class="up">', $rec['count'], '</span> ', GString::wordForm($rec['count'], $frame->_['data_records']);
    echo '</td></tr>';

    echo '<tr><td>Опубликованных:</td><td>';
    $sql = 'SELECT COUNT(*) `count` FROM `site_categories` WHERE `category_visible`=0';
    if (($rec = $query->getRecord($sql)) === false)
        echo '<em class="alert error">невозможно определить запрос</em>';
    else
         echo '<span class="up">', $rec['count'], '</span> ', GString::wordForm($rec['count'], $frame->_['data_records']);
    echo '</td></tr>';

    echo '<tr><td>Всего категорий:</td><td>';
    $sql = 'SELECT COUNT(*) `count` FROM `site_categories`';
    if (($rec = $query->getRecord($sql)) === false)
        echo '<em class="alert error">невозможно определить запрос</em>';
    else
        echo '<span class="up">', $rec['count'], '</span> ', GString::wordForm($rec['count'], $frame->_['data_records']);
    echo '</td></tr>';
    ?>
    </table>
</section>
<?php
}
?>