<?php
/**
 * Gear Manager
 *
 * Плагин             "Информация о доступных расширениях для загрузки"
 * Пакет контроллеров "Просмотр ресурсов сайта"
 * Группа пакетов     "Ресурсы сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Extensions
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: extensions.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Плагин вывода информации о доступных расширениях для загрузки
 * 
 * @param resource $frame указатель на класс контроллера фрейма
 * @param string $resPath каталог ресурсов контроллера
 * @return void
 */
function plugin_extensions($frame, $resPath = '')
{
?>
<h2>Загрузка файлов на сервер / <a class="edit" href="#" component-src="site/config/site/~/profile/interface/">изменить</a></h2>
<section>
    <img class="glyph" src="<?php echo $resPath;?>/images/icon-upload.png" />
    <p>Доступные расширения для загрузки:</p>
    <table class="grid">
        <tr><td>Изображения:</td><td>
        <?php $v = $frame->config->getFromCms('Site', 'FILES/EXT/IMAGES'); echo $v ? $v : '<em class="alert error">не указаны</em>';?>
        </td></tr>
        <tr><td>Документы:</td><td>
        <?php $v = $frame->config->getFromCms('Site', 'FILES/EXT/DOCS'); echo $v ? $v : '<em class="alert error">не указаны</em>';?>
        </td></tr>
        <tr><td>Аудио:</td><td>
        <?php $v = $frame->config->getFromCms('Site', 'FILES/EXT/AUDIO'); echo $v ? $v : '<em class="alert error">не указаны</em>';?>
        </td></tr>
        <tr><td>Видео:</td><td>
        <?php $v = $frame->config->getFromCms('Site', 'FILES/EXT/VIDEO'); echo $v ? $v : '<em class="alert error">не указаны</em>';?>
        </td></tr>
    </table>
</section>
<?php
}
?>