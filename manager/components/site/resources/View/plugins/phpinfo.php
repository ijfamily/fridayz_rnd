<?php
/**
 * Gear Manager
 *
 * Плагин             "Информация о PHP"
 * Пакет контроллеров "Просмотр ресурсов сайта"
 * Группа пакетов     "Ресурсы сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    PHP
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: php.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Возращает php ini
 * 
 * @param  string раздел php ini
 * @return string
 */
function echo_ini($name)
{
    $value = ini_get($name);

    if (is_null($value)) {
        echo '<em class="alert warning">неизвестно</em>'; return;
    }
    if ($value === '1') {
        echo '<em class="ok"></em>'; return;
    }
    if ($value === '0' || strlen($value) == 0) {
        echo '<em class="alert error">отключено</em>'; return;
    }

    echo '<span class="up">', $value, '</span>';
}

/**
 * Плагин вывода общей информации о PHP
 * 
 * @param resource $frame указатель на класс контроллера фрейма
 * @param string $resPath каталог ресурсов контроллера
 * @return void
 */
function plugin_phpinfo($frame, $resPath = '')
{
    version_compare()
?>
<h2>Настройки PHP</h2>
<section>
    <img class="glyph" src="<?php echo $resPath;?>/images/icon-phpinfo.png" />
    <table class="grid">
        <tr><td valign="top" width="400px">Версия PHP :</td><td>
        <?php if ($v = phpversion()) : ?> <span class="up"><?=$v?></span>
        <?php else : ?><em class="alert error">неизвестно</em><?php endif; ?>
        </td></tr>
        <tr><td valign="top">Список имен всех скомпилированных и загруженных в PHP интерпретаторе модулей:</td><td>
        <?php $v = get_loaded_extensions(); echo $v ? implode(', ', $v) : '<em class="alert error">неизвестно</em>';?>
        </td></tr>
    </table>

    <h3>Настройки конфигурации протоколирования событий и ошибок</h3>
    <table class="grid">
        <tr><td width="600px"><label>display_errors:</label>
            <div class="note">эта настройка определяет, требуется ли выводить ошибки на экран вместе с остальным выводом, либо ошибки должны быть скрыты от пользователя. </div>
        </td><td valign="top"><?php echo_ini('display_errors');?></td>
        </tr>

        <tr><td><label>display_startup_errors:</label>
            <div class="note">даже если display_errors включена, ошибки, возникающие во время запуска PHP, не будут отображаться. Настойчиво рекомендуем включать директиву display_startup_errors только для отладки</div>
        </td><td valign="top"><?php echo_ini('display_startup_errors');?></td>
        </tr>

        <tr><td><label>log_errors:</label>
            <div class="note">отвечает за выбор журнала, в котором будут сохраняться сообщения об ошибках. Это может быть журнал сервера или error_log. Применимость этой настройки зависит от конкретного сервера. </div>
        </td><td valign="top"><?php echo_ini('log_errors');?></td>
        </tr>
    </table>

    <h3>Конфигурационные Опции Закачивания Файлов</h3>
    <table class="grid">
        <tr><td width="300px"><label>file_uploads:</label>
            <div class="note">разрешать или не разрешать закачивание файлов.</div>
        </td><td valign="top"><?php echo_ini('file_uploads');?></td>
        </tr>

        <tr><td><label>upload_max_filesize:</label>
            <div class="note">максимальный размер закачиваемого файла.</div>
        </td><td valign="top"><?php echo_ini('upload_max_filesize');?></td>
        </tr>

        <tr><td><label>max_file_uploads:</label>
            <div class="note">максимально разрешенное количество одновременно закачиваемых файлов.</div>
        </td><td valign="top"><?php echo_ini('max_file_uploads');?></td>
        </tr>
    </table>

    <h3>Конфигурационные Опции Путей и Директорий</h3>
    <table class="grid">
        <tr><td width="600px"><label>open_basedir:</label>
            <div class="note">ограничивает указанным деревом каталогов файлы, которые могут быть доступны для PHP, включая сам файл. Эта директива НЕ подвержена влиянию безопасного режима.</div>
        </td><td valign="top"><?php echo_ini('open_basedir');?></td>
        </tr>

        <tr><td><label>doc_root:</label>
            <div class="note">"Корневая директория" PHP на этом сервере. Используется только в случае, если не пустая.</div>
        </td><td valign="top"><?php echo_ini('doc_root');?></td>
        </tr>

        <tr><td><label>user_dir:</label>
            <div class="note">базовое имя директории, используемой в домашнем каталоге пользователя для PHP файлов, например, public_html.</div>
        </td><td valign="top"><?php echo_ini('user_dir');?></td>
        </tr>
    </table>

    <h3>Конфигурационные опции, управляющие безопасным режимом и вопросами безопасности</h3>
    <table class="grid">
        <tr><td width="300px"><label>safe_mode:</label>
            <div class="note">включает/отключает безопасный режим в PHP.</div>
        </td><td valign="top"><?php echo_ini('safe_mode');?></td>
        </tr>
    </table>

    <h3>Настройка во время выполнения </h3>
    <table class="grid">
        <tr><td width="600px"><label>max_execution_time:</label>
            <div class="note">эта директива задает максимальное время в секундах, в течение которого скрипт должен полностью загрузиться. Если этого не происходит, анализатор завершает его работу. Этот механизм помогает предотвратить зависание сервера из-за криво написанного скрипта. По умолчанию на загрузку дается 30 секунд.</div>
        </td><td valign="top"><?php echo_ini('max_execution_time');?></td>
        </tr>

        <tr><td><label>max_input_time:</label>
            <div class="note">эта директива задает максимальное время в секундах, в течение которого скрипт должен разобрать все входные данные, переданные запросами вроде POST или GET. Это время измеряется от момента, когда PHP вызван на сервере до момента, когда скрипт начинает выполняться.</div>
        </td><td valign="top"><?php echo_ini('max_input_time');?></td>
        </tr>

        <tr><td><label>max_input_nesting_level:</label>
            <div class="note">максимальная глубина вложенности входных переменных.</div>
        </td><td valign="top"><?php echo_ini('max_input_nesting_level');?></td>
        </tr>

        <tr><td><label>max_input_vars:</label>
            <div class="note">сколько входных переменных может быть принято в одном запросе (ограничение накладывается на каждую из глобальных переменных $_GET, $_POST и $_COOKIE отдельно). Использование этой директивы снижает вероятность сбоев в случае атак с использованием хеш-коллизий. </div>
        </td><td valign="top"><?php echo_ini('max_input_vars');?></td>
        </tr>

        <tr><td><label>magic_quotes_gpc:</label>
            <div class="note">задает режим magic_quotes для GPC (Get/Post/Cookie) операций. Если magic_quotes включен, все ' (одинарные кавычки), " (двойные кавычки), \ (обратный слеш) и NUL экранируются обратным слешем автоматически.</div>
        </td><td valign="top"><?php echo_ini('magic_quotes_gpc');?></td>
        </tr>

        <tr><td><label>magic_quotes_sybase:</label>
            <div class="note">если включена директива magic_quotes_sybase, она замещает magic_quotes_gpc. Если включены обе директивы, то экранироваться будут только одинарные кавычки, причем экранированы они будут теми же кавычками: ''. Двойные кавычки, обратные слеши и NUL при этом останутся нетронутыми и неэкранированными.</div>
        </td><td valign="top"><?php echo_ini('magic_quotes_sybase');?></td>
        </tr>
    </table>

    <h3>Опции языка и прочих настроек</h3>
    <table class="grid">
        <tr><td width="600px"><label>short_open_tag:</label>
            <div class="note">эта настройка определяет, разрешается ли короткая форма записи (&lt;? ?&gt;) тегов PHP.</div>
        </td><td valign="top"><?php echo_ini('short_open_tag');?></td>
        </tr>

        <tr><td valign="top">Список классов не доступных по причинам безопасности:</td><td>
        <?php echo_ini('disable_classes');?>
        </td></tr>

        <tr><td valign="top">Список функций не доступных по причинам безопасности:</td><td>
        <?php echo_ini('disable_functions');?>
        </td></tr>
    </table>

    <h3>Ограничения ресурсов</h3>
    <table class="grid">
        <tr><td width="600px"><label>memory_limit:</label>
            <div class="note">эта директива задает максимальный объем памяти в байтах, который разрешается использовать скрипту. Это помогает предотвратить ситуацию, при которой плохо написанный скрипт съедает всю доступную память сервера. </div>
        </td><td valign="top"><?php echo_ini('memory_limit');?></td>
        </tr>
    </table>

    <h3>Конфигурационные опции обработки данных</h3>
    <table class="grid">
        <tr><td width="600px"><label>register_globals:</label>
            <div class="note">регистрировать или нет переменные EGPCS (Environment, GET, POST, Cookie, Server) в качестве глобальных переменных.</div>
        </td><td valign="top"><?php echo_ini('register_globals');?></td>
        </tr>

        <tr><td><label>post_max_size:</label>
            <div class="note">устанавливает максимально допустимый размер данных, отправляемых методом POST. Это значение также влияет на загрузку файлов.</div>
        </td><td valign="top"><?php echo_ini('post_max_size');?></td>
        </tr>

    </table>

    <h3>Настройка производительности</h3>
    <table class="grid">
        <tr><td width="600px"><label>realpath_cache_size:</label>
            <div class="note">определяет размера кэша realpath, используемого в PHP. Это значение должно быть увеличено на системах, в которых PHP открывает большое количество файлов соответственно количеству выполняемых файловых операций. </div>
        </td><td valign="top"><?php echo_ini('realpath_cache_size');?></td>
        </tr>

        <tr><td><label>realpath_cache_ttl:</label>
            <div class="note">время (в секундах) в течение которого будет использован кэш realpath для указанного файла или директории. Для систем с редко меняющимися файлами это значение можно увеличить.</div>
        </td><td valign="top"><?php echo_ini('realpath_cache_ttl');?></td>
        </tr>
    </table>

    <h3>Конфигурационные опции отправки писем</h3>
    <table class="grid">
        <tr><td width="600px"><label>smtp_port:</label>
            <div class="note">использутся только в Windows: порт SMTP-сервера, к которому будет обращаться PHP при отправке почты функцией mail(); по умолчанию 25.</div>
        </td><td valign="top"><?php echo_ini('smtp_port');?></td>
        </tr>

        <tr><td><label>sendmail_from:</label>
            <div class="note">адрес, который будет использоваться в заголовке "From:" в письмах, посылаемых PHP в Windows.</div>
        </td><td valign="top"><?php echo_ini('sendmail_from');?></td>
        </tr>

        <tr><td><label>sendmail_path:</label>
            <div class="note">путь до программы sendmail, обычно /usr/sbin/sendmail или /usr/lib/sendmail.</div>
        </td><td valign="top"><?php echo_ini('sendmail_path');?></td>
        </tr>
    </table>
</section>
<?php
}
?>