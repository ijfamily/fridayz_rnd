<?php
/**
 * Gear Manager
 *
 * Плагин             "Информация о идексации сайта"
 * Пакет контроллеров "Просмотр ресурсов сайта"
 * Группа пакетов     "Ресурсы сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Indexes
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: indexes.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Плагин вывода общей информации о идексации сайта
 * 
 * @param resource $frame указатель на класс контроллера фрейма
 * @param string $resPath каталог ресурсов контроллера
 * @return void
 */
function plugin_indexes($frame, $resPath = '')
{
?>
<h2>Идексация сайта</h2>
<section>
    <img class="glyph" src="<?php echo $resPath;?>/images/icon-index.png" />
    <table class="grid">
        <tr><td>Идексация сайта в настройках: </td><td>
        <?php echo $frame->config->getFromCms('Site', 'NOTINDEX') ? '<em class="alert error">отключена (счётчики не работают)</em>' : '<em class="ok"></em>';?>
        </td></tr>
        <tr><td>Счётчики на сайте: </td><td><em class="ok"></em></td></tr>
        <tr><td>Статистика посещаемости роботов: </td><td>
        <?php echo !$frame->config->getFromCms('Site', 'ROBOTS') ? '<em class="alert error">статистика отключена</em>' : '<em class="ok"></em>';?>
        </td></tr>
        <tr><td>Количество роботов посетивших сайт: </td>
        <?php 
        $count = get_count_robots();
        if ($count === false)
            echo '<td><span class="up">0</span> записей</td><td><em class="alert error">ошибка запроса</em></td>';
        else
            echo '<td>', $count, '</td>';
        ?>
        </tr>
        <tr><td>Статьи с индексацией: </td>
        <?php 
        $count = get_count_article_indexes($frame->config->getFromCms('Site', 'LANGUAGE/ID'));
        if ($count === false)
            echo '<td><span class="up">0</span> записей</td><td><em class="alert error">ошибка запроса</em></td>';
        else
            echo '<td>', $count, '</td>';
        ?>
        </tr>
        <tr><td valign="top">Файл (robots.txt) ограничения<br />доступа к содержимому роботам:</td><td>
        <?php
        $filename = DOCUMENT_ROOT . 'robots.txt';
        if (!file_exists($filename)) {
            echo '<em class="alert error">файл не существует</em>';
            return;
        }
        $text = file_get_contents($filename, true);
        if ($text === false) {
            echo '<em class="alert error">нет доступа к файлу</em>';
            return;
        }
        if (($perms = fileperms($filename)) === false) {
            echo '<em class="alert error">невозможно определить права доступа к файлу</em>';
        }
        echo 'Доступ: ', GFile::filePermsDigit($perms), ' (', GFile::filePermsInfo($perms), ')';
        echo '<pre>', $text, '</pre>';
        ?>
        </td></tr>
    </table>
</section>
<?php
}

/**
 * Возращает количество роботов
 * 
 * @return string
 */
function get_count_robots()
{
    $query = GFactory::getQuery();
    $sql = 'SELECT `bot_name`, COUNT(*) `count` FROM `site_bots` GROUP BY `bot_name`';
    if (($query->execute($sql)) === false)
        return false;
    $arr = array();
    $count = 0;
    while (!$query->eof()) {
        $rec = $query->next();
        $count += (int) $rec['count'];
        $arr[] = $rec['bot_name'] . ' - <span class="up">' . $rec['count'] . '</span>';
    }

    return 'всего <span class="up">' . $count . '</span> (' . implode(', ', $arr) . ')';
}

/**
 * Возращает количество статей
 * 
 * @param integer $langId идент. языка
 * @return string
 */
function get_count_article_indexes($langId)
{
    $query = GFactory::getQuery();
    $sql = 'SELECT `page_meta_robots`, COUNT(*) `count` FROM `site_pages` WHERE `language_id`=' . $langId . ' GROUP BY `page_meta_robots`';
    if (($query->execute($sql)) === false)
        return false;
    $arr = array();
    $count = 0;
    while (!$query->eof()) {
        $rec = $query->next();
        $count += (int) $rec['count'];
        $arr[] = $rec['page_meta_robots'] . ' - <span class="up">' . $rec['count'] . '</span>';
    }

    return 'всего <span class="up">' . $count . '</span> (' . implode(', ', $arr) . ')';
}
?>