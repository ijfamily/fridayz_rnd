<?php
/**
 * Gear Manager
 *
 * Плагин             "Информация о доступной памяти"
 * Пакет контроллеров "Просмотр ресурсов сайта"
 * Группа пакетов     "Ресурсы сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Memory
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: memory.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Плагин вывода информации о доступной памяти
 * 
 * @param resource $frame указатель на класс контроллера фрейма
 * @param string $resPath каталог ресурсов контроллера
 * @return void
 */
function plugin_memory($frame, $resPath = '')
{
    $spaceDir = '.';
    $totalSpace = disk_total_space($spaceDir);
    $freeSpace = disk_free_space($spaceDir);
    $useSpace = $totalSpace - $freeSpace;

?>
<h2>Использование дискового пространства</h2>
<section>
    <img class="glyph" src="<?php echo $resPath;?>/images/icon-memory.png" />
    <table class="grid">
        <tr><td>Всего памяти:</td><td><span class="up"><?php echo byte_to_str($totalSpace, $frame->_['data_bytes']);?></span></td></tr>
        <tr><td>Доступно памяти:</td><td><span class="up"><?php echo byte_to_str($freeSpace, $frame->_['data_bytes']);?></span></td></tr>
        <tr><td>Использовано памяти:</td><td><span class="up"><?php echo byte_to_str($useSpace,$frame->_['data_bytes']);?></span></td></tr>
    </table>
</section>
<?php
}

/**
 * Перевод байтов в строку
 * 
 * @param integer $bytes байты
 * @return string
 */
function byte_to_str($bytes, $str)
{
    $base = 1024;
    $class = min((int)log($bytes , $base) , count($str) - 1);

    return sprintf('%1.2f' , $bytes / pow($base,$class)) . ' ' . $str[$class];
}
?>