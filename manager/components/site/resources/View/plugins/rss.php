<?php
/**
 * Gear Manager
 *
 * Плагин             "Информация о RSS"
 * Пакет контроллеров "Просмотр ресурсов сайта"
 * Группа пакетов     "Ресурсы сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    RSS
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: rss.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Плагин вывода общей информации о RSS
 * 
 * @param resource $frame указатель на класс контроллера фрейма
 * @param string $resPath каталог ресурсов контроллера
 * @return void
 */
function plugin_rss($frame, $resPath = '')
{
?>
<h2>RSS лента / <a class="edit" href="#" component-src="site/config/site/~/profile/interface/">изменить</a></h2>
<div class="desc">RSS (англ. Rich Site Summary — обогащённая сводка сайта) — семейство XML-форматов, предназначенных для описания лент новостей, анонсов статей, изменений в блогах и т. п.</div>
<section>
    <img class="glyph" src="<?php echo $resPath;?>/images/icon-rss.png" />
    <table class="grid">
        <tr><td width="250px">Использовать RSS экспорт новостей:</td><td width="680px">
        <?php $v = $frame->config->getFromCms('Site', 'RSS'); echo $v ? '<em class="ok"></em>' : '<em class="alert error">отключено</em>';?>
        </td></tr>
        <tr><td>Тип экспорта RSS ленты:</td><td>
        <?php $v = $frame->config->getFromCms('Site', 'RSS/EXPORT/TYPE'); echo $v ? $v : '<em class="alert error">не указан</em>';?>
        </td></tr>
        <tr><td>Количество экспортируемых статей:</td><td>
        <?php $v = $frame->config->getFromCms('Site', 'RSS/EXPORT/COUNT'); echo $v ? $v : '<em class="alert warning">не указано (все статьи)</em>';?>
        </td></tr>
        <tr><td>Формат экспорта RSS ленты:</td><td>
        <?php $v = $frame->config->getFromCms('Site', 'RSS/EXPORT/FORMAT'); echo $v ? $v : '<em class="alert error">не указано</em>';?>
        </td></tr>
        <tr><td>Кэширование:</td><td>
        <?php $v = $frame->config->getFromCms('Site', 'RSS/CACHING'); echo $v ? '<em class="ok"></em>' : '<em class="alert error">отключено</em>';?>
        </td></tr>
    </table>
</section>
<?php
}
?>