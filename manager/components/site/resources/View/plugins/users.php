<?php
/**
 * Gear Manager
 *
 * Плагин             "Информация о сайте"
 * Пакет контроллеров "Просмотр ресурсов сайта"
 * Группа пакетов     "Ресурсы сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Users
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: users.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Плагин вывода информации о пользователях сайта
 * 
 * @param resource $frame указатель на класс контроллера фрейма
 * @param string $resPath каталог ресурсов контроллера
 * @return void
 */
function plugin_users($frame, $resPath = '')
{
?>
<h2>Пользователи и группы пользователей панели управления сайтом</h2>
<section>
    <img class="glyph" src="<?php echo $resPath;?>/images/icon-users.png" />
    <table class="grid">
        <tr><td>Контингент:</td>
        <?php 
        $count = get_user_count('gear_user_profiles');
        if ($count === false)
            echo '<td><span class="up">0</span> записей</td><td><em class="alert error">ошибка запроса</em></td>';
        else {
            if ($count == 0)
                echo '<td><span class="up">', $count, '</span> ', GString::wordForm($count, $frame->_['data_records']), '</td><td><em class="alert error">ошибки скриптов</em></td>';
            else
                echo '<td><span class="up">', $count, '</span> ', GString::wordForm($count, $frame->_['data_records']), ' / <a class="view" href="#" component-src="administration/users/contingent/~/grid/interface/">просмотреть</a></td>';
        }
        ?>
        </tr>
        <tr><td>Пользователей:</td>
        <?php 
        $count = get_user_count('site_users');
        if ($count === false)
            echo '<td><span class="up">0</span> записей</td><td><em class="alert error">ошибка запроса</em></td>';
        else {
            if ($count == 0)
                echo '<td><span class="up">', $count, '</span> ', GString::wordForm($count, $frame->_['data_records']), '</td><td><em class="alert error">ошибки скриптов</em></td>';
            else
                echo '<td><span class="up">', $count, '</span> ', GString::wordForm($count, $frame->_['data_records']), ' / <a class="view" href="#" component-src="administration/users/users/~/grid/interface/">просмотреть</a></td>';
        }
        ?>
        </tr>
        <tr><td>Групп пользователей:</td>
        <?php 
        $count = get_user_count('gear_user_groups');
        if ($count === false)
            echo '<td><span class="up">0</span> записей</td><td><em class="alert error">ошибка запроса</em></td>';
        else {
            if ($count == 0)
                echo '<td><span class="up">', $count, '</span> ', GString::wordForm($count, $frame->_['data_records']), '</td><td><em class="alert error">ошибки скриптов</em></td>';
            else
                echo '<td><span class="up">', $count, '</span> ', GString::wordForm($count, $frame->_['data_records']), ' / <a class="view" href="#" component-src="administration/groups/groups/~/grid/interface/">просмотреть</a></td>';
        }
        ?>
        </tr>
    </table>
</section>

<h2>Пользователи сайта</h2>
<section>
    <img class="glyph" src="<?php echo $resPath;?>/images/icon-users.png" />
    <table class="grid">
        <tr><td>Пользователей:</td>
        <?php 
        $count = get_user_count('site_users');
        if ($count === false)
            echo '<td><span class="up">0</span> записей</td><td><em class="alert error">ошибка запроса</em></td>';
        else {
            if ($count == 0)
                echo '<td><span class="up">', $count, '</span> ', GString::wordForm($count, $frame->_['data_records']), '</td><td><em class="alert error">ошибки скриптов</em></td>';
            else
                echo '<td><span class="up">', $count, '</span> ', GString::wordForm($count, $frame->_['data_records']), ' / <a class="view" href="#" component-src="administration/users/contingent/~/grid/interface/">просмотреть</a></td>';
        }
        ?>
        </tr>
        <tr><td>В журнале записей:</td>
        <?php 
        $count = get_user_count('site_user_log');
        if ($count === false)
            echo '<td><span class="up">0</span> записей</td><td><em class="alert error">ошибка запроса</em></td>';
        else {
            if ($count == 0)
                echo '<td><span class="up">', $count, '</span> ', GString::wordForm($count, $frame->_['data_records']), '</td><td><em class="alert error">ошибки скриптов</em></td>';
            else
                echo '<td><span class="up">', $count, '</span> ', GString::wordForm($count, $frame->_['data_records']), ' / <a class="view" href="#" component-src="administration/users/users/~/grid/interface/">просмотреть</a></td>';
        }
        ?>
        </tr>
        <tr><td>Регистрация через соц. сети:</td>
        <?php 
        $count = get_user_count('site_user_networks');
        if ($count === false)
            echo '<td><span class="up">0</span> записей</td><td><em class="alert error">ошибка запроса</em></td>';
        else {
            if ($count == 0)
                echo '<td><span class="up">', $count, '</span> ', GString::wordForm($count, $frame->_['data_records']), '</td><td><em class="alert error">ошибки скриптов</em></td>';
            else
                echo '<td><span class="up">', $count, '</span> ', GString::wordForm($count, $frame->_['data_records']), ' / <a class="view" href="#" component-src="administration/groups/groups/~/grid/interface/">просмотреть</a></td>';
        }
        ?>
        </tr>
    </table>
</section>
<?php
}

/**
 * Возращает количество записей в таблице
 * 
 * @param string $table название таблицы
 * @return integer
 */
function get_user_count($table)
{
    $query = GFactory::getQuery();
    $sql = 'SELECT COUNT(*) `count` FROM `' . $table . '`';
    if (($record = $query->getRecord($sql)) === false)
        return false;

    return (int) $record['count'];
}
?>