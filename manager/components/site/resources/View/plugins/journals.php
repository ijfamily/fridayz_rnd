<?php
/**
 * Gear Manager
 *
 * Плагин             "Информация из журналов событий"
 * Пакет контроллеров "Просмотр ресурсов сайта"
 * Группа пакетов     "Ресурсы сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Journals
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: journals.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Плагин вывода информации из журналов событий
 * 
 * @param resource $frame указатель на класс контроллера фрейма
 * @param string $resPath каталог ресурсов контроллера
 * @return void
 */
function plugin_journals($frame, $resPath = '')
{
?>
<h2>Журналы событий</h2>
<section>
    <img class="glyph" src="<?php echo $resPath;?>/images/icon-log.png" />
    <table class="grid">
        <tr><td>Авторизация пользователей:</td>
        <?php 
        $count = get_journal_count('gear_user_attends');
        if ($count === false)
            echo '<td><em class="alert error">ошибка запроса</em></td>';
        else
            echo '<td><span class="up">', $count, '</span> ', GString::wordForm($count, $frame->_['data_records']), ' / <a class="view" href="#" component-src="administration/journals/attends/~/grid/interface/">просмотреть</a></td>';
        ?>
        </tr>
        <tr><td>Восстановление учётных записей:</td>
        <?php 
        $count = get_journal_count('gear_user_restore');
        if ($count === false)
            echo '<td><span class="up">0</span> записей</td><td><em class="alert error">ошибка запроса</em></td>';
        else
            echo '<td><span class="up">', $count, '</span> ', GString::wordForm($count, $frame->_['data_records']), ' / <a class="view" href="#" component-src="administration/journals/restore/~/grid/interface/">просмотреть</a></td>';
        ?>
        </tr>
        <tr><td>Выход пользователей:</td>
        <?php 
        $count = get_journal_count('gear_user_escapes');
        if ($count === false)
            echo '<td><span class="up">0</span> записей</td><td><em class="alert error">ошибка запроса</em></td>';
        else
            echo '<td><span class="up">', $count, '</span> ', GString::wordForm($count, $frame->_['data_records']), ' / <a class="view" href="#" component-src="administration/journals/escapes/~/grid/interface/">просмотреть</a></td>';
        ?>
        </tr>
        <tr><td>Действия пользователей:</td>
        <?php 
        $count = get_journal_count('gear_log');
        if ($count === false)
            echo '<td><span class="up">0</span> записей</td><td><em class="alert error">ошибка запроса</em></td>';
        else
            echo '<td><span class="up">', $count, '</span> ', GString::wordForm($count, $frame->_['data_records']), ' / <a class="view" href="#" component-src="administration/journals/log/~/grid/interface/">просмотреть</a></td>';
        ?>
        </tr>
        <tr><td>Ошибки запросов пользователей:</td>
        <?php 
        $count = get_journal_count('gear_log_sql');
        if ($count === false)
            echo '<td><span class="up">0</span> записей</td><td><em class="alert error">ошибка запроса</em></td>';
        else
            echo '<td><span class="up">', $count, '</span> ', GString::wordForm($count, $frame->_['data_records']), ' / <a class="view" href="#" component-src="administration/journals/query/~/grid/interface/">просмотреть</a></td>';
        ?>
        </tr>
        <tr><td>Ошибки скриптов:</td>
        <?php 
        $count = get_journal_count('gear_log_script');
        if ($count === false)
            echo '<td><span class="up">0</span> записей</td><td><em class="alert error">ошибка запроса</em></td>';
        else
            echo '<td><span class="up">', $count, '</span> ', GString::wordForm($count, $frame->_['data_records']), ' / <a class="view" href="#" component-src="administration/journals/script/~/grid/interface/">просмотреть</a></td>';
        ?>
        </tr>
    </table>
</section>
<?php
}

/**
 * Возращает количество записей в журнале
 * 
 * @param srting $table название таблицы
 * @return integer
 */
function get_journal_count($table)
{
    $query = GFactory::getQuery();
    $sql = 'SELECT COUNT(*) `count` FROM `' . $table . '`';
    if (($record = $query->getRecord($sql)) === false)
        return false;

    return (int) $record['count'];
}
?>