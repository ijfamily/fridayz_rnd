<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Ресурсы сайта"
 * Группа пакетов     "Ресурсы сайта"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SResources
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Ресурсы
 * Описание: Ресурсы сайта
 * Меню: Ресурсы сайта
 * ID класса: gcontroller_sresources_view
 * Модуль: Сайт
 * Группа: Сайт
 * Очищать: нет
 * Статистика компонента: нет
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске компонентов: нет
 *    В главном меню: нет
 * Ресурс
 *    Компонент: site/resources/
 *    Контроллер: site/resources/View/
 *    Интерфейс: view/interface/
 *    Меню:
 */

return array(
    // {S}- модуль "Site" {} - пакет контроллеров "Resources" -> SResources
    'clsPrefix' => 'SResources',
    // использовать язык
    'language'  => 'ru-RU',
    // плагины
    'plugins'   => array(
        'common', 'config', 'directories', 'extensions', 'memory', 'indexes', 'journals', 'users', 'articles', 'categories',
        'languages', 'rss', 'sitemap', 'phpinfo'
    )
);
?>