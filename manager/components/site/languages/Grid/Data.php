<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка языков сайта"
 * Пакет контроллеров "Языки сайта"
 * Группа пакетов     "Сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SLanguages_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные списка языков сайта
 * 
 * @category   Gear
 * @package    GController_SLanguages_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SLanguages_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'language_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_languages';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 'SELECT SQL_CALC_FOUND_ROWS * FROM `site_languages` WHERE 1 %filter ORDER BY %sort LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // название
            'language_name' => array('type' => 'string'),
            // псевдоним
            'language_alias' => array('type' => 'string'),
            // основной
            'language_default' => array('type' => 'integer'),
            // используемый
            'language_visible' => array('type' => 'integer')
        );
    }
}
?>