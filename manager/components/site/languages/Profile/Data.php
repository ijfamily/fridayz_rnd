<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные для интерфейса языков сайта"
 * Пакет контроллеров "Профиль языка"
 * Группа пакетов     "Сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SLanguages_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные для интерфейса профиля слайдера
 * 
 * @category   Gear
 * @package    GController_SLanguages_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SLanguages_Profile_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($_tableName)
     *
     * @var array
     */
    public $fields = array('language_name', 'language_alias', 'language_default', 'language_visible');

    /**
     * Первичный ключ таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'language_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_languages';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_slanguages_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_profile_update'], $record['language_name']);
    }

    /**
     * Событие наступает после успешного добавления данных
     * 
     * @param array $data сформированные данные полученные после запроса к таблице
     * @return void
     */
    protected function dataInsertComplete($data = array())
    {
        // соединение с базой данных (т.к. для лога ранее возможно было другое соединение)
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();

        $query = new GDb_Query();
        // обновить язык по умолчанию
        if ($this->input->get('language_default', 0) == 1) {
            $sql = 'UPDATE `site_languages` SET `language_default`=0 WHERE `language_id`<>' . $this->_recordId;
            if ($query->execute($sql) === false)
                throw new GSqlException();
        }
        $this->updateLanguage($query);
    }

    /**
     * Онбновить язык
     * 
     * @return void
     */
    protected function updateLanguage($query)
    {
        $site = $this->session->get('site');

        // язык сайта
        $sql = 'SELECT * FROM `site_languages`';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $langs = $items = array();
        $default = array();
        while (!$query->eof()) {
            $lang = $query->next();
            if ($lang['language_default'])
                $default = $lang;
            if ($lang['language_visible']) {
                $items[] = array(
                    'text'    => $lang['language_default'] ? '<b>' . $lang['language_name'] . '</b>' : $lang['language_name'],
                    'params'  => 'lang=' . $lang['language_id'],
                    'iconCls' => 'icon-form-translate',
                    'url'     => ''
                );
                $langs[] = $lang;
            }
        }
        $site['language/list'] = $langs;
        $site['language/items'] = $items;
        $site['language/default/id'] = $default['language_id'];
        $site['language/default/name'] = $default['language_name'];
        $site['language/default/alias'] = $default['language_alias'];

        $this->session->set('site', $site);
    }

    /**
     * Событие наступает после успешного обновления данных
     * 
     * @param array $data сформированные данные полученные после запроса к таблице
     * @return void
     */
    protected function dataUpdateComplete($data = array())
    {
        // соединение с базой данных (т.к. для лога ранее возможно было другое соединение)
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();

        $query = new GDb_Query();
        // обновить язык по умолчанию
        if ($this->input->get('language_default', 0) == 1) {
            $sql = 'UPDATE `site_languages` SET `language_default`=0 WHERE `language_id`<>' . $this->uri->id;
            if ($query->execute($sql) === false)
                throw new GSqlException();
        }
        $this->updateLanguage($query);
    }

    /**
     * Событие наступает после успешного удаления данных
     * 
     * @return void
     */
    protected function dataDeleteComplete()
    {
        // соединение с базой данных (т.к. для лога ранее возможно было другое соединение)
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();

        $query = new GDb_Query();
        // удаление страниц статей
        $sql = 'DELETE FROM `site_pages` `language_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->execute($sql) === false)
            throw new GSqlException();
        // удаление документов статей
        $sql = 'DELETE FROM `site_documents_l` `language_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        // удаление изображений галерей
        $sql = 'DELETE FROM `site_gallery_images_l` `language_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // обновление сессии
        $this->updateLanguage($query);
    }
}
?>