<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные интерфейса списка изображений альбома"
 * Пакет контроллеров "Изображения альбома"
 * Группа пакетов     "Альбомы"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_PPlacesImages_Grid
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('File');
Gear::controller('Grid/Data');

/**
 * Данные интерфейса списка изображений альбома
 * 
 * @category   Gear
 * @package    GController_PPlacesImages_Grid
 * @subpackage Data
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_PPlacesImages_Grid_Data extends GController_Grid_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'image_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'place_images';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_pplaces_grid';

    /**
     * Каталог изображений альбома
     *
     * @var string
     */
    public $_path = '';

    /**
     * Идентификатор галереи
     *
     * @var integer
     */
    protected $_placeId;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // идент. альбома
        $this->_placeId = $this->store->get('record', 0, 'gcontroller_pplaces_grid');
        $this->_place = $this->store->get('place');
        // каталог изображений альбома
        $this->_path = DOCUMENT_ROOT . $this->config->getFromCms('Site', 'DIR/IMAGES') . $this->_place['place_folder']. '/';
        // язык сайта по умолчанию
        $languageId = $this->config->getFromCms('Site', 'LANGUAGE/ID');
        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS * FROM `place_images` '
           .'WHERE `place_id`=' . $this->_recordId . ' %filter ORDER BY %sort LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // индекс изображения
            'image_index' => array('type' => 'string'),
            // файл изображения
            'image_entire_filename' => array('type' => 'string'),
            // разрешение изображения
            'image_entire_resolution' => array('type' => 'string'),
            // размер файла изображения
            'image_entire_filesize' => array('type' => 'string'),
            // файл эскиза изображения
            'image_thumb_filename' => array('type' => 'string'),
            // разрешение эскиза изображения
            'image_thumb_resolution' => array('type' => 'string'),
            // размер файла эскиза изображения
            'image_thumb_filesize' => array('type' => 'string'),
            // заголовок изображения
            'image_title' => array('type' => 'string'),
            // показывать изображение
            'image_visible' => array('type' => 'integer'),
            // обложка изображения
            'image_cover' => array('type' => 'integer'),
            // обложка изображения
            'image_view' => array('type' => 'string'),
            'image_entire_link' => array('type' => 'string')

        );
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        $query = new GDb_Query();
        // удаление файлов изображений
        $sql = 'SELECT `i`.*, `g`.`place_folder` FROM `place_images` `i` JOIN `place_names` `g` USING(`place_id`) '
             . 'WHERE `g`.`place_id`=' . $this->_placeId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $path = DOCUMENT_ROOT . $this->config->getFromCms('Site', 'DIR/IMAGES');
        while (!$query->eof()) {
            $image = $query->next();
            // если есть изображение
            if ($image['image_entire_filename'])
                GFile::delete($path . $image['place_folder'] . '/' . $image['image_entire_filename'], false);
            // если есть эскиз
            if ($image['image_thumb_filename'])
                GFile::delete($path . $image['place_folder'] . '/' . $image['image_thumb_filename'], false);
        }
        // удаление изображений
        $sql = 'DELETE FROM `place_images` WHERE `place_id`=' . $this->_placeId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('place_images') === false)
            throw new GSqlException();
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Событие наступает после успешного удаления данных
     * 
     * @return void
     */
    protected function dataDeleteComplete()
    {
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        $query = new GDb_Query();
        // изображения
        $sql = 'SELECT `i`.*, `g`.`place_folder` FROM `place_images` `i` JOIN `place_names` `g` USING(`place_id`) '
             . 'WHERE `i`.`image_id` IN (' . $this->uri->id. ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $path = DOCUMENT_ROOT . $this->config->getFromCms('Site', 'DIR/IMAGES');
        while (!$query->eof()) {
            $image = $query->next();
            // если есть изображение
            if ($image['image_entire_filename'])
                GFile::delete($path . $image['place_folder'] . '/' . $image['image_entire_filename'], false);
            // если есть эскиз
            if ($image['image_thumb_filename'])
                GFile::delete($path . $image['place_folder'] . '/' . $image['image_thumb_filename'], false);
        }
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        $record['image_view'] = '';
        // если файл эскиза не существует
        if (!empty($record['image_thumb_filename']))
            if (!file_exists($this->_path . $record['image_thumb_filename']))
                $record['image_thumb_filename'] = '<span style="color:#eeacaf">' . $record['image_thumb_filename'] . '</span>';
            else
                $record['image_view'] ='<div class="mn-img-preview" style="background-image: url(/' . $this->_path . $record['image_thumb_filename'] . ');"></div>';
        // если файл изображения не существует
        if (!empty($record['image_entire_filename']))
            if (!file_exists($this->_path . $record['image_entire_filename'])) {
                $record['image_entire_filename'] = '<span style="color:#eeacaf">' . $record['image_entire_filename'] . '</span>';
            } else {
                $record['image_entire_link'] = '<a href="/'. $this->_path . $record['image_entire_filename'] . '" target="_blank" title="' . $this->_['tooltip_image_entire_link'] . '"><img src="' . $this->resourcePath . 'icon-item-link.png"></a>';
            }

        return $record;
    }
}
?>