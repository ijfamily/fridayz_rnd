<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные наполнения фотоальбома"
 * Пакет контроллеров "Профиль наполнения фотоальбома"
 * Группа пакетов     "Сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_PPlacesImages_Aggregate
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('String');
Gear::library('File');
Gear::controller('Grid/Process');

/**
 * Данные профиля наполнения фотоальбома
 * 
 * @category   Gear
 * @package    GController_PPlacesImages_Aggregate
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_PPlacesImages_Aggregate_Data extends GController_Grid_Process
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_pplaces_grid';

    /**
     * Идент. списка
     *
     * @var string
     */
    public $gridId = 'grid-process';

    /**
     * Фотоальбом
     *
     * @var mixed
     */
    protected $_place = false;

    /**
     * Путь к фотоальбому
     *
     * @var string
     */
    protected $_placePath = '';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // если нет списка изображений
        if (empty($this->list))
            throw new GException($this->_['title_process'], $this->_['msg_empty_list']);
        // фотоальбом
        $this->_place = $this->store->get('place', false);
        if (empty($this->_place))
            throw new GException($this->_['title_process'], $this->_['msg_empty_place']);
        // путь к фотоальбому
        $this->_placePath = DOCUMENT_ROOT . $this->config->getFromCms('Site', 'DIR/IMAGES') . $this->_place['place_folder'] . '/';
    }

    /**
     * Начало обработки
     * 
     * @return void
     */
    protected function start()
    {
        parent::start();

        // удалить все записи
        $this->clear();
        // блокировать кнопку
        $this->response->add('setTo', array('id' => 'btn-process', 'func' => 'setDisabled', 'args' => array(true)), true);
        $this->response->setMsgResult($this->_['title_process'], $this->_['msg_process_start'], true);
    }

    /**
     * Конец обработки
     * 
     * @return void
     */
    protected function end()
    {
        // закрыть окно
        $this->response->add('setTo', array('id' => 'gcontroller_pplacesimages_aggregate', 'func' => 'close'), true);
        // обновить список
        $this->response->setMsgResult($this->_['title_process'], $this->_['msg_process_end'], true);
    }

    /**
     * Удалить все записи
     * 
     * @return void
     */
    protected function clear()
    {
        // соединение с базой данных
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();

        $query = new GDb_Query();
        // удаление файлов изображений
        $sql = 'SELECT `i`.*, `g`.`place_folder` FROM `place_images` `i` JOIN `place_names` `g` USING(`place_id`) '
             . 'WHERE `g`.`place_id`=' . $this->_place['place_id'];
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $path = DOCUMENT_ROOT . $this->config->getFromCms('Site', 'DIR/IMAGES');
        while (!$query->eof()) {
            $image = $query->next();
            // если есть изображение
            if ($image['image_entire_filename'])
                GFile::delete($path . $image['place_folder'] . '/' . $image['image_entire_filename'], false);
            // если есть эскиз
            if ($image['image_thumb_filename'])
                GFile::delete($path . $image['place_folder'] . '/' . $image['image_thumb_filename'], false);
        }
        // удаление изображений
        $sql = 'DELETE FROM `place_images` WHERE `place_id`=' . $this->_place['place_id'];
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('place_images') === false)
            throw new GSqlException();
    }

    /**
     * Обработка изображения
     * 
     * @param string $filename имя файла
     * @return array
     */
    protected function updateImage($filename)
    {
        $result = array();

        // определение имени файла
        $tfileExt = $fileExt = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
        // идент. название файла
        if ($this->config->getFromCms('Partners', 'GENERATE/FILE/NAME'))
            $fileId = uniqid();
        else {
            $fileId = pathinfo($fileImage['name'], PATHINFO_FILENAME);
            $fileId = GString::toUrl($fileId,  $this->config->getFromCms('Site', 'LANGUAGE/ALIAS'));
        }
        $tfileId = $fileId . '_thumb';
        // сгенерировать название файла
        $nfilename = $this->_placePath . $fileId . '.' . $fileExt;
        $tfilename = $this->_placePath . $tfileId . '.' . $tfileExt;
        $result['image_entire_filename'] = $fileId . '.' . $fileExt;
        $result['image_thumb_filename'] = $tfileId . '.' . $tfileExt;

        // переименовать оригинал
        if (!rename($filename, $nfilename))
            throw new GException($this->_['title_process'], 'Unable to move file "%s"', $this->_placePath);

        // изменить оригинал
        $width = (int) $this->config->getFromCms('Partners', 'IMAGE/ORIGINAL/WIDTH');
        $height = (int) $this->config->getFromCms('Partners', 'IMAGE/ORIGINAL/HEIGHT');
        $img = GFactory::getClass('Image', 'Image', $nfilename);
        $cmdImage = 'resize{"width": ' . $width . ', "height": ' . $height . '}';
        if (!$img->execute($cmdImage))
            throw new GException('Error', $this->_['msg_image_size']);
        $img->save($nfilename, $this->config->getFromCms('Partners', 'IMAGE/QUALITY', 0));
        $result['image_entire_resolution'] = $img->getSizeStr();

        // создать миниатюры
        $width = (int) $this->config->getFromCms('Partners', 'IMAGE/THUMB/WIDTH');
        $height = (int) $this->config->getFromCms('Partners', 'IMAGE/THUMB/HEIGHT');
        $img = GFactory::getClass('Image', 'Image', $nfilename);
        $cmdImage = 'resize{"width": ' . $width . ', "height": ' . $height . '}';
        if (!$img->execute($cmdImage))
            throw new GException('Error', $this->_['msg_error_create_thumb']);
        $img->save($tfilename);
        $result['image_thumb_resolution'] = $img->getSizeStr();
        $result['image_thumb_filesize'] = GFile::getFileSize($tfilename);

        $img = GFactory::getClass('Image', 'Image', $nfilename);
        // 
        if ($this->config->getFromCms('Partners', 'WATERMARK')) {
            $img->watermark(
                DOCUMENT_ROOT . $this->config->getFromCms('Site', 'WATERMARK/STAMP', ''),
                $this->config->getFromCms('Partners', 'WATERMARK/POSITION', '') 
            );
        }
        $img->save($nfilename, $this->config->getFromCms('Partners', 'IMAGE/QUALITY', 0));
        $result['image_entire_filesize'] = GFile::getFileSize($nfilename);

        return $result;
    }

    /**
     * Обработка элемента списка
     * 
     * @param array $item
     * @return void
     */
    protected function itemProcess($item = array())
    {
        // соединение с базой данных
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();

        $thumb = new GDb_Table('place_images', 'image_id');

        $data = $this->updateImage($item[0]);
        $data['place_id'] = $this->_place['place_id'];
        $data['image_index'] = $this->index + 1;
        $data['image_date'] = date('Y-m-d H:i:S');

        // добавить миниатюру
        if ($thumb->insert($data) === false)
            throw new GSqlException();

        // выставить в списке ok
        $this->response->add('setTo', array('id' => $this->gridId, 'func' => 'setState', 'args' => array($this->index, 'ok')), true);
        $this->response->setMsgResult($this->_['title_process'], sprintf($this->_['msg_process'], $this->index + 1, sizeof($this->list)), true);
    }
}
?>