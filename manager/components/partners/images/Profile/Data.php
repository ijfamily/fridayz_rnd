<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные для интерфейса профиля изображения"
 * Пакет контроллеров "Изображение альбома"
 * Группа пакетов     "Альбомы"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_PPlacesImages_Profile
 * @copyright  Copyright (c) 2013-2016 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('String');
Gear::library('File');
Gear::controller('Profile/Data');

/**
 * Данные для интерфейса профиля изображения
 * 
 * @category   Gear
 * @package    GController_PPlacesImages_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_PPlacesImages_Profile_Data extends GController_Profile_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array(
        'place_id', 'image_index', 'image_entire_filename', 'image_entire_resolution',
        'image_entire_filesize', 'image_entire_style', 'image_thumb_filename', 'image_thumb_resolution',
         'image_thumb_filesize', 'image_date', 'image_visible', 'sys_date_insert', 'sys_date_update',
         'image_title', 'image_longdesc', 'image_thumb_title'
    );

    /**
     * Первичный ключ таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'image_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'place_images';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_pplaces_grid';

    /**
     * Идентификатор галереи
     *
     * @var integer
     */
    protected $_placeId = 0;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // едент. галереи
        $this->_placeId = $this->input->get('place_id', 0);
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param string $sql запрос SQL на вывод данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        $sql = 'SELECT `i`.*, `g`.`place_folder` FROM `place_names` `g` JOIN `place_images` `i` USING(`place_id`) '
             . 'WHERE `i`.`image_id`=' . (int) $this->uri->id;

        parent::dataView($sql);
    }

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_profile_update'], $record['image_entire_filename']);
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        $query = new GDb_Query();
        // изображения
        $sql = 'SELECT `i`.*, `g`.`place_folder` FROM `place_images` `i` JOIN `place_names` `g` USING(`place_id`) '
             . 'WHERE `i`.`image_id` IN (' . $this->uri->id. ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $path = DOCUMENT_ROOT . $this->config->getFromCms('Site', 'DIR/IMAGES');
        while (!$query->eof()) {
            $image = $query->next();
            // если есть изображение
            if ($image['image_entire_filename'])
                GFile::delete($path . $image['place_folder'] . '/' . $image['image_entire_filename'], false);
            // если есть эскиз
            if ($image['image_thumb_filename'])
                GFile::delete($path . $image['place_folder'] . '/' . $image['image_thumb_filename'], false);
        }
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        $params['image_date'] = date('Y-m-d H:i:s');
        // если состояние формы "правка"
        if ($this->isUpdate) return;

        $query = new GDb_Query();
        // заведение
        $sql = 'SELECT * FROM `place_names` WHERE `place_id`=' . $this->_placeId;
        if (($this->_place = $query->getRecord($sql)) === false)
                throw new GSqlException();

        // идент. заведения
        $params['place_id'] = $this->_placeId;
        $fileImage = $fileThumb = false;
        $cmdImage = $cmdThumb = '';

        // если нет изображения для загрузки
        if (!$this->input->hasFile())
            throw new GException('Adding data', $this->_['msg_file_image_empty']);
        if (($fileImage = $this->input->file->get('image')) === false)
            throw new GException('Adding data', $this->_['msg_file_image_empty']);
        // определение имени файла
        $tfileExt = $fileExt = strtolower(pathinfo($fileImage['name'], PATHINFO_EXTENSION));
        // идент. название файла
        if ($this->config->getFromCms('Albums', 'GENERATE/FILE/NAME'))
            $fileId = uniqid();
        else {
            $fileId = pathinfo($fileImage['name'], PATHINFO_FILENAME);
            $fileId = GString::toUrl($fileId,  $this->config->getFromCms('Site', 'LANGUAGE/ALIAS'));
        }
        $tfileId = $fileId . '_thumb';
        // сгенерировать название файла
        $filename = $fileId . '.' . $fileExt;
        $tfilename = $tfileId . '.' . $tfileExt;


        $uploadPath = DOCUMENT_ROOT . $this->config->getFromCms('Site', 'DIR/IMAGES') . $this->_place['place_folder'] . '/';
        $uploadExt = explode(',', strtolower($this->config->getFromCms('Site', 'FILES/EXT/IMAGES')));
        // проверка существования изображения
        if (file_exists($uploadPath . $filename))
            throw new GException('Error', sprintf($this->_['msg_file_exists'], $uploadPath . $filename));
        // проверка существования эскиза изображения
        if (file_exists($uploadPath . $tfilename))
            throw new GException('Error', sprintf($this->_['msg_file_exists'], $uploadPath . $tfilename));

        // загрузка изображения
        GFile::upload($fileImage, $uploadPath . $filename, $uploadExt);

        // изменить оригинал
        $width = (int) $this->config->getFromCms('Albums', 'IMAGE/ORIGINAL/WIDTH');
        $height = (int) $this->config->getFromCms('Albums', 'IMAGE/ORIGINAL/HEIGHT');
        $img = GFactory::getClass('Image', 'Image', $uploadPath . $filename);
        $cmdImage = 'resize{"width": ' . $width . ', "height": ' . $height . '}';
        if (!$img->execute($cmdImage))
            throw new GException('Error', $this->_['msg_image_size']);
        $img->save($uploadPath . $filename, $this->config->getFromCms('Albums', 'IMAGE/QUALITY', 0));

        // создать миниатюры
        $width = (int) $this->config->getFromCms('Albums', 'IMAGE/THUMB/WIDTH');
        $height = (int) $this->config->getFromCms('Albums', 'IMAGE/THUMB/HEIGHT');
        $img = GFactory::getClass('Image', 'Image', $uploadPath . $filename);
        $cmdImage = 'resize{"width": ' . $width . ', "height": ' . $height . '}';
        if ($img->execute($cmdImage)) {
            // изменение изображения
            $name = pathinfo($filename, PATHINFO_FILENAME) . '_thumb.' . pathinfo($filename, PATHINFO_EXTENSION);
            $img->save($uploadPath . $name);
        }
        // обновляем поля изображения
        $params['image_thumb_filename'] = $tfilename;
        $params['image_thumb_filesize'] = GFile::getFileSize($uploadPath . $tfilename);
        $params['image_thumb_resolution'] = $img->getSizeStr();

        $img = GFactory::getClass('Image', 'Image', $uploadPath . $filename);
        // 
        if ($this->config->getFromCms('Albums', 'WATERMARK')) {
            $img->watermark(
                DOCUMENT_ROOT . $this->config->getFromCms('Site', 'WATERMARK/STAMP', ''),
                $this->config->getFromCms('Albums', 'WATERMARK/POSITION', '') 
            );
        }
        $img->save($uploadPath . $filename, $this->config->getFromCms('Albums', 'IMAGE/QUALITY', 0));
        // обновляем поля изображения
        $params['image_entire_filename'] = $filename;
        $params['image_entire_filesize'] = GFile::getFileSize($uploadPath . $filename);
        $params['image_entire_resolution'] = $img->getSizeStr();
    }

    /**
     * Предварительная обработка записи
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
       $path = '/' . $this->config->getFromCms('Site', 'DIR/IMAGES') . $this->_data['place_folder'] . '/';
        // ресурсы изображения
        $record['image_entire_url'] = 'http://' . $_SERVER['SERVER_NAME'] . $path . $record['image_entire_filename'];
        // ресурсы эскиза
        if (empty($record['image_thumb_filename'])) {
            $record['image_thumb_url'] = '';
        } else {
            $record['image_thumb_url'] = 'http://' . $_SERVER['SERVER_NAME'] . $path . $record['image_thumb_filename'];
        }

        return $record;
    }
}
?>