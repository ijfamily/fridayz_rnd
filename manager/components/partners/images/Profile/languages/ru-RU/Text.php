<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2013-2016 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Создание записи "Изображение"',
    'title_profile_update' => 'Изменение записи "%s"',
    'text_btn_help'        => 'Справка',
    // поля формы
    // вкладка "атрибуты"
    'title_tab_attributes' => 'Атрибуты',
    'label_image_index'    => 'Порядок<hb></hb>',
    'tip_image_index'      => 'Порядковый номер в списке',
    'label_image_visible'  => 'Показывать<hb></hb>',
    'tip_image_visible'    => 'Показывать изображение',
    'label_image_longdesc' => 'URL адрес',
    'label_image_title'    => 'Заглавие<hb></hb>',
    'tip_image_title'      => 
        'Используется для подписи изображения если  изображение отсутствует (атрибуты &quot;alt&quot;, &quot;title&quot;)',
    'label_image_thumb_title'    => 'Заглавие (м)<hb></hb>',
    'tip_image_thumb_title'      => 
        'Используется для подписи миниатюры изображения если изображение отсутствует (атрибуты &quot;alt&quot;, &quot;title&quot;)',
    'tip_image_longdesc'   => 
        'Указывается URL адрес для перехода на страницу сайта где расположена развёрнутая информация об изображении',
    'title_bar_rename'     => 'Переименовать файл',
    // вкладка "изображение"
    'title_tab_img'           => 'Изображение',
    'title_fieldset_img'      => 'Файл изображения',
    'note_img'                => '<note>допустимые расширения файла &laquo;%s&raquo;</note>',
    'text_empty'              => 'Выберите файл изображения ...',
    'text_btn_upload'         => 'Выбрать',
    'label_iname_create'      => 'Сгенерировать название файла',
    'label_watermark_create'  => 'Использовать водяной знак',
    'label_entire_filename'   => 'Файл',
    'label_entire_uri'        => 'Русурс URI',
    'tip_entire_uri'          => 
        'Это символьная строка, позволяющая идентифицировать какой-либо ресурс: документ, изображение, файл, службу, '
      . 'ящик электронной почты и т. д.',
    'label_entire_url'        => 'Русурс URL<hb></hb>',
    'tip_entire_url'          => 
        'Eдинообразный локатор (определитель местонахождения) ресурса. Это стандартизированный способ записи адреса '
      . 'ресурса в сети Интернет.',
    'label_entire_resolution' => 'Разрешение',
    'tip_entire_resolution'   => 'Разрешение файла в пикселях',
    'label_entire_type'       => 'Тип',
    'label_entire_filesize'   => 'Размер',
    'tip_entire_filesize'     => 'Размер файла',
    'label_date_insert'       => 'Создан',
    'label_date_update'       => 'Изменён',
    'title_fieldset_iprofile' => 'Изменение изображения после загрузки',
    // закладка "эскиз"
    'title_tab_thm'           => 'Эскиз изображения',
    'title_fieldset_ath'      => 'Атрибуты эскиза',
    'label_profile_image'     => 'Изменить',
    'label_thumb_create'      => 'Создать',
    'title_fieldset_tprofile' => 'Создания эскиза из изображения',
    'text_select_image_size'  => 'выберите размер изображения',
    'text_select_thumb_size'  => 'выберите размер эскиза',
    // сообщения
    'msg_file_image_empty'    => 'Не выбран файл изображения!',
    'msg_file_exists'         => 'Файл изображения "%s" уже существует!',
    'msg_image_size'          => 'Невозможно изменить размеры изображения!',
);
?>