<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные наполнения фотоальбома"
 * Пакет контроллеров "Профиль наполнения фотоальбома"
 * Группа пакетов     "Сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_PPlacesImages_Aggregate
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('String');
Gear::library('File');
Gear::controller('Profile/Data');

/**
 * Данные профиля наполнения фотоальбома
 * 
 * @category   Gear
 * @package    GController_PPlacesImages_Aggregate
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_PPlacesImages_Resize_Data extends GController_Profile_Data
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_pplaces_grid';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // едент. галереи
        $this->_placeId = $this->store->get('record', 0, 'gcontroller_pplaces_grid');;
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        parent::dataAccessView();

        $query = new GDb_Query();
        // список адресов
        $sql = 'SELECT * FROM `place_names` WHERE `place_id`=' . $this->_placeId;
        if (($place = $query->getRecord($sql)) === false)
            throw new GSqlException();
        $this->_placePath = DOCUMENT_ROOT . $this->config->getFromCms('Site', 'DIR/IMAGES') . $place['place_folder'] . '/';

        if (!is_dir($this->_placePath))
            throw new GException($this->_['title_aggregate'], 'The directory "%s" can not be opened', $this->_placePath);
        $handle = @opendir($this->_placePath);
        if ($handle === false)
            throw new GException($this->_['title_aggregate'], 'The directory "%s" can not be opened', $this->_placePath);
        $images = $data = array();
        $index = 0;
        while(false !== ($file = readdir($handle))) {
            if($file != '.' && $file != '..') {
                $images[] = $this->_placePath . $file;
                $data[] = array($index, $file, $file, 0);
                $index++;
            }
        }

       $this->response->add('setTo', array(
            array('id' => 'grid-process-resize', 'func' => 'loadData', 'args' => array($data)),
       ));
    }
}
?>