<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля наполнения фотоальбома"
 * Пакет контроллеров "Профиль наполнения фотоальбома"
 * Группа пакетов     "Сайт"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_PPlacesImages_Aggregate
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля наполнения фотоальбома
 * 
 * @category   Gear
 * @package    GController_PPlacesImages_Aggregate
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_PPlacesImages_Resize_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        $place = $this->store->get('place', false, 'gcontroller_pplaces_grid');
        if ($place) {
            $desc = sprintf($this->_['html_desc'], $this->config->getFromCms('Site', 'DIR/IMAGES') . $place['place_folder']) . '/';
        } else
            $desc = sprintf($this->_['html_desc'], '');

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'titleEllipsis' => 45,
                  'id'            => $this->classId,
                  'gridId'        => 'gcontroller_pplaces_grid',
                  'width'         => 500,
                  'height'        => 500,
                  //'autoHeight'    => true,
                 'state'         => 'none',
                 'btnDeleteHide' => true,
                  'resizable'     => false,
                  'stateful'      => false)
        );
        $this->_cmp->buttonsAdd = array(
            array('xtype'       => 'mn-btn-form-update',
                  'id'          => 'btn-aggregate',
                  'text'        => $this->_['text_btn_aggregate'],
                  'tooltip'     => $this->_['tooltip_btn_aggregate'],
                  'icon'        => $this->resourcePath . 'icon-btn-send.png',
                  'windowId'    => $this->classId,
                  'width'       => 97,
                  'closeWindow' => false)
        );
        /*$this->_cmp->buttonsAdd = array(
            array('xtype'       => 'mn-btn-form-update',
                  'text'        => $this->_['text_btn_aggregate'],
                  'tooltip'     => $this->_['tooltip_btn_aggregate'],
                  'icon'        => $this->resourcePath . 'icon-btn-send.png',
                  'windowId'    => $this->classId,
                  'width'       => 97,
                  'closeWindow' => false)
        );*/

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype' => 'mn-grid-process',
                  'id'  => 'grid-process-resize',
                  'anchor' => '100% 100%',
                  'data'   => array(array(1, 'dd', 'aa', 'ok')),
                  'url'   => $this->componentUrl . 'resize/process/'
            )
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($items);
        $form->labelWidth = 50;
        $form->url = $this->componentUrl . 'resize/';



        parent::getInterface();
    }
}
?>