<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Заполнение фотоальбома изображениями',
    // поля формы
    'html_desc' =>
        '<note>Для заполнения фотоальбома изображениями, вам необходимо исходные изображения перенести в каталог <b>"%s"</b> и '
      . 'нажать кнопку "Выполнить". Для каждого изображения в каталоге будет выполнена процедура (смена названия, создание миниатюры, наложение водянного знака), эти'
      . 'настройки выставляются в насткройках альбома изображений. Перед заполнененим фотоальбома предыдущие записи будут удалены.</note>',
    'text_btn_aggregate'    => 'Выполнить',
    'tooltip_btn_aggregate' => 'Наполнение фотоальбома изображениями',
    // сообщения
    'title_aggregate' => 'Заполнение альбома изображениями',
    'msg_success'     => 'Наполнение альбома выполнено!',
    'msg_image_size'  => 'Невозможно изменить размеры изображения!',
    'msg_error_create_thumb' => 'Ошибка в создании миниатюры изображения!'
);
?>