<?php
/**
 * Gear Manager
 *
 * Контроллер         "Код фрейма загрузчика"
 * Пакет контроллеров "Профиль загрузчика фотоальбома"
 * Группа пакетов     "Фотоальбомы"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_PPlacesImages_Uploader
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Frame.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Frame');

/**
 * Код фрейма загрузчика
 * 
 * @category   Gear
 * @package    GController_PPlacesImages_Uploader
 * @subpackage Frame
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Frame.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_PPlacesImages_Uploader_Frame extends GController_Frame
{
    /**
     * Вывод кода фрейма
     * 
     * @return void
     */
    public function frame($resPath = '')
    {
        $uploadExt = str_replace(',', "', '", strtolower($this->config->getFromCms('Site', 'FILES/EXT/IMAGES')));
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link href="<?php echo $resPath;?>uploader/css/fine-uploader-gallery.css" rel="stylesheet" />

    <script src="<?php echo $resPath;?>uploader/js/fine-uploader.js"></script>
    <script type="text/template" id="qq-template-gallery">
        <div class="wrapper">
        <div class="round">
            <div class="title">Перетащите файлы сюда</div>
            <div class="subtitle">или нажмите кнопку "Загрузить"</div>
        </div>

        <div class="qq-uploader-selector qq-uploader qq-gallery" qq-drop-area-text="" style="position: absolute; width: 100%; height: 100%;">
            <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
                <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
            </div>
            <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
                <span class="qq-upload-drop-area-text-selector"></span>
            </div>
            <div class="qq-upload-button-selector qq-upload-button">
                <div></div>
            </div>
            <span class="qq-drop-processing-selector qq-drop-processing">
                <span>Обработка перетащенных файлов...</span>
                <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
            </span>

            <ul class="qq-upload-list-selector qq-upload-list" role="region" aria-live="polite" aria-relevant="additions removals">
                <li>
                    <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                    <div class="qq-progress-bar-container-selector qq-progress-bar-container">
                        <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                    </div>
                    <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                    <div class="qq-thumbnail-wrapper">
                        <img class="qq-thumbnail-selector" qq-max-size="120" qq-server-scale>
                    </div>
                    <button type="button" class="qq-upload-cancel-selector qq-upload-cancel"></button>
                    <button type="button" class="qq-upload-retry-selector qq-upload-retry">
                        <span class="qq-btn qq-retry-icon" aria-label="Повторить"></span>
                        Повторить
                    </button>

                    <div class="qq-file-info">
                        <div class="qq-file-name">
                            <span class="qq-upload-file-selector qq-upload-file"></span>
                            <span class="qq-edit-filename-icon-selector qq-edit-filename-icon" aria-label="Изменить имя файла"></span>
                        </div>
                        <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                        <span class="qq-upload-size-selector qq-upload-size"></span>
                        <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">
                            <span class="qq-btn qq-delete-icon" aria-label="Удалить"></span>
                        </button>
                        <button type="button" class="qq-btn qq-upload-pause-selector qq-upload-pause">
                            <span class="qq-btn qq-pause-icon" aria-label="Пауза"></span>
                        </button>
                        <button type="button" class="qq-btn qq-upload-continue-selector qq-upload-continue">
                            <span class="qq-btn qq-continue-icon" aria-label="Продолжить"></span>
                        </button>
                    </div>
                </li>
            </ul>

            <dialog class="qq-alert-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Закрыть</button>
                </div>
            </dialog>

            <dialog class="qq-confirm-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Нет</button>
                    <button type="button" class="qq-ok-button-selector">Да</button>
                </div>
            </dialog>

            <dialog class="qq-prompt-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <input type="text">
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Отмена</button>
                    <button type="button" class="qq-ok-button-selector">Ok</button>
                </div>
            </dialog>
        </div>
    </script>

</head>
<body>
    <div id="fine-uploader-gallery"></div>
    <script>
        var galleryUploader = new qq.FineUploader({
            element: document.getElementById("fine-uploader-gallery"),
            template: 'qq-template-gallery',
            request: {
                endpoint: '../upload/'
            },
            text: {
                formatProgress: "{percent}% из {total_size}",
                failUpload: "Ошибка загрузки",
                waitingForResponse: "Обработка...",
                paused: "Пауза",
                sizeSymbols: [ " Кб", " Мб", " Гб", "Тб", "Пб", "Еб" ],
                fileInputTitle: "Выбор файлов для загрузки",
            },
            thumbnails: {
                placeholders: {
                    waitingPath: '<?php echo $resPath;?>uploader/images/loader.gif',
                    notAvailablePath: '<?php echo $resPath;?>uploader/images/not_available-generic.png'
                }
            },
            validation: {
                allowedExtensions: [<?php echo "'", $uploadExt, "'";?>]
            }
        });
    </script>
</body>
</html>
<?php
    }
}
?>