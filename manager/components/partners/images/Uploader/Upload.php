<?php
/**
 * Gear Manager
 *
 * Контроллер         "Загрузка изображений в фотоальбом"
 * Пакет контроллеров "Профиль загрузчика фотоальбома"
 * Группа пакетов     "Фотоальбомы"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_PPlacesImages_Uploader
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Upload.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Data');

Gear::library('Site/Uploader');

/**
 * Данные профиля наполнения фотоальбома
 * 
 * @category   Gear
 * @package    GController_PPlacesImages_Uploader
 * @subpackage Upload
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Upload.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_PPlacesImages_Uploader_Upload extends GController_Data
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_pplaces_grid';

    /**
     * Загрузка файлов
     * 
     * @return void
     */
    protected function upload()
    {
        $this->dataAccessInsert();

        // загрузка изображений во временный каталог
        $res = GUploader::uploadImage(DOCUMENT_ROOT . $this->config->getFromCms('Site', 'DIR/TEMP'), 'qqfile', 'Partners', true);
        // создаем список временных файлов для добавления
        $list = $this->store->get('upload', array());
        $list[] = $res;
        $this->store->set('upload', $list);
    }

    /**
     * Инициализация запроса
     * 
     * @return void
     */
    protected function init()
    {
        // метод запроса
        switch ($this->uri->method) {
            // метод "POST"
            case 'POST':
                // тип действия
                switch ($this->uri->action) {
                    // загрузка файлов
                    case 'upload':
                        $this->upload();
                        return;
                }
                break;
        }

        parent::init();
    }
}
?>