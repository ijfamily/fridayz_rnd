<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка партнёров"
 * Пакет контроллеров "Список партнёров"
 * Группа пакетов     "Партнёры"
 * Модуль             "Партнёры"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_PPlaces_Grid
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('String');
Gear::library('File');
Gear::controller('Grid/Data');

/**
 * Данные списка партнёров
 * 
 * @category   Gear
 * @package    GController_PPlaces_Grid
 * @subpackage Data
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_PPlaces_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'place_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'place_names';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // язык сайта по умолчанию
        $languageId = $this->config->getFromCms('Site', 'LANGUAGE/ID');
        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS `g`.*, `i`.`images_count`, `c`.`category_name` FROM `place_names` `g` '
            // join `place_images`
          . 'LEFT JOIN (SELECT `place_id`, COUNT(`place_id`) `images_count` '
          . 'FROM `place_images` GROUP BY `place_id`) `i` USING (`place_id`) '
          . 'LEFT JOIN `site_categories` `c` USING (`category_id`) '
          . 'WHERE 1 %filter %fastfilter ORDER BY %sort LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // дата публикации
            'published_date' => array('type' => 'string'),
            'published_time' => array('type' => 'string'),
            'published_user' => array('type' => 'integer'),
            // название
            'place_name' => array('type' => 'string'),
            // изображений
            'images_count' => array('type' => 'integer'),
            // опубликован на сайте
            'published' => array('type' => 'integer'),
            // опубликовано на карте
            'place_map' => array('type' => 'integer'),
            // расширенная информация
            'place_extended' => array('type' => 'integer'),
            // доставка
            'place_delivery' => array('type' => 'integer'),
            // отображать на слайдере
            'slider' => array('type' => 'integer'),
            'slider_index' => array('type' => 'integer'),
            'likes' => array('type' => 'integer'),
            // название категории
            'category_name' => array('type' => 'string'),
            'place_uri' => array('type' => 'string'),
            // переход по ссылке
            'goto_url' => array('type' => 'string'),
            // индексация
            'page_meta_robots' => array('type' => 'string'),
            'page_meta_robots_s' => array('type' => 'string'),
            // показывать вниз
            'block_down' => array('type' => 'integer'),
            // показывать вверх
            'block_up' => array('type' => 'integer'),
        );
    }

    /**
     * Возращает sql запрос для быстрого фильтра
     * (для подстановки "%fastfilter" в $sql)
     * 
     * @param string $key идендификатор поля
     * @param string $value значение
     * @return string
     */
    protected function getTreeFilter($key, $value)
    {
        // идендификатор поля
        switch ($key) {
            // по дате публикации заведения
            case 'byPb':
                $toDate = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') + 1, date('Y')));
                switch ($value) {
                    case 'day':
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') - 1, date('Y')));
                        return ' AND (`g`.`published_date` BETWEEN "' . $fromDate . '" AND "' . $toDate . '")';

                    case 'week':
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') - 7, date('Y')));
                        return ' AND `g`.`published_date` BETWEEN "' . $fromDate . '" AND "' . $toDate . '"';

                    case 'month':
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, date('m') - 1, date('d'), date('Y')));
                        return ' AND `g`.`published_date` BETWEEN "' . $fromDate . '" AND "' . $toDate . '"';
                }
                break;

            // по автору заведения
            case 'byAu':
                return ' AND `g`.`published_user`=' . (int) $value;

            // по категории
            case 'byCt':
                return ' AND `g`.`category_id`="' . (int) $value . '" ';
        }

        return '';
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        $path = DOCUMENT_ROOT .  $this->config->getFromCms('Site', 'DIR/IMAGES');

        $query = new GDb_Query();
        // список всех записей
        $sql = 'SELECT * FROM `place_names`';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        while (!$query->eof()) {
            $rec = $query->next();
            // удаление изображений и документов статей
            if (!empty($rec['place_folder'])) {
                $dir = $path . $rec['place_folder'] . '/';
                if (file_exists($dir))
                    GDir::remove($dir);
            }
        }

        // удаление записей таблицы "place_images" (изображения альбома)
        if ($query->clear('place_images') === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('place_images') === false)
            throw new GSqlException();
        // удаление записей таблицы "place_names" (галерея)
        if ($query->clear('place_names') === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('place_names') === false)
            throw new GSqlException();
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Событие наступает после успешного удаления данных
     * 
     * @return void
     */
    protected function dataDeleteComplete()
    {
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        $query = new GDb_Query();
        $sql = 'SELECT * FROM `place_names` WHERE `place_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $names = $ids = array();
        while (!$query->eof()) {
            $item = $query->next();
            if (empty($item['place_folder']))
                $names[] = $item['place_folder'];
            else {
                $path = DOCUMENT_ROOT .  $this->config->getFromCms('Site', 'DIR/IMAGES') . $item['place_folder'] . '/';
                if (file_exists($path))
                    GDir::remove($path);
                $ids[] = $item['place_id'];
            }
        }
        $ids = implode(',', $ids);
        // если есть что удалять
        if ($ids) {
            // удаление изображений альбома
            $sql = 'DELETE FROM `place_images` WHERE `place_id` IN (' . $ids . ')';
            if ($query->execute($sql) === false)
                throw new GSqlException();
        }
        // если были ошибки
        if ($names)
            throw new GException('Error', sprintf($this->_['msg_cant_delete'], implode(',', $names)));
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        // подсказки для ЧПУ URL категорий
        $record['place_uri'] = '/' . $record['place_uri'] . '/';
        $record['goto_url'] = '<a href="' . $record['place_uri'] . '?preview" target="_blank" title="' . $this->_['tooltip_goto_url'] . '"><img src="' . $this->resourcePath . 'icon-goto.png"></a>';
        // индексация страницы сайта
        if (!empty($record['page_meta_robots'])) {
            $index = $record['page_meta_robots'];
            if ($index == 'index, follow' || $index == 'index, nofollow' || $index == 'all')
                $icon = 'green';
            else
                $icon = 'red';
            $record['page_meta_robots_s'] = $record['page_meta_robots'];
            $record['page_meta_robots'] = '<img src="' . $this->resourcePath . 'icon-index-' . $icon . '.png" align="absmiddle"> ' . $index;
        }
        // если статья не опубликована
        if (empty($record['published']))
            $record['rowCls'] = 'mn-row-notpublished';
        if (!empty($record['slider_index'])) {
            $record['block_up'] = 1;//$record['slider_index'] != 1;
            $record['block_down'] = 1;//$this->countRecords != $record['slider_index'];
        }

        return $record;
    }
}
?>