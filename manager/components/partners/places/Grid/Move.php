<?php
/**
 * Gear Manager
 *
 * Контроллер         "Перемещение блоков"
 * Пакет контроллеров "Список страниц"
 * Группа пакетов     "Landing"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Distributed under the Lesser General Public License (LGPL)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    GController_PPlaces_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Move.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Перемещение блоков
 * 
 * @category   Gear
 * @package    GController_PPlaces_Grid
 * @subpackage Move
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Move.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_PPlaces_Grid_Move extends GController_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'place_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'place_names';

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataMove()
    {
        $this->dataAccessView();

        // действие
        $action = $this->input->get('action');
        $query = new GDb_Query();
        $sql = 'SELECT * FROM `place_names` WHERE `place_id`=' . (int) $this->uri->id;
        if (($block = $query->getRecord($sql)) === false)
            throw new GSqlException();
        if (empty($block))
            throw new GException('Error', 'Selected record was deleted!');
        // предыдущий блок
        $sql = 'SELECT * FROM `place_names` WHERE `place_id`=' . $block['place_id'] . ' AND `slider_index`=';
        switch ($action) {
            case 'moveUp': $bIndex = $block['slider_index'] - 1; break;
            case 'moveDown': $bIndex =$block['slider_index'] + 1; break;
        }
        $sql .= $bIndex;
        if (($prev = $query->getRecord($sql)) === false)
            throw new GSqlException();
        if (empty($prev)) return;
        // перемещение блоков
        $sql = 'UPDATE `place_names` SET `slider_index`=' . $prev['slider_index'] . ' WHERE `place_id`=' . (int) $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'UPDATE `place_names` SET `slider_index`=' . $block['slider_index'] . ' WHERE `place_id`=' . $prev['place_id'];
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        if ($this->uri->action == 'move')
            // метод запроса
            switch ($this->uri->method) {
                // метод "POST"
                case 'POST': $this->dataMove(); return;
            }

        parent::init();
    }
}
?>