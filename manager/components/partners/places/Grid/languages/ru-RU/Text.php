<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2013-2016 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'    => 'Заведения',
    'rowmenu_edit'  => 'Редактировать',
    'rowmenu_image' => 'Изображения',
    'rowmenu_view'  => 'Просмотр изображений',
    'tooltip_grid'  => 'заведения наших партнёров',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">("Заведения", "Изображения")</span> ?',
    'text_btn_config'          => 'Настройка',
    'tooltip_btn_config'       => 'Настройка заведений',
    // столбцы
    'header_published'       => 'Дата публикации',
    'header_place_name'      => 'Название',
    'header_images_count'    => 'Изображений',
    'tooltip_images_count'   => 'Изображений в галереи',
    'header_category_name'   => 'Категория',
    'tooltip_category_name'  => 'Категория статьи',
    'header_place_uri'       => 'ЧПУ URL заведения',
    'tooltip_goto_url'       => 'Просмотр заведения',
    'tooltip_place_extended' => 'Выводить расширенную информацию',
    'tooltip_published'      => 'Опубликовано на сайте',
    'tooltip_place_map'      => 'Опубликовано на карте',
    'tooltip_place_delivery' => 'Доставка',
    'tooltip_slider'         => 'Отображать на слайдере',
    'header_robots'          => 'Индексация',
    'tooltip_robots'         => 'Мета &quot;robots&quot; на странице сайта',
    // развёрнутая запись
    'label_image_title' => 'Изображение',
    'label_image_count' => 'Изображений: <b>%s</b>',
    // быстрый фильтр
    'text_all_records' => 'Все статьи',
    'text_by_date'     => 'По дате правки заведения',
    'text_by_date_pub' => 'По дате публикации заведения',
    'text_by_day'      => 'За день',
    'text_by_week'     => 'За неделю',
    'text_by_month'    => 'За месяц',
    'text_by_author'   => 'По автору заведения',
    'text_by_category' => 'По категории',
    'text_by_yes'      => 'Да',
    'text_by_no'       => 'Нет',
    // сообщения
    'msg_cant_delete' => 'Невозможно удалить заведение "%s", т.к. заведение не имеет каталога изображений',
    // тип
    'data_boolean' => array('нет', 'да')
);
?>