<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка партнёров"
 * Пакет контроллеров "Список партнёров"
 * Группа пакетов     "Партнёры"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SAlbums_Grid
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс списка партнёров
 * 
 * @category   Gear
 * @package    GController_PPlaces_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_PPlaces_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'place_id';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'place_name';

    /**
     * Использовать быстрый фильтр
     *
     * @var boolean
     */
    public $useSlidePanel = true;

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // дата публикации
            array('name' => 'published_date', 'type' => 'string'),
            array('name' => 'published_time', 'type' => 'string'),
            array('name' => 'published_user', 'type' => 'integer'),
            // название
            array('name' => 'place_name', 'type' => 'string'),
            // изображений
            array('name' => 'images_count', 'type' => 'integer'),
            // каталог
            array('name' => 'place_folder', 'type' => 'string'),
            // опубликовано на сайте
            array('name' => 'published', 'type' => 'integer'),
            // опубликовано на карте
            array('name' => 'place_map', 'type' => 'integer'),
            // расширенная информация
            array('name' => 'place_extended', 'type' => 'integer'),
            // доставка
            array('name' => 'place_delivery', 'type' => 'integer'),
            // отображать на слайдере
            array('name' => 'slider', 'type' => 'integer'),
            array('name' => 'slider_index', 'type' => 'integer'),
            array('name' => 'likes', 'type' => 'integer'),
            // название категории
            array('name' => 'category_name', 'type' => 'string'),
            array('name' => 'place_uri', 'type' => 'string'),
            // переход по ссылке
            array('name' => 'goto_url', 'type' => 'string'),
            // индексация
            array('name' => 'page_meta_robots', 'type' => 'string'),
            array('name' => 'page_meta_robots_s', 'type' => 'string'),
            // показывать вниз
            array('name' => 'block_down', 'type' => 'integer'),
            // показывать вверх
            array('name' => 'block_up', 'type' => 'integer'),
        );

        $settings = $this->session->get('user/settings');
        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $this->columns = array(
            array('xtype'     => 'numbercolumn'),
            array('xtype'     => 'rowmenu'),
            array('xtype'     => 'datetimecolumn',
                  'dataIndex' => 'published_date',
                  'timeIndex' => 'published_time',
                  'userIndex' => 'published_user',
                  'frmDate'   => $settings['format/date'],
                  'frmTime'   => $settings['format/time'],
                  'header'    => $this->_['header_published'],
                  'isSystem'  => true,
                  'urlQuery'  => '?state=info',
                  'url'       => 'administration/users/contingent/' . ROUTER_DELIMITER . 'profile/interface/',
                  'width'     => 130,
                  'sortable'  => true,
                  'filter'    => array('type' => 'date')),
            array('dataIndex' => 'place_name',
                  'header'    => '<em class="mn-grid-hd-details"></em>'. $this->_['header_place_name'],
                  'width'     => 200,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'goto_url',
                  'align'     => 'center',
                  'header'    => '&nbsp;',
                  'tooltip'   => $this->_['tooltip_goto_url'],
                  'fixed'     => true,
                  'hideable'  => false,
                  'width'     => 25,
                  'sortable'  => false,
                  'menuDisabled' => true),
            array('dataIndex' => 'place_uri',
                  'header'    => $this->_['header_place_uri'],
                  'width'     => 160,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'category_name',
                  'header'    => $this->_['header_category_name'],
                  'tooltip'   => $this->_['tooltip_category_name'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'images_count',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-image.png" align="absmiddle">',
                  'tooltip'   => $this->_['tooltip_images_count'],
                  'align'     => 'center',
                  'width'     => 45,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('xtype'     => 'booleancolumn',
                  'align'     => 'center',
                  'cls'       => 'mn-bg-color-gray2',
                  'dataIndex' => 'place_delivery',
                  'tooltip'   => $this->_['tooltip_place_delivery'],
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-delivery.png" align="absmiddle">',
                  'width'     => 40,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean')),
            array('xtype'     => 'booleancolumn',
                  'align'     => 'center',
                  'cls'       => 'mn-bg-color-gray2',
                  'dataIndex' => 'place_map',
                  'tooltip'   => $this->_['tooltip_place_map'],
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-map.png" align="absmiddle">',
                  'width'     => 40,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean')),
            array('xtype'     => 'booleancolumn',
                  'align'     => 'center',
                  'cls'       => 'mn-bg-color-gray2',
                  'dataIndex' => 'place_extended',
                  'tooltip'   => $this->_['tooltip_place_extended'],
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-extended.png" align="absmiddle">',
                  'width'     => 40,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean')),
            array('xtype'     => 'booleancolumn',
                  'align'     => 'center',
                  'cls'       => 'mn-bg-color-gray2',
                  'dataIndex' => 'slider',
                  'tooltip'   => $this->_['tooltip_slider'],
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-slider.png" align="absmiddle">',
                  'width'     => 60,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean')),
            array('dataIndex' => 'slider_index',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-slider.png" align="absmiddle"> №',
                  'width'     => 60,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            /*
            array('xtype'     => 'actioncolumn',
                  'dataIndex' => 'slider_index',
                  'enabledIndex' => 'block_up',
                  'gridId'    => $this->classId,
                  'url'       => $this->componentUrl . 'grid/move/',
                  'icon'      => $this->resourcePath . 'icon-arrow-up.png',
                  'iconDisabled' => $this->resourcePath . 'icon-arrow-up-d.png',
                  'action'    => 'moveUp',
                  'tooltip'   => $this->_['tooltip_move_up'],
                  'header'    => '&nbsp;',
                  'width'     => 55,
                  'sortable'  => false),
            array('xtype'     => 'actioncolumn',
                  'dataIndex' => 'slider_index',
                  'enabledIndex' => 'block_down',
                  'gridId'    => $this->classId,
                  'url'       => $this->componentUrl . 'grid/move/',
                  'icon'      => $this->resourcePath . 'icon-arrow-down.png',
                  'iconDisabled' => $this->resourcePath . 'icon-arrow-down-d.png',
                  'action'    => 'moveDown',
                  'tooltip'   => $this->_['tooltip_move_down'],
                  'header'    => '&nbsp;',
                  'width'     => 55,
                  'sortable'  => false),
            */
            array('xtype'     => 'booleancolumn',
                  'align'     => 'center',
                  'cls'       => 'mn-bg-color-gray2',
                  'dataIndex' => 'likes',
                  'tooltip'   => 'Like',
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => 'Like',
                  'width'     => 40,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean')),
            array('xtype'     => 'booleancolumn',
                  'align'     => 'center',
                  'cls'       => 'mn-bg-color-gray2',
                  'dataIndex' => 'published',
                  'tooltip'   => $this->_['tooltip_published'],
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-visible.png" align="absmiddle">',
                  'width'     => 40,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean')),
            array('dataIndex' => 'page_meta_robots',
                  'header'    => $this->_['header_robots'],
                  'tooltip'   => $this->_['tooltip_robots'],
                  'width'     => 115,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string'))
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_grid'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_grid'],
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'profileUrl'    => $this->componentUrl . 'profile/')
        );

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "edit" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "добавить" (ExtJS class "Manager.button.InsertData")
        $group->items->add(array('xtype' => 'mn-btn-insert-data', 'gridId' => $this->classId));
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' => $this->classId));
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'      => 'mn-btn-clear-data',
                  'msgConfirm' => $this->_['msg_btn_clear'],
                  'gridId'     => $this->classId,
                  'isN'        => true)
        );
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка настройки" (ExtJS class "Manager.button.Widget")
        $group->items->add(
            array('xtype'   => 'mn-btn-widget',
                  'width'   => 60,
                  'text'    => $this->_['text_btn_config'],
                  'tooltip' => $this->_['tooltip_btn_config'],
                  'icon'    => $this->resourcePath . 'icon-btn-config.png',
                  'url'     => $this->componentUri . '../config/' . ROUTER_DELIMITER . 'profile/interface/')
        );
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = 'component-site-albums';
        $this->_cmp->tbar->items->add($group);

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $cellInfo =
            '<div class="mn-grid-cell-tooltip-tl">{place_name}</div>'
          . '<div class="mn-grid-cell-tooltip">'
          . '<em>' . $this->_['header_published'] . '</em>: <b>{published_date} {published_time}</b><br>'
          . '<em>' . $this->_['tooltip_published'] . '</em>: '
          . '<tpl if="published == 0"><b>' . $this->_['data_boolean'][0] . '</b></tpl>'
          . '<tpl if="published == 1"><b>' . $this->_['data_boolean'][1] . '</b></tpl><br>'
          . '<em>' . $this->_['tooltip_images_count'] . '</em>: <b>{images_count}</b><br>'
          . '<em>' . $this->_['header_category_name'] . '</em>: <b>{category_name}</b><br>'
          . '</div>';
        $this->_cmp->cellTips = /*array(
            array('field' => 'place_name', 'tpl' => $cellInfo),
            array('field' => 'category_name', 'tpl' => '{category_name}')
        );*/array();

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $items = array(
            array('text'    => $this->_['rowmenu_edit'],
                  'iconCls' => 'icon-form-edit',
                  'url'     => $this->componentUrl . 'profile/interface/'),
            array('xtype'   => 'menuseparator'),
            array('text'    => $this->_['rowmenu_image'],
                  'icon'    => $this->resourcePath . 'icon-hd-image.png',
                  'url'     => $this->componentUrl . '../../images/' . ROUTER_DELIMITER . 'grid/interface/'),
            array('text'    => $this->_['rowmenu_view'],
                  'icon'    => $this->resourcePath . 'icon-hd-view.png',
                  'url'     => $this->componentUrl . '../../images/' . ROUTER_DELIMITER . 'view/interface/')
        );
        $this->_cmp->rowMenu = array('xtype' => 'mn-grid-rowmenu', 'items' => $items);

        // быстрый фильтр списка (ExtJs class "Manager.tree.GridFilter")
        $this->_slidePanel->cls = 'mn-tree-gridfilter';
        $this->_slidePanel->width = 250;
        $this->_slidePanel->initRoot = array(
            'text'     => 'Filter',
            'id'       => 'byRoot',
            'expanded' => true,
            'children' => array(
                array('text'     => $this->_['text_all_records'],
                      'value'    => 'all',
                      'expanded' => true,
                      'leaf'     => false,
                      'children' => array(
                        array('text'     => $this->_['text_by_date_pub'],
                              'id'       => 'byPb',
                              'leaf'     => false,
                              'expanded' => true,
                              'children' => array(
                                  array('text'     => $this->_['text_by_day'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 'day'),
                                  array('text'     => $this->_['text_by_week'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true, 
                                        'value'    => 'week'),
                                  array('text'     => $this->_['text_by_month'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 'month')
                              )
                        ),
                        array('text'    => $this->_['text_by_author'],
                              'id'      => 'byAu',
                              'leaf'    => false),
                        array('text'    => $this->_['text_by_category'],
                              'id'      => 'byCt',
                              'leaf'    => false)
                      )
                )
            )
        );

        parent::getInterface();
    }
}
?>