<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск заведений партнёров"
 * Пакет контроллеров "Поиск заведений партнёров"
 * Группа пакетов     "Партнёры"
 * Модуль             "Партнёры"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_PPlaces_Search
 * @copyright  Copyright (c) 2013-2015 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Data.php 2015-07-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск заведений партнёров
 * 
 * @category   Gear
 * @package    GController_PPlaces_Search
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2015 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Data.php 2015-07-01 12:00:00 Gear Magic $
 */
final class GController_PPlaces_Search_Data extends GController_Search_Data
{
    /**
     * дентификатор компонента (списка) к которому применяется поиск
     *
     * @var string
     */
    public $gridId = 'gcontroller_pplaces_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_pplaces_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор
     * Используется для инициализации атрибутов контроллер не переданного в конструктор
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'place_name', 'label' => $this->_['header_place_name']),
        );
    }
}
?>