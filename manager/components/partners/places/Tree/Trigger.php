<?php
/**
 * Gear Manager
 *
 * Контроллер         "Триггер дерева"
 * Пакет контроллеров "Триггер дерева"
 * Группа пакетов     "Список партнёров"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Distributed under the Lesser General Public License (LGPL)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    GController_PPlaces_Tree
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Tree/Trigger');

/**
 * Триггер дерева
 * 
 * @category   Gear
 * @package    GController_PPlaces_Tree
 * @subpackage Trigger
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_PPlaces_Tree_Trigger extends GController_Tree_Trigger
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_pplaces_grid';

    /**
     * Возращает список категорий
     *
     * @return array
     */
    protected function getCategories()
    {
        $data = array();
        $query = new GDb_Query();
        $sql = 'SELECT `c`.*, COUNT(*) `count` FROM `place_names` `a` JOIN `site_categories` `c` USING(`category_id`) GROUP BY `category_id`';
        if ($query->execute($sql) !== true)
            throw new GSqlException();
        while (!$query->eof()) {
            $rec = $query->next();
            $data[] = array(
                'text'    => $rec['category_name'] . ' <b>(' . $rec['count'] . ')</b>',
                'qtip'    => $rec['category_name'],
                'leaf'    => true,
                'iconCls' => 'icon-folder-find',
                'value'   => $rec['category_id']
            );
        }

        return $data;
    }

    /**
     * Возращает список авторов
     *
     * @return array
     */
    protected function getAuthors()
    {
        $data = array();
        $query = new GDb_Query();
        $sql = 'SELECT `u`.*, `p`.`profile_name`, COUNT(*) `count` FROM `place_names` `a` '
             . 'JOIN `gear_users` `u` ON `a`.`published_user`=`u`.`user_id` '
             . 'JOIN `gear_user_profiles` `p` USING(`user_id`) GROUP BY `u`.`user_id`';
        if ($query->execute($sql) !== true)
            throw new GSqlException();
        while (!$query->eof()) {
            $rec = $query->next();
            $data[] = array(
                'text'    => $rec['profile_name'] . ' <b>(' . $rec['count'] . ')</b>',
                'qtip'    => $rec['profile_name'],
                'leaf'    => true,
                'iconCls' => 'icon-folder-find',
                'value'   => $rec['user_id']
            );
        }

        return $data;
    }

    /**
     * Вывод данных в интерфейс компонента
     * 
     * @param  string $name название триггера
     * @param  string $node id выбранного узла
     * @return void
     */
    protected function dataView($name, $node)
    {
        parent::dataView($name, $node);

        // триггер
        switch ($name) {
            // быстрый фильтр списка
            case 'gridFilter':
                // id выбранного узла
                switch ($node) {
                    // по категории
                    case 'byCt': $this->response->data = $this->getCategories(); break;

                    // по правам авторам
                    case 'byAu': $this->response->data = $this->getAuthors(); break;

                    // по виду статьи
                    case 'byTv': $this->response->data = $this->getTypeView(); break;
                }
        }
    }
}
?>