<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Заведения партнёров"
 * Группа пакетов     "Заведения партнёров"
 * Модуль             "Партнёры"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_PPlaces
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Заведения
 * Описание: Заведения наших партнёров
 * Меню: Заведения
 * ID класса: gcontroller_pplaces_grid
 * Модуль: Партнёры
 * Группа: Партнёры
 * Очищать: нет
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске компонентов: нет
 *    В главном меню: нет
 * Ресурс
 *    Компонент: partners/places/
 *    Контроллер: partners/places/Grid/
 *    Интерфейс: grid/interface/
 *    Меню: profile/interface/
 */

return array(
    // {S}- модуль "Partners" {} - пакет контроллеров "Partner places" -> PPlaces
    'clsPrefix' => 'PPlaces',
    // использовать язык
    'language'  => 'ru-RU'
);
?>