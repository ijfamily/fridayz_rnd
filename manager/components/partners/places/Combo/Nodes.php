<?php
/**
 * Gear Manager
 *
 * Контроллер         "Триггер выпадающего дерева категорий статей"
 * Пакет контроллеров "Заведения"
 * Группа пакетов     "Заведения"
 * Модуль             "Партнёры"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_PPlaces_Combo
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Nodes.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Combo/Nodes');

/**
 * Триггер выпадающего дерева категорий статей
 * 
 * @category   Gear
 * @package    GController_PPlaces_Combo
 * @subpackage Nodes
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Nodes.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_PPlaces_Combo_Nodes extends GController_Combo_NNodes
{
    /**
     * Префикс полей таблицы для формирования nested set
     *
     * @var string
     */
    public $fieldPrefix = 'category';

    /**
     * Поле используемое для сортировки данных
     *
     * @var string
     */
    public $orderBy = 'left';

    /**
     * Первичное поле таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'category_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_categories';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_pplaces_grid';

    /**
     * Возращает первые узлы дерева по условию
     * 
     * @return array
     */
    protected function getFirstNodes()
    {
        if ($this->uri->getVar('node', 0) == 1)
            return array(
                array(
                    'expanded' => false,
                    'icon'     => $this->resourcePath . 'icons/icon-category-none.png',
                    'leaf'     => true,
                    'text'     => $this->_['text_node_none'],
                    'id'       => -1
                )
            );
        else
            return array();
    }

    /**
     * Предварительная обработка узла дерева перед формированием массива записей JSON
     * 
     * @params array $node узел дерева
     * @params array $record запись
     * @return array
     */
    protected function nodesPreprocessing($node, $record)
    {
        $node['text'] = $record['category_name'];
        if ($record['category_right'] - $record['category_left'] == 1)
            $node['icon'] = $this->resourcePath . 'icons/icon-category.png';

        return $node;
    }
}
?>