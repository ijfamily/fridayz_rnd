<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс вывода справочной информации"
 * Пакет контроллеров "Справочная информация"
 * Группа пакетов     "Настройки"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YGuide_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::ext('Container/Panel/Window');
Gear::controller('Interface');

/**
 * Интерфейс вывода справочной информации
 * 
 * @category   Gear
 * @package    GController_YGuide_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_PPlaces_Map_Interface extends GController_Interface
{
    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp = new Ext_Window(
            array('id'               => strtolower(__CLASS__),
                  'xtype'            => 'window',
                  'closable'         => true,
                  'iconCls'          => 'icon-form-edit',
                  'resizable'        => true,
                  'modal'            => true,
                  'gridId'           => '',
                  'layout'           => 'fit',
                  'plain'            => false,
                  'modal' => false,
                  'width' => 400,
                  'height' => 400,
                  //'bodyStyle'        => 'padding:5px;',
                  'items' => array(array('xtype' => 'mn-iframe', 'url' => 'http://webmap-blog.ru/tools/getlonglat-ymap2.html')),
                  'closeAfterUpdate' => true)
        );
    }

}
?>