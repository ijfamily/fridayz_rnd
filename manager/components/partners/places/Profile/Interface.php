<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля заведения"
 * Пакет контроллеров "Профиль заведения"
 * Группа пакетов     "Заведения"
 * Модуль             "Партнёры"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_PPlaces_Profile
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля заведения
 * 
 * @category   Gear
 * @package    GController_PPlaces_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_PPlaces_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Возращает интерфейса редактора
     * 
     * @return void
     */
    protected function getEditor()
    {
        return array(
              'xtype'         => 'mn-tinymce',
              'id'            => 'htmleditor-place',
              'hideLabel'     => true,
              'name'          => 'place_text',
              'anchor'        => '100% 100%',
              'allowBlank'    => true,
              'tinyMCEConfig' => array(
                  'languageId'                    => $this->config->getFromCms('Site', 'LANGUAGE/ID'),
                  'language'                      => $this->config->getFromCms('Site', 'LANGUAGE/ALIAS'),
                  'uploadUrl'                     => $this->uri->scriptName . '/' . $this->componentUrl . 'editor/data/',
                  'componentIcons'                => PATH_COMPONENT . 'site/articles/articles/Component/resources/icons/',
                  'components'                    => array(),
                  'keep_styles'                   => false,
                  'allow_html_in_named_anchor'    => true,
                  'allow_conditional_comments'    => true,
                  'protected'                     => array(),
                  'cleanup'                       => false,
                  'verify_html'                   => false,
                  'cleanup_on_startup'            => false,
                  'extended_valid_elements'       => '*[*]',
                  'schema'                        => 'html5',
                  'preformatted'                  => true,
                  'cleanup_on_startup'            => false,
                  'trim_span_elements'            => false,
                  'verify_html'                   => false,
                  'cleanup'                       => false,
                  'convert_urls'                  => false,
                  'paste_auto_cleanup_on_paste'   => false,
                  'paste_block_drop'              => false,
                  'paste_remove_spans'            => false,
                  'paste_strip_class_attributes'  => false,
                  'paste_retain_style_properties' => '',
                  'inline_styles'                 => false
              )
        );
    }

    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        $data = array('folder' => '', 'coord' => '');

        // если состояние формы "вставка"
        if ($this->isInsert) {
            // все загружаемые документы и изображения привязываются к номеру папки статьи
            $data['folder'] = time();

            return $data;
        }

        parent::getDataInterface();

        $query = new GDb_Query();
        // выбранная статья, категория статьи, доступ к статье
        $sql = 'SELECT * FROM `place_names` WHERE `place_id`=' . (int)$this->uri->id;
        if (($data = $query->getRecord($sql)) === false)
            throw new GSqlException();

        if (empty($data['place_folder'])) {
            $data['place_folder'] = time();
        }
        $data['folder'] = $data['place_folder'];
        $data['coord'] = '?latitude=' . $data['place_coord_lt'] . '&longitude=' . $data['place_coord_ln'];

        return $data;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();
        $this->store->set('folder', $data['folder'], 'gcontroller_sarticles_grid');

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'titleEllipsis' => 45,
                  'gridId'        => 'gcontroller_pplaces_grid',
                  'width'         => 800,
                  'height'        => 650,
                  'stateful'      => false,
                  'resizable'     => false,
                  'buttonsAdd'    => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-parnters-places',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );

        // поля формы (ExtJS class "Ext.Panel")
        $langs = $this->config->getFromCms('Site', 'LANGUAGES');
        $lang = $langs[$this->config->getFromCms('Site', 'LANGUAGE')];

        $itemUri = array(
            'xtype'      => 'fieldset',
            'anchor'     => '99%',
            'labelWidth' => 62,
            'title'      => $this->_['title_fieldset_address'],
            'autoHeight' => true,
            'cls'        => 'mn-container-clean',
            'layout'     => 'column',
            'items'      => array(
                  array('layout' => 'form',
                        'width'  => 500,
                        'items'  => array(
                              array('xtype'      => 'textfield',
                                    'id'         => 'fldArticleUri',
                                    'fieldLabel' => $this->_['label_place_uri'],
                                    'labelTip'   => $this->_['tip_place_uri'],
                                    'name'       => 'place_uri',
                                    'blankText'  => $this->_['blank_place_uri'],
                                    'maxLength'  => 255,
                                    'anchor'     => '100%',
                                    'allowBlank' => false,
                                    'value'      => '')
                          )
                    ),
                    array('layout'    => 'form',
                          'width'     => 33,
                          'bodyStyle' => 'margin-left:3px;',
                          'items'     => array(
                              array('xtype'   => 'mn-btn-genurl',
                                    'type'    => 1,
                                    'tooltip' => $this->_['tip_button_genurl'],
                                    'icon'    => $this->resourcePath . 'icon-btn-gear.png',
                                    'url'     => $this->componentUrl . '../' . ROUTER_DELIMITER . 'generator/data/',
                                    'msgSelectHeader' => sprintf($this->_['msg_select_header'], $lang['title']),
                                    'msgSelectNode'   => $this->_['msg_select_node'])
                    )
              )
            )
        );

        // вкладка "атрибуты"
        $tabAttr = array(
            'iconSrc'     => $this->resourcePath . 'icon-tab-attr.png',
            'title'       => $this->_['title_tab_attributes'],
            'layout'      => 'form',
            'labelWidth'  => 110,
            'baseCls'     => 'mn-form-tab-body',
            'autoScroll' => true,
            'items'       => array(
                array('xtype'      => 'textfield',
                      'fieldLabel' => $this->_['label_place_name'],
                      'name'       => 'place_name',
                      'maxLength'  => 255,
                      'width'      => 565,
                      'allowBlank' => false),
                array('xtype'           => 'mn-field-browse',
                      'params'          => 'view=images',
                      'triggerWdgSetTo' => true,
                      'triggerWdgUrl'   => 'site/media/' . ROUTER_DELIMITER .'dialog/interface/',
                      'fieldLabel' => $this->_['label_place_image'],
                      'itemCls'    => 'mn-form-item-quiet',
                      'id'         => 'place_image',
                      'name'       => 'place_image',
                      'width'      => 565,
                      'allowBlank' => true),
                array('xtype'           => 'mn-field-browse',
                      'params'          => 'view=images',
                      'triggerWdgSetTo' => true,
                      'triggerWdgUrl'   => 'site/media/' . ROUTER_DELIMITER .'dialog/interface/',
                      'fieldLabel' => $this->_['label_place_logo'],
                      'itemCls'    => 'mn-form-item-quiet',
                      'id'         => 'place_logo',
                      'name'       => 'place_logo',
                      'width'      => 565,
                      'allowBlank' => true),
                array('xtype'           => 'mn-field-browse',
                      'params'          => 'view=images',
                      'triggerWdgSetTo' => true,
                      'triggerWdgUrl'   => 'site/media/' . ROUTER_DELIMITER .'dialog/interface/',
                      'fieldLabel' => $this->_['label_place_head'],
                      'itemCls'    => 'mn-form-item-quiet',
                      'id'         => 'place_head',
                      'name'       => 'place_head',
                      'width'      => 565,
                      'allowBlank' => true),
                array('xtype'      => 'mn-field-combo-tree',
                      'itemCls'    => 'mn-form-item-quiet',
                      'id'         => 'fldCategory',
                      'fieldLabel' => $this->_['label_category_name'],
                      'labelTip'   => $this->_['tip_category_name'],
                      'width'      => 225,
                      'name'       => 'category_id',
                      'hiddenName' => 'category_id',
                      'resetable'  => false,
                      'treeWidth'  => 400,
                      'treeRoot'   => array('id' => 1, 'expanded' => true, 'expandable' => true),
                      'allowBlank' => true,
                      'store'      => array(
                          'xtype' => 'jsonstore',
                          'url'   => $this->componentUrl . 'combo/nodes/'
                       )
                ),
                array('xtype'      => 'fieldset',
                      'anchor'     => '99%',
                      'labelWidth' => 100,
                      'title'      => $this->_['title_fieldset_slider'],
                      'autoHeight' => true,
                      'items'      => array(
                        array('xtype'      => 'numberfield',
                              'fieldLabel' => $this->_['label_slider_index'],
                              'name'       => 'slider_index',
                              'maxLength'  => 4,
                              'width'      => 80,
                              'allowBlank' => true),
                        array('xtype'           => 'mn-field-browse',
                              'params'          => 'view=images',
                              'triggerWdgSetTo' => true,
                              'triggerWdgUrl'   => 'site/media/' . ROUTER_DELIMITER .'dialog/interface/',
                              'fieldLabel' => $this->_['label_slider_image'],
                              'itemCls'    => 'mn-form-item-quiet',
                              'id'         => 'slider_image',
                              'name'       => 'slider_image',
                              'width'      => 565,
                              'allowBlank' => true),
                        array('xtype'           => 'mn-field-browse',
                              'params'          => 'view=images',
                              'triggerWdgSetTo' => true,
                              'triggerWdgUrl'   => 'site/media/' . ROUTER_DELIMITER .'dialog/interface/',
                              'fieldLabel' => $this->_['label_slider_logo'],
                              'itemCls'    => 'mn-form-item-quiet',
                              'id'         => 'slider_logo',
                              'name'       => 'slider_logo',
                              'width'      => 565,
                              'allowBlank' => true),
                        array('xtype'      => 'mn-field-chbox',
                              'fieldLabel' => $this->_['label_slider'],
                              'default'    => $this->isUpdate ? null : 1,
                              'name'       => 'slider'),
                      )
                ),
                array('xtype'      => 'fieldset',
                      'anchor'     => '99%',
                      'labelWidth' => 100,
                      'title'      => $this->_['title_fieldset_info'],
                      'autoHeight' => true,
                      'items'      => array(
                            array('xtype'      => 'textfield',
                                  'fieldLabel' => $this->_['label_place_address'],
                                  'name'       => 'place_address',
                                  'maxLength'  => 255,
                                  'anchor'     => '100%',
                                  'allowBlank' => true),
                            array('xtype'      => 'textfield',
                                  'fieldLabel' => $this->_['label_place_kitchen'],
                                  'name'       => 'place_kitchen',
                                  'maxLength'  => 255,
                                  'anchor'     => '100%',
                                  'allowBlank' => true),
                            array('xtype'      => 'textfield',
                                  'fieldLabel' => $this->_['label_place_phone'],
                                  'name'       => 'place_phone',
                                  'maxLength'  => 255,
                                  'anchor'     => '100%',
                                  'allowBlank' => true),
                            array('xtype'      => 'textfield',
                                  'fieldLabel' => $this->_['label_place_schedule'],
                                  'name'       => 'place_schedule',
                                  'maxLength'  => 255,
                                  'width'      => 245,
                                  'allowBlank' => true),
                            array('xtype'      => 'textarea',
                                  'fieldLabel' => $this->_['label_place_site'],
                                  'name'       => 'place_site',
                                  'itemCls'    => 'mn-form-item-quiet',
                                  'anchor'     => '100%',
                                  'height'     => 50,
                                  'allowBlank' => true)
                      )
                ),
                array('xtype'      => 'fieldset',
                      'anchor'     => '99%',
                      'labelWidth' => 100,
                      'title'      => $this->_['title_fieldset_features'],
                      'autoHeight' => true,
                      'items'      => array(
                            array('xtype'  => 'container',
                                  'layout' => 'column',
                                  'cls'    => 'mn-container-clean',
                                  'items'  => array(
                                      array('layout' => 'form',
                                            'width'  => 150,
                                            'items'  => array(
                                                array('xtype'      => 'mn-field-chbox',
                                                      'itemCls'    => 'mn-form-item-quiet',
                                                      'fieldLabel' => $this->_['label_attr_wifi'],
                                                      'default'    => $this->isUpdate ? null : 0,
                                                      'checkDirty' => false,
                                                      'name'       => 'attributes[wifi]'),
                                                array('xtype'      => 'mn-field-chbox',
                                                      'itemCls'    => 'mn-form-item-quiet',
                                                      'fieldLabel' => $this->_['label_attr_hookah'],
                                                      'default'    => $this->isUpdate ? null : 0,
                                                      'checkDirty' => false,
                                                      'name'       => 'attributes[hookah]'),
                                                array('xtype'      => 'mn-field-chbox',
                                                      'itemCls'    => 'mn-form-item-quiet',
                                                      'fieldLabel' => $this->_['label_attr_tv'],
                                                      'default'    => $this->isUpdate ? null : 0,
                                                      'checkDirty' => false,
                                                      'name'       => 'attributes[tv]'),
                                                array('xtype'      => 'mn-field-chbox',
                                                      'itemCls'    => 'mn-form-item-quiet',
                                                      'fieldLabel' => $this->_['label_attr_panoramic_view'],
                                                      'default'    => $this->isUpdate ? null : 0,
                                                      'checkDirty' => false,
                                                      'name'       => 'attributes[panoramic_view]')
                                            )
                                      ),
                                      array('layout' => 'form',
                                            'width'  => 150,
                                            'items'  => array(
                                                array('xtype'      => 'mn-field-chbox',
                                                      'itemCls'    => 'mn-form-item-quiet',
                                                      'fieldLabel' => $this->_['label_attr_karaoke'],
                                                      'default'    => $this->isUpdate ? null : 0,
                                                      'checkDirty' => false,
                                                      'name'       => 'attributes[karaoke]'),
                                                array('xtype'      => 'mn-field-chbox',
                                                      'itemCls'    => 'mn-form-item-quiet',
                                                      'fieldLabel' => $this->_['label_attr_parking'],
                                                      'default'    => $this->isUpdate ? null : 0,
                                                      'checkDirty' => false,
                                                      'name'       => 'attributes[parking]'),
                                                array('xtype'      => 'mn-field-chbox',
                                                      'itemCls'    => 'mn-form-item-quiet',
                                                      'fieldLabel' => $this->_['label_attr_vip_rooms'],
                                                      'default'    => $this->isUpdate ? null : 0,
                                                      'checkDirty' => false,
                                                      'name'       => 'attributes[vip_rooms]'),
                                                array('xtype'      => 'mn-field-chbox',
                                                      'itemCls'    => 'mn-form-item-quiet',
                                                      'fieldLabel' => $this->_['label_attr_show_program'],
                                                      'default'    => $this->isUpdate ? null : 0,
                                                      'checkDirty' => false,
                                                      'name'       => 'attributes[show_program]')
                                            )
                                      ),
                                      array('layout' => 'form',
                                            'width'  => 150,
                                            'items'  => array(
                                                array('xtype'      => 'mn-field-chbox',
                                                      'itemCls'    => 'mn-form-item-quiet',
                                                      'fieldLabel' => $this->_['label_attr_music'],
                                                      'default'    => $this->isUpdate ? null : 0,
                                                      'checkDirty' => false,
                                                      'name'       => 'attributes[music]'),
                                                array('xtype'      => 'mn-field-chbox',
                                                      'itemCls'    => 'mn-form-item-quiet',
                                                      'fieldLabel' => $this->_['label_attr_banquet'],
                                                      'default'    => $this->isUpdate ? null : 0,
                                                      'checkDirty' => false,
                                                      'name'       => 'attributes[banquet]'),
                                                array('xtype'      => 'mn-field-chbox',
                                                      'itemCls'    => 'mn-form-item-quiet',
                                                      'fieldLabel' => $this->_['label_attr_business_lunch'],
                                                      'default'    => $this->isUpdate ? null : 0,
                                                      'checkDirty' => false,
                                                      'name'       => 'attributes[business_lunch]')
                                            )
                                      ),
                                      array('layout' => 'form',
                                            'width'  => 190,
                                            'labelWidth' => 120,
                                            'items'  => array(
                                                array('xtype'      => 'mn-field-chbox',
                                                      'itemCls'    => 'mn-form-item-quiet',
                                                      'fieldLabel' => $this->_['label_attr_childrens_menu'],
                                                      'default'    => $this->isUpdate ? null : 0,
                                                      'checkDirty' => false,
                                                      'name'       => 'attributes[childrens_menu]'),
                                                array('xtype'      => 'mn-field-chbox',
                                                      'itemCls'    => 'mn-form-item-quiet',
                                                      'fieldLabel' => $this->_['label_attr_baking'],
                                                      'default'    => $this->isUpdate ? null : 0,
                                                      'checkDirty' => false,
                                                      'name'       => 'attributes[baking]'),
                                                array('xtype'      => 'mn-field-chbox',
                                                      'itemCls'    => 'mn-form-item-quiet',
                                                      'fieldLabel' => $this->_['label_attr_summer_playground'],
                                                      'default'    => $this->isUpdate ? null : 0,
                                                      'checkDirty' => false,
                                                      'name'       => 'attributes[summer_playground]')
                                            )
                                      )
                                  )
                            )
                      )
                ),
                array('xtype'      => 'fieldset',
                      'labelWidth' => 135,
                      'title'      => $this->_['title_fieldset_net'],
                      'items'      => array(
                          array('xtype'  => 'container',
                                'layout' => 'column',
                                'cls'    => 'mn-container-clean',
                                'items'  => array(
                                    array('layout'      => 'form',
                                          'columnWidth' => 0.5,
                                          'labelWidth'  => 70,
                                          'items'       => array(
                                              array('xtype'      => 'textfield',
                                                    'fieldLabel' => 'Facebook',
                                                    'checkDirty' => false,
                                                    'name'       => 'attributes[facebook]',
                                                    'itemCls'    => 'mn-form-item-quiet',
                                                    'maxLength'  => 255,
                                                    'anchor'     => '100%'),
                                              array('xtype'      => 'textfield',
                                                    'fieldLabel' => 'LinkedIn',
                                                    'checkDirty' => false,
                                                    'name'       => 'attributes[linkedin]',
                                                    'itemCls'    => 'mn-form-item-quiet',
                                                    'maxLength'  => 255,
                                                    'anchor'     => '100%'),
                                              array('xtype'      => 'textfield',
                                                    'fieldLabel' => 'Instagram',
                                                    'checkDirty' => false,
                                                    'name'       => 'attributes[instagram]',
                                                    'itemCls'    => 'mn-form-item-quiet',
                                                    'maxLength'  => 255,
                                                    'anchor'     => '100%'),
                                          ) // items
                                    ), // layout form
                                    array('layout'      => 'form',
                                          'columnWidth' => 0.5,
                                          'labelWidth'  => 58,
                                          'bodyStyle'   => 'padding-left:12px;',
                                          'items'       => array(
                                            array('xtype'      => 'textfield',
                                                  'fieldLabel' => 'Twitter',
                                                  'checkDirty' => false,
                                                  'name'       => 'attributes[twitter]',
                                                  'itemCls'    => 'mn-form-item-quiet',
                                                  'maxLength'  => 255,
                                                  'anchor'     => '100%'),
                                            array('xtype'      => 'textfield',
                                                  'fieldLabel' => 'VK',
                                                  'checkDirty' => false,
                                                  'name'       => 'attributes[vk]',
                                                  'itemCls'    => 'mn-form-item-quiet',
                                                  'maxLength'  => 255,
                                                  'anchor'     => '100%'),
                                            array('xtype'      => 'textfield',
                                                  'fieldLabel' => 'OK',
                                                  'checkDirty' => false,
                                                  'name'       => 'attributes[ok]',
                                                  'itemCls'    => 'mn-form-item-quiet',
                                                  'maxLength'  => 255,
                                                  'anchor'     => '100%')
                                          ) // items
                                    ) // layout form
                              ) // items container
                        ) // container
                    ) // items fieldset
                ),
                array('xtype'      => 'fieldset',
                      'anchor'     => '99%',
                      'labelWidth' => 100,
                      'title'      => $this->_['title_fieldset_delivery'],
                      'autoHeight' => true,
                      'items'      => array(
                            array('xtype'      => 'mn-field-chbox',
                                  'fieldLabel' => $this->_['label_place_delivery'],
                                  'default'    => $this->isUpdate ? null : 0,
                                  'itemCls'    => 'mn-form-item-quiet',
                                  'name'       => 'place_delivery'),
                            array('xtype'      => 'textfield',
                                  'fieldLabel' => $this->_['label_place_note'],
                                  'name'       => 'place_delivery_note',
                                  'itemCls'    => 'mn-form-item-quiet',
                                  'maxLength'  => 255,
                                  'anchor'     => '100%',
                                  'allowBlank' => true),
                      )
                ),
                array('xtype'      => 'fieldset',
                      'anchor'     => '99%',
                      'title'      => $this->_['title_fieldset_coord'],
                      'autoHeight' => true,
                      'layout'     => 'column',
                      'cls'        => 'mn-container-clean',
                      'items'      => array(
                          array('layout'     => 'form',
                                'width'      => 185,
                                'labelWidth' => 63,
                                'items'      => array(
                                    array('xtype'      => 'textfield',
                                          'id'         => 'placeCoordLt',
                                          'itemCls'    => 'mn-form-item-quiet',
                                          'fieldLabel' => $this->_['label_coord_lat'],
                                          'name'       => 'place_coord_lt',
                                          'maxLength'  => 14,
                                          'width'      => 90,
                                          'allowBlank' => true)
                                )
                          ),
                          array('layout'     => 'form',
                                'labelWidth' => 70,
                                'items'      => array(
                                    array('xtype'      => 'textfield',
                                          'id'         => 'placeCoordLn',
                                          'itemCls'    => 'mn-form-item-quiet',
                                          'fieldLabel' => $this->_['label_coord_long'],
                                          'name'       => 'place_coord_ln',
                                          'maxLength'  => 14,
                                          'width'      => 90,
                                          'allowBlank' => true),
                                )
                          ),
                      )
                ),
                array('xtype'      => 'fieldset',
                      'anchor'     => '99%',
                      'title'      => $this->_['title_fieldset_coord1'],
                      'autoHeight' => true,
                      'items'      => array(
                            array('xtype'      => 'textfield',
                                  'fieldLabel' => $this->_['label_place_coord_other'],
                                  'name'       => 'place_coord_other',
                                  'itemCls'    => 'mn-form-item-quiet',
                                  'maxLength'  => 255,
                                  'anchor'     => '100%',
                                  'allowBlank' => true),
                      )
                ),
                $itemUri,
                array('xtype'      => 'fieldset',
                      'anchor'     => '99%',
                      'title'      => $this->_['title_fieldset_public'],
                      'autoHeight' => true,
                      'layout'     => 'column',
                      'cls'        => 'mn-container-clean',
                      'items'      => array(
                          array('layout'     => 'form',
                                'width'      => 185,
                                'labelWidth' => 62,
                                'items'      => array(
                                    array('xtype'      => 'datefield',
                                          'itemCls'    => 'mn-form-item-quiet',
                                          'fieldLabel' => $this->_['label_published_date'],
                                          'format'     => 'd-m-Y',
                                          'name'       => 'published_date',
                                          'checkDirty' => false,
                                          'width'      => 95,
                                          'allowBlank' => true)
                                )
                          ),
                          array('layout'     => 'form',
                                'labelWidth' => 55,
                                'items'      => array(
                                    array('xtype'      => 'textfield',
                                          'itemCls'    => 'mn-form-item-quiet',
                                          'fieldLabel' => $this->_['label_published_time'],
                                          'name'       => 'published_time',
                                          'checkDirty' => false,
                                          'maxLength'  => 8,
                                          'width'      => 70,
                                          'allowBlank' => true)
                                )
                          )
                      )
                ),
                array('xtype'      => 'fieldset',
                      'width'      => 580,
                      'anchor'     => '100%',
                      'labelWidth' => 80,
                      'title'      => $this->_['title_fieldset_like'],
                      'collapsible' => true,
                      'collapsed'   => false,
                      'autoHeight' => true,
                      'items'      => array(
                            array('xtype'      => 'mn-field-chbox',
                                  'fieldLabel' => $this->_['label_rating'],
                                  'default'    => $this->isUpdate ? null : 1,
                                  'name'       => 'likes'),
                            array('xtype'      => 'numberfield',
                                  'fieldLabel' => $this->_['label_like_votes'],
                                  'name'       => 'likes_votes',
                                  'maxLength'  => 4,
                                  'width'      => 80,
                                  'allowBlank' => true),
                      )
                ),
                array('xtype'      => 'fieldset',
                      'width'      => 580,
                      'anchor'     => '100%',
                      'labelWidth' => 80,
                      'title'      => $this->_['title_fieldset_rating'],
                      'collapsible' => true,
                      'collapsed'   => false,
                      'autoHeight' => true,
                      'items'      => array(
                            array('xtype'      => 'mn-field-chbox',
                                  'fieldLabel' => $this->_['label_rating'],
                                  'default'    => $this->isUpdate ? null : 1,
                                  'name'       => 'rating'),
                            array('xtype'      => 'numberfield',
                                  'fieldLabel' => $this->_['label_rating_votes'],
                                  'name'       => 'rating_votes',
                                  'maxLength'  => 4,
                                  'width'      => 80,
                                  'allowBlank' => true),
                            array('xtype'      => 'numberfield',
                                  'fieldLabel' => $this->_['label_rating_value'],
                                  'name'       => 'rating_value',
                                  'maxLength'  => 4,
                                  'width'      => 80,
                                  'allowBlank' => true),
                      )
                ),
                array('layout'     => 'form',
                      'labelWidth' => 160,
                      'cls'        => 'mn-container-clean',
                      'items'      => array(
                            array('xtype'      => 'mn-field-chbox',
                                  'fieldLabel' => $this->_['label_extended'],
                                  'default'    => $this->isUpdate ? null : 1,
                                  'name'       => 'place_extended'),
                            array('xtype'      => 'mn-field-chbox',
                                  'fieldLabel' => $this->_['label_map'],
                                  'default'    => $this->isUpdate ? null : 1,
                                  'name'       => 'place_map'),
                            array('xtype'      => 'mn-field-chbox',
                                  'fieldLabel' => $this->_['label_published'],
                                  'labelTip'   => $this->_['tip_published'],
                                  'default'    => $this->isUpdate ? null : 1,
                                  'name'       => 'published'),
                      )
                )
            )
        );

        // владка "SEO статьи"
        $tabSeo = array(
            'title'       => $this->_['title_tab_seo'],
            'iconSrc'     => $this->resourcePath . 'icon-tab-seo.png',
            'layout'      => 'form',
            'labelWidth'  => 85,
            'autoScroll'  => true,
            'baseCls'     => 'mn-form-tab-body',
            'defaultType' => 'textfield',
            'items'       => array(
                array('xtype'      => 'fieldset',
                      'anchor'     => '99%',
                      'labelWidth' => 120,
                      'collapsible' => true,
                      'collapsed'   => false,
                      'title'      => $this->_['title_fieldset_r'],
                      'autoHeight' => true,
                      'items'      => array(
                          array('xtype'         => 'combo',
                                'fieldLabel'    => $this->_['label_page_meta_r'],
                                'labelTip'      => $this->_['tip_page_meta_r'],
                                'id'            => 'page_meta_robots',
                                'name'          => 'page_meta_robots',
                                //'value'         => $metaRobots,
                                'editable'      => true,
                                'maxLength'     => 100,
                                'width'         => 180,
                                'typeAhead'     => false,
                                'triggerAction' => 'all',
                                'mode'          => 'local',
                                'store'         => array(
                                    'xtype'  => 'arraystore',
                                    'fields' => array('list'),
                                    'data'   => $this->_['data_robots']
                                ),
                                'hiddenName'    => 'page_meta_robots',
                                'valueField'    => 'list',
                                'displayField'  => 'list',
                                'allowBlank'    => true),
                          array('xtype'         => 'combo',
                                'itemCls'       => 'mn-form-item-quiet',
                                'fieldLabel'    => $this->_['label_page_meta_v'],
                                'labelTip'      => $this->_['tip_page_meta_v'],
                                'id'            => 'page_meta_revisit',
                                'name'          => 'page_meta_revisit',
                                'editable'      => true,
                                'maxLength'     => 10,
                                'width'         => 180,
                                'typeAhead'     => false,
                                'triggerAction' => 'all',
                                'mode'          => 'local',
                                'store'         => array(
                                    'xtype'  => 'arraystore',
                                    'fields' => array('list', 'value'),
                                    'data'   => $this->_['data_revisit']
                                ),
                                'hiddenName'    => 'page_meta_revisit',
                                'valueField'    => 'value',
                                'displayField'  => 'list',
                                'allowBlank'    => true),
                          array('xtype'         => 'combo',
                                'itemCls'       => 'mn-form-item-quiet',
                                'fieldLabel'    => $this->_['label_page_meta_m'],
                                'labelTip'      => $this->_['tip_page_meta_m'],
                                'id'            => 'page_meta_document',
                                'name'          => 'page_meta_document',
                                'editable'      => true,
                                'maxLength'     => 100,
                                'width'         => 180,
                                'typeAhead'     => false,
                                'triggerAction' => 'all',
                                'mode'          => 'local',
                                'store'         => array(
                                    'xtype'  => 'arraystore',
                                    'fields' => array('list'),
                                    'data'   => $this->_['data_doc']
                                ),
                                'hiddenName'    => 'page_meta_document',
                                'valueField'    => 'list',
                                'displayField'  => 'list',
                                'allowBlank'    => true),
                      )
                ),
                array('xtype'      => 'fieldset',
                      'width'      => 580,
                      'anchor'     => '100%',
                      'title'      => $this->_['title_fieldset_a'],
                      'collapsible' => true,
                      'collapsed'   => false,
                      'autoHeight' => true,
                      'items'      => array(
                          array('xtype'      => 'textfield',
                                'itemCls'       => 'mn-form-item-quiet',
                                'fieldLabel' => $this->_['label_page_meta_a'],
                                'labelTip'   => $this->_['tip_page_meta_a'],
                                'id'         => 'page_meta_author',
                                'name'       => 'page_meta_author',
                                //'value'      => $metaAuthor,
                                'emptyText'  => $_SESSION['profile/name'],
                                'maxLength'  => 100,
                                'width'      => 252,
                                'allowBlank' => true),
                          array('xtype'      => 'textfield',
                                'itemCls'       => 'mn-form-item-quiet',
                                'fieldLabel' => $this->_['label_page_meta_c'],
                                'labelTip'   => $this->_['tip_page_meta_c'],
                                'id'         => 'page_meta_copyright',
                                'name'       => 'page_meta_copyright',
                                'maxLength'  => 255,
                                'width'      => 252,
                                'allowBlank' => true),
                      )
                ),
                array('xtype'      => 'fieldset',
                      'anchor'     => '100%',
                      'labelWidth' => 120,
                      'title'      => $this->_['title_fieldset_meta'],
                      'autoHeight' => true,
                      'items'      => array(
                            array('xtype'      => 'textarea',
                                  'itemCls'    => 'mn-form-item-quiet',
                                  'fieldLabel' => $this->_['label_page_meta_k'],
                                  'labelTip'   => $this->_['tip_page_meta_k'],
                                  'id'         => 'page_meta_keywords',
                                  'name'       => 'page_meta_keywords',
                                  'anchor'     => '100%',
                                  'height'     => 150,
                                  'allowBlank' => true),
                            array('xtype'      => 'textarea',
                                  'itemCls'    => 'mn-form-item-quiet',
                                  'fieldLabel' => $this->_['label_page_meta_d'],
                                  'labelTip'   => $this->_['tip_page_meta_d'],
                                  'id'         => 'page_meta_description',
                                  'name'       => 'page_meta_description',
                                  'anchor'     => '100%',
                                  'height'     => 150,
                                  'allowBlank' => true)
                      )
                )
            )
        );

        // владка "Описание заведения"
        $tabText = array(
            'title'        => $this->_['title_tab_text'],
            'iconSrc'      => $this->resourcePath . 'icon-tab-text.png',
            'layout'       => 'anchor',
            'style'        => 'padding:1px',
            'bodyCssClass' => 'mn-tinymce',
            'items'        => array($this->getEditor())
        );

        // владка "Расположение на карте"
        $tabMap = array(
            'title'        => $this->_['title_tab_map'],
            'iconSrc'      => $this->resourcePath . 'icon-tab-map.png',
            'layout'       => 'fit',
            'bodyCssClass' => 'mn-bg-progress',
            'items'        => array(array('xtype' => 'mn-iframe', 'url' => $this->resourcePath . '../Map.php' . $data['coord']))
        );
        //print_r($this);

        // вкладки окна
        $tabs = array(
            array('xtype' => 'hidden',
                  'name'  => 'place_folder',
                  'value' => $data['folder']),
            array('xtype'             => 'tabpanel',
                  'layoutOnTabChange' => true,
                  'activeTab'         => 0,
                  'style'             => 'padding:3px',
                  'anchor'            => '100%',
                  'height'            => 575,
                  'items'             => array($tabAttr, $tabText, $tabMap, $tabSeo))
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($tabs);
        $form->url = $this->componentUrl . 'profile/';

        parent::getInterface();
    }
}
?>