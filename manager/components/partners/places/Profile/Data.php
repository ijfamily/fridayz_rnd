<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные для интерфейса профиля заведения"
 * Пакет контроллеров "Профиль заведения"
 * Группа пакетов     "Заведения"
 * Модуль             "Партнёры"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_PPlaces_Profile
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные для интерфейса профиля заведения
 * 
 * @category   Gear
 * @package    GController_PPlaces_Profile
 * @subpackage Data
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_PPlaces_Profile_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($_tableName)
     *
     * @var array
     */
    public $fields = array(
        'category_id', 'place_uri', 'place_name', 'place_folder', 'place_text', 'place_image', 'place_logo', 'published',
        'place_background', 'place_address', 'place_phone', 'published_date', 'published_time', 'published_user', 'place_kitchen', 'place_delivery', 'place_delivery_note',
        'place_schedule', 'place_coord_ln', 'place_coord_lt', 'place_extended', 'place_map', 'page_meta_keywords', 'page_meta_description', 
        'page_meta_robots', 'page_meta_author', 'page_meta_copyright', 'page_meta_revisit', 'page_meta_document',
        'rating', 'rating_value', 'rating_votes', 'place_site', 'place_attributes', 'place_coord_other', 'slider', 'slider_index', 'slider_image', 'slider_logo',
        'likes', 'likes_votes', 'place_head'
    );

    /**
     * Первичный ключ таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'place_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'place_names';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_pplaces_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_profile_update'], $record['place_name']);
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        // состояние формы "вставка"
        if ($this->isInsert) {
            $path = DOCUMENT_ROOT .  $this->config->getFromCms('Site', 'DIR/IMAGES') . $params['place_folder'] . '/';
            if (!file_exists($path)) {
                if (@mkdir($path, 0700) === false)
                    throw new GException('Error', sprintf($this->_['msg_cant_mk_dir'], $path));
            }
        }
        // категория альбома
        if (isset($params['category_id']))
            if ($params['category_id'] == -1)
                $params['category_id'] = null;
        // дата публикации альбома
        $date = $this->input->getDate('published_date', 'Y-m-d', false);
        if (!$date)
            $params['published_date'] = date('Y-m-d');
        else
            $params['published_date'] = $date;
        // время публикации альбома
        $time = $this->input->get('published_time', false);
        if (!$time)
            $params['published_time'] = date('H:i:s');
        // кто опубликовал
        $params['published_user'] = $this->session->get('user_id');
        // атрибуты заведения
        $attr = $this->input->get('attributes');
        $params['place_attributes'] = json_encode($attr);
    }

    /**
     * Событие наступает после успешного удаления данных
     * 
     * @return void
     */
    protected function dataDeleteComplete()
    {
        // обновление страницы сайта для компонента "Конструктор страниц"
        $this->response->add('setTo', array(array('id' => 'site', 'func' => 'reload')));
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        Gear::library('File');

        $query = new GDb_Query();
        $sql = 'SELECT * FROM `place_names` WHERE `place_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $names = $ids = array();
        while (!$query->eof()) {
            $item = $query->next();
            if (empty($item['place_folder']))
                $names[] = $item['place_folder'];
            else {
                $path = DOCUMENT_ROOT .  $this->config->getFromCms('Site', 'DIR/IMAGES') . $item['place_folder'] . '/';
                if (file_exists($path))
                    GDir::remove($path);
                $ids[] = $item['place_id'];
            }
        }
        $ids = implode(',', $ids);
        // если есть что удалять
        if ($ids) {
            // удаление изображений альбома
            $sql = 'DELETE FROM `place_images` WHERE `place_id` IN (' . $ids . ')';
            if ($query->execute($sql) === false)
                throw new GSqlException();
        }
        // если были ошибки
        if ($names)
            throw new GException('Error', sprintf($this->_['msg_cant_delete'], implode(',', $names)));
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function recordPreprocessing($record)
    {
        $setTo = array();
        $query = new GDb_Query();
        // категории альбома
        if (!empty($record['category_id'])) {
            $sql = 'SELECT * FROM `site_categories` WHERE `category_id`=' . $record['category_id'];
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            if (!empty($rec))
                $setTo[] = array('xtype' => 'mn-field-combo', 'id' => 'fldCategory', 'text' => $rec['category_name'], 'value' => $record['category_id']);
            unset($record['category_id']);
        }

        // установка полей
        if ($setTo)
            $this->response->add('setTo', $setTo);

        return $record;
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        if (!empty($record['place_attributes'])) {
            $attr = json_decode($record['place_attributes'], true);
            foreach ($attr as $name => $value) {
                $record['attributes[' . $name . ']'] = $value;
            }
        }

        return $record;
    }
}
?>