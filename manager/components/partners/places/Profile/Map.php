<?php
/**
 * Gear Manager
 *
 * Контроллер         "Расположение заведения на карте"
 * Пакет контроллеров "Профиль заведения"
 * Группа пакетов     "Заведения"
 * Модуль             "Партнёры"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_PPlaces_Profile
 * @copyright  Copyright (c) Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Map.php 2016-01-01 12:00:00 Gear Magic $
 */

// долгота
$lng = isset($_GET['longitude']) ? $_GET['longitude'] : '';
// шиорота
$lat = isset($_GET['latitude']) ? $_GET['latitude'] : '';
if (empty($lng) || empty($lat)) {
    $lat = '48.5738';
    $lng = '39.3077';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Расположение заведения на карте</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>

    <style>
        html, body, #ymap { margin: 0; padding: 0; height: 100%; }
        .place-coord { position: absolute; right: 170px; top: 5px; z-index: 1000; background: none repeat scroll 0% 0% rgba(255, 255, 255, 0.8); padding: 10px; border-radius: 5px;  font: 13px arial,sans-serif; }
        .place-coord span { font-weight: bold; }
    </style>
    <script type="text/javascript">
        var myMap, myPlacemark, coords;
        ymaps.ready(init);

        function init(){
                    document.getElementById("lat").innerHTML = <?php echo $lat;?>;
        document.getElementById("lng").innerHTML = <?php echo $lng;?>;

            // Определяем начальные параметры карты
            myMap = new ymaps.Map('ymap', { center: [<?php echo $lat, ',', $lng;?>], zoom: 16, behaviors: ['default', 'scrollZoom'] });

            // Определяем элемент управления поиск по карте	
            var SearchControl = new ymaps.control.SearchControl({ noPlacemark: true });

            //Добавляем элементы управления на карту
            myMap.controls.add(SearchControl).add('zoomControl').add('typeSelector').add('mapTools');
            coords = [<?php echo $lat, ',', $lng;?>];

            // Определяем метку и добавляем ее на карту
            myPlacemark = new ymaps.Placemark([<?php echo $lat, ',', $lng;?>],{}, {preset: "twirl#blueIcon", draggable: true});	
            myMap.geoObjects.add(myPlacemark);

            // Отслеживаем событие перемещения метки
            myPlacemark.events.add("dragend", function (e){
                coords = this.geometry.getCoordinates();
                setCoord();
            }, myPlacemark);

            // Отслеживаем событие щелчка по карте
            myMap.events.add('click', function (e){
                coords = e.get('coordPosition');
                setCoord();
            });

            // Отслеживаем событие выбора результата поиска
            SearchControl.events.add("resultselect", function (e){
                coords = SearchControl.getResultsArray()[0].geometry.getCoordinates();
                setCoord();
            });

            // Ослеживаем событие изменения области просмотра карты - масштаб и центр карты
            myMap.events.add('boundschange', function (event){
                if (event.get('newZoom') != event.get('oldZoom')) {
                    setCoord();
                }
                if (event.get('newCenter') != event.get('oldCenter')) {
                    setCoord();
                }
            });
        }

        // Функция для передачи полученных значений в форму
        function setCoord(){
            var newCoords = [coords[0].toFixed(4), coords[1].toFixed(4)];
            myPlacemark.getOverlay().getData().geometry.setCoordinates(newCoords);

            document.getElementById("lat").innerHTML = newCoords[0];
            document.getElementById("lng").innerHTML = newCoords[1];

            var center = myMap.getCenter();
            var isExistManager = (typeof window.top.Manager != 'undefined');
            if (isExistManager) {
                window.top.Ext.getCmp('placeCoordLt').setValue(newCoords[0]);
                window.top.Ext.getCmp('placeCoordLn').setValue(newCoords[1]);
            }
        }
    </script>
</head>

<body>
    <div id="ymap"></div>
    <div class="place-coord">
        <div><label>Широта: </label><span id="lat"></span></div>
        <div><label>Долгота: </label><span id="lng"></span></div>
    </div>
</body>
</html>