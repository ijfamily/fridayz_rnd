<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля настроеек заведений"
 * Пакет контроллеров "Настройка заведений"
 * Группа пакетов     "Заведения партнёров"
 * Модуль             "Партнёры"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_PPlacesConfig_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля настроеек заведений
 * 
 * @category   Gear
 * @package    GController_PPlacesConfig_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_PPlacesConfig_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // Ext_Window (ExtJS class 'Manager.window.DataProfile')
        $this->_cmp->setProps(
            array('title'           => $this->_['title_profile_update'],
                  'titleEllipsis'   => 40,
                  'width'           => 640,
                  'autoHeight'      => true,
                  'resizable'       => false,
                  'btnDeleteHidden' => true,
                  'stateful'        => false,
                  'buttonsAdd'      => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-partner-places-config',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );
        $this->_cmp->state = 'update';

        $items = array(
            array('xtype' => 'label', 'html' => $this->_['html_desc']),
            array('xtype'      => 'fieldset',
                  'title'      => $this->_['title_fieldset_original'],
                  'width'      => 592,
                  'labelWidth' => 90,
                  'items'      => array(
                      array('xtype'      => 'numberfield',
                            'fieldLabel' => $this->_['label_image_width'],
                            'width'      => 100,
                            'name'       => 'settings[IMAGE/ORIGINAL/WIDTH]',
                            'checkDirty' => false),
                      array('xtype'      => 'numberfield',
                            'fieldLabel' => $this->_['label_image_height'],
                            'width'      => 100,
                            'name'       => 'settings[IMAGE/ORIGINAL/HEIGHT]',
                            'checkDirty' => false),
                      array('xtype' => 'label', 'html' => $this->_['label_image_info']),
                  )
            ),
            array('xtype'       => 'fieldset',
                  'id'          => 'fldOriginalMacro',
                  'labelWidth'  => 120,
                  'title'       => $this->_['label_original_macro'],
                  'width'       => 592,
                  'collapsible' => true,
                  'collapsed'   => true,
                  'autoHeight'  => true,
                  'items'       => array(
                      array('xtype'      => 'textarea',
                            'hideLabel'  => true,
                            'anchor'     => '100%',
                            'height'     => 70,
                            'name'       => 'settings[IMAGE/ORIGINAL/MACRO]',
                            'checkDirty' => false)
                  )
            ),
            array('xtype'      => 'fieldset',
                  'title'      => $this->_['title_fieldset_thumb'],
                  'labelWidth' => 90,
                  'width'      => 592,
                  'items'      => array(
                      array('xtype'      => 'numberfield',
                            'fieldLabel' => $this->_['label_image_width'],
                            'width'      => 100,
                            'name'       => 'settings[IMAGE/THUMB/WIDTH]',
                            'checkDirty' => false),
                      array('xtype'      => 'numberfield',
                            'fieldLabel' => $this->_['label_image_height'],
                            'width'      => 100,
                            'name'       => 'settings[IMAGE/THUMB/HEIGHT]',
                            'checkDirty' => false),
                      array('xtype' => 'label', 'html' => $this->_['label_image_info2'])
                  )
            ),
            array('xtype'       => 'fieldset',
                  'id'          => 'fldThumbMacro',
                  'labelWidth'  => 120,
                  'title'       => $this->_['label_thumb_macro'],
                  'width'       => 592,
                  'collapsible' => true,
                  'collapsed'   => true,
                  'autoHeight'  => true,
                  'items'       => array(
                      array('xtype'      => 'textarea',
                            'hideLabel'  => true,
                            'anchor'     => '100%',
                            'height'     => 70,
                            'name'       => 'settings[IMAGE/THUMB/MACRO]',
                            'checkDirty' => false)
                  )
            ),
            array('xtype'      => 'fieldset',
                  'labelWidth' => 90,
                  'title'      => $this->_['title_fs_watermaker'],
                  'width'      => 592,
                  'items'      => array(
                      array('xtype'      => 'checkbox',
                            'fieldLabel' => $this->_['label_use'],
                            'checkDirty' => false,
                            'name'       => 'settings[WATERMARK]'),
                      array('xtype'         => 'combo',
                            'fieldLabel'    => $this->_['label_position'],
                            'name'          => 'settings[WATERMARK/POSITION]',
                            'checkDirty'    => false,
                            'editable'      => false,
                            'width'         => 180,
                            'typeAhead'     => false,
                            'triggerAction' => 'all',
                            'mode'          => 'local',
                            'store'         => array(
                                'xtype'  => 'arraystore',
                                'fields' => array('display', 'value'),
                                'data'   => $this->_['data_position']
                            ),
                            'hiddenName'    => 'settings[WATERMARK/POSITION]',
                            'valueField'    => 'value',
                            'displayField'  => 'display',
                            'allowBlank'    => true),
                  )
            ),
            array('xtype'      => 'checkbox',
                  'fieldLabel' => $this->_['label_generate'],
                  'checkDirty' => false,
                  'name'       => 'settings[GENERATE/FILE/NAME]'),
            array('xtype'      => 'numberfield',
                  'fieldLabel' => $this->_['label_image_quality'],
                  'width'      => 100,
                  'value'      => 100,
                  'name'       => 'settings[IMAGE/QUALITY]',
                  'checkDirty' => false),
            array('xtype' => 'label', 'html' => $this->_['info_image_quality']),
            array('xtype'      => 'numberfield',
                  'fieldLabel' => $this->_['label_list_limit'],
                  'width'      => 100,
                  'value'      => 100,
                  'name'       => 'settings[LIST/LIMIT]',
                  'checkDirty' => false)
        );

        // форма (ExtJS class 'Manager.form.DataProfile')
        $form = $this->_cmp->items->get(0);
        $form->url = $this->componentUrl . 'profile/';
        $form->labelWidth = 240;
        $form->items->addItems($items);
        $form->autoScroll = true;

        parent::getInterface();
    }
}
?>