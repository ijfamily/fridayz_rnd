<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля настроеек галереи изображений"
 * Пакет контроллеров "Настройка галереи изображений"
 * Группа пакетов     "Галереи изображений"
 * Модуль             "Сайт"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_PPlacesConfig_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

define('_INC', 1);

Gear::controller('Profile/Data');

/**
 * Возращает шаблон настроеек заведений
 * 
 * @params string $tpl данные в шаблоне
 */
function getTplSiteConfig($tpl)
{
    return <<<TEXT
<?php
/**
 * Gear CMS
 *
 * Настройка заведений (создан: {$tpl['date']})
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 *
 * @category   Gear
 * @package    Config
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    \$Id: Partners.php {$tpl['date']} Gear Magic \$
 */

return array(
    // генерировать название файла
    'GENERATE/FILE/NAME' => {$tpl['GENERATE/FILE/NAME']},
    // количество изображений в списке
    'LIST/LIMIT'         => {$tpl['LIST/LIMIT']},

    // Изображения
    // размеры оригинального изображения
    'IMAGE/ORIGINAL/WIDTH'  => {$tpl['IMAGE/ORIGINAL/WIDTH']},
    'IMAGE/ORIGINAL/HEIGHT' => {$tpl['IMAGE/ORIGINAL/HEIGHT']},
    'IMAGE/ORIGINAL/MACRO'  => '{$tpl['IMAGE/ORIGINAL/MACRO']}',
    // размеры миниатюры изображения
    'IMAGE/THUMB/WIDTH'     => {$tpl['IMAGE/THUMB/WIDTH']},
    'IMAGE/THUMB/HEIGHT'    => {$tpl['IMAGE/THUMB/HEIGHT']},
    'IMAGE/THUMB/MACRO'     => '{$tpl['IMAGE/THUMB/MACRO']}',
    // качество изображения
    'IMAGE/QUALITY'         => {$tpl['IMAGE/QUALITY']},

    // Водяной знак
    // использовать
    'WATERMARK'          => {$tpl['WATERMARK']},
    // положение штампа
    'WATERMARK/POSITION' => '{$tpl['WATERMARK/POSITION']}'
);
?>
TEXT;
}

/**
 * Данные профиля настройки заведения
 * 
 * @category   Gear
 * @package    GController_PPlacesConfig_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_PPlacesConfig_Profile_Data extends GController_Profile_Data
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_pplacesconfig_profile';

    /**
     * Файл настроек роботов
     *
     * @var string
     */
    public $filename = 'Partners.php';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return $this->_['profile_title_update'];
    }

    /**
     * Обновление настроеек сайта
     * 
     * @return void
     */
    protected function fileUpdate($params)
    {
        // запись для CMS
        if (file_put_contents('../' . PATH_CONFIG_CMS . $this->filename, getTplSiteConfig($params), FILE_TEXT) === false)
            throw new GException('Error', sprintf($this->_['msg_create_file'], $this->filename));
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        $params['date'] = date('Y-m-d H:i:s');
        if ($params['IMAGE/ORIGINAL/MACRO'])
            $params['IMAGE/ORIGINAL/MACRO'] = addslashes($params['IMAGE/ORIGINAL/MACRO']);
        if ($params['IMAGE/THUMB/MACRO'])
            $params['IMAGE/THUMB/MACRO'] = addslashes($params['IMAGE/THUMB/MACRO']);
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        parent::dataAccessUpdate();

        if ($this->input->isEmpty('PUT'))
            throw new GException('Warning', 'To execute a query, you must change data!');
        $params = $this->input->get('settings', array());
        // проверки входных данных
        $this->isDataCorrect($params);
        // обновить файл конфигурации
        $this->fileUpdate($params);
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($params, 'method "PUT"');
        }
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        parent::dataAccessView();

        $setTo = array();

        $filename = '../' . PATH_CONFIG_CMS . $this->filename;
        // если файл настроек сайта не существует
        if (!file_exists($filename))
            throw new GException('Error', sprintf($this->_['msg_file_exist'], $filename));

        // настройки форм
        $arrSettings = include($filename);
        $settings = array();
        foreach ($arrSettings as $key => $value) {
            if ($key == 'IMAGE/THUMB/MACRO' || $key == 'IMAGE/ORIGINAL/MACRO') {
                $value = stripslashes($value);
            }
            if (is_array($value)) {
                foreach ($value as $subkey => $subvalue) {
                    $settings['settings[' . $key . '/' . $subkey . ']'] = $subvalue;
                }
            } else
                $settings['settings[' . $key . ']'] = $value;
        }

        // если есть макрос
        if (!empty($arrSettings['IMAGE/ORIGINAL/MACRO']))
            $setTo[] = array('id' => 'fldOriginalMacro', 'func' => 'expand', 'args' => array(true));
        // если есть макрос
        if (!empty($arrSettings['IMAGE/THUMB/MACRO']))
            $setTo[] = array('id' => 'fldThumbMacro', 'func' => 'expand', 'args' => array(true));

        $this->response->data = $settings;
        // установка полей
        $this->response->add('setTo', $setTo);
    }
}
?>