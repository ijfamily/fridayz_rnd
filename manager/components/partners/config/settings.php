<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Настройка заведений"
 * Группа пакетов     "Заведения партнёров"
 * Модуль             "Партнёры"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_PPlacesConfig
 * @copyright  Copyright (c) 2013-2016 <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Настройка заведений
 * Описание: Настройка заведений
 * Меню: Настройка заведений
 * ID класса: gcontroller_pplacesconfig_profile
 * Модуль: Партнёры
 * Группа: Партнёры
 * Очищать: нет
 * Статистика компонента: нет 
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске компонентов: нет
 *    В главном меню: нет
 * Ресурс
 *    Компонент: site/partners/config/
 *    Контроллер: site/partners/config/Profile/
 *    Интерфейс: profile/interface/
 *    Меню:
 */

return array(
    // {P}- модуль "Partners" {Places} - пакет контроллеров "Partner places config" -> PPlacesConfig
    'clsPrefix' => 'PPlacesConfig',
    // использовать язык
    'language'  => 'ru-RU'
);
?>