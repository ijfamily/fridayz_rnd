<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Входящие письма"
 * Группа пакетов     "Почта"
 * Модуль             "Адмнистрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AMailsReceived
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Входящие письма
 * Описание: Входящие письма пользователям системы
 * ID класса: gcontroller_amailsreceived_grid
 * Группа: Администрирование / Почта
 * Очищать: нет
 * Ресурс
 *    Пакет: administration/mails/received/
 *    Контроллер: administration/mails/received/Grid/
 *    Интерфейс: grid/interface/
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске: нет
 */

return array(
    // {A}- модуль "Administration" {Mails received} - пакет контроллеров "Mails received" -> AMailsReceived
    'clsPrefix' => 'AMailsReceived'
);
?>