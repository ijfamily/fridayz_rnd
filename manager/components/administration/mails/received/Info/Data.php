<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля полученного письма"
 * Пакет контроллеров "Отправленные письма"
 * Группа пакетов     "Почта"
 * Модуль             "Адмнистрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AMailsReceived_Info
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные профиля полученного письма
 * 
 * @category   Gear
 * @package    GController_AMailsReceived_Info
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AMailsReceived_Info_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы
     *
     * @var array
     */
    public $fields = array('profile_name', 'mail_theme', 'mail_text');

    /**
     * Первичный ключ таблицы
     *
     * @var string
     */
    public $idProperty = 'mail_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_user_mails';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_amailsreceived_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        $date = date('d-m-Y H:i:s', strtotime($record['mail_date']));

        return sprintf($this->_['profile_title_info'], $date);
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        $sql = 'SELECT * '
             . 'FROM `gear_user_mails` `m` '
             . 'LEFT JOIN `gear_user_profiles` `p` ON `p`.`user_id`=`m`.`receiver_id` '
             . 'WHERE `m`.`mail_id`=' . (int)$this->uri->id;

        parent::dataView($sql);

        $table = new GDb_Table('gear_user_mails', 'mail_id', array('mail_read'));
        $table->update(array('mail_read' => 1), $this->uri->id);
    }
}
?>