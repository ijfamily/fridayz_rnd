<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные интерфейса списка полученых писем"
 * Пакет контроллеров "Отправленные письма"
 * Группа пакетов     "Почта"
 * Модуль             "Адмнистрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AMailsReceived_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные интерфейса списка полученых писем
 * 
 * @category   Gear
 * @package    GController_AMailsReceived_Grid
 * @subpackage Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AMailsReceived_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'mail_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_user_mails';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 'SELECT SQL_CALC_FOUND_ROWS `p`.*, `m`.`mail_id`, `m`.`mail_date`, `m`.`mail_read`, `m`.`mail_theme`, '
                    . 'SUBSTRING(`m`.`mail_text`, 1, 100) AS `_mail_text` '
                    . 'FROM `gear_user_mails` AS `m` '
                    . 'LEFT JOIN `gear_user_profiles` AS `p` ON `p`.`user_id`=`m`.`sender_id` '
                    . 'WHERE `m`.`receiver_id`=' . $this->session->get('user_id'). ' AND '
                    . '`m`.`mail_status`=1 %filter'
                    . 'ORDER BY %sort '
                    . 'LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            'mail_theme' => array('type' => 'string'),
            'mail_date' => array('type' => 'string'),
            '_mail_text' => array('type' => 'string'),
            'profile_name' => array('type' => 'string'),
            'mail_read' => array('type' => 'string')
        );
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        if ($record['mail_read'] == 1)
            $record['mail_read'] = '<img src="' . $this->resourcePath . 'icon_mail_open.png">';
        else
            $record['mail_read'] = '<img src="' . $this->resourcePath . 'icon_mail.png">';
        $record['_mail_text'] = strip_tags(trim($record['_mail_text']));
        if (mb_strlen($record['_mail_text']) > 100)
            $record['_mail_text'] = $record['_mail_text'] . ' ...';
        if (mb_strlen($record['_mail_text']) == 0)
            $record['_mail_text'] = $record['_mail_text'] . ' ...';
        $record['mail_date'] = date('d-m-Y H:i:s', strtotime($record['mail_date']));

        return $record;
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        // удаление полученных писем ("gear_user_mails")
        $query = new GDb_Query();
        $sql = 'DELETE FROM `gear_user_mails` WHERE `receiver_id`=' . $this->session->get('user_id');
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_user_mails` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }
}
?>