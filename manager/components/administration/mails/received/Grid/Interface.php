<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка полученных писем"
 * Пакет контроллеров "Отправленные письма"
 * Группа пакетов     "Почта"
 * Модуль             "Адмнистрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AMailsPosted_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс списка полученных писем
 * 
 * @category   Gear
 * @package    GController_AMailsPosted_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AMailsReceived_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'mail_id';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'mail_date';

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('name' => 'mail_theme', 'type' => 'string'),
            array('name' => 'mail_date', 'type' => 'string'),
            array('name' => '_mail_text', 'type' => 'string'),
            array('name' => 'profile_name', 'type' => 'string'),
            array('name' => 'mail_read', 'type' => 'string')
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $this->columns = array(
            array('xtype'     => 'numbercolumn'),
            array('xtype'     => 'rowmenu'),
            array('dataIndex' => 'mail_read',
                  'header'    => '&nbsp;',
                  'resizable' => false,
                  'hideable' => true,
                  'menuDisabled' => true,
                  'width'     => 30,
                  'sortable'  => false),
            array('dataIndex' => 'mail_date',
                  'header'    => '<em class="mn-grid-hd-details"></em>'
                               . $this->_['header_mail_date'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'profile_name',
                  'header'    => $this->_['header_posted'],
                  'width'     => 200,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'mail_theme',
                  'header'    => $this->_['header_mail_theme'],
                  'width'     => 180,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => '_mail_text',
                  'header'    => $this->_['header_mail_text'],
                  'width'     => 220,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string'))
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_grid'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_grid'],
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'profileUrl'    => $this->componentUrl . 'profile/')
        );

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "правка" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' => $this->classId));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array(
                'xtype'      => 'mn-btn-clear-data',
                'msgConfirm' => $this->_['msg_btn_clear'],
                'gridId'     => $this->classId,
                'isN'        => true
            )
        );
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = 'component-send-message';
        $this->_cmp->tbar->items->add($group);

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $this->_cmp->cellTips = array(
            array('field' => 'mail_date',
                  'tpl'   => '<div class="mn-grid-cell-tooltip-tl">{mail_date}</div>'
                           . $this->_['header_posted'] . ': <b>{profile_name}</b><br>'
                           . $this->_['header_mail_theme'] . ': <b>{mail_theme}</b><br>'),
            array('field' => 'profile_name', 'tpl' => '{profile_name}'),
            array('field' => 'mail_theme', 'tpl' => '{mail_theme}'),
            array('field' => '_mail_text', 'tpl' => '{_mail_text}')
        );

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $this->_cmp->rowMenu = array(
            'xtype' => 'mn-grid-rowmenu',
            'items' => array(
                array('text'    => $this->_['rowmenu_info'],
                      'iconCls' => 'icon-form-edit',
                      'url'     => $this->componentUrl . 'info/interface/')
            )
        );

        parent::getInterface();
    }
}
?>