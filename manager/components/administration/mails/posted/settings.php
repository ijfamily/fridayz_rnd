<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Отправленные письма"
 * Группа пакетов     "Почта"
 * Модуль             "Адмнистрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AMailsPosted
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Отправленные письма
 * Описание: Отправленные письма пользователям системы
 * ID класса: gcontroller_amailsposted_grid
 * Группа: Администрирование / Почта
 * Очищать: нет
 * Ресурс
 *    Пакет: administration/mails/posted/
 *    Контроллер: administration/mails/posted/Grid/
 *    Интерфейс: grid/interface/
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске: нет
 */

return array(
    // {A}- модуль "Administration" {Mails posted} - пакет контроллеров "MailsPosted" -> AMailsPosted
    'clsPrefix' => 'AMailsPosted'
);
?>