<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля отправленного письма"
 * Пакет контроллеров "Отправленные письма"
 * Группа пакетов     "Почта"
 * Модуль             "Адмнистрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AMailsPosted_Info
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля отправленного письма
 * 
 * @category   Gear
 * @package    GController_AMailsPosted_Info
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AMailsPosted_Info_Interface extends GController_Profile_Interface
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_amailsposted_grid';

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('titleEllipsis' => 50,
                  'gridId'        => 'gcontroller_amailsposted_grid',
                  'width'         => 450,
                  'autoHeight'    => true,
                  'resizable'     => false,
                  'state'         => 'info',
                  'stateful'      => false)
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
           array('xtype'      => 'textfield',
                 'fieldLabel' =>$this->_['label_receiver'],
                 'name'       => 'profile_name',
                 'maxLength'  => 50,
                 'anchor'     => '100%',
                 'readOnly'   => true,
                 'allowBlank' => true),
           array('xtype'      => 'textfield',
                 'fieldLabel' =>$this->_['label_mail_theme'],
                 'name'       => 'mail_theme',
                 'maxLength'  => 50,
                 'anchor'     => '100%',
                 'readOnly'   => true,
                 'allowBlank' => true),
           array('xtype'      => 'htmleditor',
                 'hideLabel'  => true,
                 'name'       => 'mail_text',
                 'width'      => 425,
                 'height'     => 208,
                 'readOnly'   => true,
                 'allowBlank' => true)
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->addItems($items);
        $form->url = $this->componentUrl . 'info/';

        parent::getInterface();
    }
}
?>