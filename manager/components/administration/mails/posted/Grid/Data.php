<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные интерфейса списка отправленных писем"
 * Пакет контроллеров "Отправленные письма"
 * Группа пакетов     "Почта"
 * Модуль             "Адмнистрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AMailsPosted_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные интерфейса списка отправленных писем
 * 
 * @category   Gear
 * @package    GController_AMailsPosted_Grid
 * @subpackage Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AMailsPosted_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'mail_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_user_mails';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 'SELECT SQL_CALC_FOUND_ROWS `p`.*, `m`.`mail_id`, `m`.`mail_date`, `m`.`mail_theme`, '
                    . 'SUBSTRING(`m`.`mail_text`, 1, 100) AS `_mail_text` '
                    . 'FROM `gear_user_mails` AS `m` '
                    . 'LEFT JOIN `gear_user_profiles` AS `p` ON `p`.`user_id`=`m`.`receiver_id` '
                    . 'WHERE `m`.`sender_id`=' . $this->session->get('user_id'). ' AND '
                    . '`m`.`mail_status`=1 %filter'
                    . 'ORDER BY %sort '
                    . 'LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            'mail_theme' => array('type' => 'string'),
            'mail_date' => array('type' => 'string'),
            '_mail_text' => array('type' => 'string'),
            'profile_name' => array('type' => 'string')
        );
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        // удаление отправленных писем ("gear_user_mails")
        $query = new GDb_Query();
        $sql = 'DELETE FROM `gear_user_mails` WHERE `sender_id`=' . $this->session->get('user_id');
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_user_mails` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }
}
?>