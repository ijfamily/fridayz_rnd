<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Sent a letter',
    'tooltip_grid' => 'отправленные письма пользователями системы',
    'rowmenu_info' => 'Detail',
    // панель управления
    'title_buttongroup_edit'   => 'Edit',
    'title_buttongroup_cols'   => 'Columns',
    'title_buttongroup_filter' => 'Filter',
    'msg_btn_delete'           => 'Вы действительно желаете удалить записи <span class="mn-msg-delete">'
                                . '"Отправленные письма"</span> ?',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">'
                                . '"Отправленные письма"</span> ?',
    // столбцы
    'header_mail_date'  => 'Date',
    'header_mail_theme' => 'Theme',
    'header_mail_text'  => 'Text',
    'header_receiver'   => 'Receiver'
);
?>