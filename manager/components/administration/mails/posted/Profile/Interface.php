<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля отправленного письма"
 * Пакет контроллеров "Отправленные письма"
 * Группа пакетов     "Почта"
 * Модуль             "Адмнистрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AMailsPosted_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля отправленного письма
 * 
 * @category   Gear
 * @package    GController_AMailsPosted_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AMailsPosted_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_amailsposted_grid';

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'titleEllipsis' => 45,
                  'gridId'        => 'gcontroller_amailsposted_grid',
                  'width'         => 450,
                  'autoHeight'    => true,
                  'resizable'     => false,
                  'stateful'      => false,
                  'buttonsAdd'    => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-mail-posted',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
           array('xtype'      => 'mn-field-combo',
                 'fieldLabel' =>$this->_['label_receiver'],
                 'name'       => 'receiver_id',
                 'editable'   => true,
                 'width'      => 250,
                 'hiddenName' => 'receiver_id',
                 'allowBlank' => false,
                 'store'      => array(
                     'xtype' => 'jsonstore',
                     'url'   => $this->componentUrl . 'combo/trigger/?name=users'
                 )
           ),
           array('xtype'      => 'textfield',
                 'fieldLabel' =>$this->_['label_mail_theme'],
                 'name'       => 'mail_theme',
                 'maxLength'  => 50,
                 'anchor'     => '100%',
                 'allowBlank' => false),
           array('xtype'      => 'htmleditor',
                 'hideLabel'  => true,
                 'name'       => 'mail_text',
                 'width'      => 425,
                 'height'     => 208, 
                 'allowBlank' => false));

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->addItems($items);
        $form->url = $this->componentUrl . 'profile/';

        parent::getInterface();
    }
}
?>