<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля отправленного письма"
 * Пакет контроллеров "Отправленные письма"
 * Группа пакетов     "Почта"
 * Модуль             "Адмнистрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AMailsPosted_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные профиля отправленного письма
 * 
 * @category   Gear
 * @package    GController_AMailsPosted_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AMailsPosted_Profile_Data extends GController_Profile_Data
{
    /**
     * Письмо отправлено
     */
    const MAIL_POSTED   = 1;

    /**
     * Письмо получено
     */
    const MAIL_RECEIVED = 2;

    /**
     * Массив полей таблицы
     *
     * @var array
     */
    public $fields = array('sender_id', 'receiver_id', 'mail_theme', 'mail_text', 'mail_status', 'mail_date');

    /**
     * Первичный ключ таблицы
     *
     * @var string
     */
    public $idProperty = 'mail_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_user_mails';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_amailsposted_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        $date = date('d-m-Y H:i:s', strtotime($record['mail_date']));

        return sprintf($this->_['title_profile_update'], $date);
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        $params['mail_date'] = date('Y-m-d H:i:s');
        $params['sender_id'] = $this->session->get('user_id');
        $params['mail_status'] = self::MAIL_POSTED;
    }
}
?>