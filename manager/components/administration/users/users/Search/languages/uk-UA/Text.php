<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Пошук у списку "Користувачі системи"',
    // поля
    'header_profile_name' => 'Контингент',
    'header_profile_nick' => 'Нік',
    'header_user_name'    => 'Логін',
    'header_user_enabled' => 'Активный',
    'header_user_visited' => 'Аторизація',
    'header_user_escaped' => 'Вихід',
    'header_group_name'   => 'Група',
    'header_user_status'  => 'Статус'
);
?>