<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск пользователей системы"
 * Пакет контроллеров "Пользователи системы"
 * Группа пакетов     "Пользователи системы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AUsers_Search
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск пользователей системы
 * 
 * @category   Gear
 * @package    GController_AUsers_Search
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AUsers_Search_Data extends GController_Search_Data
{
    /**
     * дентификатор компонента (списка) к которому применяется поиск
     *
     * @var string
     */
    public $gridId = 'gcontroller_ausers_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ausers_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор,
     * используется для инициализации атрибутов контроллера без конструктора
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'profile_name', 'label' => $this->_['header_profile_name']),
            array('dataIndex' => 'user_name', 'label' => $this->_['header_user_name']),
            array('dataIndex' => 'user_enabled', 'label' => $this->_['header_user_enabled']),
            array('dataIndex' => 'user_visited', 'label' => $this->_['header_user_visited']),
            array('dataIndex' => 'user_escaped', 'label' => $this->_['header_user_escaped']),
            array('dataIndex' => 'group_shortname', 'label' => $this->_['header_group_name']),
            array('dataIndex' => 'user_status', 'label' => $this->_['header_user_status'])
        );
    }
}
?>