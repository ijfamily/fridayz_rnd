<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Пользователи системы"
 * Группа пакетов     "Пользователи системы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AUsers
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Пользователи системы
 * Описание: Пользователи системы
 * ID класса: gcontroller_ausers_grid
 * Группа: Администрирование / Пользователи
 * Очищать: нет
 * Ресурс
 *    Пакет: administration/users/users/
 *    Контроллер: administration/users/users/Grid/
 *    Интерфейс: grid/interface/
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске: нет
 */

return array(
    // {A}- модуль "Administration" {Users} - пакет контроллеров "Users" -> AUsers
    'clsPrefix' => 'AUsers'
);
?>