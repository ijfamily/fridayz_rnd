<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные учётной записи пользователя"
 * Пакет контроллеров "Учётная запись пользователя"
 * Группа пакетов     "Пользователи системы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AUsers_Account
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные учётной записи пользователя
 * 
 * @category   Gear
 * @package    GController_AUsers_Account
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AUsers_Account_Data extends GController_Profile_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Массив полей таблицы
     *
     * @var array
     */
    public $fields = array(
        'group_id', 'user_enabled', 'user_name', 'user_password', 'user_password_salt', 'user_visited', 'user_visited_trial', 'user_escaped',
        'user_process', 'profile_id'
    );

    /**
     * Первичный ключ таблицы
     *
     * @var string
     */
    public $idProperty = 'user_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_users';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ausers_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_account_update'], $record['user_name']);
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        parent::dataView($sql);

        // переопределить идент. группы
        unset($this->response->data['group_id']);
        // скрыть пароль
        $this->response->data['user_password'] = '';
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        // массив полей с их соответствующими значениями
        $params = $this->input->getBy($this->fields);
        // шифрование пароля
        $password = Gear::getPassword($this->input->get('user_password'), true);
        $params['user_password_salt'] = $password['salt'];
        $params['user_password'] = $password['password'];
        // сброс параметров аккаунта
        $params['user_visited'] = '';
        $params['user_visited_trial'] = 0;
        $params['user_escaped'] = '';
        $params['user_process'] = '';

        parent::dataUpdate($params);
    }

    /**
     * Вставка данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataInsert($params = array())
    {
        // массив полей с их соответствующими значениями
        $params = $this->input->getBy($this->fields);
        // шифрование пароля
        $password = Gear::getPassword($this->input->get('user_password'), true);
        $params['user_password_salt'] = $password['salt'];
        $params['user_password'] = $password['password'];

        parent::dataInsert($params);

        $table = new GDb_Table('gear_user_profiles', 'profile_id');
        $table->update(array('user_id' => $this->_recordId), $this->input->getInt('profile_id'));
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        $query = new GDb_Query();
        $sql = 'SELECT * FROM `gear_users` WHERE `sys_record`<>1 AND `gear_users`.`user_id`=' . $this->uri->id;
        $user = $query->getRecord($sql);
        if ($user === false)
            throw new GSqlException();
        if (empty($user))
            throw new GException('Error', 'Unable to delete records!');

        // удаление журнала выхода пользователей ("gear_user_escapes")
        $sql = 'DELETE `gear_user_escapes` '
             . 'FROM `gear_user_escapes`,`gear_users` '
             . 'WHERE `gear_user_escapes`.`user_id`=`gear_users`.`user_id` AND '
             . '`gear_users`.`sys_record`<>1 AND '
             . '`gear_users`.`user_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_user_escapes` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление журнала авторизации пользователей ("gear_user_attends")
        $sql = 'DELETE `gear_user_attends` '
             . 'FROM `gear_user_attends`,`gear_users` '
             . 'WHERE `gear_user_attends`.`user_id`=`gear_users`.`user_id` AND '
             . '`gear_users`.`sys_record`<>1 AND '
             . '`gear_users`.`user_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_user_attends` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление журнала действий пользователей ("gear_log")
        $sql = 'DELETE `gear_log` '
             . 'FROM `gear_log`,`gear_users` '
             . 'WHERE `gear_log`.`user_id`=`gear_users`.`user_id` AND '
             . '`gear_users`.`sys_record`<>1 AND '
             . '`gear_users`.`user_id`=' . $this->uri->id ;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_log` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // обновление профилей пользователей ("gear_user_profiles")
        $sql = 'UPDATE `gear_user_profiles` `p`, `gear_users` `u` '
             . 'SET `p`.`user_id`=NULL '
             . 'WHERE `p`.`user_id`=`u`.`user_id` AND `u`.`sys_record`<>1 AND '
             . '`u`.`user_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        if ($this->isInsert) {
            if (empty($params['profile_id']))
                throw new GException('Warning', 'Incorrectly entered or selected from the fields: %s#' 
                                    . $this->_['msg_profile_name']);
            else
                if (!is_numeric($params['profile_id']))
                    throw new GException('Warning', 'Incorrectly entered or selected from the fields: %s#'
                                        . $this->_['msg_profile_name']);
        }
        unset($params['profile_id']);
    }

    /**
     * Проверка существования записи с одинаковыми значениями
     * 
     * @param  array $params array (field => value, ....)
     * @return void
     */
    protected function isDataExist($params)
    {
        $table = new GDb_Table($this->tableName, $this->idProperty);
        if ($table->isDataExist(array('user_name' => $params['user_name']), (int)$this->uri->id) === true)
            throw new GException('Warning', sprintf($this->_['msg_wrong_username'], $params['user_name']));
    }
}
?>