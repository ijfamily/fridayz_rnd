<?php
/**
 * Gear Manager
 *
 * Контроллер         "Изменение полей учётной записи пользователя"
 * Пакет контроллеров "Учётная запись пользователя"
 * Группа пакетов     "Пользователи системы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AUsers_Account
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Field.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Field');

/**
 * Изменение полей учётной записи пользователя
 * 
 * @category   Gear
 * @package    GController_AUsers_Account
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Field.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AUsers_Account_Field extends GController_Profile_Field
{
    /**
     * Массив полей таблицы
     *
     * @var array
     */
    public $fields = array(
        'user_enabled', 'user_visited', 'user_visited_trial', 'user_escaped', 'user_process'
    );

    /**
     * Первичный ключ таблицы
     *
     * @var string
     */
    public $idProperty = 'user_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_users';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ausers_grid';

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        if ($params['user_enabled'] == 1) {
            $params['user_visited'] = null;
            $params['user_visited_trial'] = 0;
            $params['user_escaped'] = null;
            $params['user_process'] = null;
        }
    }
}
?>