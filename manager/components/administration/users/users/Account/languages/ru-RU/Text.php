<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_account_insert' => 'Создание аккаунта',
    'title_account_update' => 'Изменение аккаунта "%s"',
    'text_btn_help'        => 'Справка',
    // поля формы
    'title_fieldset_user'      => 'Пользователь',
    'label_profile_name'       => 'Контингент',
    'title_fieldset_account'   => 'Учётная запись',
    'label_user_name'          => 'Логин',
    'label_user_password'      => 'Пароль',
    'label_user_password-cfrm' => 'Пароль (потв.)',
    'label_user_enabled'       => 'Доступная',
    'title_fieldset_groups'    => 'Группа пользователя',
    'label_group_name'         => 'Название',
    'title_fieldset_nacocunt'  => 'Новая учётная запись',
    // сообщения
    'msg_wrong_username' => 'Учётная запись с логином "%s" уже существует!',
    'msg_profile_name'   => '"Контингент"'
);
?>