<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_account_insert' => 'Creating a user account',
    'title_account_update' => 'Update account "%s"',
    // поля формы
    'title_fieldset_user'      => 'User',
    'label_profile_name'       => 'Contingent',
    'title_fieldset_account'   => 'Account',
    'label_user_name'          => 'Login',
    'label_user_password'      => 'Password',
    'label_user_password-cfrm' => 'Password (cfrm.)',
    'label_user_enabled'       => 'Enabled',
    'title_fieldset_groups'    => 'User group',
    'label_group_name'         => 'Name',
    'title_fieldset_nacocunt'  => 'New account',
    // сообщения
    'msg_wrong_username' => 'Account with the login "%" of already exists!',
    'msg_profile_name'   => '"Contingent"'
);
?>