<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_account_insert' => 'Створення аккаунта користувачеві',
    'title_account_update' => 'Зміна аккаунта "%s"',
    // поля формы
    'title_fieldset_user'      => 'Користувач',
    'label_profile_name'       => 'Контингент',
    'title_fieldset_account'   => 'Обліковий запис',
    'label_user_name'          => 'Логін',
    'label_user_password'      => 'Пароль',
    'label_user_password-cfrm' => 'Пароль (пітв.)',
    'label_user_enabled'       => 'Доступна',
    'title_fieldset_groups'    => 'Група користувача',
    'label_group_name'         => 'Назва',
    'title_fieldset_nacocunt'  => 'Новий обліковий запис',
    // сообщения
    'msg_wrong_username' => 'Обліковий запис з логіном "%s" вже існує!',
    'msg_profile_name'   => '"Контингент"'
);
?>