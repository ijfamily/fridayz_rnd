<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс учётной записи пользователя"
 * Пакет контроллеров "Учётная запись пользователя"
 * Группа пакетов     "Пользователи системы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AUsers_Account
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс учётной записи пользователя
 * 
 * @category   Gear
 * @package    GController_AUsers_Account
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AUsers_Account_Interface extends GController_Profile_Interface
{
    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        $data = array('group_id' => '');
        // состояние профиля "вставка"
        if ($this->isInsert) return $data;

        // соединение с базой данных
        GFactory::getDb()->connect();
        $query = new GDb_Query();
        // группа пользователей
        $sql = 'SELECT `g`.`group_name`, `g`.`group_id` '
             . 'FROM `gear_users` `u` '
             . 'LEFT JOIN `gear_user_groups` `g` USING (`group_id`) '
             . 'WHERE `u`.`user_id`=' . (int) $this->uri->id;;
        $record = $query->getRecord($sql);
        $data['group_id'] = $record['group_name'];

        return $data;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_account_insert'],
                  'titleEllipsis' => 40,
                  'gridId'        => 'gcontroller_ausers_grid',
                  'width'         => 365,
                  'height'        => $this->isUpdate ? 284 : 354,
                  'resizable'     => false,
                  'stateful'      => false,
                  'buttonsAdd'    => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-users',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );

        // поля формы (ExtJS class "Ext.Panel")
        // набор полей "пользователь"
        $fieldsetUser = array(
            'xtype'      => 'fieldset',
            'labelWidth' => 90,
            'title'      => $this->_['title_fieldset_user'],
            'items'      => array(
                array('xtype'      => 'mn-field-combo',
                      'fieldLabel' => $this->_['label_profile_name'],
                      'name'       => 'profile_id',
                      'pageSize'   => 50,
                      'anchor'     => '100%',
                      'hiddenName' => 'profile_id',
                      'allowBlank' => false,
                      'store'      => array(
                          'xtype' => 'jsonstore',
                          'url'   => $this->componentUrl . 'combo/trigger/?name=profiles'
                      )
                )
            )
        );

        // набор полей "учётная запись"
        $fieldsetAccount = array(
            'xtype'      => 'fieldset',
            'labelWidth' => 90,
            'title'      => $this->_['title_fieldset_account'],
            'items'      => array(
                array('xtype'         => 'textfield',
                      'fieldLabel'    => $this->_['label_user_name'],
                      'name'          => 'user_name',
                      'maxLength'     => 15,
                      'width'         => 220,
                      'checkDirty'    => false,
                      'allowBlank'    => false),
                array('xtype'         => 'textfield',
                      'inputType'     => 'password',
                      'fieldLabel'    => $this->_['label_user_password'],
                      'name'          => 'user_password',
                      'id'            => 'user_password',
                      'maxLength'     => 32,
                      'width'         => 220,
                      'allowBlank'    => false),
                array('xtype'         => 'textfield',
                      'inputType'     => 'password',
                      'vtype'         => 'password',
                      'fieldLabel'    => $this->_['label_user_password-cfrm'],
                      'name'          => 'user_password-cfrm',
                      'initialPassField' => 'user_password',
                      'width'         => 220,
                      'allowBlank'    => false),
                array('xtype'      => 'mn-field-chbox',
                      'fieldLabel' => $this->_['label_user_enabled'],
                      'default'    => 1,
                      'name'       => 'user_enabled')
            )
        );

        // набор полей "группа пользователя"
        $fieldsetGroups = array(
            'xtype'      => 'fieldset',
            'labelWidth' => 90,
            'title'      => $this->_['title_fieldset_groups'],
            'autoHeight' => true,
            'items'      => array(
                array('xtype'      => 'mn-field-combo-tree',
                      'fieldLabel' => $this->_['label_group_name'],
                      'resetable'  => false,
                      'anchor'     => '100%',
                      'name'       => 'group_id',
                      'hiddenName' => 'group_id',
                      'treeWidth'  => 400,
                      'treeRoot'   => $this->_isRootUser ? array('id' => 'root', 'expanded' => false, 'expandable' => true) : null,
                      'value'      => $data['group_id'],
                      'allowBlank' => false,
                      'store'      => array(
                          'xtype' => 'jsonstore',
                          'url'   => $this->componentUrl . 'combo/nodes/'
                      )
                )
            )
        );

        $items = array();
        // состояние профиля "вставка"
        if ($this->isInsert)
            $items[] = $fieldsetUser;
        $items[] = $fieldsetAccount;
        $items[] = $fieldsetGroups;

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->addItems($items);
        $form->url = $this->componentUrl . 'account/';

        parent::getInterface();
    }
}
?>