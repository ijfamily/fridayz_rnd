<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка пользователей системы"
 * Пакет контроллеров "Список пользователей системы"
 * Группа пакетов     "Пользователи системы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AUsers_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс списка пользователей системы
 * 
 * @category   Gear
 * @package    GController_AUsers_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AUsers_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'user_id';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'profile_name';

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // имя пользователя
            array('name' => 'profile_name', 'type' => 'string'),
            // логин пользователя
            array('name' => 'user_name', 'type' => 'string'),
            // активная учетная запись
            array('name' => 'user_enabled', 'type' => 'integer'),
            // дата авторизации
            array('name' => 'user_visited', 'type' => 'string'),
            // дата выхода
            array('name' => 'user_escaped', 'type' => 'string'),
            // группа пользователя
            array('name' => 'group_name', 'type' => 'string'),
            // группа пользователя (сокр)
            array('name' => 'group_shortname', 'type' => 'string'),
            // фото
            array('name' => 'photo', 'type' => 'string'),
            // статус
            array('name' => 'user_status', 'type' => 'string')
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $this->columns = array(
            array('xtype'     => 'expandercolumn',
                  'url'       => $this->componentUrl . 'grid/expand/',
                  'typeCt'    => 'row'),
            array('xtype'     => 'numbercolumn'),
            array('xtype'     => 'rowmenu'),
            array('dataIndex' => 'profile_name',
                  'header'    => '<em class="mn-grid-hd-details"></em>'
                               . $this->_['header_profile_name'],
                  'tooltip'   => $this->_['tooltip_profile_name'],
                  'width'     => 200,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'user_name',
                  'header'    => $this->_['header_user_name'],
                  'tooltip'   => $this->_['tooltip_user_name'],
                  'width'     => 110,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('xtype'     => 'booleancolumn',
                  'cls'       => 'mn-bg-color-gray2',
                  'dataIndex' => 'user_enabled',
                  'url'       => $this->componentUrl . 'account/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-enabled.png" align="absmiddle">',
                  'tooltip'   => $this->_['header_user_enabled'],
                  'align'     => 'center',
                  'width'     => 45,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'user_visited',
                  'header'    => $this->_['header_user_visited'],
                  'tooltip'   => $this->_['tooltip_user_visited'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'user_escaped',
                  'header'    => $this->_['header_user_escaped'],
                  'tooltip'   => $this->_['tooltip_user_escaped'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'group_shortname',
                  'header'    => $this->_['header_group_name'],
                  'tooltip'   => $this->_['tooltip_group_name'],
                  'width'     => 170,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'user_status',
                  'header'    => $this->_['header_user_status'],
                  'tooltip'   => $this->_['tooltip_user_status'],
                  'width'     => 90,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_grid'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_grid'],
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'profileUrl'    => $this->componentUrl . 'account/')
        );

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "правка" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "добавить" (ExtJS class "Manager.button.InsertData")
        $group->items->add(array('xtype' => 'mn-btn-insert-data', 'gridId' => $this->classId));
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' => $this->classId));
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array(
                'xtype'      => 'mn-btn-clear-data',
                'msgConfirm' => $this->_['msg_btn_clear'],
                'gridId'     => $this->classId,
                'isN'        => true
            )
        );
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = 'component-users';
        $this->_cmp->tbar->items->add($group);

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $img = '<div class="icon-photo-s" style="background-image: url(\'{photo}\')"></div>';
        $cellInfo =
            '<div class="mn-grid-cell-tooltip-tl">{profile_name}</div>'
          . '<table class="mn-grid-cell-tooltip" cellpadding="0" cellspacing="5" border="0">'
          . '<tr><td valign="top">' . $img . '</td><td valign="top" style="width:350px">'
          . '<em>' . $this->_['header_user_visited'] . '</em>: <b>{user_visited}</b><br>'
          . '<em>' . $this->_['header_user_escaped'] . '</em>: <b>{user_escaped}</b><br>'
          . '<em>' . $this->_['header_user_name'] . '</em>: <b>{user_name}</b><br>'
          . '<em>' . $this->_['header_user_status'] . '</em>: <b>{user_status}</b><br>'
          . '<em>' . $this->_['header_group_name'] . '</em>: <b>{group_name}</b>'
          . '</td></tr></table>';
        $this->_cmp->cellTips = array(
            array('field' => 'profile_name', 'tpl' => $cellInfo),
            array('field' => 'group_shortname', 'tpl' => '{group_name}'),
            array('field' => 'user_name', 'tpl' => '{user_name}')
        );
        $this->_cmp->cellTipConfig = array('maxWidth' => 350);

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $this->_cmp->rowMenu = array(
            'xtype' => 'mn-grid-rowmenu',
            'items' => array(
                array('text'    => $this->_['rowmenu_account'],
                      'icon'    => $this->resourcePath . 'icon-item-account.png',
                      'url'     => $this->componentUrl . 'account/interface/'),
                /*
                array('xtype'   => 'menuseparator'),
                array('text'    => $this->_['rowmenu_joint'],
                      'icon'    => $this->resourcePath . 'icon-item-joint.png',
                      'url'     => $this->componentUri . '../joint/' . ROUTER_DELIMITER . 'grid/interface/')
                */
            )
        );

        parent::getInterface();
    }
}
?>