<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'      => 'Users of the system',
    'tooltip_grid'    => 'список учётных записей пользователей системы',
    'rowmenu_edit'    => 'Edit',
    'rowmenu_account' => 'Account',
    'rowmenu_preview' => 'Photo',
    'rowmenu_joint'   => 'Joint access',
    // панель управления
    'title_buttongroup_edit'   => 'Edit',
    'title_buttongroup_cols'   => 'Columns',
    'title_buttongroup_filter' => 'Filter',
    'msg_btn_clear'            => 'Do you really want to delete all entries <span class="mn-msg-delete"> '
                                . '("Users of the system")</span> ?',
    // столбцы
    'header_profile_name'     => 'Contingent',
    'tooltip_profile_name'    => 'User name',
    'header_profile_nick'     => 'Nick',
    'tooltip_profile_nick'    => 'Nickname',
    'header_user_name'        => 'Login',
    'tooltip_user_name'       => 'Login',
    'header_user_enabled'     => 'Active',
    'tooltip_user_enabled'    => 'Active user account',
    'header_user_visited'     => 'Visited',
    'tooltip_user_visited'    => 'Date visited',
    'header_user_escaped'     => 'Escaped',
    'tooltip_user_escaped'    => 'Date logout',
    'header_group_name'       => 'Group',
    'tooltip_group_name'      => 'User group',
    'header_user_status'      => 'Status',
    'tooltip_user_status'     => 'User status',
    // типы
    'data_string' => array('offline', 'online',),
    'data_gender' => array('женский', 'мужской'),
    // развернутая запись
    'label_profile_name'         => 'Фамилия Имя Отчество',
    'label_profile_gender'       => 'Пол',
    'label_profile_born'         => 'Дата рождения',
    'title_fieldset_connection'  => 'Средства связи',
    'label_contact_work_phone'   => 'Рабочий телефон',
    'label_contact_mobile_phone' => 'Мобильный телефон',
    'label_contact_home_phone'   => 'Домашний телефон',
    'title_fieldset_net'         => 'Социальные сети',
    'title_fieldset_address'     => 'Адрес',
    'label_address'              => 'Адрес',
    'label_address_index'        => 'Индекс',
    'label_address_country'      => 'Страна',
    'label_address_region'       => 'Область / штат',
    'label_address_city'         => 'Город'
);
?>