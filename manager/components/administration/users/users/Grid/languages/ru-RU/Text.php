<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'      => 'Пользователи системы',
    'tooltip_grid'    => 'список учётных записей пользователей системы',
    'rowmenu_edit'    => 'Редактировать',
    'rowmenu_account' => 'Учётная запись',
    'rowmenu_preview' => 'Фотография',
    'rowmenu_joint'   => 'Совместный доступ',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">("Пользователи системы")</span> ?',
    // столбцы
    'header_profile_name'     => 'Контингент',
    'tooltip_profile_name'    => 'Имя пользователя',
    'header_profile_nick'     => 'Ник',
    'tooltip_profile_nick'    => 'Псевдоним пользователя',
    'header_user_name'        => 'Логин',
    'tooltip_user_name'       => 'Логин пользователя',
    'header_user_enabled'     => 'Активный',
    'tooltip_user_enabled'    => 'Активность учётной записи пользователя',
    'header_user_visited'     => 'Аторизация',
    'tooltip_user_visited'    => 'Дата авторизации пользователя',
    'header_user_escaped'     => 'Выход',
    'tooltip_user_escaped'    => 'Дата выхода пользователя из системы',
    'header_group_name'       => 'Группа',
    'tooltip_group_name'      => 'Группа пользователя',
    'header_user_status'      => 'Статус',
    'tooltip_user_status'     => 'Статус пользователя',
    // типы
    'data_string' => array('не в сети', 'в сети'),
    'data_gender' => array('женский', 'мужской'),
    // развернутая запись
    'label_profile_name'         => 'Фамилия Имя Отчество',
    'label_profile_gender'       => 'Пол',
    'label_profile_born'         => 'Дата рождения',
    'title_fieldset_connection'  => 'Средства связи',
    'label_contact_work_phone'   => 'Рабочий телефон',
    'label_contact_mobile_phone' => 'Мобильный телефон',
    'label_contact_home_phone'   => 'Домашний телефон',
    'title_fieldset_net'         => 'Социальные сети',
    'title_fieldset_address'     => 'Адрес',
    'label_address'              => 'Адрес',
    'label_address_index'        => 'Индекс',
    'label_address_country'      => 'Страна',
    'label_address_region'       => 'Область / штат',
    'label_address_city'         => 'Город'
);
?>