<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'      => 'Користувачі системи',
    'tooltip_grid'    => 'список учётных записей пользователей системы',
    'rowmenu_edit'    => 'Редагувати',
    'rowmenu_account' => 'Обліковий запис',
    'rowmenu_preview' => 'Фотографія',
    'rowmenu_joint'   => 'Совместный доступ',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Стовпці',
    'title_buttongroup_filter' => 'Фільтр',
    'msg_btn_clear'            => 'Ви дійсно бажаєте видалити всі записи <span class="mn-msg-delete">("Користувачі системи")</span> ?',
    // столбцы
    'header_profile_name'     => 'Контингент',
    'tooltip_profile_name'    => 'Ім\'я користувача',
    'header_profile_nick'     => 'Нік',
    'tooltip_profile_nick'    => 'Псевдоним користувача',
    'header_user_name'        => 'Логін',
    'tooltip_user_name'       => 'Логін користувача',
    'header_user_enabled'     => 'Активний',
    'tooltip_user_enabled'    => 'Активність облікового запису користувача',
    'header_user_visited'     => 'Аторізація',
    'tooltip_user_visited'    => 'Дата авторизації користувача',
    'header_user_escaped'     => 'Вихід',
    'tooltip_user_escaped'    => 'Дата виходу користувача з системи',
    'header_group_name'       => 'Група',
    'tooltip_group_name'      => 'Група користувача',
    'header_user_status'      => 'Статус',
    'tooltip_user_status'     => 'Статус користувача',
    // типы
    'data_string' => array('не в мережі', 'в мережі'),
    'data_gender' => array('женский', 'мужской'),
    // развернутая запись
    'label_profile_name'         => 'Фамилия Имя Отчество',
    'label_profile_gender'       => 'Пол',
    'label_profile_born'         => 'Дата рождения',
    'title_fieldset_connection'  => 'Средства связи',
    'label_contact_work_phone'   => 'Рабочий телефон',
    'label_contact_mobile_phone' => 'Мобильный телефон',
    'label_contact_home_phone'   => 'Домашний телефон',
    'title_fieldset_net'         => 'Социальные сети',
    'title_fieldset_address'     => 'Адрес',
    'label_address'              => 'Адрес',
    'label_address_index'        => 'Индекс',
    'label_address_country'      => 'Страна',
    'label_address_region'       => 'Область / штат',
    'label_address_city'         => 'Город'
);
?>