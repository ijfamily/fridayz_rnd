<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка пользователей системы"
 * Пакет контроллеров "Список пользователей системы"
 * Группа пакетов     "Пользователи системы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AUsers_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные списка пользователей системы
 * 
 * @category   Gear
 * @package    GController_AUsers_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AUsers_Grid_Data extends GController_Grid_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * (для обработки записей таблицы)
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'user_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_users';

    /**
     * Формат даты ив время
     *
     * @var string
     */
    protected $_frmDateTime = '';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);


        $this->_isRoot = $this->session->get('group/id');
        $settings = $this->session->get('user/settings');
        $this->_frmDateTime = $settings['format/datetime'];

        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS `p`.`profile_name`, `p`.`profile_photo`, `ug`.*, `u`.*, '
          . "IF(TIMESTAMPDIFF(SECOND, IF(`u`.`user_process`, `u`.`user_process`, '" . date('Y-m-d'). "'), '" . date('Y-m-d H:i:s') . "')>60, '0', '1') `user_status` "
          . 'FROM `gear_user_profiles` `p`, `gear_users` `u` '
          . 'LEFT JOIN `gear_user_groups` `ug` USING (`group_id`) '
           .'WHERE `p`.`user_id`=`u`.`user_id` %filter '
          . 'ORDER BY %sort '
          . 'LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // имя пользователя
            'profile_name' => array('type' => 'string'),
            // логин пользователя
            'user_name' => array('type' => 'string'),
            'user_id' => array('type' => 'integer'),
            // активная учетная запись
            'user_enabled' => array('type' => 'integer'),
            // дата авторизации
            'user_visited' => array('type' => 'string'),
            // дата выхода
            'user_escaped' => array('type' => 'string'),
            // группа пользователя
            'group_name' => array('type' => 'string'),
            // группа пользователя (сокр)
            'group_shortname' => array('type' => 'string'),
            // группа пользователя (сокр.)
            'group_shortname' => array('type' => 'string'),
            // фото
            'profile_photo' => array('type' => 'string'),
            // статус
            'user_status' => array('type' => 'string'),
            // системные поля
            'sys_date_update' => array('type' => 'string', 'sort' => 'u.sys_date_update'),
            'sys_date_insert' => array('type' => 'string', 'sort' => 'u.sys_date_insert')
        );
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        $query = new GDb_Query();
        // удаление журнала выхода пользователей ("gear_user_escapes")
        $sql = 'DELETE `gear_user_escapes` '
             . 'FROM `gear_user_escapes`,`gear_users` '
             . 'WHERE `gear_user_escapes`.`user_id`=`gear_users`.`user_id` AND '
             . '`gear_users`.`sys_record`<>1 AND '
             . '`gear_users`.`user_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_user_escapes` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление журнала авторизации пользователей ("gear_user_attends")
        $sql = 'DELETE `gear_user_attends` '
             . 'FROM `gear_user_attends`,`gear_users` '
             . 'WHERE `gear_user_attends`.`user_id`=`gear_users`.`user_id` AND '
             . '`gear_users`.`sys_record`<>1 AND '
             . '`gear_users`.`user_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_user_attends` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление журнала действий пользователей ("gear_log")
        $sql = 'DELETE `gear_log` '
             . 'FROM `gear_log`,`gear_users` '
             . 'WHERE `gear_log`.`user_id`=`gear_users`.`user_id` AND '
             . '`gear_users`.`sys_record`<>1 AND '
             . '`gear_users`.`user_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_log` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // обновление профилей пользователей ("gear_user_profiles")
        $sql = 'UPDATE `gear_user_profiles` `p`, `gear_users` `u` '
             . 'SET `p`.`user_id`=NULL '
             . 'WHERE `p`.`user_id`=`u`.`user_id` AND `u`.`sys_record`<>1 AND '
             . '`u`.`user_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }


    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        // если пользователь существует
        $user = GUsers::get($record['user_id']);
        if ($user) {
            $record['photo'] = $user['photo'];
            $record['profile_name'] = $user['profile_name'];
        }
        // время авторизации и выхода пользователя
        if (!empty($record['user_escaped']))
            $record['user_escaped'] = date($this->_frmDateTime, strtotime($record['user_escaped']));
        if (!empty($record['user_visited']))
            $record['user_visited'] = date($this->_frmDateTime, strtotime($record['user_visited']));
        // статус пользователя
        $record['user_status'] = '<span class="icon-user-status-' . ($record['user_status'] ? 'on' : 'off') . '">' . $this->_['data_string'][(int)$record['user_status']] . '</span>';

        return $record;
    }
}
?>