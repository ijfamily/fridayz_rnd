<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Статистика пользователей системы"
 * Группа пакетов     "Пользователи системы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AStatUsers
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Статистика пользователей системы
 * Описание: Статистика пользователей системы
 * ID класса: gcontroller_astatusers_grid
 * Группа: Администрирование / Пользователи
 * Очищать: нет
 * Ресурс
 *    Пакет: administration/users/statistics/
 *    Контроллер: administration/users/statistics/Grid/
 *    Интерфейс: grid/interface/
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске: нет
 */

return array(
    // {A}- модуль "Administration" {Users} - пакет контроллеров "Users" -> AStatUsers
    'clsPrefix' => 'AStatUsers'
);
?>