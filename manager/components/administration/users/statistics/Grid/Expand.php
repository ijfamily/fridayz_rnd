<?php
/**
 * Gear Manager
 *
 * Контроллер         "Развёрнутая запись статистики пользователей"
 * Пакет контроллеров "Список статистики пользователей"
 * Группа пакетов     "Пользователи системы"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AStatUsers_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Expand');

/**
 * Развёрнутая запись статистики пользователя
 * 
 * @category   Gear
 * @package    GController_AStatUsers_Grid
 * @subpackage Expand
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AStatUsers_Grid_Expand extends GController_Grid_Expand
{
    /**
     * Тайтл записи
     * 
     * @var string
     */
    protected $_title = '';

    /**
     * Вывод данных в интерфейс
     * 
     * @return void
     */
    protected function dataExpand()
    {
        parent::dataAccessView();

        $data = $this->getAttributes();
        $this->response->data = $data;
    }

    /**
     * Возращает атрибуты записи
     * 
     * @return string
     */
    protected function getAttributes()
    {
        $result = '';
        $settings = $this->session->get('user/settings');
        $query = new GDb_Query();
        $list = new GDb_Query();
        $sql = 'SELECT `c`.*,`cl` .*,`cg`.`group_name`,`cm`.`module_id`,`cm`.`module_name` FROM `gear_controllers` `c` '
             . 'LEFT JOIN `gear_controllers_l` `cl` USING(`controller_id`) '
               // группа компонентов
             . 'LEFT JOIN (SELECT `g`.*,`l`.`group_name`,`l`.`group_about` FROM `gear_controller_groups` `g` JOIN `gear_controller_groups_l` `l` USING(`group_id`) '
             . 'WHERE `l`.`language_id`=' . $this->language->get('id') . ') `cg` USING (`group_id`) '
               // модули
             . 'LEFT JOIN `gear_modules` `cm` ON `cm`.`module_id`=`c`.`module_id` '
             . 'WHERE `cl`.`language_id`=' . $this->language->get('id') . ' AND `c`.`controller_statistics`=1 ';
        if ($list->execute($sql) === false)
            throw new GSqlException();
        $date = date('Y-m-d');
        $userId = $this->uri->id;
        while (!$list->eof()) {
            $cmp = $list->next();
            $table = $cmp['controller_statistics_table'];
            if (empty($table))
                throw new GException('Data processing error', $this->_['msg_table_empty'], array($cmp['controller_name']), true);

            $data = '';
            // за весь период
            $data .= '<fieldset class="fixed" style="width:260px;"><label>' . $this->_['title_fieldset_all'] . '</label><ul>';
            $sql = "SELECT COUNT(*) `count` FROM `" . $table . "` WHERE `sys_user_insert`=$userId OR `sys_user_update`=$userId";
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            $data .= '<li><label>' . $this->_['label_total_records'] . ':</label> ' . $rec['count'] . '</li>';
            $data .= '</fieldset>';

            // за месяц
            $newDate = date('Y-m-d', mktime(0, 0, 0, date("m")-1, date("d"), date("Y")));
            $data .= '<fieldset class="fixed" style="width:260px;"><label>' . $this->_['title_fieldset_month'] . '</label><ul>';
            // добавлено записей:
            $sql = "SELECT COUNT(*) `count` FROM `$table` WHERE `sys_date_insert` BETWEEN '$newDate' AND '$date' AND `sys_user_insert`=$userId";
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            $data .= '<li><label>' . $this->_['label_insert_records'] . ':</label> ' . '0' . '</li>';
            // изменено записей:
            $sql = "SELECT COUNT(*) `count` FROM `$table` WHERE `sys_date_update` BETWEEN '$newDate' AND '$date' AND `sys_user_update`=$userId";
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            $data .= '<li><label>' . $this->_['label_update_records'] . ':</label> ' . $rec['count'] . '</li>';
            $data .= '</fieldset>';

            // за неделю
            $newDate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d") - 7, date("Y")));
            $data .= '<fieldset class="fixed" style="width:260px;"><label>' . $this->_['title_fieldset_week'] . '</label><ul>';
            // добавлено записей:
            $sql = "SELECT COUNT(*) `count` FROM `$table` WHERE `sys_date_insert` BETWEEN '$newDate' AND '$date' AND `sys_user_insert`=$userId";
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            $data .= '<li><label>' . $this->_['label_insert_records'] . ':</label> ' . '0' . '</li>';
            // изменено записей:
            $sql = "SELECT COUNT(*) `count` FROM `$table` WHERE `sys_date_update` BETWEEN '$newDate' AND '$date' AND `sys_user_update`=$userId";
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            $data .= '<li><label>' . $this->_['label_update_records'] . ':</label> ' . $rec['count'] . '</li>';
            $data .= '</fieldset>';

            // за день
            $data .= '<fieldset class="fixed" style="width:260px;"><label>' . $this->_['title_fieldset_day'] . '</label><ul>';
            // добавлено записей:
            $sql = "SELECT COUNT(*) `count` FROM `$table` WHERE `sys_date_insert`='$date' AND `sys_user_insert`=$userId";
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            $data .= '<li><label>' . $this->_['label_insert_records'] . ':</label> ' . '0' . '</li>';
            // изменено записей:
            $sql = "SELECT COUNT(*) `count` FROM `$table` WHERE `sys_date_update`='$date' AND `sys_user_update`=$userId";
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            $data .= '<li><label>' . $this->_['label_update_records'] . ':</label> ' . $rec['count'] . '</li>';
            $data .= '</fieldset>';

            // за час
            $newDate = date('Y-m-d', mktime(date("H")-1, date("i"), date("s"), date("m"), date("d"), date("Y")));
            $data .= '<fieldset class="fixed" style="width:260px;"><label>' . $this->_['title_fieldset_hour'] . '</label><ul>';
            // добавлено записей:
            $sql = "SELECT COUNT(*) `count` FROM `$table` WHERE `sys_date_insert` BETWEEN '$newDate' AND '$date' AND `sys_user_insert`=$userId";
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            $data .= '<li><label>' . $this->_['label_insert_records'] . ':</label> ' . '0' . '</li>';
            // изменено записей:
            $sql = "SELECT COUNT(*) `count` FROM `$table` WHERE `sys_date_update` BETWEEN '$newDate' AND '$date' AND `sys_user_update`=$userId";
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            $data .= '<li><label>' . $this->_['label_update_records'] . ':</label> ' . $rec['count'] . '</li>';
            $data .= '</fieldset>';

            // последняя запись
            $sql = "SELECT * FROM `$table` WHERE `sys_user_insert`=$userId ORDER BY `sys_date_insert` DESC,`sys_time_insert` DESC LIMIT 1";
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            $data .= '<fieldset class="fixed" style="width:260px;"><label>' . $this->_['title_fieldset_last'] . '</label><ul>';
            if (empty($rec['sys_date_insert']))
                $value = '';
            else
                $value = date($settings['format/datetime'], strtotime($rec['sys_date_insert'] . ' ' . $rec['sys_time_insert']));
            $data .= '<li><label>' . $this->_['label_insert_record'] . ':</label> ' . $value . '</li>';
            $sql = "SELECT * FROM `$table` WHERE `sys_user_update`=$userId ORDER BY `sys_date_update` DESC,`sys_time_update` DESC LIMIT 1";
            if (($rec = $query->getRecord($sql)) === false)
                throw new GSqlException();
            if (empty($rec['sys_date_update']))
                $value = '';
            else
                $value = date($settings['format/datetime'], strtotime($rec['sys_date_update'] . ' ' . $rec['sys_time_update']));
            $data .= '<li><label>' . $this->_['label_update_record'] . ':</label> ' . $value . '</li>';
            $data .= '</fieldset>';

            $result .= '<div class="mn-row-header">' .  $this->_['header_attr'] . ': <b>' 
                   . '<img align="absmiddle" style="margin-left:5px;" src="' . PATH_COMPONENT . $cmp['controller_uri']. 'resources/icon.png">'
                   . $cmp['controller_name'] . '</b></div>'
                   . '<div class="mn-row-body border">' . $data . '<div class="wrap"></div></div>';
        }

        return $result;
    }
}
?>