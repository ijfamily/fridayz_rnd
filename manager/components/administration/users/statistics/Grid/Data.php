<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные статистики пользователей системы"
 * Пакет контроллеров "Список статистики пользователей системы"
 * Группа пакетов     "Пользователи системы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AStatUsers_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные статистики пользователей системы
 * 
 * @category   Gear
 * @package    GController_AStatUsers_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AStatUsers_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'user_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_users';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS `p`.`profile_name`, `p`.`profile_photo`, `ug`.*, `u`.* '
          . 'FROM `gear_user_profiles` `p`, `gear_users` `u` '
          . 'LEFT JOIN `gear_user_groups` `ug` USING (`group_id`) '
           .'WHERE `p`.`user_id`=`u`.`user_id` %filter '
          . 'ORDER BY %sort '
          . 'LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // имя пользователя
            'profile_name' => array('type' => 'string'),
            // логин пользователя
            'user_name' => array('type' => 'string'),
            'user_id' => array('type' => 'integer'),
            // группа пользователя
            'group_name' => array('type' => 'string'),
            // фото
            'photo' => array('type' => 'string')
        );
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        // если пользователь существует
        $user = GUsers::get($record['user_id']);
        if ($user) {
            $record['photo'] = $user['photo'];
            $record['profile_name'] = $user['profile_name'];
        }

        return $record;
    }
}
?>