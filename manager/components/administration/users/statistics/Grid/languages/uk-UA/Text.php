<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Статистика користувачів системи',
    'tooltip_grid' => 'статистика действий пользователей в системе',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Стовпці',
    'title_buttongroup_filter' => 'Фільтр',
    // столбцы
    'header_profile_name'     => 'Контингент',
    'tooltip_profile_name'    => 'Ім\'я користувача',
    'header_profile_nick'     => 'Нік',
    'tooltip_profile_nick'    => 'Псевдоним користувача',
    'header_user_name'        => 'Логін',
    'tooltip_user_name'       => 'Логін користувача',
    'header_group_name'       => 'Група',
    'tooltip_group_name'      => 'Група користувача',
    // развернутая запись
    'header_attr'          => 'Статистика оброблених даних компонента',
    'title_fieldset_all'   => 'За весь період',
    'label_total_records'  => 'Всього записів',
    'label_insert_records' => 'Додано записів',
    'label_insert_record'  => 'Додана запис',
    'label_update_records' => 'Змінини записи',
    'label_update_record'  => 'Змінена запис',
    'label_unknow_users'   => 'Без урахування користувача',
    'title_fieldset_month' => 'За місяць',
    'title_fieldset_week'  => 'За тиждень',
    'title_fieldset_day'   => 'За день',
    'title_fieldset_hour'  => 'За останню годину',
    'title_fieldset_last'  => 'Останній запис',
    // сообщения
    'msg_table_empty' => 'Не указана таблица в настройках компонента "%s"!'
);
?>