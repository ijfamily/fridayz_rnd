<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Statistics system users',
    'tooltip_grid' => 'статистика действий пользователей в системе',
    // панель управления
    'title_buttongroup_edit'   => 'Edit',
    'title_buttongroup_cols'   => 'Columns',
    'title_buttongroup_filter' => 'Filter',
    // столбцы
    'header_profile_name'     => 'Contingent',
    'tooltip_profile_name'    => 'User name',
    'header_profile_nick'     => 'Nick',
    'tooltip_profile_nick'    => 'Nickname',
    'header_user_name'        => 'Login',
    'tooltip_user_name'       => 'Login',
    'header_group_name'       => 'Group',
    'tooltip_group_name'      => 'User group',
    // развернутая запись
    'header_attr'          => 'Statistics processed data component',
    'title_fieldset_all'   => 'Over the entire period',
    'label_total_records'  => 'Total number of records',
    'label_insert_records' => 'Added entries',
    'label_insert_record'  => 'Added entry',
    'label_update_records' => 'Changed records',
    'label_update_record'  => 'Changed record',
    'label_unknow_users'   => 'Excluding user',
    'title_fieldset_month' => 'For a month',
    'title_fieldset_week'  => 'For the week',
    'title_fieldset_day'   => 'For a day',
    'title_fieldset_hour'  => 'In the last hour',
    'title_fieldset_last'  => 'Last record',
    // сообщения
    'msg_table_empty' => 'Не указана таблица в настройках компонента "%s"!'
);
?>