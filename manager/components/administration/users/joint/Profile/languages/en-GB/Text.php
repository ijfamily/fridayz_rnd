<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Добавление пользователя к совместному доступу',
    // поля формы
    'title_fieldset_users' => 'Совместный доступ для',
    'value_user_select'    => 'Для выбора пользователя из списка используйте <b>Ctrl</b> + <b>LeftMB</b>',
    'header_profile_name'  => 'Контингент',
    'tooltip_profile_name' => 'Имя пользователя',
    'header_profile_nick'  => 'Ник',
    'tooltip_profile_nick' => 'Псевдоним пользователя',
    'header_user_name'     => 'Логин',
    'tooltip_user_name'    => 'Логин пользователя',
    'header_group_name'    => 'Группа',
    'tooltip_group_name'   => 'Группа пользователя',
    // сообщения
    'msg_not_selected' => 'Пользователь из списка не выбран!'
);
?>