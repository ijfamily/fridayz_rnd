<?php
/**
 * Gear Manager
 *
 * Controller          "Интерфейс профиля совместного доступа"
 * Package controllers "Профиль совместного доступа"
 * Группа пакетов      "Пользователи системы"
 * Модуль              "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AJointUsers_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2014-08-04 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля совместного доступа
 * 
 * @category   Gear
 * @package    GController_AJointUsers_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AJointUsers_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ausers_grid';

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительные данные для формирования интерфейса
        $this->getDataInterface();

        // Ext_Window (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'titleEllipsis' => 70,
                  'gridId'        => 'gcontroller_ajointusers_grid',
                  'width'         => 650,
                  'autoHeight'    => true,
                  'resizable'     => false,
                  'stateful'      => false)
        );

        // список пользователей
        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $params = $this->config->get('PHOTO');
        $img = '<div class="icon-photo-s"><img style="width:' . $params['SIZE']['WIDTH'] . 'px;height:' . $params['SIZE']['HEIGHT'] 
             . 'px" src="{profile_photo}"/></div>';
        $cellInfo =
            '<div class="mn-grid-cell-tooltip-tl">{profile_name}</div>'
          . '<table class="mn-grid-cell-tooltip" cellpadding="0" cellspacing="5" border="0">'
          . '<tr><td valign="top">' . $img . '</td><td valign="top" style="width:350px">'
          . '<em>' . $this->_['header_user_name'] . '</em>: <b>{user_name}</b><br>'
          . '<em>' . $this->_['header_group_name'] . '</em>: <b>{group_name}</b>'
          . '</td></tr></table>';
        $cellTips = array(
            array('field' => 'profile_name', 'tpl' => $cellInfo),
            array('field' => 'group_name', 'tpl' => '{group_name}'),
            array('field' => 'user_name', 'tpl' => '{user_name}')
        );
        $gridItem = array(
            'xtype'      => 'mn-grid',
            'height'     => 200,
            'anchor'     => '100%',
            'baseCls'    => 'mn-grid-nobg',
            'stateful'   => false,
            'isReadOnly' => true,
            'selectTo' => array(
                'inputId'   => 'joint_id',
                'fieldText' => 'profile_name'
            ),
            'columns'    => array(
                array('xtype' => 'numbercolumn', 'stateIndex' => 0),
                array('dataIndex'  => 'profile_name',
                      'header'     => '<em class="mn-grid-hd-details"></em>' . $this->_['header_profile_name'],
                      'tooltip'    => $this->_['tooltip_profile_name'],
                      'width'      => 200,
                      'sortable'   => true,
                      'filter'     => array('type' => 'string'),
                      'stateIndex' => 1
                ),
                array('dataIndex'  => 'user_name',
                      'header'     => $this->_['header_user_name'],
                      'tooltip'    => $this->_['tooltip_user_name'],
                      'width'      => 110,
                      'sortable'   => true,
                      'filter'     => array('type' => 'string'),
                      'stateIndex' => 3
                ),
                array('dataIndex'  => 'group_name',
                      'header'     => $this->_['header_group_name'],
                      'tooltip'    => $this->_['tooltip_group_name'],
                      'width'      => 170,
                      'sortable'   => true,
                      'filter'     => array('type' => 'string'),
                      'stateIndex' => 4
                )
            ),
            'cellTips' => $cellTips,
            'store' => array(
                'xtype'           => 'jsonstore',
                'url'             => 'administration/users/joint/' . ROUTER_DELIMITER . 'list/',
                'root'            => 'data',
                'totalProperty'   => 'totalCount',
                'messageProperty' => 'message',
                'remoteSort'      => true,
                'autoLoad'        => true,
                'batch'           => false,
                'autoSave'        => true,
                'idProperty'      => 'user_id',
                'sortInfo'        => array('field' => 'profile_name', 'direction' => 'ASC'),
                'fields'          => array(
                    array('name' => 'user_id', 'type' => 'integer'),
                    array('name' => 'profile_name', 'type' => 'string'),
                    array('name' => 'profile_photo', 'type' => 'string'),
                    array('name' => 'user_name', 'type' => 'string'),
                    array('name' => 'group_name', 'type' => 'string'),
                )
            ),
            'bbar' => array(
                'xtype'       => 'paging',
                'displayInfo' => true,
                'cls'         => 'x-toolbar-body',
                'pageSize'    => 20
            )
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            $gridItem,
            array('xtype'      => 'displayfield',
                  'fieldClass' => 'mn-field-value-notice',
                  'style'      => 'margin-top:6px;',
                  'hideLabel'  => true,
                  'value'      => $this->_['value_user_select']),
            array('xtype' => 'hidden',
                  'id'    => 'joint_id',
                  'name'  => 'joint_id')
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->labelWidth = 150;
        $form->items->addItems($items);
        $form->url = $this->componentUrl . 'profile/';

        parent::getInterface();
    }
}
?>