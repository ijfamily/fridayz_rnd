<?php
/**
 * Gear Manager
 *
 * Контроллер         "Изменение полей совместного доступа"
 * Пакет контроллеров "Совместный доступ"
 * Группа пакетов     "Пользователи системы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AJointUsers_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Field.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Field');

/**
 * Изменение полей учётной записи пользователя
 * 
 * @category   Gear
 * @package    GController_AJointUsers_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Field.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AJointUsers_Profile_Field extends GController_Profile_Field
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * (для обработки записей таблицы)
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Массив полей таблицы
     *
     * @var array
     */
    public $fields = array('joint_enabled');

    /**
     * Первичный ключ таблицы
     *
     * @var string
     */
    public $idProperty = 'joint_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_user_joint';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ausers_grid';
}
?>