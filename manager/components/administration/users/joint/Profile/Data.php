<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные для интерфейса совместного доступа"
 * Пакет контроллеров "Профиль совместного доступа"
 * Группа пакетов     "Пользователи системы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AJointUsers_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные для интерфейса профиля совместного доступа
 * 
 * @category   Gear
 * @package    GController_AJointUsers_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AJointUsers_Profile_Data extends GController_Profile_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    protected $fields = array('joint_id');

    /**
     * Первичный ключ таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'joint_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_user_joint';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ausers_grid';

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        if ($this->isInsert) {
            if (empty($params['joint_id']))
                throw new GException('Error', $this->_['msg_not_selected']);
        }
    }

    /**
     * Проверка существования записи с одинаковыми значениями
     * 
     * @param  array $params array (field => value, ....)
     * @return void
     */
    protected function isDataExist($params)
    {
        $userId = $this->store->get('record', 0, 'gcontroller_ausers_grid');
        $query = new GDb_Query();
        $sql = 'SELECT COUNT(*) `count` FROM `gear_user_joint` WHERE `user_id`=' . $userId . ' AND `joint_user` IN (' . $params['joint_id'] . ') ';
        if (($record = $query->getRecord($sql)) === false)
            throw new GSqlException();
        if ($record['count'] > 0)
            throw new GException('Data processing error', 'Record the values of fields:%s already exists!', array('"' . $this->_['header_profile_name'] . '"'));
    }

    /**
     * Вставка данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataInsert($params = array())
    {
        parent::dataAccessInsert();

        if ($this->input->isEmpty('POST'))
            throw new GException('Warning', 'To execute a query, you must change data!');
        $params = $this->dataIntersect($this->input->post, $this->fields);
        // проверки входных данных
        $this->isDataCorrect($params);
        // проверка существования записи с одинаковыми значениями
        $this->isDataExist($params);
        // использование системных полей в запросе SQL
        $data = array(
            'sys_date_insert' => date('Y-m-d'),
            'sys_time_insert' => date('H:i:s'),
            'sys_user_insert' => $this->session->get('user_id'),
            'user_id'         => $this->store->get('record', 0, 'gcontroller_ausers_grid')
        );
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($params, 'method "POST"');
        }
        $table = new GDb_Table($this->tableName, $this->idProperty);
        // вставка данных
        $ids = explode(',', $params['joint_id']);
        for ($i = 0; $i < sizeof($ids); $i++) {
            $data['joint_user'] = $ids[$i];
            $data['joint_enabled'] = 1;
            if ($table->insert($data) === false)
                throw new GException('Data processing error', 'Query error (Internal Server Error)');
        }
        // запись в журнал действий пользователя
        $this->logInsert(
            array('log_query'        => $table->query->getSQL(),
                  'log_error'        => $this->response->success === false ? $table->getError() : null,
                  'log_query_params' => $params)
        );
    }
}
?>