<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка пользователей системы"
 * Пакет контроллеров "Список пользователей системы"
 * Группа пакетов     "Пользователи системы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AJointUsers_List
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные списка пользователей системы
 * 
 * @category   Gear
 * @package    GController_AJointUsers_List
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AJointUsers_List_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'user_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_users';

    /**
     * Каталог изображений пользователей
     *
     * @var string
     */
    protected $_pathData = 'data/photos/';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ausers_grid';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        $userId = $this->store->get('record', 0, 'gcontroller_ausers_grid');

        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS `p`.`profile_name`, `p`.`profile_photo`, `ug`.*, `u`.* '
          . 'FROM `gear_user_profiles` `p`, `gear_users` `u` '
          . 'LEFT JOIN `gear_user_groups` `ug` USING (`group_id`) '
           .'WHERE `p`.`user_id`=`u`.`user_id` AND `u`.`user_id`<>' . $userId . ' AND `u`.`user_id` NOT IN '
          . '(SELECT `joint_user` FROM `gear_user_joint` WHERE `user_id`=' . $userId . ') %filter '
          . 'ORDER BY %sort '
          . 'LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // имя пользователя
            'profile_name' => array('type' => 'string'),
            // логин пользователя
            'user_name' => array('type' => 'string'),
            // группа пользователя
            'group_name' => array('type' => 'string'),
            // группа пользователя (сокр.)
            'group_shortname' => array('type' => 'string'),
            // фото
            'profile_photo' => array('type' => 'string')
        );
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        // фото пользователей
        if (empty($record['profile_photo']))
            $record['profile_photo'] = $this->_pathData . 'none_s.png';
        else {
            $src = '../../../../'. $this->_pathData . $record['profile_photo'];
            if (!file_exists($src))
                $record['profile_photo'] = 'none_sb.png';
        }

        return $record;
    }
}
?>