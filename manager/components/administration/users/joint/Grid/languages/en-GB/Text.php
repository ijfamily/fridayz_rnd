<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'      => 'Совместный доступ для "%s"',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete"> '
                                . '("Совместный доступ")</span> ?',
    // столбцы
    'header_profile_name'     => 'Контингент',
    'tooltip_profile_name'    => 'Имя пользователя',
    'header_profile_nick'     => 'Ник',
    'tooltip_profile_nick'    => 'Псевдоним пользователя',
    'header_user_name'        => 'Логин',
    'tooltip_user_name'       => 'Логин пользователя',
    'header_group_name'       => 'Группа',
    'tooltip_group_name'      => 'Группа пользователя',
    'header_joint_enabled'    => 'Использовать совместный доступ'
);
?>