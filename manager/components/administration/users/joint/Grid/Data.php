<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка совместного доступа"
 * Пакет контроллеров "Список совместного доступа"
 * Группа пакетов     "Пользователи системы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AJointUsers_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные списка совместного доступа
 * 
 * @category   Gear
 * @package    GController_AJointUsers_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AJointUsers_Grid_Data extends GController_Grid_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * (для обработки записей таблицы)
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'joint_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_user_joint';

    /**
     * Каталог изображений пользователей
     *
     * @var string
     */
    protected $_pathData = 'data/photos/';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_ausers_grid';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS `p`.`profile_name`,`p`.`profile_photo`,`ug`.`group_name`,`u`.`user_name`,`j`.* '
          . 'FROM `gear_user_joint` `j` '
          . 'JOIN `gear_users` `u` ON `u`.`user_id`=`j`.`joint_user` '
          . 'JOIN `gear_user_profiles` `p` ON `p`.`user_id`=`j`.`joint_user` '
          . 'JOIN `gear_user_groups` `ug` ON `ug`.`group_id`=`u`.`group_id` '
           .'WHERE `j`.`user_id`=' . $this->_recordId . ' %filter '
          . 'ORDER BY %sort '
          . 'LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // имя пользователя
            'profile_name' => array('type' => 'string'),
            // логин пользователя
            'user_name' => array('type' => 'string'),
            // совместный доступ
            'joint_enabled' => array('type' => 'integer'),
            // группа пользователя
            'group_name' => array('type' => 'string'),
            // фото
            'profile_photo' => array('type' => 'string')
        );
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        $query = new GDb_Query();
        //  удаление совместного доступа ("gear_user_joint")
        $sql = 'DELETE FROM `gear_user_joint` WHERE `user_id`=' . $this->store->get('record');
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_user_joint` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        // фото пользователей
        if (empty($record['profile_photo']))
            $record['profile_photo'] = $this->_pathData . 'none_s.png';
        else {
            $src = '../../../../'. $this->_pathData . $record['profile_photo'];
            if (!file_exists($src))
                $record['profile_photo'] = 'none_sb.png';
        }

        return $record;
    }
}
?>