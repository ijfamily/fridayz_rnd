<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Пошук у списку "Контингент"',
    // поля
    'header_profile_name'          => 'Фамилия Имя Отчество',
    'header_profile_born'          => 'Дата рождения',
    'header_profile_gender'        => 'Пол',
    'header_contact_home_phone'    => 'Домашний телефон',
    'header_contact_mobile_phone'  => 'Мобильный телефон',
    'header_contact_work_phone'    => 'Рабочий телефон',
    'header_contact_email'         => 'E-mail',
    'header_contact_skype'         => 'Skype',
    'header_contact_facebook'      => 'Facebook',
    'header_contact_twitter'       => 'Twitter',
    'header_contact_linkedin'      => 'LinkedIn',
    'header_contact_icq'           => 'ICQ',
    'header_contact_web'           => 'Web',
    'header_contact_vk'            => 'Вконтакте',
    'header_profile_address_index' => 'Индекс',
    'header_profile_address_country' => 'Страна',
    'header_profile_address_region'  => 'Область',
    'header_profile_address_city'    => 'Город',
    'header_profile_address'         => 'Адрес'
);
?>