<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск контингента"
 * Пакет контроллеров "Контингент"
 * Группа пакетов     "Контингент"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AContingent_Search
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск контингента
 * 
 * @category   Gear
 * @package    GController_AContingent_Search
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AContingent_Search_Data extends GController_Search_Data
{
    /**
     * дентификатор компонента (списка) к которому применяется поиск
     *
     * @var string
     */
    public $gridId = 'gcontroller_acontingent_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_acontingent_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор
     * Используется для инициализации атрибутов контроллер не переданного в конструктор
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'profile_name', 'label' => $this->_['header_profile_name']),
            array('dataIndex' => 'profile_born', 'label' => $this->_['header_profile_born']),
            array('dataIndex' => 'profile_gender', 'label' => $this->_['header_profile_gender']),
            array('dataIndex' => 'contact_work_phone', 'label' => $this->_['header_contact_work_phone']),
            array('dataIndex' => 'contact_mobile_phone', 'label' => $this->_['header_contact_mobile_phone']),
            array('dataIndex' => 'contact_home_phone', 'label' => $this->_['header_contact_home_phone']),
            array('dataIndex' => 'contact_email', 'label' => $this->_['header_contact_email']),
            array('dataIndex' => 'contact_icq', 'label' => 'ICQ'),
            array('dataIndex' => 'contact_skype', 'label' => 'Skype'),
            array('dataIndex' => 'contact_web', 'label' => 'Web'),
            array('dataIndex' => 'contact_facebook', 'label' => $this->_['header_contact_facebook']),
            array('dataIndex' => 'contact_linkedin', 'label' => $this->_['header_contact_linkedin']),
            array('dataIndex' => 'contact_twitter', 'label' => $this->_['header_contact_twitter']),
            array('dataIndex' => 'contact_vk', 'label' => $this->_['header_contact_vk']),
            array('dataIndex' => 'profile_address_index', 'label' => $this->_['header_profile_address_index']),
            array('dataIndex' => 'profile_address_country', 'label' => $this->_['header_profile_address_country']),
            array('dataIndex' => 'profile_address_region', 'label' => $this->_['header_profile_address_region']),
            array('dataIndex' => 'profile_address_city', 'label' => $this->_['header_profile_address_city']),
            array('dataIndex' => 'profile_address', 'label' => $this->_['header_profile_address'])
        );
    }
}
?>