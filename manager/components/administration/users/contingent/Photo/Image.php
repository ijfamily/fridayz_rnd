<?php
/**
 * Gear Manager
 *
 * Контроллер         "Обработка изображений контингента"
 * Пакет контроллеров "Фотографии контингента"
 * Группа пакетов     "Контингент"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AContingent_Photo
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Image.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Image');

/**
 * Обработка изображений контингента
 * 
 * @category   Gear
 * @package    GController_AContingent_Photo
 * @subpackage Image
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Image.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AContingent_Photo_Image extends GController_Image_Base
{
    /**
     * Название поля для загрузки изображения
     *
     * @var string
     */
    public $field = 'photo-image';

    /**
     * Каталог изображений
     *
     * @var string
     */
    public $pathData = 'data/photos/';

    /**
     * Название контроллера обработки данных изображений
     *
     * @var string
     */
    public $profileId = 'gcontroller_acontingent_profile';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_acontingent_grid';

    /**
     * Загрузка изображения
     * 
     * @return void
     */
    protected function imageUpload()
    {
        parent::imageUpload();

        // если изображение было ранее загружено для этого профиля, тогда удаляем
        $img = new GImage($this->pathData . $this->filename);
        // профиль изображения
        $cmdImage = 'resize{"width": 80, "height": 110}';
        if ($img->execute($cmdImage)) {
            // изменение изображения
            $img->save($this->pathData .  $this->filename);
        }
    }

    /**
     * Удаление загруженного изображения
     * 
     * @return void
     */
    protected function imageDelete()
    {
        parent::imageDelete();

        // соединение с базой данных
        GFactory::getDb()->connect();
        // данные пользователя
        $table = new GDb_Table('gear_user_profiles', 'profile_id');
        $user = $table->getRecord($this->uri->id);
        if (empty($user))
            throw new GException('Data processing error', 'Query error (Internal Server Error)');
        if (empty($user['profile_photo']))
            throw new GException('Data processing error', 'Query error (Internal Server Error)');
        // удаление изображения
        GFile::delete($this->pathData . $user['profile_photo'], false);
        if ($table->update(array('profile_photo' => null), $this->uri->id) === false)
            throw new GException('Data processing error', 'Query error (Internal Server Error)');
    }

    /**
     * Обновить изображения
     * 
     * @return void
     */
    protected function imageUpdate()
    {
        parent::imageUpdate();

        // если изображение было ранее загружено для этого профиля
        $filename = $this->store->get('upload', $this->accessId);
        if (empty($filename)) return;
        $nfilename = GFile::getNewFilename($filename, 'user_' . $this->uri->id);
        GFile::rename($this->pathData . $filename, $this->pathData . $nfilename);
        // соединение с базой данных
        GFactory::getDb()->connect();
        // обновить данные пользователя
        $table = new GDb_Table('gear_user_profiles', 'profile_id');
        if ($table->update(array('profile_photo' => $nfilename), $this->uri->id) === false)
            throw new GException('Data processing error', 'Query error (Internal Server Error)');
        $this->response->data = array('file' => $nfilename);
        // обновить сессию
        $this->store->set('upload', '', $this->accessId);
    }
}
?>