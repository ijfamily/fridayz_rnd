<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля записи контингента
 * Пакет контроллеров "Профиль записи контингента"
 * Группа пакетов     "Контингент"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AContingent_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля записи контингента
 * 
 * @category   Gear
 * @package    GController_AContingent_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AContingent_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Каталог изображений пользователей
     *
     * @var string
     */
    public $pathData = 'data/photos/';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_acontingent_profile';

    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        $data = array('profile_photo' => '');
        // обновить сессию
        $this->store->set('upload', '', $this->accessId);

        // состояние профиля "вставка"
        if ($this->isInsert) return $data;

        if ($this->state == 'info')
            $this->uri->id = $this->session->get('profile/id');

        // соединение с базой данных
        GFactory::getDb()->connect();
        $query = new GDb_Query();
        // группа пользователей
        $sql = 'SELECT * FROM `gear_user_profiles` WHERE `profile_id`=' . (int)$this->uri->id;
        $record = $query->getRecord($sql);
        if ($record === false)
            throw new GSqlException();
        if (!empty($record['profile_photo']))
            $data['url'] = 'f=' . $record['profile_photo'];
        else
            $record['profile_photo'] = '';
        // фото пользователей
        if ($record['profile_photo']) {
            $src = $this->pathData . $record['profile_photo'];
            if (!file_exists($src))
                $record['profile_photo'] = 'none_sb.png';
        }

        return $record;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // Ext_Window (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'titleEllipsis' => 40,
                  'gridId'        => 'gcontroller_acontingent_grid',
                  'width'         => 580,
                  'autoHeight'    => true,
                  'resizable'     => false,
                  'stateful'      => false,
                  'buttonsAdd'    => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-users-contingent',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );

        // поля вкладки "контакты"
        $tabContactItems = array(
            array('xtype'      => 'fieldset',
                  'labelWidth' => 135,
                  'title'      => $this->_['title_fieldset_connection'],
                  'items'      => array(
                      array('xtype'  => 'container',
                            'layout' => 'column',
                            'cls'    => 'mn-container-clean',
                            'items'  => array(
                                array('layout'      => 'form',
                                      'columnWidth' => 0.55,
                                      'labelWidth'  => 135,
                                      'items'       => array(
                                          array('xtype'      => 'textfield',
                                                'fieldLabel' => $this->_['label_contact_work_phone'],
                                                'name'       => 'contact_work_phone',
                                                'maxLength'  => 10,
                                                'anchor'     => '100%'),
                                          array('xtype'      => 'textfield',
                                                'fieldLabel' => $this->_['label_contact_mobile_phone'],
                                                'name'       => 'contact_mobile_phone',
                                                'maxLength'  => 20,
                                                'anchor'     => '100%'),
                                          array('xtype'      => 'textfield',
                                                'fieldLabel' => $this->_['label_contact_home_phone'],
                                                'name'       => 'contact_home_phone',
                                                'maxLength'  => 10,
                                                'anchor'     => '100%'),
                                      ) // items
                                ), // layout form
                                array('layout'      => 'form',
                                      'columnWidth' => 0.45,
                                      'labelWidth'  => 50,
                                      'bodyStyle'   => 'padding-left:12px;',
                                      'items'       => array(
                                        array('xtype'      => 'textfield',
                                              'fieldLabel' => 'E-mail',
                                              'name'       => 'contact_email',
                                              'maxLength'  => 50,
                                              'anchor'     => '100%'),
                                        array('xtype'      => 'textfield',
                                              'fieldLabel' => 'ICQ',
                                              'name'       => 'contact_icq',
                                              'maxLength'  => 50,
                                              'anchor'     => '100%'),
                                        array('xtype'      => 'textfield',
                                              'fieldLabel' => 'Skype',
                                              'name'       => 'contact_skype',
                                              'maxLength'  => 50,
                                              'anchor'     => '100%')
                                      ) // items
                                ) // layout form
                          ) // items container
                    ), // container
                    array('xtype'      => 'textfield',
                          'fieldLabel' => 'Web',
                          'name'       => 'contact_web',
                          'maxLength'  => 255,
                          'anchor'     => '100%')
                ) // items fieldset
            ), // fieldset
            array('xtype'      => 'fieldset',
                  'labelWidth' => 135,
                  'title'      => $this->_['title_fieldset_net'],
                  'items'      => array(
                      array('xtype'  => 'container',
                            'layout' => 'column',
                            'cls'    => 'mn-container-clean',
                            'items'  => array(
                                array('layout'      => 'form',
                                      'columnWidth' => 0.5,
                                      'labelWidth'  => 70,
                                      'items'       => array(
                                          array('xtype'      => 'textfield',
                                                'fieldLabel' => 'Facebook',
                                                'name'       => 'contact_facebook',
                                                'maxLength'  => 255,
                                                'anchor'     => '100%'),
                                          array('xtype'      => 'textfield',
                                                'fieldLabel' => 'LinkedIn',
                                                'name'       => 'contact_linkedin',
                                                'maxLength'  => 255,
                                                'anchor'     => '100%')
                                      ) // items
                                ), // layout form
                                array('layout'      => 'form',
                                      'columnWidth' => 0.5,
                                      'labelWidth'  => 58,
                                      'bodyStyle'   => 'padding-left:12px;',
                                      'items'       => array(
                                        array('xtype'      => 'textfield',
                                              'fieldLabel' => 'Twitter',
                                              'name'       => 'contact_twitter',
                                              'maxLength'  => 255,
                                              'anchor'     => '100%'),
                                        array('xtype'      => 'textfield',
                                              'fieldLabel' => 'VK',
                                              'name'       => 'contact_vk',
                                              'maxLength'  => 255,
                                              'anchor'     => '100%'),
                                      ) // items
                                ) // layout form
                          ) // items container
                    ) // container
                ) // items fieldset
            ) // fieldset
        );
        // вкладка "контакт"
        $tabContact = array(
            'title'       => $this->_['title_tab_contact'],
            'layout'      => 'form',
            'labelWidth'  => 60,
            'height'      => 240,
            'baseCls'     => 'mn-form-tab-body',
            'defaultType' => 'textfield',
            'items'       => $tabContactItems
        );

        // вкладка "адрес"
        $tabAddress = array(
            'layout'  => 'fit',
            'title'   => $this->_['title_tab_address'],
            'baseCls' => 'mn-form-tab-body',
            'items'   => array(
              array('xtype'  => 'container',
                    'layout' => 'column',
                    'cls'    => 'mn-container-clean',
                    'items'  => array(
                        array('layout'      => 'form',
                              'columnWidth' => 0.5,
                              'labelWidth'  => 90,
                              'items'       => array(
                                  array('xtype'      => 'textfield',
                                        'fieldLabel' => $this->_['label_address_index'],
                                        'name'       => 'profile_address_index',
                                        'width'      => 80,
                                        'maxLength'  => 10,
                                        'allowBlank' => true),
                                  array('xtype'      => 'textfield',
                                        'fieldLabel' => $this->_['label_address_country'],
                                        'name'       => 'profile_address_country',
                                        'anchor'     => '100%',
                                        'maxLength'  => 50,
                                        'allowBlank' => true),
                                  array('xtype'      => 'textfield',
                                        'fieldLabel' => $this->_['label_address_region'],
                                        'name'       => 'profile_address_region',
                                        'anchor'     => '100%',
                                        'maxLength'  => 70,
                                        'allowBlank' => true)
                              ) // items
                        ), // layout form
                        array('layout'      => 'form',
                              'columnWidth' => 0.5,
                              'labelWidth'  => 60,
                              'bodyStyle'   => 'padding-left:12px;',
                              'items'       => array(
                                  array('xtype'      => 'textfield',
                                        'fieldLabel' => $this->_['label_address_city'],
                                        'name'       => 'profile_address_city',
                                        'anchor'     => '100%',
                                        'maxLength'  => 70,
                                        'allowBlank' => true),
                                  array('xtype'      => 'textarea',
                                        'fieldLabel' => $this->_['label_address'],
                                        'name'       => 'profile_address',
                                        'anchor'     => '100%',
                                        'maxLength'  => 255,
                                        'height'     => 50,
                                        'allowBlank' => true)
                              ) // items
                        ) // layout form
                  ) // items container
            ) // container
            )
        );

        // вкладка "фотография"
        $tabPhoto = array(
            'layout'  => 'fit',
            'title'   => $this->_['title_tab_photo'],
            'baseCls' => 'mn-form-tab-body',
            'items'   => array(
                array('xtype'     => 'mn-form-image-panel',
                      'recordId'  => $this->uri->id,
                      'id'        => 'photo',
                      'imageSize' => 'auto',
                      'readOnly'  => $this->state == 'info',
                      'applyImg'  => 'photo-small',
                      'actionUrl' => $this->componentUrl . 'photo/',
                      'dataUrl'   => $this->pathData,
                      'default'   => $this->pathData . 'none_b.png',
                      'image'     => $data['profile_photo'])
            )
        );

        // вкладки
        $tabPanel = array(
            'xtype'             => 'tabpanel',
            'layoutOnTabChange' => true,
            'style'             => 'margin-top:5px;',
            'deferredRender'  => false,
            'activeTab'       => 0,
            'enableTabScroll' => true,
            'anchor'          => '100%',
            'height'          => 287,
            'defaults'        => array('autoScroll' => true),
            'items'           => array($tabContact, $tabAddress, $tabPhoto)
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype'       => 'fieldset',
                  'layout'      => 'column',
                  'hideBorders' => true,
                  'items'       => array(
                      array('width'    => 80,
                            'height'   => 100,
                            'layout'   => 'form',
                            'defaults' => array('hideLabel' => true),
                            'items'    => array(
                                array('xtype'   => 'mn-form-image',
                                      'id'      => 'photo-small',
                                      'image'   => $data['profile_photo'] ? $this->pathData . $data['profile_photo'] : '',
                                      'default' => $this->pathData . 'none_s.png',
                                      'width'   => 80,
                                      'height'  => 100)
                            )
                      ),
                      array('columnWidth' => '1',
                            'style'       => 'margin-left:8px;',
                            'cls'         => 'mn-container-clean',
                            'labelWidth' => 150,
                            'layout'      => 'form',
                            'items'       => array(
                                array('xtype'      => 'textfield',
                                      'fieldLabel' => $this->_['label_profile_name'],
                                      'name'       => 'profile_name',
                                      'maxLength'  => 255,
                                      'anchor'      => '100%',
                                      'allowBlank' => false),
                                array('xtype'      => 'datefield',
                                      'fieldLabel' => $this->_['label_profile_born'],
                                      'name'       => 'profile_born',
                                      'format'     => 'd-m-Y',
                                      'width'      => 100,
                                      'allowBlank' => true),
                                array('xtype'         => 'combo',
                                      'fieldLabel'    => $this->_['label_profile_gender'],
                                      'name'          => 'profile_gender',
                                      'editable'      => false,
                                      'width'         => 100,
                                      'typeAhead'     => true,
                                      'triggerAction' => 'all',
                                      'mode'          => 'local',
                                      'value'         => 1,
                                      'store'         => array(
                                          'xtype'  => 'arraystore',
                                          'fields' => array('value', 'display'),
                                          'data'   => $this->_['data_profile_gender']
                                      ),
                                      'hiddenName'    => 'profile_gender',
                                      'valueField'    => 'value',
                                      'displayField'  => 'display',
                                      'allowBlank'    => false)
                            )
                      )
                  )
            ),
            $tabPanel
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->labelWidth = 100;
        $form->fileUpload = true;
        $form->url = $this->componentUrl . 'profile/';
        $form->items->addItems($items);

        // если показать профиль о себе
        if ($this->uri->getVar('self', false))
            $this->uri->id = $this->session->get('profile/id');

        parent::getInterface();

        $this->store->set('upload', '');
    }
}
?>