<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные для интерфейса профиля записи контингента"
 * Пакет контроллеров "Профиль записи контингента"
 * Группа пакетов     "Контингент"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AContingent_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * анные для интерфейса профиля записи контингента
 * 
 * @category   Gear
 * @package    GController_AContingent_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AContingent_Profile_Data extends GController_Profile_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array(
        'profile_name', 'profile_photo', 'profile_gender', 'profile_born', 'contact_home_phone',
        'contact_mobile_phone', 'contact_work_phone', 'contact_web', 'contact_fax', 'contact_email', 'contact_icq',
        'contact_skype', 'contact_facebook', 'contact_twitter', 'contact_linkedin', 'contact_vk', 'profile_address',
        'profile_address_index', 'profile_address_country', 'profile_address_region', 'profile_address_city'
    );

    /**
     * Первичный ключ таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'profile_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_user_profiles';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_acontingent_profile';

    /**
     * Каталог изображений пользователей
     *
     * @var string
     */
    public $pathData = 'data/photos/';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        if ($this->state == 'update')
            return sprintf($this->_['title_profile_update'], $record['profile_name']);
        else
            return sprintf($this->_['title_profile_info'], $record['profile_name']);
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        if ($this->isInsert) {
            $filename = $this->store->get('upload', '', $this->accessId);
            if ($filename)
                $params['profile_photo'] = $filename;
        }
        if (isset($params['profile_born']))
            if (empty($params['profile_born']) || $params['profile_born'] == 'NaN-NaN-0NaN')
                $params['profile_born'] = null;
            else
                $params['profile_born'] = date('Y-m-d', strtotime($params['profile_born']));
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        Gear::library('File');

        $query = new GDb_Query();
        // удаление изображений пользователей
        $sql = 'SELECT * FROM `gear_user_profiles` WHERE `sys_record`<>1 AND `profile_id`=' . $this->uri->id ;
        $user = $query->getRecord($sql);
        if ($user === false)
            throw new GSqlException();
        if (empty($user))
            throw new GException('Error', 'Unable to delete records!');
        // если есть изображение
        if ($user['profile_photo'])
            GFile::delete($this->pathData . $user['profile_photo']);
        // удаление журнала выхода пользователей
        // удаление записей таблицы "gear_user_escapes"
        $sql = 'DELETE `gear_user_escapes` FROM `gear_user_escapes`,`gear_user_profiles` '
             . 'WHERE `gear_user_escapes`.`user_id`=`gear_user_profiles`.`user_id` AND '
             . '`gear_user_profiles`.`sys_record`<>1 AND '
             . '`gear_user_profiles`.`user_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_user_escapes` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление журнала авторизации пользователей ("gear_user_attends")
        $sql = 'DELETE `gear_user_attends` FROM `gear_user_attends`,`gear_user_profiles` '
             . 'WHERE `gear_user_attends`.`user_id`=`gear_user_profiles`.`user_id` AND '
             . '`gear_user_profiles`.`sys_record`<>1 AND '
             . '`gear_user_profiles`.`user_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_user_attends` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление журнала действий пользователей ("gear_log")
        $sql = 'DELETE `gear_log` FROM `gear_log`,`gear_user_profiles` '
             . 'WHERE `gear_log`.`user_id`=`gear_user_profiles`.`user_id` AND '
             . '`gear_user_profiles`.`sys_record`<>1 AND '
             . '`gear_user_profiles`.`user_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_log` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление аккаунтов пользователей ("gear_users")
        $sql = 'DELETE `gear_users` FROM `gear_users`,`gear_user_profiles` '
             . 'WHERE `gear_users`.`user_id`=`gear_user_profiles`.`user_id` AND '
             . '`gear_user_profiles`.`sys_record`<>1 AND '
             . '`gear_user_profiles`.`user_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_users` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Событие наступает после успешного удаления всех данных
     * 
     * @return void
     */
    protected function dataClearComplete()
    {
        // обновление пользователей в кэше
        if ($this->config->get('USERS/PRELOADABLE'))
            GFactory::getUsers()->toCache();
    }

    /**
     * Событие наступает после успешного удаления данных
     * 
     * @return void
     */
    protected function dataDeleteComplete()
    {
        // обновление пользователей в кэше
        if ($this->config->get('USERS/PRELOADABLE'))
            GFactory::getUsers()->toCache();
    }

    /**
     * Событие наступает после успешного обновления данных
     * 
     * @param array $data сформированные данные полученные после запроса к таблице
     * @return void
     */
    protected function dataUpdateComplete($data = array())
    {
        // обновление пользователей в кэше
        if ($this->config->get('USERS/PRELOADABLE'))
            GFactory::getUsers()->toCache();
    }

    /**
     * Событие наступает после успешного добавления данных
     * 
     * @param array $data сформированные данные полученные после запроса к таблице
     * @return void
     */
    protected function dataInsertComplete($data = array())
    {
        // обновление пользователей в кэше
        if ($this->config->get('USERS/PRELOADABLE'))
            GFactory::getUsers()->toCache();
    }

    /**
     * Проверка существования записи с одинаковыми значениями
     * 
     * @param  array $params array (field => value, ....)
     * @return void
     */
    protected function isDataExist($params)
    {
        if (isset($params['profile_name'])) {
            $table = new GDb_Table($this->tableName, $this->idProperty);
            if ($table->isDataExist(array('profile_name' => $params['profile_name']), (int)$this->uri->id) === true)
                throw new GException('Warning', 'Record the values of fields:%s already exists!#"' . $params['profile_name'] . '"');
        }
    }
}
?>