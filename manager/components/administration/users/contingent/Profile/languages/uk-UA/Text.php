<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Створення запису "Контингент"',
    'title_profile_update' => 'Зміна запису "%s"',
    'title_profile_info'   => 'Детали записи "%s"',
    // поля формы
    'label_profile_name'         => 'Фамилия Имя Отчество',
    'label_profile_gender'       => 'Пол',
    'label_profile_born'         => 'Дата рождения',
    'title_tab_photo'            => 'Фотография',
    'label_photo_file'           => 'Изображенние (формат JPEG)',
    'label_photo_action'         => 'Действие',
    'text_photo_file'            => 'выбрать...',
    'title_tab_contact'          => 'Контакт',
    'title_fieldset_connection'  => 'Средства связи',
    'label_contact_work_phone'   => 'Рабочий телефон',
    'label_contact_mobile_phone' => 'Мобильный телефон',
    'label_contact_home_phone'   => 'Домашний телефон',
    'title_fieldset_net'         => 'Социальные сети',
    'title_tab_address'          => 'Адрес',
    'label_address'              => 'Адрес',
    'label_address_index'        => 'Индекс',
    'label_address_country'      => 'Страна',
    'label_address_region'       => 'Область / штат',
    'label_address_city'         => 'Город',
    // типы
    'data_photo_action'   => array('змінити зображення', 'видалити зображення', 'додати зображення'),
    'data_profile_gender' => array(array(0, 'жіночий'), array(1, 'чоловічий'))
);
?>