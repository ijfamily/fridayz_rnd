<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка контингента"
 * Пакет контроллеров "Список контингента"
 * Группа пакетов     "Контингент"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AContingent_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');
Gear::library('File');

/**
 * Данные списка контингента
 * 
 * @category   Gear
 * @package    GController_AContingent_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AContingent_Grid_Data extends GController_Grid_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * (для обработки записей таблицы)
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'profile_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_user_profiles';

    /**
     * Путь к фоткам
     *
     * @var string
     */
    public  $pathData = 'data/photos/';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS *, IF(`user_id` IS NULL, 0, 1) `account` '
          . 'FROM `gear_user_profiles` WHERE 1 %filter ORDER BY %sort LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // имя пользователя
            'profile_name' => array('type' => 'string'),
            // фото пользователя
            'profile_photo' => array('type' => 'string'),
            'user_id' => array('type' => 'integer'),
            // пол
            'profile_gender' => array('type' => 'string'),
            // дата рождения
            'profile_born' => array('type' => 'string'),
            // телефон домашний
            'contact_home_phone' => array('type' => 'string'),
            // телефон мобильный
            'contact_mobile_phone' => array('type' => 'string'),
            // телефон рабочий
            'contact_work_phone' => array('type' => 'string'),
            // e-mail
            'contact_email' => array('type' => 'string'),
            // факс
            'contact_fax' => array('type' => 'string'),
            // icq
            'contact_icq' => array('type' => 'string'),
            // skype
            'contact_skype' => array('type' => 'string'),
            // web сайт
            'contact_web' => array('type' => 'string'),
            // facebook
            'contact_facebook' => array('type' => 'string'),
            // twitter
            'contact_twitter' => array('type' => 'string'),
            // lnkedin
            'contact_linkedin' => array('type' => 'string'),
            // vk
            'contact_vk' => array('type' => 'string'),
            // адрес
            'profile_address' => array('type' => 'string'),
            'profile_address_index' => array('type' => 'string'),
            'profile_address_country' => array('type' => 'string'),
            'profile_address_region' => array('type' => 'string'),
            'profile_address_city' => array('type' => 'string'),
            // аккаунт
            'account' => array('type' => 'integer')
        );
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        Gear::library('File');

        $query = new GDb_Query();
        // удаление изображений пользователей
        GDir::clear($this->pathData, array('none_b.png', 'none_bb.png', 'none_s.png', 'none_sb.png'));
        // удаление журнала выхода пользователей ("gear_user_escapes")
        $sql = 'DELETE `gear_user_escapes` FROM `gear_user_escapes`,`gear_users` '
             . 'WHERE `gear_user_escapes`.`user_id`=`gear_users`.`user_id` AND '
             . '`gear_users`.`sys_record`<>1';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_user_escapes` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление журнала авторизации пользователей ("gear_user_attends")
        $sql = 'DELETE `gear_user_attends` FROM `gear_user_attends`,`gear_users` '
             . 'WHERE `gear_user_attends`.`user_id`=`gear_users`.`user_id` AND '
             . '`gear_users`.`sys_record`<>1';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_user_attends` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление журнала действий пользователей ("gear_log")
        $sql = 'DELETE `gear_log` FROM `gear_log`,`gear_users` '
             . 'WHERE `gear_log`.`user_id`=`gear_users`.`user_id` AND '
             . '`gear_users`.`sys_record`<>1';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_log` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление учётных записей пользователей ("gear_users")
        $sql = 'DELETE FROM `gear_users` '
             . 'WHERE `sys_record`<>1';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_users` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление профилей пользователей ("gear_user_profiles")
        $sql = 'DELETE FROM `gear_user_profiles` '
             . 'WHERE `sys_record`<>1';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_user_profiles` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Событие наступает после успешного удаления данных
     * 
     * @return void
     */
    protected function dataDeleteComplete()
    {
        // обновление пользователей в кэше
        if ($this->config->get('USERS/PRELOADABLE'))
            GFactory::getUsers()->toCache();
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        $query = new GDb_Query();
        // удаление изображений пользователей
        $sql = 'SELECT * FROM `gear_user_profiles` WHERE `sys_record`<>1 AND '
             . '`profile_id` IN (' . $this->uri->id . ')';
        $query->execute($sql);
        while (!$query->eof()) {
            $user = $query->next();
            // если есть изображение
            if ($user['profile_photo'])
                GFile::delete($this->pathData . $user['profile_photo']);
        }
        // удаление журнала выхода пользователей ("gear_user_escapes")
        $sql = 'DELETE `gear_user_escapes` FROM `gear_user_escapes`,`gear_user_profiles` '
             . 'WHERE `gear_user_escapes`.`user_id`=`gear_user_profiles`.`user_id` AND '
             . '`gear_user_profiles`.`sys_record`<>1 AND '
             . '`gear_user_profiles`.`user_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_user_escapes` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление журнала авторизации пользователей ("gear_user_attends")
        $sql = 'DELETE `gear_user_attends` FROM `gear_user_attends`,`gear_user_profiles` '
             . 'WHERE `gear_user_attends`.`user_id`=`gear_user_profiles`.`user_id` AND '
             . '`gear_user_profiles`.`sys_record`<>1 AND '
             . '`gear_user_profiles`.`user_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_user_attends` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление журнала действий пользователей ("gear_log")
        $sql = 'DELETE `gear_log` FROM `gear_log`,`gear_user_profiles` '
             . 'WHERE `gear_log`.`user_id`=`gear_user_profiles`.`user_id` AND '
             . '`gear_user_profiles`.`sys_record`<>1 AND '
             . '`gear_user_profiles`.`user_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_log` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление аккаунтов пользователей ("gear_users")
        $sql = 'DELETE `gear_users` FROM `gear_users`,`gear_user_profiles` '
             . 'WHERE `gear_users`.`user_id`=`gear_user_profiles`.`user_id` AND '
             . '`gear_user_profiles`.`sys_record`<>1 AND '
             . '`gear_user_profiles`.`user_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_users` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }


    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        // пол
        $record['profile_gender'] = $this->_['data_gender'][$record['profile_gender']];
        // если пользователь существует
        if (empty($record['profile_photo']))
            $record['profile_photo'] = 'none_s.png';
        $record['profile_photo'] = $this->pathData . $record['profile_photo'];

        return $record;
    }
}
?>