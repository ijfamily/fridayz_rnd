<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка контингента"
 * Пакет контроллеров "Список контингента"
 * Группа пакетов     "Контингент"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AContingent_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс списка контингента
 * 
 * @category   Gear
 * @package    GController_AContingent_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AContingent_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * (для обработки записей таблицы)
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'profile_id';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'profile_name';

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // имя пользователя
            array('name' => 'profile_name', 'type' => 'string'),
            // фото пользователя
            array('name' => 'profile_photo', 'type' => 'string'),
            // пол
            array('name' => 'profile_gender', 'type' => 'string'),
            // дата рождения
            array('name' => 'profile_born', 'type' => 'string'),
            // телефон домашний
            array('name' => 'contact_home_phone', 'type' => 'string'),
            // телефон мобильный
            array('name' => 'contact_mobile_phone', 'type' => 'string'),
            // телефон рабочий
            array('name' => 'contact_work_phone', 'type' => 'string'),
            // e-mail
            array('name' => 'contact_email', 'type' => 'string'),
            // факс
            array('name' => 'contact_fax', 'type' => 'string'),
            // icq
            array('name' => 'contact_icq', 'type' => 'string'),
            // skype
            array('name' => 'contact_skype', 'type' => 'string'),
            // web сайт
            array('name' => 'contact_web', 'type' => 'string'),
            // facebook
            array('name' => 'contact_facebook', 'type' => 'string'),
            // twitter
            array('name' => 'contact_twitter', 'type' => 'string'),
            // lnkedin
            array('name' => 'contact_linkedin', 'type' => 'string'),
            // vk
            array('name' => 'contact_vk', 'type' => 'string'),
            // адрес
            array('name' => 'profile_address', 'type' => 'string'),
            array('name' => 'profile_address_index', 'type' => 'string'),
            array('name' => 'profile_address_country', 'type' => 'string'),
            array('name' => 'profile_address_region', 'type' => 'string'),
            array('name' => 'profile_address_city', 'type' => 'string'),
            // аккаунт
            array('name' => 'account', 'type' => 'integer')
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $settings = $this->session->get('user/settings');
        $this->columns = array(
            array('xtype'     => 'expandercolumn',
                  'url'       => $this->componentUrl . 'grid/expand/',
                  'typeCt'    => 'row'),
            array('xtype'     => 'numbercolumn'),
            array('xtype'     => 'rowmenu'),
            array('dataIndex' => 'profile_name',
                  'header'    => '<em class="mn-grid-hd-details"></em>' . $this->_['header_profile_name'],
                  'width'     => 190,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('xtype'     => 'datetimecolumn',
                  'dataIndex' => 'profile_born',
                  'frmDate'   => $settings['format/date'],
                  'header'    => $this->_['header_profile_born'],
                  'tooltip'   => $this->_['tooltip_profile_born'],
                  'width'     => 75,
                  'sortable'  => true),
            array('dataIndex' => 'profile_gender',
                  'header'    => $this->_['header_profile_gender'],
                  'width'     => 60,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'contact_work_phone',
                  'header'    => $this->_['header_contact_work_phone'],
                  'tooltip'   => $this->_['tooltip_contact_work_phone'],
                  'width'     => 130,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'contact_mobile_phone',
                  'header'    => $this->_['header_contact_mobile_phone'],
                  'tooltip'   => $this->_['tooltip_contact_mobile_phone'],
                  'hidden'    => true,
                  'width'     => 130,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'contact_home_phone',
                  'header'    => $this->_['header_contact_home_phone'],
                  'tooltip'   => $this->_['tooltip_contact_home_phone'],
                  'hidden'    => true,
                  'width'     => 130,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'contact_email',
                  'header'    => $this->_['header_contact_email'],
                  'width'     => 160,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'contact_icq',
                  'header'    => $this->_['header_contact_icq'],
                  'hidden'    => true,
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'contact_skype',
                  'header'    => $this->_['header_contact_skype'],
                  'width'     => 160,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'contact_web',
                  'header'    => $this->_['header_contact_web'],
                  'hidden'    => true,
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'contact_facebook',
                  'header'    => $this->_['header_contact_facebook'],
                  'hidden'    => true,
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'contact_linkedin',
                  'header'    => $this->_['header_contact_linkedin'],
                  'hidden'    => true,
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'contact_twitter',
                  'header'    => $this->_['header_contact_twitter'],
                  'hidden'    => true,
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'contact_vk',
                  'header'    => 'VK',
                  'tooltip'   => $this->_['tooltip_contact_vk'],
                  'hidden'    => true,
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'profile_address_index',
                  'header'    => $this->_['header_profile_address_index'],
                  'hidden'    => true,
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'profile_address_country',
                  'header'    => $this->_['header_profile_address_country'],
                  'hidden'    => true,
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'profile_address_region',
                  'header'    => $this->_['header_profile_address_region'],
                  'hidden'    => true,
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'profile_address_city',
                  'header'    => $this->_['header_profile_address_city'],
                  'hidden'    => true,
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'profile_address',
                  'header'    => $this->_['header_profile_address'],
                  'hidden'    => true,
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('xtype'     => 'booleancolumn',
                  'dataIndex' => 'account',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-account.png" align="absmiddle">',
                  'tooltip'   => $this->_['tooltip_account'],
                  'align'     => 'center',
                  'width'     => 45,
                  'sortable'  => true)
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_grid'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_grid'],
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'profileUrl'    => $this->componentUrl . 'profile/')
        );

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "edit" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "добавить" (ExtJS class "Manager.button.InsertData")
        $group->items->add(array('xtype' => 'mn-btn-insert-data', 'gridId' => $this->classId));
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' => $this->classId));
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array(
                'xtype'      => 'mn-btn-clear-data',
                'msgConfirm' => $this->_['msg_btn_clear'],
                'gridId'     => $this->classId,
                'isN'        => true
            )
        );
        $this->_cmp->tbar->items->add($group);
        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = 'component-users-contingent';
        $this->_cmp->tbar->items->add($group);

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $img = '<div class="icon-photo-s" style="background-image: url(\'{profile_photo}\')"></div>';
        $cellInfo =
            '<div class="mn-grid-cell-tooltip-tl">{profile_name}</div>'
          . '<table class="mn-grid-cell-tooltip" cellpadding="0" cellspacing="5" border="0">'
          . '<tr><td valign="top">' . $img . '</td><td valign="top">'
          . '<em>' . $this->_['header_profile_born'] . '</em>: <b>{profile_born}</b><br>'
          . '<em>' . $this->_['header_profile_gender'] . '</em>: <b>{profile_gender}</b><br>'
          . '<em>' . $this->_['header_contact_work_phone'] . '</em>: <b>{contact_work_phone}</b><br>'
          . '<em>' . $this->_['header_contact_mobile_phone'] . '</em>: <b>{contact_mobile_phone}</b><br>'
          . '<em>' . $this->_['header_contact_home_phone'] . '</em>: <b>{contact_home_phone}</b><br>'
          . '<em>' . $this->_['header_contact_email'] . '</em>: <b>{contact_email}</b><br>'
          . '<em>' . $this->_['header_contact_icq'] . '</em>: <b>{contact_icq}</b><br>'
          . '<em>' . $this->_['header_contact_skype'] . '</em>: <b>{contact_skype}</b><br>'
          . '<em>' . $this->_['header_contact_web'] . '</em>: <b>{contact_web}</b><br>'
          . '<em>Twitter</em>: <b>{contact_twitter}</b><br>'
          . '<em>Facebook</em>: <b>{contact_facebook}</b><br>'
          . '<em>LinkedIn</em>: <b>{contact_linkedin}</b><br>'
          . '<em>VK</em>: <b>{contact_vk}</b><br>'
          . '<em>' . $this->_['header_profile_address_index'] . '</em>: <b>{profile_address_index}</b><br>'
          . '<em>' . $this->_['header_profile_address_country'] . '</em>: <b>{profile_address_country}</b><br>'
          . '<em>' . $this->_['header_profile_address_region'] . '</em>: <b>{profile_address_region}</b><br>'
          . '<em>' . $this->_['header_profile_address_city'] . '</em>: <b>{profile_address_city}</b><br>'
          . '<em>' . $this->_['header_profile_address'] . '</em>: <b>{profile_address}</b>'
          . '</td></tr></table>';
        $this->_cmp->cellTips = array(
            array('field' => 'profile_name', 'tpl' => $cellInfo),
            array('field' => 'contact_facebook', 'tpl' => '{contact_facebook}'),
            array('field' => 'contact_twitter', 'tpl' => '{contact_twitter}'),
            array('field' => 'contact_linkedin', 'tpl' => '{contact_linkedin}'),
            array('field' => 'contact_vk', 'tpl' => '{contact_vk}'),
            array('field' => 'profile_address_country', 'tpl' => '{profile_address_country}'),
            array('field' => 'profile_address_region', 'tpl' => '{profile_address_region}'),
            array('field' => 'profile_address_city', 'tpl' => '{profile_address_city}'),
            array('field' => 'profile_address', 'tpl' => '{profile_address}')
        );
        $this->_cmp->cellTipConfig = array('maxWidth' => 450);

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $this->_cmp->rowMenu = array(
            'xtype' => 'mn-grid-rowmenu',
            'items' => array(
                array('text'    => $this->_['rowmenu_edit'],
                      'iconCls' => 'icon-form-edit',
                      'url'     => $this->componentUrl . 'profile/interface/')
            )
        );

        parent::getInterface();
    }
}
?>