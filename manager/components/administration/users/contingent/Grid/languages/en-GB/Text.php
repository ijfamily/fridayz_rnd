<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Contingent',
    'tooltip_grid' => 'список пользователей без аккаунтов',
    'rowmenu_edit' => 'Edit',
    // панель управления
    'title_buttongroup_edit'   => 'Edit',
    'title_buttongroup_cols'   => 'Columns',
    'title_buttongroup_filter' => 'Filter',
    'msg_btn_clear'            => 'Do you really want to delete all entries <span class="mn-msg-delete"> '
                                . '("Contingent", "Users of the system", "User logs ...")</span> ?',
    // столбцы
    'header_profile_name'          => 'Фамилия Имя Отчество',
    'header_profile_born'          => 'Дата р.',
    'tooltip_profile_born'         => 'Дата рождения',
    'header_profile_gender'        => 'Пол',
    'header_contact_home_phone'    => 'Домашний телефон',
    'tooltip_contact_home_phone'   => 'Домашний телефон',
    'header_contact_mobile_phone'  => 'Мобильный телефон',
    'tooltip_contact_mobile_phone' => 'Мобильный телефон',
    'header_contact_work_phone'    => 'Рабочий телефон',
    'tooltip_contact_work_phone'   => 'Рабочий телефон',
    'header_contact_email'         => 'E-mail',
    'header_contact_skype'         => 'Skype',
    'header_contact_facebook'      => 'Facebook',
    'header_contact_twitter'       => 'Twitter',
    'header_contact_linkedin'      => 'LinkedIn',
    'header_contact_icq'           => 'ICQ',
    'header_contact_web'           => 'Web',
    'tooltip_contact_vk'           => 'Вконтакте',
    'header_profile_address_index' => 'Индекс',
    'header_profile_address_country' => 'Страна',
    'header_profile_address_region'  => 'Область',
    'header_profile_address_city'    => 'Город',
    'header_profile_address'         => 'Адрес',
    'header_account'                 => 'Аккаунт',
    'tooltip_account'                => 'Учётная запись пользователя',
    // типы
    'data_gender' => array('female', 'male'),
    // развернутая запись
    'label_profile_name'         => 'Фамилия Имя Отчество',
    'label_profile_gender'       => 'Пол',
    'label_profile_born'         => 'Дата рождения',
    'title_fieldset_connection'  => 'Средства связи',
    'label_contact_work_phone'   => 'Рабочий телефон',
    'label_contact_mobile_phone' => 'Мобильный телефон',
    'label_contact_home_phone'   => 'Домашний телефон',
    'title_fieldset_net'         => 'Социальные сети',
    'title_fieldset_address'     => 'Адрес',
    'label_address'              => 'Адрес',
    'label_address_index'        => 'Индекс',
    'label_address_country'      => 'Страна',
    'label_address_region'       => 'Область / штат',
    'label_address_city'         => 'Город'
);
?>