<?php
/**
 * Gear Manager
 *
 * Controller         "Изменение записи доступной категории сайта"
 * Пакет контроллеров "Список доступных категорий сайта"
 * Группа пакетов     "Группы пользователей системы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SCategoriesAccess_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Field.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Field');

/**
 * Изменение записи доступной категории сайта
 * 
 * @category   Gear
 * @package    GController_SCategoriesAccess_Profile
 * @subpackage Field
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Field.php 2014-08-04 12:00:00 Gear Magic$
 */
final class GController_SCategoriesAccess_Profile_Field extends GController_Profile_Field
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array('share');

    /**
     * Первичное поле таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'access_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_categories_access';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_agroups_grid';

    /**
     * Идентификатор группы пользователя
     *
     * @var integer
     */
    public $groupId = 0;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        $this->groupId = $this->store->get('record', 0, 'gcontroller_agroups_grid');
    }

    /**
     * Открыть доступ к категории
     * 
     * @param integer $access открыть доступ
     * @return void
     */
    protected function setShare($access)
    {
        $query = new GDb_Query();
        if ($access) {
            $sql = 'INSERT  INTO `site_categories_access` (`group_id`, `category_id`) VALUES (' . $this->groupId . ', ' . $this->uri->id . ')';
            if ($query->execute($sql) === false)
                throw new GSqlException();
        } else {
            $sql = 'DELETE FROM `site_categories_access` WHERE `group_id`=' . $this->groupId . ' AND `category_id`=' . $this->uri->id;
            if ($query->execute($sql) === false)
                throw new GSqlException();
        }
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        parent::dataAccessUpdate();

        if ($this->input->isEmpty('PUT'))
            throw new GException('Warning', 'To execute a query, you must change data!');
        // массив полей с их соответствующими значениями
        if (empty($params))
            $params = $this->input->getBy($this->fields);
        // проверка параметра в запросе
        if (!isset($params['share']))
            throw new GException('Warning', $this->_['msg_no_param']);

        $this->setShare((int) $params['share']);
    }
}
?>