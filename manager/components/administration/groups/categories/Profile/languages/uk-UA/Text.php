<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Створення запису "Доступний компонент"',
    'title_profile_update' => 'зміна запису "%s"',
    // поля формы
    'title_fldset_cmp'      => 'Компонент',
    'title_fldset_debug'    => 'Налагодження',
    'label_access_shortcut' => 'Ярлик',
    'tip_access_shortcut'   => 'Ярлик на робочому столі',
    'label_controller_name' => 'Назва',
    'label_access_log'      => 'Журнал',
    'label_access_debug'    => 'Налагодження',
    'label_access_error'    => 'Помилки',
    'tip_access_log'        => 'Вести журнал подій скоєних компонентом у вигляді призначених привілеїв групі користувача',
    'tip_access_debug'      => 'Використовувати відладчик Терміналу для перегляд всіх дій користувача (необхідний FireBug)',
    'tip_access_error'      => 'Висновок детальних помилок компонента',
    // сообщения
    'msg_already_added' => 'В выбранной Вами группе пользователя уже добавлены все компоненты!'
);
?>