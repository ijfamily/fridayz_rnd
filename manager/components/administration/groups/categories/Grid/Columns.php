<?php
/**
 * Gear Manager
 *
 * Контроллер         "Столбцы списка доступных категорий сайта"
 * Пакет контроллеров "Список доступных категорий сайта"
 * Группа пакетов     "Группы пользователей системы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SCategoriesAccess_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Columns.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Columns');

/**
 * Столбцы списка доступных категорий сайта
 * 
 * @category   Gear
 * @package    GController_SCategoriesAccess_Grid
 * @subpackage Columns
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Columns.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SCategoriesAccess_Grid_Columns extends GController_Grid_Columns
{}
?>