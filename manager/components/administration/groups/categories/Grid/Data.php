<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка доступных категорий сайта"
 * Пакет контроллеров "Список доступных категорий сайта"
 * Группа пакетов     "Группы пользователей системы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_SCategoriesAccess_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные списка доступных категорий сайта
 * 
 * @category   Gear
 * @package    GController_SCategoriesAccess_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_SCategoriesAccess_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'category_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'site_categories';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'category_name';

    /**
     * Домены
     *
     * @var array
     */
    public $domains = array();

    /**
     * Управления сайтами на других доменах
     *
     * @var boolean
     */
    public $isOutControl = false;

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_agroups_grid';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // управление сайтами на других доменах
        $this->isOutControl = $this->config->getFromCms('Site', 'OUTCONTROL');
        if ($this->isOutControl)
            $this->domains = $this->config->getFromCms('Domains', 'indexes');
        $this->domains[0] = array($_SERVER['SERVER_NAME'], $this->config->getFromCms('Site', 'NAME'));

        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS `c`.*, `cp`.`category_name` `pcategory_name`, `cp`.`category_left` `pcategory_left`, IF(`a`.`access_id`>0, 1, 0) `share`'
          . 'FROM `site_categories` `c` JOIN `site_categories` `cp` '
          . 'ON `c`.`category_left` > `cp`.`category_left` AND `cp`.`category_right` > `c`.`category_right` AND `cp`.`category_level` = `c`.`category_level`-1 '
          . 'LEFT JOIN `site_categories_access` `a` ON `a`.`category_id`=`c`.`category_id` AND `a`.`group_id`=' . $this->_recordId
          . ' WHERE 1 %filter ';
        // фильтр из панели инструментов
        $filter = $this->store->get('tlbFilter', false);
        if ($this->isOutControl) {
            if ($filter) {
                $domainId = (int) $filter['domain'];
                if ($domainId >= 0)
                    $this->sql .= ' AND `c`.`domain_id`=' . $domainId;
            }
        }
        $this->sql .= ' ORDER BY %sort LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // название сайта
            'domain_id' => array('type' => 'string'),
            // название
            'category_name' => array('type' => 'string', 'as' => 'c.category_name'),
            'category_title' => array('type' => 'string'),
            // название предка
            'pcategory_name' => array('type' => 'string', 'as' => 'cp.category_name'),
            // показывать
            'category_visible' => array('type' => 'integer'),
            // адрес
            'category_uri' => array('type' => 'string', 'as' => 'c.category_uri'),
            'category_url' => array('type' => 'string'),
            'goto_url' => array('type' => 'string'),
            // атрибуты дерева
            'pcategory_left' => array('type' => 'integer'),
            'category_level' => array('type' => 'integer'),
            'category_left' => array('type' => 'integer'),
            'category_right' => array('type' => 'integer'),
            // доступ
            'share' => array('type' => 'integer'),
        );
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        $query = new GDb_Query();
        $sql = 'DELETE FROM `site_categories_access` WHERE `group_id`=' . $this->store->get('record', 0, 'gcontroller_agroups_grid');
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $this->response->set('action', 'delete');
        $this->response->setMsgResult($this->_['title_data_access'], $this->_['msg_full_access'], true);
    }

    /**
     * Возращает поле сортировик
     * 
     * @return string
     */
    protected function getSortField()
    {
        $s = parent::getSortField();
        if ($s != 'category_left')
            $s = 'category_left ASC ,' . $s;

        return $s;
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        $domain = $_SERVER['SERVER_NAME'];
        // сайты на других доменах
        if (isset($this->domains[$record['domain_id']])) {
            $domain = $this->domains[$record['domain_id']][0];
            $record['domain_id'] = $this->domains[$record['domain_id']][1];
        } else
            $record['domain_id'] = '';

        $isParent = ($record['category_right'] - $record['category_left']) > 1;

        $record['category_title'] = $record['category_name'];
        $record['category_name'] = '<span' . ($isParent ? ' class="mn-tree-row-collapsed"' : '') . ' style="padding-left:' . (($record['category_level'] - 1) * 11) . 'px"></span>' . $record['category_name'];
        if ($record['category_left'] - 2 < 2)
            $record['category_step'] = 1;
        else
            $record['category_step'] = ($record['category_left'] - 2) / 2;
        // если категории не отображаются
        if (empty($record['category_visible']))
            $record['rowCls'] = 'mn-row-notpublished';
        // подсказки для ЧПУ URL категорий
        $url = 'http://' . $domain . '/';
        if ($record['category_uri'] != '/')
            $url .= $record['category_uri'];
        $record['category_url'] = $url;
        if ($record['domain_id'])
            $record['goto_url'] = '<a href="'. $url . '" target="_blank" title="' . $this->_['tooltip_goto_url'] . '"><img src="' . $this->resourcePath . 'icon-goto.png"></a>';
        else
            $record['goto_url'] = '';

        return $record;
    }
}
?>