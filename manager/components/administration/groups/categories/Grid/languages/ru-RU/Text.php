<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'    => 'Доступные категории для группы "%s"',
    'tooltip_grid'  => 'список категорий статей сайта доступных группе пользователя для изменений',
    'rowmenu_edit'  => 'Редактировать',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете предоставить полный доступ группе пользователя к категориям статей?',
    // столбцы
    'header_site_name'         => 'Сайт',
    'header_category_name'     => 'Название',
    'header_pcategory_name'    => 'Название "предка"',
    'header_category_uri'      => 'ЧПУ URL категории',
    'tooltip_share'            => 'Категория доступна группе',
    'text_root_category'       => 'Основная категория',
    'label_domain'             => 'Сайт',
    'tooltip_goto_url'         => 'Просмотр категории сайта',
    // тип данных
    'data_boolean' => array('нет', 'да'),
    // сообщения
    'title_data_access' => 'Полный доступ',
    'msg_full_access'   => 'группе пользователя доступны все категории статей',
);
?>