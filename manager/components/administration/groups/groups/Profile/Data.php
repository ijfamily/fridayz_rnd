<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные для интерфейса профиля группы пользователя
 * Пакет контроллеров "Профиль группы пользователя"
 * Группа пакетов     "Группы пользователей"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AGroups_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные для интерфейса профиля группы пользователя
 * 
 * @category   Gear
 * @package    GController_AGroups_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AGroups_Profile_Data extends GController_Profile_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Массив полей таблицы ($_tableName)
     *
     * @var array
     */
    public $fields = array(
        'group_id', 'group_name', 'group_shortname', 'group_about', 'group_logo_woman', 'group_logo_man'
    );

    /**
     * Первичный ключ таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'group_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_user_groups';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_agroups_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_profile_update'], $record['group_name']);
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        parent::dataView($sql);

        unset($this->response->data['group_id']);
    }

    /**
     * Вставка данных
     * 
     * @param  array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataInsert($params = array())
    {
        parent::dataAccessInsert();

        if ($this->input->isEmpty('POST'))
            throw new GException('Warning', 'To execute a query, you must change data!');

        // массив полей с их соответствующими значениями
        if (empty($params))
            $params = $this->input->getBy($this->fields);
        // проверки входных данных
        $this->isDataCorrect($params);
        // проверка существования записи с одинаковыми значениями
        $this->isDataExist($params);
        // использование системных полей в запросе SQL
        if ($this->isSysFields) {
            $params['sys_date_insert'] = date('Y-m-d');
            $params['sys_time_insert'] = date('H:i:s');
            $params['sys_user_insert'] = $this->session->get('user_id');
        }
        Gear::library('Db/Drivers/MySQL/Tree/Tree');

        // указатель на базу данных
        $db = GFactory::getDb();
        $tree = new GDb_Tree($this->tableName, 'group', $db->getHandle());
        $parentNode = $this->input->get('group_id');
        // добавление записи
        $this->_recordId = $tree->insert($parentNode, '', $params);
        if (!empty($tree->ERRORS_MES)) {
            $error = implode(',', $tree->ERRORS_MES);
            throw new GException('Data processing error', 'Query error (Internal Server Error)', $error, false);
        }
        // добавление компонентов группе от предка с привилегиями
        $query = new GDb_Query();
        $newGroupId = $query->getLastInsertId();
        $sql = 'INSERT INTO `gear_controller_access` '
             . '(`group_id`,`controller_id`,`access_shortcut`,`access_privileges`,`access_fields`,`access_log`,`access_debug`,`access_error`,'
             . '`sys_record`,`sys_date_update`,`sys_date_insert`,`sys_time_update`,`sys_time_insert`,`sys_user_update`,`sys_user_insert`) '
             . 'SELECT ' . $newGroupId . ',`controller_id`,`access_shortcut`,`access_privileges`,`access_fields`,`access_log`,`access_debug`,`access_error`,'
             . "`sys_record`,null,'" . date('Y-m-d') . "',null,'" . date('H:i:s') . "',null," . $this->session->get('user_id')
             . ' FROM `gear_controller_access` WHERE `group_id`=' . $parentNode;
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        parent::dataAccessUpdate();

        if ($this->input->isEmpty('PUT'))
            throw new GException('Warning', 'To execute a query, you must change data!');

        $table = new GDb_Table($this->tableName, $this->idProperty, $this->fields);
        // массив полей с их соответствующими значениями
        if (empty($params))
            $params = $this->input->getBy($this->fields);
        // проверки входных данных
        $this->isDataCorrect($params);
        // cпроверка существования записи с одинаковыми значениями
        $this->isDataExist($params);
        // выбранная группа
        $group = $table->getRecord($this->uri->id);
        // идентификатор группы компонентов
        $groupId = $this->input->get('group_id');
        if ($groupId)
            $groupId = is_numeric($groupId) ? $groupId : $group['group_id'];
        else
            $groupId = $group['group_id'];
        // использование системных полей в запросе SQL
        if ($this->isSysFields) {
            $params['sys_date_update'] = date('Y-m-d');
            $params['sys_time_update'] = date('H:i:s');
            $params['sys_user_update'] = $this->session->get('user_id');
        }
        $table->update($params, $this->uri->id);
        // смена узла дерева
        if ($group['group_id'] != $groupId) {
            Gear::library('Db/Drivers/MySQL/Tree/Tree');

            // указатель на базу данных
            $db = GFactory::getDb();
            $tree = new GDb_Tree($this->tableName, 'group', $db->getHandle());
            $tree->moveAll($group['group_id'], $groupId);
            if (!empty($tree->ERRORS_MES)) {
                $error = implode(',', $tree->ERRORS_MES);
                throw new GException('Data processing error', 'Query error (Internal Server Error)', $error, false);
            }
        }
    }

    /**
     * Удаление данных
     * 
     * @return void
     */
    protected function dataDelete()
    {
        parent::dataAccessDelete();

        // определяем откуда id брать
        if (empty($this->uri->id)) {
            $input = GFactory::getInput();
            $this->uri->id = $input->get('id');
        }
        // если не указан $id
        if (empty($this->uri->id))
            throw new GException('Error', 'Unable to delete records!');

        // проверка существования зависимых записей
        $this->isDataDependent();

        Gear::library('Db/Drivers/MySQL/Tree/Tree');

        // указатель на базу данных
        $db = GFactory::getDb();
        // удаление групп пользователей ("gear_user_groups")
        $tree = new GDb_Tree($this->tableName, 'group', $db->getHandle());
        if ($tree->deleteAll((int)$this->uri->id, '') === false)
            throw new GException('Data processing error', 'Query error (Internal Server Error)', $tree->ERRORS_MES[0], false);
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        $query = new GDb_Query();
        // выбранная группа пользователя
        $groupId = (int) $this->uri->id;
        $sql = 'SELECT * FROM `gear_user_groups` WHERE `group_id`=' . $groupId;
        $group = $query->getRecord($sql);
        if ($group === false)
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($group['sys_record'] == 1)
            throw new GException('Error', 'Unable to delete records!');
        $error = array();
        // если в группе есть потомки
        $count = ($group['group_right'] - $group['group_left'] - 1) / 2;
        if ($count > 0)
            $error[] = sprintf($this->_['data_depends'][0], $count);
        // если есть зависимости
        if ($error)
            throw new GException('Deleting data', 'There is a correlation record for deletion', implode(', ', $error));

        // удаление журнала выхода пользователей ("gear_user_escapes")
        $sql = 'DELETE `e` FROM `gear_user_escapes` `e`,`gear_users` `u` '
             . 'WHERE `e`.`user_id`=`u`.`user_id` AND `u`.`sys_record`<>1 AND `u`.`group_id`=' . $groupId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_user_escapes` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление журнала авторизации пользователей ("gear_user_attends")
        $sql = 'DELETE `a` FROM `gear_user_attends` `a`,`gear_users` `u` '
             . 'WHERE `a`.`user_id`=`u`.`user_id` AND `u`.`sys_record`<>1 AND `u`.`group_id`=' . $groupId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_user_attends` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление журнала действий пользователей ("gear_log")
        $sql = 'DELETE `l` FROM `gear_log` `l`,`gear_users` `u` '
             . 'WHERE `l`.`user_id`=`u`.`user_id` AND `u`.`sys_record`<>1 AND `u`.`group_id`=' . $groupId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_log` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление учётных записей пользователей ("gear_users")
        $sql = 'DELETE FROM `gear_users` WHERE `sys_record`<>1 AND `group_id`=' . $groupId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_users` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // обновление профилей пользователей ("gear_user_profiles")
        $sql = 'UPDATE `gear_user_profiles` SET `user_id`=NULL '
             . 'WHERE `user_id` NOT IN (SELECT `user_id` FROM `gear_users`)';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }
}
?>