<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Створення запису "Група користувача"',
    'title_profile_update' => 'Зміна запису "%s"',
    // поля формы
    'label_group_name'       => 'Назва',
    'label_pgroup_name'      => 'Предок',
    'label_group_shortname'  => 'Назва (с.)',
    'label_group_about'      => 'Опис',
    'label_group_icon'       => 'Значок (css)',
    'msg_dependent_groups'   => '"Групи користувачів"',
    'empty_group_name'       => 'Група користувача',
    'empty_group_shortname'  => 'Група користувача (скорочення)',
    'title_fieldset_group'   => 'Положення в групі користувачів',
    'title_fieldset_icons'   => 'Логотип користувача (70x120 пкс.)',
    'label_group_logo_man'   => 'чоловічий',
    'label_group_logo_woman' => 'жіночий',
    // типы
    'data_depends' => array('"Групи" (%s)')
);;
?>