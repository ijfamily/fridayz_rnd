<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Creating an entry "User Group"',
    'title_profile_update' => 'Change entry "%s"',
    // поля формы
    'label_group_name'       => 'Name',
    'label_pgroup_name'      => 'Parent',
    'label_group_shortname'  => 'Name (с.)',
    'label_group_about'      => 'About',
    'label_group_icon'       => 'Icon (css)',
    'msg_dependent_groups'   => '"User groups"',
    'empty_group_name'       => 'User group',
    'empty_group_shortname'  => 'User group (short)',
    'title_fieldset_group'   => 'Position in the group of users',
    'title_fieldset_icons'   => 'User\'s Logo (70x120 px.)',
    'label_group_logo_man'   => 'male',
    'label_group_logo_woman' => 'female',
    // типы
    'data_depends' => array('"Groups" (%s)')
);
?>