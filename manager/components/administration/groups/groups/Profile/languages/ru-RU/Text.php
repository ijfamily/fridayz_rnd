<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Создание записи "Группа пользователя"',
    'title_profile_update' => 'Изменение записи "%s"',
    'text_btn_help'        => 'Справка',
    // поля формы
    'label_group_name'       => 'Название',
    'label_pgroup_name'      => 'Предок',
    'label_group_shortname'  => 'Название (с.)',
    'label_group_about'      => 'Описание',
    'label_group_icon'       => 'Значок (css)',
    'msg_dependent_groups'   => '"Группы пользователей"',
    'empty_group_name'       => 'Группа пользователя',
    'empty_group_shortname'  => 'Группа пользователя (сокращение)',
    'title_fieldset_group'   => 'Положение в группе пользователей',
    'title_fieldset_icons'   => 'Логотип пользователя (70x120 пкс.)',
    'label_group_logo_man'   => 'мужской',
    'label_group_logo_woman' => 'женский',
    // типы
    'data_depends' => array('"Группы" (%s)')
);
?>