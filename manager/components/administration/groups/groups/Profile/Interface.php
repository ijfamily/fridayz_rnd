<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля группы пользователя
 * Пакет контроллеров "Профиль группы пользователя"
 * Группа пакетов     "Группы пользователей"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AGroups_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля группы пользователя
 * 
 * @category   Gear
 * @package    GController_AGroups_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AGroups_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        $data = array('group_logo_woman' => '', 'group_logo_man' => '', 'group' => array('name' => '', 'id' => ''));
        // состояние профиля "вставка"
        if ($this->isInsert) return $data;

        parent::getDataInterface();

        $query = new GDb_Query();
        // группа пользователя
        $sql = 'SELECT * FROM `gear_user_groups` WHERE `group_id`=' . (int)$this->uri->id;
        $group = $query->getRecord($sql);
        $data['group_logo_woman'] = basename($group['group_logo_woman'], '.png');
        $data['group_logo_man'] = basename($group['group_logo_man'], '.png');
        // список групп пользователей
        $sql = 'SELECT group_name, group_id '
             . 'FROM `gear_user_groups` '
             . 'WHERE ' . $group['group_left'] .' > `group_left` AND ' . $group['group_right'] . ' < `group_right` AND '
             . $group['group_level'] . ' = `group_level` + 1';
        $record = $query->getRecord($sql);
        if (!empty($record)) {
            $data['group']['name'] = $record['group_name'];
            $data['group']['id'] = $record['group_id'];
        }

        return $data;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'titleEllipsis' => 40,
                  'gridId'        => 'gcontroller_agroups_grid',
                  'width'         => 360,
                  'autoHeight'    => true,
                  'resizable'     => false,
                  'stateful'      => false,
                  'buttonsAdd'    => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-disabled-ip-addresses',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );

        // поля формы (ExtJS class "Ext.Panel")
        $tpl = '<tpl for="."><div ext:qtip="<img width=71 height=120 src=\'resources/images/shell/users/{name}.png\'>" class="x-combo-list-item">'
             . '{name}</div></tpl>';
        $items = array(
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_group_name'],
                  'name'       => 'group_name',
                  'emptyText'  => $this->_['empty_group_name'],
                  'maxLength'  => 50,
                  'anchor'     => '100%',
                  'allowBlank' => false),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_group_shortname'],
                  'name'       => 'group_shortname',
                  'emptyText'  => $this->_['empty_group_shortname'],
                  'maxLength'  => 30,
                  'anchor'     => '100%',
                  'allowBlank' => false),
            array('xtype'      => 'textfield',
                  'fieldLabel' => $this->_['label_group_about'],
                  'name'       => 'group_about',
                  'maxLength'  => 255,
                  'anchor'     => '100%',),
            array('xtype'      => 'fieldset',
                  'labelWidth' => 80,
                  'title'      => $this->_['title_fieldset_group'],
                  'autoHeight' => true,
                  'items'      => array(
                      array('xtype'      => 'mn-field-combo-tree',
                            'fieldLabel' => $this->_['label_pgroup_name'],
                            'resetable'  => false,
                            'anchor'     => '100%',
                            'name'       => 'group_id',
                            'hiddenName' => 'group_id',
                            'disabled'   => $this->uri->id == 1,
                            'treeWidth'  => 400,
                            'treeRoot'   => $this->_isRootUser ? array('id' => 'root', 'expanded' => false, 'expandable' => true) : null,
                            'value'      => $data['group']['name'],
                            'allowBlank' => false,
                            'store'      => array(
                                'xtype' => 'jsonstore',
                                'url'   => $this->componentUrl . 'combo/nodes/'
                            )
                      )
                  )
            ),
            array('xtype'      => 'fieldset',
                  'labelWidth' => 60,
                  'title'      => $this->_['title_fieldset_icons'],
                  'autoHeight' => true,
                  'defaults'   => array('width' => 234),
                  'items'      => array(
                      array('xtype'      => 'mn-field-combo',
                            'tpl'        => $tpl,
                            'fieldLabel' => $this->_['label_group_logo_man'],
                            'value'      => $data['group_logo_man'],
                            'name'       => 'group_logo_man',
                            'pageSize'   => 50,
                            'hiddenName' => 'group_logo_man',
                            'editable'   => false,
                            'allowBlank' => false,
                            'store'      => array(
                                'xtype' => 'jsonstore',
                                'url'   => $this->componentUrl . 'combo/trigger/?name=userlogo'
                            )
                      ),
                      array('xtype'      => 'mn-field-combo',
                            'tpl'        => $tpl,
                            'fieldLabel' => $this->_['label_group_logo_woman'],
                            'value'      => $data['group_logo_woman'],
                            'name'       => 'group_logo_woman',
                            'pageSize'   => 50,
                            'hiddenName' => 'group_logo_woman',
                            'editable'   => false,
                            'allowBlank' => false,
                            'store'      => array(
                                'xtype' => 'jsonstore',
                                'url'   => $this->componentUrl. 'combo/trigger/?name=userlogo'
                            )
                      )
                  )
            )
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->addItems($items);
        $form->labelWidth = 85;
        $form->url = $this->componentUrl . 'profile/';

        parent::getInterface();
    }
}
?>