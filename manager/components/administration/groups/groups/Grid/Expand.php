<?php
/**
 * Gear Manager
 *
 * Контроллер         "Развёрнутая запись группы пользователя"
 * Пакет контроллеров "Список групп пользователей системы"
 * Группа пакетов     "Группы пользователей системы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AGroups_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Expand');

/**
 * Развёрнутая запись модуля
 * 
 * @category   Gear
 * @package    GController_AGroups_Grid
 * @subpackage Expand
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AGroups_Grid_Expand extends GController_Grid_Expand
{
    /**
     * Тайтл записи
     * 
     * @var string
     */
    protected $_title = '';

    /**
     * Вывод данных в интерфейс
     * 
     * @return void
     */
    protected function dataExpand()
    {
        parent::dataAccessView();

        $data = $this->getAttributes();
        $this->response->data = $data;
    }

    /**
     * Возращает атрибуты записи
     * 
     * @return string
     */
    protected function getAttributes()
    {
        $data = '';
        // компоненты группы пользователей
        $query = new GDb_Query();
        $sql = 
            'SELECT SQL_CALC_FOUND_ROWS `c`.*,`ca`.*,`cg`.`group_name`,`cm`.`module_name`, `cl`.`controller_name`, `cl`.`controller_about` '
          . 'FROM `gear_controller_access` `ca` '
          . 'JOIN `gear_controllers` `c` USING(`controller_id`) '
          . 'JOIN `gear_controllers_l` `cl` USING(`controller_id`) '
            // группа компонентов
          . 'LEFT JOIN (SELECT `g`.*,`l`.`group_name`,`l`.`group_about` FROM `gear_controller_groups` `g` JOIN `gear_controller_groups_l` `l` USING(`group_id`) '
          . 'WHERE `l`.`language_id`=' . $this->language->get('id') . ') `cg` ON `cg`.`group_id`=`c`.`group_id` '
            // модули
          . 'LEFT JOIN `gear_modules` `cm` ON `cm`.`module_id`=`cg`.`module_id` '
          . 'WHERE `cl`.`language_id`=' . $this->language->get('id') . ' AND '
          . '`ca`.`group_id`=' . $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();

        while (!$query->eof()) {
            $cmp = $query->next();
            // Компонент
            $data .= '<fieldset class="fixed" style="height:370px"><label>' . $cmp['controller_name'] . '</label>';
            if ($cmp['controller_about'])
                $data .= '<br><em>' . $cmp['controller_about'] . '</em>';
            $data .= '<ul><li><img src="' . PATH_COMPONENT . $cmp['controller_uri'] . 'resources/shortcut.png"/></li>';
            $data .= '<li><label>' . $this->_['label_controller_class'] . ':</label> ' . $cmp['controller_class'] . '</li>';
            $data .= '<li><label>' . $this->_['label_group_id'] . ':</label> ' 
                   . '<a type="widget" url="system/components/groups/' . ROUTER_DELIMITER . 'profile/interface/' . $cmp['group_id'] . '?state=info" href="#">' . $cmp['group_name'] . '</a></li>';
            $data .= '<li><label>' . $this->_['label_module_name'] . ':</label> ' 
                   . '<a type="widget" url="system/components/modules/' . ROUTER_DELIMITER . 'profile/interface/' . $cmp['module_id'] . '?state=info" href="#">' . $cmp['module_name'] . '</a></li>';
            // Ресурс
            $data .= '<li><label>' . $this->_['label_controller_uri_pkg'] . ':</label> ' . $cmp['controller_uri_pkg'] . '</li>';
            $data .= '<li><label>' . $this->_['label_controller_uri'] . ':</label> ' . $cmp['controller_uri'] . '</li>';
            $data .= '<li><label>' . $this->_['label_controller_uri_action'] . ':</label> ' . $cmp['controller_uri_action'] . '</li>';
            // Интерфейс
            $data .= '<li><label>' . $this->_['label_controller_enabled'] . ':</label> <span type="chbox" mn:value="' . $cmp['controller_enabled'] . '"></span></li>';
            $data .= '<li><label>' . $this->_['label_controller_visible'] . ':</label> <span type="chbox" mn:value="' . $cmp['controller_visible'] . '"></span></li>';
            $data .= '<li><label>' . $this->_['label_controller_dashboard'] . ':</label> <span type="chbox" mn:value="' . $cmp['controller_dashboard'] . '"></span></li>';
            // События
            $json = json_decode($cmp['controller_privileges'], true);
            if ($json) {
                $data .= '<li><label>' . $this->_['label_privileges'] . ':</label> ';
                $data .= GPrivileges::toString($json, ', ');
                $data .= '</li>';
            }
            // Поля
            $json = json_decode($cmp['controller_fields'], true);
            $res = array();
            for ($i = 0; $i < sizeof($json); $i++) {
                $res[] = $json[$i]['label'];
            }
            if ($res) {
                $data .= '<li><label>' . $this->_['title_fieldset_fields'] . ':</label> ';
                $data .= implode(', ', $res);
                $data .= '</li>';
            }
            $data .= '</fieldset>';
        }

        $data .= '<div class="wrap"></div>';
        $data = '<div class="mn-row-body border">' . $data . '<div class="wrap"></div>';

        return $data;
    }
}
?>