<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'       => 'Групи користувачів системи',
    'tooltip_grid'     => 'список групп пользователей системы',
    'rowmenu_edit'     => 'Редагувати',
    'rowmenu_access'   => 'Доступні компоненти',
    'rowmenu_mods'     => 'Доступ к настройкам модулей',
    'rowmenu_articles' => 'Доступні статті',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Стовпці',
    'title_buttongroup_filter' => 'Фільтр',
    'msg_btn_clear'            => 'Ви дійсно бажаєте видалити всі записи <span class="mn-msg-delete"> '
                                . '("ГГрупи користувачів системи", "Доступні компоненти", "Користувачі системи ","Журнали користувачів")</span> ?',
    // столбцы
    'header_group'             => 'Група',
    'header_group_name'        => 'Назва',
    'header_group_shortname'   => 'Назва (с.)',
    'tooltip_group_shortname'  => 'Назва скорочена',
    'header_pgroup_name'       => 'Назва "предка"',
    'header_pgroup_shortname'  => 'Назва "предка" (с.)',
    'tooltip_pgroup_shortname' => 'Назва "предка" скорочена',
    'header_group_about'       => 'Опис',
    'header_access_count'      => 'Компонентів',
    'tooltip_access_count'     => 'Доступні компоненти для групи користувача',
    'header_group_count'       => 'Груп',
    'tooltip_group_count'      => 'Кількість дочірніх груп',
    // тип
    'data_depends' => array('"Групи" (%s)'),
    // развернутая запись
    'header_attr'                 => 'Компоненты модуля',
    'title_fieldset_common'       => 'Компонент',
    'label_controller_class'      => 'ID класса',
    'label_group_id'              => 'Группа',
    'label_module_name'           => 'Модуль',
    'label_controller_clear'      => 'Очищать',
    'label_privileges'            => 'Допустимые права',
    'label_controller_uri_pkg'    => 'Компонент (интерфейс)',
    'label_controller_uri'        => 'Контроллер (интерфейс)',
    'label_controller_uri_action' => 'Интерфейс',
    'label_controller_enabled'    => 'Доступный',
    'label_controller_visible'    => 'Видимый',
    'label_controller_dashboard'  => 'На доске',
    'title_fieldset_fields'       => 'Права на поля'
);
?>