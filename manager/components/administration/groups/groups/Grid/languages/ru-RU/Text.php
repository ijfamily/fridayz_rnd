<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'       => 'Группы пользователей системы',
    'tooltip_grid'     => 'список групп пользователей системы',
    'rowmenu_edit'     => 'Редактировать',
    'rowmenu_access'   => 'Доступные компоненты',
    'rowmenu_mods'     => 'Доступ к настройкам модулей',
    'rowmenu_articles' => 'Доступные статьи',
    'rowmenu_categories' => 'Доступные категории статей',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete"> '
                                . '("Группы пользователей системы", "Доступные компоненты", "Пользователи системы", "Журналы пользователей")</span> ?',
    // столбцы
    'header_group'             => 'Группа',
    'header_group_name'        => 'Название',
    'header_group_shortname'   => 'Название (с.)',
    'tooltip_group_shortname'  => 'Название сокращённое',
    'header_pgroup_name'       => 'Название "предка"',
    'header_pgroup_shortname'  => 'Название "предка" (с.)',
    'tooltip_pgroup_shortname' => 'Название "предка" сокращённое',
    'header_group_about'       => 'Описание',
    'header_access_count'      => 'Компонентов',
    'tooltip_access_count'     => 'Доступные компоненты для группы пользователя',
    'header_group_count'       => 'Групп',
    'tooltip_group_count'      => 'Количество дочерних групп',
    // типы
    'data_depends' => array('"Группы" (%s)'),
    // развернутая запись
    'header_attr'                 => 'Компоненты модуля',
    'title_fieldset_common'       => 'Компонент',
    'label_controller_class'      => 'ID класса',
    'label_group_id'              => 'Группа',
    'label_module_name'           => 'Модуль',
    'label_controller_clear'      => 'Очищать',
    'label_privileges'            => 'Допустимые права',
    'label_controller_uri_pkg'    => 'Компонент (интерфейс)',
    'label_controller_uri'        => 'Контроллер (интерфейс)',
    'label_controller_uri_action' => 'Интерфейс',
    'label_controller_enabled'    => 'Доступный',
    'label_controller_visible'    => 'Видимый',
    'label_controller_dashboard'  => 'На доске',
    'title_fieldset_fields'       => 'Права на поля'
);
?>