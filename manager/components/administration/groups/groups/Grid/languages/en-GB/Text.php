<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'       => 'Groups of system users',
    'tooltip_grid'     => 'список групп пользователей системы',
    'rowmenu_edit'     => 'Edit',
    'rowmenu_access'   => 'Available components',
    'rowmenu_mods'     => 'Access to module settings',
    'rowmenu_articles' => 'Available articles',
    // панель управления
    'title_buttongroup_edit'   => 'Edit',
    'title_buttongroup_cols'   => 'Columns',
    'title_buttongroup_filter' => 'Filter',
    'msg_btn_clear'            => 'Do you really want to delete all entries <span class="mn-msg-delete"> '
                                . '("Groups of users of the system", "Available components", "Users of the system", "user logs")</span> ?',
    // столбцы
    'header_group'             => 'Group',
    'header_group_name'        => 'Name',
    'header_group_shortname'   => 'Name (sh.)',
    'tooltip_group_shortname'  => 'Short name',
    'header_pgroup_name'       => 'Name "parent"',
    'header_pgroup_shortname'  => 'Name "parent" (sh.)',
    'tooltip_pgroup_shortname' => 'Short name "parent"',
    'header_group_about'       => 'About',
    'header_access_count'      => 'Component',
    'tooltip_access_count'     => 'Available components for the user group',
    'header_group_count'       => 'Groups',
    'tooltip_group_count'      => 'The number of child groups',
    // тип
    'data_depends' => array('"Groups" (%s)'),
    // развернутая запись
    'header_attr'                 => 'Компоненты модуля',
    'title_fieldset_common'       => 'Компонент',
    'label_controller_class'      => 'ID класса',
    'label_group_id'              => 'Группа',
    'label_module_name'           => 'Модуль',
    'label_controller_clear'      => 'Очищать',
    'label_privileges'            => 'Допустимые права',
    'label_controller_uri_pkg'    => 'Компонент (интерфейс)',
    'label_controller_uri'        => 'Контроллер (интерфейс)',
    'label_controller_uri_action' => 'Интерфейс',
    'label_controller_enabled'    => 'Доступный',
    'label_controller_visible'    => 'Видимый',
    'label_controller_dashboard'  => 'На доске',
    'title_fieldset_fields'       => 'Права на поля'
)
?>