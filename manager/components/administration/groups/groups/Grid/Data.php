<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка групп пользователей системы"
 * Пакет контроллеров "Список групп пользователей системы"
 * Группа пакетов     "Группы пользователей системы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AGroups_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные списка групп пользователей системы
 * 
 * @category   Gear
 * @package    GController_AGroups_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AGroups_Grid_Data extends GController_Grid_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * (для обработки записей таблицы)
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'group_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_user_groups';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS `g`.*, `pg`.`group_shortname` `pgroup_shortname`, `pg`.`group_name` `pgroup_name`, '
          . '`pg`.`group_about` `pgroup_about` '
          . 'FROM `gear_user_groups` `g` '
          . 'LEFT JOIN `gear_user_groups` `pg` ON `pg`.`group_left` < `g`.`group_left` AND '
          . '`pg`.`group_right` > `g`.`group_right` AND `pg`.`group_level` = `g`.`group_level`-1  %filter '
          . 'ORDER BY %sort '
          . 'LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // название
            'group_name' => array('type' => 'string'),
            // название сокр.
            'group_shortname' => array('type' => 'string'),
            // описание
            'group_about' => array('type' => 'string'),
            // название предка
            'pgroup_name' => array('type' => 'string'),
            // название предка сокр.
            'pgroup_shortname' => array('type' => 'string'),
            // доступных компонентов
            'access_count' => array('type' => 'string')
        );
    }

    /**
     * Удаление данных
     * 
     * @return void
     */
    protected function dataDelete()
    {
        parent::dataAccessDelete();

        // определяем откуда id брать
        if (empty($this->uri->id)) {
            $input = GFactory::getInput();
            $this->uri->id = $input->get('id');
        }
        // если не указан $id
        if (empty($this->uri->id))
            throw new GException('Error', 'Unable to delete records!');

        // проверка существования зависимых записей
        $this->isDataDependent();

        Gear::library('Db/Drivers/MySQL/Tree/Tree');

        // указатель на базу данных
        $db = GFactory::getDb();
        // удаление групп пользователей ("gear_user_groups")
        $tree = new GDb_Tree($this->tableName, 'group', $db->getHandle());
        if ($tree->deleteAll((int)$this->uri->id, '') === false)
            throw new GException('Data processing error', 'Query error (Internal Server Error)', $tree->ERRORS_MES[0], false);
    }

    /**
     * Проверка существования зависимых записей (в каскадном удалении)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        $query = new GDb_Query();
        // выбранная группа пользователя
        $groupId = (int) $this->uri->id;
        $sql = 'SELECT * FROM `gear_user_groups` WHERE `group_id`=' . $groupId;
        $group = $query->getRecord($sql);
        if ($group === false)
        if ($query->execute($sql) === false)
            throw new GSqlException();
        if ($group['sys_record'] == 1)
            throw new GException('Error', 'Unable to delete records!');
        $error = array();
        // если в группе есть потомки
        $count = ($group['group_right'] - $group['group_left'] - 1) / 2;
        if ($count > 0)
            $error[] = sprintf($this->_['data_depends'][0], $count);
        // если есть зависимости
        if ($error)
            throw new GException('Deleting data', 'There is a correlation record for deletion', implode(', ', $error));

        // удаление журнала выхода пользователей ("gear_user_escapes")
        $sql = 'DELETE `e` FROM `gear_user_escapes` `e`,`gear_users` `u` '
             . 'WHERE `e`.`user_id`=`u`.`user_id` AND `u`.`sys_record`<>1 AND `u`.`group_id`=' . $groupId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_user_escapes` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление журнала авторизации пользователей ("gear_user_attends")
        $sql = 'DELETE `a` FROM `gear_user_attends` `a`,`gear_users` `u` '
             . 'WHERE `a`.`user_id`=`u`.`user_id` AND `u`.`sys_record`<>1 AND `u`.`group_id`=' . $groupId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_user_attends` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление журнала действий пользователей ("gear_log")
        $sql = 'DELETE `l` FROM `gear_log` `l`,`gear_users` `u` '
             . 'WHERE `l`.`user_id`=`u`.`user_id` AND `u`.`sys_record`<>1 AND `u`.`group_id`=' . $groupId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_log` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // удаление учётных записей пользователей ("gear_users")
        $sql = 'DELETE FROM `gear_users` WHERE `sys_record`<>1 AND `group_id`=' . $groupId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_users` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();

        // обновление профилей пользователей ("gear_user_profiles")
        $sql = 'UPDATE `gear_user_profiles` SET `user_id`=NULL '
             . 'WHERE `user_id` NOT IN (SELECT `user_id` FROM `gear_users`)';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }
}
?>