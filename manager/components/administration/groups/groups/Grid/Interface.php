<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка групп пользователей системы"
 * Пакет контроллеров "Список групп пользователей системы"
 * Группа пакетов     "Группы пользователей системы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AGroups_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс списка групп пользователей системы
 * 
 * @category   Gear
 * @package    GController_AGroups_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AGroups_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'group_id';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'group_name';

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // название
            array('name' => 'group_name', 'type' => 'string'),
            // название сокр.
            array('name' => 'group_shortname', 'type' => 'string'),
            // описание
            array('name' => 'group_about', 'type' => 'string'),
            // название предка
            array('name' => 'pgroup_name', 'type' => 'string'),
            // название предка сокр.
            array('name' => 'pgroup_shortname', 'type' => 'string'),
            // доступных компонентов
            array('name' => 'access_count', 'type' => 'string')
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $this->columns = array(
            array('xtype'     => 'expandercolumn',
                  'url'       => $this->componentUrl . 'grid/expand/',
                  'typeCt'    => 'row'),
            array('xtype'     => 'numbercolumn'),
            array('xtype'     => 'rowmenu'),
            array('dataIndex' => 'group_name',
                  'header'    => '<em class="mn-grid-hd-details"></em>' . $this->_['header_group_name'],
                  'width'     => 150,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'group_shortname',
                  'header'    => $this->_['header_group_shortname'],
                  'tooltip'   => $this->_['tooltip_group_shortname'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'group_about',
                  'header'    => $this->_['header_group_about'],
                  'width'     => 170,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'pgroup_name',
                  'header'    => $this->_['header_pgroup_name'],
                  'width'     => 130,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'pgroup_shortname',
                  'header'    => $this->_['header_pgroup_shortname'],
                  'tooltip'   => $this->_['tooltip_pgroup_shortname'],
                  'width'     => 150,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string'))
        );

        // список (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_grid'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_grid'],
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'profileUrl'    => $this->componentUrl . 'profile/',
                  'sm'            => array('singleSelect' => true))
        );

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "правка" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "добавить" (ExtJS class "Manager.button.InsertData")
        $group->items->add(array('xtype' => 'mn-btn-insert-data', 'gridId' => $this->classId));
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' => $this->classId));
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = 'component-users-groups';
        $this->_cmp->tbar->items->add($group);

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $cellInfo =
            '<div class="mn-grid-cell-tooltip-tl">{group_name}</div>'
          . '<div class="mn-grid-cell-tooltip" style="width:300px">'
          . '<em>' . $this->_['header_group_shortname'] . '</em>: <b>{group_shortname}</b><br>'
          . '<em>' . $this->_['header_group_about'] . '</em>: <b>{group_about}</b><br>'
          . '<em>' . $this->_['header_pgroup_name'] . '</em>: <b>{pgroup_name}</b><br>'
          . '<em>' . $this->_['header_pgroup_shortname'] . '</em>: <b>{pgroup_shortname}</b>'
          . '</div>';
        $this->_cmp->cellTips = array(
            array('field' => 'group_name', 'tpl' => $cellInfo),
            array('field' => 'group_shortname', 'tpl' => '{group_name}'),
            array('field' => 'group_about', 'tpl' => '{group_about}'),
            array('field' => 'pgroup_name', 'tpl' => '{pgroup_name}'),
            array('field' => 'pgroup_shortname', 'tpl' => '{pgroup_shortname}')
        );
        $this->_cmp->cellTipConfig = array('maxWidth' => 300);

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $this->_cmp->rowMenu = array(
            'xtype' => 'mn-grid-rowmenu',
            'items' => array(
                array('text'    => $this->_['rowmenu_edit'],
                      'iconCls' => 'icon-form-edit',
                      'url'     => $this->componentUrl . 'profile/interface/'),
                array('xtype'   => 'menuseparator'),
                array('text'    => $this->_['rowmenu_access'],
                      'icon'    => $this->resourcePath . 'icon-grid.png',
                      'url'     => $this->componentUri . '../access/' . ROUTER_DELIMITER . 'grid/interface/'),
                array('text'    => $this->_['rowmenu_categories'],
                      'icon'    => $this->resourcePath . 'icon-grid.png',
                      'url'     => $this->componentUri . '../categories/' . ROUTER_DELIMITER . 'grid/interface/'),
                array('text'    => $this->_['rowmenu_mods'],
                      'icon'    => $this->resourcePath . 'icon-item-access.png',
                      'url'     => $this->componentUrl . 'access/interface/')
            )
        );

        parent::getInterface();
    }
}
?>