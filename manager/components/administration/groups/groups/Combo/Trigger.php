<?php
/**
 * Gear Manager
 *
 * Контроллер         "Триггер выпадающего списка"
 * Пакет контроллеров "Триггер групп пользователей"
 * Группа пакетов     "Группы пользователей системы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AGroups_Combo
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Combo/Trigger');

/**
 * Триггер выпадающего списка
 * 
 * @category   Gear
 * @package    GController_AGroups_Combo
 * @subpackage Trigger
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AGroups_Combo_Trigger extends GController_Combo_Trigger
{
    /**
     * Вывод логотипов групп пользователей
     * 
     * @return void
     */
    protected function queryUserlogo()
    {
        $dir = 'resources/images/shell/users';
        if (($handle = opendir($dir)) === false) 
            throw new GException('Error', 'The directory can not be opened');
        $data = array();
        $count = 0;
        while (false !== ($file = readdir($handle))) {
            if ($file != '.' && $file != '..') {
                $file1 = basename($file, '.png'); 
                $data[] = array('id' => $file, 'name' => $file1);
                $count++;
            }
        }
        $this->response->set('totalCount', $count);
        $this->response->success = true;
        $this->response->data = $data;
        closedir($handle);
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $name название триггера
     * @return void
     */
    protected function dataView($name)
    {
        parent::dataView($name);

        // триггер
        switch ($name) {
            // логотипы групп пользователей
            case 'userlogo': $this->queryUserLogo(); break;
        }
    }
}
?>