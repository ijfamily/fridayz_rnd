<?php
/**
 * Gear Manager
 *
 * Контроллер         "Триггер выпадающего дерева"
 * Пакет контроллеров "Группы пользователей системы"
 * Группа пакетов     "Группы пользователей системы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AGroups_Combo
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Nodes.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Combo/Nodes');

/**
 * Триггер выпадающего дерева
 * 
 * @category   Gear
 * @package    GController_AGroups_Combo
 * @subpackage Nodes
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Nodes.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AGroups_Combo_Nodes extends GController_Combo_NNodes
{
    /**
     * Префикс полей таблицы для формирования nested set
     *
     * @var string
     */
    public $fieldPrefix = 'group';

    /**
     * Поле используемое для сортировки данных
     *
     * @var string
     */
    public $orderBy = 'left';

    /**
     * Первичное поле таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = 'group_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_user_groups';

    /**
     * Значок (css) для ветвей дерева
     *
     * @var string
     */
    public $iconChildCls = 'icon-usr-groups';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_agroups_grid';

    /**
     * Предварительная обработка узла дерева перед формированием массива записей JSON
     * 
     * @params array $node узел дерева
     * @params array $record запись
     * @return array
     */
    protected function nodesPreprocessing($node, $record)
    {
        $node['text'] = $record['group_name'];

        return $node;
    }
}
?>