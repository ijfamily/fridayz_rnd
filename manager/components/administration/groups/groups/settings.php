<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Группы пользователей системы"
 * Группа пакетов     "Группы пользователей системы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AGroups
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 * 
 * Установка компонента
 * 
 * Название: Группы пользователей системы
 * Описание: Группы пользователей системы
 * ID класса: gcontroller_agroups_grid
 * Группа: Администрирование / Пользователи
 * Очищать: нет
 * Ресурс
 *    Пакет: administration/groups/groups/
 *    Контроллер: administration/groups/groups/Grid/
 *    Интерфейс: grid/interface/
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске: нет
 */

return array(
    // {A}- модуль "Administration" {Groups} - пакет контроллеров "Groups" -> AGroups
    'clsPrefix' => 'AGroups'
);
?>