<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск групп пользователей системы"
 * Пакет контроллеров "Группы пользователией системы"
 * Группа пакетов     "Группы пользователей системы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AGroups_Search
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск пользователей системы
 * 
 * @category   Gear
 * @package    GController_AGroups_Search
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AGroups_Search_Data extends GController_Search_Data
{
    /**
     * дентификатор компонента (списка) к которому применяется поиск
     *
     * @var string
     */
    public $gridId = 'gcontroller_agroups_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_agroups_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор
     * Используется для инициализации атрибутов контроллер не переданного в конструктор
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'group_name', 'label' => $this->_['header_group_name'], 'as' => 'g.group_name'),
            array('dataIndex' => 'group_shortname', 'label' => $this->_['header_group_shortname'], 'as' => 'g.group_shortname'),
            array('dataIndex' => 'group_about', 'label' => $this->_['header_group_about']),
            array('dataIndex' => 'pgroup_name', 'label' => $this->_['header_pgroup_name'], 'as' => 'pg.group_name'),
            array('dataIndex' => 'pgroup_shortname', 'label' => $this->_['header_pgroup_shortname'], 'as' => 'pg.group_shortname')
        );
    }
}
?>