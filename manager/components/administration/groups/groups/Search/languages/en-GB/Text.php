<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Search in the list "Groups of users of the system"',
    // поля
    'header_group'            => 'Group',
    'header_group_name'       => 'Name',
    'header_group_shortname'  => 'Name (sh.)',
    'header_pgroup_name'      => 'Name "parent"',
    'header_pgroup_shortname' => 'Name "parent" (sh.)',
    'header_group_about'      => 'About',
    'header_access_count'     => 'Components',
    'header_group_count'      => 'Groups'
);
?>