<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Поиск в списке "Группы пользователей системы"',
    // поля
    'header_group'            => 'Группа',
    'header_group_name'       => 'Название',
    'header_group_shortname'  => 'Название (с.)',
    'header_pgroup_name'      => 'Название "предка"',
    'header_pgroup_shortname' => 'Название "предка" (с.)',
    'header_group_about'      => 'Описание',
    'header_access_count'     => 'Компонентов',
    'header_group_count'      => 'Групп'
);
?>