<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Пошук у списку "Групи користувачів системи"',
    // поля
    'header_group'            => 'Група',
    'header_group_name'       => 'Назва',
    'header_group_shortname'  => 'Назва (с.)',
    'header_pgroup_name'      => 'Назва "предка"',
    'header_pgroup_shortname' => 'Назва "предка" (с.)',
    'header_group_about'      => 'Опис',
    'header_access_count'     => 'Компонентів',
    'header_group_count'      => 'Груп'
);
?>