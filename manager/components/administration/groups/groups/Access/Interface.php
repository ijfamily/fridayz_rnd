<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля доступа к модулям"
 * Пакет контроллеров "Профиль доступа к модулям"
 * Группа пакетов     "Группы пользователей"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AGroups_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля доступа к модулям
 * 
 * @category   Gear
 * @package    GController_AGroups_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AGroups_Access_Interface extends GController_Profile_Interface
{
    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        parent::getDataInterface();

        $query = new GDb_Query();
        // модули
        $sql = 'SELECT * FROM `gear_modules`';
        if (!$query->execute($sql))
            throw new GSqlException();
        $items = array();
        while (!$query->eof()) {
            $record = $query->next();
            $items[] = array(
                'xtype'      => 'mn-field-chbox',
                'fieldLabel' => $record['module_name'],
                'checkDirty' => false,
                'default'    => 0,
                'name'       => 'mods[' . $record['module_name_settings'] . ']'
            );
        }

        return $items;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'           => '',
                  'titleEllipsis'   => 80,
                  'gridId'          => 'gcontroller_agroups_grid',
                  'width'           => 500,
                  'autoHeight'      => true,
                  'resizable'       => false,
                  'btnDeleteHidden' => true,
                  'stateful'        => false)
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->addItems($data);
        $form->labelWidth = 140;
        $form->url = $this->componentUrl . 'access/';

        parent::getInterface();
    }
}
?>