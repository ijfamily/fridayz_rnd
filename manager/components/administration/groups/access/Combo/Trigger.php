<?php
/**
 * Gear Manager
 *
 * Контроллер         "Триггер выпадающего списка"
 * Пакет контроллеров "Триггер доступных компонентов"
 * Группа пакетов     "Группы пользователей системы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AGroupAccess_Combo
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Combo/Trigger');

/**
 * Триггер выпадающего списка
 * 
 * @category   Gear
 * @package    GController_AGroupAccess_Combo
 * @subpackage Trigger
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AGroupAccess_Combo_Trigger extends GController_Combo_Trigger
{
    /**
     * Вывод данных компонентов
     * 
     * @return void
     */
    protected function queryControllers()
    {
        $groupId = $this->store->get('record', 0, 'gcontroller_agroups_grid');
        $table = new GDb_Table('gear_controller_access', 'access_id');
        $table->setStart($this->_start);
        $table->setLimit($this->_limit);
        $query = 'SELECT SQL_CALC_FOUND_ROWS `c`.*,`l`.* '
               . 'FROM `gear_controllers` `c` '
               . 'JOIN `gear_controllers_l` `l` USING(`controller_id`) '
               . 'WHERE `c`.`controller_id` NOT IN (SELECT `controller_id` FROM `gear_controller_access` WHERE `group_id`=' . $groupId . ') '
               . 'AND `l`.language_id=' . $this->language->get('id')
               . ($this->_query ? " AND `l`.`controller_name` LIKE '{$this->_query}%' " : ' ')
               . 'ORDER BY `l`.`controller_name` '
               . 'LIMIT %limit';
        if ($table->query($query) === false)
            throw new GSqlException();

        $data = array();
        while (!$table->query->eof()) {
            $rec = $table->query->next();
            $data[] = array(
                'id'   => $rec['controller_id'],
                'name' => $rec['controller_name'],
                'data' => PATH_COMPONENT . $rec['controller_uri']
            );
        }
        $this->response->set('totalCount', $table->query->getFoundRows());
        $this->response->success = true;
        $this->response->data = $data;
    }

    /**
     * Вывод групп компонентов
     * 
     * @return void
     */
    protected function queryGroups()
    {
        $table = new GDb_Table('gear_controller_groups', 'group_id');
        $table->setStart($this->_start);
        $table->setLimit($this->_limit);
        $sql = 'SELECT SQL_CALC_FOUND_ROWS * '
             . 'FROM `gear_controller_groups` `g` JOIN `gear_controller_groups_l` `l` USING(`group_id`) '
             . 'WHERE `l`.`language_id`=' . $this->language->get('id')
             . ($this->_query ? " AND `l`.`group_name` LIKE '{$this->_query}%' " : ' ')
             . 'ORDER BY `group_name` ASC '
             . 'LIMIT %limit';
        $table->query($sql);
        $data = array();
        while (!$table->query->eof()) {
            $record = $table->query->next();
            $info = $record['group_name'] . '<br>'
               . '<span class="mn-combo-item-info">' 
               . $record['group_about'] . '</span>';
            $data[] = array(
                'id'   => $this->_isString ? $record['group_name'] : $record['group_id'],
                'name' => $record['group_name'],
                'data' => $info
            );
        }
        $this->response->set('totalCount', $table->query->getFoundRows());
        $this->response->success = true;
        $this->response->data = $data;
    }

    /**
     * Вывод модулей системы
     * 
     * @return void
     */
    protected function queryModules()
    {
        $table = new GDb_Table('gear_modules', 'module_id');
        $table->setStart($this->_start);
        $table->setLimit($this->_limit);
        $sql = 'SELECT SQL_CALC_FOUND_ROWS * '
             . 'FROM `gear_modules` '
             . 'WHERE 1 ' . ($this->_query ? "AND `module_name` LIKE '{$this->_query}%' " : '')
             . 'ORDER BY `module_name` ASC '
             . 'LIMIT %limit';
        $table->query($sql);
        $data = array();
        while (!$table->query->eof()) {
            $record = $table->query->next();
            $info = $record['module_name'] . '<br>'
               . '<span class="mn-combo-item-info">' 
               . $record['module_description'] . '</span>';
            $data[] = array(
                'id'   => $this->_isString ? $record['module_name'] : $record['module_id'],
                'name' => $record['module_name'],
                'data' => $info
            );
        }
        $this->response->set('totalCount', $table->query->getFoundRows());
        $this->response->success = true;
        $this->response->data = $data;
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $name название триггера
     * @return void
     */
    protected function dataView($name)
    {
        parent::dataView($name);

        // триггер
        switch ($name) {
            // компоненты
            case 'controllers': $this->queryControllers(); break;

            // группы компонентов
            case 'groups': $this->queryGroups(); break;

            // модули
            case 'modules': $this->queryModules(); break;
        }
    }
}
?>