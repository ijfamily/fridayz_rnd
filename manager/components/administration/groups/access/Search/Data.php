<?php
/**
 * Gear Manager
 *
 * Controller          "Поиск данных в списке"
 * Package controllers "Доступные компоненты"
 * Group of packages   "Компоненты"
 * Module              "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AGroupAccess_Search
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск данных в списке
 * 
 * @category   Gear
 * @package    GController_AGroupAccess_Search
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AGroupAccess_Search_Data extends GController_Search_Data
{
    /**
     * дентификатор компонента (списка) к которому применяется поиск
     *
     * @var string
     */
    public $gridId = 'gcontroller_agroupaccess_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_agroups_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор,
     * используется для инициализации атрибутов контроллера без конструктора
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'controller_name', 'label' => $this->_['header_controller_name']),
            array('dataIndex' => 'group_name', 'label' => $this->_['header_group_name']),
            array('dataIndex' => 'module_name', 'label' => $this->_['header_module_name']),
            array('dataIndex' => 'controller_about', 'label' => $this->_['header_controller_about']),
            array('dataIndex' => 'controller_class', 'label' => $this->_['header_controller_class']),
            array('dataIndex' => 'controller_uri_pkg', 'label' => $this->_['header_controller_uri_pkg']),
            array('dataIndex' => 'controller_uri', 'label' => $this->_['header_controller_uri']),
            array('dataIndex' => 'controller_uri_action', 'label' => $this->_['header_controller_uri_action']),
            array('dataIndex' => 'access_shortcut', 'label' => $this->_['header_access_shortcut']),
            array('dataIndex' => 'access_log', 'label' => $this->_['header_access_log']),
            array('dataIndex' => 'access_debug', 'label' => $this->_['header_access_debug']),
            array('dataIndex' => 'access_error', 'label' => $this->_['header_access_error']),
            array('dataIndex' => 'controller_enabled', 'label' => $this->_['header_controller_enabled']),
            array('dataIndex' => 'controller_visible', 'label' => $this->_['header_controller_visible']),
            array('dataIndex' => 'controller_dashboard', 'label' => $this->_['header_controller_board']),
            array('dataIndex' => 'controller_clear', 'label' => $this->_['header_controller_clear'])
        );
    }
}
?>