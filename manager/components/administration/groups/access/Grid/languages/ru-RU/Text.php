<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'         => 'Доступные компоненты списка "%s"',
    'rowmenu_edit'       => 'Редактировать',
    'rowmenu_privileges' => 'Привилегии',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete"> '
                                . '("Доступные компоненты")</span> ?',
    // столбцы
    'header_controller_name'        => 'Название',
    'header_group_name'             => 'Группа',
    'tooltip_group_name'            => 'Группа компонентов',
    'header_module_name'            => 'Модуль',
    'tooltip_module_name'           => 'Модуль системы',
    'header_controller_about'       => 'Описание',
    'header_controller_class'       => 'ID класса',
    'header_controller_uri_pkg'     => 'Компонент',
    'tooltip_controller_uri_pkg'    => 'Путь к каталогу компонента',
    'header_controller_uri'         => 'Контроллер',
    'tooltip_controller_uri'        => 'Путь к каталогу контроллера',
    'header_controller_uri_action'  => 'Интерфейс',
    'tooltip_controller_uri_action' => 'Интерфейс контроллера',
    'header_controller_enabled'  => 'Доступный',
    'header_controller_visible'  => 'Видимый',
    'header_controller_board'    => 'На доске',
    'tooltip_controller_board'   => 'Присутствие компонента на доске компонентов',
    'header_controller_clear'    => 'Очистка',
    'tooltip_controller_clear'   => 'Выполнять очистку (удаление всех) данных компонента',
    'tooltip_access_shortcut'    => 'Ярлык на рабочем столе',
    'tooltip_access_log'         => 'Вести журнал событий совершаемых компонентом в виде назначенных привилегий группе пользователя',
    'tooltip_access_debug'       => 'Использовать отладчик Терминала для просмотр всех действий пользователя (необходим FireBug)',
    'tooltip_access_error'       => 'Вывод детальных ошибок компонента',
    'header_access_privileges'   => 'Привилегии',
    'tooltip_controller_statistics' => 'Данные компонента участвуют в статистике',
    'tooltip_privileges'          => 'Редактировать привилегии',
    // типы
    'data_boolean' => array('Нет', 'Да'),
    // развернутая запись
    'header_attr'                => 'Атрибуты компонента',
    'title_fieldset_common'      => 'Компонент',
    'label_controller_class'     => 'ID класса',
    'label_group_id'             => 'Группа',
    'label_module_name'          => 'Модуль',
    'label_controller_clear'     => 'Очищать',
    'title_fieldset_path'        => 'Ресурс',
    'title_fieldset_interface'   => 'Интерфейс',
    'label_controller_uri_pkg'   => 'Компонент',
    'label_controller_uri'       => 'Контроллер',
    'label_controller_uri_action' => 'Интерфейс',
    'label_controller_enabled'   => 'Доступный',
    'label_controller_visible'   => 'Видимый',
    'label_controller_dashboard' => 'На доске',
    'title_fieldset_privileges'  => 'Допустимые права',
    'title_fieldset_fields'      => 'Права на поля'
);
?>