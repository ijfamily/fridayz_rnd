<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка доступных компонентов"
 * Пакет контроллеров "Список доступных компонентов"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AGroupAccess_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс списка доступных компонентов
 * 
 * @category   Gear
 * @package    GController_AGroupAccess_Grid
 * @subpackage Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AGroupAccess_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * (для обработки записей таблицы)
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'access_id';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'controller_name';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_agroups_grid';

    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        // соединение с базой данных
        GFactory::getDb()->connect();
        $table = new GDb_Table('gear_user_groups', 'group_id', array());
        $record = $table->getRecord($this->uri->id);
        if ($record === false)
            throw new GSqlException();

        return array('title' => sprintf($this->_['title_grid'], $record['group_name']));
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительный данные для создания интерфейса
        $data = $this->getDataInterface();

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('name' => 'controller_name', 'type' => 'string'),
            array('name' => 'group_name', 'replacer' => 'group_id', 'type' => 'string'),
            array('name' => 'controller_path', 'type' => 'string'),
            array('name' => 'controller_about', 'type' => 'string'),
            array('name' => 'controller_uri', 'type' => 'string'),
            array('name' => 'controller_uri_pkg', 'type' => 'string'),
            array('name' => 'controller_uri_action', 'type' => 'string'),
            array('name' => 'controller_class', 'type' => 'string'),
            array('name' => 'controller_enabled', 'type' => 'string'),
            array('name' => 'controller_visible', 'type' => 'string'),
            array('name' => 'controller_dashboard', 'type' => 'string'),
            array('name' => 'controller_clear', 'type' => 'string'),
            array('name' => 'module_name', 'type' => 'string'),
            array('name' => 'access_shortcut', 'type' => 'string'),
            array('name' => 'access_log', 'type' => 'string'),
            array('name' => 'access_debug', 'type' => 'string'),
            array('name' => 'access_error', 'type' => 'string'),
            array('name' => 'access_privileges', 'type' => 'string')
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $this->columns = array(
            array('xtype'     => 'numbercolumn'),
            array('xtype'     => 'rowmenu'),
            array('dataIndex' => 'controller_name',
                  'header'    => '<em class="mn-grid-hd-details"></em>'
                               . $this->_['header_controller_name'],
                  'width'     => 140,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'group_name',
                  'header'    => $this->_['header_group_name'],
                  'tooltip'   => $this->_['tooltip_group_name'],
                  'hidden'    => true,
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'module_name',
                  'header'    => $this->_['header_module_name'],
                  'tooltip'   => $this->_['tooltip_module_name'],
                  'hidden'    => true,
                  'width'     => 140,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'controller_about',
                  'header'    => $this->_['header_controller_about'],
                  'hidden'    => true,
                  'width'     => 160,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'controller_class',
                  'header'    => $this->_['header_controller_class'],
                  'width'     => 150,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'controller_uri_pkg',
                  'header'    => $this->_['header_controller_uri_pkg'],
                  'width'     => 190,
                  'hidden'    => true,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'controller_uri',
                  'header'    => $this->_['header_controller_uri'],
                  'width'     => 190,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'controller_uri_action',
                  'header'    => $this->_['header_controller_uri_action'],
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('xtype'     => 'widgetcolumn',
                  'dataIndex' => 'access_id',
                  'url'       => $this->componentUrl . 'privileges/interface/',
                  'icon'      => $this->resourcePath . 'icon-hd-access.png',
                  'tooltip'   => $this->_['tooltip_privileges']),
            array('dataIndex' => 'access_privileges',
                  'header'    => $this->_['header_access_privileges'],
                  'width'     => 150,
                  'sortable'  => true),
            array('xtype'     => 'booleancolumn',
                  'cls'       => 'mn-bg-color-gray2',
                  'dataIndex' => 'access_shortcut',
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-shortcut.png">',
                  'tooltip'   => $this->_['tooltip_access_shortcut'],
                  'width'     => 40,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean')),
            array('xtype'     => 'booleancolumn',
                  'dataIndex' => 'access_log',
                  'cls'       => 'mn-bg-color-gray2',
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-log.png">',
                  'tooltip'   => $this->_['tooltip_access_log'],
                  'width'     => 40,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean')),
            array('xtype'     => 'booleancolumn',
                  'dataIndex' => 'access_debug',
                  'cls'       => 'mn-bg-color-gray2',
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-debug.png">',
                  'tooltip'   => $this->_['tooltip_access_debug'],
                  'width'     => 40,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean')),
            array('xtype'     => 'booleancolumn',
                  'cls'       => 'mn-bg-color-gray2',
                  'dataIndex' => 'access_error',
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-error.png">',
                  'tooltip'   => $this->_['tooltip_access_error'],
                  'width'     => 40,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean')),
            array('xtype'     => 'booleancolumn',
                  'dataIndex' => 'controller_enabled',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-enabled.png">',
                  'tooltip'   => $this->_['header_controller_enabled'],
                  'width'     => 40,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean')),
            array('xtype'     => 'booleancolumn',
                  'dataIndex' => 'controller_visible',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-visible.png">',
                  'tooltip'   => $this->_['header_controller_visible'],
                  'width'     => 40,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean')),
            array('xtype'     => 'booleancolumn',
                  'dataIndex' => 'controller_dashboard',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-desk.png">',
                  'tooltip'   => $this->_['tooltip_controller_board'],
                  'width'     => 40,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean')),
            array('xtype'     => 'booleancolumn',
                  'dataIndex' => 'controller_clear',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-clear.png">',
                  'tooltip'   => $this->_['tooltip_controller_clear'],
                  'width'     => 40,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean')),
            array('xtype'     => 'booleancolumn',
                  'url'       => $this->componentUrl . 'profile/field/',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-chart.png">',
                  'tooltip'   => $this->_['tooltip_controller_statistics'],
                  'width'     => 40,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean'))
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $data['title'],
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'profileUrl'    => $this->componentUrl . 'profile/')
        );

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "edit" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "добавить" (ExtJS class "Manager.button.InsertData")
        $group->items->add(array('xtype' => 'mn-btn-insert-data', 'gridId' => $this->classId));
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' => $this->classId));
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'      => 'mn-btn-clear-data',
                  'msgConfirm' => $this->_['msg_btn_clear'],
                  'gridId'     => $this->classId,
                  'isN'        => true)
        );
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = $this->classId;
        $this->_cmp->tbar->items->add($group);

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $cellInfo =
            '<div class="mn-grid-cell-tooltip-tl">{controller_name}</div>'
          . '<div class="mn-grid-cell-tooltip" style="width:400px">'
          . '<img src="' . PATH_COMPONENT . '{controller_uri}/resources/shortcut.png" width="60px" height="60px">'
          . '<em>' . $this->_['header_group_name'] . '</em>: <b>{group_name}</b><br>'
          . '<em>' . $this->_['header_controller_uri_pkg'] . '</em>: <b>{controller_uri_pkg}</b><br>'
          . '<em>' . $this->_['header_controller_uri'] . '</em>: <b>{controller_uri}</b><br>'
          . '<em>' . $this->_['header_controller_uri_action'] . '</em>: <b>{controller_uri_action}</b><br>'
          . '<em>' . $this->_['header_controller_class'] . '</em>: <b>{controller_class}</b>'
          . '</div>';
        $this->_cmp->cellTips = array(
            array('field' => 'controller_name', 'tpl' => $cellInfo),
            array('field' => 'group_name', 'tpl' => '{group_name}'),
            array('field' => 'controller_about', 'tpl' => '{controller_about}'),
            array('field' => 'controller_class', 'tpl' => '{controller_class}'),
            array('field' => 'controller_uri_pkg', 'tpl' => '{controller_uri_pkg}'),
            array('field' => 'controller_uri', 'tpl' => '{controller_uri}'),
            array('field' => 'controller_uri_action', 'tpl' => '{controller_uri_action}'),
            array('field' => 'module_name', 'tpl' => '{module_name}'),
            array('field' => 'access_privileges', 'tpl' => '{access_privileges}')
        );
        $this->_cmp->cellTipConfig = array('maxWidth' => 400);

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $this->_cmp->rowMenu = array(
            'xtype' => 'mn-grid-rowmenu',
            'items' => array(
                array('text'    => $this->_['rowmenu_edit'],
                      'url'     =>  $this->componentUrl . 'profile/interface/',
                      'iconCls' => 'icon-form-edit'),
                array('xtype'   => 'menuseparator'),
                array('text'    => $this->_['rowmenu_privileges'],
                      'url'     =>  $this->componentUrl . 'privileges/interface/',
                      'icon'    => $this->resourcePath . 'icon-hd-access.png')
            )
        );

        parent::getInterface();
    }
}
?>