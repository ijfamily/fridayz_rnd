<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка доступных компонентов"
 * Пакет контроллеров "Список компонентов"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AGroupAccess_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные списка доступных компонентов
 * 
 * @category   Gear
 * @package    GController_AGroupAccess_Grid
 * @subpackage Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AGroupAccess_Grid_Data extends GController_Grid_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * (для обработки записей таблицы)
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'access_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_controller_access';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_agroups_grid';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS `c`.*,`ca`.*,`cg`.`group_name`,`cm`.`module_name`, `cl`.`controller_name`, `cl`.`controller_about` '
          . 'FROM `gear_controller_access` `ca` '
          . 'JOIN `gear_controllers` `c` USING(`controller_id`) '
          . 'JOIN `gear_controllers_l` `cl` USING(`controller_id`) '
            // группа компонентов
          . 'LEFT JOIN (SELECT `g`.*,`l`.`group_name`,`l`.`group_about` FROM `gear_controller_groups` `g` JOIN `gear_controller_groups_l` `l` USING(`group_id`) '
          . 'WHERE `l`.`language_id`=' . $this->language->get('id') . ') `cg` ON `cg`.`group_id`=`c`.`group_id` '
            // модули
          . 'LEFT JOIN `gear_modules` `cm` ON `cm`.`module_id`=`cg`.`module_id` '
          . 'WHERE `cl`.`language_id`=' . $this->language->get('id') . ' AND '
          . '`ca`.`group_id`=' . $this->_recordId . ' %filter '
          . 'ORDER BY %sort '
          . 'LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            'controller_name' => array('type' => 'string'),
            'group_name' => array('type' => 'string'),
            'controller_path' => array('type' => 'string'),
            'controller_about' => array('type' => 'string'),
            'controller_uri' => array('type' => 'string'),
            'controller_uri_pkg' => array('type' => 'string'),
            'controller_uri_action' => array('type' => 'string'),
            'controller_class' => array('type' => 'string'),
            'controller_enabled' => array('type' => 'string'),
            'controller_visible' => array('type' => 'string'),
            'controller_dashboard' => array('type' => 'string'),
            'controller_clear' => array('type' => 'string'),
            'module_name' => array('type' => 'string'),
            'access_shortcut' => array('type' => 'string'),
            'access_log' => array('type' => 'string'),
            'access_debug' => array('type' => 'string'),
            'access_error' => array('type' => 'string'),
            'access_privileges' => array('type' => 'string'),
        );
    }

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор,
     * используется для инициализации атрибутов контроллера без конструктора
     * 
     * @return void
     */
    public function construct()
    {
        $this->_['privileges'] = GText::get('privileges', 'interface');
    }

    /**
     * Если выбрана моя группа пользователя
     * 
     * @return boolean
     */
    protected function isMyGroup()
    {
        return $this->session->get('group/id') == $this->store->get('record', 0, 'gcontroller_agroups_grid');
    }

    /**
     * Удаление среды компонента
     * 
     * @return void
     */
    protected function storeDelete()
    {
        $query = new GDb_Query();
        // выбранный компонент
        $sql = 'SELECT * '
             . 'FROM `gear_controllers` `c` JOIN `gear_controller_access` `ca` USING(`controller_id`) '
             . 'WHERE `ca`.`access_id` IN (' . $this->uri->id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        while (!$query->eof()) {
            $cnt = $query->next();
            // удалить среду
            $this->store->remove($cnt['controller_class']);
        }
    }

    /**
     * Проверка существования зависимых записей (в каскадное удаление)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        // проверка группы пользователя
        if ($this->isMyGroup()) {
            // удаление среды компонента
            $this->storeDelete();
        }
    }

    /**
     * Удаление среды компонента
     * 
     * @return void
     */
    protected function storeClear()
    {
        $query = new GDb_Query();
        // выбранный компонент
        $sql = 'SELECT * '
             . 'FROM `gear_controllers` `c` JOIN `gear_controller_access` `ca` USING(`controller_id`) '
             . 'WHERE `ca`.`group_id`=' . $this->store->get('record', 0, 'gcontroller_agroups_grid');
        if ($query->execute($sql) === false)
            throw new GSqlException();
        while (!$query->eof()) {
            $cnt = $query->next();
            // удалить среду
            $this->store->remove($cnt['controller_class']);
        }
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        // проверка группы пользователя
        if ($this->isMyGroup()) {
            // удаление среды компонента
            $this->storeClear();
        }

        $query = new GDb_Query();
        //  удаление прав доступа ("gear_controller_access")
        $sql = 'DELETE FROM `gear_controller_access` WHERE `group_id`=' . $this->store->get('record', 0, 'gcontroller_agroups_grid');
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $sql = 'ALTER TABLE `gear_controller_access` auto_increment=1';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }


    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        // формирование списка привилегий
        if (!empty($record['access_privileges'])) {
            $json = json_decode($record['access_privileges'], true);
            $privileges = '';
            $count = sizeof($json);
            for ($i = 0; $i < $count; $i++) {
                $name = $json[$i]['action'];
                if (isset($this->_['privileges'][$name]))
                    $name = $this->_['privileges'][$name];
                if ($count > $i - 1 && $i > 0)
                    $privileges .= ', ' . $name;
                else
                    $privileges .= $name;
            }
            $record['access_privileges'] = $privileges;
        }
        $record['controller_name'] = '<img align="absmiddle" style="margin-right:5px;" src="' . PATH_COMPONENT . $record['controller_uri']. 'resources/icon.png">' . $record['controller_name'];

        return $record;
    }
}
?>