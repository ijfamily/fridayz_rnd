<?php
/**
 * Gear Manager
 *
 * Controller          "Интерфейс привилегий доступных компонентов"
 * Package controllers "Привилегии доступных компонентов"
 * Группа пакетов      "Группы пользователей"
 * Модуль              "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AGroupAccess_Privileges
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс привилегий доступных компонентов
 * 
 * @category   Gear
 * @package    GController_AGroupAccess_Privileges
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AGroupAccess_Privileges_Interface extends GController_Profile_Interface
{
    /**
     * Возращает дополнительные данные для создания интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        parent::getDataInterface();

        $query = new GDb_Query();
        $sql = 'SELECT `cl`.`controller_name` '
             . 'FROM `gear_controllers_l` AS `cl`, `gear_controller_access` AS `ca` '
             . 'WHERE `ca`.`controller_id`=`cl`.`controller_id` AND '
             . '`cl`.`language_id`=' . $_SESSION['language/default/id'] . ' AND '
             . '`ca`.`access_id`=' . $this->_request->id;
        $record = $query->getRecord($sql);
        $data = array('title' => $record['controller_name']);

        return $data;
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('titleEllipsis'   => 80,
                  'gridId'          => 'gcontroller_agroupaccess_grid',
                  'width'           => 527,
                  'height'          => 425,
                  'resizable'       => false,
                  'stateful'        => false,
                  'btnDeleteHidden' => true)
        );

        // вкладка "события"
        $tabPrivileges = array(
            'title'        => $this->_['title_tab_privileges'],
            'iconSrc'      => $this->resourcePath . 'icon-tab-privileges.png',
            'bodyCssClass' => 'mn-tab-panel',
            'items'        => array(
                array('xtype'     => 'mn-field-grid-selector',
                      'gridTo'    => array(
                           'title'     => $this->_['title_assigned'],
                           'height'    => 270,
                           'hideLabel' => true,
                           'name'      => 'access_privileges',
                           'fields'    => array('label', 'action'),
                           'columns'   => array(
                               array('header'    => $this->_['header_label'],
                                     'dataIndex' => 'label',
                                     'width'     => 110,
                                     'sortable'  => true),
                               array('header'    => $this->_['header_action'],
                                     'dataIndex' => 'action',
                                     'width'     => 90,
                                     'sortable'  => true)
                           )
                      ),
                      'gridFrom' => array(
                           'title'     => $this->_['title_privileges'],
                           'height'    => 270,
                           'hideLabel' => true,
                           'name'      => 'controller_privileges',
                           'fields'    => array('label', 'action'),
                           'value'     => $this->isUpdate ? '' : $this->_['data_array'],
                           'columns'   => array(
                               array('header'    => $this->_['header_label'],
                                     'dataIndex' => 'label',
                                     'width'     => 110,
                                     'sortable'  => true),
                               array('header'    => $this->_['header_action'],
                                     'dataIndex' => 'action',
                                     'width'     => 90,
                                     'sortable'  => true)
                           )
                      )
                )
            )
        );

        // вкладка "поля"
        $tabFields = array(
            'title'        => $this->_['title_tab_fields'],
            'iconSrc'      => $this->resourcePath . 'icon-tab-fields.png',
            'bodyCssClass' => 'mn-tab-panel',
            'items'        => array(
                array('xtype'     => 'mn-field-grid-selector',
                      'gridTo'    => array(
                           'title'     => $this->_['title_assigned'],
                           'height'    => 270,
                           'hideLabel' => true,
                           'name'      => 'access_fields',
                           'fields'    => array('label', 'field'),
                           'columns'   => array(
                               array('header'    => $this->_['header_label'],
                                     'dataIndex' => 'label',
                                     'width'     => 110,
                                     'sortable'  => true),
                               array('header'    => $this->_['header_field'],
                                     'dataIndex' => 'field',
                                     'width'     => 90,
                                     'sortable'  => true)
                           )
                      ),
                      'gridFrom' => array(
                           'title'     => $this->_['title_privileges'],
                           'height'    => 270,
                           'hideLabel' => true,
                           'name'      => 'controller_fields',
                           'fields'    => array('label', 'field'),
                           'columns'   => array(
                                array('header'    => $this->_['header_label'],
                                      'dataIndex' => 'label',
                                      'width'     => 110,
                                      'sortable'  => true),
                                array('header'    => $this->_['header_field'],
                                      'dataIndex' => 'field',
                                      'width'     => 90,
                                      'sortable'  => true)
                            )
                      )
                )
            )
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            'xtype'             => 'tabpanel',
            'layoutOnTabChange' => true,
            'activeTab'         => 0,
            //'bodyStyle'         => 'background-color:transparent',
            'style'             => 'padding:3px',
            'enableTabScroll'   => true,
            'anchor'            => '100% 100%',
            'items'             => array($tabPrivileges, $tabFields)
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->add($items);
        $form->url = $this->componentUrl . 'privileges/';

        parent::getInterface();
    }
}
?>