<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_update' => 'Изменение привилегий компонента "%s"',
    // поля формы
    'title_tab_privileges' => 'Привилегии',
    'title_tab_fields'     => 'Поля',
    'title_assigned'       => 'Назначенные',
    'title_privileges'     => 'Доступные',
    'header_label'         => 'Название',
    'header_action'        => 'Событие',
    'header_field'         => 'Поле'
);
?>