<?php
/**
 * Gear Manager
 *
 * Controller          "Данные для интерфейса привилегий доступных компонентов"
 * Package controllers "Привилегии доступных компонентов"
 * Группа пакетов      "Группы пользователей"
 * Модуль              "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AGroupAccess_Privileges
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные для интерфейса привилегий доступных компонентов
 * 
 * @category   Gear
 * @package    GController_AGroupAccess_Privileges
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2012-10-04 12:00:00 Gear Magic$
 */
class GController_AGroupAccess_Privileges_Data extends GController_Profile_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * (для обработки записей таблицы)
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Массив полей таблицы
     *
     * @var array
     */
    public $fields = array('access_privileges', 'access_fields', 'type_id');

    /**
     * Первичный ключ таблицы
     *
     * @var string
     */
    public $idProperty = 'access_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_controller_access';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_agroups_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_profile_update'], $record['controller_name']);
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        $this->fields[] = 'controller_privileges';
        $this->fields[] = 'controller_fields';

        $sql = 'SELECT * '
             . 'FROM `gear_controllers` `c` '
             . 'JOIN `gear_controllers_l` `cl` USING(`controller_id`) '
             . 'JOIN `gear_controller_access` `ca` USING(`controller_id`) '
             . 'WHERE `cl`.`language_id`=' . $this->language->get('id') . ' AND '
             . '`ca`.`access_id`=%id%';

        parent::dataView($sql);
    }

    /**
     * Если выбрана моя группа пользователя
     * 
     * @return boolean
     */
    protected function isMyGroup()
    {
        return $this->session->get('group/id') == $this->store->get('record', 0, 'gcontroller_agroups_grid');
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        // если изменились привилегии доступа
        if (!empty($params['access_privileges'])) {
            $pr = json_decode($params['access_privileges'], true);
            $params['access_privileges'] = GPrivileges::toJson($pr);
            $params['type_id'] = GPrivileges::getTypeAccess($pr);
        } else
            $params['type_id'] = 3;
    }

    /**
     * Обновление среды компонента
     * 
     * @return void
     */
    protected function storeUpdate()
    {
        $query = new GDb_Query();
        // выбранный компонент
        $sql = 'SELECT * '
             . 'FROM `gear_controllers` `c` JOIN `gear_controller_access` `ca` USING(`controller_id`) '
             . 'WHERE `ca`.`access_id`=' . $this->uri->id;
        $cnt = $query->getRecord($sql);
        if ($cnt === false)
            throw new GSqlException();
        // доступные привилегии
        $data = GPrivileges::toStore($cnt['access_privileges']);
        $this->store->set('privileges', $data, $cnt['controller_class']);
        // доступные поля
        $data = GPrivileges::toStore($cnt['access_fields']);
        $this->store->set('fields', $data, $cnt['controller_class']);

        GFactory::getDg()->log($this->store->getAll($cnt['controller_class']));
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        parent::dataUpdate($params);

        // проверка группы пользователя
        if ($this->isMyGroup()) {
            // обновление среды компонента
            $this->storeUpdate();
        }
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        // пересечение привилегий
        $ac = json_decode($record['access_privileges'], true);
        $ac = GPrivileges::to($ac, true);
        $cp = json_decode($record['controller_privileges'], true);
        $cp = GPrivileges::to($cp, true);
        $ci = sizeof($ac);
        for ($i = 0; $i < $ci; $i++) {
            $cj = sizeof($cp);
            for ($j = 0; $j < $cj; $j++) {
                if ($cp[$j]['action'] == $ac[$i]['action']) {
                    array_splice($cp, $j, 1);
                    break;
                }
            }
        }
        if (sizeof($cp) == 0)
            $record['controller_privileges'] = '[]';
        else
            $record['controller_privileges'] = json_encode($cp);
        if (sizeof($ac))
            $record['access_privileges'] = json_encode($ac);

        // пересечение полей
        $ac = json_decode($record['access_fields'], true);
        $cp = json_decode($record['controller_fields'], true);
        $ci = sizeof($ac);
        for ($i = 0; $i < $ci; $i++) {
            $cj = sizeof($cp);
            for ($j = 0; $j < $cj; $j++) {
                if ($cp[$j]['field'] == $ac[$i]['field']) {
                    array_splice($cp, $j, 1);
                    break;
                }
            }
        }
        if (sizeof($cp) == 0)
            $record['controller_fields'] = '[]';
        else
            $record['controller_fields'] = json_encode($cp);

        return $record;
    }
}
?>