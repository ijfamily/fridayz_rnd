<?php
/**
 * Gear Manager
 *
 * Controller          "Изменение записи профиля доступного компонента"
 * Пакет контроллеров "Список доступных компонентов"
 * Группа пакетов     "Компоненты"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AGroupAccess_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Field.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Field');

/**
 * Изменение записи профиля доступного компонента
 * 
 * @category   Gear
 * @package    GController_AGroupAccess_Profile
 * @subpackage Field
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Field.php 2014-08-04 12:00:00 Gear Magic$
 */
final class GController_AGroupAccess_Profile_Field extends GController_Profile_Field
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array(
        'access_shortcut', 'access_log', 'access_debug', 'access_error'
    );

    /**
     * Первичное поле таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'access_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_controller_access';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_agroups_grid';

    /**
     * Обновление среды компонента
     * 
     * @return void
     */
    protected function storeUpdate()
    {
        $query = new GDb_Query();
        // выбранный компонент
        $sql = 'SELECT * '
             . 'FROM `gear_controllers` `c` JOIN `gear_controller_access` `ca` USING(`controller_id`) '
             . 'WHERE `ca`.`access_id`=' . $this->uri->id;
        $cnt = $query->getRecord($sql);
        if ($cnt === false)
            throw new GSqlException();
        // среда компонента
        $data = array(
            'error' => $cnt['access_error'], 'log' => $cnt['access_log'], 'debug' => $cnt['access_debug']
        );
        $this->store->set('trace', $data, $cnt['controller_class']);
        $this->store->setTo('params', 'shortcut', $cnt['access_shortcut'], $cnt['controller_class']);
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        parent::dataUpdate($params);

        // проверка группы пользователя
        if ($this->session->get('group/id') != $this->store->get('record', 'gcontroller_agroups_grid'))
            return;
        // обновление среды компонента
        $this->storeUpdate();
    }
}
?>