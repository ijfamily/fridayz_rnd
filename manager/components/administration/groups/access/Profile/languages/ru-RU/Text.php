<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Создание записи "Доступный компонент"',
    'title_profile_update' => 'Изменение записи "%s"',
    // поля формы
    'title_fldset_cmp'      => 'Компонент',
    'title_fldset_debug'    => 'Отладка',
    'label_access_shortcut' => 'Ярлык',
    'tip_access_shortcut'   => 'Ярлык на рабочем столе',
    'label_controller_name' => 'Название',
    'label_access_log'      => 'Журнал',
    'label_access_debug'    => 'Отладчик',
    'label_access_error'    => 'Ошибки',
    'tip_access_log'        => 'Вести журнал событий совершаемых компонентом в виде назначенных привилегий группе пользователя',
    'tip_access_debug'      => 'Использовать отладчик Терминала для просмотр всех действий пользователя (необходим FireBug)',
    'tip_access_error'      => 'Вывод детальных ошибок компонента',
    // сообщения
    'msg_already_added' => 'В выбранной Вами группе пользователя уже добавлены все компоненты!'
);
?>