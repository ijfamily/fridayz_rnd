<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Creating a Record "Affordable component"',
    'title_profile_update' => 'Update record "%s"',
    // поля формы
    'title_fldset_cmp'      => 'Component',
    'title_fldset_debug'    => 'Debugger',
    'label_access_shortcut' => 'Shortcut',
    'tip_access_shortcut'   => 'Shortcut on desktop',
    'label_controller_name' => 'Name',
    'label_access_log'      => 'Log',
    'label_access_debug'    => 'Debug',
    'label_access_error'    => 'Error',
    'tip_access_log'        => 'Keep a log of events committed component in the form of designated user group privileges',
    'tip_access_debug'      => 'Gear use the debugger to view all user actions (requires FireBug)',
    'tip_access_error'      => 'Output detailed error component',
    // сообщения
    'msg_already_added' => 'В выбранной Вами группе пользователя уже добавлены все компоненты!'
);
?>