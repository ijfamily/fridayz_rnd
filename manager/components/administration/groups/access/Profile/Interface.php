<?php
/**
 * Gear Manager
 *
 * Controller          "Интерфейс профиля доступных компонентов"
 * Package controllers "Профиль доступных компонентов"
 * Группа пакетов      "Группы пользователей системы"
 * Модуль              "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AGroupAccess_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2014-08-04 12:00:00 Gear Magic$
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля доступных компонентов
 * 
 * @category   Gear
 * @package    GController_AGroupAccess_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AGroupAccess_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Возращает дополнительные данные для формирования интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        // если состояние формы "правка"
        if ($this->isUpdate) return false;

        // соединения с базой данных
        GFactory::getDb()->connect();
        $query = new GDb_Query();
        // проверка существования компонентов
        $sql = 'SELECT COUNT(*) `count` '
             . 'FROM `gear_controllers` `c` '
             . 'WHERE `c`.`controller_id` NOT IN (SELECT `controller_id` '
             . 'FROM `gear_controller_access` WHERE `group_id`=' . $this->store->get('record', 0, 'gcontroller_agroups_grid') . ')';
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        if (empty($rec['count']))
            throw new GException('Warning', $this->_['msg_already_added']);
    }

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // дополнительные данные для формирования интерфейса
        $this->getDataInterface();

        // Ext_Window (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'titleEllipsis' => 70,
                  'gridId'        => 'gcontroller_agroupaccess_grid',
                  'width'         => 400,
                  'autoHeight'    => true,
                  'resizable'     => false,
                  'stateful'      => false)
        );

        // поля "компонент"
        $tpl = '<tpl for="."><div ext:qtip="{name}" class="x-combo-list-item">'
             . '<img width="16px" height="16px" align="absmiddle" src="' 
             . '{data}resources/icon.png" style="margin-right:5px">{name}</div></tpl>';
        $fieldsetCmp = array(
            'xtype'      => 'fieldset',
            'title'      => $this->_['title_fldset_cmp'],
            'labelWidth' => 65,
            'autoHeight' => true,
            'items'      => array()
        );
        // состояние профиля "вставка"
        if ($this->isInsert)
            $fieldsetCmp['items'][] = array(
                'dynamicFields' => array('id' => 'test'),
                'xtype'      => 'mn-field-combo',
                'tpl'        => $tpl,
                'fieldLabel' => $this->_['label_controller_name'],
                'name'       => 'controller_id',
                'pageSize'   => 50,
                'anchor'     => '100%',
                'hiddenName' => 'controller_id',
                'allowBlank' => false,
                'store'      => array(
                    'xtype' => 'jsonstore',
                    'url'   => $this->componentUrl . 'combo/trigger/?name=controllers'
                )
            );
        $fieldsetCmp['items'][] = array(
            'xtype'      => 'mn-field-chbox',
            'fieldLabel' => $this->_['label_access_shortcut'],
            'labelTip'   => $this->_['tip_access_shortcut'],
            'name'       => 'access_shortcut',
            'default'    => 1
         );
        // поля "отладка"
        $fieldsetDebug = array(
            'xtype'      => 'fieldset',
            'title'      => $this->_['title_fldset_debug'],
            'labelWidth' => 65,
            'autoHeight' => true,
            'items'      => array(
                array('xtype'      => 'mn-field-chbox',
                      'fieldLabel' => $this->_['label_access_log'],
                      'labelTip'   => $this->_['tip_access_log'],
                      'name'       => 'access_log',
                      'default'    => 1),
                
                array('xtype'      => 'mn-field-chbox',
                      'fieldLabel' => $this->_['label_access_debug'],
                      'labelTip'   => $this->_['tip_access_debug'],
                      'name'       => 'access_debug',
                      'default'    => 0),
                array('xtype'      => 'mn-field-chbox',
                      'fieldLabel' => $this->_['label_access_error'],
                      'labelTip'   => $this->_['tip_access_error'],
                      'name'       => 'access_error',
                      'default'    => 1)
            )
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype' => 'hidden',
                  'name'  => 'group_id',
                  'value' => $this->store->get('record', 0, 'gcontroller_agroups_grid')),
            $fieldsetCmp, $fieldsetDebug
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->addItems($items);
        $form->url = $this->componentUrl . 'profile/';

        parent::getInterface();
    }
}
?>