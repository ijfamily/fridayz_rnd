<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля группы пользователя
 * Пакет контроллеров "Профиль группы пользователя"
 * Группа пакетов     "Группы пользователей"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Controller_AGroupAccess_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные профиля группы пользователя
 * 
 * @category   Gear
 * @package    Controller_AGroupAccess_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AGroupAccess_Profile_Data extends GController_Profile_Data
{
    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * (для обработки записей таблицы)
     * 
     * @var boolean
     */
    public $isSysFields = true;

    /**
     * Массив полей таблицы 
     *
     * @var array
     */
    public $fields = array(
        'controller_id', 'group_id', 'access_shortcut', 'access_log', 'access_debug', 'access_error',
        'access_privileges'
    );

    /**
     * Первичный ключ таблицы
     *
     * @var string
     */
    public $idProperty = 'access_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_controller_access';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_agroups_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_profile_update'], $record['controller_name']);
    }

    /**
     * Если выбрана моя группа пользователя
     * 
     * @return boolean
     */
    protected function isMyGroup()
    {
        return $this->session->get('group/id') == $this->store->get('record', 0, 'gcontroller_agroups_grid');
    }

    /**
     * Создание среды компонента
     * 
     * @return void
     */
    protected function storeInsert()
    {
        $query = new GDb_Query();
        // выбранный компонент
        $sql = 'SELECT * '
             . 'FROM `gear_controllers` `c` JOIN `gear_controller_access` `ca` USING(`controller_id`) '
             . 'WHERE `ca`.`access_id`=' . $this->_recordId;
        $cnt = $query->getRecord($sql);
        if ($cnt === false)
            throw new GSqlException();
        // идентификатор класса компонента
        $classId = $cnt['controller_class'];
        // создаем среду
        $this->store->create($classId);
        // среда компонента
        $data = array(
            'error' => $cnt['access_error'], 'log' => $cnt['access_log'], 'debug' => $cnt['access_debug']
        );
        $this->store->set('trace', $data, $classId);
        $data = array(
            'id'         => $cnt['controller_id'],
            'group'      => $cnt['group_id'],
            'class'      => $cnt['controller_class'],
            'uri'        => $cnt['controller_uri'],
            'componentUri' => $cnt['controller_uri_pkg'],
            'actionUri'    => $cnt['controller_uri_action'],
            'enabled'    => $cnt['controller_enabled'],
            'visible'    => $cnt['controller_visible'],
            'clear'      => $cnt['controller_clear'],
            'shortcut'   => $cnt['access_shortcut']
        );
        $this->store->set('params', $data, $classId);
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        // состояние профиля "вставка"
        if ($this->isInsert) {
            $params['access_privileges'] = GPrivileges::defaultToJson(array('root'));
            
            //'[{"label":"","action":"root"}]';
        }
    }

    /**
     * Вставка данных
     * 
     * @param  array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataInsert($params = array())
    {
        parent::dataInsert($params);

        // проверка группы пользователя
        if ($this->isMyGroup()) {
            // создание среды компонента
            $this->storeInsert();
        }
    }

    /**
     * Обновление среды компонента
     * 
     * @return void
     */
    protected function storeUpdate()
    {
        $query = new GDb_Query();
        // выбранный компонент
        $sql = 'SELECT * '
             . 'FROM `gear_controllers` `c` JOIN `gear_controller_access` `ca` USING(`controller_id`) '
             . 'WHERE `ca`.`access_id`=' . $this->uri->id;
        $cnt = $query->getRecord($sql);
        if ($cnt === false)
            throw new GSqlException();
        // среда компонента
        $data = array(
            'error' => $cnt['access_error'], 'log' => $cnt['access_log'], 'debug' => $cnt['access_debug']
        );
        $this->store->set('trace', $data, $cnt['controller_class']);
        $this->store->setTo('params', 'shortcut', $cnt['access_shortcut'], $cnt['controller_class']);
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        parent::dataUpdate($params);

        // проверка группы пользователя
        if ($this->isMyGroup()) {
            // обновление среды компонента
            $this->storeUpdate();
        }
    }

    /**
     * Удаление среды компонента
     * 
     * @return void
     */
    protected function storeDelete()
    {
        $query = new GDb_Query();
        // выбранный компонент
        $sql = 'SELECT * '
             . 'FROM `gear_controllers` `c` JOIN `gear_controller_access` `ca` USING(`controller_id`) '
             . 'WHERE `ca`.`access_id`=' . $this->uri->id;
        $cnt = $query->getRecord($sql);
        if ($cnt === false)
            throw new GSqlException();
        // удалить среду
        $this->store->remove($cnt['controller_class']);
    }

    /**
     * Проверка существования зависимых записей (в каскадное удаление)
     * 
     * @return void
     */
    protected function isDataDependent()
    {
        // проверка группы пользователя
        if ($this->isMyGroup()) {
            // удаление среды компонента
            $this->storeDelete();
        }
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        $sql = 'SELECT `ca`.*, `cl`.`controller_name` '
             . 'FROM `gear_controller_access` AS `ca`, `gear_controllers_l` AS `cl` '
             . 'WHERE `ca`.`controller_id`=`cl`.`controller_id` AND '
             . '`cl`.`language_id`=' . $this->language->get('id') . ' AND '
             . '`ca`.`access_id`=%id%';

        parent::dataView($sql);
    }
}
?>