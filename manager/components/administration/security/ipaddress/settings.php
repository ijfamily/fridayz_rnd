<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Заблокированные ip адреса"
 * Группа пакетов     "Безопасность"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YIpAddresses
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Заблокированные ip адреса
 * Описание: Заблокированные ip адреса
 * ID класса: gcontroller_yipaddresses_grid
 * Группа: Администрирование / Пользователи
 * Очищать: нет
 * Ресурс
 *    Пакет: administration/security/ipaddress/
 *    Контроллер: administration/security/ipaddress/Grid/
 *    Интерфейс: grid/interface/
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске: нет
 */

return array(
    // {Y}- модуль "System" {IP addresses} - пакет контроллеров "IpAddresses" -> YIpAddresses
    'clsPrefix' => 'YIpAddresses'
);
?>