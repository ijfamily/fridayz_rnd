<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка заблокированных ip адресов"
 * Пакет контроллеров "Заблокированные ip адреса"
 * Группа пакетов     "Безопасность"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YIpAddresses_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные списка заблокированных ip адресов
 * 
 * @category   Gear
 * @package    GController_YIpAddresses_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YIpAddresses_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'secure_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_secure_ipaddress';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS * FROM `gear_secure_ipaddress` '
          . 'WHERE 1 %filter ORDER BY %sort LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            'secure_ipaddress' => array('type' => 'string'),
            'secure_disabled' => array('type' => 'string'),
            'secure_description' => array('type' => 'string'),
            'secure_date' => array('type' => 'string')
        );
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        // удаление записей таблицы "gear_secure_ipaddress"
        $table = new GDb_Table('gear_secure_ipaddress', 'secure_id');
        if ($table->clear(true) !== true)
            throw new GSqlException();
    }
}
?>