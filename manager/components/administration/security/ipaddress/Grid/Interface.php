<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка заблокированных ip адресов"
 * Пакет контроллеров "Заблокированные ip адреса"
 * Группа пакетов     "Безопасность"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YIpAddresses_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс списка заблокированных ip адресов
 * 
 * @category   Gear
 * @package    GController_YIpAddresses_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YIpAddresses_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'secure_id';

    /**
     * Вид сортировки
     * (указывается в настройках Ext.data.JsonStore.sortInfo.dir)
     *
     * @var string
     */
    public $dir = 'DESC';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'secure_ipaddress';

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('name' => 'secure_ipaddress', 'type' => 'string'),
            array('name' => 'secure_disabled', 'type' => 'string'),
            array('name' => 'secure_description', 'type' => 'string'),
            array('name' => 'secure_date', 'type' => 'string')
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $settings = $this->session->get('user/settings');
        $this->columns = array(
            array('xtype'     => 'numbercolumn'),
            array('xtype'     => 'rowmenu'),
            array('dataIndex' => 'secure_ipaddress',
                  'header'    =>$this->_['header_secure_ipaddress'],
                  'width'     => 115,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('xtype'     => 'booleancolumn',
                  'dataIndex' => 'secure_disabled',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-disabled.png" align="absmiddle">',
                  'tooltip'   =>$this->_['header_secure_disabled'],
                  'align'     => 'center',
                  'width'     => 45,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'secure_description',
                  'header'    =>$this->_['header_secure_description'],
                  'width'     => 200,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('xtype'     => 'datetimecolumn',
                  'dataIndex' => 'secure_date',
                  'frmDate'   => $settings['format/date'],
                  'header'    => $this->_['header_secure_date'],
                  'width'     => 120,
                  'sortable'  => true)
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_grid'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_grid'],
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'profileUrl'    => $this->componentUrl . 'profile/')
        );

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // группа кнопок "правка" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "добавить" (ExtJS class "Manager.button.InsertData")
        $group->items->add(array('xtype' => 'mn-btn-insert-data', 'gridId' => $this->classId));
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' =>$this->classId));
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'      => 'mn-btn-clear-data',
                  'msgConfirm' => $this->_['msg_btn_clear'],
                  'gridId'     => $this->classId,
                  'isN'        => true)
        );
        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = 'component-disabled-ip-addresses';
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "фильтр" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Filter(
            array('title'  => $this->_['title_buttongroup_filter'],
                  'gridId' => $this->classId)
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(0);
        $btn['gridId'] = $this->classId;
        $btn['url'] = $this->componentUrl . 'search/data/';

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $cellInfo =
            '<div class="mn-grid-cell-tooltip-tl">{secure_ipaddress}</div>'
          . '<div class="mn-grid-cell-tooltip">'
          . '<em>' . $this->_['header_secure_disabled'] . '</em>: <b>{secure_disabled}</b><br>'
          . '<em>' . $this->_['header_secure_date'] . '</em>: <b>{secure_date}</b><br>'
          . '<em>' . $this->_['header_secure_description'] . '</em>: <b>{secure_description}</b>'
          . '</div>';
        $this->_cmp->cellTips = array(
            array('field' => 'secure_ipaddress', 'tpl' => $cellInfo),
            array('field' => 'secure_description', 'tpl' => '{secure_description}')
        );

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $this->_cmp->rowMenu = array(
            'xtype' => 'mn-grid-rowmenu',
            'items' => array(
                array('text'    => $this->_['rowmenu_edit'],
                      'iconCls' => 'icon-form-edit',
                      'url'     => $this->componentUrl . 'profile/interface/')
            )
        );

        parent::getInterface();
    }
}
?>