<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Заблоковані IP-адреси',
    'tooltip_grid' => 'ip-адреса запрещенные для доступа к системе',
    'rowmenu_edit' => 'Редагувати',
    // панель управления
    'title_buttongroup_edit'    => 'Правка',
    'title_buttongroup_cols'    => 'Стовпці',
    'title_buttongroup_filter'  => 'Фільтр',
    'msg_btn_delete'           => 'Вы действительно желаете удалить записи <span class="mn-msg-delete">'
                                . '"Заблокированные IP-адреса"</span> ?',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">'
                                . '"Заблокированные IP-адреса"</span> ?',
    // столбцы
    'header_secure_ipaddress'   => 'IP-адреса',
    'header_secure_disabled'    => 'Заблоковано',
    'header_secure_description' => 'Причина',
    'header_secure_date'        => 'Дата'
);
?>