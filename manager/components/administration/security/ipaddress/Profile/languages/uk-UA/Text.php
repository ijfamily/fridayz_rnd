<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Створення запису "IP-адрес"',
    'title_profile_update' => 'Зміна запису "%s"',
    // поля
    'label_secure_ipaddress'    => 'IP-адрес',
    'label_secure_disabled'     => 'Заблокувати',
    'label_secure_description'  => 'Причина',
    // тип
    'data_secure_disabled' => array('Ні','Так')
);
?>