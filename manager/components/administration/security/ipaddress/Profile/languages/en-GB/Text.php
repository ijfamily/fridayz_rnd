<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Create an "IP-address"',
    'title_profile_update' => 'Change the record "%s"',
    // поля
    'label_secure_ipaddress'    => 'IP-address',
    'label_secure_disabled'     => 'Blocked',
    'label_secure_description'  => 'Cause',
    // тип
    'data_secure_disabled' => array('No', 'Yes'),
);
?>