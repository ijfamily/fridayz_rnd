<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля заблокированного ip адреса"
 * Пакет контроллеров "Заблокированные ip адреса"
 * Группа пакетов     "Безопасность"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YIpAddresses_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля заблокированного ip адреса
 * 
 * @category   Gear
 * @package    GController_YIpAddresses_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YIpAddresses_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'titleEllipsis' => 45,
                  'gridId'        => 'gcontroller_yipaddresses_grid',
                  'width'         => 370,
                  'height'        => 150,
                  'resizable'     => false,
                  'stateful'      => false,
                  'buttonsAdd'    => array(
                      array('xtype'   => 'mn-btn-widget-form',
                            'text'    => $this->_['text_btn_help'],
                            'url'     => ROUTER_SCRIPT . 'system/guide/guide/' . ROUTER_DELIMITER . 'modal/interface/?doc=component-disabled-ip-addresses',
                            'iconCls' => 'icon-item-info')
                  )
            )
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype'      => 'textfield',
                  'fieldLabel' =>$this->_['label_secure_ipaddress'],
                  'name'       => 'secure_ipaddress',
                  'maxLength'  => 15,
                  'width'      => 180,
                  'allowBlank' => false),
            array('xtype'      => 'textfield',
                  'fieldLabel' =>$this->_['label_secure_description'],
                  'name'       => 'secure_description',
                  'maxLength'  => 255,
                  'width'      => 180,
                  'allowBlank' => true),
            array('xtype'         => 'combo',
                  'fieldLabel'    =>$this->_['label_secure_disabled'],
                  'name'          => 'secure_disabled',
                  'editable'      => false,
                  'width'         => 65,
                  'typeAhead'     => true,
                  'triggerAction' => 'all',
                  'mode'          => 'local',
                  'allowBlank'    => false,
                  'store'         => array('xtype'  => 'arraystore',
                                           'fields' => array('value', 'display'),
                                           'data'   => array(array(0,$this->_['data_secure_disabled'][0]),
                                                             array(1,$this->_['data_secure_disabled'][1]))),
                  'hiddenName'    => 'secure_disabled',
                  'valueField'    => 'value',
                  'displayField'  => 'display',
                  'allowBlank'    => false)
         );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->labelWidth = 100;
        $form->items->addItems($items);
        $form->url = $this->componentUrl . 'profile/';

        parent::getInterface();
    }
}
?>