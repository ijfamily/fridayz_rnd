<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля заблокированного ip адреса"
 * Пакет контроллеров "Заблокированные ip адреса"
 * Группа пакетов     "Безопасность"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YIpAddresses_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные профиля заблокированного ip адреса
 * 
 * @category   Gear
 * @package    GController_YIpAddresses_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YIpAddresses_Profile_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы
     *
     * @var array
     */
    public $fields = array('secure_ipaddress', 'secure_disabled', 'secure_date', 'secure_description');

    /**
     * Первичный ключ таблицы
     *
     * @var string
     */
    public $idProperty = 'secure_id';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_yipaddresses_grid';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_secure_ipaddress';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_profile_update'], $record['secure_ipaddress']);
    }
}
?>