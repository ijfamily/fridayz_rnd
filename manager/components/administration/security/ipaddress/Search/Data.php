<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск в списке заблокированных ip адресов"
 * Пакет контроллеров "Заблокированные ip адреса"
 * Группа пакетов     "Безопасность"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YIpAddresses_Search
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Интерфейс списка заблокированных ip адресов
 * 
 * @category   Gear
 * @package    GController_YIpAddresses_Search
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YIpAddresses_Search_Data extends GController_Search_Data
{
    /**
     * дентификатор компонента (списка) к которому применяется поиск
     *
     * @var string
     */
    public $gridId = 'gcontroller_yipaddresses_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_yipaddresses_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор,
     * используется для инициализации атрибутов контроллера без конструктора
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'secure_ipaddress',
                  'label'     =>$this->_['header_secure_ipaddress']),
            array('dataIndex' => 'secure_disabled',
                  'label'     =>$this->_['header_secure_disabled']),
            array('dataIndex' => 'secure_description',
                  'label'     =>$this->_['header_secure_description']),
            array('dataIndex' => 'secure_date',
                  'label'     =>$this->_['header_secure_date'])
        );
    }
}
?>