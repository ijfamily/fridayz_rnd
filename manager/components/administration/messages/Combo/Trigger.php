<?php
/**
 * Gear Manager
 *
 * Контроллер         "Триггер выпадающего списка для обработки данных"
 * Пакет контроллеров "Сообщения пользователей"
 * Группа пакетов     "Сообщения пользователей"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AMessages_Combo
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Combo/Trigger');

/**
 * Триггер выпадающего списка для обработки данных
 * 
 * @category   Gear
 * @package    GController_AMessages_Combo
 * @subpackage Trigger
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AMessages_Combo_Trigger extends GController_Combo_Trigger
{
    /**
     * Возращает пользователей
     * 
     * @return void
     */
    protected function queryUsers()
    {
        $table = new GDb_Table('gear_user_profiles', 'profile_id');
        $table->setStart($this->_start);
        $table->setLimit($this->_limit);
        $sql = 'SELECT SQL_CALC_FOUND_ROWS `p`.* '
             . 'FROM `gear_user_profiles` `p` JOIN `gear_users` `u` USING(`user_id`) WHERE '
             . ($this->_query ? "`profile_name` LIKE '{$this->_query}%' " : '1')
             . ' AND `user_id`<>' . $this->session->get('user_id')
             . ' ORDER BY `profile_name` ASC '
             . 'LIMIT %limit';
        $table->query($sql);
        $data = array();
        if (!$this->_isString)
            $data[] = array('id' => -1, 'name' => 'Все');
        while (!$table->query->eof()) {
            $record = $table->query->next();
            $data[] = array('id' => $this->_isString ? $record['profile_name'] : $record['user_id'], 'name' => $record['profile_name']);
        }
        $this->response->set('totalCount', $table->query->getFoundRows());
        $this->response->success = true;
        $this->response->data = $data;
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $name название триггера
     * @return void
     */
    protected function dataView($name)
    {
        parent::dataView($name);

        // имя триггера
        switch ($name) {
            // пользователи
            case 'users':
                    $this->queryUsers();
                break;
        }
    }
}
?>