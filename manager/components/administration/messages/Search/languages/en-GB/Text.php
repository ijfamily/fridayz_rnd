<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'search_title' => 'Search in the list "Visitor messages"',
    // поля
    'header_msg_send_date'    => 'Sent',
    'header_msg_receive_date' => 'Received',
    'header_profile_name'     => 'Recipient',
    'header_msg_read'         => 'Read',
    'header_msg_text'         => 'Text'
);
?>