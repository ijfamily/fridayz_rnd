<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск сообщений"
 * Пакет контроллеров "Поиск сообщений"
 * Группа пакетов     "Сообщения пользователей"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AMessages_Search
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2013-08-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск сообщений
 * 
 * @category   Gear
 * @package    GController_AMessages_Search
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2013-08-01 12:00:00 Gear Magic $
 */
final class GController_AMessages_Search_Data extends GController_Search_Data
{
    /**
     * дентификатор компонента (списка) к которому применяется поиск
     *
     * @var string
     */
    public $gridId = 'gcontroller_amessages_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_amessages_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор
     * Используется для инициализации атрибутов контроллер не переданного в конструктор
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'msg_send_date', 'label' => $this->_['header_msg_send_date']),
            array('dataIndex' => 'msg_receive_date', 'label' => $this->_['header_msg_receive_date']),
            array('dataIndex' => 'profile_name', 'label' => $this->_['header_profile_name']),
            array('dataIndex' => 'msg_read', 'label' => $this->_['header_msg_read']),
            array('dataIndex' => 'msg_text', 'label' => $this->_['header_msg_text']),
        );
    }
}
?>