<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск сообщений"
 * Пакет контроллеров "Поиск сообщений"
 * Группа пакетов     "Сообщения пользователей"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AMessages_Search
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Interface');

/**
 * Поиск сообщений
 * 
 * @category   Gear
 * @package    GController_AMessages_Search
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2013-07-01 12:00:00 Gear Magic $
 */
final class GController_AMessages_Search_Interface extends GController_Search_Interface
{
    /**
     * Идентификатор DOM компонента (списка)
     *
     * @var string
     */
    public $gridId = 'gcontroller_amessages_grid';

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // Ext_Window (ExtJS class "Manager.window.DataSearch")
        $this->_cmp->setProps(
            array('title'       => $this->_['search_title'],
                  'iconSrc'     => $this->resourcePath . 'icon.png',
                  'gridId'      => $this->gridId,
                  'btnSearchId' => $this->gridId . '-bnt_search',
                  'url'         => $this->componentUrl . 'search/data/')
        );

        // виды полей для поиска записей
        $this->_cmp->editorFields = array(
          array('xtype' => 'datefield', 'format' => 'd-m-Y'),
          array('xtype' => 'datefield', 'format' => 'd-m-Y'),
          array('xtype'      => 'mn-field-combo',
                'name'       => 'user_id',
                'editable'   => false,
                'pageSize'   => 50,
                'width'      => 260,
                'hiddenName' => 'user_id',
                'allowBlank' => true,
                'store'      => array(
                    'xtype' => 'jsonstore',
                    'url'   => $this->componentUrl . 'combo/trigger/?name=users&string=1'
                )
          ),
          array('xtype' => 'datefield', 'format' => 'd-m-Y'),
          array('xtype' => 'mn-field-combo-logic'),
          array('xtype' => 'textfield')
        );

        parent::getInterface();
    }
}
?>