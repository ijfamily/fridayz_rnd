<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля сообщения"
 * Пакет контроллеров "Сообщения пользователей"
 * Группа пакетов     "Сообщения пользователей"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AMessages_Info
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные профиля сообщения
 * 
 * @category   Gear
 * @package    GController_AMessages_Info
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AMessages_Info_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array(
        'profile_name', 'msg_text', 'msg_read', 'msg_send_date', 'msg_send_time',
        'msg_receive_date', 'msg_receive_time'
    );

    /**
     * Первичный ключ таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'msg_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_user_messages';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_amessages_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return $this->_['title_profile_update'];
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        $sql = 'SELECT `m`.*, `p`.`profile_name` FROM `gear_user_messages` `m` '
             . 'LEFT JOIN `gear_user_profiles` `p` ON `m`.`sender_id`=`p`.`user_id` '
             . 'WHERE `m`.`msg_id`=%id%';

        parent::dataView($sql);
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        $record['msg_send_date'] = date('d-m-Y', strtotime($record['msg_send_date'])) . ' '
                                 . $record['msg_send_time'];
        if ($record['msg_receive_date'])
            $record['msg_receive_date'] = date('d-m-Y', strtotime($record['msg_receive_date'])) . ' '
                                        . $record['msg_receive_time'];

        return $record;
    }
}
?>