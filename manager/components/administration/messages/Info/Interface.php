<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля сообщения"
 * Пакет контроллеров "Сообщения пользователей"
 * Группа пакетов     "Сообщения пользователей"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AMessages_Info
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля сообщения
 * 
 * @category   Gear
 * @package    GController_AMessages_Info
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AMessages_Info_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_profile_insert'],
                  'titleEllipsis' => 50,
                  'modal'         => false,
                  'width'         => 350,
                  'autoHeight'    => true,
                  'state'         => 'info',
                  'resizable'     => false,
                  'stateful'      => false)
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
           array('xtype'      => 'textfield',
                 'fieldLabel' => $this->_['label_profile_name'],
                 'name'       => 'profile_name',
                 'anchor'     => '100%',
                 'readOnly'   => true),
           array('xtype'      => 'textfield',
                 'fieldLabel' => $this->_['label_receive_date'],
                 'name'       => 'msg_receive_date',
                 'width'      => 150,
                 'readOnly'   => true),
           array('xtype'      => 'textfield',
                 'fieldLabel' => $this->_['label_send_date'],
                 'name'       => 'msg_send_date',
                 'width'      => 150,
                 'readOnly'   => true),
            array('xtype'            => 'htmleditor',
                  'anchor'           => '100%',
                  'hideLabel'        => true,
                  'enableLists'      => false,
                  'enableSourceEdit' => false,
                  'enableFont'       => false,
                  'enableAlignments' => false,
                  'enableLinks'      => false,
                  'readOnly'         => true,
                  'height'           => 200)
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->addItems($items);
        $form->labelWidth = 87;
        $form->url = $this->componentUrl . 'info/';

        parent::getInterface();
    }
}
?>