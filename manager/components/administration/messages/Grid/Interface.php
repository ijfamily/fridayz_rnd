<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка сообщений пользователей"
 * Пакет контроллеров "Сообщения пользователей"
 * Группа пакетов     "Сообщения пользователей"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AMessages_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс списка сообщений пользователей
 * 
 * @category   Gear
 * @package    GController_AMessages_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AMessages_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'msg_id';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'msg_send_date';

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // дата и время отправления
            array('name' => 'msg_send_date', 'type' => 'string'),
            array('name' => 'msg_send_time', 'type' => 'string'),
            // получатель
            array('name' => 'profile_name', 'type' => 'string'),
            // дата и время получателя
            array('name' => 'msg_receive_date', 'type' => 'string'),
            array('name' => 'msg_receive_time', 'type' => 'string'),
            // прочитано
            array('name' => 'msg_read', 'type' => 'boolean'),
            // текст сообщения
            array('name' => 'msg_text', 'type' => 'string')
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $this->columns = array(
            array('xtype'     => 'numbercolumn'),
            array('xtype'     => 'rowmenu'),
            array('xtype'     => 'datetimecolumn',
                  'dataIndex' => 'msg_send_date',
                  'timeIndex' => 'msg_send_time',
                  'header'    => '<em class="mn-grid-hd-details"></em>' . $this->_['header_msg_send_date'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'date')),
            array('dataIndex' => 'profile_name',
                  'header'    => $this->_['header_profile_name'],
                  'width'     => 170,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('xtype'     => 'datetimecolumn',
                  'dataIndex' => 'msg_receive_date',
                  'timeIndex' => 'msg_receive_time',
                  'header'    => $this->_['header_msg_receive_date'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'date')),
            array('xtype'     => 'booleancolumn',
                  'dataIndex' => 'msg_read',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-msg.png" align="absmiddle">',
                  'tooltip'   => $this->_['header_msg_read'],
                  'align'     => 'center',
                  'width'     => 45,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean')),
            array('dataIndex' => 'msg_text',
                  'header'    => $this->_['header_msg_text'],
                  'width'     => 250,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_grid'],
                  'titleTpl'      => $this->_['title_grid_tpl'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'progressLoad'  => true,
                  'profileUrl'    => $this->componentUrl . 'profile/')
        );

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        //панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "правка" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "добавить" (ExtJS class "Manager.button.InsertData")
        $group->items->add(
            array('xtype'   => 'mn-btn-insert-data',
                  'iconCls' => '',
                  'text'    => $this->_['text_btn_send'],
                  'tooltip' => $this->_['tooltip_btn_send'],
                  'icon'    => $this->resourcePath . 'icon-btn-send.png',
                  'gridId'  => $this->classId)
        );
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' => $this->classId));
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array(
                'xtype'      => 'mn-btn-clear-data',
                'msgConfirm' => $this->_['msg_btn_clear'],
                'gridId'     => $this->classId,
                'isN'        => true
            )
        );
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = 'component-users-messages';
        $this->_cmp->tbar->items->add($group);

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $this->_cmp->cellTips = array(
            array('field' => 'msg_send_date',
                  'tpl'   => '<div class="mn-grid-cell-tooltip-tl">{msg_send_date} {msg_send_time}</div>'
                           . $this->_['header_msg_receive_date'] . ': <b>{msg_receive_date} {msg_receive_time}</b><br>'
                           . $this->_['header_profile_name'] . ': <b>{profile_name}</b>'
                           . '</div>'
            ),
            array('field' => 'profile_name', 'tpl' => '{profile_name}'),
            array('field' => 'msg_text', 'tpl' => '{msg_text}')
        );

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $this->_cmp->rowMenu = array(
            'xtype' => 'mn-grid-rowmenu',
            'items' => array(
                array('text' => $this->_['rowmenu_info'],
                      'icon' => $this->resourcePath . 'icon-item-info.png',
                      'url'  => $this->componentUrl . 'info/interface/')
            )
        );

        parent::getInterface();
    }
}
?>