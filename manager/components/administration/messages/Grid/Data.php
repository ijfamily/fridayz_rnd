<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка сообщений пользователей"
 * Пакет контроллеров "Сообщения пользователей"
 * Группа пакетов     "Сообщения пользователей"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AMessages_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные списка сообщений пользователей
 * 
 * @category   Gear
 * @package    GController_AMessages_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AMessages_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'msg_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_user_messages';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS `m`.*, `p`.`profile_name` FROM `gear_user_messages` `m` '
          . 'LEFT JOIN `gear_user_profiles` `p` ON `m`.`receiver_id`=`p`.`user_id` '
          . 'WHERE `m`.`sender_id`=' . $this->session->get('user_id') . ' %filter '
          . 'ORDER BY %sort '
          . 'LIMIT %limit';

        $this->fields = array(
            // дата и время отправления
            'msg_send_date' => array('name' => 'msg_send_date'),
            'msg_send_time' => array('name' => 'msg_send_time'),
            // получатель
            'profile_name' => array('name' => 'profile_name'),
            // дата и время получателя
            'msg_receive_date' => array('name' => 'msg_receive_date'),
            'msg_receive_time' => array('name' => 'msg_receive_time'),
            // прочитано
            'msg_read' => array('name' => 'msg_read'),
            // текст сообщения
            'msg_text' => array('name' => 'msg_text')
        );
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        $query = new GDb_Query();
        // удаление записей таблицы "gear_user_messages" (сообщения)
        $sql = 'DELETE FROM `gear_user_messages` WHERE `sender_id`=' . $this->session->get('user_id');
        if ($query->execute($sql) !== true)
            throw new GException('Data processing error', 'Query error (Internal Server Error)');
        $sql = 'ALTER TABLE `gear_user_messages` auto_increment=1';
        if ($query->execute($sql) !== true)
            throw new GException('Data processing error', 'Query error (Internal Server Error)');
    }
}
?>