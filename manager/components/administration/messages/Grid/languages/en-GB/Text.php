<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'     => 'Messages',
    'title_grid_tpl' => 'Users messages in the system',
    'rowmenu_info'   => 'Info',
    // панель управления
    'title_buttongroup_edit'   => 'Edit',
    'title_buttongroup_cols'   => 'Columns',
    'title_buttongroup_filter' => 'Filter',
    'msg_btn_delete'           => 'Are you sure you want to delete records <span class="mn-msg-delete">'
                                . '("Message")</span> ?',
    'msg_btn_clear'            => 'Are you sure you want to delete all records <span class="mn-msg-delete">'
                                . '"Messages")</span> ?',
    'text_btn_send'    => 'Send',
    'tooltip_btn_send' => 'Send a message to',
    // столбцы
    'header_msg_send_date'    => 'Sent',
    'header_msg_receive_date' => 'Received',
    'header_profile_name'     => 'Recipient',
    'header_msg_read'         => 'Read',
    'header_msg_text'         => 'Text'
);
?>