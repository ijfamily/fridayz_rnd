<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'     => 'Повідомлення',
    'title_grid_tpl' => 'повідомлення користувачів в системі',
    'rowmenu_info'   => 'Деталі',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Стовпці',
    'title_buttongroup_filter' => 'Фільтр',
    'msg_btn_delete'           => 'Ви дійсно бажаєте видалити записи <span class="mn-msg-delete">'
                                . '("Повідомлення")</span> ?',
    'msg_btn_clear'            => 'Ви дійсно бажаєте видалити всі записи <span class="mn-msg-delete">'
                                . '"Повідомлення")</span> ?',
    'text_btn_send'    => 'Відправити',
    'tooltip_btn_send' => 'Надіслати повідомлення користувачу',
    // столбцы
    'header_msg_send_date'    => 'Відправив',
    'header_msg_receive_date' => 'Отримав',
    'header_profile_name'     => 'Отримувач',
    'header_msg_read'         => 'Прочитано',
    'header_msg_text'         => 'Текст'
);
?>