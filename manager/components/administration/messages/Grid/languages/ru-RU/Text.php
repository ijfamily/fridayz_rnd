<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'     => 'Сообщения',
    'title_grid_tpl' => 'cообщения пользователей в системе',
    'rowmenu_info'   => 'Детали',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_delete'           => 'Вы действительно желаете удалить записи <span class="mn-msg-delete">'
                                . '("Сообщения")</span> ?',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">'
                                . '"Сообщения")</span> ?',
    'text_btn_send'    => 'Отправить',
    'tooltip_btn_send' => 'Отправить сообщение пользователю',
    // столбцы
    'header_msg_send_date'    => 'Отправил',
    'header_msg_receive_date' => 'Получил',
    'header_profile_name'     => 'Получатель',
    'header_msg_read'         => 'Прочитано',
    'header_msg_text'         => 'Текст'
);
?>