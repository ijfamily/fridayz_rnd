<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Message',
    // панель управления
    'text_btn_send'    => 'Send',
    'tooltip_btn_send' => 'Send a message to',
    'tooltip_btn_list' => 'List of user messages',
    // поля формы
    'label_user_name'  => 'User',
    // сообщения
    'title_msg_add'  => 'Message',
    'title_msg_text' => 'Message sent successfully!',
    'To execute a query, you must change data!' => 'To execute the query, you must fill out a text message!'
);
?>