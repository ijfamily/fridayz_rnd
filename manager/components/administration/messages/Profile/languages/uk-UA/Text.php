<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Повідомлення',
    // панель управления
    'text_btn_send'    => 'Відправити',
    'tooltip_btn_send' => 'Надіслати повідомлення користувачу',
    'tooltip_btn_list' => 'Список повідомлень користувачів',
    // поля формы
    'label_user_name'  => 'Користувач',
    // сообщения
    'title_msg_add'  => 'Повідомлення',
    'title_msg_text' => 'Повідомлення успішно відправлено!',
    'To execute a query, you must change data!' => 'Для виконання запиту, необхідно заповнити текст повідомлення!'
);
?>