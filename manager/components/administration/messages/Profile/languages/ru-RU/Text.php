<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_profile_insert' => 'Сообщение',
    // панель управления
    'text_btn_send'    => 'Отправить',
    'tooltip_btn_send' => 'Отправить сообщение пользователю',
    'tooltip_btn_list' => 'Список сообщений пользователей',
    // поля формы
    'label_user_name'  => 'Пользователь',
    // сообщения
    'title_msg_add'  => 'Сообщение',
    'title_msg_text' => 'Сообщение успешно отправлено!',
    'To execute a query, you must change data!' => 'Для выполнения запроса, необходимо заполнить текст сообщения!'
);
?>