<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля сообщения пользователя"
 * Пакет контроллеров "Сообщения пользователей"
 * Группа пакетов     "Сообщения пользователей"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Distributed under the Lesser General Public License (LGPL)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    GController_AMessages_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные профиля цвета товара
 * 
 * @category   Gear
 * @package    GController_AMessages_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AMessages_Profile_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array('receiver_id', 'msg_text');

    /**
     * Первичный ключ таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'msg_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_user_messages';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_amessages_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record выбранная запись
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['title_profile_update'], $record['msg_date']);
    }

    /**
     * Вставка данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataInsert($params = array())
    {
        parent::dataAccessInsert();

        $this->response->setMsgResult($this->_['title_msg_add'], $this->_['title_msg_text'], true);
        $table = new GDb_Table($this->tableName, $this->idProperty, $this->fields);
        if ($this->input->isEmpty('POST'))
            throw new GException('Warning', 'To execute a query, you must change data!');
        // массив полей с их соответствующими значениями
        if (empty($params))
            $params = $this->dataIntersect($this->input->post, $this->fields);
        // проверки входных данных
        $this->isDataCorrect($params);
        // проверка существования записи с одинаковыми значениями
        $this->isDataExist($params);
        if ($params['receiver_id'] == -1) {
            $sql = 'INSERT INTO `gear_user_messages` '
                 . '(`sender_id`,`receiver_id`,`msg_text`,`msg_read`,`msg_send_date`,`msg_send_time`,`msg_receive_date`,`msg_receive_time`) '
                 . 'SELECT ' . $this->session->get('user_id') . ',user_id,'
                 . $table->query->escapeStr($params['msg_text']) . ",0,'" . date('Y-m-d') . "','" . date('H:i:s') . "',null,null "
                 . 'FROM `gear_users` WHERE `user_id`<>' . $this->session->get('user_id');
            if ($table->query->execute($sql) === false)
                throw new GException('Data processing error', 'Query error (Internal Server Error)' . $table->query->getSql());
        } else {
            $params['sender_id'] = $this->session->get('user_id');
            $params['msg_read'] = 0;
            $params['msg_send_date'] = date('Y-m-d');
            $params['msg_send_time'] = date('H:i:s');
            // вставка данных
            if ($table->insert($params) === false)
                throw new GException('Data processing error', 'Query error (Internal Server Error)');
        }
        $this->_recordId = $table->getLastInsertId();
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        if (mb_strlen($params['msg_text'], 'UTF-8') == 0)
            throw new GException('Warning', $this->_['To execute a query, you must change data!']);
    }
}
?>