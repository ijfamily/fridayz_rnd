<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля сообщения пользователя"
 * Пакет контроллеров "Сообщения пользователей"
 * Группа пакетов     "Сообщения пользователей"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    GController_AMessages_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля сообщения пользователя
 * 
 * @category   Gear
 * @package    GController_AMessages_Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_AMessages_Profile_Interface extends GController_Profile_Interface
{
    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('id'            => strtolower(__CLASS__),
                  'title'         => $this->_['title_profile_insert'],
                  'titleEllipsis' => 50,
                  'gridId'        => 'gcontroller_amessages_grid',
                  'modal'         => false,
                  'width'         => 270,
                  'autoHeight'    => true,
                  'btnCancelHidden' => true,
                  'resizable'     => true,
                  'state'         => 5,
                  'stateful'      => true)
        );
        $this->_cmp->buttonsAdd = array(
            array('xtype'       => 'mn-btn-widget',
                  'cls'         => '',
                  'url'         => $this->componentUrl. 'grid/interface/',
                  'scale'       => 'small',
                  'tooltip'     => $this->_['tooltip_btn_list'],
                  'icon'        => $this->resourcePath . 'icon-btn-list.png',
                  'width'       => 30,
                  'closeWindow' => false),
            array('xtype'       => 'mn-btn-form-update',
                  'text'        => $this->_['text_btn_send'],
                  'tooltip'     => $this->_['tooltip_btn_send'],
                  'icon'        => $this->resourcePath . 'icon-btn-send.png',
                  'windowId'    => strtolower(__CLASS__),
                  'width'       => 97,
                  'closeWindow' => false)
        );

        // поля формы (ExtJS class "Ext.Panel")
        $items = array(
            array('xtype'      => 'mn-field-combo',
                  'hideLabel' => true,
                  'name'       => 'receiver_id',
                  'pageSize'   => 50,
                  'anchor'     => '100%',
                  'hiddenName' => 'receiver_id',
                  'allowBlank' => false,
                  'store'      => array(
                      'xtype' => 'jsonstore',
                      'url'   => $this->componentUrl . 'combo/trigger/?name=users'
                  )
            ),
            array('xtype'            => 'htmleditor',
                  'anchor'           => '100%',
                  'allowBlank'       => false,
                  'name'             => 'msg_text',
                  'hideLabel'        => true,
                  'enableLists'      => false,
                  'enableSourceEdit' => false,
                  'enableFont'       => false,
                  'enableAlignments' => false,
                  'enableLinks'      => false,
                  'height'           => 200)
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->addItems($items);
        $form->labelWidth = 70;
        $form->url = $this->componentUrl . 'profile/';

        parent::getInterface();
    }
}
?>