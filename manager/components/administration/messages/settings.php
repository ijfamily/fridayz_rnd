<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Сообщения пользователей"
 * Группа пакетов     "Сообщения пользователей"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_AMessages
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Отправить сообщение
 * Описание: Отправлений сообщений пользователям системы
 * ID класса: gcontroller_amessages_profile
 * Группа: Администрирование / Сообщения
 * Очищать: нет
 * Ресурс
 *    Пакет: administration/messages/
 *    Контроллер: administration/messages/Profile/
 *    Интерфейс: profile/interface/
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске: нет
 * 
 * Название: Сообщения пользователей
 * Описание: Сообщения пользователей системы
 * ID класса: gcontroller_amessages_grid
 * Группа: Администрирование / Сообщения
 * Очищать: нет
 * Ресурс
 *    Пакет: administration/messages/
 *    Контроллер: administration/messages/Grid/
 *    Интерфейс: grid/interface/
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске: нет
 */

return array(
    // {A}- модуль "Administration" {} - пакет контроллеров "Messages" -> AMessages
    'clsPrefix' => 'AMessages'
);
?>