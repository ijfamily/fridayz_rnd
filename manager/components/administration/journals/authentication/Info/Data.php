<?php
/**
 * Gear Manager
 *
 * Контроллеров       "Данные профиля аутентификации пользователей"
 * Пакет контроллеров "Аутентификация пользователей"
 * Группа пакетов     "Журналы"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalAuth_Info
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные профиля аутентификации пользователей
 * 
 * @category   Gear
 * @package    GController_YJournalAuth_Info
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalAuth_Info_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    protected $fields = array('auth_date', 'auth_time', 'auth_ipaddress', 'auth_browser');

    /**
     * Первичное поле таблицы ($tableName)
     *
     * @var string
     */
    protected $idProperty = 'auth_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    protected $tableName = 'gear_user_authentication';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_yjournalauth_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record array fields
     * @return string
     */
    protected function getTitle($record)
    {
        return sprintf($this->_['info_title'],
               date('d-m-Y', strtotime($record['auth_date'])) . ' ' . $record['auth_time']);
    }

    /**
     * Добавление в журнал действий пользователей
     * (удаление данных)
     * 
     * @param array $params параметры журнала
     * @return void
     */
    protected function logDelete($params = array())
    {}

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        $record['auth_date'] = date('d-m-Y', strtotime($record['auth_date'])) . ' ' . $record['auth_time'];

        return $record;
    }
}
?>