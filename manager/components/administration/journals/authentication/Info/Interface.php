<?php
/**
 * Gear Manager
 *
 * Контроллеров       "Интерфейс профиля аутентификации пользователей"
 * Пакет контроллеров "Аутентификация пользователей"
 * Группа пакетов     "Журналы"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalAuth_Info
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля аутентификации пользователей
 * 
 * @category   Gear
 * @package    GController_YJournalAuth_Info
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalAuth_Info_Interface extends GController_Profile_Interface
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_yjournalauth_grid';


    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('titleEllipsis' => 45,
                  'gridId'        => 'gcontroller_yjournalauth_grid',
                  'width'         => 275,
                  'autoHeight'    => true,
                  'resizable'     => false,
                  'state'         => 'info',
                  'stateful'      => false)
        );

        // поля формы (Ext.form.FormPanel)
        $items = array(
            array('xtype'      => 'displayfield',
                  'fieldLabel' => $this->_['label_authentication_date'],
                  'fieldClass' => 'mn-field-value-info',
                  'name'       => 'auth_date'),
            array('xtype'      => 'displayfield',
                  'fieldLabel' => $this->_['label_authentication_ipaddress'],
                  'fieldClass' => 'mn-field-value-info',
                  'name'       => 'auth_ipaddress'),
            array('xtype'      => 'displayfield',
                  'fieldLabel' => $this->_['label_authentication_browser'],
                  'fieldClass' => 'mn-field-value-info',
                  'name'       => 'auth_browser')
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->addItems($items);
        $form->url = $this->componentUrl . 'info/';

        parent::getInterface();
    }
}
?>