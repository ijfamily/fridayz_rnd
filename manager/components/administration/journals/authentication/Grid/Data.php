<?php
/**
 * Gear Manager
 *
 * Контроллеров       "Данные аутентификации пользователей"
 * Пакет контроллеров "Аутентификация пользователей"
 * Группа пакетов     "Журналы"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalAuth_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные аутентификации пользователей
 * 
 * @category   Gear
 * @package    GController_YJournalAuth_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalAuth_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'auth_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_user_authentication';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS * FROM `gear_user_authentication` '
          . 'WHERE 1 %filter '
          . 'ORDER BY %sort '
          . 'LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            'auth_date' => array('type' => 'string'),
            'auth_time' => array('type' => 'string'),
            'auth_ipaddress' => array('type' => 'string'),
            'auth_browser' =>  array('type' => 'string')
        );
    }

    /**
     * Добавление в журнал действий пользователей
     * (удаление данных)
     * 
     * @param array $params параметры журнала
     * @return void
     */
    protected function logDelete($params = array())
    {}

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        // удаление записей таблицы "gear_user_authentication"
        $table = new GDb_Table('gear_user_authentication', 'auth_id');
        if ($table->clear(true) !== true)
            throw new GException('Data processing error', 'Query error (Internal Server Error)');
    }
}
?>