<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Форма авторизации пользователей',
    'tooltip_grid' => 'попытки пользователей пройти авторизацию',
    'rowmenu_info' => 'Информация о записи',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">("Форма авторизации пользователей")</span> ?',
    // столбцы
    'header_authentication_date'      => 'Дата',
    'header_authentication_ipaddress' => 'IP адрес',
    'header_authentication_browser'   => 'Браузер',
    'title_buttongroup_edit'          => 'Правка',
    'title_buttongroup_cols'          => 'Столбцы',
    'title_buttongroup_filter'        => 'Фильтр'
);
?>