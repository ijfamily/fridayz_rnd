<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'User authentication',
    'tooltip_grid' => 'попытки пользователей пройти авторизацию',
    'rowmenu_info' => 'Record details',
    // панель управления
    'title_buttongroup_edit'   => 'Edit',
    'title_buttongroup_cols'   => 'Columns',
    'title_buttongroup_filter' => 'Filter',
    'msg_btn_clear'            => 'Are you sure you want to delete all entries <span class="mn-msg-delete">"User authentication"</span> ?',
    // столбцы
    'header_authentication_date'      => 'Date',
    'header_authentication_ipaddress' => 'IP address',
    'header_authentication_browser'   => 'Browser',
    'title_buttongroup_edit'          => 'Edit',
    'title_buttongroup_cols'          => 'Columns',
    'title_buttongroup_filter'        => 'Filter'
);
?>