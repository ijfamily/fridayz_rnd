<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск в списке восстановленных учётных записей"
 * Пакет контроллеров "Восстановление учётный записей"
 * Группа пакетов     "Журналы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalRestore_Search
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск в списке восстановленных учётных записей
 * 
 * @category   Gear
 * @package    GController_YJournalRestore_Search
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalRestore_Search_Data extends GController_Search_Data
{
    /**
     * дентификатор компонента (списка) к которому применяется поиск
     *
     * @var string
     */
    public $gridId = 'gcontroller_yjournalrestore_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_yjournalrestore_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор,
     * используется для инициализации атрибутов контроллера без конструктора
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'restore_date', 'label' => $this->_['header_rst_date']),
            array('dataIndex' => 'restore_date_actived', 'label' => $this->_['header_rst_date_actived']),
            array('dataIndex' => 'user_name', 'label' => $this->_['header_user_name']),
            array('dataIndex' => 'profile_name', 'label' => $this->_['header_profile_name']),
            array('dataIndex' => 'restore_hash', 'label' => $this->_['header_rst_hash']),
            array('dataIndex' => 'restore_ipaddress', 'label' => $this->_['header_rst_ipaddress']),
            array('dataIndex' => 'restore_email', 'label' => $this->_['header_rst_email']),
            array('dataIndex' => 'restore_actived', 'label' => $this->_['header_rst_actived'])
        );
    }
}
?>