<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'search_title' => 'Пошук у списку "Протокол відновлення облікових записів"',
    // поля
    'header_rst_date'         => 'Дата (р)',
    'header_rst_date_actived' => 'Дата (в)',
    'header_rst_ipaddress'    => 'IP адрес',
    'header_rst_hash'         => 'Хеш',
    'header_rst_email'        => 'E-mail',
    'header_profile_name'     => 'Користувач',
    'header_user_name'        => 'Логін',
    'header_rst_actived'      => 'Відновлена'
);
?>