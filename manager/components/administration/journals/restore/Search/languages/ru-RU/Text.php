<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Поиск в списке "Журнал восстановления учетных записей"',
    // поля
    'header_rst_date'         => 'Дата (р)',
    'header_rst_date_actived' => 'Дата (в)',
    'header_rst_ipaddress'    => 'IP адрес',
    'header_rst_hash'         => 'Хэш',
    'header_rst_email'        => 'E-mail',
    'header_profile_name'     => 'Пользователь',
    'header_user_name'        => 'Логин',
    'header_rst_actived'      => 'Восстановлена'
);
?>