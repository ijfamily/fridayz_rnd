<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка восстановленияя учётных записей"
 * Пакет контроллеров "Восстановление учётный записей"
 * Группа пакетов     "Журналы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalRestore_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные списка восстановленияя учётных записей
 * 
 * @category   Gear
 * @package    GController_YJournalRestore_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalRestore_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'restore_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_user_restore';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS * '
          . 'FROM (SELECT `r`.*, `p`.`profile_name`, `u`.`user_name` '
          . 'FROM `gear_user_restore` `r`, `gear_user_profiles` `p`, `gear_users` `u` '
          . 'WHERE `p`.`user_id`=`u`.`user_id` AND '
          . '`u`.`user_id`=`r`.`user_id`) `table` '
          . 'WHERE 1 %filter '
          . 'ORDER BY %sort '
          . 'LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            'restore_email' => array('type' => 'string'),
            'restore_hash' => array('type' => 'string'),
            'restore_actived' => array('type' => 'string'),
            'restore_date' => array('type' => 'string'),
            'restore_date_actived' => array('type' => 'boolean'),
            'restore_ipaddress' => array('type' => 'string'),
            'user_name' => array('type' => 'string'),
            'user_id' => array('type' => 'integer'),
            'profile_name' => array('type' => 'string'),
            'photo' => array('type' => 'string')
        );
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        // удаление записей таблицы "gear_user_restore"
        $table = new GDb_Table('gear_user_restore', 'restore_id');
        if ($table->clear(true) !== true)
            throw new GException('Data processing error', 'Query error (Internal Server Error)');
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        // если пользователь существует
        $user = GUsers::get($record['user_id']);
        if ($user) {
            $record['photo'] = $user['photo'];
            $record['profile_name'] = $user['profile_name'];
        }

        return $record;
    }
}
?>