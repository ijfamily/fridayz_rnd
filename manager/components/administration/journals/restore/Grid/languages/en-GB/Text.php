<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Protocol Recovery Account',
    'tooltip_grid' => 'список восстановленных учётных записей пользователей',
    // панель управления
    'title_buttongroup_edit'    => 'Edit',
    'title_buttongroup_cols'    => 'Cols',
    'title_buttongroup_filter'  => 'Filter',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">("Восстановления учетных записей")</span> ?',
    // столбцы
    'header_rst_date'          => 'Date (r)',
    'tooltip_rst_date'         => 'Registered restoration',
    'header_rst_date_actived'  => 'Date (rs)',
    'tooltip_rst_date_actived' => 'Date of restoration',
    'header_rst_ipaddress'     => 'IP address',
    'header_rst_hash'          => 'Hash',
    'header_rst_email'         => 'E-mail',
    'header_profile_name'      => 'User',
    'header_user_name'         => 'Login',
    'header_rst_actived'       => 'Restored'
);
?>