<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Восстановление учетных записей',
    'tooltip_grid' => 'список восстановленных учётных записей пользователей',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">("Восстановления учетных записей")</span> ?',
    // столбцы
    'header_rst_date'          => 'Дата (р)',
    'tooltip_rst_date'         => 'Дата регистрации востановления',
    'header_rst_date_actived'  => 'Дата (в)',
    'tooltip_rst_date_actived' => 'Дата востановления',
    'header_rst_ipaddress'     => 'IP адрес',
    'header_rst_hash'          => 'Хэш',
    'header_rst_email'         => 'E-mail',
    'header_profile_name'      => 'Пользователь',
    'header_user_name'         => 'Логин',
    'header_rst_actived'       => 'Восстановлена'
);
?>