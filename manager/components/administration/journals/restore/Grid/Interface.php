<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка восстановленияя учётных записей"
 * Пакет контроллеров "Восстановление учётный записей"
 * Группа пакетов     "Журналы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalRestore_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс списка восстановленияя учётных записей
 * 
 * @category   Gear
 * @package    GController_YJournalRestore_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalRestore_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'restore_id';

    /**
     * Вид сортировки
     * (указывается в настройках Ext.data.JsonStore.sortInfo.dir)
     *
     * @var string
     */
    public $dir = 'DESC';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'restore_date';

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('name' => 'restore_email', 'type' => 'string'),
            array('name' => 'restore_hash', 'type' => 'string'),
            array('name' => 'restore_actived', 'type' => 'string'),
            array('name' => 'restore_date', 'type' => 'string'),
            array('name' => 'restore_date_actived', 'type' => 'boolean'),
            array('name' => 'restore_ipaddress', 'type' => 'string'),
            array('name' => 'user_name', 'type' => 'string'),
            array('name' => 'profile_name', 'type' => 'string'),
            array('name' => 'photo', 'type' => 'string')
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $this->columns = array(
            array('xtype'     => 'numbercolumn'),
            array('dataIndex' => 'restore_date',
                  'tooltip'   => $this->_['tooltip_rst_date'],
                  'header'    => '<em class="mn-grid-hd-details"></em>'
                               . $this->_['header_rst_date'],
                  'width'     => 115,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'restore_date_actived',
                  'header'    => $this->_['header_rst_date_actived'],
                  'tooltip'   => $this->_['tooltip_rst_date_actived'],
                  'width'     => 115,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'user_name',
                  'header'    => $this->_['header_user_name'],
                  'width'     => 110,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'profile_name',
                  'header'    => $this->_['header_profile_name'],
                  'width'     => 170,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'restore_hash',
                  'header'    => $this->_['header_rst_hash'],
                  'width'     => 150,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'restore_ipaddress',
                  'header'    => $this->_['header_rst_ipaddress'],
                  'width'     => 90,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'restore_email',
                  'header'    => $this->_['header_rst_email'],
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('xtype'     => 'booleancolumn',
                  'dataIndex' => 'restore_actived',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-restore.png" align="absmiddle">',
                  'tooltip'   => $this->_['header_rst_actived'],
                  'align'     => 'center',
                  'width'     => 45,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string'))
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_grid'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_grid'],
                  'titleEllipsis' => 40,
                  'isReadOnly'    => true)
        );

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "правка" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' =>$this->classId));
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'      => 'mn-btn-clear-data',
                  'msgConfirm' => $this->_['msg_btn_clear'],
                  'gridId'     => $this->classId,
                  'isN'        => true)
        );
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = 'component-restore-account';
        $this->_cmp->tbar->items->add($group);

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $img = '<div class="icon-photo-s" style="background-image: url(\'{photo}\')"></div>';
        $cellInfo =
            '<div class="mn-grid-cell-tooltip-tl">{restore_date}</div>'
          . '<table class="mn-grid-cell-tooltip" cellpadding="0" cellspacing="5" border="0">'
          . '<tr><td valign="top">' . $img . '</td><td valign="top" style="width:400px">'
          . '<em>' . $this->_['header_profile_name'] . '</em>: <b>{profile_name}</b><br>'
          . '<em>' . $this->_['header_user_name'] . '</em>: <b>{user_name}</b><br>'
          . '<em>' . $this->_['header_rst_date_actived'] . '</em>: <b>{restore_date_actived}</b><br>'
          . '<em>' . $this->_['header_rst_email'] . '</em>: <b>{restore_email}</b>'
          . '</td></tr></table>';
        $this->_cmp->cellTips = array(
            array('field' => 'restore_date', 'tpl' => $cellInfo),
            array('field' => 'profile_name', 'tpl' => '{profile_name}'),
            array('field' => 'restore_email', 'tpl' => '{restore_email}'),
            array('field' => 'restore_ipaddress', 'tpl' => '{restore_ipaddress}'),
            array('field' => 'restore_hash', 'tpl' => '{restore_hash}')
        );

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $this->_cmp->rowMenu = array('xtype' => 'mn-grid-rowmenu');

        parent::getInterface();
    }
}
?>