<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка авторизаций пользователей"
 * Пакет контроллеров "Авторизация пользователей"
 * Группа пакетов     "Журналы"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalAttends_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::ns('Controller/Grid/Data');
Gear::ns('User/Log');

/**
 * Данные списка авторизаций пользователей
 * 
 * @category   Gear
 * @package    GController_YJournalAttends_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalAttends_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
     public $idProperty = 'attend_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_user_attends';

    /**
     * Каталог изображений пользователей
     *
     * @var string
     */
    protected $_pathData = '../../../../data/photos/';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // путь к фотками
        $photo = $this->config->get('PHOTO');
        $this->_photoPath = $photo['PATH'];
        // фильтр для toolbar
        $tlbFilter = $this->store->get('tlbFilter');

        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS * '
          . 'FROM (SELECT `a`.*, `u`.* '
          . 'FROM `gear_user_attends` `a` '
          . 'LEFT JOIN (SELECT `u`.`user_name`, `up`.`profile_name`, `up`.`profile_id`, `up`.`profile_photo`, `up`.`user_id` `_user_id` '
          . 'FROM `gear_users` `u`, `gear_user_profiles` `up` '
          . 'WHERE `up`.`user_id`=`u`.`user_id`) `u` '
          . 'ON `u`.`_user_id`=`a`.`user_id`) `table` '
          . 'WHERE 1 %filter %fastfilter ' . $tlbFilter
          . 'ORDER BY %sort '
          . 'LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            'attend_date' => array('type' => 'string'),
            'attend_time' => array('type' => 'string'),
            'attend_browser' => array('type' => 'string'),
            'attend_os' => array('type' => 'string'),
            'attend_ipaddress' => array('type' => 'string'),
            'attend_name' => array('type' => 'string'),
            'attend_error' => array('type' => 'string'),
            'user_id' => array('type' => 'integer'),
            'profile_id' => array('type' => 'integer'),
            'profile_name' => array('type' => 'string'),
            'photo' => array('type' => 'string'),
            'attend_success' => array('type' => 'boolean')
        );
    }

    /**
     * Возращает sql запрос для быстрого фильтра
     * (для подстановки "%fastfilter" в $sql)
     * 
     * @param string $key идендификатор поля
     * @param string $value значение
     * @return string
     */
    protected function getTreeFilter($key, $value)
    {
        // идендификатор поля
        switch ($key) {
            // по дате
            case 'byDt':
                $toDate = date('Y-m-d');
                switch ($value) {
                    case 'day':
                        return ' AND `attend_date` BETWEEN "' . $toDate . '" AND "' . $toDate . '"';

                    case 'week':
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d") - 7, date("Y")));
                        return ' AND `attend_date` BETWEEN "' . $fromDate . '" AND "' . $toDate . '"';

                    case 'month':
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, date("m")-1, date("d"), date("Y")));
                        return ' AND `attend_date` BETWEEN "' . $fromDate . '" AND "' . $toDate . '"';
                }
                break;

            // по версии браузера
            case 'byBr':
                return ' AND `attend_browser`="' . $value . '" ';

            // по ос
            case 'byOs':
                return ' AND `attend_os`="' . $value . '" ';

            // по доступу
            case 'bySc':
                return ' AND `attend_success`="' . (int) $value . '" ';
        }

        return '';
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        GUser_Log_Attend::clear();
    }

    /**
     * Добавление в журнал действий пользователей
     * (удаление данных)
     * 
     * @param array $params параметры журнала
     * @return void
     */
    protected function logDelete($params = array())
    {}

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        // если пользователь существует
        $user = GUsers::get($record['user_id']);
        if ($user) {
            $record['photo'] = $user['photo'];
            $record['profile_name'] = $user['profile_name'];
        }

        return $record;
    }
}
?>