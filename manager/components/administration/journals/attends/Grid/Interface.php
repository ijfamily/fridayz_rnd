<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка авторизаций пользователей"
 * Пакет контроллеров "Авторизация пользователей"
 * Группа пакетов     "Журналы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalAttends_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс списка авторизаций пользователей
 * 
 * @category   Gear
 * @package    GController_YJournalAttends_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalAttends_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'attend_id';

    /**
     * Вид сортировки
     * (указывается в настройках Ext.data.JsonStore.sortInfo.dir)
     *
     * @var string
     */
    public $dir = 'DESC';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'attend_date';

    /**
     * Использовать быстрый фильтр
     *
     * @var boolean
     */
    public $useSlidePanel = true;

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('name' => 'attend_date', 'type' => 'string'),
            array('name' => 'attend_time', 'type' => 'string'),
            array('name' => 'attend_browser', 'type' => 'string'),
            array('name' => 'attend_os', 'type' => 'string'),
            array('name' => 'attend_ipaddress', 'type' => 'string'),
            array('name' => 'attend_name', 'type' => 'string'),
            array('name' => 'attend_error', 'type' => 'string'),
            array('name' => 'profile_id', 'type' => 'integer'),
            array('name' => 'profile_name', 'type' => 'string'),
            array('name' => 'photo', 'type' => 'string'),
            array('name' => 'attend_success', 'type' => 'boolean')
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $settings = $this->session->get('user/settings');
        $this->columns = array(
            array('xtype'     => 'expandercolumn',
                  'url'       => $this->componentUrl . 'grid/expand/',
                  'typeCt'    => 'row'),
            array('xtype'     => 'numbercolumn'),
            array('xtype'     => 'rowmenu'),
            array('xtype'     => 'datetimecolumn',
                  'dataIndex' => 'attend_date',
                  'timeIndex' => 'attend_time',
                  'frmDate'   => $settings['format/date'],
                  'frmTime'   => $settings['format/time'],
                  'header'    => '<em class="mn-grid-hd-details"></em>' . $this->_['header_attend_date'],
                  'width'     => 115,
                  'sortable'  => true),
            array('dataIndex' => 'attend_browser',
                  'header'    => $this->_['header_attend_browser'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'attend_os',
                  'header'    => $this->_['header_attend_os'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'attend_ipaddress',
                  'header'    => $this->_['header_attend_ipaddress'],
                  'width'     => 100,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'attend_name',
                  'header'    => $this->_['header_attend_name'],
                  'width'     => 105,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'profile_name',
                  'header'    => $this->_['header_profile_name'],
                  'width'     => 170,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'attend_error',
                  'header'    => $this->_['header_attend_error'],
                  'hidden'    => true,
                  'width'     => 170,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('xtype'     => 'booleancolumn',
                  'dataIndex' => 'attend_success',
                  'header'    => '<img src="' . $this->resourcePath . 'icon-hd-shield.png" align="absmiddle">',
                  'tooltip'   => $this->_['header_attend_success'],
                  'align'     => 'center',
                  'width'     => 45,
                  'sortable'  => true,
                  'filter'    => array('type' => 'boolean'))
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_grid'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_grid'],
                  //'tabTip' => 'ddd',
                  //'titleTpl' => '111',
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false)
        );

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // группа кнопок "правка" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' =>$this->classId));
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'      => 'mn-btn-clear-data',
                  'msgConfirm' => $this->_['msg_btn_clear'],
                  'gridId'     => $this->classId,
                  'isN'        => true)
        );
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = 'component-journal-attends';
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "фильтр" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Filter(
            array('title'  => $this->_['title_buttongroup_filter'],
                  'gridId' => $this->classId)
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(0);
        $btn['gridId'] = $this->classId;
        $btn['url'] = $this->componentUrl . 'search/data/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->add(array(
            'xtype'       => 'form',
            'labelWidth'  => 47,
            'bodyStyle'   => 'border:1px solid transparent;background-color:transparent',
            'frame'       => false,
            'bodyBorder ' => false,
            'items'       => array(
                array('xtype' => 'datefield', 'format' => 'd-m-Y', 'name' => 'filter_df',
                      'fieldLabel' => $this->_['label_buttongroup_df']),
                array('xtype' => 'datefield', 'format' => 'd-m-Y', 'name' => 'filter_dt',
                      'fieldLabel' => $this->_['label_buttongroup_dt'])
            )
        ));
        $this->_cmp->tbar->items->add($group);
        $this->store->set('tlbFilter', '');

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $img = '<div class="icon-photo-s" style="background-image: url(\'{photo}\')"></div>';
        $cellInfo =
            '<div class="mn-grid-cell-tooltip-tl">{attend_date}</div>'
          . '<table class="mn-grid-cell-tooltip" cellpadding="0" cellspacing="5" border="0">'
          . '<tr><td valign="top">' . $img . '</td><td valign="top" style="width:350px">'
          . '<em>' . $this->_['header_profile_name'] . '</em>: <b>{profile_name}</b><br>'
          . '<em>' . $this->_['header_attend_name'] . '</em>: <b>{attend_name}</b><br>'
          . '<em>' . $this->_['header_attend_browser'] . '</em>: <b>{attend_browser}</b><br>'
          . '<em>' . $this->_['header_attend_os'] . '</em>: <b>{attend_os}</b><br>'
          . '<em>' . $this->_['header_attend_ipaddress'] . '</em>: <b>{attend_ipaddress}</b>'
          . '</td></tr></table>';
        $this->_cmp->cellTips = array(
           array('field' => 'attend_date', 'tpl' => $cellInfo),
           array('field' => 'attend_browser', 'tpl' => '{attend_browser}'),
           array('field' => 'attend_os', 'tpl' => '{attend_os}'),
           array('field' => 'attend_name', 'tpl' => '{attend_name}'),
           array('field' => 'profile_name', 'tpl' => '{profile_name}'),
           array('field' => 'attend_error', 'tpl' => '{attend_error}')
        );

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $this->_cmp->rowMenu = array(
            'xtype' => 'mn-grid-rowmenu',
            'items' => array(
                array('text'    => $this->_['rowmenu_info'],
                      'icon'    => $this->resourcePath . 'icon-item-info.png',
                      'url'     => $this->componentUrl . 'info/interface/'),
                array('text'   => $this->_['rowmenu_user'],
                      'field'  => 'profile_id',
                      'icon'   => $this->resourcePath . 'icon-item-info.png',
                      'params' => 'state=info',
                      'url'    => 'administration/users/contingent/' . ROUTER_DELIMITER . 'profile/interface/')
            )
        );

        // быстрый фильтр списка (ExtJs class "Manager.tree.GridFilter")
        $this->_slidePanel->cls = 'mn-tree-gridfilter';
        $this->_slidePanel->initRoot = array(
            'text'     => 'Filter',
            'id'       => 'byRoot',
            'expanded' => true,
            'children' => array(
                array('text'     => $this->_['text_all_records'],
                      'value'    => 'all',
                      'expanded' => true,
                      'leaf'     => false,
                      'children' => array(
                        array('text'     => $this->_['text_by_date'],
                              'id'       => 'byDt',
                              'leaf'     => false,
                              'expanded' => true,
                              'children' => array(
                                  array('text'     => $this->_['text_by_day'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 'day'),
                                  array('text'     => $this->_['text_by_week'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true, 
                                        'value'    => 'week'),
                                  array('text'     => $this->_['text_by_month'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 'month')
                              )
                        ),
                        array('text'    => $this->_['text_by_os'],
                              'id'      => 'byOs',
                              'leaf'    => false),
                        array('text'    => $this->_['text_by_browser'],
                              'id'      => 'byBr',
                              'leaf'    => false),
                        array('text'     => $this->_['text_by_access'],
                              'id'       => 'bySc',
                              'leaf'     => false,
                              'children' => array(
                                  array('text'     => $this->_['text_by_success'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 1),
                                  array('text'     => $this->_['text_by_error'],
                                        'iconCls'  => 'icon-folder-find',
                                        'leaf'     => true,
                                        'value'    => 0)
                              )
                        )
                    )
                )
            )
        );

        parent::getInterface();
    }
}
?>