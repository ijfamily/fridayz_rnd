<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Авторизація користувачів',
    'tooltip_grid' => 'список пользователей прошедших авторизацию',
    'rowmenu_info' => 'Информация о записи',
    'rowmenu_user' => 'Информация о пользователе',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Стовпці',
    'title_buttongroup_filter' => 'Фільтр',
    'msg_btn_clear'            => 'Ви дійсно бажаєте видалити всі записи <span class="mn-msg-delete">"Авторизація користувачів"</span> ?',
    'label_buttongroup_df'     => 'дата від',
    'label_buttongroup_dt'     => 'дата до',
    // столбцы
    'header_attend_date'       => 'Дата',
    'header_attend_browser'    => 'Браузер',
    'header_attend_os'         => 'ОС',
    'header_attend_ipaddress'  => 'IP адреса',
    'header_attend_name'       => 'Логін',
    'header_profile_name'      => 'Iм\'я',
    'header_attend_error'      => 'Помилки',
    'header_attend_success'    => 'Успіх',
    // быстрый фильтр
    'text_all_records' => 'Всі записи',
    'text_by_date'     => 'За датою',
    'text_by_day'      => 'За день',
    'text_by_week'     => 'За тиждень',
    'text_by_month'    => 'За місяць',
    'text_by_os'       => 'За ОС',
    'text_by_browser'  => 'За версією браузера',
    'text_by_access'   => 'З доступу',
    'text_by_success'  => 'Успішна авторизація',
    'text_by_error'    => 'Помилки доступу',
    // типы
    'data_gender' => array('женский', 'мужской'),
    'data_boolean' => array('нет', 'да'),
    // развернутая запись
    'title_fieldset_common'      => 'Основные параметры',
    'label_attend_date'          => 'Дата авторизации',
    'label_attend_browser'       => 'Браузер',
    'label_attend_os'            => 'ОС',
    'label_attend_ipaddress'     => 'IP адрес',
    'label_attend_name'          => 'Логин',
    'label_attend_error'         => 'Ошибки',
    'label_attend_success'       => 'Успех',
    'title_fieldset_user'        => 'Пользователь',
    'label_profile_name'         => 'Фамилия Имя Отчество',
    'label_profile_gender'       => 'Пол',
    'label_profile_born'         => 'Дата рождения',
    'title_fieldset_connection'  => 'Средства связи',
    'label_contact_work_phone'   => 'Рабочий телефон',
    'label_contact_mobile_phone' => 'Мобильный телефон',
    'label_contact_home_phone'   => 'Домашний телефон',
    'title_fieldset_net'         => 'Социальные сети',
    'title_fieldset_address'     => 'Адрес',
    'label_address'              => 'Адрес',
    'label_address_index'        => 'Индекс',
    'label_address_country'      => 'Страна',
    'label_address_region'       => 'Область / штат',
    'label_address_city'         => 'Город',
    'msg_empty_profile'          => 'нет данных для отображения'
);
?>