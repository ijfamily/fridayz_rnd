<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'User authorization',
    'tooltip_grid' => 'список пользователей прошедших авторизацию',
    'rowmenu_info' => 'Details',
    'rowmenu_user' => 'User information',
    // панель инструментов
    'label_buttongroup_df'     => 'date from',
    'label_buttongroup_dt'     => 'date to',
    'title_buttongroup_edit'   => 'Edit',
    'title_buttongroup_cols'   => 'Columns',
    'title_buttongroup_filter' => 'Filter',
    'msg_btn_clear'            => 'Are you sure you want to delete all records <span class="mn-msg-delete">"User authorization</span> ?',
    'title_buttongroup_filter' => 'Filter',
    // поля
    'header_attend_date'       => 'Date',
    'header_attend_browser'    => 'Browser',
    'header_attend_os'         => 'OS',
    'header_attend_ipaddress'  => 'IP address',
    'header_attend_name'       => 'Login',
    'header_profile_name'      => 'Name',
    'header_attend_error'      => 'Error',
    'header_attend_success'    => 'Success',
    // быстрый фильтр
    'text_all_records' => 'All records',
    'text_by_date'     => 'By date',
    'text_by_day'      => 'For day',
    'text_by_week'     => 'For week',
    'text_by_month'    => 'Form month',
    'text_by_os'       => 'By OS',
    'text_by_browser'  => 'By browser version',
    'text_by_access'   => 'By access',
    'text_by_success'  => 'Successful authentication',
    'text_by_error'    => 'Access errors',
    // типы
    'data_gender' => array('женский', 'мужской'),
    'data_boolean' => array('нет', 'да'),
    // развернутая запись
    'title_fieldset_common'      => 'Основные параметры',
    'label_attend_date'          => 'Дата авторизации',
    'label_attend_browser'       => 'Браузер',
    'label_attend_os'            => 'ОС',
    'label_attend_ipaddress'     => 'IP адрес',
    'label_attend_name'          => 'Логин',
    'label_attend_error'         => 'Ошибки',
    'label_attend_success'       => 'Успех',
    'title_fieldset_user'        => 'Пользователь',
    'label_profile_name'         => 'Фамилия Имя Отчество',
    'label_profile_gender'       => 'Пол',
    'label_profile_born'         => 'Дата рождения',
    'title_fieldset_connection'  => 'Средства связи',
    'label_contact_work_phone'   => 'Рабочий телефон',
    'label_contact_mobile_phone' => 'Мобильный телефон',
    'label_contact_home_phone'   => 'Домашний телефон',
    'title_fieldset_net'         => 'Социальные сети',
    'title_fieldset_address'     => 'Адрес',
    'label_address'              => 'Адрес',
    'label_address_index'        => 'Индекс',
    'label_address_country'      => 'Страна',
    'label_address_region'       => 'Область / штат',
    'label_address_city'         => 'Город',
    'msg_empty_profile'          => 'нет данных для отображения'
);
?>