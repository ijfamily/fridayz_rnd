<?php
/**
 * Gear Manager
 *
 * Controller          "Export grid of user attends"
 * Package controllers "User attends"
 * Group of packages   "Journals"
 * Module              "System"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalAttends_Grid
 * @subpackage Export
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * @see GController_Grid_Export
 */
require_once ('Gear/Controllers/Grid/Export.php');

/**
 * Export grid of user attends
 * 
 * @category   Gear
 * @package    GController_YJournalAttends_Grid
 * @subpackage Export
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalAttends_Grid_Export extends GController_Grid_Export
{
    /**
     * Fields TDatabase_Table_Custom
     * (config options - Ext.data.JsonStore.fields) 
     *
     * @var array
     */
    protected $_fields = array('attend_date', 'attend_browser', 'attend_os', 'attend_ipaddress', 'attend_name',
                               'attend_error', 'profile_name', 'profile_photo', 'attend_success');

    /**
     * Grid identifier in the session
     *
     * @var string
     */
    protected $_gridId = 'gcontroller_yjournalattends_grid';

    /**
     * Table name
     *
     * @var string
     */
    protected $_tableName = 'gear_user_attends';

    /**
     * Constructor
     * 
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        // export type
        $this->_sql = 'SELECT SQL_CALC_FOUND_ROWS * '
                    . 'FROM (SELECT `a`.*, `u`.* '
                    . 'FROM `gear_user_attends` AS `a` '
                    . 'LEFT JOIN (SELECT `u`.`user_name`, `up`.`profile_name`, `up`.`profile_photo`, `up`.`user_id` AS `_user_id` '
                    . 'FROM `gear_users` AS `u`, `gear_user_profiles` AS `up` '
                    . 'WHERE `up`.`user_id`=`u`.`user_id`) AS `u` '
                    . 'ON `u`.`_user_id`=`a`.`user_id`) AS `table` '
                    . 'WHERE 1 %filter '
                    . 'ORDER BY %sort '
                    . 'LIMIT %limit';
    }
}
?>