<?php
/**
 * Gear Manager
 *
 * Контроллер         "Развёрнутая запись авторизации пользователя"
 * Пакет контроллеров "Авторизация пользователей"
 * Группа пакетов     "Журналы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalAttends_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Expand');

/**
 * Развёрнутая запись авторизации пользователя
 * 
 * @category   Gear
 * @package    GController_YJournalAttends_Grid
 * @subpackage Expand
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalAttends_Grid_Expand extends GController_Grid_Expand
{
    /**
     * Вывод данных в интерфейс
     * 
     * @return void
     */
    protected function dataExpand()
    {
        parent::dataAccessView();

        $data = $this->getAttributes();
        $this->response->data = $data;
    }

    /**
     * Возращает атрибуты записи
     * 
     * @return string
     */
    protected function getAttributes()
    {
        $data = '';
        $settings = $this->session->get('user/settings');
        // контингент
        $query = new GDb_Query();
        $sql =
            'SELECT `u`.*, `p`.*, `a`.* '
          . 'FROM `gear_user_attends` `a` '
          . 'LEFT JOIN `gear_users` `u` USING(`user_id`) '
          . 'LEFT JOIN `gear_user_profiles` `p` USING(`user_id`) '
          . 'WHERE `a`.`attend_id`=' . $this->uri->id;
        if (($profile = $query->getRecord($sql)) === false)
            throw new GSqlException();
        if (empty($profile))
            return '<div class="mn-row-body border">' . $this->_['msg_empty_profile'] . '<div class="wrap"></div>';
        // основные параметры
        $data .= '<fieldset class="fixed"><label>' . $this->_['title_fieldset_common'] . '</label><ul>';
        $data .= '<li><label>' . $this->_['label_attend_date'] . ':</label> ' . date($settings['format/datetime'], strtotime($profile['attend_date'] .  ' ' . $profile['attend_time'])) . '</li>';
        $data .= '<li><label>' . $this->_['label_attend_browser'] . ':</label> ' . $profile['attend_browser'] . '</li>';
        $data .= '<li><label>' . $this->_['label_attend_os'] . ':</label> ' . $profile['attend_os'] . '</li>';
        $data .= '<li><label>' . $this->_['label_attend_ipaddress'] . ':</label> ' . $profile['attend_ipaddress'] . '</li>';
        $data .= '<li><label>' . $this->_['label_attend_name'] . ':</label> ' . $profile['attend_name'] . '</li>';
        $data .= '<li><label>' . $this->_['label_attend_error'] . ':</label> ' . $profile['attend_error'] . '</li>';
        $data .= '<li><label>' . $this->_['label_attend_success'] . ':</label> ' . $this->_['data_boolean'][$profile['attend_success']] . '</li>';
        $data .= '</ul></fieldset>';
        $data .= '<div class="wrap"></div>';
        // если есть пользователь
        if (!empty($profile['profile_id'])) {
            // формирование атрибутов
            if (empty($profile['profile_born']))
                $born = '';
            else
                $born = date($settings['format/date'], strtotime($profile['profile_born']));
            $data .= '<fieldset class="fixed"><label>' . $this->_['title_fieldset_user'] . '</label><ul>';
            $data .= '<li><label>' . $this->_['label_profile_name'] . ':</label> ' . $profile['profile_name'] . '</li>';
            $data .= '<li><label>' . $this->_['label_profile_born'] . ':</label> ' . $born . '</li>';
            $data .= '<li><label>' . $this->_['label_profile_gender'] . ':</label> ' . $this->_['data_gender'][$profile['profile_gender']] . '</li>';
            $data .= '</ul></fieldset>';
            $data .= '<div class="wrap"></div>';
            // Средства связи
            $data .= '<fieldset class="fixed" style="width:260px"><label>' . $this->_['title_fieldset_connection'] . '</label><ul>';
            $data .= '<li><label>' . $this->_['label_contact_work_phone'] . ':</label> ' . $profile['contact_work_phone'] . '</li>';
            $data .= '<li><label>' . $this->_['label_contact_mobile_phone'] . ':</label> ' . $profile['contact_mobile_phone'] . '</li>';
            $data .= '<li><label>' . $this->_['label_contact_home_phone'] . ':</label> ' . $profile['contact_home_phone'] . '</li>';
            $data .= '<li><label>E-mail:</label> ' . $profile['contact_email'] . '</li>';
            $data .= '<li><label>ICQ:</label> ' . $profile['contact_icq'] . '</li>';
            $data .= '<li><label>Skype:</label> ' . $profile['contact_skype'] . '</li>';
            $data .= '</ul></fieldset>';
            // Социальные сети
            $data .= '<fieldset class="fixed" style="width:260px"><label>' . $this->_['title_fieldset_net'] . '</label><ul>';
            $data .= '<li><label>Facebook:</label> ' . $profile['contact_facebook'] . '</li>';
            $data .= '<li><label>LinkedIn:</label> ' . $profile['contact_linkedin'] . '</li>';
            $data .= '<li><label>Twitter:</label> ' . $profile['contact_twitter'] . '</li>';
            $data .= '<li><label>VK:</label> ' . $profile['contact_vk'] . '</li>';
            $data .= '</ul></fieldset>';
            // Адрес
            $data .= '<fieldset class="fixed" style="width:260px"><label>' . $this->_['title_fieldset_address'] . '</label><ul>';
            $data .= '<li><label>' . $this->_['label_address_index'] . ':</label> ' . $profile['profile_address_index'] . '</li>';
            $data .= '<li><label>' . $this->_['label_address_country'] . ':</label> ' . $profile['profile_address_country'] . '</li>';
            $data .= '<li><label>' . $this->_['label_address_region'] . ':</label> ' . $profile['profile_address_region'] . '</li>';
            $data .= '<li><label>' . $this->_['label_address_city'] . ':</label> ' . $profile['profile_address_city'] . '</li>';
            $data .= '<li><label>' . $this->_['label_address'] . ':</label> ' . $profile['profile_address'] . '</li>';
            $data .= '</ul></fieldset>';
            $data .= '<div class="wrap"></div>';
        }

        return '<div class="mn-row-body border">' . $data . '<div class="wrap"></div>';
    }
}
?>