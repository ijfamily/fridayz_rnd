<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Авторизация пользователей"
 * Группа пакетов     "Журналы"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalAttends
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Авторизация пользователей систем
 * Описание: Журнал авторизаций пользователей системы
 * ID класса: gcontroller_yjournalattends_grid
 * Группа: Администрирование / Журнал событий
 * Очищать: нет
 * Ресурс
 *    Пакет: administration/journals/attends/
 *    Контроллер: administration/journals/attends/Grid/
 *    Интерфейс: grid/interface/
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске: нет
 */

return array(
    // {Y}- модуль "System" {Journal} - пакет контроллеров "JournalAttends" -> YJournalAttends
    'clsPrefix' => 'YJournalAttends'
);
?>