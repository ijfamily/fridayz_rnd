<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск в списке авторизаций пользователей"
 * Пакет контроллеров "Поиск в списке авторизаций пользователей"
 * Группа пакетов     "Авторизация пользователей"
 * Модуль             "Журналы"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalAttends_Search
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск в списке авторизаций пользователей
 * 
 * @category   Gear
 * @package    GController_YJournalAttends_Search
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalAttends_Search_Data extends GController_Search_Data
{
    /**
     * дентификатор компонента (списка) к которому применяется поиск
     *
     * @var string
     */
    public $gridId = 'gcontroller_yjournalattends_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_yjournalattends_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор,
     * используется для инициализации атрибутов контроллера без конструктора
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'attend_date', 'label' => $this->_['header_attend_date']),
            array('dataIndex' => 'attend_browser', 'label' => $this->_['header_attend_browser']),
            array('dataIndex' => 'attend_os', 'label' => $this->_['header_attend_os']),
            array('dataIndex' => 'attend_ipaddress', 'label' => $this->_['header_attend_ipaddress']),
            array('dataIndex' => 'attend_name', 'label' => $this->_['header_attend_name']),
            array('dataIndex' => 'attend_error', 'label' => $this->_['header_attend_error']),
            array('dataIndex' => 'profile_name', 'label' => $this->_['header_profile_name']),
            array('dataIndex' => 'attend_success', 'label' => $this->_['header_attend_success'])
        );
    }

    /**
     * Применение фильтра
     * 
     * @return void
     */
    protected function dataAccept()
    {
        if ($this->_sender == 'toolbar') {
            if ($this->input->get('action') == 'search') {
                $tlbFilter = '';
                // если попытка сбросить фильтр
                if (!$this->input->isEmptyPost(array('filter_df', 'filter_dt'))) {
                    $dateFrom = $this->input->getDate('filter_df');
                    $dateTo = $this->input->getDate('filter_dt');
                    if ($dateFrom && $dateTo) {
                        $tlbFilter = " AND `attend_date` BETWEEN '$dateFrom' AND '$dateTo' ";
                    }
                }
                $this->store->set('tlbFilter', $tlbFilter);
                return;
            }
        }

        parent::dataAccept();
    }
}
?>