<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Поиск в списке "Авторизация пользователей"',
    // поля
    'header_attend_date'      => 'Дата',
    'header_attend_browser'   => 'Браузер',
    'header_attend_os'        => 'ОС',
    'header_attend_ipaddress' => 'IP адрес',
    'header_attend_name'      => 'Логин',
    'header_profile_name'     => 'Имя',
    'header_attend_error'     => 'Ошибки',
    'header_attend_success'   => 'Успех'
);
?>