<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля авторизации пользователей"
 * Пакет контроллеров "Авторизация пользователей"
 * Группа пакетов     "Журналы"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalAttends_Info
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные профиля авторизации пользователей
 * 
 * @category   Gear
 * @package    GController_YJournalAttends_Info
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalAttends_Info_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array(
        'attend_date', 'attend_time', 'attend_browser', 'attend_os','attend_ipaddress', 'attend_name',
        'attend_error', 'profile_name', 'attend_success', 'attend_error'
    );

    /**
     * Первичное поле таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'attend_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_user_attends';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_yjournalattends_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record array fields
     * @return string
     */
    protected function getTitle($record)
    {
        $settings = $this->session->get('user/settings');

        return sprintf($this->_['title_info'],
               date($settings['format/datetime'], strtotime($record['attend_date']. ' ' . $record['attend_time'])));
    }

    /**
     * Добавление в журнал действий пользователей
     * (удаление данных)
     * 
     * @param array $params параметры журнала
     * @return void
     */
    protected function logDelete($params = array())
    {}

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        $settings = $this->session->get('user/settings');
        $record['attend_date'] = date($settings['format/datetime'], strtotime($record['attend_date'] . ' ' . $record['attend_time']));
        $record['attend_success'] = $this->_['data_attend_success'][$record['attend_success']];

        return $record;
    }
}
?>