<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_info' => 'Record details "%s"',
    // поля
    'label_attend_date'       => 'Details',
    'label_attend_browser'    => 'Browser',
    'label_attend_os'         => 'OS',
    'label_attend_ipaddress'  => 'IP address',
    'label_attend_name'       => 'Login',
    'label_people_category'   => 'User',
    'label_profile_name'      => 'Name',
    'label_attend_error'      => 'Error',
    'label_attend_success'    => 'Success',
    // типы
    'data_attend_success' => array('Yes', 'No'),
);
?>