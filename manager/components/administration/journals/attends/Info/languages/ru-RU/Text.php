<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_info' => 'Информация о записи "%s"',
    // поля
    'label_attend_date'       => 'Дата',
    'label_attend_browser'    => 'Браузер',
    'label_attend_os'         => 'ОС',
    'label_attend_ipaddress'  => 'IP адрес',
    'label_attend_name'       => 'Логин',
    'label_people_category'   => 'Пользователь',
    'label_profile_name'      => 'Имя',
    'label_attend_error'      => 'Ошибки',
    'label_attend_success'    => 'Успех',
    // типы
    'data_attend_success' => array('Нет', 'Да'),
);
?>