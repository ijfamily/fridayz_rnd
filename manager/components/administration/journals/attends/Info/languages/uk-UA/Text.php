<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_info' => 'Деталі запису "%s"',
    // поля
    'label_attend_date'       => 'Дата',
    'label_attend_browser'    => 'Браузер',
    'label_attend_os'         => 'ОС',
    'label_attend_ipaddress'  => 'IP адреса',
    'label_attend_name'       => 'Логін',
    'label_people_category'   => 'Користувач',
    'label_profile_name'      => 'Ім\'я',
    'label_attend_error'      => 'Помилки',
    'label_attend_success'    => 'Успіх',
    'data_attend_success'     => array ('Ні', 'Так')
);
?>