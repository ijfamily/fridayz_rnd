<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля авторизации пользователей"
 * Пакет контроллеров "Авторизация пользователей"
 * Группа пакетов     "Журналы"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalAttends_Info
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля авторизации пользователей
 * 
 * @category   Gear
 * @package    GController_YJournalAttends_Info
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalAttends_Info_Interface extends GController_Profile_Interface
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_yjournalattends_grid';

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('titleEllipsis' => 45,
                  'gridId'        => 'gcontroller_yjournalattends_grid',
                  'width'         => 370,
                  'autoHeight'    => true,
                  'resizable'     => false,
                  'state'         => 'info',
                  'stateful'      => false)
        );

        // поля формы (Ext.form.FormPanel)
        $items = array(
           array('xtype'      => 'displayfield',
                 'fieldLabel' => $this->_['label_attend_date'],
                 'fieldClass' => 'mn-field-value-info',
                 'name'       => 'attend_date'),
           array('xtype'      => 'displayfield',
                 'fieldLabel' => $this->_['label_attend_browser'],
                 'fieldClass' => 'mn-field-value-info',
                 'name'       => 'attend_browser'),
           array('xtype'      => 'displayfield',
                 'fieldLabel' => $this->_['label_attend_os'],
                 'fieldClass' => 'mn-field-value-info',
                 'name'       => 'attend_os'),
           array('xtype'      => 'displayfield',
                 'fieldLabel' => $this->_['label_attend_ipaddress'],
                 'fieldClass' => 'mn-field-value-info',
                 'name'       => 'attend_ipaddress'),
           array('xtype'      => 'displayfield',
                 'fieldLabel' => $this->_['label_attend_name'],
                 'fieldClass' => 'mn-field-value-info',
                 'name'       => 'attend_name'),
           array('xtype'      => 'displayfield',
                 'fieldLabel' => $this->_['label_profile_name'],
                 'fieldClass' => 'mn-field-value-info',
                 'name'       => 'profile_name'),
           array('xtype'      => 'displayfield',
                 'fieldLabel' => $this->_['label_attend_success'],
                 'fieldClass' => 'mn-field-value-info',
                 'name'       => 'attend_success'),
           array('xtype'      => 'displayfield',
                 'fieldLabel' => $this->_['label_attend_error'],
                 'fieldClass' => 'mn-field-value-info',
                 'name'       => 'attend_error')
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->addItems($items);
        $form->labelWidth = 60;
        $form->url = $this->componentUrl . 'info/';

        parent::getInterface();
    }
}
?>