<?php
/**
 * Gear Manager
 *
 * Контроллер         "Триггер выпадающего списка обработки данных"
 * Пакет контроллеров "Авторизация пользователей"
 * Группа пакетов     "Журналы"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalAttends_Combo
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Combo/Trigger');

/**
 * Триггер выпадающего списка обработки данных
 * 
 * @category   Gear
 * @package    GController_YJournalAttends_Combo
 * @subpackage Trigger
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalAttends_Combo_Trigger extends GController_Combo_Trigger
{
    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $name название триггера
     * @return void
     */
    protected function dataView($name)
    {
        parent::dataView($name);

        // имя триггера
        switch ($name) {
            // пользователи
            case 'users': $this->query('gear_users', 'user_name', 'user_name'); break;

            // контингент
            case 'profiles': $this->query('gear_user_profiles', 'profile_name', 'profile_name'); break;
        }
    }
}
?>