<?php
/**
 * Gear Manager
 *
 * Контроллер         "Триггер дерева"
 * Пакет контроллеров "Триггер дерева"
 * Группа пакетов     "Журналы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalAttends_Tree
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Tree/Trigger');

/**
 * Триггер дерева
 * 
 * @category   Gear
 * @package    GController_YJournalAttends_Tree
 * @subpackage Trigger
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalAttends_Tree_Trigger extends GController_Tree_Trigger
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_yjournalattends_grid';

    /**
     * Возращает список ОС
     *
     * @return array
     */
    protected function getOS()
    {
        $data = array();
        $query = new GDb_Query();
        $sql = 'SELECT *, COUNT(*) `count` FROM `gear_user_attends` GROUP BY `attend_os`';
        if ($query->execute($sql) !== true)
            throw new GSqlException();
        while (!$query->eof()) {
            $rec = $query->next();
            $data[] = array(
                'text'    => $rec['attend_os'] . ' <b>(' . $rec['count'] . ')</b>',
                'qtip'    => $rec['attend_os'],
                'leaf'    => true,
                'iconCls' => 'icon-folder-find',
                'value'   => $rec['attend_os']
            );
        }

        return $data;
    }

    /**
     * Возращает список версий браузера
     *
     * @return array
     */
    protected function getBrowser()
    {
        $data = array();
        $query = new GDb_Query();
        $sql = 'SELECT *, COUNT(*) `count` FROM `gear_user_attends` GROUP BY `attend_browser`';
        if ($query->execute($sql) !== true)
            throw new GSqlException();
        while (!$query->eof()) {
            $rec = $query->next();
            $data[] = array(
                'text'    => $rec['attend_browser'] . ' <b>(' . $rec['count'] . ')</b>',
                'qtip'    => $rec['attend_browser'],
                'leaf'    => true,
                'iconCls' => 'icon-folder-find',
                'value'   => $rec['attend_browser']
            );
        }

        return $data;
    }

    /**
     * Вывод данных в интерфейс компонента
     * 
     * @param  string $name название триггера
     * @param  string $node id выбранного узла
     * @return void
     */
    protected function dataView($name, $node)
    {
        parent::dataView($name, $node);

        // триггер
        switch ($name) {
            // быстрый фильтр списка
            case 'gridFilter':
                // id выбранного узла
                switch ($node) {
                    // по версии ос
                    case 'byOs':
                        $this->response->data = $this->getOS();
                        break;

                    // по версии браузера
                    case 'byBr':
                        $this->response->data = $this->getBrowser();
                        break;
                }
        }
    }
}
?>