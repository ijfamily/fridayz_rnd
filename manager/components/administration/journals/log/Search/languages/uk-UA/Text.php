<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'search_title' => 'Пошук у списку "Компоненти"',
    // поля
    'header_log_date'         => 'Дата',
    'header_log_error'        => 'Помилки',
    'header_log_query_params' => 'Параметри',
    'header_profile_name'     => 'Користувач',
    'header_user_name'        => 'Логін',
    'header_log_query_id'     => 'ID',
    'header_log_query'        => 'Запрос',
    'header_log_action'       => 'Дія',
    'header_log_uri'          => 'Адрес',
    'header_controller_name'  => 'Компонент',
    'header_controller_class' => 'Клас'
);
?>