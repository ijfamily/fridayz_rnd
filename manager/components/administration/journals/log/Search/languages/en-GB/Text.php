<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'search_title' => 'Search in the "Log of user actions"',
    // поля
    'header_log_date'         => 'Date',
    'header_log_error'        => 'Error',
    'header_log_query_params' => 'Params',
    'header_profile_name'     => 'User',
    'header_user_name'        => 'Login',
    'header_log_query_id'     => 'ID',
    'header_log_query'        => 'Request',
    'header_log_action'       => 'Action',
    'header_log_uri'          => 'URL',
    'header_controller_name'  => 'Component',
    'header_controller_class' => 'Class'
);
?>