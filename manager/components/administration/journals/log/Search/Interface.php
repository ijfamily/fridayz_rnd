<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск в списке действий пользователей"
 * Пакет контроллеров "Список действий пользователей"
 * Группа пакетов     "Журнал действий пользователей"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalLog_Search
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Interface');

/**
 * Поиск в списке действий пользователей
 * 
 * @category   Gear
 * @package    GController_YJournalLog_Search
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalLog_Search_Interface extends GController_Search_Interface
{
    /**
     * Идентификатор DOM компонента (списка)
     *
     * @var string
     */
    public $gridId = 'gcontroller_yjournallog_grid';

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataSearch")
        $this->_cmp->setProps(
            array('title'       => $this->_['search_title'],
                  'gridId'      => $this->gridId,
                  'btnSearchId' => $this->gridId . '-bnt_search',
                  'url'         => $this->componentUrl . 'search/data/')
        );

        // виды полей для поиска записей
        $this->_cmp->editorFields = array(
            array('xtype'      => 'datefield', 'format' => 'd-m-Y'),
            array('xtype'      => 'mn-field-combo',
                  'name'       => 'user_name',
                  'editable'   => true,
                  'pageSize'   => 50,
                  'width'      => 260,
                  'hiddenName' => 'user_name',
                  'allowBlank' => true,
                  'store'      => array(
                      'xtype' => 'jsonstore',
                      'url'   => $this->componentUrl . 'combo/trigger/?name=users'
                  )
            ),
            array('xtype'      => 'mn-field-combo',
                  'name'       => 'profile_name',
                  'editable'   => true,
                  'pageSize'   => 50,
                  'width'      => 260,
                  'hiddenName' => 'profile_name',
                  'allowBlank' => true,
                  'store'      => array(
                      'xtype' => 'jsonstore',
                      'url'   => $this->componentUrl . 'combo/trigger/?name=profiles'
                  )
            ),
            array('xtype'      => 'mn-field-combo',
                  'name'       => 'controller_name',
                  'editable'   => true,
                  'pageSize'   => 50,
                  'width'      => 260,
                  'hiddenName' => 'controller_name',
                  'allowBlank' => true,
                  'store'      => array(
                      'xtype' => 'jsonstore',
                      'url'   => $this->componentUrl . 'combo/trigger/?name=controllers'
                  )
            ),
            array('xtype'      => 'mn-field-combo',
                  'name'       => 'controller_class',
                  'editable'   => true,
                  'pageSize'   => 50,
                  'width'      => 260,
                  'hiddenName' => 'controller_class',
                  'allowBlank' => true,
                  'store'      => array(
                      'xtype' => 'jsonstore',
                      'url'   => $this->componentUrl . 'combo/trigger/?name=classes'
                  )
            ),
            array('xtype'      => 'mn-field-combo',
                  'name'       => 'controller_url',
                  'editable'   => true,
                  'pageSize'   => 50,
                  'width'      => 260,
                  'hiddenName' => 'controller_url',
                  'allowBlank' => true,
                  'store'      => array(
                      'xtype' => 'jsonstore',
                      'url'   => $this->componentUrl . 'combo/trigger/?name=urls'
                  )
            ),
            array('xtype' => 'textfield'),
            array('xtype' => 'numberfield'),
            array('xtype' => 'textfield'),
            array('xtype' => 'textfield'),
            array('xtype' => 'textfield')
        );

        parent::getInterface();
    }
}
?>