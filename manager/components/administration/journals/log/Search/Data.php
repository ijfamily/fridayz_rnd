<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск в списке действий пользователей"
 * Пакет контроллеров "Список действий пользователей"
 * Группа пакетов     "Журнал действий пользователей"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalLog_Search
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск в списке действий пользователей
 * 
 * @category   Gear
 * @package    GController_YJournalLog_Search
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalLog_Search_Data extends GController_Search_Data
{
    /**
     * Идентификатор DOM компонента (списка)
     *
     * @var string
     */
    public $gridId = 'gcontroller_yjournallog_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_yjournallog_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор,
     * используется для инициализации атрибутов контроллера без конструктора
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => '_log_date',
                  'label'     => $this->_['header_log_date']),
            array('dataIndex' => 'user_name',
                  'label'     => $this->_['header_user_name']),
            array('dataIndex' => 'profile_name',
                  'label'     => $this->_['header_profile_name']),
            array('dataIndex' => 'controller_name',
                  'label'     => $this->_['header_controller_name']),
            array('dataIndex' => 'controller_class',
                  'label'     => $this->_['header_controller_class']),
            array('dataIndex' => 'log_url',
                  'label'     => $this->_['header_log_uri']),
            array('dataIndex' => 'log_action',
                  'label'     => $this->_['header_log_action']),
            array('dataIndex' => 'log_query_id',
                  'label'     => $this->_['header_log_query_id']),
            array('dataIndex' => 'log_query',
                  'label'     => $this->_['header_log_query']),
            array('dataIndex' => 'log_query_params',
                  'label'     => $this->_['header_log_query_params']),
            array('dataIndex' => 'log_error',
                  'label'     => $this->_['header_log_error'])
        );
    }
}
?>