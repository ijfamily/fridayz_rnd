<?php
/**
 * Gear Manager
 *
 * Контроллер         "Триггер выпадающего списка"
 * Пакет контроллеров "Журнал действия пользователей"
 * Группа пакетов     "Журналы"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalLog_Combo
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Combo/Trigger');

/**
 * Триггер выпадающего списка
 * 
 * @category   Gear
 * @package    GController_YJournalLog_Combo
 * @subpackage Trigger
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalLog_Combo_Trigger extends GController_Combo_Trigger
{
    /**
     * Вывод компонентов
     * 
     * @return void
     */
    protected function queryControllers()
    {
        // выбранный язык
        $languageId = $this->session->get('language/default/id');
        $table = new GDb_Table('gear_controllers_l', 'controller_lang_id');
        $table->setStart($this->_start);
        $table->setLimit($this->_limit);
        $sql = 'SELECT SQL_CALC_FOUND_ROWS * '
             . 'FROM `gear_controllers_l` '
             . 'WHERE 1 ' . ($this->_query ? "AND `controller_name` LIKE '{$this->_query}%' " : '')
             . ' AND `language_id`=' . $languageId
             . ' ORDER BY `controller_name` ASC '
             . 'LIMIT %limit';
        $table->query($sql);
        $data = array();
        while (!$table->query->eof()) {
            $record = $table->query->next();
            $data[] = array(
                'id'   => $record['controller_name'],
                'name' => $record['controller_name']
            );
        }
        $this->response->set('totalCount', $table->query->getFoundRows());
        $this->response->success = true;
        $this->response->data = $data;
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $name название триггера
     * @return void
     */
    protected function dataView($name)
    {
        parent::dataView($name);

        // имя триггера
        switch ($name) {
            // пользователи
            case 'users': $this->query('gear_users', 'user_name', 'user_name'); break;

            // контингент
            case 'profiles': $this->query('gear_user_profiles', 'profile_name', 'profile_name'); break;

            // компоненты
            case 'controllers': $this->queryControllers(); break;

            // классы компонентов
            case 'classes': $this->query('gear_controllers', 'controller_class', 'controller_class'); break;

            // адреса компонентов
            case 'urls': $this->query('gear_controllers', 'controller_url', 'controller_url'); break;
        }
    }
}
?>