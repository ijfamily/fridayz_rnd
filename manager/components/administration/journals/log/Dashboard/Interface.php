<?php
/**
 * Gear Manager
 *
 * Контроллер         "Доска действий пользователей"
 * Пакет контроллеров "Список действий пользователей"
 * Группа пакетов     "Журнал действий пользователей"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalLog_Dashboard
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Доска действий пользователей
 * 
 * @category   Gear
 * @package    GController_YJournalLog_Dashboard
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalLog_Dashboard_Interface extends GController_Dashboard_Interface
{
    /**
     * Возращает дополнительные данные для формирования интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        $data = array('total' => array());

        $settings = $this->session->get('user/settings');
        // соединение с базой данных
        GFactory::getDb()->connect();
        $query = new GDb_Query();
        // всего сообщений
        $sql = 'SELECT COUNT(*) `total`, MAX(`log_id`) `id`, MAX(`log_date`) `last` '
             . 'FROM `gear_log`';
        $rec = $query->getRecord($sql);
        if ($rec === false)
            throw new GSqlException();
        if (!empty($rec)) {
            $data['total']['total'] = $rec['total'];
            if ($rec['last'])
                $data['total']['last'] = date($settings['format/date'], strtotime($rec['last']));
            $data['total']['id'] = $rec['id'];
        }

        // сообщения сегодня
        $sql = 'SELECT COUNT(*) `total`, `log_id` FROM `gear_log` '
              .'WHERE DATE_FORMAT(NOW(), \'%Y-%m-%d\')=DATE_FORMAT(`log_date`, \'%Y-%m-%d\') ';
        $rec = $query->getRecord($sql);
        if ($rec === false)
            throw new GSqlException();
        if (!empty($rec))
            $data['today'] = array('total' => $rec['total']);

        return $data;
    }

    /**
     * Вывод контента
     * 
     * @param  array $data данные контента
     * @return void
     */
    protected function content($data)
    {
        $widgetUrl = 'administration/journals/log/' . ROUTER_DELIMITER . 'grid/interface/';
        if (!empty($data['total']['id']))
            $url = 'administration/journals/log/' . ROUTER_DELIMITER . 'info/interface/' . $data['total']['id'];
?>
    <a href="#" onclick="App.loadCmp({url: Manager.COMPONENTS_URL + '<?php echo $widgetUrl;?>'});">
    <img align="left" src="<?php echo $this->resourcePath;?>icon.png" />
    </a>
    <em><?php echo $this->_['msg_count'];?></em>: <b><?php echo $data['total']['total'];?></b><br />
    <em><?php echo $this->_['msg_count_today'];?></em>: <b><?php echo $data['today']['total'];?></b><br />
<?php if (!empty($data['total']['last'])) { ?>
    <em><?php echo $this->_['msg_last'];?></em>:
    <a href="#" onclick="javascript:Manager.widget.load({url:'router.php/<?php echo $url;?>'});">
    <?php echo $data['total']['last'];?></a><br />
<?php } ?>
    <div class="wrap"></div>
<?php
    }
}
?>