<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля действий пользователей"
 * Пакет контроллеров "Список действий пользователей"
 * Группа пакетов     "Журнал действий пользователей"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalLog_Info
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные профиля действий пользователей
 * 
 * @category   Gear
 * @package    GController_YJournalLog_Info
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalLog_Info_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array(
        'controller_class', 'log_url', 'log_action', 'log_query', 'log_query_id', 'log_date',
        'log_query_params', 'log_date', 'log_time', 'log_error', 'user_name', 'profile_name',
        'contact_email', 'controller_name'
    );

    /**
     * Первичное поле таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'log_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_log';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_yjournallog_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record array fields
     * @return string
     */
    protected function getTitle($record)
    {
        $settings = $this->session->get('user/settings');
        if (!empty($record['log_date']))
            return sprintf($this->_['title_info'], 
                   date($settings['format/datetime'], strtotime($record['log_date']. ' ' . $record['log_time'])));
        else
            return sprintf($this->_['title_info'], '');
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        $languageId = $this->session->get('language/default/id');
        $sql = 'SELECT * '
             . 'FROM `gear_log` `l`, `gear_user_profiles` `p`, `gear_users` `u`, '
             . '(SELECT `c`.`controller_id`, `cl`.`controller_name` '
             . 'FROM `gear_controllers` `c`, `gear_controllers_l` `cl` '
             . 'WHERE `c`.`controller_id`=`cl`.`controller_id` AND '
             . '`cl`.`language_id`=' . $languageId . ') `c` '
             . 'WHERE `p`.`user_id`=`l`.`user_id` AND '
             . '`c`.`controller_id`=`l`.`controller_id` AND '
             . '`u`.`user_id`=`l`.`user_id` AND '
             . '`l`.`log_id`=%id%';

        parent::dataView($sql);
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        $settings = $this->session->get('user/settings');
        if (!empty($record['log_query_params']))
            $record['log_query_params_php'] = var_export(json_decode($record['log_query_params'], true), true);
        if (!empty($record['log_date']))
            $record['log_date'] = date($settings['format/datetime'], strtotime($record['log_date']. ' ' . $record['log_time']));

        return $record;
    }
}
?>