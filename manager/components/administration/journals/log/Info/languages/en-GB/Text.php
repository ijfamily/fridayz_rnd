<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_info' => 'Record details "%s"',
    // поля
    'title_tab_common'        => 'Common', 
    'label_log_date'          => 'Data',
    'label_log_error'         => 'Error',
    'label_log_query_params'  => 'Params',
    'label_profile_name'      => 'User',
    'label_log_query_id'      => 'ID',
    'label_log_query'         => 'Query',
    'label_log_action'        => 'Acition',
    'label_log_uri'           => 'URI',
    'label_controller_name'   => 'Component',
    'label_controller_class'  => 'Class',
    'label_profile_nick'      => 'Nick',
    'label_contact_email'     => 'E-mail',
    'title_tab_params_json'   => 'Params (JSON)',
    'title_tab_params_php'    => 'Params (PHP)',
    'title_tab_query'         => 'Query',
    'title_tab_error'         => 'Error'
);
?>