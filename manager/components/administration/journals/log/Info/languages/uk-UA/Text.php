<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_info' => 'Деталі запису "%s"',
    // поля
    'title_tab_common'        => 'Основні параметри', 
    'label_log_date'          => 'Дата',
    'label_log_error'         => 'Помилки',
    'label_log_query_params'  => 'Параметри',
    'label_profile_name'      => 'Користувач',
    'label_log_query_id'      => 'ID',
    'label_log_query'         => 'Запрос',
    'label_log_action'        => 'Дія',
    'label_log_uri'           => 'Адрес',
    'label_controller_name'   => 'Компонент',
    'label_controller_class'  => 'Клас',
    'label_profile_nick'      => 'Нік',
    'label_contact_email'     => 'E-mail',
    'title_tab_params_json'   => 'Параметри (JSON)',
    'title_tab_params_php'    => 'Параметри (PHP)',
    'title_tab_query'         => 'Запрос',
    'title_tab_error'         => 'Помилки'
);
?>