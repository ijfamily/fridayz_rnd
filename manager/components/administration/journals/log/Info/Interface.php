<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля действий пользователей"
 * Пакет контроллеров "Список действий пользователей"
 * Группа пакетов     "Журнал действий пользователей"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalLog_Info
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля действий пользователей
 * 
 * @category   Gear
 * @package    GController_YJournalLog_Info
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalLog_Info_Interface extends GController_Profile_Interface
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_yjournallog_grid';

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('titleEllipsis' => 45,
                  'gridId'        => 'gcontroller_yjournallog_grid',
                  'width'         => 500,
                  'height'        => 350,
                  'resizable'     => false,
                  'state'         => 'info',
                  'stateful'      => false)
        );

        // Ext.form.FormPanel
        // tab "common"
        $tabCommon = array(
            'title'       => $this->_['title_tab_common'],
            'layout'      => 'form',
            'labelWidth'  => 90,
            'bodyStyle'   => 'padding: 6px;background-color:#F3F5F4',
            'defaultType' => 'textfield',
            'items'       => array(
                array('xtype'      => 'displayfield',
                      'fieldLabel' => $this->_['label_log_date'],
                      'fieldClass' => 'mn-field-value-info',
                      'name'       => 'log_date'),
                array('xtype'      => 'displayfield',
                      'fieldLabel' => $this->_['label_profile_name'],
                      'fieldClass' => 'mn-field-value-info',
                      'name'       => 'profile_name'),
                array('xtype'      => 'displayfield',
                      'fieldLabel' => $this->_['label_contact_email'],
                      'fieldClass' => 'mn-field-value-info',
                      'name'       => 'contact_email'),
                array('xtype'      => 'displayfield',
                      'fieldLabel' => $this->_['label_log_action'],
                      'fieldClass' => 'mn-field-value-info',
                      'name'       => 'log_action'),
                array('xtype'      => 'displayfield',
                      'fieldLabel' => $this->_['label_log_query_id'],
                      'fieldClass' => 'mn-field-value-info',
                      'name'       => 'log_query_id'),
                array('xtype'      => 'displayfield',
                      'fieldLabel' => $this->_['label_controller_name'],
                      'fieldClass' => 'mn-field-value-info',
                      'name'       => 'controller_name'),
                array('xtype'      => 'displayfield',
                      'fieldLabel' => $this->_['label_controller_class'],
                      'fieldClass' => 'mn-field-value-info',
                      'name'       => 'controller_class'),
                array('xtype'      => 'displayfield',
                      'fieldLabel' => $this->_['label_log_uri'],
                      'fieldClass' => 'mn-field-value-info',
                      'name'       => 'log_url')
            )
        );

        // вкладка "params (json)"
       $tabParamsJSON = array(
           'title'       => $this->_['title_tab_params_json'],
           'layout'      => 'form',
           'labelWidth'  => 70,
           'bodyStyle'   => 'padding: 1px;background-color:#F3F5F4',
           'defaultType' => 'textfield',
           'items'       => array(
               array('xtype'      => 'textarea',
                     'hideLabel'  => true,
                     'name'       => 'log_query_params',
                     'readOnly'   => true,
                     'anchor'     => '100% 99%',
                     'allowBlank' => true)
           )
       );

        // вкладка "params (php)"
       $tabParamsPHP = array(
           'title'       => $this->_['title_tab_params_php'],
           'layout'      => 'form',
           'labelWidth'  => 70,
           'bodyStyle'   => 'padding: 1px;background-color:#F3F5F4',
           'defaultType' => 'textfield',
           'items'       => array(
               array('xtype'      => 'textarea',
                     'hideLabel'  => true,
                     'name'       => 'log_query_params_php',
                     'readOnly'   => true,
                     'anchor'     => '100% 99%',
                     'allowBlank' => true)
           )
       );

        // вкладка "query"
       $tabQuery = array(
           'title'       => $this->_['title_tab_query'],
           'layout'      => 'form',
           'labelWidth'  => 70,
           'bodyStyle'   => 'padding: 1px;background-color:#F3F5F4',
           'defaultType' => 'textfield',
           'items'       => array(
               array('xtype'      => 'textarea',
                     'hideLabel'  => true,
                     'name'       => 'log_query',
                     'readOnly'   => true,
                     'anchor'     => '100% 99%',
                     'allowBlank' => false)
           )
       );

        // вкладка "error"
       $tabError = array(
           'title'       => $this->_['title_tab_error'],
           'layout'      => 'form',
           'labelWidth'  => 70,
           'bodyStyle'   => 'padding: 1px;background-color:#F3F5F4',
           'defaultType' => 'textfield',
           'items'       => array(
               array('xtype'      => 'textarea',
                     'hideLabel'  => true,
                     'name'       => 'log_error',
                     'readOnly'   => true,
                     'anchor'     => '100% 99%',
                     'allowBlank' => false)
           )
       );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->url = $this->componentUrl . 'info/';
        $form->items->add(
            array('xtype'           => 'tabpanel',
                  'layoutOnTabChange' => true,
                  'deferredRender'  => false,
                  'activeTab'       => 0,
                  'bodyStyle'       => 'background-color:transparent',
                  'style'           => 'padding:3px',
                  'enableTabScroll' => true,
                  'anchor'          => '100% 100%',
                  'defaults'        => array('autoScroll' => true),
                  'items'           => array($tabCommon, $tabParamsJSON, $tabParamsPHP, $tabQuery, $tabError))
        );

        parent::getInterface();
    }
}
?>