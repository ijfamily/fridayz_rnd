<?php
/**
 * Gear Manager
 *
 * Контроллер         "Триггер дерева"
 * Пакет контроллеров "Триггер дерева"
 * Группа пакетов     "Журнал действий пользователей"
 * Модуль             "Администрирование"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalLog_Tree
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Tree/Trigger');

/**
 * Триггер дерева
 * 
 * @category   Gear
 * @package    GController_YJournalLog_Tree
 * @subpackage Trigger
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalLog_Tree_Trigger extends GController_Tree_Trigger
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_yjournallog_grid';

    /**
     * Возращает список компонентов
     *
     * @return array
     */
    protected function getСomponents()
    {
        $data = array();
        $query = new GDb_Query();
        $sql = 'SELECT `c`.`controller_id`, `cl`.`controller_name`, `cl`.`controller_about`, COUNT(*) `count` '
             . 'FROM `gear_log` `l` JOIN `gear_controllers` `c` USING(`controller_id`) '
             . 'JOIN `gear_controllers_l` `cl` USING(`controller_id`) '
             . 'WHERE `cl`.`language_id`=' . $this->language->get('id')
             . ' GROUP BY `l`.`controller_id`';
        if ($query->execute($sql) !== true)
            throw new GSqlException();
        while (!$query->eof()) {
            $rec = $query->next();
            $data[] = array(
                'text'    => $rec['controller_name'] . ' <b>(' . $rec['count'] . ')</b>',
                'qtip'    => $rec['controller_about'],
                'leaf'    => true,
                'iconCls' => 'icon-folder-find',
                'value'   => $rec['controller_id']
            );
        }

        return $data;
    }

    /**
     * Возращает список действий
     *
     * @return array
     */
    protected function getActions()
    {
        // действия пользователей
        $_ = GText::get('log actions', 'interface');
        $data = array();
        $query = new GDb_Query();
        $sql = 'SELECT *, COUNT(*) `count` FROM `gear_log` GROUP BY `log_action`';
        if ($query->execute($sql) !== true)
            throw new GSqlException();
        while (!$query->eof()) {
            $rec = $query->next();
            if (isset($_[$rec['log_action']]))
                $action = $_[$rec['log_action']];
            else
                $action = $_[$rec['log_action']];
            $data[] = array(
                'text'    => $action . ' <b>(' . $rec['count'] . ')</b>',
                'qtip'    => $action,
                'leaf'    => true,
                'iconCls' => 'icon-folder-find',
                'value'   => $rec['log_action']
            );
        }

        return $data;
    }

    /**
     * Возращает список пользователей
     *
     * @return array
     */
    protected function getUsers()
    {
        $data = array();
        $query = new GDb_Query();
        $sql = 'SELECT `p`.`profile_name`, `p`.`user_id`, COUNT(*) `count` '
             . 'FROM `gear_log` `l` '
             . 'JOIN `gear_user_profiles` `p` USING(`user_id`) '
             . 'GROUP BY `p`.`user_id`';
        if ($query->execute($sql) !== true)
            throw new GSqlException();
        while (!$query->eof()) {
            $rec = $query->next();
            $data[] = array(
                'text'    => $rec['profile_name'] . ' <b>(' . $rec['count'] . ')</b>',
                'qtip'    => $rec['profile_name'],
                'leaf'    => true,
                'iconCls' => 'icon-folder-find',
                'value'   => $rec['user_id']
            );
        }

        return $data;
    }

    /**
     * Вывод данных в интерфейс компонента
     * 
     * @param  string $name название триггера
     * @param  string $node id выбранного узла
     * @return void
     */
    protected function dataView($name, $node)
    {
        parent::dataView($name, $node);

        // триггер
        switch ($name) {
            // быстрый фильтр списка
            case 'gridFilter':
                // id выбранного узла
                switch ($node) {
                    // по компоненту
                    case 'byCp':
                        $this->response->data = $this->getСomponents();
                        break;

                    // по действию
                    case 'byAc':
                        $this->response->data = $this->getActions();
                        break;

                    // по пользователю
                    case 'byUs':
                        $this->response->data = $this->getUsers();
                        break;
                }
        }
    }
}
?>