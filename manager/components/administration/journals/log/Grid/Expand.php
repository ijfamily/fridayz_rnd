<?php
/**
 * Gear Manager
 *
 * Контроллер         "Развёрнутая запись действия пользователя"
 * Пакет контроллеров "Список журнал действий пользователей"
 * Группа пакетов     "Журнал действий пользователей"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalLog_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Expand');

/**
 * Развёрнутая запись действия пользователя
 * 
 * @category   Gear
 * @package    GController_YJournalLog_Grid
 * @subpackage Expand
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalLog_Grid_Expand extends GController_Grid_Expand
{
    /**
     * Вывод данных в интерфейс
     * 
     * @return void
     */
    protected function dataExpand()
    {
        parent::dataAccessView();

        $this->response->data = $this->getAttributes();
    }

    /**
     * Возращает атрибуты записи
     * 
     * @return string
     */
    protected function getAttributes()
    {
        $data = '';
        $settings = $this->session->get('user/settings');
        $query = new GDb_Query();
        // протокол
        $sql = 
            'SELECT * '
          . 'FROM `gear_log` `l` '
          . 'LEFT JOIN (SELECT `cl`.`controller_name`, `c`.* '
          . 'FROM `gear_controllers` `c`, `gear_controllers_l` `cl` WHERE `cl`.`controller_id`=`c`.`controller_id` AND '
          . '`cl`.`language_id`=' . $this->session->get('language/default/id') . ') `s` ON `s`.`controller_id`=`l`.`controller_id` '
          . 'WHERE `l`.`log_id`=' . $this->uri->id;
        if (($log = $query->getRecord($sql)) === false)
            throw new GSqlException();
        // данные пользователя
        $sql = 
            'SELECT * '
          . 'FROM `gear_users` `u` JOIN `gear_user_profiles` `p` USING(`user_id`) '
          . 'WHERE `u`.`user_id`=' . $log['user_id'];
        if (($user = $query->getRecord($sql)) === false)
            throw new GSqlException();

        // формирование атрибутов
        $data = '<fieldset><label>' . $this->_['title_tab_common'] . '</label><ul>';
        $data .= '<li><label>' . $this->_['label_log_date'] . ':</label> ' 
                . date($settings['format/datetime'], strtotime($log['log_date'] .  ' ' . $log['log_time'])) . '</li>';
        $data .= '<li><label>' . $this->_['label_profile_name'] . ':</label> <a class="user" href="#" type="widget" url="administration/users/contingent/' . ROUTER_DELIMITER . 'profile/interface/' . $user['profile_id'] . '?state=info">' . $user['profile_name'] . '</a></li>';
        $data .= '<li><label>' . $this->_['label_contact_email'] . ':</label> ' . $user['contact_email'] . '</li>';
        $data .= '<li><label>' . $this->_['label_log_action'] . ':</label> ' . $log['log_action'] . '</li>';
        $data .= '<li><label>' . $this->_['label_log_query_id'] . ':</label> ' . $log['log_query_id'] . '</li>';
        $data .= '<li><label>' . $this->_['label_controller_name'] . ':</label> ' . $log['controller_name'] . '</li>';
        $data .= '<li><label>' . $this->_['label_controller_class'] . ':</label> ' . $log['controller_class'] . '</li>';
        $data .= '<li><label>' . $this->_['label_log_uri'] . ':</label> ' . $log['log_url'] . '</li>';
        $data .= '</ul></fieldset>';
        if (!empty($log['log_query_params'])) {
            $data .= '<fieldset><label>' . $this->_['title_tab_params_json'] . '</label><ul>';
            $data .= '<li>' . $log['log_query_params'] . '</li>';
            $data .= '</ul></fieldset>';
        }
        if (!empty($log['log_query_params'])) {
            $data .= '<fieldset><label>' . $this->_['title_tab_params_php'] . '</label><ul>';
            $data .= '<li>' . var_export(json_decode($log['log_query_params'], true), true) . '</li>';
            $data .= '</ul></fieldset>';
        }
        if (!empty($log['log_query'])) {
            $data .= '<fieldset><label>' . $this->_['title_tab_query'] . '</label><ul>';
            $data .= '<li>' . $log['log_query'] . '</li>';
            $data .= '</ul></fieldset>';
        }
        if (!empty($log['log_error'])) {
            $data .= '<fieldset><label>' . $this->_['title_tab_error'] . '</label><ul>';
            $data .= '<li>' . $log['log_error'] . '</li>';
            $data .= '</ul></fieldset>';
        }
        $data = '<div class="mn-row-body border">' . $data . '<div class="wrap"></div></div>';

        return $data;
    }
}
?>