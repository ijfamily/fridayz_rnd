<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс списка действий пользователей"
 * Пакет контроллеров "Список действий пользователей"
 * Группа пакетов     "Журнал действий пользователей"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalLog_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс списка действий пользователей
 * 
 * @category   Gear
 * @package    GController_YJournalLog_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalLog_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'log_id';

    /**
     * Вид сортировки
     * (config options - Ext.data.JsonStore.sortInfo.dir)
     *
     * @var string
     */
    public $dir = 'DESC';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'log_date';

    /**
     * Использовать быстрый фильтр
     *
     * @var boolean
     */
    public $useSlidePanel = true;

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // класс контроллера
            array('name' => 'controller_class', 'type' => 'string'),
            // название контроллера
            array('name' => 'controller_name', 'type' => 'string'),
            // ресурс
            array('name' => 'log_url', 'type' => 'string'),
            // имя действия
            array('name' => 'log_action', 'type' => 'string'),
            // запрос
            array('name' => 'log_query', 'type' => 'string'),
            // идент. запроса
            array('name' => 'log_query_id', 'type' => 'string'),
            // параметры запроса
            array('name' => 'log_query_params', 'type' => 'string'),
            // дата события
            array('name' => 'log_date', 'type' => 'string'),
            array('name' => 'log_time', 'type' => 'string'),
            // ошибка
            array('name' => 'log_error', 'type' => 'string'),
            // ник пользователя
            array('name' => 'user_name', 'type' => 'string'),
            // имя пользователя
            array('name' => 'profile_id', 'type' => 'integer'),
            array('name' => 'profile_name', 'type' => 'string'),
            // фото пользователя
            array('name' => 'photo', 'type' => 'string')
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $settings = $this->session->get('user/settings');
        $this->columns = array(
            array('xtype'     => 'expandercolumn',
                  'url'       => $this->componentUrl . 'grid/expand/',
                  'typeCt'    => 'row'),
            array('xtype'     => 'numbercolumn'),
            array('xtype'     => 'rowmenu'),
            array('xtype'     => 'datetimecolumn',
                  'dataIndex' => 'log_date',
                  'timeIndex' => 'log_time',
                  'frmDate'   => $settings['format/date'],
                  'frmTime'   => $settings['format/time'],
                  'header'    => '<em class="mn-grid-hd-details"></em>' . $this->_['header_log_date'],
                  'width'     => 115,
                  'sortable'  => true),
            array('dataIndex' => 'user_name',
                  'header'    => $this->_['header_user_name'],
                  'width'     => 130,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'profile_name',
                  'header'    => $this->_['header_profile_name'],
                  'width'     => 170,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'controller_name',
                  'header'    => $this->_['header_controller_name'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'controller_class',
                  'header'    => $this->_['header_controller_class'],
                  'width'     => 150,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'log_url',
                  'header'    => $this->_['header_log_uri'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'log_action',
                  'header'    => $this->_['header_log_action'],
                  'width'     => 80,
                  'sortable'  => true),
            array('dataIndex' => 'log_query_id',
                  'header'    => $this->_['header_log_query_id'],
                  'width'     => 50,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'log_query',
                  'header'    => $this->_['header_log_query'],
                  'width'     => 170,
                  'sortable'  => true,
                  'hidden'    => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'log_query_params',
                  'header'    => $this->_['header_log_query_params'],
                  'width'     => 170,
                  'sortable'  => true,
                  'hidden'    => true,
                  'filter'    => array('type' => 'string', 'disabled' => false)),
            array('dataIndex' => 'log_error',
                  'header'    => $this->_['header_log_error'],
                  'width'     => 170,
                  'sortable'  => true,
                  'hidden'    => true,
                  'filter'    => array('type' => 'string', 'disabled' => false))
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_grid'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_grid'],
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false,
                  'profileUrl'    => $this->componentUrl . 'profile/')
        );

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "правка" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' =>$this->classId));
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'rowspan' => 3, 'cls' => 'mn-menu-separator'));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array(
                'xtype'      => 'mn-btn-clear-data',
                'msgConfirm' => $this->_['msg_btn_clear'],
                'gridId'     => $this->classId,
                'isN'        => true
            )
        );
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = 'component-journal-log';
        $this->_cmp->tbar->items->add($group);

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $img = '<div class="icon-photo-s" style="background-image: url(\'{photo}\')"></div>';
        $cellInfo =
            '<div class="mn-grid-cell-tooltip-tl">{log_date}</div>'
          . '<table class="mn-grid-cell-tooltip" cellpadding="0" cellspacing="5" border="0">'
          . '<tr><td valign="top">' . $img . '</td><td valign="top" style="width:400px">'
          . '<em>' . $this->_['header_profile_name'] . '</em>: <b>{profile_name}</b><br>'
          . '<em>' . $this->_['header_user_name'] . '</em>: <b>{user_name}</b><br>'
          . '<em>' . $this->_['header_controller_class'] . '</em>: <b>{controller_class}</b><br>'
          . '<em>' . $this->_['header_controller_name'] . '</em>: <b>{controller_name}</b><br>'
          . '<em>' . $this->_['header_log_action'] . '</em>: <b>{log_action}</b><br>'
          . '<em>' . $this->_['header_log_uri'] . '</em>: <b>{log_url}</b>'
          . '</td></tr></table>';
        $this->_cmp->cellTips = array(
            array('field' => 'log_date', 'tpl' => $cellInfo),
            array('field' => 'profile_name', 'tpl' => '{profile_name}'),
            array('field' => 'controller_class', 'tpl' => '{controller_class}'),
            array('field' => 'log_query', 'tpl' => '{log_query}'),
            array('field' => 'log_query_params', 'tpl' => '{log_query_params}'),
            array('field' => 'log_url', 'tpl' => '{log_url}'),
            array('field' => 'log_error', 'tpl' => '{log_error}')
        );
        $this->_cmp->cellTipConfig = array('maxWidth' => 400);

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $this->_cmp->rowMenu = array(
            'xtype' => 'mn-grid-rowmenu',
            'items' => array(
                array('text' => $this->_['rowmenu_info'],
                      'icon' => $this->resourcePath . 'icon-item-info.png',
                      'url'  => $this->componentUrl . 'info/interface/'),
                array('xtype'   => 'menuseparator'),
                array('text'   => $this->_['rowmenu_user'],
                      'field'  => 'profile_id',
                      'icon'   => $this->resourcePath . 'icon-item-info.png',
                      'params' => 'state=info',
                      'url'    => 'administration/users/contingent/' . ROUTER_DELIMITER . 'profile/interface/')
            )
        );

        // быстрый фильтр списка (ExtJs class "Manager.tree.GridFilter")
        $this->_slidePanel->width = 300;
        $this->_slidePanel->initRoot = array(
            'text'     => 'Filter',
            'id'       => 'byRoot',
            'expanded' => true,
            'children' => array(
                array('text'     => $this->_['text_all_records'],
                      'value'    => 'all',
                      'leaf'     => false,
                      'expanded' => true,
                      'children' => array(
                            array('text'     => $this->_['text_by_date'],
                                  'id'       => 'byDt',
                                  'leaf'     => false,
                                  'expanded' => true,
                                  'children' => array(
                                      array('text'     => $this->_['text_by_day'],
                                            'iconCls'  => 'icon-folder-find',
                                            'leaf'     => true,
                                            'value'    => 'day'),
                                      array('text'     => $this->_['text_by_week'],
                                            'iconCls'  => 'icon-folder-find',
                                            'leaf'     => true, 
                                            'value'    => 'week'),
                                      array('text'     => $this->_['text_by_month'],
                                            'iconCls'  => 'icon-folder-find',
                                            'leaf'     => true,
                                            'value'    => 'month')
                                  )
                            ),
                            array('text'    => $this->_['text_by_component'],
                                  'id'      => 'byCp',
                                  'leaf'    => false),
                            array('text'    => $this->_['text_by_action'],
                                  'id'      => 'byAc',
                                  'leaf'    => false),
                            array('text'    => $this->_['text_by_user'],
                                  'id'      => 'byUs',
                                  'leaf'    => false)
                    )
                )
            )
        );

        parent::getInterface();
    }
}
?>