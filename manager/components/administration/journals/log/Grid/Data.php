<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка действий пользователей"
 * Пакет контроллеров "Список действий пользователей"
 * Группа пакетов     "Журнал действий пользователей"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalLog_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные списка действий пользователей
 * 
 * @category   Gear
 * @package    GController_YJournalLog_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalLog_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'log_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_log';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // действия пользователей
        $this->la = GText::get('log actions', 'interface');
        // язык оболочки
        $languageId = $this->session->get('language/default/id');

        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS `p`.`profile_photo`, `p`.`profile_name`, `p`.`profile_id`, `l`.*, `u`.`user_name`, `s`.`controller_name` '
          . 'FROM `gear_user_profiles` `p`, `gear_users` `u`, `gear_log` `l` '
            // join "gear_controllers"
          . 'LEFT JOIN (SELECT `cl`.`controller_name`, `c`.* '
          . 'FROM `gear_controllers` `c`, `gear_controllers_l` `cl` WHERE `cl`.`controller_id`=`c`.`controller_id` AND '
          . '`cl`.`language_id`=' . $languageId . ') `s` ON `s`.`controller_id`=`l`.`controller_id` '
          . 'WHERE `p`.`user_id`=`l`.`user_id` AND `u`.`user_id`=`l`.`user_id` '
          . 'HAVING 1 %filter %fastfilter '
          . 'ORDER BY %sort '
          . 'LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            // класс контроллера
            'controller_class' => array('type' => 'string'),
            // название контроллера
            'controller_name' => array('type' => 'string'),
            // ресурс
            'log_url' => array('type' => 'string'),
            // имя действия
            'log_action' => array('type' => 'string'),
            // запрос
            'log_query' => array('type' => 'string'),
            // идент. запроса
            'log_query_id' => array('type' => 'string'),
            // параметры запроса
            'log_query_params' => array('type' => 'string'),
            // дата события
            'log_date' => array('type' => 'string'),
            'log_time' => array('type' => 'string'),
            // ошибка
            'log_error' => array('type' => 'string'),
            // ник пользователя
            'user_name' => array('type' => 'string'),
            'user_id' => array('type' => 'integer'),
            // имя пользователя
            'profile_id' => array('type' => 'integer'),
            'profile_name' => array('type' => 'string'),
            // фото пользователя
            'photo' => array('type' => 'string')
        );
    }

    /**
     * Возращает sql запрос для быстрого фильтра
     * (для подстановки "%fastfilter" в $sql)
     * 
     * @param string $key идендификатор поля
     * @param string $value значение
     * @return string
     */
    protected function getTreeFilter($key, $value)
    {
        // идендификатор поля
        switch ($key) {
            // по дате
            case 'byDt':
                $toDate = date('Y-m-d');
                switch ($value) {
                    case 'day':
                        return ' AND `log_date` BETWEEN "' . $toDate . '" AND "' . $toDate . '"';

                    case 'week':
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d") - 7, date("Y")));
                        return ' AND `log_date` BETWEEN "' . $fromDate . '" AND "' . $toDate . '"';

                    case 'month':
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, date("m")-1, date("d"), date("Y")));
                        return ' AND `log_date` BETWEEN "' . $fromDate . '" AND "' . $toDate . '"';
                }
                break;

            // по компоненту
            case 'byCp':
                return ' AND `controller_id`="' . $value . '" ';

            // по действию
            case 'byAc':
                return ' AND `log_action`="' . $value . '" ';

            // по пользователю
            case 'byUs':
                return ' AND `user_id`="' . $value . '" ';
        }

        return '';
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        // удаление записей таблицы "gear_log"
        $table = new GDb_Table('gear_log', 'log_id');
        if ($table->clear(true) !== true)
            throw new GSqlException();
    }

    /**
     * Добавление в журнал действий пользователей
     * (удаление данных)
     * 
     * @param array $params параметры журнала
     * @return void
     */
    protected function logDelete($params = array())
    {}

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        // если пользователь существует
        $user = GUsers::get($record['user_id']);
        if ($user) {
            $record['photo'] = $user['photo'];
            $record['profile_name'] = $user['profile_name'];
        }
        if (isset($this->la[$record['log_action']]))
            $record['log_action'] = $this->la[$record['log_action']];

        return $record;
    }
}
?>