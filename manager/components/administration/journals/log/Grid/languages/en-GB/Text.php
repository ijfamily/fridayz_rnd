<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Действия пользователей',
    'tooltip_grid' => 'журнал действий пользователей в системе',
    'rowmenu_info' => 'Информация о записи',
    'rowmenu_user' => 'Информация о пользователе',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">("Действия пользователей")</span> ?',
    // столбцы
    'header_log_date'         => 'Дата',
    'header_log_error'        => 'Ошибки',
    'header_log_query_params' => 'Параметры',
    'header_profile_name'     => 'Пользователь',
    'header_user_name'        => 'Логин',
    'header_log_query_id'     => 'ID',
    'header_log_query'        => 'Запрос',
    'header_log_action'       => 'Действие',
    'header_log_uri'          => 'Адрес',
    'header_controller_name'  => 'Компонент',
    'header_controller_class' => 'Класс',
    // развернутая запись
    'title_tab_common'        => 'Основные параметры', 
    'label_log_date'          => 'Дата',
    'label_log_error'         => 'Ошибки',
    'label_log_query_params'  => 'Параметры',
    'label_profile_name'      => 'Пользователь',
    'label_log_query_id'      => 'ID',
    'label_log_query'         => 'Запрос',
    'label_log_action'        => 'Действие',
    'label_log_uri'           => 'Адрес',
    'label_controller_name'   => 'Компонент',
    'label_controller_class'  => 'Класс',
    'label_profile_nick'      => 'Ник',
    'label_contact_email'     => 'E-mail',
    'title_tab_params_json'   => 'Параметры (JSON)',
    'title_tab_params_php'    => 'Параметры (PHP)',
    'title_tab_query'         => 'Запрос',
    'title_tab_error'         => 'Ошибки',
    // быстрый фильтр
    'text_all_records'  => 'Все записи',
    'text_by_date'      => 'По дате',
    'text_by_day'       => 'За день',
    'text_by_week'      => 'За неделю',
    'text_by_month'     => 'За месяц',
    'text_by_component' => 'По компоненту',
    'text_by_action'    => 'По действию',
    'text_by_user'      => 'По пользователю'
);
?>