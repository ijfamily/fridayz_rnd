<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Журнал действий пользователей"
 * Группа пакетов     "Журнал действий пользователей"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalLog
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Действия пользователей систем
 * Описание: Журнал действий пользователей системы
 * ID класса: gcontroller_yjournallog_grid
 * Группа: Администрирование / Журнал событий
 * Очищать: нет
 * Ресурс
 *    Пакет: /administration/journals/log
 *    Контроллер: /administration/journals/log/Grid
 *    Интерфейс: /grid/interface
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске: нет
 */

return array(
    // {Y}- модуль "System" {Journal} - пакет контроллеров "JournalLog" -> YJournalLog
    'clsPrefix' => 'YJournalLog'
);
?>