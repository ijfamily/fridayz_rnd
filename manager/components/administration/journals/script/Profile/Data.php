<?php
/**
 * Gear Manager
 *
 * Контроллеров       "Данные профиля ошибки скрипта"
 * Пакет контроллеров "Ошибки скрипта"
 * Группа пакетов     "Журналы"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalScript_Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные профиля ошибки скрипта
 * 
 * @category   Gear
 * @package    GController_YJournalScript_Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalScript_Profile_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array('user_id', 'log_file', 'log_level', 'log_line', 'log_message', 'log_date', 'log_time');

    /**
     * Первичное поле таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'log_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_log_script';

    /**
     * Добавление в журнал действий пользователей
     * (удаление данных)
     * 
     * @param array $params параметры журнала
     * @return void
     */
    protected function logDelete($params = array())
    {}

    /**
     * Доступ на добавление данных
     * 
     * @return void
     */
    protected function dataAccessInsert()
    {
        GFactory::getDb()->connect();
        $this->response->set('action', 'insert');
        $this->response->setMsgResult('Adding data', 'Is successful append' , true);
    }

    /**
     * Добавление в журнал действий пользователей
     * (вставка данных)
     * 
     * @param array $params параметры журнала
     * @return void
     */
    protected function logInsert($params = array())
    {}

    /**
     * Добавление в журнал действий пользователей
     * (обновление данных)
     * 
     * @param array $params параметры журнала
     * @return void
     */
    protected function logUpdate($params = array())
    {}

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {
        $params['user_id'] = $this->session->get('user_id');
        $params['log_file'] = $this->input->get('file', null);
        $params['log_level'] = $this->input->get('level', null);
        $params['log_line'] = $this->input->get('line', null);
        $params['log_message'] = $this->input->get('msg', null);
        $params['log_date'] = date('Y-m-d');
        $params['log_time'] = date('H:i:s');
    }
}
?>