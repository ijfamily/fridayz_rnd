<?php
/**
 * Gear Manager
 *
 * Контроллеров       "Данные ошибок скрипта"
 * Пакет контроллеров "Ошибки скрипта"
 * Группа пакетов     "Журналы"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalScript_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные ошибок скрипта
 * 
 * @category   Gear
 * @package    GController_YJournalScript_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalScript_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'log_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_log_script';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS `g`.`group_name`, `p`.`profile_photo`, `p`.`profile_name`, `p`.`profile_id`, `l`.*, `u`.`user_name` '
          . 'FROM `gear_log_script` `l` '
          . 'JOIN `gear_users` `u` USING(`user_id`) '
          . 'JOIN `gear_user_groups` `g` USING(`group_id`) '
          . 'JOIN `gear_user_profiles` `p` USING(`user_id`) '
          . 'WHERE 1 %filter %fastfilter ORDER BY %sort LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            'log_id' => array('type' => 'integer'),
            'log_date' => array('type' => 'string'),
            'log_time' => array('type' => 'string'),
            'log_file' => array('type' => 'string'),
            'log_level' =>  array('type' => 'string'),
            'log_line' =>  array('type' => 'integer'),
            'log_message' =>  array('type' => 'string'),
            'profile_id' =>  array('type' => 'integer'),
            'profile_name' =>  array('type' => 'string'),
            'photo' =>  array('type' => 'string'),
            'user_name' =>  array('type' => 'string'),
            'user_id' =>  array('type' => 'integer'),
            'group_name' =>  array('type' => 'string')
        );
    }

    /**
     * Возращает sql запрос для быстрого фильтра
     * (для подстановки "%fastfilter" в $sql)
     * 
     * @param string $key идендификатор поля
     * @param string $value значение
     * @return string
     */
    protected function getTreeFilter($key, $value)
    {
        // идендификатор поля
        switch ($key) {
            // по дате
            case 'byDt':
                $toDate = date('Y-m-d');
                switch ($value) {
                    case 'day':
                        return ' AND `l`.`log_date` BETWEEN "' . $toDate . '" AND "' . $toDate . '"';

                    case 'week':
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d") - 7, date("Y")));
                        return ' AND `l`.`log_date` BETWEEN "' . $fromDate . '" AND "' . $toDate . '"';

                    case 'month':
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, date("m")-1, date("d"), date("Y")));
                        return ' AND `l`.`log_date` BETWEEN "' . $fromDate . '" AND "' . $toDate . '"';
                }
                break;

            // по пользователю
            case 'byUs':
                return ' AND `l`.`user_id`="' . $value . '" ';

            // последняя ошибка
            case 'byLa':
                return ' AND `l`.`log_id`=' . (int) $value;
        }

        return '';
    }

    /**
     * Добавление в журнал действий пользователей
     * (удаление данных)
     * 
     * @param array $params параметры журнала
     * @return void
     */
    protected function logDelete($params = array())
    {}

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        // удаление записей таблицы "gear_log_script"
        $table = new GDb_Table('gear_log_script', 'log_id');
        if ($table->clear(true) !== true)
            throw new GSqlException();
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON записи
     * 
     * @params array $record запись из базы данных
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        // если пользователь существует
        $user = GUsers::get($record['user_id']);
        if ($user) {
            $record['photo'] = $user['photo'];
            $record['profile_name'] = $user['profile_name'];
        }

        return $record;
    }
}
?>