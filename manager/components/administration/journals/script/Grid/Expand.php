<?php
/**
 * Gear Manager
 *
 * Контроллер         "Развёрнутая запись ошибки скрипта"
 * Пакет контроллеров "Ошибки скрипта"
 * Группа пакетов     "Журналы"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalScript_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Expand');

/**
 * Развёрнутая запись ошибки скрипта
 * 
 * @category   Gear
 * @package    GController_YJournalScript_Grid
 * @subpackage Expand
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalScript_Grid_Expand extends GController_Grid_Expand
{
    /**
     * Вывод данных в интерфейс
     * 
     * @return void
     */
    protected function dataExpand()
    {
        parent::dataAccessView();

        $data = $this->getAttributes();
        $this->response->data = $data;
    }

    /**
     * Возращает атрибуты записи
     * 
     * @return string
     */
    protected function getAttributes()
    {
        $data = '';
        $settings = $this->session->get('user/settings');
        // контингент
        $query = new GDb_Query();
        $sql =
            'SELECT `u`.*, `g`.*, `p`.*, `a`.* '
          . 'FROM `gear_log_script` `a` '
          . 'JOIN `gear_users` `u` USING(`user_id`) '
          . 'JOIN `gear_user_groups` `g` USING(`group_id`) '
          . 'JOIN `gear_user_profiles` `p` USING(`user_id`) '
          . 'WHERE `a`.`log_id`=' . $this->uri->id;
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        if (empty($rec))
            return '<div class="mn-row-body border">' . $this->_['msg_empty_profile'] . '<div class="wrap"></div>';
        // основные параметры
        $data .= '<fieldset class="fixed" style="width:100%"><label>' . $this->_['title_fieldset_common'] . '</label><ul>';
        $data .= '<li><label>' . $this->_['label_log_date'] . ':</label> ' . date($settings['format/datetime'], strtotime($rec['log_date'] .  ' ' . $rec['log_time'])) . '</li>';
        $data .= '<li><label>' . $this->_['label_log_file'] . ':</label> ' . $rec['log_file'] . '</li>';
        $data .= '<li><label>' . $this->_['label_log_level'] . ':</label> ' . $rec['log_level'] . '</li>';
        $data .= '<li><label>' . $this->_['label_log_line'] . ':</label> ' . $rec['log_line'] . '</li>';
        $data .= '<li><label>' . $this->_['label_log_message'] . ':</label> ' . $rec['log_message'] . '</li>';
        $data .= '<li><label>' . $this->_['label_user_name'] . ':</label> ' . $rec['user_name'] . '</li>';
        $data .= '<li><label>' . $this->_['label_group_name'] . ':</label> ' . $rec['group_name'] . '</li>';
        $data .= '</ul></fieldset>';
        $data .= '<div class="wrap"></div>';
        // если есть пользователь
        if (!empty($rec['profile_id'])) {
            // формирование атрибутов
            if (empty($rec['profile_born']))
                $born = '';
            else
                $born = date($settings['format/date'], strtotime($rec['profile_born']));
            $data .= '<fieldset class="fixed"><label>' . $this->_['title_fieldset_user'] . '</label><ul>';
            $data .= '<li><label>' . $this->_['label_profile_name'] . ':</label> ' . $rec['profile_name'] . '</li>';
            $data .= '<li><label>' . $this->_['label_profile_born'] . ':</label> ' . $born . '</li>';
            $data .= '<li><label>' . $this->_['label_profile_gender'] . ':</label> ' . $this->_['data_gender'][$rec['profile_gender']] . '</li>';
            $data .= '</ul></fieldset>';
            $data .= '<div class="wrap"></div>';
            // Средства связи
            $data .= '<fieldset class="fixed" style="width:260px"><label>' . $this->_['title_fieldset_connection'] . '</label><ul>';
            $data .= '<li><label>' . $this->_['label_contact_work_phone'] . ':</label> ' . $rec['contact_work_phone'] . '</li>';
            $data .= '<li><label>' . $this->_['label_contact_mobile_phone'] . ':</label> ' . $rec['contact_mobile_phone'] . '</li>';
            $data .= '<li><label>' . $this->_['label_contact_home_phone'] . ':</label> ' . $rec['contact_home_phone'] . '</li>';
            $data .= '<li><label>E-mail:</label> ' . $rec['contact_email'] . '</li>';
            $data .= '<li><label>ICQ:</label> ' . $rec['contact_icq'] . '</li>';
            $data .= '<li><label>Skype:</label> ' . $rec['contact_skype'] . '</li>';
            $data .= '</ul></fieldset>';
            // Социальные сети
            $data .= '<fieldset class="fixed" style="width:260px"><label>' . $this->_['title_fieldset_net'] . '</label><ul>';
            $data .= '<li><label>Facebook:</label> ' . $rec['contact_facebook'] . '</li>';
            $data .= '<li><label>LinkedIn:</label> ' . $rec['contact_linkedin'] . '</li>';
            $data .= '<li><label>Twitter:</label> ' . $rec['contact_twitter'] . '</li>';
            $data .= '<li><label>VK:</label> ' . $rec['contact_vk'] . '</li>';
            $data .= '</ul></fieldset>';
            // Адрес
            $data .= '<fieldset class="fixed" style="width:260px"><label>' . $this->_['title_fieldset_address'] . '</label><ul>';
            $data .= '<li><label>' . $this->_['label_address_index'] . ':</label> ' . $rec['profile_address_index'] . '</li>';
            $data .= '<li><label>' . $this->_['label_address_country'] . ':</label> ' . $rec['profile_address_country'] . '</li>';
            $data .= '<li><label>' . $this->_['label_address_region'] . ':</label> ' . $rec['profile_address_region'] . '</li>';
            $data .= '<li><label>' . $this->_['label_address_city'] . ':</label> ' . $rec['profile_address_city'] . '</li>';
            $data .= '<li><label>' . $this->_['label_address'] . ':</label> ' . $rec['profile_address'] . '</li>';
            $data .= '</ul></fieldset>';
            $data .= '<div class="wrap"></div>';
        }

        return '<div class="mn-row-body border">' . $data . '<div class="wrap"></div>';
    }
}
?>