<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск в списке ошибок скриптов"
 * Пакет контроллеров "Ошибки скриптов"
 * Группа пакетов     "Журналы"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalScript_Search
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск в списке ошибок скриптов
 * 
 * @category   Gear
 * @package    GController_YJournalScript_Search
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalScript_Search_Data extends GController_Search_Data
{
    /**
     * Идентификатор DOM компонента (списка)
     *
     * @var string
     */
    public $gridId = 'gcontroller_yjournalscript_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_yjournalscript_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор,
     * используется для инициализации атрибутов контроллера без конструктора
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'log_date', 'label' => $this->_['header_log_date']),
            array('dataIndex' => 'log_file', 'label' => $this->_['header_log_file']),
            array('dataIndex' => 'log_level', 'label' => $this->_['header_log_level']),
            array('dataIndex' => 'log_line', 'label' => $this->_['header_log_line']),
            array('dataIndex' => 'log_message', 'label' => $this->_['header_log_message']),
            array('dataIndex' => 'user_name', 'label' => $this->_['header_user_name']),
            array('dataIndex' => 'group_name', 'label' => $this->_['header_group_name']),
            array('dataIndex' => 'profile_name', 'label' => $this->_['header_profile_name'])
        );
    }
}
?>