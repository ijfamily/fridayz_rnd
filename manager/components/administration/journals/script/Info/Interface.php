<?php
/**
 * Gear Manager
 *
 * Контроллеров       "Интерфейс профиля ошибки скрипта"
 * Пакет контроллеров "Ошибки скриптов"
 * Группа пакетов     "Журналы"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalScript_Info
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля ошибки скрипта
 * 
 * @category   Gear
 * @package    GController_YJournalScript_Info
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
 
final class GController_YJournalScript_Info_Interface extends GController_Profile_Interface
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_yjournalscript_grid';


    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('titleEllipsis' => 45,
                  'gridId'        => 'gcontroller_yjournalscript_grid',
                  'width'         => 470,
                  'autoHeight'    => true,
                  'resizable'     => false,
                  'state'         => 'info',
                  'stateful'      => false)
        );

        // поля формы (Ext.form.FormPanel)
        $items = array(
           array('xtype'      => 'displayfield',
                 'fieldLabel' => $this->_['label_log_id'],
                 'fieldClass' => 'mn-field-value-info',
                 'name'       => 'log_id'),
           array('xtype'      => 'displayfield',
                 'fieldLabel' => $this->_['label_log_file'],
                 'fieldClass' => 'mn-field-value-info',
                 'name'       => 'log_file'),
           array('xtype'      => 'displayfield',
                 'fieldLabel' => $this->_['label_log_level'],
                 'fieldClass' => 'mn-field-value-info',
                 'name'       => 'log_level'),
           array('xtype'      => 'displayfield',
                 'fieldLabel' => $this->_['label_log_line'],
                 'name'       => 'log_line'),
           array('xtype'      => 'textarea',
                 'fieldLabel' => $this->_['label_log_message'],
                 'name'       => 'log_message',
                 'anchor'     => '100%',
                 'height'     => 100)
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->addItems($items);
        $form->labelWidth = 80;
        $form->url = $this->componentUrl . 'info/';

        parent::getInterface();
    }
}
?>