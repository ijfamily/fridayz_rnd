<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные списка выхода пользователей"
 * Пакет контроллеров "Выход пользователей"
 * Группа пакетов     "Журналы"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalEscapes_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Data');

/**
 * Данные списка выхода пользователей
 * 
 * @category   Gear
 * @package    GController_YJournalEscapes_Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalEscapes_Grid_Data extends GController_Grid_Data
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'escape_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_user_escapes';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // SQL запрос
        $this->sql = 
            'SELECT SQL_CALC_FOUND_ROWS `u`.`user_name`, `up`.*, `ug`.`group_name`, `a`.* '
          . 'FROM `gear_user_escapes` `a` '
          . 'LEFT JOIN `gear_users` `u` USING(`user_id`) '
          . 'LEFT JOIN `gear_user_profiles` `up` USING(`user_id`) '
          . 'LEFT JOIN `gear_user_groups` `ug` USING(`group_id`) '
          . 'WHERE 1 %filter '
          . 'ORDER BY %sort '
          . 'LIMIT %limit';

        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            'escape_date' => array('type' => 'string'),
            'escape_time' => array('type' => 'string'),
            'profile_name' => array('type' => 'string'),
            'profile_id' => array('type' => 'integer'),
            'photo' => array('type' => 'string'),
            'user_name' => array('type' => 'string'),
            'user_id' => array('type' => 'integer'),
            'group_name' => array('type' => 'string')
        );
    }

    /**
     * Добавление в журнал действий пользователей
     * (удаление данных)
     * 
     * @param array $params параметры журнала
     * @return void
     */
    protected function logDelete($params = array())
    {}

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataClear();

        // удаление записей таблицы "gear_user_escapes"
        $table = new GDb_Table('gear_user_escapes', 'escape_id');
        if ($table->clear(true) !== true)
            throw new GSqlException();
    }

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        // если пользователь существует
        $user = GUsers::get($record['user_id']);
        if ($user) {
            $record['photo'] = $user['photo'];
            $record['profile_name'] = $user['profile_name'];
        }

        return $record;
    }
}
?>