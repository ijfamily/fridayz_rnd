<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Виход користувачів із системи',
    'tooltip_grid' => 'список выхода пользователей из системы',
    'rowmenu_info' => 'Деталі запису',
    'rowmenu_user' => 'Iнформацiя о користувачі',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Стовпці',
    'title_buttongroup_filter' => 'Фільтр',
    'msg_btn_clear'            => 'Ви дійсно бажаєте видалити всі записи <span class="mn-msg-delete">"Виход користувачів із системи"</span> ?',
    // столбцы
    'header_escape_date'  => 'Дата',
    'header_user_name'    => 'Логін',
    'header_group_name'   => 'Група',
    'header_profile_name' => 'Iм\'я',
    // типы
    'data_gender' => array('женский', 'мужской'),
    // развернутая запись
    'title_fieldset_common'      => 'Основные параметры',
    'label_escape_date'          => 'Дата выхода',
    'label_user_name'          => 'Логин',
    'title_fieldset_user'        => 'Пользователь',
    'label_profile_name'         => 'Фамилия Имя Отчество',
    'label_profile_gender'       => 'Пол',
    'label_profile_born'         => 'Дата рождения',
    'title_fieldset_connection'  => 'Средства связи',
    'label_contact_work_phone'   => 'Рабочий телефон',
    'label_contact_mobile_phone' => 'Мобильный телефон',
    'label_contact_home_phone'   => 'Домашний телефон',
    'title_fieldset_net'         => 'Социальные сети',
    'title_fieldset_address'     => 'Адрес',
    'label_address'              => 'Адрес',
    'label_address_index'        => 'Индекс',
    'label_address_country'      => 'Страна',
    'label_address_region'       => 'Область / штат',
    'label_address_city'         => 'Город',
    'msg_empty_profile'          => 'нет данных для отображения'
);
?>