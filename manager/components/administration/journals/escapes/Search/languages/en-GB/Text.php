<?php
/**
 * Gear
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Distributed under the Lesser General Public License (LGPL)
 * http://www.gnu.org/copyleft/lesser.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2011-2014 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Text.php 2014-08-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'search_title' => 'Search in the "User logoff"',
    // поля
    'header_escape_date'  => 'Date',
    'header_user_name'    => 'Login',
    'header_group_name'   => 'Group',
    'header_profile_name' => 'Name'
);
?>