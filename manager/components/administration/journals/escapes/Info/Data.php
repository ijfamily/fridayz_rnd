<?php
/**
 * Gear Manager
 *
 * Контроллер         "Данные профиля выхода пользователей"
 * Пакет контроллеров "Выход пользователей"
 * Группа пакетов     "Журналы"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalEscapes_Info
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Data');

/**
 * Данные профиля выхода пользователей
 * 
 * @category   Gear
 * @package    GController_YJournalEscapes_Info
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalEscapes_Info_Data extends GController_Profile_Data
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    public $fields = array('escape_date', 'escape_time', 'profile_name', 'user_name');

    /**
     * Первичное поле таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'escape_id';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_user_escapes';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_yjournalescapes_grid';

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record array fields
     * @return string
     */
    protected function getTitle($record)
    {
        $settings = $this->session->get('user/settings');

        return sprintf($this->_['title_info'], date($settings['format/datetime'], strtotime($record['escape_date'] . ' ' . $record['escape_time'])));
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        $sql = 'SELECT `a`.*, `u`.*, `e`.`profile_name` '
             . 'FROM `gear_user_escapes` AS `a` '
             . 'LEFT JOIN `gear_user_profiles` AS `e` USING (`user_id`) '
             . 'LEFT JOIN `gear_users` AS `u` USING (`user_id`) '
             . 'WHERE `a`.`escape_id`=%id%';

        parent::dataView($sql);
    }

    /**
     * Добавление в журнал действий пользователей
     * (удаление данных)
     * 
     * @param array $params параметры журнала
     * @return void
     */
    protected function logDelete($params = array())
    {}

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        $settings = $this->session->get('user/settings');

        $record['escape_date'] = date($settings['format/datetime'], strtotime($record['escape_date'] . ' ' . $record['escape_time']));

        return $record;
    }
}
?>