<?php
/**
 * Gear Manager
 *
 * Контроллер         "Интерфейс профиля выхода пользователей"
 * Пакет контроллеров "Выход пользователей"
 * Группа пакетов     "Журналы"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalEscapes_Info
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Profile/Interface');

/**
 * Интерфейс профиля выхода пользователей
 * 
 * @category   Gear
 * @package    GController_YJournalEscapes_Info
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalEscapes_Info_Interface extends GController_Profile_Interface
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_yjournalescapes_grid';

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp->setProps(
            array('titleEllipsis' => 45,
                  'gridId'        => 'gcontroller_yjournalescapes_grid',
                  'width'         => 340,
                  'autoHeight'    => true,
                  'resizable'     => false,
                  'state'         => 'info',
                  'stateful'      => false)
        );

        // поля формы (Ext.form.FormPanel)
        $items = array(
           array('xtype'      => 'displayfield',
                 'fieldLabel' => $this->_['label_escape_date'],
                 'fieldClass' => 'mn-field-value-info',
                 'name'       => 'escape_date'),
           array('xtype'      => 'displayfield',
                 'fieldLabel' => $this->_['label_profile_name'],
                 'fieldClass' => 'mn-field-value-info',
                 'name'       => 'profile_name'),
           array('xtype'      => 'displayfield',
                 'fieldLabel' => $this->_['label_user_name'],
                 'fieldClass' => 'mn-field-value-info',
                 'name'       => 'user_name')
        );

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->items->addItems($items);
        $form->labelWidth = 50;
        $form->url = $this->componentUrl . 'info/';

        parent::getInterface();
    }
}
?>