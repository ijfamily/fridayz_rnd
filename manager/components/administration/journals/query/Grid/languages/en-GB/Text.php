<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // список
    'title_grid'   => 'Ошибки запросов пользователей',
    'tooltip_grid' => 'журнал ошибок запросов к базе данных',
    'rowmenu_user' => 'Информация о пользователе',
    'rowmenu_info' => 'Информация о записи',
    // панель управления
    'title_buttongroup_edit'   => 'Правка',
    'title_buttongroup_cols'   => 'Столбцы',
    'title_buttongroup_filter' => 'Фильтр',
    'msg_btn_clear'            => 'Вы действительно желаете удалить все записи <span class="mn-msg-delete">"Ошибки запросов пользователей"</span> ?',
    // столбцы
    'header_log_id'         => 'Код',
    'header_log_date'       => 'Дата',
    'header_log_query'      => 'Запрос',
    'header_log_error_code' => 'Код ошибки',
    'header_log_error'      => 'Ошибка',
    'header_user_name'      => 'Логин',
    'header_group_name'     => 'Группа',
    'header_profile_name'   => 'Пользователь',
    // быстрый фильтр
    'text_all_records' => 'Все записи',
    'text_by_date'     => 'По дате',
    'text_by_day'      => 'За день',
    'text_by_week'     => 'За неделю',
    'text_by_month'    => 'За месяц',
    'text_by_code'     => 'По коду',
    'text_by_user'     => 'По пользователю',
    'text_by_last'     => 'Последняя ошибка',
    // типы
    'data_gender' => array('женский', 'мужской'),
    // развернутая запись
    'title_fieldset_common'      => 'Основные параметры',
    'label_log_id'               => 'Код',
    'label_log_date'             => 'Дата',
    'label_log_query'            => 'Запрос',
    'label_log_error_code'       => 'Код ошибки',
    'label_log_error'            => 'Ошибка',
    'label_user_name'            => 'Логин',
    'label_group_name'           => 'Группа пользователя',
    'title_fieldset_user'        => 'Пользователь',
    'label_profile_name'         => 'Фамилия Имя Отчество',
    'label_profile_gender'       => 'Пол',
    'label_profile_born'         => 'Дата рождения',
    'title_fieldset_connection'  => 'Средства связи',
    'label_contact_work_phone'   => 'Рабочий телефон',
    'label_contact_mobile_phone' => 'Мобильный телефон',
    'label_contact_home_phone'   => 'Домашний телефон',
    'title_fieldset_net'         => 'Социальные сети',
    'title_fieldset_address'     => 'Адрес',
    'label_address'              => 'Адрес',
    'label_address_index'        => 'Индекс',
    'label_address_country'      => 'Страна',
    'label_address_region'       => 'Область / штат',
    'label_address_city'         => 'Город',
    'msg_empty_profile'          => 'нет данных для отображения'
);
?>