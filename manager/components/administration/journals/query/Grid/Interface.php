<?php
/**
 * Gear Manager
 *
 * Контроллеров       "Интерфейс ошибок запросов пользователей"
 * Пакет контроллеров "Ошибки запросов пользователей"
 * Группа пакетов     "Журналы"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalQuery_Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Grid/Interface');

/**
 * Интерфейс ошибок запросов пользователей
 * 
 * @category   Gear
 * @package    GController_YJournalQuery_Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalQuery_Grid_Interface extends GController_Grid_Interface
{
    /**
     * Первичное поле
     * (указывается в настройках Ext.data.JsonStore.idProperty)
     *
     * @var string
     */
    public $idProperty = 'log_id';

    /**
     * Вид сортировки
     * (указывается в настройках Ext.data.JsonStore.sortInfo.dir)
     *
     * @var string
     */
    public $dir = 'DESC';

   /**
     * Сортируемое поле
     * (указывается в настройках Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = 'log_id';

    /**
     * Использовать быстрый фильтр
     *
     * @var boolean
     */
    public $useSlidePanel = true;

    /**
     * Инициализация интерфейса компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // поля списка (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('name' => 'log_id', 'type' => 'integer'),
            array('name' => 'log_date', 'type' => 'string'),
            array('name' => 'log_time', 'type' => 'string'),
            array('name' => 'log_query', 'type' => 'string'),
            array('name' => 'log_error', 'type' => 'string'),
            array('name' => 'log_error_code', 'type' => 'string'),
            array('name' => 'profile_id', 'type' => 'integer'),
            array('name' => 'profile_name', 'type' => 'string'),
            array('name' => 'photo', 'type' => 'string'),
            array('name' => 'user_id', 'type' => 'string'),
            array('name' => 'user_name', 'type' => 'string'),
            array('name' => 'group_name', 'type' => 'string')
        );

        // столбцы списка (ExtJS class "Ext.grid.ColumnModel")
        $settings = $this->session->get('user/settings');
        $this->columns = array(
            array('xtype'     => 'expandercolumn',
                  'url'       => $this->componentUrl . 'grid/expand/',
                  'typeCt'    => 'row'),
            array('xtype'     => 'numbercolumn'),
            array('xtype'     => 'rowmenu'),
            array('dataIndex' => 'log_id',
                  'header'    => '<em class="mn-grid-hd-details"></em>' . $this->_['header_log_id'],
                  'width'     => 67,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('xtype'     => 'datetimecolumn',
                  'dataIndex' => 'log_date',
                  'timeIndex' => 'log_time',
                  'frmDate'   => $settings['format/date'],
                  'frmTime'   => $settings['format/time'],
                  'header'    => $this->_['header_log_date'],
                  'width'     => 125,
                  'sortable'  => true),
            array('dataIndex' => 'log_query',
                  'header'    => $this->_['header_log_query'],
                  'width'     => 150,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'log_error_code',
                  'header'    => '<em class="mn-grid-hd-details"></em>' . $this->_['header_log_error_code'],
                  'width'     => 95,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'log_error',
                  'header'    =>$this->_['header_log_error'],
                  'width'     => 120,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'user_name',
                  'header'    => '<em class="mn-grid-hd-details"></em>' . $this->_['header_user_name'],
                  'width'     => 105,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'group_name',
                  'header'    => $this->_['header_group_name'],
                  'width'     => 140,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string')),
            array('dataIndex' => 'profile_name',
                  'header'    => $this->_['header_profile_name'],
                  'width'     => 150,
                  'sortable'  => true,
                  'filter'    => array('type' => 'string'))
        );

        // компонент "список" (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp->setProps(
            array('title'         => $this->_['title_grid'],
                  'iconSrc'       => $this->resourcePath . 'icon.png',
                  'iconTpl'       => $this->resourcePath . 'shortcut.png',
                  'titleTpl'      => $this->_['tooltip_grid'],
                  'titleEllipsis' => 40,
                  'isReadOnly'    => false)
        );

        // источник данных (ExtJS class "Ext.data.Store")
        $this->_cmp->store->url = $this->componentUrl . 'grid/';

        // панель инструментов списка (ExtJS class "Ext.Toolbar")
        // группа кнопок "правка" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup(array('title' => $this->_['title_buttongroup_edit']));
        // кнопка "удалить" (ExtJS class "Manager.button.DeleteData")
        $group->items->add(array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->classId));
        // кнопка "правка" (ExtJS class "Manager.button.EditData")
        $group->items->add(array('xtype' => 'mn-btn-edit-data', 'gridId' => $this->classId));
        // кнопка "выделить" (ExtJS class "Manager.button.SelectData")
        $group->items->add(array('xtype' => 'mn-btn-select-data', 'gridId' => $this->classId));
        // кнопка "обновить" (ExtJS class "Manager.button.RefreshData")
        $group->items->add(array('xtype' => 'mn-btn-refresh-data', 'gridId' =>$this->classId));
        // разделитель (ExtJS class "Ext.menu.Separator")
        $group->items->add(array('xtype' => 'menuseparator', 'cls' => 'mn-menu-separator'));
        // кнопка "очистить" (ExtJS class "Manager.button.ClearData")
        $group->items->add(
            array('xtype'      => 'mn-btn-clear-data',
                  'msgConfirm' => $this->_['msg_btn_clear'],
                  'gridId'     => $this->classId,
                  'isN'        => true)
        );
        $this->_cmp->tbar->items->add($group);

        // группа кнопок "столбцы" (ExtJS class "Ext.ButtonGroup")
        $group = new Ext_ButtonGroup_Columns(
            array('title'   => $this->_['title_buttongroup_cols'],
                  'gridId'  => $this->classId,
                  'columns' => 3)
        );
        $btn = &$group->items->get(1);
        $btn['url'] = $this->componentUrl . 'grid/columns/';
        // кнопка "поиск" (ExtJS class "Manager.button.Search")
        $group->items->addFirst(
            array('xtype'   => 'mn-btn-search-data',
                  'id'      =>  $this->classId . '-bnt_search',
                  'rowspan' => 3,
                  'url'     => $this->componentUrl . 'search/interface/',
                  'iconCls' => 'icon-btn-search' . ($this->isSetFilter() ? '-a' : ''))
        );
        // кнопка "справка" (ExtJS class "Manager.button.Help")
        $btn = &$group->items->get(1);
        $btn['fileName'] = 'component-journal-queries';
        $this->_cmp->tbar->items->add($group);

        // всплывающие подсказки для каждой записи (ExtJS property "Manager.grid.GridPanel.cellTips")
        $img = '<div class="icon-photo-s" style="background-image: url(\'{photo}\')"></div>';
        $cellInfo =
            '<div class="mn-grid-cell-tooltip-tl">{log_date} {log_time}</div>'
          . '<div class="mn-grid-cell-tooltip">'
          . '<em>' . $this->_['header_log_error_code'] . '</em>: <b>{log_error_code}</b><br>'
          . '<em>' . $this->_['header_log_error'] . '</em>: <b>{log_error}</b><br><br>'
          . '<div class="mn-grid-cell-tooltip-tl">{user_name}</div>'
          . '<table class="mn-grid-cell-tooltip" cellpadding="0" cellspacing="5" border="0">'
          . '<tr><td valign="top">' . $img . '</td><td valign="top">'
          . '<em>' . $this->_['header_group_name'] . '</em>: <b>{group_name}</b><br>'
          . '<em>' . $this->_['header_profile_name'] . '</em>: <b>{profile_name}</b><br>'
          . '<em>' . $this->_['header_user_name'] . '</em>: <b>{user_name}</b>'
          . '</td></tr></table></div>';
        $cellInfoU =
            '<div class="mn-grid-cell-tooltip-tl">{user_name}</div>'
          . '<div class="mn-grid-cell-tooltip">'
          . '<table class="mn-grid-cell-tooltip" cellpadding="0" cellspacing="5" border="0">'
          . '<tr><td valign="top">' . $img . '</td><td valign="top">'
          . '<em>' . $this->_['header_group_name'] . '</em>: <b>{group_name}</b><br>'
          . '<em>' . $this->_['header_profile_name'] . '</em>: <b>{profile_name}</b><br>'
          . '<em>' . $this->_['header_user_name'] . '</em>: <b>{user_name}</b>'
          . '</td></tr></table></div>';
        $cellInfoC =
            '<div class="mn-grid-cell-tooltip-tl">{log_error_code}</div>'
          . '<div class="mn-grid-cell-tooltip">'
          . '<em>' . $this->_['header_log_error'] . '</em>: <b>{log_error}</b><br>'
          . '<em>' . $this->_['header_log_query'] . '</em>: <b>{log_query}</b><br><br>'
          . '</div>';
        $this->_cmp->cellTips = array(
            array('field' => 'log_id', 'tpl' => $cellInfo),
            array('field' => 'log_query', 'tpl' => '{log_query}'),
            array('field' => 'log_error_code', 'tpl' => $cellInfoC),
            array('field' => 'log_error', 'tpl' => '{log_error}'),
            array('field' => 'user_name', 'tpl' => $cellInfoU),
            array('field' => 'log_group_name', 'tpl' => '{log_group_name}'),
            array('field' => 'profile_name', 'tpl' => '{profile_name}')
        );

        // всплывающие меню правки для каждой записи (ExtJS property "Manager.grid.GridPanel.rowMenu")
        $this->_cmp->rowMenu = array(
            'xtype' => 'mn-grid-rowmenu',
            'items' => array(
                array('text' => $this->_['rowmenu_info'],
                      'icon' => $this->resourcePath . 'icon-item-info.png',
                      'url'  => $this->componentUrl . 'info/interface/'),
                array('text'   => $this->_['rowmenu_user'],
                      'field'  => 'profile_id',
                      'icon'   => $this->resourcePath . 'icon-item-info.png',
                      'params' => 'state=info',
                      'url'    => 'administration/users/contingent/' . ROUTER_DELIMITER . 'profile/interface/')
            )
        );

        // быстрый фильтр списка (ExtJs class "Manager.tree.GridFilter")
        $this->_slidePanel->width = 300;
        $this->_slidePanel->initRoot = array(
            'text'     => 'Filter',
            'id'       => 'byRoot',
            'expanded' => true,
            'children' => array(
                array('text'     => $this->_['text_all_records'],
                      'value'    => 'all',
                      'leaf'     => false,
                      'expanded' => true,
                      'children' => array(
                            array('text'     => $this->_['text_by_date'],
                                  'id'       => 'byDt',
                                  'leaf'     => false,
                                  'expanded' => true,
                                  'children' => array(
                                      array('text'     => $this->_['text_by_day'],
                                            'iconCls'  => 'icon-folder-find',
                                            'leaf'     => true,
                                            'value'    => 'day'),
                                      array('text'     => $this->_['text_by_week'],
                                            'iconCls'  => 'icon-folder-find',
                                            'leaf'     => true, 
                                            'value'    => 'week'),
                                      array('text'     => $this->_['text_by_month'],
                                            'iconCls'  => 'icon-folder-find',
                                            'leaf'     => true,
                                            'value'    => 'month')
                                  )
                            ),
                            array('text'    => $this->_['text_by_last'],
                                  'id'      => 'byLa',
                                  'leaf'    => false),
                            array('text'    => $this->_['text_by_code'],
                                  'id'      => 'byCo',
                                  'leaf'    => false),
                            array('text'    => $this->_['text_by_user'],
                                  'id'      => 'byUs',
                                  'leaf'    => false)
                    )
                )
            )
        );

        parent::getInterface();
    }
}
?>