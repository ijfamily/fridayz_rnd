<?php
/**
 * Gear Manager
 *
 * Пакет контроллеров "Ошибки запросов пользователей"
 * Группа пакетов     "Журналы"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalQuery
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: settings.php 2016-01-01 12:00:00 Gear Magic $
 *
 * Установка компонента
 * 
 * Название: Ошибки запросов пользователей
 * Описание: Журнал ошибок запросов пользователей
 * ID класса: gcontroller_yjournalquery_grid
 * Группа: Администрирование / Журнал ошибок запросов пользователей
 * Очищать: нет
 * Ресурс
 *    Пакет: administration/journals/query/
 *    Контроллер: administration/journals/query/Grid/
 *    Интерфейс: grid/interface/
 * Интерфейс
 *    Доступный: да
 *    Видимый: да
 *    На доске: нет
 */

return array(
    // {Y}- модуль "System" {Journal} - пакет контроллеров "JournalQuery" -> YJournalQuery
    'clsPrefix' => 'YJournalQuery'
);
?>