<?php
/**
 * Gear Manager
 *
 * Контроллер         "Триггер дерева"
 * Пакет контроллеров "Триггер дерева"
 * Группа пакетов     "Журналы"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalQuery_Tree
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Tree/Trigger');

/**
 * Триггер дерева
 * 
 * @category   Gear
 * @package    GController_YJournalQuery_Tree
 * @subpackage Trigger
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalQuery_Tree_Trigger extends GController_Tree_Trigger
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_yjournalquery_grid';

    /**
     * Возращает последнюю запись
     *
     * @return array
     */
    protected function getLastRecord()
    {
        $query = new GDb_Query();
        $sql = 'SELECT *, MAX(`log_id`) FROM `gear_log_sql`';
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();
        if ($rec)
            return array(
                'text'    => date('d-m-Y', strtotime($rec['log_date'])) . ' ' . $rec['log_time'],
                'qtip'    => $rec['log_error'],
                'leaf'    => true,
                'iconCls' => 'icon-folder-find',
                'value'   => $rec['log_id']
            );

        return array();
    }

    /**
     * Возращает список кодов ошибок
     *
     * @return array
     */
    protected function getCodes()
    {
        // действия пользователей
        $data = array();
        $query = new GDb_Query();
        $sql = 'SELECT *, COUNT(*) `count` FROM `gear_log_sql` GROUP BY `log_error_code`';
        if ($query->execute($sql) !== true)
            throw new GSqlException();
        while (!$query->eof()) {
            $rec = $query->next();
            $data[] = array(
                'text'    => $rec['log_error_code'] . ' <b>(' . $rec['count'] . ')</b>',
                'qtip'    => $rec['log_error_code'],
                'leaf'    => true,
                'iconCls' => 'icon-folder-find',
                'value'   => $rec['log_error_code']
            );
        }

        return $data;
    }

    /**
     * Возращает список пользователей
     *
     * @return array
     */
    protected function getUsers()
    {
        $data = array();
        $query = new GDb_Query();
        $sql = 'SELECT `p`.`profile_name`, `p`.`user_id`, COUNT(*) `count` '
             . 'FROM `gear_log_sql` `l` '
             . 'JOIN `gear_user_profiles` `p` USING(`user_id`) '
             . 'GROUP BY `p`.`user_id`';
        if ($query->execute($sql) !== true)
            throw new GSqlException();
        while (!$query->eof()) {
            $rec = $query->next();
            $data[] = array(
                'text'    => $rec['profile_name'] . ' <b>(' . $rec['count'] . ')</b>',
                'qtip'    => $rec['profile_name'],
                'leaf'    => true,
                'iconCls' => 'icon-folder-find',
                'value'   => $rec['user_id']
            );
        }

        return $data;
    }

    /**
     * Вывод данных в интерфейс компонента
     * 
     * @param  string $name название триггера
     * @param  string $node id выбранного узла
     * @return void
     */
    protected function dataView($name, $node)
    {
        parent::dataView($name, $node);

        // триггер
        switch ($name) {
            // быстрый фильтр списка
            case 'gridFilter':
                // id выбранного узла
                switch ($node) {
                    // по коду
                    case 'byCo':
                        $this->response->data = $this->getCodes();
                        break;

                    // по пользователю
                    case 'byUs':
                        $this->response->data = $this->getUsers();
                        break;

                    // последняя запись
                    case 'byLa':
                        $data = $this->getLastRecord();
                        if ($data)
                            $this->response->data = $data;
                        break;
                }
        }
    }
}
?>