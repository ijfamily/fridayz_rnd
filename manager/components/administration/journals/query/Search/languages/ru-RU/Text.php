<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Text
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Text.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // окно
    'title_search' => 'Поиск в списке "Ошибки запросов пользователей"',
    // поля
    'header_log_id'         => 'Код',
    'header_log_date'       => 'Дата',
    'header_log_query'      => 'Запрос',
    'header_log_error_code' => 'Код ошибки',
    'header_log_error'      => 'Ошибка',
    'header_user_name'      => 'Логин',
    'header_group_name'     => 'Группа',
    'header_profile_name'   => 'Пользователь'

);
?>