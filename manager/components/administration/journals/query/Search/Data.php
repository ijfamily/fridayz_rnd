<?php
/**
 * Gear Manager
 *
 * Контроллер         "Поиск в списке ошибок запросов пользователей"
 * Пакет контроллеров "Ошибки запросов пользователей"
 * Группа пакетов     "Журналы"
 * Модуль             "Система"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    GController_YJournalQuery_Search
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Search/Data');

/**
 * Поиск в списке аутентификации пользователей
 * 
 * @category   Gear
 * @package    GController_YJournalQuery_Search
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
final class GController_YJournalQuery_Search_Data extends GController_Search_Data
{
    /**
     * Идентификатор DOM компонента (списка)
     *
     * @var string
     */
    public $gridId = 'gcontroller_yjournalquery_grid';

    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = 'gcontroller_yjournalquery_grid';

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор,
     * используется для инициализации атрибутов контроллера без конструктора
     * 
     * @return void
     */
    public function construct()
    {
        // поля записи (ExtJS class "Ext.data.Record")
        $this->fields = array(
            array('dataIndex' => 'log_query', 'label' => $this->_['header_log_query']),
            array('dataIndex' => 'log_error_code', 'label' => $this->_['header_log_error_code']),
            array('dataIndex' => 'log_error', 'label' => $this->_['header_log_error']),
            array('dataIndex' => 'user_name', 'label' => $this->_['header_user_name']),
            array('dataIndex' => 'group_name', 'label' => $this->_['header_group_name']),
            array('dataIndex' => 'profile_name', 'label' => $this->_['header_profile_name'])
        );
    }
}
?>