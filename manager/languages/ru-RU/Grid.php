<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Grid.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // Столбцы списка
    'Id column'          => 'ID',
    'Date insert column' => 'Дата вставки',
    'Date update column' => 'Дата правки',
    'System column'      => 'Системная',
    // Подсказки
    'Update record' => 'Изменил(а) запись - ',
    'Insert record' => 'Добавил(а) запись - ',
    'Date insert'   => 'Дата вставки',
    'Time insert'   => 'Время вставки',
    'Date update'   => 'Дата правки',
    'Time update'   => 'Время правки',
    // Cообщения
    'grid' => 'Список',
    'Successfully columns restored!' => 'Столбцы списка успешно востановлены!',
    'Successfully columns stored!'   => 'Столбцы списка успешно сохранены!'
);
?>