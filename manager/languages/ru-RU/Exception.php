<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Exception
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Exception.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // GException
    'Template error' => 'Тип: <font color="red"><b>%s</b></font><br>Строка: <b>%s</b><br>Файл: <b>%s</b><br> %s',
    'Send error report' => '&nbsp;&nbsp;Отправить отчёт об ошибке&nbsp;&nbsp;',

    // GController
    // статусы
    'Error access'                => 'Ошибка доступа',
    'Error processing controller' => 'Ошибка обработки контроллера',
    'Controller error'            => 'Ошибка контроллера',
    'Error'                       => 'Ошибка',
    'Warning'                     => 'Предупреждение',
    // сообщения
    'Controller inner error'                                    => 'Ошибка контроллера',
    'Error key'                                                 => 'Неверный ключ доступа или вы долго отсутствовали!',
    'Unable to initialize the action "%s" controller'           => 'Невозможно инициализировать действие "%s" контроллера',
    'Unable to initialize method "%s" controller'               => 'Невозможно инициализировать метод "%s" контроллера',
    'The controller class "%s" is not found in the script "%s"' => 'Класс контроллера "%s" не найден в сценарии "%s"',
    'Controller "%s" does not exist'                            => 'Контроллер "%s" не существует',
    'Controller is disabled'                                    => 'Контроллер отказывается отвечать на запросы пользователя',
    'You are not authorized or long-absent'                     => 'Вы не авторизировались или долго отсутствовали,<br>'
                                                                 . 'для корректной работы в системе вам необходимо пройти авторизацию',
    'Unauthorized access to the system'    => 'Попытка несанкционированного доступа к системе (подмена IP адреса и доменного имени)',
    'No privileges to perform this action' => 'Ошибка выполнения запроса.<br>Нет привилегий для выполнения этого действия!',
    'System can not handle expected by requests from your Web server' => 'Система не может обработать ожидаемы запросы от Вашего Веб-сервера (установите в файле конфигурации нужный веб-сервер)',

    // GController_Data
    // статусы
    'Adding data'   => 'Добавление данных',
    'Updating data' => 'Изменение данных',
    'Deleting data' => 'Удаление данных',
    // сообщения
    'Is successful append'              => 'Успешно выполнено добавление записи!',
    'Change is successful'              => 'Изменение данных выполнено успешно!',
    'Is successful record deletion!'    => 'Успешно выполнено удаление записи!',
    'Successfully deleted %s records!'  => 'Успешно выполнено удаление %s записей!',
    'Successfully deleted all records!' => 'Успешно выполнено удаление всех записей!',
    'Unable to delete records!'         => 'Невозможно выполнить удаление записи(ей) (возможно запись(и) системные)!',
    'Selected record was deleted!'      => 'Выбранная Вами запись не существует или была ранее удалена!',
     // проверка
    'Incorrectly entered or selected from the fields: %s' => 'Неправильно введено или выбрано значение из полей: %s',
    'Unable to validate fields!'                          => 'Невозможно выполнить проверку полей!',
    'Record the values of fields:%s already exists!'      => 'Запись со значениями полей: %s уже существует!',
    'There is a correlation record for deletion'          => 'Существуют зависимые записи, для удаления вам<br>'
                                                           . 'необходимо удалить записи в списках: %s',

    // GDb
    // статусы
    'Connection error' => 'Ошибка подключения',
    // сообщения
    'Error when making a connection to the server' => 'Ошибка при выполнении соединения с сервером базы данных "%s".',

    // GDb_Query
    // статусы
    'Data processing error' => 'Ошибка обработки данных',
    // сообщения
    'Query error (Internal Server Error)'       => 'Ошибка выполенения запроса (внутренняя ошибка сервера)',
    'To execute a query, you must change data!' => 'Для выполнения запроса, необходимо выполнить изменение данных!',
    'Error executing a database query (malformed request)' => 'Ошибка выполнения запроса к базе данных (неправильно сформирован запрос) <br><br> Код ошибки: <b>%s</b>',

    // GFile
    // статусы
    'Loading data' => 'Загрузка данных',
    // сообщения
    'Upload file size exceeded the UPLOAD_MAX_FILESIZE' => 'Размер загружаемого файла превысил значение "UPLOAD_MAX_FILESIZE"!',
    'Can not open file "%s" for writing'          => 'Невозможно открыть файл "%s" для записи!',
    'Unable to write data to a file "%s"'         => 'Невозможно записать данные в файл "%s"!',
    'Upload file size exceeded the MAX_FILE_SIZE' => 'Размер загружаемого файла превысил значение "MAX_FILE_SIZE", который был указан в виде HTML',
    'The uploaded file was only partially loaded' => 'Загруженный файл был загружен лишь частично!',
    'File was not loaded'                         => 'Файл не загружен!',
    'Could not write the file to disk'            => 'Не удалось записать файл на диск!',
    'The download does not match the expansion'   => 'Загружаемый файл не соответствует расширению!',
    'Can not delete file "%s"'                    => 'Невозможно удалить файл "%s"!',
    'Unable to move file "%s"'                    => 'Невозможно переместить файл "%s"!',
    'Error loading file "%s" on the server'       => 'Ошибка загрузки файла "%s" на сервер!',
    'File successfully downloaded!'               => 'Файл успешно загружен!',
    'Can`t perform file deletion "%s"'            => 'Невозможно выполнить удаление файла "%s", файл не существует!',
    'Can`t perform dir deletion "%s"'             => 'Невозможно выполнить удаление каталога "%s", каталог не существует!',
    'Can`t open file "%s" for reading'            => 'Невозможно открыть файла "%s" для чтения!',
    'File "%s" does not exist'                    => 'Файл "%s" не существует!',
    'The file with the name "%s" already loaded!' => 'Файл с названием "%s" уже загружен!',

    // GDir
    // сообщения
    'The directory can not be opened' => 'Каталог не может быть открыт из-за ограничений или ошибок файловой системы!',
    'The directory "%s" can not be opened' => 'Каталог "%s" не может быть открыт из-за ограничений или ошибок файловой системы!',

    // GImage
    // сообщения
    'Unable to determine the type of image' => 'Невозможно определить тип изображения',

    // JSON
    // сообщения
    'Maximum stack depth exceeded'       => 'Максимальная вложенность данных в стэке',
    'Unexpected control character found' => 'Найден неизвестный символ в JSON представлении',
    'Syntax error, malformed JSON'       => 'Ошибка синтаксиса в JSON представлении',
    'The key "%s" is not exist in JSON'  => 'Не существует ключ "%s" в JSON представлении',

    // GInstall
    // сообщения
    'Install'                  => 'Ошибка установки',
    'Unknow install structure' => 'Неизвестен способ установки в JSON представлении'
);
?>