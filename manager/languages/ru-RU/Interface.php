<?php
/**
 * Gear Manager
 *
 * Пакет русской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // Восстановление пароля
    'Recover account (%s)'                         => 'Восстановление учётной записи Manager (%s)',
    'Recover account'                              => 'Восстановление учётной записи - Manager',
    'Title recovery account'                       => 'Восстановление учётной записи',
    'Recover password'                             => 'Востановление пароля',
    'You entered the code wrong!'                  => 'Вы неправильно ввели код!',
    'Procedure of restore successfully completed!' => 'Процедура востановаления успешно пройдена!',
    'In your e-mail will be sent instructions'     => 'На Ваш e-mail будут высланы инструкции по восстановлению пароля.',
    'Code'             => 'Код капчи',
    'Refresh code'     => 'Обновить капчу',
    'Enter the email'  => 'Введите e-mail адрес',
    'Enter the code'   => 'Введите код капчи',
    'Recovery'         => 'Восстановить',

    // Авторизация
    'Application name'    => 'Gear Manager',
    'Gear Magic'          => 'Gear Magic',
    'User name'           => 'Имя пользователя',
    'Enter the user name' => 'Введите имя пользователя',
    'Password'            => 'Пароль',
    'Enter the account password' => 'Введите пароль учетной записи',
    'mail'                => '<a style="color:#747B7D" href="mailto:manager@gearmagic.ru">Gear Magic</a>',
    'All right reserved'  => 'Все права защищены.',
    'Sign in'             => 'Войти',
    'Close'               => 'Закрыть',
    'Login page'          => 'Страница авторизации',
    'To site'             => 'Перейти на сайт',
    'Help'                => 'Справка',
    'version %s'          => 'версия Gear Manager %s (панель управления сайтом)',

    // Оболочка
    'Loading'                            => 'Загрузка',
    'Core loading ...'                   => 'Загрузка ядра ...',
    'Plugins loading ...'                => 'Загрузка плагинов...',
    'Shell creating ...'                 => 'Создание оболочки...',
    'Language loading ...'               => 'Загрузка языков...',
    'Welcome to the Manager (<b>%s</b>)' => 'Добро пожаловать в панель управления сайтом (<b>%s</b>)',
    'user'                               => 'пользователь',
    'user groups'                        => 'группа пользователя',
    'browser version'                    => 'версия браузера',
    'web version'                        => 'веб-сервер',
    'os version'                         => 'версия ОС',
    'core version'                       => 'версия ядра',
    'system version'                     => 'версия системы',
    'language'                           => 'язык',
    'Desktop'                            => 'Рабочий стол',
    'Shortcuts available components'     => 'Ярлыки компонентов',
    'Ping server'                        => 'Пинг сервера',
    'Version'                            => 'Версия системы',
    'You have new mail'                  => 'У вас новое сообщение',
    'Components'                         => 'Компоненты на рабочем столе',
    'Shortcuts'                          => 'Ярлыки компонентов на рабочем столе',

    // Ошибки
    'Warning'         => 'Предупреждение',
    'Error'           => 'Ошибка',
    'Error access'    => 'Ошибка доступа',
    'Unknow error'    => 'Неизвестная ошибка',
    'Action error'    => 'Сервер не может выполнить процесс',
    'User error'      => 'Пользователь с таким E-mail не существует!',
    'Browser error'   => 'Вы используете версию браузера "Internet Explorer", '
                       . 'для коректной работы системы, вам необходим браузер: '
                       . '"<a title="Mozilla Firefox" href="http://www.mozilla.org">Firefox 2+</a>", '
                       . '"<a title="Opera Software" href="http://www.opera.com">Opera 9+</a>", '
                       . '"<a title="Apple Safari" href="http://www.apple.com/ru/safari">Safari 2+</a>", '
                       . '"<a title="Google Chrome" href="http://www.google.com/chrome">Chrome</a>"',
    'PHP error'       => 'Невозможно установить параметр PHP - "%s"!',
    'Extension error' => 'Невозможно подключить расширение - "%s"!',
    'Language error'  => 'Невозможно подключить выбранный вами язык - "%s"!',
    'Location error'  => 'Невозможно установить локальный путь - "%s"!',
    'Timezone error'  => 'Невозможно установить временную зону - "%s"!',
    'Session error'   => 'Невозможно использовать сессию - "%s"!',
    'Hack'            => 'Попытка несанкциоинированного доступа к системе (подмена данных)!',
    '404 error'       => 'Запрашиваемый документ (файл, директория) не найден!',
    '500 error'       => 'Внутренняя ошибка сервера!',
    'Database error'  => 'Ошибка при выполнении соединения с сервером базы данных!',
    'Refresh warning' => 'Вы часто обновляете страницу!',
    'Your account is blocked' => 'Ваша учетная запись заблокирована, обратитесь к администратору системы!',

    // Привилегии
    'privileges' => array(
        'root' => 'Полный доступ', 'select' => 'Чтение', 'insert' => 'Вставка', 'update' => 'Правка',
        'delete' => 'Удаление', 'clear' => 'Очистка', 'export' => 'Экспорт'
    ),

    // Действия пользователей над записями
    'log actions' => array(
        'select' => 'Чтение', 'insert' => 'Вставка', 'update' => 'Правка',
        'delete' => 'Удаление', 'clear' => 'Очистка', 'export' => 'Экспорт', 'restore' => 'Восстановление',
        'print' => 'Печать', 'uninstall' => 'Демонтаж', 'posting' => 'Рассылка', 'import' => 'Импорт', 'archiving' => 'Архивация'
    )
);
?>