<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // Восстановление пароля
    'Recover account (%s)'                         => 'Recover Account Manager (%s)',
    'Recover account'                              => 'Recover account - Manager',
    'Title recovery account'                       => 'Recovery account',
    'Recover password'                             => 'Recover password',
    'You entered the code wrong!'                  => 'You entered the code wrong!',
    'Procedure of restore successfully completed!' => 'Procedure of restore successfully completed!',
    'In your e-mail will be sent instructions'     => 'In your e-mail will be sent instructions on how to reset your password.',
    'Code'             => 'Code captcha',
    'Refresh code'     => 'Refresh captcha',
    'Enter the email'  => 'Enter e-mail address',
    'Enter the code'   => 'Enter the captcha code',
    'Recovery'         => 'Recovery',

    // Авторизация
    'Application name'    => 'Manager',
    'Gear Magic'          => 'Gear Magic',
    'User name'           => 'User name',
    'Enter the user name' => 'Enter the user name',
    'Password'            => 'Password',
    'Enter the account password' => 'Enter the account password',
    'mail'                => '<a style="color:#747B7D" href="mailto:gearmagic.ru@gmail.com">Gear Magic</a>',
    'All right reserved'  => 'All right reserved.',
    'Sign in'             => 'Sign in',
    'Close'               => 'Close',
    'Login page'          => 'Login page',
    'To site'             => 'To site',
    'Help'                => 'Help',
    'version %s'          => 'version %s',

    // Оболочка
    'Loading'                            => 'Loading',
    'Core loading ...'                   => 'Core loading ...',
    'Plugins loading ...'                => 'Plugins loading ...',
    'Shell creating ...'                 => 'Shell creating ...',
    'Language loading ...'               => 'Language loading ...',
    'Welcome to the Manager (<b>%s</b>)' => 'Welcome to the Manager (<b>%s</b>)',
    'user'                               => 'user',
    'user groups'                        => 'user groups',
    'browser version'                    => 'browser version',
    'web version'                        => 'web-server',
    'os version'                         => 'os version',
    'core version'                       => 'core version',
    'system version'                     => 'system version',
    'language'                           => 'language',
    'Desktop'                            => 'Desktop',
    'Shortcuts available components'     => 'Shortcuts available components',
    'Ping server'                        => 'Ping server',
    'Version'                            => 'Version',
    'You have new mail'                  => 'You have new mail',
    'Components'                         => 'Desktop components',
    'Shortcuts'                          => 'Components shortcuts',

    // Ошибки
    'Warning'         => 'Warning',
    'Error'           => 'Error',
    'Error access'    => 'Error access',
    'Unknow error'    => 'Unknow error',
    'Action error'    => 'The server can not perform the process',
    'User error'      => 'User with this E-mail does not exist!',
    'Browser error'   => 'You are using a version of the browser "Internet Explorer",<br> '
                       . 'for correct operation of the system, you need a browser: '
                       . '"<a title="Mozilla Firefox" href="http://www.mozilla.org">Firefox 2+</a>", '
                       . '"<a title="Opera Software" href="http://www.opera.com">Opera 9+</a>", '
                       . '"<a title="Apple Safari" href="http://www.apple.com/ru/safari">Safari 2+</a>", '
                       . '"<a title="Google Chrome" href="http://www.google.com/chrome">Chrome</a>"',
    'PHP error'       => 'Can\'t set a parameter PHP - "%s"!',
    'Extension error' => 'Can\'t include one of the extensions - "%s"!',
    'Language error'  => 'Can\'t include selected language - "%s"!',
    'Location error'  => 'Unable to set local path - "%s"!',
    'Timezone error'  => 'Can not set time zone - "%s"!',
    'Session error'   => 'Can not use the session - "%s"!',
    'Hack'            => 'Try to get system access, without atorization (inserting data)!',
    '404 error'       => 'The requested document (file, directory) not found!',
    '500 error'       => 'Server internel error!',
    'Database error'  => 'Execution failed connection to server database!',
    'Refresh warning' => 'You often refreshes the page!',
    'Your account is blocked' => 'Your account is blocked, contact your system administrator!',

    // Привилегии
    'privileges' => array(
        'root' => 'Full access', 'select' => 'Select', 'insert' => 'Insert', 'update' => 'Update',
        'delete' => 'Delete', 'clear' => 'Clear', 'export' => 'Export'
    ),

    // Действия пользователей над записями
    'log actions' => array(
        'select' => 'Read', 'insert' => 'Insert', 'update' => 'Update',
        'delete' => 'Delete', 'clear' => 'Clear', 'export' => 'Export', 'restore' => 'Restore',
        'print' => 'Print', 'uninstall' => 'Uninstall', 'posting' => 'Posting', 'import' => 'Import', 'archiving' => 'Archiving'
    )
);
?>