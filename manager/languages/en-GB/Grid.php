<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Grid.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // Столбцы списка
    'Id column'          => 'ID',
    'Date insert column' => 'Data insert',
    'Date update column' => 'Data update',
    'System column'      => 'System',
    // Подсказки
    'Update record' => 'Update record - ',
    'Insert record' => 'Insert record - ',
    'Date insert'   => 'Date insert',
    'Time insert'   => 'Time insert',
    'Date update'   => 'Date update',
    'Time update'   => 'Time update',
    // Cообщения
    'grid' => 'Grid',
    'Successfully columns restored!' => 'Successfully columns restored!',
    'Successfully columns stored!'   => 'Successfully columns stored!'
);
?>