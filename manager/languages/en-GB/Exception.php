<?php
/**
 * Gear Manager
 *
 * Пакет британской (английской) локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Exception
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Exception.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // GException
    'Template error' => 'Type: <font color="red"><b>%s</b></font><br>Line: <b>%s</b><br>File: <b>%s</b><br>%s',
    'Send error report' => '&nbsp;&nbsp;Отправить отчёт об ошибке&nbsp;&nbsp;',

    // GController
    // статусы
    'Error access'                => 'Error access',
    'Error processing controller' => 'Error processing controller',
    'Controller error'            => 'Controller error',
    'Error'                       => 'Error',
    'Warning'                     => 'Warning',
    // сообщения
    'Controller inner error'                                    => 'Controller inner error',
    'Error key'                                                 => 'The access key is not correct or you long-absen!',
    'Unable to initialize the action "%s" controller'           => 'Unable to initialize the action "%s" controller',
    'Unable to initialize method "%s" controller'               => 'Unable to initialize method "%s" controller',
    'The controller class "%s" is not found in the script "%s"' => 'The controller class "%s" is not found in the script "%s"',
    'Controller "%s" does not exist'                            => 'Controller "%s" does not exist',
    'Controller is disabled'                                    => 'Controller is disabled',
    'You are not authorized or long-absent'                     => 'You are not logged in or long-absent,<br>'
                                                                 . 'for correct operation of the system you need to be authorized',
    'Unauthorized access to the system'    => 'Unauthorized access to the system (IP spoofing and domain name)',
    'No privileges to perform this action' => 'Error query. <br> No privileges to perform this action!',
    'System can not handle expected by requests from your Web server' => 'System can not handle expected by requests from your Web server (set in the configuration file the required web server)',

    // GController_Data
    // статусы
    'Adding data'   => 'Adding data',
    'Updating data' => 'Updating data',
    'Deleting data' => 'Deleting data',
    // сообщения
    'Is successful append'              => 'Is successful append!',
    'Change is successful'              => 'Change is successful!',
    'Is successful record deletion!'    => 'Is successful record deletion!',
    'Successfully deleted %s records!'  => 'Successfully deleted %s records!',
    'Successfully deleted all records!' => 'Successfully deleted all records!',
    'Unable to delete records!'         => 'Unable to perform the removal of the record (s) (possibly record (s) of the system)!',
    'Selected record was deleted!'      => 'Выбранная Вами запись не существует или была ранее удалена!',
     // проверка
    'Incorrectly entered or selected from the fields: %s' => 'Incorrectly entered or selected from the fields: %s',
    'Unable to validate fields!'                          => 'Unable to validate fields!',
    'Record the values of fields:%s already exists!'      => 'Record the values of fields:%s already exists!',
    'There is a correlation record for deletion'          => 'There is a correlation record for deletion, you need<br>'
                                                           . 'to delete the entries in the list: %s',

    // GDb
    // статусы
    'Connection error' => 'Connection error',
    // сообщения
    'Error when making a connection to the server' => 'Error when making a connection to the database server "%s".',

    // GDb_Query
    // статусы
    'Data processing error' => 'Data processing error',
    // сообщения
    'Query error (Internal Server Error)'       => 'Query error (Internal Server Error)',
    'To execute a query, you must change data!' => 'To execute a query, you must change data!',

    // GFile
    // статусы
    'Loading data' => 'Loading data',
    // сообщения
    'Upload file size exceeded the UPLOAD_MAX_FILESIZE' => 'The download file size exceeds the value "UPLOAD_MAX_FILESIZE"!',
    'Can not open file "%s" for writing'          => 'Can not open file "%s" for writing!',
    'Unable to write data to a file "%s"'         => 'Unable to write data to a file "%s"!',
    'Upload file size exceeded the MAX_FILE_SIZE' => 'Upload file size exceeded  "MAX_FILE_SIZE", который был указан в виде HTML',
    'The uploaded file was only partially loaded' => 'The uploaded file was only partially loaded!',
    'File was not loaded'                         => 'File was not loaded!',
    'Could not write the file to disk'            => 'Could not write the file to disk!',
    'The download does not match the expansion'   => 'The download does not match the expansion!',
    'Can not delete file "%s"'                    => 'Can not delete file "%s"!',
    'Unable to move file "%s"'                    => 'Unable to move file "%s"!',
    'Error loading file "%s" on the server'       => 'Error loading file "%s" on the server!',
    'File successfully downloaded!'               => 'File successfully downloaded!',
    'Can`t perform file deletion "%s"'            => 'Can`t perform file deletion "%s"!',
    'Can`t perform dir deletion "%s"'             => 'Невозможно выполнить удаление каталога "%s", каталог не существует!',
    'Can`t open file "%s" for reading'            => 'Can`t open file "%s" for reading!',
    'File "%s" does not exist'                    => 'File "%s" does not exist!',
    'The file with the name "%s" already loaded!' => 'The file with the name "%s" already loaded!',

    // GDir
    // сообщения
    'The directory can not be opened' => 'The directory can not be opened due to permission restrictions or filesystem errors!',

    // GImage
    // сообщения
    'Unable to determine the type of image' => 'Unable to determine the type of image',

    // JSON
    // сообщения
    'Maximum stack depth exceeded'       => 'Maximum stack depth exceeded',
    'Unexpected control character found' => 'Unexpected control character found',
    'Syntax error, malformed JSON'       => 'Syntax error, malformed JSON',
    'The key "%s" is not exist in JSON'  => 'The key "%s" is not exist in JSON',

    // GInstall
    // сообщения
    'Install'                  => 'Error install',
    'Unknow install structure' => 'Unknown method of installing a JSON representation'
);
?>