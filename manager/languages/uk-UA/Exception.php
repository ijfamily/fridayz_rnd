<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Exception
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Exception.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // GException
    'Template error' => 'Тип: <font color="red"><b>%s</b></font><br>Строка: <b>%s</b><br>Файл: <b>%s</b><br>%s',
    'Send error report' => '&nbsp;&nbsp;Отправить отчёт об ошибке&nbsp;&nbsp;',

    // GController
    // статусы
    'Error access'                => 'Помилка доступу',
    'Error processing controller' => 'Помилка обробки контролера',
    'Controller error'            => 'Помилка контролера',
    'Error'                       => 'Помилка',
    'Warning'                     => 'Попередження',
    // сообщения
    'Controller inner error'                                    => 'Помилка контролера',
    'Error key'                                                 => 'Невiрний ключ доступу або ви довго були відсутні!',
    'Unable to initialize the action "%s" controller'           => 'Неможливо ініціалізувати дію "%s" контролера',
    'Unable to initialize method "%s" controller'               => 'Неможливо ініціалізуваті дію "% s" контролера',
    'The controller class "%s" is not found in the script "%s"' => 'Класс контроллера "%s" не найден в сценарии "%s"',
    'Controller "%s" does not exist'                            => 'Контролер "%s" не існує',
    'Controller is disabled'                                    => 'Контролер відмовляється відповідати на запити користувача',
    'You are not authorized or long-absent'                     => 'Ви не авторизовані чи довго були відсутні,<br>'
                                                                 . 'для коректної роботи в системі вам необхідно пройти авторизацію',
    'Unauthorized access to the system'    => 'Спроба несанкціонованого доступу до системи (підміна IP адреси і доменного імені)',
    'No privileges to perform this action' => 'Помилка виконання запиту. <br> Ні привілеїв для виконання цієї дії!',
    'System can not handle expected by requests from your Web server' => 'Система не може обробити очікувані запити від Вашого Веб-сервера (встановіть у файлі конфігурації потрібний веб-сервер)',

    // GController_Data
    // статусы
    'Adding data'   => 'Додавання даних',
    'Updating data' => 'Зміна даних',
    'Deleting data' => 'Видалення даних',
    // сообщения
    'Is successful append'              => 'Успішно виконано додавання запису!',
    'Change is successful'              => 'Зміна даних виконано успішно!',
    'Is successful record deletion!'    => 'Успішно виконано видалення запису!',
    'Successfully deleted %s records!'  => 'Успішно виконано видалення %s записів!',
    'Successfully deleted all records!' => 'Успішно виконано видалення всіх записів!',
    'Unable to delete records!'         => 'Неможливо виконати видалення запису(iв) (можливо запис(и) системні)!',
    'Selected record was deleted!'      => 'Выбранная Вами запись не существует или была ранее удалена!',
     // проверка
    'Incorrectly entered or selected from the fields: %s' => 'Неправильно введено або вибрано значення з полів: %s',
    'Unable to validate fields!'                          => 'Неможливо виконати перевірку полів!',
    'Record the values of fields:%s already exists!'      => 'Запис зі значеннями полів: %s вже існує!',
    'There is a correlation record for deletion'          => 'Існують залежні записи, для видалення вам <br>'
                                                           . 'необхідно видалити записи в списках: %s',

    // GDb
    // статусы
    'Connection error' => 'Помилка підключення',
    // сообщения
    'Error when making a connection to the server' => 'Помилка при виконанні з\'єднання з сервером бази даних "%s".',

    // GDb_Query
    // статусы
    'Data processing error' => 'Помилка обробки даних',
    // сообщения
    'Query error (Internal Server Error)'       => 'Помилка виконання запиту (внутрішня помилка сервера)',
    'To execute a query, you must change data!' => 'Для виконання запиту, необхідно виконати зміну даних!',

    // GFile
    // статусы
    'Loading data' => 'Завантаження даних',
    // сообщения
    'Upload file size exceeded the UPLOAD_MAX_FILESIZE' => 'Розмір завантаження перевищив значенняе "UPLOAD_MAX_FILESIZE"!',
    'Can not open file "%s" for writing'          => 'Неможливо відкрити файл "%s" для запису!',
    'Unable to write data to a file "%s"'         => 'Неможливо записати дані у файл "%s"!',
    'Upload file size exceeded the MAX_FILE_SIZE' => 'Розмір завантаження перевищив значення "MAX_FILE_SIZE", який був вказаний у вигляді HTML',
    'The uploaded file was only partially loaded' => 'Завантажений файл був завантажений лише частково!',
    'File was not loaded'                         => 'Файл не завантажено!',
    'Could not write the file to disk'            => 'Не вдалося записати файл на диск!',
    'The download does not match the expansion'   => 'Файл завантаження не відповідає розширенню!',
    'Can not delete file "%s"'                    => 'Неможливо видалити файл "%s"!',
    'Unable to move file "%s"'                    => 'Неможливо перемістити файл "%s"!',
    'Error loading file "%s" on the server'       => 'Помилка завантаження файлу "%s" на сервер!',
    'File successfully downloaded!'               => 'Файл успішно завантажений!',
    'Can`t perform file deletion "%s"'            => 'Неможливо виконати видалення файлу "%s", файл не існує!',
    'Can`t perform dir deletion "%s"'             => 'Невозможно выполнить удаление каталога "%s", каталог не существует!',
    'Can`t open file "%s" for reading'            => 'Неможливо відкрити файлу "%s" для читання!',
    'File "%s" does not exist'                    => 'Файл "%s" не існує!',
    'The file with the name "%s" already loaded!' => 'Файл с назвою "%s" вже існує!',

    // GDir
    // сообщения
    'The directory can not be opened' => 'Каталог не може бути відкритий через обмеження або помилок файлової системи!',

    // GImage
    // сообщения
    'Unable to determine the type of image' => 'Неможливо визначити тип зображення',

    // JSON
    // сообщения
    'Maximum stack depth exceeded'       => 'Максимальна вкладеність даних в стеку',
    'Unexpected control character found' => 'Знайдено невідомий символ в JSON поданні',
    'Syntax error, malformed JSON'       => 'Помилка синтаксису в JSON поданні',
    'The key "%s" is not exist in JSON'  => 'Не існує ключ "%s" в JSON поданні',

    // GInstall
    // сообщения
    'Install'                  => 'Помилка установки',
    'Unknow install structure' => 'Невідомий спосіб установки в JSON поданні'
);
?>