<?php
/**
 * Gear Manager
 *
 * Пакет украинской локализации
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

return array(
    // Восстановление пароля
    'Recover account (%s)'                         => 'Відновлення облікового запису Manager (%s)',
    'Recover account'                              => 'Відновлення облікового запису - Manager',
    'Title recovery account'                       => 'Відновлення облікового запису',
    'Recover password'                             => 'Відновлення пароля',
    'You entered the code wrong!'                  => 'Ви неправильно ввели код!',
    'Procedure of restore successfully completed!' => 'Процедура відновлення успішно пройдена!',
    'In your e-mail will be sent instructions'     => 'На Ваш e-mail будуть надіслані інструкції з відновлення пароля..',
    'Code'             => 'Код капчи',
    'Refresh code'     => 'Оновити капчу',
    'Enter the email'  => 'Введіть e-mail адресу',
    'Enter the code'   => 'Введіть код капчи',
    'Recovery'         => 'Відновити',

    // Авторизация
    'Application name'    => 'Gear Manager',
    'Gear Magic'          => 'Gear Magic',
    'User name'           => 'Iм\'я користувача',
    'Enter the user name' => 'Введіть ім\'я користувача',
    'Password'            => 'Пароль',
    'Enter the account password' => 'Введіть пароль облікового запису',
    'mail'                => '<a style="color:#747B7D" href="mailto:gearmagic.ru@gmail.com">Gear Magic</a>',
    'All right reserved'  => 'Всі права захіщені.',
    'Sign in'             => 'Ввійти',
    'Close'               => 'Закрити',
    'Login page'          => 'Сторінка авторизації',
    'To site'             => 'Перейти на сайт',
    'Help'                => 'Справка',
    'version %s'          => 'версія %s',

    // Оболочка
    'Loading'                                 => 'Завантаження',
    'Core loading ...'                        => 'Завантаження ядра ...',
    'Plugins loading ...'                     => 'Завантаження плагінів...',
    'Shell creating ...'                      => 'Створення оболонки...',
    'Language loading ...'                    => 'Завантаження мов...',
    'Welcome to the Manager (<b>%s</b>)'      => 'Ласкаво просимо в систему адміністрування (<b>%s</b>)',
    'user'                                    => 'користувач',
    'user groups'                             => 'група користувача',
    'browser version'                         => 'версія браузера',
    'web version'                             => 'веб-сервер',
    'os version'                              => 'версія ОС',
    'core version'                            => 'версія ядра',
    'system version'                          => 'версія системи',
    'language'                                => 'мова',
    'Desktop'                                 => 'Робочий стіл',
    'Shortcuts available components'          => 'Ярлики доступних компонентів',
    'Ping server'                             => 'Пінг сервера',
    'Version'                                 => 'Версія системи',
    'You have new mail'                       => 'У вас нове повідомлення',
    'Components'                              => 'Компоненти на робочому столі',
    'Shortcuts'                               => 'Ярлики компонентів на робочому столі',

    // Ошибки
    'Warning'         => 'Попередження',
    'Error'           => 'Помилка',
    'Error access'    => 'Помилка доступу',
    'Unknow error'    => 'Невідома помилка',
    'Action error'    => 'Сервер не може виконати процес',
    'User error'      => 'Користувач з таким E-mail не існує!',
    'Browser error'   => 'Ви використовуєте версію браузера "Internet Explorer",<br> '
                       . 'для коректної роботи системи, вам необхідний браузер: '
                       . '"<a title="Mozilla Firefox" href="http://www.mozilla.org">Firefox 2+</a>", '
                       . '"<a title="Opera Software" href="http://www.opera.com">Opera 9+</a>", '
                       . '"<a title="Apple Safari" href="http://www.apple.com/ru/safari">Safari 2+</a>", '
                       . '"<a title="Google Chrome" href="http://www.google.com/chrome">Chrome</a>"',
    'PHP error'       => 'Неможливо встановити параметр PHP - "%s"!',
    'Extension error' => 'Неможливо підключити одне з розширень - "%s"!',
    'Language error'  => 'Неможливо підключити вибрану вами мову - "%s"!',
    'Location error'  => 'Неможливо встановити локальний шлях - "%s"!',
    'Timezone error'  => 'Неможливо встановити тимчасову зону - "%s"!',
    'Session error'   => 'Неможливо використовувати сесію - "%s"!',
    'Hack'            => 'Спроба несанкціоінірованного доступу до системи (підміна даних)!',
    '404 error'       => 'Запитуваний документ (файл, директорія) не знайдений!',
    '500 error'       => 'Внутрішня помилка сервера!',
    'Database error'  => 'Помилка при виконанні з\'єднання з сервером бази даних!',
    'Refresh warning' => 'Ви часто оновлюєте сторінку!',
    'Your account is blocked' => 'Ваш обліковий запис заблокований, зверніться до адміністратора системи!',

    // Привилегии
    'privileges' => array(
        'root' => 'Повний доступ', 'select' => 'Читання', 'insert' => 'Вставка', 'update' => 'Правка',
        'delete' => 'Видалення', 'clear' => 'Очищення', 'export' => 'Експорт'
    ),

    // Действия пользователей над записями
    'log actions' => array(
        'select' => 'Читання', 'insert' => 'Вставка', 'update' => 'Правка',
        'delete' => 'Видалення', 'clear' => 'Очищення', 'export' => 'Експорт', 'restore' => 'Восстановление',
        'print' => 'Друк', 'uninstall' => 'Демонтаж', 'posting' => 'Рассылка', 'import' => 'Импорт', 'archiving' => 'Архивация'
    )
);
?>