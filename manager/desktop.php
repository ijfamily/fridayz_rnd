<?php
/**
 * Gear Manager
 *
 * Оболочка
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Desktop
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: desktop.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * @see Gear
 */
require('core/Gear.php');

// проверка ключа безопасности
if (!Gear::checkKey()) Gear::redirect('/error-404/');

// инициализация конфигурации системы
$config = GFactory::getConfig();
// инициализация сессии
$sess = GFactory::getSession();
$sess->start();
// инициализация языка системы
$language = GLanguage::getInstance($config->get('LANGUAGE'), $config->get('LANGUAGES'));
$alias = $language->get('alias');
// верефикация пользователя
if (!$sess->has('user_id')) Gear::redirect('./?authorize');

//Gear::library('Uri');
$uri = GFactory::getUri();
// инициализация языка интерфейса
$_ = GText::getSection('interface');
// версия ядра системы
$vCore = GFactory::getClass('Version_Core', 'Version');
// версия приложения
$vApp = GFactory::getClass('Version_Application', 'Version');
// инициализация профиля пользователя
$shellTitle = $sess->get('group/shortname') . ': ' . $sess->get('profile/name');
$userIcon = $sess->get('profile/gender') ? $sess->get('group/logo/man') : $sess->get('group/logo/woman');
if ($sess->get('invisible'))
    $userIcon = str_replace('.', '-inv.', $userIcon);
$langTitle = '<a href="#" onclick="javascript:Manager.widget.load({url: \'router/system/settings/languages' . ROUTER_DELIMITER . '/grid/interface\'});">'
           . $language->get('name') . '</a>';
$userSettings = $sess->get('user/settings');
?>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="robots" content="none"/>

    <link rel="shortcut icon" type="image/x-icon" href="resources/themes/<?=$userSettings['theme'];?>/images/favicon.ico" />
    <!-- extjs extensions -->
    <link rel="stylesheet" type="text/css" href="resources/themes/default/ext-all-notheme.css" />
    <link rel="stylesheet" type="text/css" href="resources/themes/<?=$userSettings['theme'];?>/theme.css" />
    <link rel="stylesheet" type="text/css" href="resources/js/extjs/ux/css/ux-all.css" />
    <!-- widgets -->
    <link rel="stylesheet" type="text/css" href="resources/js/extjs/ux/htmleditor/css/HtmlEditor.css" />
    <link rel="stylesheet" type="text/css" href="resources/themes/<?=$userSettings['theme'];?>/widgets/mediaviewer.css" />
    <link rel="stylesheet" type="text/css" href="resources/themes/<?=$userSettings['theme'];?>/widgets/grid.css" />
    <link rel="stylesheet" type="text/css" href="resources/themes/<?=$userSettings['theme'];?>/widgets/shortcuts.css" />
    <link rel="stylesheet" type="text/css" href="resources/themes/<?=$userSettings['theme'];?>/widgets/field.css" />
    <link rel="stylesheet" type="text/css" href="resources/themes/<?=$userSettings['theme'];?>/widgets/form.css" />
    <link rel="stylesheet" type="text/css" href="resources/themes/<?=$userSettings['theme'];?>/widgets/button.css" />
    <link rel="stylesheet" type="text/css" href="resources/themes/<?=$userSettings['theme'];?>/widgets/site.css" />
    <!-- manager -->
    <link rel="stylesheet" type="text/css" href="resources/themes/<?=$userSettings['theme'];?>/manager.css" />
    <!-- desktop -->
    <link rel="stylesheet" type="text/css" href="resources/themes/<?=$userSettings['theme'];?>/desktop.css" />

    <!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="resources/css/shell-ie.css" />
    <![endif]-->
    <style>
<?php
// если есть фон изображения списка
if (!$userSettings['grid/background/use']) {
    echo '.x-grid3-scroller { background: none; }';
}
// если есть цвет фона списка
if (!empty($userSettings['grid/background/color'])) {
    echo '.x-grid3-scroller { background-color: #', $userSettings['grid/background/color'],'; }';
}
?>
    </style>
</head>

<body>
    <div id="cmp-mask"></div>
    <div id="loading-mask"></div>
    <div id="loading">
        <div class="center">
            <div class="logo"></div>
            <div class="content">
                <img src="resources/themes/default/images/loading/loading_large.gif" style="float: left;"/>
                <div class="title">
                    <div id="loading-title"><?=$_['Loading'];?> <span id="loading-percent">0%</span></div>
                    <div id="loading-msg">...</div>
                </div>
            </div>
        </div>
    </div>
    <!-- ext core -->
    <script type="text/javascript">
        document.getElementById('loading-msg').innerHTML = '<?=$_['Core loading ...'];?>';
        document.getElementById('loading-percent').innerHTML = '20%';
    </script>
    <script type="text/javascript" src="resources/js/extjs/adapter/ext/ext-base<?=$userSettings['debug/js/core'] ? '-debug' : '';?>_3.4.0.js"></script>
    <script type="text/javascript" src="resources/js/extjs/ext-all<?=$userSettings['debug/js/core'] ? '-debug' : '';?>_3.4.0.js"></script>
    <script type="text/javascript">
        Ext.ROUTER_DELIMITER = "<?=ROUTER_DELIMITER;?>";
        document.getElementById('loading-msg').innerHTML = '<?=$_['Plugins loading ...'];?>';
        document.getElementById('loading-percent').innerHTML = '70%';
    </script>
    <!-- ext extensions -->
    <script type="text/javascript" src="resources/js/extjs/ux/ux-all.min.js"></script>
    <script type="text/javascript" src="resources/js/vendors/md5.min.js"></script>
    <script type="text/javascript" src="resources/js/extjs/ux/htmleditor/htmleditor.min.js"></script>
    <!-- gear manager -->
    <script type="text/javascript" src="resources/js/manager/manager.min.js"></script>
    <script type="text/javascript" src="router.php/system/shell/<?=ROUTER_DELIMITER;?>init/data/"></script>
    <script type="text/javascript" src="resources/js/manager/site.js"></script>
    <script type="text/javascript" src="resources/js/vendors/tinymce/tinymce.min.js"></script>

    <iframe id="a-frame" style="display: none;" src="router.php/system/user/<?=ROUTER_DELIMITER;?>sound/play/?action=welcome"></iframe>

    <!-- language -->
    <script type="text/javascript">
        document.getElementById('loading-msg').innerHTML = '<?=$_['Language loading ...'];?>';
        document.getElementById('loading-percent').innerHTML = '100%';
    </script>
    <script type="text/javascript" src="resources/locale/ext-lang-<?=$sess->get('language/default/alias');?>.min.js"></script>
    <script type="text/javascript" src="resources/locale/ux-lang-<?=$sess->get('language/default/alias');?>.min.js"></script>

    <div id="header">
        <div id="header-button"></div>
        <div id="header-menu"></div>
        <div id="header-tray"> </div>
    </div>
    <div id="main-tab" class="x-hide-display">
        <div id="main-tab-data" class="main-tab-data">
            <div class="main-tab-data" style="position:relative;background: transparent url(resources/themes/default/images/users/<?=$userIcon;?>) no-repeat scroll 20px 10px">
                <p style="margin-left:90px; padding-top:5px;"><?php printf($_['Welcome to the Manager (<b>%s</b>)'], strtoupper('<a href="' . $uri->getHost(). '" target="_blank">' . $_SERVER['SERVER_NAME'] . '</a>'));?>
<?php if ($sess->get('invisible')) { ?>
                 - <b>[INVISIBLE MODE]</b>
<?php } ?>
                </p>
                <div style="margin-left:130px;margin-top:15px;">
                    <div class="main-tab-block">
                        <div class="main-tab-label">
                            <span class="left"><?=$_['user'];?></span>: 
                            <a href="#" onclick="javascript:Manager.widget.load({url: 'router.php/administration/users/contingent/<?=ROUTER_DELIMITER;?>profile/interface/?state=info&self=true'});"><?php print $sess->get('profile/name'); ?></a>
                        </div>
                        <div class="main-tab-label" style="margin-top: 2px;">
                            <span class="left"><?=$_['user groups'];?></span>: <?=$sess->get('group/shortname') , ' (' , $sess->get('group/about') , ')';?>
                        </div>
                        <div class="main-tab-label" style="margin-top: 2px;">
                            <span class="left"><?=$_['language'];?></span>: <?=$language->get('name');?>
                        </div>
                        <div class="main-tab-label" style="margin-top: 2px;">
                            <span class="left"><?=$_['browser version'];?></span>: <?=$sess->get('version/browser'), ' / ', $_SERVER['REMOTE_ADDR'];?>
                        </div>
                    </div>
                    <div class="main-tab-block">
                        <div class="main-tab-label" style="margin-top: 8px;">
                            <span class="left"><?=$_['web version'];?></span>: <?=$sess->get('version/web'), ' / ', $_SERVER['SERVER_ADDR'];?>
                        </div>
                        <div class="main-tab-label" style="margin-top: 2px;">
                            <span class="left"><?=$_['os version'];?></span>: <?=$sess->get('version/os');?>
                        </div>
                        <div class="main-tab-label" style="margin-top: 2px; padding-bottom: 0px;">
                            <span class="left"><?=$_['core version'];?></span>: <?=$vCore->getFullVersion();?>
                        </div>
                        <div class="main-tab-label" style="margin-top: 2px; padding-bottom: 0px;">
                            <span class="left"><?=$_['system version'];?></span>: <?=$vApp->getFullVersion();?>
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                </div>
                <div class="main-desk">
                    <div class="main-desk-title">
                        <?=$_['Desktop'];?> / <span id="main-desk-title"><?=$_['Shortcuts available components'];?></span> <span id="main-desk-count"></span>
                        <span class="main-desk-links"><a id="main-btn-vc" href="#" title=" <?=$_['Components'];?>"></a>
                        <a id="main-btn-vs" href="#" title="<?=$_['Shortcuts'];?>"></a></span>
                    </div>
                    <div class="main-desk-line" align="right"><div></div></div>
                    <div class="main-desk-toolbar"><span id="main-btn-view" style="float: right;"></span><span id="main-btn-dsk" style="float: right;"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="logout-mask">
        <div class="logout">
            <div class="logout-title logo"></div>
            <ul>
                <li><a class="icon-back" href="#"></a></li>
                <li><a class="icon-account" href="#"></a></li>
                <li><a class="icon-turnoff" href="#"></a></li>
            </ul>
        </div>
    </div>
</body>
</html>