<?php
/**
 * Gear Manager
 *
 * Конфигурация системы
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Libraries
 * @package    Config
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Config.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * Конфигурация системы Gear
 * 
 * @category   Libraries
 * @package    Config
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Config.php 2016-01-01 15:00:00 Gear Magic $
 */
final class GConfig
{
   /**
     * Установленные параметры конфигурации
     *
     * @var array
     */
    protected $_install = array();

   /**
     * Параметры конфигурации по умолчанию
     *
     * @var array
     */
    protected $_default = array(
        // ключ для запросов к cms из админ-панели (для каждого сайта свой)
        'CMS/TOKEN'         => '',
        // ключ безопасности
        'SECURITY_KEY'      => '',
        // ограничивает действие куки сессии в пределах домена
        'COOKIE/DOMAIN'     => '',
        // ограничивает действие куки сессии в пределах URI
        'COOKIE/PATH'       => '/',
        // настройки модулей
        'CONFIG'            => array('Mimes' => 'MIMES'),
        // E-mail сервера (то кем будут подписаны письма отправленные сервером)
        'MAIL/HOST'         => 'info@host.com',
        // кому будут отправляться письма от клиентов (подписываться MAIL/HOST)
        'MAIL/ADMIN'        => 'info@host.com',
        // ip адрес сервера
        'SERVER/IP_ADDRESS' => '127.0.0.1',
        // dns имя сервера
        'SERVER/NAME'       => array('host.com', 'www.host.com'),
        // подключения к серверам баз данных
        'DB' => array(),
        // драйвер подключения к базе данных
        'DB/DRIVER'         => 'MySQL',
        // часовой пояс
        'TIMEZONE'          => 'Europe/Kiev',
        // выгрузка данных пользователей в динамичиский массив (users/data)
        'USERS/PRELOADABLE' => true,
        // настройки пользователя
        'USER/SETTINGS'     => array(
            // тема
            'theme'     => 'gray',
            // звук
            'sound'     => 1,
            // вид ярлыков
            'shortcuts' => 1,
            // вид рабочего стола
            'desktop'           => 3,
            // отладка
            'debug/js'              => 1,
            'debug/js/core'         => 1,
            'debug/php'             => 1,
            'debug/php/errors'      => 0,
            'receive/mails'         => 1,
            'receive/messages'      => 1,
            'receive/js/errors'     => 1,
            'grid/background/use'   => 1,
            'grid/background/color' => '',
            // дата и время
            'format/date' => 'd-m-Y',
            'format/time' => 'H:i:s',
            'format/datetime' => 'd-m-Y H:i:s'
        ),
        // восстановление учётной записи (форма авторизации)
        'LOGON/RESTORE_ACCOUNT' => true,
        // используемыу языки
        'LANGUAGES' => array(
            'ru-RU' => array('id' => 1, 'alias' => 'ru-RU', 'name' => 'Русский', 'title' => 'Русский - Russian'),
            'uk-UA' => array('id' => 2, 'alias' => 'uk-UA', 'name' => 'Українська', 'title' => 'Українська - Ukrainia'),
            'en-GB' => array('id' => 3, 'alias' => 'en-GB', 'name' => 'English (United Kingdom)', 'title' => 'English (United Kingdom)'),
            //'ge-GE' => array('id' => 4, 'alias' => 'en-GB', 'name' => 'ქართული', 'title' => 'ქართული - Georgian'),
            'cs-CZ' => array('id' => 5, 'alias' => 'cs-CZ', 'name' => 'Čeština', 'title' => 'Čeština - Czech')
        ),
        // язык по умолчанию
        'LANGUAGE'       => 'ru-RU',
        // подключать пути в настройках php
        'PATH/INCLUDE'   => true,
        // имя сессии
        'SESSION/NAME'   => 'sid',
        // жизнь сессии в минутах
        'SESSION/EXPIRE' => 15,
        // настройки PHP
        'PHP'            => array(
            // кодировка по умолчанию
            'DEFAULT_CHARSET'     => 'UTF8',
            // максимальный размер загрузки файлов
            'UPLOAD_MAX_FILESIZE' => '',
            // максимальный размер данных передаваемых через метод "POST"
            'POST_MAX_SIZE'       => '',
            // maximum amount of memory in bytes that a script is allowed to allocate
            'MEMORY_LIMIT'        => '100M',
            // maximum time in seconds a script is allowed to run before it is terminated by the parser
            'MAX_EXECUTION_TIME'  => 60
        ),
        // настройки фотографий
        'PHOTO'          => array(
            'SIZE' => array('WIDTH' => 80, 'HEIGHT' => 100),
            'PATH' => 'data/photos/'
        ),
        // допустимые php расширения
        'PHP/EXTENSIONS'    => array('mysql', 'json', 'session', 'gd', 'mbstring'/*, 'bcmath'*/),
        // импорт модулей
        'IMPORT' => array(),
        // соцсети автора
        'AUTHOR' => array(
            'Skype'    => 'gearmagic',
            'Twitter'  => 'http://twitter.com/GearMagic',
            'LinkedIn' => 'http://www.linkedin.com/pub/gear-magic/',
            'Google +' => 'https://plus.google.com/GearMagic/'
        ),
        // справочники
        'REFERENCES' => array(),
        // проверка попыток авторизаций
        'ACCOUNT/CHECK_VISITS' => false,
        // количество попыток авторизации
        'ACCOUNT/COUNT_VISITS' => 3
    );

   /**
     * Текущие настройки
     *
     * @var array
     */
    protected $_params = array();

    /**
     * Файл конфигурации (по умолчанию)
     *
     * @var string
     */
    protected $_filename = 'System.php';

    /**
     * Сылка на экземпляр класса
     *
     * @var mixed
     */
    protected static $_instance = null;

    /**
     * Конструктор
     *
     * @return void
     */
    public function __construct($path = 'config')
    {
        // подключение установленных настроек
        if (($this->_install = require_once($path . '/' . $this->_filename)) === false) return;
        // применении настроек
        $this->_params = array_merge($this->_default, $this->_install);
        if (empty($this->_params['DOCUMENT_ROOT']))
            $this->_params['DOCUMENT_ROOT'] = $_SERVER['DOCUMENT_ROOT'];
        // возращает настройки модулей
        $this->additionalConfig($path);
        // импорт сценариев
        $this->loadImport($this->_params['IMPORT']);
        // инициализация часового пояса
        $this->initTimeZone();
    }

    /**
     * Возращает указатель на созданный экземпляр класса (если класс не создан, создаст его)
     * 
     * @return mixed
     */
    public static function getInstance($path = 'config')
    {
        if (null === self::$_instance)
            self::$_instance = new self($path);

        return self::$_instance;
    }

    /**
     * Возращает настройки модулей (указанных в параметре "CONFIG")
     *
     * @return array
     */
    public function additionalConfig($path = 'config')
    {
        foreach ($this->_params['CONFIG'] as $key => $value) {
            $filename = $path . '/' . $key . '.php';
            if (($arr = require_once($filename)) !== false)
                $this->_params[$value] = $arr;
        }
    }

    /**
     * Импорт сценариев из каталога "Gear/Imports"
     *
     * @param  mixed $arr массив сценариев (array("simple1", "simple2", ...) => array("Gear/Imports/simple1.php", "Gear/Imports/simple2.php", ....))
     * @return array
     */
    public function loadImport($arr)
    {
        if (is_array($arr)) {
            $size = sizeof($arr);
            for ($i = 0; $i < $size; $i++) {
                Gear::import($arr[$i], true, false);
            }
        } else
            if ($arr)
                Gear::import($arr, true, false);
    }

    /**
     * Возращает установленные параметры по ключу (если он есть)
     *
     * @param  mixed $key ключ
     * @return mixed
     */
    public function getInstall($key = null)
    {
        if ($key)
            return isset($this->_install[$key]) ? $this->_install[$key] : false;

        return $this->_install;
    }

    /**
     * Возвращает текущие настройки системы по ключу (если он есть)
     *
     * @param  string $key ключ
     * @return mixed
     */
    public function getParams($key = '', $default = '')
    {
        if ($key)
            return isset($this->_params[$key]) ? $this->_params[$key] : $default;

        return $this->_params;
    }

    /**
     * Возвращает текущие настройки системы по ключу (если он есть)
     *
     * @param  string $key ключ
     * @return mixed
     */
    public function get($key = '', $default = '')
    {
        return $this->getParams($key, $default = '');
    }

    /**
     * Возвращает значение параметра из файла конфигурации по ключу
     *
     * @param  string $name файл конфигурации
     * @param  string $key ключ
     * @return mixed
     */
    public function getFrom($name, $key = '', $default = false)
    {
        if ($key)
            return isset($this->_params[$name][$key]) ? $this->_params[$name][$key] : $default;

        return isset($this->_params[$name]) ? $this->_params[$name] : $default;
    }

    /**
     * Возвращает значение параметра из файла конфигурации по ключу
     *
     * @param  string $basename файл конфигурации
     * @param  string $name название параметра
     * @param  string $default поумолчанию (если нет значения)
     * @return mixed
     */
    public function getFromCms($basename, $name = false, $default = false)
    {
        if (isset($this->_params[$basename]))
            $settings = $this->_params[$basename];
        else {
            if (($settings = require_once('../' . PATH_CONFIG_CMS . $basename . '.php')) === false) return false;
            $this->_params[$basename] = $settings;
        }

        if ($name === false)
            return $settings;
        else
            return isset($settings[$name]) ? $settings[$name] : $default;
    }

    /**
     * Возвращает значение параметра из файла конфигурации по ключу
     *
     * @param  string $name файл конфигурации
     * @param  string $key ключ
     * @return mixed
     */
    public function load($basename)
    {
        if (($settings = require_once(PATH_CONFIG . '/' . $basename . '.php')) === false) return false;

        return $this->_params[strtoupper($basename)] = $settings;
    }

    /**
     * Возвращает настройки системы по умолчанию
     *
     * @param  string $key ключ
     * @return mixed
     */
    public function getDefault($key = '')
    {
        if ($key)
            return isset($this->_default[$key]) ? $this->_default[$key] : false;

        return $this->_default;
    }

     /**
     * Установка значения $value переменной $name
     * 
     * @param  string $name имя переменной
     * @param  mixed $value значение
     * @return void
     */
    public function set($name, $value = null)
    {
        if ($value === null)
            unset($this->_params[$name]);
        else
            $this->_params[$name] = $value;
    }

     /**
     * Проверка существования переменной $name
     * 
     * @param  string $name имя переменной
     * @return boolean
     */
    public function has($name)
    {
        return isset($this->_params[$name]);
    }

    /**
     * Проверка существования расширения
     *
     * @var boolean
     */
    public function initExtensions()
    {
        $ext = $this->getParams('PHP/EXTENSIONS');
        $count = sizeof($ext);
        for ($i = 0; $i < $count; $i++)
            if (!extension_loaded($ext[$i]))
                return $ext[$i];

        return true;
    }

    /**
     * Проверка существования расширения PHP
     *
     * @return mixed
     */
    public function initPHP()
    {
        foreach($this->_params['PHP'] as $key => $value) {
            $key = strtolower($key);
            if (strlen($value) > 0)
                if (ini_set($key, $value) === false)
                    return $key;
        }

        return true;
    }

    /**
     * Инициализация часового пояса
     *
     * @return void
     */
    public function initTimeZone()
    {
        date_default_timezone_set($this->get('TIMEZONE'));
    }
}
?>