<?php
/**
 * Gear Manager
 * 
 * Пакет отбработки писем
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Libraries
 * @package    Mail
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Mail.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * Класс обработки писем
 * 
 * @category   Libraries
 * @package    Mail
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Mail.php 2016-01-01 15:00:00 Gear Magic $
 */
class GMail
{
    /**
     * Очередь писем
     * 
     * @var array
     */
    public $_mails = array();

    /**
     * Возращает атрибуты письма по умолчанию если $mail отсутствует
     * 
     * @param  array $mail атрибуты письма
     * @return array
     */
    protected function getDefault($mail = array())
    {
        return array_merge(array(
            'type'    => 'simple',
            'to'      => 'to@simple.com',
            'from'    => 'from@simple.com',
            'message' => 'Simple message',
            'subject' => 'New message',
            'headers' => array(),
            'data'    => array()
        ), $mail);
    }

    /**
     * Возращает письмо из шаблона
     * 
     * @param  array $data данные для подставновки в шаблон
     * @param  array $template параметры шаблона
     * @return mixed
     */
    protected function getTemplate($data = '', $template)
    {
        if (empty($template) || empty($data)) return false;

        $GLOBALS['data-mail'] = $data;
        // путь к шаблону
        ob_start();
        // подключение шаблона
        require($template);
        $tpl = ob_get_contents();
        ob_clean();

        return $tpl;
    }

    /**
     * Добавляет письмо в очередь
     * 
     * @param  array $mail письмо
     * @return void
     */
    public function add($mail)
    {
        $this->_mails[] = $this->getDefault($mail);
    }

    /**
     * Возращает письмо с его атрибутами
     * 
     * @param  integer $index порядковый номер
     * @return array
     */
    public function get($index = null)
    {
        if ($index == null)
            return $this->_mails;

        return $this->_mails[$index];
    }

    /**
     * формирует заголовок письма
     * 
     * @param  array $headers заголовки письма
     * @return string
     */
    protected function getHeaders($headers)
    {
        $str = '';
        foreach ($headers as $key => $value)
            $str .= $key . ': '. $value . "\r\n";

        return $str;
    }

    /**
     * Отправляет письмо $mail
     * 
     * @param  array $mail письмо
     * @return boolean
     */
    protected function _send($mail)
    {
        if (!function_exists('mail')) return false;
        $params = array();
        $params[] = $mail['to'];
        $params[] = $mail['subject'];
        $params[] = $mail['message'];
        // тип письма
        switch ($mail['type']) {
            // простое письмо
            case 'simple':
                if ($mail['headers'])
                    $params[] = $this->getHeaders($mail['headers']);
                else
                    if ($mail['from'])
                        $params[] = 'From: ' . $mail['from'];

                return call_user_func_array('mail', $params);

            // письмо ввиде html
            case 'html':
                $mail['headers'] = $mail['headers']
                    + array('From' => $mail['from'], 'Content-type' => 'text/html; charset=utf-8');
                if ($mail['headers'])
                    $params[] = $this->getHeaders($mail['headers']);

                return call_user_func_array('mail', $params);

            // письмо из шаблона
            case 'template':
                if (($msg = $this->getTemplate($mail['data'], $mail['template'])) === false) return false;
                $params[2] = $msg;
                $mail['headers'] = $mail['headers']+ array('From' => $mail['from'], 'Content-type' => 'text/html; charset=utf-8');
                if ($mail['headers'])
                    $params[] = $this->getHeaders($mail['headers']);

                return call_user_func_array('mail', $params);
        }
    }

    /**
     * Отправляет очередь писем, если указан $mail отправляет его
     * 
     * @param  array $mail письмо
     * @return boolean
     */
    public function send($mail = null)
    {
        if ($mail) {
            if (!isset($mail['to'])) $mail['to'] = '';
            if (is_array($mail['to'])) {
                $count = sizeof($mail['to']);
                $def = $this->getDefault($mail);
                for ($i = 0; $i < $count; $i++) {
                    $def['to'] = $mail['to'][$i];
                    if ($this->_send($def) != true)
                        return false;
                }
            } else
                return $this->_send($this->getDefault($mail));
        }

        $icount = count($this->_mails);
        for ($i = 0; $i < $icount; $i++) {
            $mail = $this->_mails[$i];
            if ($this->_send($mail) != true)
                return false;
        }

        return true;
    }
}
?>