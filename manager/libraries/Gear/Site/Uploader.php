<?php
/**
 * Gear Manager
 * 
 * Загрузчик
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Libraries
 * @package    Site
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Uploader.php 2016-01-01 15:00:00 Gear Magic $
 */

Gear::library('File');

/**
 * Класс загрузчика изображений
 * 
 * @category   Libraries
 * @package    Site
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Uploader.php 2016-01-01 15:00:00 Gear Magic $
 */
final class GUploader
{
  /**
     * Удаление всех статей
     * 
     * @param  object $query указатель на 
     * @return void
     */
    public static function updateImage($path, $filename, $configName, $msg)
    {
        $result = array();

        $config = GFactory::getConfig();

        // определение имени файла
        $tfileExt = $fileExt = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
        // идент. название файла
        if ($config->getFromCms($configName, 'GENERATE/FILE/NAME'))
            $fileId = uniqid();
        else {
            $fileId = pathinfo($fileImage['name'], PATHINFO_FILENAME);
            $fileId = GString::toUrl($fileId,  $config->getFromCms('Site', 'LANGUAGE/ALIAS'));
        }
        $tfileId = $fileId . '_thumb';
        // сгенерировать название файла
        $nfilename = $path . $fileId . '.' . $fileExt;
        $tfilename = $path . $tfileId . '.' . $tfileExt;
        $result['image_entire_filename'] = $fileId . '.' . $fileExt;
        $result['image_thumb_filename'] = $tfileId . '.' . $tfileExt;

        // переименовать оригинал
        if (!rename($filename, $nfilename))
            throw new GException($msg['title_process'], 'Unable to move file "%s"', $path);

        // изменить оригинал
        $width = (int) $config->getFromCms($configName, 'IMAGE/ORIGINAL/WIDTH');
        $height = (int) $config->getFromCms($configName, 'IMAGE/ORIGINAL/HEIGHT');
        $img = GFactory::getClass('Image', 'Image', $nfilename);
        $cmdImage = 'resize{"width": ' . $width . ', "height": ' . $height . '}';
        if (!$img->execute($cmdImage))
            throw new GException('Error', $msg['msg_image_size']);
        $img->save($nfilename, $config->getFromCms($configName, 'IMAGE/QUALITY', 0));
        $result['image_entire_resolution'] = $img->getSizeStr();

        // создать миниатюры
        $width = (int) $config->getFromCms($configName, 'IMAGE/THUMB/WIDTH');
        $height = (int) $config->getFromCms($configName, 'IMAGE/THUMB/HEIGHT');
        $img = GFactory::getClass('Image', 'Image', $nfilename);
        $cmdImage = 'resize{"width": ' . $width . ', "height": ' . $height . '}';
        if (!$img->execute($cmdImage))
            throw new GException('Error', $msg['msg_error_create_thumb']);
        $img->save($tfilename);
        $result['image_thumb_resolution'] = $img->getSizeStr();
        $result['image_thumb_filesize'] = GFile::getFileSize($tfilename);

        $img = GFactory::getClass('Image', 'Image', $nfilename);
        // 
        if ($config->getFromCms($configName, 'WATERMARK')) {
            $img->watermark(
                DOCUMENT_ROOT . $config->getFromCms('Site', 'WATERMARK/STAMP', ''),
                $config->getFromCms($configName, 'WATERMARK/POSITION', '') 
            );
        }
        $img->save($nfilename, $config->getFromCms($configName, 'IMAGE/QUALITY', 0));
        $result['image_entire_filesize'] = GFile::getFileSize($nfilename);

        return $result;
    }

    /**
     * Загрузка изображения в каталог альбома
     * 
     * @param string $path путь к каталогу с изображениями
     * @param string $name имя поля загружаемого файла
     * @param boolean $details возращает информацию при успешной загрузки изображения
     * @return array
     */
    public static function uploadImage($path, $name, $configName, $details = false)
    {
        $config = GFactory::getConfig();
        $input = GFactory::getInput();

        $fileImage = $fileThumb = false;
        $cmdImage = $cmdThumb = '';

        $result = array();

        // если нет изображения для загрузки
        if (($fileImage = $input->file->get($name)) === false)
            throw new GException('Loading data', sprintf('Error loading file "%s" on the server', $uploadPath . $filename));
        // определение имени файла
        $tfileExt = $fileExt = strtolower(pathinfo($fileImage['name'], PATHINFO_EXTENSION));
        // идент. название файла
        if ($config->getFromCms($configName, 'GENERATE/FILE/NAME'))
            $fileId = uniqid();
        else {
            $fileId = pathinfo($fileImage['name'], PATHINFO_FILENAME);
            $fileId = GString::toUrl($fileId,  $config->getFromCms('Site', 'LANGUAGE/ALIAS'));
        }
        $tfileId = $fileId . '_thumb';
        // сгенерировать название файла
        $filename = $fileId . '.' . $fileExt;
        $tfilename = $tfileId . '.' . $tfileExt;


        $uploadPath = $path;
        $uploadExt = explode(',', strtolower($config->getFromCms('Site', 'FILES/EXT/IMAGES')));
        // проверка существования изображения
        if (file_exists($uploadPath . $filename))
            throw new GException('Loading data', sprintf('The file with the name "%s" already loaded!', $uploadPath . $filename));
        // проверка существования эскиза изображения
        if (file_exists($uploadPath . $tfilename))
            throw new GException('Loading data', sprintf('The file with the name "%s" already loaded!', $uploadPath . $tfilename));

        // загрузка изображения
        GFile::upload($fileImage, $uploadPath . $filename, $uploadExt);

        // изменить оригинал
        $width = (int) $config->getFromCms($configName, 'IMAGE/ORIGINAL/WIDTH');
        $height = (int) $config->getFromCms($configName, 'IMAGE/ORIGINAL/HEIGHT');
        $img = GFactory::getClass('Image', 'Image', $uploadPath . $filename);
        $cmdImage = 'resize{"width": ' . $width . ', "height": ' . $height . '}';
        if (!$img->execute($cmdImage))
            throw new GException('Error', $this->_['msg_image_size']);
        $img->save($uploadPath . $filename, $config->getFromCms($configName, 'IMAGE/QUALITY', 0));

        // создать миниатюры
        $width = (int) $config->getFromCms($configName, 'IMAGE/THUMB/WIDTH');
        $height = (int) $config->getFromCms($configName, 'IMAGE/THUMB/HEIGHT');
        $img = GFactory::getClass('Image', 'Image', $uploadPath . $filename);
        $cmdImage = 'resize{"width": ' . $width . ', "height": ' . $height . '}';
        if ($img->execute($cmdImage)) {
            // изменение изображения
            $name = pathinfo($filename, PATHINFO_FILENAME) . '_thumb.' . pathinfo($filename, PATHINFO_EXTENSION);
            $img->save($uploadPath . $name);
        }
        // обновляем поля изображения
        if ($details) {
            $result['image_thumb_filename'] = $tfilename;
            $result['image_thumb_filesize'] = GFile::getFileSize($uploadPath . $tfilename);
            $result['image_thumb_resolution'] = $img->getSizeStr();
        }

        $img = GFactory::getClass('Image', 'Image', $uploadPath . $filename);
        // 
        if ($config->getFromCms($configName, 'WATERMARK')) {
            $img->watermark(
                DOCUMENT_ROOT . $config->getFromCms('Site', 'WATERMARK/STAMP', ''),
                $config->getFromCms($configName, 'WATERMARK/POSITION', '') 
            );
        }
        $img->save($uploadPath . $filename, $config->getFromCms($configName, 'IMAGE/QUALITY', 0));
        // обновляем поля изображения
        if ($details) {
            $result['image_entire_filename'] = $filename;
            $result['image_entire_filesize'] = GFile::getFileSize($uploadPath . $filename);
            $result['image_entire_resolution'] = $img->getSizeStr();
        }

        return $result;
    }
}
?>