<?php
/**
 * Gear Manager
 * 
 * Пакет сетевых функций
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Libraries
 * @package    Net
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Net.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * Класс сетевых функций
 * 
 * @category   Libraries
 * @package    Net
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Net.php 2016-01-01 15:00:00 Gear Magic $
 */
class GNet
{
    /**
     * Возращает IP адрес
     * 
     * @return string
     */
    public static function getIpAddress()
    {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) return $_SERVER['HTTP_X_FORWARDED_FOR'];
        if (isset($_SERVER['HTTP_CLIENT_IP'])) return $_SERVER['HTTP_CLIENT_IP'];
        if (isset($_SERVER['REMOTE_ADDR'])) return $_SERVER['REMOTE_ADDR'];
    }

    /**
     * Проверка IP адреса
     * 
     * @param  array $search поиск ip адресов
     * @param  array $allow доступный диапазон ip адресов
     * @return boolean
     */
    public static function checkIpAddress($search, $allow)
    {
        $ipCount = array(32 => 0, 31 => 1, 30 => 3, 29 => 7, 28 => 15, 27 => 31, 26 => 63, 25 => 127, 24 => 255,
                         23 => 511, 22 => 1023, 21 => 2047, 20 => 4095, 19 => 8191, 18 => 16383, 17 => 32767,
                         16 => 65535, 15 => 131071, 14 => 262143, 13 => 524287, 12 => 1048575, 11 => 2097151,
                         10 => 4194303, 9 => 8388607, 8 => 16777215, 7 => 33554431, 6 => 67108863, 5 => 134217727,
                         4 => 268435455, 3 => 536870911, 2 => 1073741823);
        
        $searchConverted = ip2long($search);
        foreach ($allow as $value) {
          list($ip, $prefix) = preg_split('/\//',$value);
          $rangeStart = ip2long($ip);
          $rangeEnd = $rangeStart + $ipCount[$prefix];
          if ($searchConverted >= $rangeStart && $searchConverted <= $rangeEnd)
            return true;
        }

        return false;
    }
}
?>