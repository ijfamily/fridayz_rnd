<?php
/**
 * Gear Manager
 * 
 * Пакет обработки изображений
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Libraries
 * @package    Image
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Image.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * Класс обработки изображения
 * 
 * @category   Libraries
 * @package    Image
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Image.php 2016-01-01 15:00:00 Gear Magic $
 */
class GImage
{
    /**
     * Идентификатор изображения
     * 
     * @var mixed
     */
    private $image;

    /**
     * Исходная ширина изображения
     * 
     * @var integer
     */
    private $width;

    /**
     * Исходная высота изображения
     * 
     * @var integer
     */
    private $height;

    /**
     * Тип изображения (jpg, png, gif)
     * 
     * @var string
     */
    private $type;

    /**
     * Конструктор
     * 
     * @return void
     */
    public function __construct($file)
    {
        if(!$this->setType($file))
            throw new GException('Error', 'Unable to determine the type of image');
        // получение указателя на $image
        if ($this->openImage($file) === false)
            throw new GException('Error', 'Unable to determine the type of image');
        // определение размеров изображения
        $this->setSize();
    }

    /**
     * Выполнение команд для обработки изображения
     * 
     * @param  string $cmd команды
     * @return void
     */
    public function execute($cmd)
    {
        $result = false;
        $lines = explode(';', $cmd);
        $count = sizeof($lines);
        for ($i = 0; $count > $i; $i++) {
            list($func, $params) = explode('{', $lines[$i]);
            $func = 'cmd' . $func;
            $params = '{' . $params;
            if (method_exists($this, $func)) {
                $this->$func(json_decode($params, true));
                $result = true;
            }
        }

        return $result;
    }

    /**
     * Команда обрезки изображения
     * crop{"type": value}
     * 
     * @param  array $params параметры команды
     * @return void
     */
    protected function cmdCrop($params)
    {
        if (isset($params['type']))
            if ($params['type'] == 'square')
                $this->cropSquare();
    }

    /**
     * Команда измененения размера изображения
     * resize{"width": value, "height": value}
     * 
     * @param  array $params параметры команды
     * @return void
     */
    protected function cmdResize($params)
    {
        $width = isset($params['width']) ? $params['width'] : false;
        $height = isset($params['height']) ? $params['height'] : false;

        $this->resize($width, $height);
    }

    /**
     * Команда измененения размера изображения
     * resize{"width": value, "height": value}
     * 
     * @param  array $params параметры команды
     * @return void
     */
    protected function cmdStretch($params)
    {
        $width = isset($params['width']) ? $params['width'] : false;
        $height = isset($params['height']) ? $params['height'] : false;

        $this->stretch($width, $height);
    }

    /**
     * Определение типа изображения
     * 
     * @param  string $file имя файла
     * @return void
     */
    private function setType($file)
    {
        $this->type = strtolower(pathinfo($file, PATHINFO_EXTENSION));
        return true;
        
        $mime = mime_content_type($file);
        switch($mime) {
            case 'image/jpeg':
            $this->type = 'jpg';
            return true;

        case 'image/png':
            $this->type = 'png';
            return true;

        case 'image/gif':
            $this->type = 'gif';
            return true;

        default:
            return false;
        }
    }

    /**
     * Получение указателя на $image
     * 
     * @param  string $filename имя файла
     * @return void
     */
    private function openImage($filename)
    {
        switch($this->type) {
            case 'jpg':
                return $this->image = @imagecreatefromjpeg($filename);

            case 'png':
                return $this->image = @imagecreatefrompng($filename);

            case 'gif':
                return $this->image = @imagecreatefromgif($filename);
        }

        return false;
   }

    /**
     * Определение размеров изображения
     * 
     * @return void
     */
    private function setSize()
    {
        $this->width = imagesx($this->image);
        $this->height = imagesy($this->image);
    }

    /**
     * Определение размеров рамки
     * 
     * @param  integer $width ширина
     * @param  integer $height высота
     * @return array
     */
    private function getSizeByFramework($width, $height)
    {
        if($this->width <= $width && $this->height <= $height) 
            return array($this->width, $this->height);
        if($this->width / $width > $this->height / $height) {
            $newSize[0] = $width;
            $newSize[1] = round($this->height * $width / $this->width);
        } else {
            $newSize[1] = $height;
            $newSize[0] = round($this->width * $height / $this->height);
       }

       return $newSize;
    }

    /**
     * Определение размеров рамки по ширине
     * 
     * @param  integer $width ширина
     * @return array
     */
    private function getSizeByWidth($width)
    {
       if($width >= $this->width) return array($this->width, $this->height);
       $newSize[0] = $width;
       $newSize[1] = round($this->height * $width / $this->width);

       return $newSize;
    }

    /**
     * Возращает разрешение изображения
     * 
     * @return string
     */
    public function getSizeStr()
    {
       return $this->width . 'x' . $this->height;
    }

    /**
     * Определение размеров рамки по высоте
     * 
     * @param  integer $height высота
     * @return array
     */
    private function getSizeByHeight($height)
    {
       if($height >= $this->height) return array($this->width, $this->height);
       $newSize[1] = $height;
       $newSize[0] = round($this->width * $height / $this->height);

       return $newSize;
    }

    /**
     * Изменение размеров изображения
     * 
     * @param  integer $width ширина
     * @param  integer $height высота
     * @return mixed
     */
    function resize($width = false, $height = false)
    {
        // в зависимости от типа ресайза, запишем в $newSize новые размеры изображения
        if(is_numeric($width) && is_numeric($height) && $width > 0 && $height > 0) {
            $newSize = $this->getSizeByFramework($width, $height);
        } else
            if(is_numeric($width) && $width > 0) {
                $newSize = $this->getSizeByWidth($width);
            } else
                if(is_numeric($height) && $height > 0) {
                    $newSize = $this->getSizeByHeight($height);
                } else
                    $newSize = array($this->width, $this->height);
       //создаём новое пустое изображение
       $newImage = imagecreatetruecolor($newSize[0], $newSize[1]);
       imagecopyresampled($newImage, $this->image, 0, 0, 0, 0, $newSize[0], $newSize[1], $this->width, $this->height);
       $this->image = $newImage;
       $this->setSize();

       return $this;
    }

    /**
     * Изменение размеров изображения
     * 
     * @param  integer $width ширина
     * @param  integer $height высота
     * @return mixed
     */
    function stretch($width = false, $height = false)
    {
       //создаём новое пустое изображение
       $newImage = imagecreatetruecolor($width, $height);
       imagecopyresampled($newImage, $this->image, 0, 0, 0, 0, (int) $width, (int) $height, $this->width, $this->height);
       $this->image = $newImage;
       $this->setSize();

       return $this;
    }

    /**
     * Обрезка изображения
     * 
     * @param  integer $x0 точка X
     * @param  integer $y0 точка Y
     * @param  integer $w ширина
     * @param  integer $h высота
     * @return mixed
     */
    function crop($x0 = 0, $y0 = 0, $w = false, $h = false)
    {
       if(!is_numeric($x0) || $x0 < 0 || $x0 >= $this->width) $x0 = 0;
       if(!is_numeric($y0) || $y0 < 0 || $y0 >= $this->height) $y0 = 0;
       if(!is_numeric($w) || ($w = $this->width - $x0)) $w = $this->width - $x0;
       if(!is_numeric($h) || ($h = $this->height - $y0)) $h = $this->height - $y0;

       return $this->cropSave($x0, $y0, $w, $h);
    }

    /**
     * Обрезка изображения по квадратной области
     * 
     * @return void
     */
    public function cropSquare()
    {
        // если изображение - альбом
        if ($this->width > $this->height) {
            $x = round(($this->width - $this->height) / 2);
            $y = 0;
            $this->cropSave($x, $y, $this->height, $this->height);
        // если изображение - портрет
        } else {
            $x = 0;
            $y = round(($this->height - $this->width) / 2);
            $this->cropSave($x, $y, $this->width, $this->width);
        }
    }

    /**
     * Заменить исходное изображение кроупом
     * 
     * @param  integer $x0 точка X
     * @param  integer $y0 точка Y
     * @param  integer $w ширина
     * @param  integer $h высота
     * @return mixed
     */
    public function cropSave($x0, $y0, $w, $h)
    {
       $newImage = imagecreatetruecolor($w, $h);
       imagecopyresampled($newImage, $this->image, 0, 0, $x0, $y0, $w, $h, $w, $h);
       $this->image = $newImage;
       $this->setSize();

       return $this;
    }

    /**
     * Создание водяного знака
     *
     * @return void
     */
    public function watermark($stamp, $position = 'top-left', $offsetX = 0, $offsetY = 0)
    {
        $stamp = imagecreatefrompng($stamp);
        // установка полей для штампа и получение высоты/ширины штампа
        $sx = imagesx($stamp);
        $sy = imagesy($stamp);
        switch ($position) {
            case 'top-left':
                $left = $offsetX;
                $bottom = $offsetY;
                break;

            case 'top-right':
                $left = imagesx($this->image) - $sx - $offsetX;
                $bottom = $offsetY;
                break;

            case 'top-center':
                $left = round(imagesx($this->image) / 2) - round($sx / 2) - $offsetX;
                $bottom = $offsetY;
                break;

           case 'bottom-left':
                $left = $offsetX;
                $bottom = imagesy($this->image) - $sy - $offsetY;
                break;

            case 'bottom-right':
                $left = imagesx($this->image) - $sx - $offsetX;
                $bottom = imagesy($this->image) - $sy - $offsetY;
                break;

            case 'bottom-center':
                $left = round(imagesx($this->image) / 2) - round($sx / 2) - $offsetX;
                $bottom = imagesy($this->image) - $sy - $offsetY;
                break;
        }

        // копирование изображения штампа на фотографию с помощью смещения края и ширины фотографии для расчета позиционирования штампа
        imagecopy($this->image, $stamp, $left, $bottom, 0, 0, imagesx($stamp), imagesy($stamp));
    }

    /**
     * Вывод изображения
     *
     * @return void
     */
    public function render()
    {
        header('Content-type: ' . $this->type);
        imagejpeg($this->image);
        // удаление изображения
        imagedestroy($this->image);
    }

    /**
     * Сохранить изображения
     * 
     * @param  string $filename название файла
     * @return void
     */
    public function save($filename, $quality = 100)
    {
        switch($this->type) {
            case 'jpg':
                imagejpeg($this->image, $filename, $quality);
                break;

            case 'png':
                //imagealphablending($this->image, $filename);
                //imagesavealpha($this->image, true);
                imagepng($this->image, $filename);
                break;

            case 'gif':
                imagegif($this->image, $filename);
                break;
         }
        // удаление изображения
        imagedestroy($this->image);
    }
}
?>