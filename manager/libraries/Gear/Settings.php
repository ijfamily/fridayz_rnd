<?php
/**
 * Gear Manager
 * 
 * Пакет обработки сессий
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Libraries
 * @package    Session
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Session.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * Класс обработки сессий
 * 
 * @category   Libraries
 * @package    Session
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Session.php 2016-01-01 15:00:00 Gear Magic $
 */
class GSettings
{
    /**
     * Возращает состояние сессии
     * 
     * @return string 'inactive'|'active'|'expired'|'destroyed'|'error'
     */
    public static function getModule($module, $name = '')
    {
        $st = GFactory::getSession()->get('settings', false, 'modules/' . $module);

        if ($st === false) return false;
        if ($name)
            return isset($st[$name]) ? $st[$name] : false;
        else
            return $st;
    }

    /**
     * Возращает состояние сессии
     * 
     * @return string 'inactive'|'active'|'expired'|'destroyed'|'error'
     */
    public static function setModule($module, $name, $value)
    {
        $st = &GFactory::getSession()->get('settings', false, 'modules/' . $module);

        if ($st === false) return false;

        $st[$name] = $value;
    }
}
?>