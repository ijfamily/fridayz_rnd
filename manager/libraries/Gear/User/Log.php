<?php
/**
 * Gear CMS
 * 
 * Пакет обработки пользователей сайта
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: User.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Класс пользователя сайта
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: User.php 2016-01-01 12:00:00 Gear Magic $
 */
class GUser_Log
{

}


/**
 * Класс пользователя сайта
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: User.php 2016-01-01 12:00:00 Gear Magic $
 */
class GUser_Log_Attend
{
    protected static $defData = array(
        'attend_browser'   => '',
        'attend_os'        => '',
        'attend_name'      => '',
        'attend_password'  => '',
        'attend_success'   => 0
    );

    public static function defData($data)
    {
        self::$defData = array_merge(self::$defData , $data);
    }

    /**
     * Занесения данных пользователя в журнал авторизации
     * 
     * @param  array $data данные авторизации
     * @return void
     */
    public static function add($data)
    {
        self::$defData['attend_ipaddress'] = $_SERVER['REMOTE_ADDR'];
        self::$defData['attend_date'] = date('Y-m-d');
        self::$defData['attend_time'] = date('H:i:s');
        $table = new GDb_Table('gear_user_attends', 'attend_id');
        $data = array_merge(self::$defData, $data);

        return $table->insert($data);
    }

    /**
     * Занесения данных пользователя в журнал авторизации
     * 
     * @param  array $data данные авторизации
     * @return void
     */
    public static function clear()
    {
        $table = new GDb_Table('gear_user_attends', 'attend_id');
        if ($table->clear(true) !== true)
            throw new GSqlException();
    }
}
?>