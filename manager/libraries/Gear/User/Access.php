<?php
/**
 * Gear CMS
 * 
 * Пакет обработки пользователей сайта
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: User.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::ns('User/User');

/**
 * Класс пользователя сайта
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: User.php 2016-01-01 12:00:00 Gear Magic $
 */
class GUser_Access
{
    public static function forData($table, $who = 'user', $field = '', $whoId = '')
    {
        if (empty($table) || empty($who)) return false;

        $query = GFactory::getQuery();
        switch ($who) {
            case 'user':
                $who = 'user_id';
                if (empty($whoId))
                    $whoId = GUser::getId();
                break;

            case 'group':
                $who = 'group_id';
                if (empty($whoId))
                    $whoId = GUser::getGroupId();
                break;
        }
        //
        $sql = "SELECT * FROM `$table` WHERE $who=" . (int) $whoId;

        //echo $sql;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $fields = array();
        while (!$query->eof()) {
            $rec = $query->next();
            if ($field)
                $fields[] = $rec[$field];
            else
                $fields[] = $rec;
        }

        return $fields;
    }
}


?>