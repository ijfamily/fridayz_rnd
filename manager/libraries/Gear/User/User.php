<?php
/**
 * Gear CMS
 * 
 * Пакет обработки пользователей сайта
 * 
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: User.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Класс пользователя сайта
 * 
 * @category   Gear
 * @package    Libraries
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: User.php 2016-01-01 12:00:00 Gear Magic $
 */
class GUser
{
    /**
     * Добавление пользователя
     *
     * @param  array $data данные пользователя
     * @return void
     */
    public static function add($data)
    {
        // данные килента
        if (!empty($data['user_password']))
            $data['user_password'] = md5($data['user_password']);
        $data['user_account'] = 1;
        $data['user_account_date'] = date('Y-m-d');
        $data['user_account_time'] = date('H:i:s');
        $data['user_account_ip'] = $_SERVER['REMOTE_ADDR'];
        $query = GFactory::getQuery();
        $result = $query->insert('gear_users', $data);
        if ($result === false)
            return false;
        else
            return $query->getLastInsertId();
    }

    /**
     * Гнерация логина пользователя в случае успешного выполнения возвращает уникальный логин пользователя
     * 
     * @param $first_name
     * @param string $last_name
     * @param string $nickname
     * @param string $bdate
     * @param array $delimiters
     * @return string
     */
	public static function genNickname($first_name, $last_name="", $nickname="", $bdate="", $delimiters=array('.', '_')) {
		$delim = array_shift($delimiters);

		$first_name = $this->translitIt($first_name);
		$first_name_s = substr($first_name, 0, 1);

		$variants = array();
		if (!empty($nickname))
			$variants[] = $nickname;
		$variants[] = $first_name;
		if (!empty($last_name)) {
			$last_name = $this->translitIt($last_name);
			$variants[] = $first_name.$delim.$last_name;
			$variants[] = $last_name.$delim.$first_name;
			$variants[] = $first_name_s.$delim.$last_name;
			$variants[] = $first_name_s.$last_name;
			$variants[] = $last_name.$delim.$first_name_s;
			$variants[] = $last_name.$first_name_s;
		}
		if (!empty($bdate)) {
			$date = explode('.', $bdate);
			$variants[] = $first_name.$date[2];
			$variants[] = $first_name.$delim.$date[2];
			$variants[] = $first_name.$date[0].$date[1];
			$variants[] = $first_name.$delim.$date[0].$date[1];
			$variants[] = $first_name.$delim.$last_name.$date[2];
			$variants[] = $first_name.$delim.$last_name.$delim.$date[2];
			$variants[] = $first_name.$delim.$last_name.$date[0].$date[1];
			$variants[] = $first_name.$delim.$last_name.$delim.$date[0].$date[1];
			$variants[] = $last_name.$delim.$first_name.$date[2];
			$variants[] = $last_name.$delim.$first_name.$delim.$date[2];
			$variants[] = $last_name.$delim.$first_name.$date[0].$date[1];
			$variants[] = $last_name.$delim.$first_name.$delim.$date[0].$date[1];
			$variants[] = $first_name_s.$delim.$last_name.$date[2];
			$variants[] = $first_name_s.$delim.$last_name.$delim.$date[2];
			$variants[] = $first_name_s.$delim.$last_name.$date[0].$date[1];
			$variants[] = $first_name_s.$delim.$last_name.$delim.$date[0].$date[1];
			$variants[] = $last_name.$delim.$first_name_s.$date[2];
			$variants[] = $last_name.$delim.$first_name_s.$delim.$date[2];
			$variants[] = $last_name.$delim.$first_name_s.$date[0].$date[1];
			$variants[] = $last_name.$delim.$first_name_s.$delim.$date[0].$date[1];
			$variants[] = $first_name_s.$last_name.$date[2];
			$variants[] = $first_name_s.$last_name.$delim.$date[2];
			$variants[] = $first_name_s.$last_name.$date[0].$date[1];
			$variants[] = $first_name_s.$last_name.$delim.$date[0].$date[1];
			$variants[] = $last_name.$first_name_s.$date[2];
			$variants[] = $last_name.$first_name_s.$delim.$date[2];
			$variants[] = $last_name.$first_name_s.$date[0].$date[1];
			$variants[] = $last_name.$first_name_s.$delim.$date[0].$date[1];
		}
		$i=0;

		$exist = true;
		while (true) {
			if ($exist = $this->userExist($variants[$i])) {
				foreach ($delimiters as $del) {
					$replaced = str_replace($delim, $del, $variants[$i]);
					if($replaced !== $variants[$i]){
						$variants[$i] = $replaced;
						if (!$exist = $this->userExist($variants[$i]))
							break;
					}
				}
			}
			if ($i >= count($variants)-1 || !$exist)
				break;
			$i++;
		}

		if ($exist) {
			while ($exist) {
				$nickname = $first_name.mt_rand(1, 100000);
				$exist = $this->userExist($nickname);
			}
			return $nickname;
		} else
			return $variants[$i];
	}

    /**
     * Возращает пользователя по его идент.
     * 
     * @param integer $id идентификатор пользователя
     * @return mixed
     */
    public static function getId()
    {
        return GFactory::getSession()->get('user_id', false);
    }

    /**
     * Возращает пользователя по его идент.
     * 
     * @param integer $id идентификатор пользователя
     * @return mixed
     */
    public static function getGroupId()
    {
        return GFactory::getSession()->get('group/id', false);
    }

    /**
     * Возращает пользователя по его идент.
     * 
     * @param integer $id идентификатор пользователя
     * @return mixed
     */
    public static function get($id)
    {
        return self::getBy($id, 'id');
    }

    /**
     * Возращает пользователя по условию
     * 
     * @param string $value
     * @param string $type тип (login, e-mail, login_or_e-mail, id, code)
     * @return mixed
     */
    public static function getBy($value, $type = 'login')
    {
        $query = GFactory::getQuery();
        $value = $query->escapeStr($value);
        switch ($type) {
            case 'confirm': $cond = "`user_confirm_code`=$value"; break;

            case 'code': $cond = "`user_recovery_code`=$value"; break;

            case 'id': $cond = "`user_id`=$value"; break;

            case 'login': $cond = "`user_name`=$value"; break;

            case 'e-mail': $cond = "`contact_email`=$value"; break;

            case 'login or e-mail':
            case 'login_or_e-mail': $cond = "`contact_email`=$value OR `user_name`=$value"; break;

            default: $cond = ' 0';
        }

        // проверка существования пользователя
        $sql = 'SELECT * FROM `gear_users` WHERE ' . $cond;
        $user = $query->getRecord($sql);
        if ($user === false) return false;

        return $user;
    }

    /**
     * Возращает хеш-код восстановления
     * 
     * @param integer $uid идентификатор пользователя
     * @return string
     */
    public static function getRecoveryHash($uid)
    {
        return md5(GFactory::getConfig()->get('HOME') . $uid);
    }

    /**
     * Возращает код подтверждения регистрации
     * 
     * @return string
     */
    public static function getConfirmCode()
    {
        return md5(rand());
    }

    /**
     * Проверка хеш-кода
     * 
     * @param integer $uid идентификатор пользователя
     * @return boolean
     */
    public static function checkHash($hash, $uid)
    {
        return self::getRecoveryHash($uid) == $hash;
    }

    /**
     * Установить код восстановления
     * 
     * @param integer $id идентификатор пользователя
     * @return boolean
     */
    public static function setRecoveryCode($id)
    {
        $code = md5(uniqid(rand(), true));
        $data = array(
            'user_recovery_code' => $code,
            'user_recovery_date' => date('Y-m-d H:i:s')
        );
        if (self::update($data, $id) == false) return false;

        return $code;
    }

    /**
     * Обновляет данные пользователя
     * 
     * @param array $data данные пользователя
     * @param integer $id идентификатор пользователя
     * @return boolean
     */
    public static function update($data, $id)
    {
        return GFactory::getQuery()->update('gear_users', $data, 'user_id', $id);
    }

    /**
     * Сброс пароля пользователя
     * 
     * @param integer $id идентификатор пользователя
     * @return boolean
     */
    public static function resetPassword($id)
    {
        $password = uniqid();
        $data = array(
            'user_password'      => md5($password),
            'user_recovery_date' => null,
            'user_recovery_code' => null
        );
        if (GFactory::getQuery()->update('gear_users', $data, 'user_id', $id) != true)
            return false;
        else
            return $password;
    }

    /**
     * Подтверждение регистрации пользователя
     * 
     * @param integer $code код подтверждения
     * @return boolean
     */
    public static function confirm($code)
    {
        $user = self::getBy($code, 'confirm');
        if ($user == false) return false;

        $data = array(
            'user_confirm_code'  => null,
            'user_confirm_date' => date('Y-m-d H:i:s')
        );

        return GFactory::getQuery()->update('gear_users', $data, 'user_id', $user['user_id']);
    }

    /**
     * Проверка максимального количества зарегистрированных пользователей
     * 
     * @param integer $maximum максимум пользователей
     * @return boolean
     */
    public static function isMaximum($number)
    {
        $sql = "SELECT COUNT(*) `count` FROM `gear_users`";
        $rec =  GFactory::getQuery()->getRecord($sql);
        if ($rec === false) return false;

        return $rec['count'] >= $number;
    }

    /**
     * Есть ли регистрация нескольких пользователей с одного IP адреса
     * 
     * @param string $ip IP адрес пользователя
     * @return boolean
     */
    public static function isOneIp($ip)
    {
        $sql = "SELECT COUNT(*) `count` FROM `gear_users` WHERE `user_account_ip`='$ip'";
        $rec = GFactory::getQuery()->getRecord($sql);
        if ($rec === false) return false;

        return $rec['count'] > 0;
    }

    /**
     * Удаление пользователя
     *
     * @param integer $id идент.пользователя
     * @return void
     */
    public static function delete($id)
    {
        return GFactory::getQuery()->execute('DELETE FROM `gear_users` WHERE `user_id`=' . $id . ' AND `sys_record`<>1');
    }
}
?>