<?php
/**
 * Gear Manager
 *
 * Контроллер вывода кода фрейма
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Controllers
 * @package    Frame
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Frame.php 2016-01-01 21:00:00 Gear Magic $
 */

/**
 * Контроллер вывода кода фрейма
 * 
 * @category   Controllers
 * @package    Frame
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Frame.php 2016-01-01 21:00:00 Gear Magic $
 */
class GController_Frame extends GController
{
    /**
     * Выполнять проверку учетной записи пользователя
     *
     * @var boolean
     */
    public $checkAccount = true;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        $this->response->toJSON = false;
    }

    /**
     * Возвращает дополнительные данные для формирования интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        // соединение с базой данных
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();
    }

    /**
     * Вывод кода фрейма
     * 
     * @return void
     */
    public function frame($resPath = '')
    {}

    /**
     * Инициализация запроса
     * 
     * @return void
     */
    protected function init()
    {
        // метод запроса
        switch ($this->uri->method) {
            // метод "GET"
            case 'GET':
                // тип действия
                switch ($this->uri->action) {
                    // возращает интерфейс контроллера
                    case 'frame':
                        $this->frame('/' . PATH_APPLICATION . $this->resourcePath);
                        return;

                }
                break;
        }

        parent::init();

    }
}?>