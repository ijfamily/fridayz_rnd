<?php
/**
 * Gear Manager
 *
 * Контроллер "Процесс"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Controllers
 * @package    Process
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Process.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Контроллер "Процесс"
 * 
 * @category   Controllers
 * @package    Process
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Process.php 2016-01-01 12:00:00 Gear Magic $
 */
class GController_Process extends GController
{
    /**
     * Обработка данных пользователя
     */
    const TYPE_USER = 2;

    /**
     * Обработка сообщений пользователя
     */
    const TYPE_MAIL = 1;

    /**
     * Обработка сообщений пользователя
     */
    const TYPE_MESSAGE = 3;

    /**
     * Выполнять проверку учетной записи пользователя
     *
     * @var boolean
     */
    public $checkAccount = true;


    /**
     * Вызов процесса на запрос пользователя
     * 
     * @param  string $action действие
     * @return void
     */
    protected function callProcess($action)
    {}

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        // метод запроса
        switch ($this->uri->method) {
            // метод "POST"
            case 'POST': $this->callProcess($this->uri->action); return;
        }

        parent::init();
    }
}
?>