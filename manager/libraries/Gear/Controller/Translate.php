<?php
/**
 * Gear Manager
 *
 * Контроллер обработки запросов перевода текста
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Controllers
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Контроллер обработки запросов перевода текста
 * 
 * @category   Gear
 * @package    Controllers
 * @subpackage Translate
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Translate.php 2016-01-01 12:00:00 Gear Magic $
 */
class GController_Translate extends GController
{
    /**
     * Ключ для соединения с сервером
     *
     * @var string
     */
    protected $_key = '';

    /**
     * URL сервера переводчика
     *
     * @var string
     */
    protected $_url = '';

    /**
     * Название перевода ("ru-en", "en-ru", ....)
     *
     * @var string
     */
    protected $_lang = '';

    /**
     * Ассоц. массив полей с их значениями (которые необходимо перевести)
     *
     * @var array
     */
    protected $_field = array();

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор
     * Используется для инициализации атрибутов контроллер не переданного в конструктор
     * 
     * @return void
     */
    protected function construct()
    {
        // направление перевода
        $this->_lang = $this->_request->getPost('lang');
        // поля для перевода
        $this->_fields = $this->_request->getPost('fields');
    }

    /**
     * Шифровать текст для перевода
     * 
     * @param  array $fields ассоц. массив полей
     * @return string
     */
    protected function encodeText($fields)
    {
        $str = '';
        if (!is_array($fields)) return '';

        foreach ($fields as $key => $value) {
            if (empty($str))
                $str .= $value;
            else
                $str .= '###' . $value;
        }

        return $str;
    }

    /**
     * Расшифровать текст полученнй из перевода
     * 
     * @param  array $fields ассоц. массив полей
     * @param  string $text текст
     * @return string
     */
    protected function decodeText($fields, $text)
    {
        if (!is_array($fields)) return '';

        $str = explode('###', $text);
        $index = 0;
        foreach ($fields as $key => $value) {
            $fields[$key] = $str[$index];
            $index++;
        }

        return $fields;
    }

    /**
     * Соединение с сервером для перевода
     * 
     * @return mixed
     */
    protected function connect()
    {}

    /**
     * Перевод текста
     * 
     * @return void
     */
    protected function translate()
    {
        $this->_response->setMsgResult('Updating data', 'Change is successful' , true);
    }

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        // метод запроса
        switch ($this->_request->method) {
            // метод "POST"
            case GController_Request::METHOD_POST:
                // тип действия
                switch ($this->_request->action) {
                    // перевод
                    case 'translate':
                        $this->translate();
                        return;
                }
                break;
        }

        parent::init();
    }

}


/**
 * Контроллер обработки запросов перевода текста через Яндекс
 * 
 * @category   Gear
 * @package    Controllers
 * @subpackage Translate
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Translate.php 2012-06-21 21:00:00 Gear Magic $
 */
class GController_Translate_Yandex extends GController_Translate
{
    /**
     * Ключ для соединения с сервером
     *
     * @var string
     */
    protected $_key = 'trnsl.1.1.20140116T092611Z.ef9e20c7153aa496.0dc623d72b661229536bd8f21b500b316241ebd3';

    /**
     * URL сервера переводчика
     *
     * @var string
     */
    protected $_url = 'https://translate.yandex.net/api/v1.5/tr.json/translate';

    /**
     * Соединение с сервером для перевода
     * @param  string $text текст перевода
     * @return mixed
     */
    protected function connect($text = '')
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL        => $this->_url,
            CURLOPT_POST       => true,
            CURLOPT_POSTFIELDS => http_build_query(array(
                'key'    => $this->_key,
                'text'   => $text,
                'lang'   => $this->_lang,
                'format' => 'html')
            ),
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
        ));
        $data = curl_exec($curl);
        if ($data === false)
            return array('success' => false, 'error' => 'cURL: ' . curl_error($curl));
        curl_close($curl);

        return array('success' => true, 'data' => json_decode($data, true));
    }

    /**
     * Перевод текста
     * 
     * @return void
     */
    protected function translate()
    {
        parent::translate();

        try {
            // шифруем текст
            $text = $this->encodeText($this->_fields);
            // создаем переводчик Яндекс
            $res = $this->connect($text);
            // если перевод успешен
            if ($res['success']) {
                // результат перевода
                $data = $res['data'];
                // если нет ошибок
                if ($data['code'] == 200) {
                    $this->_response->data = $this->decodeText($this->_fields, $data['text'][0]);
                } else
                    throw new GException('Data processing error', $data['code'] . ': ' . $data['message'], false);
            } else
                throw new GException('Data processing error', $res['error'], false);
        } catch(GException $e) {}
    }
}
?>