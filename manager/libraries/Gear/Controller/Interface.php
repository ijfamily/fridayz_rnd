<?php
/**
 * Gear Manager
 *
 * Контроллер интерфейса компонента
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Controllers
 * @package    Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 21:00:00 Gear Magic $
 */

/**
 * Контроллер формирования интерфейса компонента
 * 
 * @category   Controllers
 * @package    Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 21:00:00 Gear Magic $
 */
class GController_Interface extends GController
{
    /**
     * Компонент для формирования интерфейса ExtJS
     *
     * @var object
     */
    protected $_cmp;

    /**
     * Выполнять проверку учетной записи пользователя
     *
     * @var boolean
     */
    public $checkAccount = true;

    /**
     * Возращает интерфейс компонента
     * 
     * @return void
     */
    protected function getCmpInterface()
    {
        return array($this->_cmp->getInterface());
    }

    /**
     * Возвращает дополнительные данные для формирования интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        // соединение с базой данных
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();
    }

    /**
     * Возращает интерфейс компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        $c = $this->getCmpInterface();
        $this->response->data = $c;
    }
}?>