<?php
/**
 * Gear
 * 
 * Пакет контроллера
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Controllers
 * @package    Controller
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Controller.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Базовый класс контроллера
 * 
 * @category   Controllers
 * @package    Controller
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Controller.php 2016-01-01 12:00:00 Gear Magic $
 */
class GController
{
    /**
     * Идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
     *
     * @var string
     */
    public $accessId = '';

    /**
     * Идентификатор класса контроллера ($_SESSION[$_classId]->...)
     *
     * @var string
     */
    public $classId = '';

    /**
     * Проверка привилегий контроллера
     *
     * @var boolean
     */
    public $checkPrivilege = true;

    /**
     * Выполнять проверку учетной записи пользователя
     *
     * @var boolean
     */
    public $checkAccount = false;

    /**
     * Контроллер ответа
     *
     * @var object
     */
    public $response;

    /**
     * Указатель на экземпляр класса запроса пользователя
     *
     * @var object
     */
    public $uri;

    /**
     * Указатель на экземпляр класс конфигурации системы
     *
     * @var object
     */
    public $config;

    /**
     * Указатель на экземпляр класс языка системы
     *
     * @var object
     */
    public $language;

    /**
     * Указатель на экземпляр класс сессии
     *
     * @var object
     */
    public $session;

    /**
     * Указатель на экземпляр класс сессии контроллера
     *
     * @var object
     */
    public $store;

    /**
     * Текст пакета локализации языков
     *
     * @var array
     */
    public $_;

    /**
     * Доступ к контроллеру
     *
     * @var boolean
     */
    public $disabled = true;

    /**
     * Настройки контроллера
     *
     * @var array
     */
    public $settings;

    /**
     * Если пользователь принадлежит к суперадминам
     *
     * @var boolean
     */
    protected $_isRootUser = false;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        // параметры конфигурации
        $this->config = GFactory::getConfig();
        // запрос клиента
        $this->uri = GFactory::getUri();
        $this->_ = &GText::getSection('controller');
        // default settings
        if (!isset($settings['conName']))
            $settings['conName'] = '';
        // если есть настройки контроллера
        if (!empty($settings))
            // установить идентификатор класса доступа к контроллеру ($_SESSION[$_accessId]['privilege']->...)
            if (empty($this->classId))
                $this->classId = self::genClassId($settings['clsPrefix'], $settings['controller']);
        // если не указан в атрибутах класса
        if (empty($this->accessId))
            $this->accessId = $this->classId;
        $this->language = GLanguage::$instance;
        // сессия
        $this->session = GFactory::getSession();
        // если пользователь суперадмин
        $this->_isRootUser = $this->session->get('group/id') == 1;
        // сессия контроллера
        $this->store = GFactory::getClass('Session_Controller', 'Session', $this->accessId);
        $this->store->active();
        // ответ клиенту
        $this->response = GFactory::getResponse();
        // обработка исключений
        GException::setDetail($this->store->getFrom('trace', 'error'));
        // автозагрузка сценариев
        if (!empty($settings['autoload'])) {
            foreach ($settings['autoload'] as $load) {
                require_once(PATH_COMPONENT . $this->uri->component . '/autoload/' . $load . '.php');
            }
        }
        // настройки контроллера
        $this->settings = $settings;
    }

    /**
     * Возращает название класса контроллера через параметры uri
     * 
     * @param string $clsPrefix префикс класс из настроек
     * @param string $controller название котроллера
     * @param string $action действие котроллера
     * @return string
     */
    public static function genClassName($clsPrefix, $controller, $action)
    {
        return 'GController_' . $clsPrefix . '_' . $controller . '_' . $action;
    }

    /**
     * Возращает идиндификатор контроллера через параметры uri
     * 
     * @param string $clsPrefix префикс класс из настроек
     * @param string $controller название котроллера
     * @return string
     */
    public static function genClassId($clsPrefix ='', $controller = '')
    {
        return 'gcontroller_' . strtolower($clsPrefix) . '_' . $controller;
    }

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор,
     * используется для инициализации атрибутов контроллера без конструктора
     * 
     * @return void
     */
    public function construct()
    {}

    /**
     * Инициализация запроса
     * 
     * @return void
     */
    protected function init()
    {
        // метод запроса
        switch ($this->uri->method) {
            // метод "GET"
            case 'GET':
                // тип действия
                switch ($this->uri->action) {
                    // возращает интерфейс контроллера
                    case 'interface':
                        $this->getInterface();
                        return;

                    default:
                        throw new GException(
                            'Error processing controller',
                            'Unable to initialize the action "%s" controller',
                            $this->uri->action,
                            true,
                            array('icon'      => 'icon-msg-error',
                                  'buttons'   => array('ok' => 'OK', 'yes' => GText::_('Send error report', 'exception')),
                                  'btnAction' => 'send report')
                        );
                        return;
                }
                break;

            default:
                throw new GException(
                    'Error processing controller',
                    'Unable to initialize method "%s" controller',
                    $this->uri->method,
                    true,
                    array('icon'      => 'icon-msg-error',
                          'buttons'   => array('ok' => 'OK', 'yes' => GText::_('Send error report', 'exception')),
                          'btnAction' => 'send report')
                );
                return;
        }
    }

    /**
     * Вывод данных
     * 
     * @return void
     */
    public function render()
    {
        try {
        /*
        try {
            
            // проверка ключа
            if (!TSecurity::checkKey($this->cfg['SECURITY_KEY'])) {
                if (TSecurity::checkAccount()) {
                    GException::setResponse(array('isAuthorized' => false));
                    throw new GException('Error access', 'Error key');
                } else {
                    header('HGTP/1.0 404 Not Found');
                    exit;
                }
            }
            
            // проверка сервера
            if (!TSecurity::checkServer($this->cfg['SERVER/NAME'])) {
                GException::setResponse(array('isAuthorized' => false,
                                              'redirect'     => $this->cfg['PATH/APPLICATION'] . '/error?hack'));
                throw new GException('Controller error', 'Unauthorized access to the system');
            }
            $verify = true;
            if ($this->_verifyAccount)
                $verify = TSecurity::checkAccount();
            // проверка клиента
            if (!$verify) {
                GException::setResponse(array('isAuthorized' => false));
                throw new GException('Controller error', 'You are not authorized or long-absent');
            }
            if ($this->_disabled)
                throw new GException('Controller error', 'Controller is disabled');
        } catch(GException $e) {}
        */
            $this->init();
        } catch(GException $e) {}

        // BUG: for IE
        //header('Content-type: text/html; charset=utf-8');

        echo $this->response->toJSON();
    }
}
?>