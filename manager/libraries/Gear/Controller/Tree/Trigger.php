<?php
/**
 * Gear Manager
 *
 * Триггер дерева
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Controllers
 * @package    Tree
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Trigger.php 2016-01-01 21:00:00 Gear Magic $
 */

/**
 * Триггер дерева
 * 
 * @category   Controllers
 * @package    Tree
 * @subpackage Trigger
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Trigger.php 2016-01-01 21:00:00 Gear Magic $
 */
class GController_Tree_Trigger extends GController
{
    /**
     * Тип триггера
     *
     * @var string
     */
    public $type = '';

    /**
     * Название триггера
     *
     * @var string
     */
    protected $_name = '';

    /**
     * Id выбранного узла
     *
     * @var string
     */
    protected $_node = '';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // название триггера
        $this->_name = $this->uri->getVar('name', '');
        // название узла
        $this->_node = $this->uri->getVar('node', '');
    }

    /**
     * Доступ на вывод данных
     * 
     * @return void
     */
    protected function dataAccessView()
    {
        // проверка доступа к контроллеру класса
        if ($this->checkPrivilege) {
            if (!$this->store->hasPrivilege(array('root', 'select')))
                throw new GException('Error access', 'No privileges to perform this action');
        }
        // соединение с базой данных
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();
        $this->response->set('action', 'select');
    }

    /**
     * Вывод данных в интерфейс компонента
     * 
     * @param  string $name название триггера
     * @param  string $node id выбранного узла
     * @return void
     */
    protected function dataView($name, $node)
    {
        $this->dataAccessView();
    }

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        // метод запроса
        switch ($this->uri->method) {
            // метод "GET"
            case 'GET':
                    if (empty($this->_name) || empty($this->_node))
                        throw new GException('Controller error', 'Controller is disabled');
                    $this->dataView($this->_name, $this->_node);
            return;
        }

        parent::init();
    }
}
?>