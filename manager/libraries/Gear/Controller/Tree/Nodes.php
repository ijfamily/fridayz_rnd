<?php
/**
 * Gear Manager
 *
 * Контроллер обработки узлов дерева
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Controllers
 * @package    Tree
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Nodes.php 2016-01-01 21:00:00 Gear Magic $
 */

Gear::controller('Nodes');

/**
 * Контроллер узлов дерева
 * 
 * @category   Controllers
 * @package    Tree
 * @subpackage Nodes
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Nodes.php 2016-01-01 21:00:00 Gear Magic $
 */
class GController_Tree_Nodes extends GController_Nodes
{}
?>