<?php
/**
 * Gear
 *
 * Контроллер данных списка
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Controllers
 * @package    Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Data');

/**
 * Контроллер данных списка
 * 
 * @category   Controllers
 * @package    Grid
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
class GController_Grid_Data extends GController_Data
{
    /**
     * Тип сортировки
     * (config options - Ext.data.JsonStore.sortInfo.dir)
     *
     * @var string
     */
    public $dir = 'ASC';

    /**
     * Количество записей на странице
     * (config options - Ext.PagingToolbar.pageSize)
     *
     * @var string
     */
    public $limit = 20;

    /**
     * SQL запрос
     *
     * @var string
     */
    public $sql = '';

    /**
     * Формировать умный SQL запрос
     *
     * @var boolean
     */
    protected $sqlSmart = false;

    /**
     * SQL запрос для подчета количества строк
     *
     * @var string
     */
    protected $sqlFoundRows = '';

    /**
     * Сортировка по полю
     * (config options - Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = '';

    /**
     * Массив полей таблицы
     * ("field" => array("type" => "field type", "as" => "table.field", "sort" => "table.field"))
     * 
     * @var array
     */
    protected $fields = array();

    /**
     * Диапазон записей (первая запись)
     *
     * @var integer
     */
    private $_start = 0;

    /**
     * Массив подсчёт сумм по полям
     *
     * @var array
     */
    protected $_dataTally = array('isTallyUp' => 1, 'isRowMenu' => 0);

    /**
     * Записи отображаемые в списке
     * 
     * @var array
     */
    protected $_data = array();

    /**
     * Идентификатор передаваемый из быстрого фильтр списка
     * (из дерева Manager.tree.GridFilter)
     *
     * @var string
     */
    protected $_slidePanel;

    public $countRecords = 0;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    
    {
        parent::__construct($settings);

        // записей на странице
        $this->limit = $this->uri->getVar('limit', $this->limit);
        // сортировать по полю
        $this->sort = $this->uri->getVar('sort', $this->sort);
        // вид сортировки
        $this->dir = $this->uri->getVar('dir', $this->dir);;
        // диапазон записей (первая запись)
        $this->_start = $this->uri->getVar('start', $this->_start);
        // для вложенного списка идент. родителя
        $this->_recordId = $this->uri->getVar('record', 0);
        // значение и идентификатор передаваемый из быстрого фильтр списка
        $this->_slidePanel = array(
            'key'   => $this->uri->getVar('tf', ''),
            'value' => $this->uri->getVar('tfv', '')
        );
    }

    /**
     * Возращает поле сортировик
     * 
     * @return string
     */
    protected function getSortField()
    {
        if (isset($this->fields[$this->sort]['sort']))
            return $this->fields[$this->sort]['sort'];
        else
            return $this->sort;
    }

    /**
     * Возращает sql запрос для быстрого фильтра
     * (для подстановки "%fastfilter" в $sql)
     * 
     * @param string $key идендификатор поля
     * @param string $value значение
     * @return string
     */
    protected function getTreeFilter($key, $value)
    {}

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        parent::dataAccessView();

        $usersData = array();
        // если есть полный доступ
        $isPrvRoot = $this->store->hasPrivilege('root');
        if ($isPrvRoot) {
            // если таблица содержит системные поля
            if ($this->isSysFields) {
                $this->fields[$this->idProperty] = array('type' => 'integer');
                if (!isset($this->fields['sys_record']))
                    $this->fields['sys_record'] = array('type' => 'boolean');
                if (!isset($this->fields['sys_user']))
                    $this->fields['sys_user'] = array('type' => 'integer');
                if (!isset($this->fields['sys_date_update']))
                    $this->fields['sys_date_update'] = array('type' => 'string');
                if (!isset($this->fields['sys_date_insert']))
                    $this->fields['sys_date_insert'] = array('type' => 'string');
                $this->fields['sys_time_update'] = array('type' => 'string');
                $this->fields['sys_time_insert'] = array('type' => 'string');
                $this->fields['sys_user_update'] = array('type' => 'integer');
                $this->fields['sys_user_insert'] = array('type' => 'integer');
                // если есть данные пользователей получаемые динамически
                if ($this->config->get('USERS/PRELOADABLE')) {
                    $usersData = $this->session->get('users/data');
                    // поля: имя, фото
                    $this->fields['sys_user_update_n'] = array('type' => 'string');
                    $this->fields['sys_user_update_p'] = array('type' => 'string');
                    $this->fields['sys_user_insert_n'] = array('type' => 'string');
                    $this->fields['sys_user_insert_p'] = array('type' => 'string');
                }
            }
        }
        // формирование запроса
        $table = new GDb_Table($this->tableName, $this->idProperty, $this->fields);
        $table->setFilter(new GController_Grid_Filter($this->fields, $this->store));
        if ($this->_slidePanel)
            $table->fastFilter = $this->getTreeFilter($this->_slidePanel['key'], $this->_slidePanel['value']);
        $table->setStart($this->_start);
        $table->setLimit($this->limit);
        $table->setSort($this->getSortField(), $this->dir);
        $table->setSmart($this->sqlSmart);
        // запрос
        if ($table->query($this->sql) === false)
            throw new GSqlException();
        $this->countRecords = $table->query->getCountRecords();
        // поля с правом доступа
        $fieldsAccess = $this->store->get('fields');
        // выборка данных
        while (!$table->query->eof()) {
            // предварительная обработка записи запроса
            $record = $this->recordPreprocessing($table->query->next());
            $row = array();
            // список указанный полей
            foreach ($this->fields as $fieldName => $fieldItem) {
                $checkField = true;
                // проверка поля на доступ
                if ($fieldsAccess)
                    $checkField = isset($fieldsAccess[$fieldName]);
                // если поле доступно
                if ($checkField) {
                    //если поле записи cуществует
                    if (isset($record[$fieldName])) {
                        // типизация записи
                        if (isset($fieldItem['toType'])) {
                            $record[$fieldName] = TRecord::toType($record[$fieldName], $fieldItem['toType']);
                        }
                        $row[$fieldName] = $record[$fieldName];
                        // подсчёт суммы по полю
                        if (isset($fieldItem['tallyUp'])) {
                            if (isset($this->_dataTally[$fieldName]))
                                $this->_dataTally[$fieldName] = $this->_dataTally[$fieldName] + $record[$fieldName];
                            else
                                $this->_dataTally[$fieldName] = $record[$fieldName];
                        }
                    } else
                        $row[$fieldName] = '';
                } else
                    $row[$fieldName] = '';
            }
            // если есть полный доступ
            if ($isPrvRoot) {
                // если таблица содержит системные поля
                if ($this->isSysFields) {
                    // данные пользователей получаемые динамически
                    if ($usersData) {
                        // пользователь добавил запись
                        if ($user = GUsers::get($record['sys_user_insert'])) {
                            $row['sys_user_insert_n'] = $user['profile_name'];
                            $row['sys_user_insert_p'] = $user['photo'];
                        } else {
                                $row['sys_user_insert_n'] = '?';
                                $row['sys_user_insert_p'] = 'none_s.png';
                        }
                        // пользователь изменил запись
                        if ($user = GUsers::get($record['sys_user_update'])) {
                            $row['sys_user_update_n'] = $user['profile_name'];
                            $row['sys_user_update_p'] = $user['photo'];
                        } else {
                                $row['sys_user_update_n'] = '?';
                                $row['sys_user_update_p'] = 'none_s.png';
                        }
                    }
                }
            }
            // если есть первичный ключ в записе
            if (isset($record[$this->idProperty]))
                $row[$this->idProperty] = $record[$this->idProperty];
            $this->_data[] = $this->dataPreprocessing($row);
        }
        // если был подсчёт
        if (sizeof($this->_dataTally) > 2) {
            // если записей > 0
            if ($this->_data)
                $this->_data[] = $this->tallyPreprocessing($this->_dataTally);
        }
        // если есть запрос для посчета коли-а записей
        if ($this->sqlFoundRows) {
            $table->query->setTypeRecord(GDb_Query::SQL_NUM);
            $table->query($this->sqlFoundRows);
            $record = $table->query->first();
            $this->response->add('totalCount', $record[0]);
        } else
            $this->response->add('totalCount', $table->query->getFoundRows());
        $this->response->success = true;
        $this->response->data = $this->_data;
    }

    /**
     * Предварительная обработка записи подсчёта по каждому полю
     * 
     * @params array $record запись
     * @return array
     */
    protected function tallyPreprocessing($record)
    {
        return $record;
    }
}


/**
 * Контроллер обработки фильтра списка
 * 
 * @category   Controllers
 * @package    Grid
 * @subpackage Filter
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
class GController_Grid_Filter
{
    /**
     * Массив полей таблицы
     * ("field" => array("type" => "field type", "as" => "table.field", "sort" => "table.field"))
     * 
     * @var array
     */
    protected $_fields = array();

    /**
     * Идент.класс списка
     *
     * @var string
     */
    protected $_classId;

    /**
     * Фильтр
     *
     * @var array
     */
    protected $_filter;

    /**
     * Конструктор
     * 
     * @param  array $_aliasFields массив псевдонимов полей
     * @param  string $_classId идент.класс списка
     * @return void
     */
    public function __construct($_fields = array(), $store)
    {
        $this->_fields = $_fields;
        $this->_filter = isset($_REQUEST['filter']) ? $_REQUEST['filter'] : $this->_filter;
        $this->_store = $store;
    }

    /**
     * Очистить фильтр
     * 
     * @return void
     */
    public function clear()
    {
        $this->_filter = array();
    }

    /**
     * Возращает фильтр
     * 
     * @return array
     */
    public function getFilter()
    {
        return $this->_filter;
    }

    /**
     * Возращает запрос для фильтрации списка из сессии
     * 
     * @return string
     */
    public function getSessionQuery()
    {
        $query = '';
        $filter = $this->_store->get('filter');
        while (list($field, $params) = each($filter)) {
            if (isset($params['as']))
                $field = $params['as'];
            else
                $field = $params['field'];
            $search = trim($params['search']);
            // если есть что искать 
            if (strlen($params['search']) > 0) {
                if (get_magic_quotes_gpc())
                    $search = stripslashes($search);
                $search = mysql_real_escape_string($search);
                // проверка значений для поиска
                // тип "дата"
                if ($params['type'] != 11)
                    if (@preg_match("/([0-9]{1,2})-([0-9]{1,2})-([0-9]{4})/", $search, $regs))
                        $search = $regs[3] . '-' . $regs[2] . '-' . $regs[1];
                // вид поиска
                switch ($params['type']) {
                    // identity
                    case 2:
                        $query .= ' AND ' . $field . "='" . $search . "'";
                        break;

                    // entry
                    case 3:
                        $query .= ' AND ' . $field . " LIKE '%" . $search . "%'";
                        break;

                    // first entry
                    case 4:
                        $query .= ' AND ' . $field . " LIKE '" . $search . "%'";
                        break;

                    // latest entry
                    case 5:
                        $query .= ' AND ' . $field . " LIKE '%" . $search . "'";
                        break;

                    // more (>)
                    case 6:
                        $query .= " AND '" . $search . "'<" . $field;
                        break;

                    // greater than or equal to (=>)
                    case 7:
                        $query .= " AND '" . $search . "'<=" . $field;
                        break;

                    // less (<)
                    case 8:
                        $query .= " AND '" . $search . "'>" . $field;
                        break;

                    // less than or equal to (<=)
                    case 9:
                        $query .= " AND '" . $search . "'>=" . $field;
                        break;

                    // negative (<>)
                    case 10:
                        $query .= ' AND ' . $field . "<>'" . $search . "'";
                        break;

                    // range (from;to)
                    case 11:
                        $s = explode(';', $search);
                        if (count($s) == 2) {
                            //print $s[0];
                            if (@ereg('([0-9]{1,2})-([0-9]{1,2})-([0-9]{4})', $s[0], $regs))
                                $s[0] = $regs[3] . '-' . $regs[2] . '-' . $regs[1];
                            if (@ereg ("([0-9]{1,2})-([0-9]{1,2})-([0-9]{4})", $s[1], $regs))
                                $s[1] = $regs[3] . '-' . $regs[2] . '-' . $regs[1];
                            $query .= ' AND ' . $field . ">='" .$s[0] . "'";
                            $query .= ' AND ' . $field . "<='" . $s[1] . "'";
                        }
                        break;

                    // logic (Yes, No)
                    case 12:
                        $synonyms = array('yes' => 1, 'no' => 0, 'да' => 1, 'нет' => 0);
                        if (isset($synonyms[$search]))
                            $search = $synonyms[$search];
                        else
                            $search = -1;
                        $query .= ' AND ' . $field . '=' . (int)$search; 
                        break;
                }
            // исключение для вида поиска
            } else {
                // вид поиска
                switch ($params['type']) {
                    // есть значение
                    case 13: $query .= ' AND ' . $field . ' IS NULL'; break;

                    // нет значения
                    case 14: $query .= ' AND ' . $field . ' IS NOT NULL'; break;
                }
            }
        }

        return $query;
    }

    /**
     * Возращает готовый SQL запрос для фильтра
     * 
     * @return string
     */
    public function getQuery()
    {
        $where = $qs = '';
        if (is_array($this->_filter)) {
            for ($i = 0; $i < count($this->_filter); $i++) {
                $field = $this->_filter[$i]['field'];
                // поиск по псевдониму поля "настоящие поле"
                if (isset($this->_fields[$field]))
                    if (isset($this->_fields[$field]['as']))
                        $field = $this->_fields[$field]['as'];
                $value = $this->_filter[$i]['data']['value'];
                if ($value) {
                    if (get_magic_quotes_gpc())
                        $value = stripslashes($value);
                    $value = mysql_real_escape_string($value);
                }
                switch($this->_filter[$i]['data']['type']) {
                    case 'string' :
                        $qs .= " AND $field LIKE '%" . $value ."%'";
                        break;
                    case 'list' :
                        if (strstr($value,',')) {
                            $fi = explode(',', $value);
                            for ($q = 0;$q < count($fi);$q++) {
                                $fi[$q] = "'".$fi[$q]."'";
                            }
                            $value = implode(',',$fi);
                            $qs .= " AND $field IN (".$value.")"; 
                            } else {
                                $qs .= " AND $field = '".$value."'"; 
                            }
                        break;
                    case 'boolean' :
                        $qs .= " AND $field = " . ($value);
                        break;
                    case 'numeric' :
                        switch ($this->_filter[$i]['data']['comparison']) {
                            case 'eq' : $qs .= " AND $field = ".$value; break;
                            case 'lt' : $qs .= " AND $field < ".$value; break;
                            case 'gt' : $qs .= " AND $field > ".$value; break;
                        }
                        break;
                    case 'date' :
                        switch ($this->_filter[$i]['data']['comparison']) {
                            case 'eq' :
                                $qs .= " AND $field = '".date('Y-m-d',strtotime($value))."'";
                                break;
                            case 'lt' :
                                $qs .= " AND $field < '".date('Y-m-d',strtotime($value))."'";
                                break;
                            case 'gt' : $qs .= " AND $field > '".date('Y-m-d',strtotime($value))."'";
                                break;
                        }
                        break;
                }
            }
            $where .= $qs;
        }

        return $this->getSessionQuery() . ' ' . $where;
    }

    /**
     * Установка фильтра
     * 
     * @param  array $filter фильтр
     * @return void
     */
    public function init($filter)
    {
        $this->_filter = $filter;
    }
}
?>