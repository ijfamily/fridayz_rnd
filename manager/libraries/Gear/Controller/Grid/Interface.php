<?php
/**
 * Gear Manager
 *
 * Контроллер интерфейса списка
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Controllers
 * @package    Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Interface.php 2016-01-01 21:00:00 Gear Magic $
 */

Gear::ext('Container/Panel/Grid');
Gear::ext('Container/Panel/ButtonGroup');
Gear::controller('Interface');

/**
 * Контроллер интерфейса списка
 * 
 * @category   Controllers
 * @package    Grid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Interface.php 2016-01-01 21:00:00 Gear Magic $
 */
class GController_Grid_Interface extends GController_Interface
{
    /**
     * Первичное поле таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = '';

    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * 
     * @var boolean
     */
    public $isSysFields = false;

    /**
     * Массив полей таблицы ($_tableName)
     * (config options - Ext.data.JsonStore.fields) 
     *
     * @var array
     */
    public $fields = array();

    /**
     * Массив столбцов для создания модели Ext.grid.ColumnModel
     *
     * @var array
     */
    public $columns = array();

    /**
     * Количество записей на странице
     * (config options - Ext.PagingToolbar.pageSize)
     *
     * @var string
     */
    public $limit = 20;

    /**
     * Вид сортировки
     * (config options - Ext.data.JsonStore.sortInfo.dir)
     *
     * @var string
     */
    public $dir = 'ASC';

    /**
     * Сортировка по полю
     * (config options - Ext.data.JsonStore.sortInfo)
     *
     * @var string
     */
    public $sort = '';

    /**
     * Быстрый фильтр для списка ввиде дерева элементов
     * (Manager.tree.GridFilter)
     *
     * @var object
     */
    protected $_slidePanel;

    /**
     * Использовать быстрый фильтр
     *
     * @var boolean
     */
    public $useSlidePanel = false;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // идент. записи родителя (дла вложенного списка)
        $this->_recordId = $this->uri->getVar('record', 0);
        // список (ExtJS class "Manager.grid.GridPanel")
        $this->_cmp = new Ext_Grid_GridPanel();
        // панель инструментов (ExtJS class "Ext.Toolbar")
        $this->_cmp->tbar = new Ext_Container();
        // панель инструментов (ExtJS class "Ext.Toolbar")
        $this->_cmp->bbar = new Ext_Container(
            array('xtype' => 'paging', 'displayInfo' => true, 'cls' => 'x-toolbar-body')
        );
        $this->_cmp->id = $this->classId;
    }

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор,
     * используется для инициализации атрибутов контроллера без конструктора
     * 
     * @return void
     */
    public function construct()
    {
        // быстрый фильтр списка (ExtJS class "Manager.tree.GridFilter")
        $this->_slidePanel = new Ext_Component(array(
            'xtype'       => 'mn-tree-gridfilter',
            'gridId'      => $this->classId,
            'region'      => 'east',
            'minSize'     => 200,
            'maxSize'     => 300,
            'width'       => 200,
            'initLoader'  => array(
                'url'        => ROUTER_SCRIPT . $this->componentUrl . 'tree/trigger/',
                'baseParams' => array(
                    'name' => 'gridFilter'
                )
            ),
            'bodyCssClass' => 'mn-tree-gridfilter',
            'bodyBorder'  => false,
            'collapsed'   => true,
            'initRoot'    => array(),
            'collapsedCls' => 'ddd',
            'collapsible' => true
        ));
    }

    /**
     * Добавление системных полей
     * 
     * @param array $columns столбцы списка
     * @return void
     */
    protected function addSysFields($columns)
    {
        // если есть полный доступ
        if ($this->store->hasPrivilege('root')) {
            GText::add('grid', 'Grid.php');
            $settings = $this->session->get('user/settings');
            // если таблица содержит системные поля
            if ($this->isSysFields) {
                $this->fields[] = array('name' => $this->idProperty, 'type' => 'integer');
                $this->fields[] = array('name' => 'sys_record', 'type' => 'boolean');
                $this->fields[] = array('name' => 'sys_user', 'type' => 'integer');
                $this->fields[] = array('name' => 'sys_date_update', 'type' => 'string');
                $this->fields[] = array('name' => 'sys_date_insert', 'type' => 'string');
                $this->fields[] = array('name' => 'sys_time_update', 'type' => 'string');
                $this->fields[] = array('name' => 'sys_time_insert', 'type' => 'string');
                $this->fields[] = array('name' => 'sys_user_update', 'type' => 'integer');
                $this->fields[] = array('name' => 'sys_user_insert', 'type' => 'integer');
                // если есть данные пользователей получаемые динамически
                if ($this->config->get('USERS/PRELOADABLE')) {
                    // поля: имя, фото
                    $this->fields[] = array('name' => 'sys_user_insert_n', 'type' => 'string');
                    $this->fields[] = array('name' => 'sys_user_insert_p', 'type' => 'string');
                    $this->fields[] = array('name' => 'sys_user_update_n', 'type' => 'string');
                    $this->fields[] = array('name' => 'sys_user_update_p', 'type' => 'string');
                }
                $colId = array(
                    'dataIndex' => $this->idProperty,
                    'header'    => '<em class="mn-grid-hd-id"></em>' . GText::get('Id column', 'grid'),
                    'cls'       => 'mn-grid-col-sys',
                    'hidden'    => true,
                    'width'     => 55,
                    'sortable'  => true,
                    'filter'    => array('type' => 'numeric'));
                $colDateInsert = array(
                    'xtype'     => 'datetimecolumn',
                    'dataIndex' => 'sys_date_insert',
                    'timeIndex' => 'sys_time_insert',
                    'userIndex' => 'sys_user_insert',
                    'frmDate'   => $settings['format/date'],
                    'frmTime'   => $settings['format/time'],
                    'header'    => GText::get('Date insert column', 'grid'),
                    'hidden'    => true,
                    'isSystem'  => true,
                    'urlQuery'  => '?state=info',
                    'url'       => 'administration/users/contingent/' . ROUTER_DELIMITER . 'profile/interface/',
                    'width'     => 130,
                    'sortable'  => true,
                    'filter'    => array('type' => 'date'));
                $colDateUpdate = array(
                    'xtype'     => 'datetimecolumn',
                    'dataIndex' => 'sys_date_update',
                    'timeIndex' => 'sys_time_update',
                    'userIndex' => 'sys_user_update',
                    'frmDate'   => $settings['format/date'],
                    'frmTime'   => $settings['format/time'],
                    'header'    => GText::get('Date update column', 'grid'),
                    'isSystem'  => true,
                    'urlQuery'  => '?state=info',
                    'url'       => 'administration/users/contingent/' . ROUTER_DELIMITER . 'profile/interface/',
                    'hidden'    => true,
                    'width'     => 130,
                    'sortable'  => true,
                    'filter'    => array('type' => 'date'));
                $colSysRecord = array(
                    'xtype'     => 'booleancolumn',
                    'dataIndex' => 'sys_record',
                    'header'    => GText::get('System column', 'grid'),
                    'hidden'    => true,
                    'width'     => 80,
                    'sortable'  => true,
                    'filter'    => array('type' => 'boolean'));

                $count = sizeof($columns);
                $isAdded = false;
                for ($i = 0; $i < $count; $i++) {
                    $xtype = empty($columns[$i]['xtype']) ? false : $columns[$i]['xtype'];
                    if ($xtype !== false)
                        if ($xtype == 'expandercolumn' || $xtype == 'numbercolumn' || $xtype == 'rowmenu')
                            $xtype = true;
                    if ($xtype !== true)
                        if (!$isAdded) {
                            array_push($result, $colId, $colDateInsert, $colDateUpdate);
                            $isAdded = true;
                        }
                    $result[] = $columns[$i];
                }
                return $result;
            }
        }

        return $columns;
    }

    /**
     * Возвращает интерфейс компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        $this->columns = $this->addSysFields($this->columns);

        // Ext_Grid_GridPanel (ExtJS class "Manager.grid.GridPanel")
        //$grid = $this->_component;
        $this->restoreController($this->columns, $this->limit);
        
        $this->_cmp->columns = $this->getColumns();
        if ($this->_cmp->isReadOnly)
            $this->_cmp->isUseRowEditor = false;
        if  (!$this->_cmp->iconSrc)
            $this->_cmp->iconSrc = $this->resourcePath . 'icon.png';

        // Ext_Data_Store (ExtJS property "Manager.grid.GridPanel.store")
        $this->_cmp->store->idProperty = $this->idProperty;
        $this->_cmp->store->baseParams = $this->getBaseParams();
        $this->_cmp->store->fields = $this->fields;
        $this->_cmp->store->sortInfo = array('field' => $this->sort, 'direction' => $this->dir);

        // установить кол-о записей на странице (ExtJS property "Manager.grid.GridPanel.pageSize")
        //$bbar = $grid->getBottomToolbar();
        if ($this->_cmp->bbar != null)
            $this->_cmp->bbar->pageSize = $this->limit;

        // если есть полный доступ
        if ($this->store->hasPrivilege('root')) {
            // если таблица содержит системные поля
            if ($this->isSysFields) {
                // если есть данные пользователей получаемые динамически
                if ($this->config->get('USERS/PRELOADABLE')) {
                    $_ = GText::getSection('grid');
                    // вывод данных пользователей которые изменили записи
                    $cellTips = $this->_cmp->cellTips;
                    $params = $this->config->get('PHOTO');
                    $img = '<div class="icon-photo-s" style="background-image: url(\'{sys_user_update_p}\')"></div>';
                    // пользователь изменил запись
                    $cellInfo =
                        '<div class="mn-grid-cell-tooltip-tl">' . $_['Update record'] . '{sys_user_update_n}</div>'
                      . '<table class="mn-grid-cell-tooltip" cellpadding="0" cellspacing="5" border="0">'
                      . '<tr><td valign="top">' . $img . '</td><td valign="top" style="width:200px">'
                      . '<em>' . $_['Date update'] . '</em>: <b>{sys_date_update}</b><br>'
                      . '<em>' . $_['Time update'] . '</em>: <b>{sys_time_update}</b>'
                      . '</td></tr></table>';
                    $cellTips[] = array('field' => 'sys_date_update', 'tpl' => $cellInfo);
                    // пользователь добавил запись
                    $img = '<div class="icon-photo-s" style="background-image: url(\'{sys_user_insert_p}\')"></div>';
                    $cellInfo =
                        '<div class="mn-grid-cell-tooltip-tl">' . $_['Insert record'] . '{sys_user_insert_n}</div>'
                      . '<table class="mn-grid-cell-tooltip" cellpadding="0" cellspacing="5" border="0">'
                      . '<tr><td valign="top">' . $img . '</td><td valign="top" style="width:200px">'
                      . '<em>' . $_['Date insert'] . '</em>: <b>{sys_date_insert}</b><br>'
                      . '<em>' . $_['Time insert'] . '</em>: <b>{sys_time_insert}</b>'
                      . '</td></tr></table>';
                    $cellTips[] = array('field' => 'sys_date_insert', 'tpl' => $cellInfo);
                    $this->_cmp->cellTips = $cellTips;
                }
            }
        }
        $this->_cmp->region = 'center';

        parent::getInterface();
    }

    /**
     * Возвращает сохраненные столбцы
     * 
     * @return array
     */
    protected function getStoreController()
    {
        try {
            // соединения с базой данных
            GFactory::getDb()->connect();
            $query = new GDb_Query(); 
            $sql = 'SELECT * FROM `gear_controller_store` WHERE '
                 . '`controller_id`=\'' . $this->classId . '\' AND `user_id`=' . $_SESSION['user_id'];
            if (($rec = $query->getRecord($sql)) === false)
                throw new GException('Data processing error', 'Query error (Internal Server Error)', $query->getError(false), false);
            if (empty($rec))
                return array();
            else
                if (!empty($rec['columns'])) {
                    $store = json_decode($rec['columns'], true);
                    return array(
                        'columns' => $store['columns'],
                        'limit' => $store['toolbar']['pageSize']
                    );
                }
        } catch(GException $e) {}

        return array();
    }

    /**
     * Возвращает поля таблицы
     * 
     * @return void
     */
    protected function getColumns()
    {
        $fields = $this->store->get('fields');
        if ($fields) {
            $columns = array();
            $c = sizeof($this->columns);
            for ($i = 0; $i < $c; $i++) {
                // if column has another xtype
                if (isset($this->columns[$i]['dataIndex'])) {
                    if (key_exists($this->columns[$i]['dataIndex'], $fields))
                        $columns[] = $this->columns[$i];
                } else
                    $columns[] = $this->columns[$i];
            }
            return $columns;
        }

        return $this->columns;
    }

    /**
     * Обновляет столбцы таблицы
     * 
     * @param  array $columns столбцы таблицы
     * @return void
     */
    protected function restoreController($columns, $limit)
    {
        // список сохраненных столбцов
        $store = $this->getStoreController();
        // если список есть
        if (!empty($store)) {
            $storeCols = $store['columns'];
            $newLimit = $store['limit'];
            $newCols = array();
            $count = sizeof($storeCols);
            for ($i = 0; $i < $count; $i++) {
                $stateIndex = $storeCols[$i]['stateIndex'];
                if (isset($columns[$stateIndex])) {
                    $columns[$stateIndex]['stateIndex'] = $stateIndex;
                    $columns[$stateIndex]['width'] = $storeCols[$i]['width'];
                    $columns[$stateIndex]['hidden'] = $storeCols[$i]['hidden'] ? true : false;
                    $newCols[] = $columns[$stateIndex];
                }
            }
        } else {
            $newCols = $columns;
            $newLimit = $limit;
            $c = sizeof($newCols);
            for ($i = 0; $i < $c; $i++)
                $newCols[$i]['stateIndex'] = $i;
        }

        // обновить
        $this->columns = $newCols;
        $this->limit = $newLimit;
    }

    /**
     * Массив содержащий свойства, который отправляется
     * в качестве параметров для каждого запроса HGTP (Ext.data.JsonStore.baseParams)
     * 
     * @return array
     */
    protected function getBaseParams()
    {
        if ($this->uri->id) {
            $this->store->set('record', $this->uri->id);
            return array('record' => $this->uri->id);
        }

        return null;
    }

    /**
     * Проверка существования фильтра в сессии для списка
     * 
     * @return boolean
     */
    protected function isSetFilter()
    {
        if (($filter = $this->store->get('filter', false)) === false) return false;

        while (list($field, $params) = each($filter))
            if ($params['type'] > 1 && !empty($params['search'])) return true;

        return false;
    }

    /**
     * Возращает интерфейс компонента
     * 
     * @return void
     */
    protected function getCmpInterface()
    {
        if ($this->useSlidePanel) {
            // вкладка
            $tab = new Ext_Panel(array(
                'title'         => $this->_['title_grid'],
                'iconSrc'       => $this->resourcePath . 'icon.png',
                'iconTpl'       => $this->resourcePath . 'shortcut.png',
                'titleEllipsis' => 40,
                'layout'        => 'fit',
                'closable'      => true,
                'component'     => array('container' => 'content-tab', 'destroy' => true)
            ));
            if ($this->_cmp->titleTpl) {
                $tab->tabTip = $this->_cmp->title;
                $tab->titleTpl = $this->_cmp->titleTpl;
            }
            unset($this->_cmp->title);
            // контейнер
            $tab->items->add(array(
                'xtype'    => 'container',
                'layout'   => 'border',
                'defaults' => array('collapsible' => true, 'split' => true),
                'items'    => array($this->_slidePanel->getInterface(), $this->_cmp->getInterface())
            ));

            return array($tab->getInterface());
        } else
            return array($this->_cmp->getInterface());
    }
}?>