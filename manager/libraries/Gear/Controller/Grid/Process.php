<?php
/**
 * Gear Manager
 *
 * Контроллер процесса обработки списка
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Controllers
 * @package    Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Process.php 2016-01-01 21:00:00 Gear Magic $
 */

/**
 * Контроллер интерфейса списка
 * 
 * @category   Controllers
 * @package    Grid
 * @subpackage Process
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Process.php 2016-01-01 21:00:00 Gear Magic $
 */
class GController_Grid_Process extends GController
{
    /**
     * Массив столбцов для создания модели Ext.grid.ColumnModel
     *
     * @var array
     */
    public $list = array();

    /**
     * Количество записей на странице
     * (config options - Ext.PagingToolbar.pageSize)
     *
     * @var string
     */
    public $index = -1;

    /**
     * Идент. списка
     *
     * @var string
     */
    public $gridId = '';

    /**
     * Запросы пользователей через форму
     *
     * @var object
     */
    public $input;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        $this->input = GFactory::getInput();
        $this->list = $this->store->get('process', false);
        $this->index = $this->input->get('proIndex', -1);
    }

    /**
     * Начало обработки
     * 
     * @return void
     */
    protected function start()
    {
        $this->response->add('setTo', array('id' => $this->gridId, 'func' => 'process', 'args' => array(0)), true);
    }

    /**
     * Конец обработки
     * 
     * @return void
     */
    protected function end()
    {}

    /**
     * Обработка элемента списка
     * 
     * @param array $item
     * @return void
     */
    protected function itemProcess($item = array())
    {}

    /**
     * Если конец обработки
     * 
     * @return boolean
     */
    protected function isOver()
    {
        return !(sizeof($this->list) -1 > $this->index);
    }

    /**
     * Процесс обработки
     * 
     * @return boolean
     */
    protected function process()
    {
        $this->itemProcess($this->list[$this->index]);

        if ($this->isOver())
            $this->end();
        else
            $this->response->add('setTo', array('id' => $this->gridId, 'func' => 'process', 'args' => array($this->index + 1)), true);
    }

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        if ($this->uri->action == 'data')
            // метод запроса
            switch ($this->uri->method) {
                // метод "POST"
                case 'POST': $this->start(); return;

                // метод "PROCESS"
                case 'PROCESS': $this->process(); return;
            }

        parent::init();
    }
}?>