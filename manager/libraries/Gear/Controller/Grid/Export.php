<?php
/**
 * Gear Manager
 *
 * Контроллер экспорта данных списка
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Controllers
 * @package    Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Export.php 2012-09-22 21:00:00 Gear Magic $
 */

/**
 * Контроллер экспорта данных списка
 * 
 * @category   Controllers
 * @package    Grid
 * @subpackage Export
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Export.php 2012-09-22 21:00:00 Gear Magic $
 */
class GController_Grid_Export extends GController
{
    /**
     * Идентификатор класс списка
     *
     * @var string
     */
    protected $_gridId = '';

    /**
     * SQL запрос
     *
     * @var string
     */
    protected $_sql = '';

    /**
     * Поля таблицы (GDb_Table)
     * (config options - Ext.data.JsonStore.fields) 
     *
     * @var array
     */
    protected $_fields = array();

    /**
     * Ссылка на базу данных
     *
     * @var mixed
     */
    protected $_db;

    /**
     * Журнал действий пользователя (GDb_Table_Log)
     *
     * @var mixed
     */
    protected $_log;

    /**
     * Название таблицы
     *
     * @var string
     */
    protected $_tableName = '';

    /**
     * Загаловок посылаемый браузеру
     *
     * @var boolean
     */
    protected $_header = true;

    /**
     * Вид эскпорта
     *
     * @var string
     */
    protected $_type = '';

    /**
     * Виды загаловков
     *
     * @var string
     */
    protected $_contentType = array(
        'excel'      => array('application' => 'vnd.ms-excel', 'file' => '.xls'),
        'powerpoint' => array('application' => 'vnd.ms-powerpoint', 'file' => '.ppt'),
        'word'       => array('application' => 'msword', 'file' => '.doc'),
        'download'   => array('application' => 'force-download', 'file' => '.html')
    );

    /**
     * Конструктор
     * 
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        // инициализация базы данных
        $this->_db = GDb::getInstance(TConfig::getSettings);
        // журнал действий пользователей
        $this->_log = new GDb_Table_Log();
        $this->_log->setActive(TStore_Controller::isLog());
    }

    /**
     * Этот метод вызывается сразу после создания контроллера через маршрутизатор
     * Используется для инициализации атрибутов контроллер не переданного в конструктор
     * 
     * @return void
     */
    protected function construct()
    {
        // вид экспорта
        $this->_type = $this->_request->getGet('type');
    }

    /**
     * Приминение параметров к эскпорту списка
     * 
     * @return void
     */
    protected function dataAccept()
    {
        // хранилище списка
        $store = $this->_request->getPost('store');
        // поля списка
        $fields = $this->_request->getPost('fields');
        // если невозможно получить поля из запроса
        if (empty($fields) || empty($store) || empty($this->_gridId)) return;
        $fields = $this->dataIntersect($fields, $this->_fields);
        // экспорт
        TStore_Controller::setVar('export', array('fields' => $fields, 'store' => $store), $this->_gridId);
    }

    /**
     * Возращает данные полученнные при пересечении полей
     * 
     * @param  array $data данные записи
     * @param  array $fields поля контроллера
     * @return array
     */
    protected function dataIntersect($data, $fields)
    {
        $result = array();
        $count = sizeof($fields);
        for ($i = 0; $i < $count; $i++) {
            if (key_exists($fields[$i], $data))
                $result[$fields[$i]] = $data[$fields[$i]];
        }

        return $result;
    }

    /**
     * Эспорт данных
     * 
     * @return void
     */
    protected function dataExport()
    {
        GException::setStop(true);
        // определения вида эскпорта
        if (isset($this->_contentType[$this->_type]))
            $ct = $this->_contentType[$this->_type];
        else
            $ct = $this->_contentType['download'];

        $fileName = 'Export_' . uniqid() . $ct['file'];
        if ($this->_header) {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private", false);
            header('Content-Type: application/' . $ct['application']);
            header("Content-Disposition: attachment; filename=\"". $fileName ."\";" );
        }

        $this->dataView();

        // запись в журнал действий пользователя
        $this->_log->add(
            array('log_action'       => GDb_Table_Log::LOG_EXPORT,
                  'controller_id'    => TStore_Controller::getControllerId(self::$_accessId),
                  'controller_class' => self::$_accessId,
                  'log_query_params' => json_encode(array('file' => $fileName)))
        );

    }

    /**
     * Предварительная обработка записи
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        return $record;
    }

   /**
     * Вывод данных
     * 
     * @return void
     */
    protected function dataView()
    {
        // соединениа с базой данных
        $this->_db->reConnect();
        // данные для экспорта
        $export = TStore_Controller::getVar('export');
?>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
        <title><?php print $this->_title;?></title>
    </head>
    <body bgcolor="white">
        <style>
            table {
                font-family:"Times New Roman", serif;
                font-size: 12pt;
            }
            td {
                border: 0.5pt solid black;
                padding: 2pt;
            }
        </style>
<?php
        print '<span style="font-weight:bold;font-size:14px;padding:2pt;font-family:Times New Roman, serif">' . $this->_title . '<br></span><br>';
?>
        <table>
<?php
        if ($export == false) exit('data was corrupted!');
        // формирование загаловка списка
        $fields = $export['fields'];
        print '<tr style="font-weight: bold;">';
        foreach($fields as $field => $params) {
            print '<td style="white-space:nowrap;width:' . $params['width'] . 'pt">';
            print  $params['header'];
            print '</td>';
        }
        print '</tr>';
        // формирование тела списка
        $store = $export['store'];
        $table = new GDb_Table_Custom($this->_tableName, '', $this->_fields);
        $table->setFilter(new GController_Grid_Filter(array(), $this->_gridId));
        $table->setTypeRecord(GDb_Query::SQL_ASSOC);
        $table->setStart($store['start']);
        $table->setLimit($store['limit']);
        $table->setSort($store['field'], $store['direction']);
        $table->query($this->_sql);
        while (!$table->eof()) {
            $record = $this->dataPreprocessing($table->next());
            
            print '<tr>';
            reset($fields);
            foreach($fields as $field => $params) {
                if (isset($record[$field])) {
                    print '<td align="left" valign="top">';
                    print $record[$field];
                    print '</td>';
                } else
                    print '<td>&nbsp;</td>';
            }
            print '</tr>' . "\r\n";
        }
?>
        </table>
    </body>
</html>
<?php
    }

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        // метод запроса
        switch ($this->_request->method) {
            // тип действия
            case GController_Request::METHOD_POST:
                // действие
                if ($this->_request->action == 'export') {
                    $this->dataAccept();
                    return;
                }
            break;

            // метод "GET"
            case GController_Request::METHOD_GET:
                // действие
                if ($this->_request->action == 'export') {
                    $this->dataExport();
                    exit;
                }
            break;
        }

        parent::init();
    }
}?>