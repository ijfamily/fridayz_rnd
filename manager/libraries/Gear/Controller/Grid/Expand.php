<?php
/**
 * Gear
 *
 * Контроллер развёртывания записи списка
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Grid
 * @copyright  Copyright (c) 2013-2014 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Expand.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Data');

/**
 * Контроллер развёртывания записи списка
 * 
 * @category   Gear
 * @package    Grid
 * @subpackage Expand
 * @copyright  Copyright (c) 2013-2014 <gearmagic.ru@gmail.com>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
class GController_Grid_Expand extends GController_Data
{
    /**
     * SQL запрос
     *
     * @var string
     */
    public $sql = '';

    /**
     * Столбцы (id => field)
     *
     * @var array
     */
    public $columns = array();

    /**
     * Возращает SQL запрос
     * 
     * @return string
     */
    protected function getQuery()
    {
        return '';
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataExpand()
    {
        parent::dataAccessView();

        $query = new GDb_Query();
        $this->_data = $query->getRecord($this->getQuery());
        if ($this->_data === false)
            throw new GSqlException();
        if (sizeof($this->_data) == 0)
            throw new GSqlException();

        $this->response->data = $this->dataPreprocessing($this->getColumns($this->_data));
    }

    /**
     * Возращает данные по столбцам
     * 
     * @param  array $data данные запроса
     * @return void
     */
    protected function getColumns($data)
    {
        $cols = array();
        foreach ($this->columns as $id => $field) {
            if (!empty($data[$field])) {
                $cols[$id] = $data[$field];
            }
        }

        return $cols;
    }

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        // метод запроса
        switch ($this->uri->method) {
            // метод "POST"
            case 'POST':
                if ($this->uri->action == 'expand') {
                    $this->dataExpand();
                }
                break;
        }
    }
}
?>