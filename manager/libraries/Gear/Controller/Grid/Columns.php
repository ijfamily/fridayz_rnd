<?php
/**
 * Gear
 *
 * Контроллер столбцов списка
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Controllers
 * @package    Grid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Columns.php 2016-01-01 21:00:00 Gear Magic $
 */

Gear::controller('Data');

/**
 * Контроллер обработки столбцов списка
 * 
 * @category   Controllers
 * @package    Grid
 * @subpackage Columns
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Columns.php 2016-01-01 21:00:00 Gear Magic $
 */
class GController_Grid_Columns extends GController_Data
{
    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = 'gear_controller_store';

    /**
     * Первичное поле таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = 'store_id';

    /**
     * Доступ на удаление данных
     * 
     * @return void
     */
    protected function dataAccessDelete()
    {
        // проверка доступа к контроллеру класса
        if ($this->checkPrivilege)
            if (!$this->store->hasPrivilege(array('root', 'delete'))) {
                $this->response->add('icon', 'icon-msg-access');
                throw new GException('Error access', 'No privileges to perform this action');
            }
        // соединение с базой данных
        GFactory::getDb()->connect();
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($this->uri->id, 'id');
        }
        $this->response->set('action', 'delete');
        $this->response->setMsgResult('Deleting data', 'Is successful record deletion!', true);
    }

    /**
     * Удаление данных
     * 
     * @return void
     */
    protected function dataDelete()
    {
        $this->dataAccessDelete();

        $query = new GDb_Query();
        $sql = 'DELETE FROM ' . $this->tableName 
             . ' WHERE `controller_id`=\'' . $this->classId . '\' AND `user_id`=' .  $this->session->get('user_id');
        if ($query->execute($sql) === false)
            throw new GSqlException();
        GText::add('grid', 'Grid.php');
        $this->response->setMsgResult(GText::get('grid', 'grid'), GText::get('Successfully columns restored!', 'grid'), true);
    }

    /**
     * Доступ на обновление данных
     * 
     * @return void
     */
    protected function dataAccessUpdate()
    {
        // проверка доступа к контроллеру класса
        if ($this->checkPrivilege)
            if (!$this->store->hasPrivilege(array('root', 'update'))) {
                $this->response->add('icon', 'icon-msg-access');
                throw new GException('Error access', 'No privileges to perform this action');
            }
        // соединение с базой данных
        GFactory::getDb()->connect();
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($this->uri->id, 'id');
        }
        $this->response->set('action', 'update');
        $this->response->setMsgResult('Updating data', 'Change is successful' , true);
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        $this->dataAccessUpdate();

        $query = new GDb_Query();
        $input = GFactory::getInput();
        $wSQL = '`controller_id`=\'' . $this->classId . '\' AND `user_id`=' . $this->session->get('user_id');
        $sql = 'SELECT COUNT(*) `count` FROM `gear_controller_store` WHERE ' . $wSQL;
        $rec = $query->getRecord($sql);
        if ($rec === false)
            throw new GSqlException();
        $count  = 0;
        if (!empty($rec))
            $count = $rec['count'];
        if ($count > 0) {
            $sql = 'UPDATE `gear_controller_store` SET `columns`=' . $query->escapeStr($input->get('columns')) . ' WHERE ' . $wSQL;
            if ($query->execute($sql) === false)
                throw new GSqlException();
        } else {
            $table = new GDb_Table($this->tableName, $this->idProperty);
            $params = array(
                'columns'       => $input->get('columns'),
                'controller_id' => $this->classId,
                'user_id'       => $this->session->get('user_id')
            );
            if ($table->insert($params) === false)
                throw new GSqlException();
        }
        GText::add('grid', 'Grid.php');
        $this->response->set('action', 'columns');
        $this->response->setMsgResult(GText::get('grid', 'grid'), GText::get('Successfully columns stored!', 'grid'), true);
    }

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        // метод запроса
        switch ($this->uri->method) {
            // метод "PUT"
            case 'PUT':
                // действие
                if ($this->uri->action == 'columns') {
                    $this->dataUpdate();
                    return;
                }
                break;

            // метод "DELETE"
            case 'DELETE':
                // действие
                if ($this->uri->action == 'columns') {
                    $this->dataDelete();
                    return;
                }
                break;
        }

        parent::init();
    }
}
?>