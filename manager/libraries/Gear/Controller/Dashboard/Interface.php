<?php
/**
 * Gear
 *
 * Контроллер интерфейса доски компонента
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Controllers
 * @package    Dashboard
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 21:00:00 Gear Magic $
 */

/**
 * Контроллер интерфейса доски компонента
 * 
 * @category   Controllers
 * @package    Dashboard
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 21:00:00 Gear Magic $
 */
class GController_Dashboard_Interface extends GController
{
    /**
     * Компонент для формирования интерфейса ExtJS
     *
     * @var array
     */
    protected $_component;

    /**
     * Возращает дополнительные данные для формирования интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        // соединение с базой данных
        GFactory::getDb()->connect();

        return array();
    }

    /**
     * Возращает интерфейс компонента
     * 
     * @return void
     */
    public function getInterface()
    {
        $data = $this->getDataInterface();

        if (empty($this->_component['title']))
            $this->_component['title'] = $this->_['title_dashboard'];

        ob_start();
        $this->content($data);
        $this->_component['html'] = ob_get_contents();
        $this->_component['cls'] = 'mn-dashboard-item';
        ob_end_clean();

        return $this->_component;
    }

    /**
     * Вывод контента
     * 
     * @param  array $data данные контента
     * @return void
     */
    protected function content($data)
    {}
}?>