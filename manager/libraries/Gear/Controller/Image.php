<?php
/**
 * Gear Manager
 *
 * Контроллер обработки изображений
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Controllers
 * @package    Image
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Image.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('File');
Gear::library('Image');

/**
 * Базовый класс обработки изображений
 * 
 * @category   Controllers
 * @package    Image
 * @subpackage Base
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Image.php 2016-01-01 12:00:00 Gear Magic $
 */
class GController_Image_Base extends GController
{
    /**
     * Каталог изображений
     *
     * @var string
     */
    public $pathData = '';

    /**
     * Название поля для загрузки изображения
     *
     * @var string
     */
    public $field = '';

    /**
     * Сгенерируемое имя файла полученное после его загрузки
     *
     * @var string
     */
    public $filename = '';

    /**
     * Доступные расширения файлов для загрузки
     *
     * @var array
     */
    public $upload = array('jpg', 'jpeg', 'png', 'gif');

    /**
     * Запросы пользователей через форму
     *
     * @var object
     */
    public $input;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        $this->input = GFactory::getInput();
    }

    /**
     * Генерирование названия файла
     * 
     * @return string
     */
    protected function genFilename()
    {
        return 'tmp_' . uniqid();
    }

    /**
     * Загрузка изображения
     * 
     * @return void
     */
    protected function imageUpload()
    {
        $this->response->set('action', 'update');
        $this->response->setMsgResult('Updating data', 'Change is successful' , true);
        // идент. название файла
        $fileId = $this->genFilename();
        // если выбран флажок "Профиль для изменения изображения после загрузки"
        $cmdImage = '';
        $fileImage = false;
        // если нет изображения для загрузки
        if ($this->input->file->has($this->field) === false)
            throw new GException('Adding data', $this->_['msg_file_image_empty']);
        $fileImage = $this->input->file->get($this->field);
        // определение имени файла
        $fileExt = strtolower(pathinfo($fileImage['name'], PATHINFO_EXTENSION));
        $this->filename = $fileId . '.' . $fileExt;
        // загрузка изображения
        GFile::upload($fileImage, $this->pathData . $this->filename, $this->upload);
        // удаляем ранее добавленное изображение
        $filename = $this->store->get('upload', $this->accessId);
        if ($filename)
            GFile::delete($this->pathData . $filename, false);
        $this->store->set('upload', $this->filename, $this->accessId);

        $this->response->data = array('file' => $this->filename);
    }

    /**
     * Удаление загруженного изображения
     * 
     * @return void
     */
    protected function imageClear()
    {
        $this->response->set('action', 'delete');
        $this->response->setMsgResult('Deleting data', 'Is successful record deletion!', true);

        // если изображение было ранее загружено для этого профиля, тогда удаляем
        $filename = $this->store->get('upload', $this->accessId);
        if ($filename)
            GFile::delete($this->pathData . $filename);
        // обновить сессию
        $this->store->set('upload', '', $this->accessId);
    }

    /**
     * Удаление изображения профиля
     * 
     * @return void
     */
    protected function imageDelete()
    {
        $this->response->set('action', 'delete');
        $this->response->setMsgResult('Deleting data', 'Is successful record deletion!', true);
    }

    /**
     * Обновить изображения
     * 
     * @return void
     */
    protected function imageUpdate()
    {
        $this->response->set('action', 'update');
        $this->response->setMsgResult('Updating data', 'Change is successful' , true);
    }

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        // метод запроса
        switch ($this->uri->method) {
            // метод "PUT"
            case 'PUT': $this->imageUpdate(); return;

            // метод "DELETE"
            case 'DELETE': $this->imageDelete(); return;

            // метод "CLEAR"
            case 'CLEAR':  $this->imageClear(); return;

            // метод "POST"
            case 'POST': $this->imageUpload(); return;
        }

        parent::init();
    }
}
?>