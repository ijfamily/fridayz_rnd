<?php
/**
 * Gear Manager
 *
 * Контроллер интерфейса списка дерева
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Controllers
 * @package    Treegrid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::component('Tree/TreeLoader');
Gear::component('Container/Panel/TreeGrid');
Gear::component('Container/Panel/ButtonGroup');
Gear::controller('Interface');

/**
 * Контроллер интерфейса списка дерева
 * 
 * @category   Controllers
 * @package    Treegrid
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Interface.php 2016-01-01 12:00:00 Gear Magic $
 */
class GController_Treegrid_Interface extends GController_Interface
{
    /**
     * Массив столбцов для создания модели Ext.tree.Column
     *
     * @var array
     */
    protected $_columns = array();

    /**
     * Конструктор
     * 
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        // Ext_Tree_TreeGrid (ExtJS class "Ext.ux.tree.TreeGrid")
        $this->_component = new Ext_Tree_TreeGrid();
        // Ext_Container (ExtJS class "Ext.Toolbar")
        $this->_component->setTopToolbar(new Ext_Container());
        // Ext_Tree_TreeGridLoader (ExtJS class "Ext.ux.tree.TreeGridLoader")
        $this->_component->setLoader(new Ext_Tree_TreeGridLoader());
        $this->_component->setProperty('id' , GController::getAccessId());
    }

    /**
     * Возращает интерфейс компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // Ext_Tree_TreeGrid (ExtJS class "Ext.ux.tree.TreeGrid")
        $treeGrid = $this->_component;
        $treeGrid->setProperty('columns', $this->_columns);

        parent::getInterface();
    }

}?>