<?php
/**
 * Gear Manager
 *
 * Контроллер файлов списка дерева
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Controllers
 * @package    Treegrid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Files.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Files');

/**
 * Контроллер файлов списка дерева
 * 
 * @category   Controllers
 * @package    Treegrid
 * @subpackage Files
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Files.php 2016-01-01 12:00:00 Gear Magic $
 */
class GController_TreeGrid_Files extends GController_Files
{
    /**
     * Идент. каталога в списке дерева
     * 
     * @var string
     */
    protected $_fileId = '/';

   /**
     * Доступные расширения файлов
     *
     * @var array
     */
    protected $_icons = array(
        '3GP', 'AAC', 'ARJ', 'ASP', 'AVI', 'BMP','CGI', 'CSS', 'DOC', 'GIF', 'GZIP', 'HTML', 'JPG', 'JS', 'MP3', 'MP4', 'PNG',
        'PSD', 'RTF', 'SWF', 'TGA', 'TIFF', 'TXT', 'XML'
    );

   /**
     * Доступные фантомы каталогов
     *
     * @var array
     */
    protected $_phantomDirs = array();

   /**
     * Использоват фантомы каталогов
     *
     * @var boolean
     */
    protected $_usePhantoms = true;

   /**
     * Exclude files with extension
     *
     * @var array
     */
    protected $_excludeFiles = array();

    /**
     * Constructor
     * 
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        // идент. каталога в списке дерева
        $this->_fileId = GFile::trimPath($this->_request->getPost('node', $this->_fileId));
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @return void
     */
    protected function filesView()
    {
        parent::dataAccessView();

        // если главный узел дерева
        if ($this->_fileId == '/')
            $this->_fileId = '';
        // если используются фантомы
        if ($this->_usePhantoms) {
            if (isset($this->_phantomDirs['/' . $this->_fileId])) {
                $mask = $this->_phantomDirs['/' . $this->_fileId]['mask'];
                if ($mask == '*') {
                    $this->_usePhantoms = false;
                }
            }
        }
        $this->_path = $this->_path . $this->_fileId;
        if (!file_exists($this->_path)) {
            $this->_response->setMsgResult(L_ACTION_EXECUTE_STATUS_ERROR, sprintf(L_ACTION_PARAMS_MESSAGE_ERROR, 'fileId'), false);
            return;
        }
        $handle = opendir($this->_path);
        $index = 1;
        $isMore = false;
        while (false !== ($filename = readdir($handle))) {
            if ($isMore) {
                $index++;
                continue;
            }
            $path = $this->_path . $filename;
            $lpath = $this->_localPath . $this->_fileId . $filename;
            if ($isDir = is_dir($path))
                $path .= '/';
            if ($filename == '.htaccess')
                continue;
            if (($filename != '.' && $filename != '..')) {
                if ($this->_excludeFiles)
                    if (in_array(strtoupper(pathinfo($filename, PATHINFO_EXTENSION)), $this->_excludeFiles))
                        continue;
                $item = array('file_name' => $filename,
                              'file_dir'  => $lpath,
                              'isDir'     => $isDir,
                              'expanded'  => false,
                              'leaf'      => !is_dir($path),
                              'id'        => $this->_fileId . $filename . ($isDir ? '/' : ''));
                $node = $this->filesPreprocessing($item);
                if ($isDir) {
                    if ($this->_usePhantoms) {
                        if (isset($this->_phantomDirs[$lpath . '/']))
                            $node['file_name'] = $this->_phantomDirs[$lpath . '/']['alias'];
                        else
                            continue;
                    }
                }
                $this->_files[] = $node;
                $index++;
                if ($index > 50) $isMore = true;
            }
        }
        if ($isMore) {
            $this->_files[] = array('isDir' => false, 'expanded' => false, 'leaf' => true, 'file_name' => 'Файлов ~ ' . $index);
        }
        $this->_response->data = array($this->_files);
    }

    /**
     * Предварительная обработка данных файла во время формирования массива JSON
     * 
     * @params array $file файл
     * @return array
     */
    protected function filesPreprocessing($file)
    {
        $filename = $this->_path .  $file['file_name'];
        if ($file['isDir']) {
            $file['file_size_bytes'] = '';
            $file['file_size'] = '';
            $p = fileperms($filename);
            $file['file_perms'] = GFile::filePermsInfo($p);
            $file['file_perms_digit'] =  GFile::filePermsDigit($p);
            $file['file_datea'] = date("d-m-Y H:i:s.", fileatime($filename));
            $file['file_datem'] = date("d-m-Y H:i:s.", filemtime($filename));
            $file['file_type'] = '[]';
        } else {
            $file['file_size_bytes'] = filesize($filename);
            $file['file_size'] = GFile::fileSize($file['file_size_bytes']);
            $p = fileperms($filename);
            $file['file_perms'] = GFile::filePermsInfo($p);
            $file['file_perms_digit'] =  GFile::filePermsDigit($p);
            $file['file_datea'] = date("d-m-Y H:i:s.", fileatime($filename));
            $file['file_datem'] = date("d-m-Y H:i:s.", filemtime($filename));
            $file['file_type'] = strtoupper(pathinfo($file['file_name'], PATHINFO_EXTENSION));
            if (in_array($file['file_type'], $this->_icons))
                $file['icon'] = 'resources/images/icons/files/16x16/' . $file['file_type'] . '.png';
            else {
                $file['file_type'] = '---';
                $file['file_icon'] = '<img src="resources/images/icons/files/16x16/NONE.png"/>';
            }
        }

        return $file;
    }
}
?>