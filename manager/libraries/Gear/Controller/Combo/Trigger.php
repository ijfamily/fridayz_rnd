<?php
/**
 * Gear
 *
 * Триггер выпадающих списков
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Controllers
 * @package    Combo
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 21:00:00 Gear Magic $
 */

Gear::controller('Trigger');

/**
 * Триггер выпадающих списков
 * 
 * @category   Controllers
 * @package    Combo
 * @subpackage Trigger
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 21:00:00 Gear Magic $
 */
class GController_Combo_Trigger extends GController_Trigger
{
    /**
     * Тип триггера
     *
     * @var string
     */
    public $type = 'combo';

    /**
     * Количество записей в списке
     *
     * @var integer
     */
    protected $_limit = 10;

    /**
     * Запрос пользователя
     *
     * @var string
     */
    protected $_query = '';

    /**
     * Первая страница
     *
     * @var integer
     */
    protected $_start = 0;

    /**
     * Если данные по запросу должны возвращаться в виде (name => name)
     *
     * @var boolean
     */
    protected $_isString = false;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // первая страницы
        $this->_start = isset($_POST['start']) ? $_POST['start'] : $this->_start;
        // запрос пользователя
        $this->_query = isset($_POST['query']) ? $_POST['query'] : $this->_query;
        // количество записей в списке
        $this->_limit = isset($_POST['limit']) ? $_POST['limit'] : $this->_limit;
        $this->_isString = $this->uri->getVar('string', false);
    }

    /**
     * Возращает записи полученные по запросу пользователя
     * 
     * @param  string $tableName название таблицы
     * @param  string $primaryField первичный ключ
     * @param  string $infoField поле для формирования данных списка
     * @return void
     */
    protected function query($tableName, $primaryField, $infoField, $orderField = '', $condition = '')
    {
        if (empty($orderField))
            $orderField = $infoField;
        $table = new GDb_Table($tableName, $primaryField);
        $table->setStart($this->_start);
        $table->setLimit($this->_limit);
        $sql = 'SELECT SQL_CALC_FOUND_ROWS * '
             . 'FROM ' . $tableName . ' WHERE 1 '
             . ($this->_query ? "AND $infoField LIKE '{$this->_query}%' " : '')
             . (empty($condition) ? '' : ' AND ' . $condition)
             . ' ORDER BY ' . $orderField . ' ASC '
             . 'LIMIT %limit';
        //print $sql;
        $table->query($sql);
        $data = array();
        while (!$table->query->eof()) {
            $record = $table->query->next();
            $data[] = array('id' => $record[$primaryField], 'name' => $record[$infoField]);
        }
        $this->response->set('totalCount', $table->query->getFoundRows());
        $this->response->success = true;
        $this->response->data = $data;
    }
}
?>