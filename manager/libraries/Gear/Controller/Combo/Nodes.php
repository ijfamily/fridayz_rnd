<?php
/**
 * Gear
 *
 * Триггер выпадающего дерева
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Controllers
 * @package    Combo
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Nodes.php 2016-01-01 21:00:00 Gear Magic $
 */

Gear::controller('Nodes');

/**
 * Триггер выпадающего списка (для nested set)
 * 
 * @category   Controllers
 * @package    Combo
 * @subpackage NNodes
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Nodes.php 2016-01-01 21:00:00 Gear Magic $
 */
class GController_Combo_NNodes extends GController_Nodes_Base
{
    /**
     * Префикс полей для запросов в nested set
     *
     * @var string
     */
    public $fieldPrefix = '';

    /**
     * Поле для сортовки в запросе
     *
     * @var string
     */
    public $orderBy = 'index';

    /**
     * Значок для узла дерева
     *
     * @var string
     */
    public $nodeIcon = 'icon-page';

    /**
     * Значок (css) для узла дерева
     *
     * @var string
     */
    public $iconCls = '';

    /**
     * Значок (css) для ветвей дерева
     *
     * @var string
     */
    public $iconChildCls = '';

    /**
     * Первичное поле $tableName
     *
     * @var string
     */
    public $idProperty = '';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = '';

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function nodesView($sql = '')
    {
        parent::nodesAccessView();

        $query = new GDb_Query();
        // данные узла по выбранному идент.
        $node = $this->getNode($this->uri->getVar('node', 0));
        if ($node === false)
            throw new GSqlException();
        // префиксы полей
        $prefix = array(
            'left'  => $this->fieldPrefix . '_left',
            'right' => $this->fieldPrefix . '_right',
            'level' => $this->fieldPrefix . '_level',
            'index' => $this->fieldPrefix . '_index'
        );
        // сборка узлов по запросу
        $nodes = $this->getFirstNodes();
        if ($this->uri->getVar('node', 0) == 'root') {
            $nodes[] = $this->getNodeRoot($node, $prefix);
        } else
            if ($query->execute($this->queryPreprocessing($prefix, $node)) === false)
                throw new GSqlException();

        while (!$query->eof()) {
            $record = $query->next();
            if ($record[$prefix['right']] - $record[$prefix['left']] == 1) {
                $iconCls = $this->iconChildCls ? $this->iconChildCls : null;
            } else {
                $iconCls = $this->iconCls ? $this->iconCls : null;
            }
            $node = array(
                'expanded' => false,
                'iconCls'  => $iconCls,
                'leaf'     => !($record[$prefix['right']] - $record[$prefix['left']] > 1),
                'id'       => $record[$this->idProperty]
            );
            $nodes[] = $this->nodesPreprocessing($node, $record);
        }

        $this->response->data = $nodes;
    }

    /**
     * Возращает первые узлы дерева по условию
     * 
     * @return array
     */
    protected function getFirstNodes()
    {
        return array();
    }

    protected function getNodeRoot($record, $prefix)
    {
        $record = $this->nodesPreprocessing($record, $record);

        return array(
            'expanded' => false,
            'iconCls'  => $this->iconCls,
            'leaf'     => !($record[$prefix['right']] - $record[$prefix['left']] > 1),
            'text'     => $record['text'],
            'id'       => $record[$this->idProperty]
        );
    }

    /**
     * Возращает выбранный узел дерева по его идент.
     * 
     * @param integer $nodeId идент. выбранного узла дерева
     * @return void
     */
    protected function getNode($nodeId)
    {
        $query = new GDb_Query();
        if ($nodeId == 'root')
            $sql = 'SELECT * FROM `' . $this->tableName . '` '
                 . 'WHERE `' . $this->idProperty . '`=1';
        else
            $sql = 'SELECT * FROM `' . $this->tableName . '` '
                 . 'WHERE `' . $this->idProperty . '`=' . $nodeId;

        return $query->getRecord($sql);
    }

    /**
     * Вызывается перед предварительной обработкой запроса при формировании записей
     * 
     * @param  array $prefix префикс полей
     * @param  array $id идинд. узла дерева
     * @return string
     */
    protected function queryPreprocessing($prefix, $node)
    {
        $query = 'SELECT * '
               . 'FROM `' . $this->tableName . '` '
               . 'WHERE `' . $prefix['left'] . '`>' . $node[$prefix['left']] . ' AND '
               . '`' . $prefix['right'] . '`<' . $node[$prefix['right']] . ' AND '
               . '`' . $prefix['level'] . '`=' . ($node[$prefix['level']] + 1) . ' ';
        if ($this->orderBy) {
            if (isset($prefix[$this->orderBy]))
                $query .= 'ORDER BY `' . $prefix[$this->orderBy] . '`';
            else
                $query .= 'ORDER BY `' . $this->orderBy . '`';
        }

        return $query;
    }
}


/**
 * Триггер выпадающего списка дерева
 * 
 * @category   Controllers
 * @package    Combo
 * @subpackage Nodes
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Nodes.php 2016-01-01 21:00:00 Gear Magic $
 */
class GController_Combo_Nodes extends GController_Nodes_Base
{
    /**
     * Префикс полей для запросов в nested set
     *
     * @var string
     */
    public $fieldPrefix = '';

    /**
     * Поле для сортовки в запросе
     *
     * @var string
     */
    public $orderBy = 'index';

    /**
     * Значок для узла дерева
     *
     * @var string
     */
    public $nodeIcon = 'icon-page';

    /**
     * Первичный ключ $tableName
     *
     * @var string
     */
    public $idProperty = '';

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = '';

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function nodesView($sql = '')
    {
        parent::nodesAccessView();

        $query = new GDb_Query();
        $sql = $this->queryPreprocessing(array('parent_id' => $this->fieldPrefix . '_parent_id'), $this->uri->getVar('node', 0));
        if ($query->execute($sql) === false)
            throw new GSqlException();
        // сборка узлов по запросу
        while (!$query->eof()) {
            $record = $query->next();
            if (!isset($record['leaf']))
                $record['leaf'] = true;
            $node = array(
                'expanded' => false,
                'iconCls'  => $this->nodeIcon,
                'leaf'     => !(bool)$record['leaf'],
                'id'       => $record[$this->idProperty]
            );
            $this->_nodes[] = $this->nodesPreprocessing($node, $record);
        }

        $this->response->data = $this->_nodes;
    }

    /**
     * Вызывается перед предварительной обработкой запроса при формировании записей
     * 
     * @param  array $prefix префикс полей
     * @param  array $id идинд. узла дерева
     * @return string
     */
    protected function queryPreprocessing($prefix, $id)
    {
        $query = 'SELECT `a`.*, COUNT(`b`.`' . $this->idProperty . '`) `leaf` '
               . 'FROM `' . $this->tableName . '` `a` '
               . 'LEFT JOIN `' . $this->tableName . '` `b` ON `b`.`' . $prefix['parent_id'] . '`=`a`.`' . $this->idProperty . '` '
               . 'WHERE `a`.`' . $prefix['parent_id'] . '` ' . ($id == 'root' ? 'is NULL' : '=' . $id)
               . ' GROUP BY `a`.`' . $this->idProperty . '`';
        if ($this->orderBy) {
            if (isset($prefix[$this->orderBy]))
                $query .= ' ORDER BY `' . $prefix[$this->orderBy] . '`';
            else
                $query .= ' ORDER BY `' . $this->orderBy . '`';
        }

        return $query;
    }
}
?>