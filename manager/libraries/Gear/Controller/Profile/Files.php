<?php
/**
 * Gear Manager
 *
 * Контроллер обработки профиля файла
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Controllers
 * @package    Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Files.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::controller('Files');

/**
 * Контроллер обработки профиля файла
 * 
 * @category   Controllers
 * @package    Profile
 * @subpackage Files
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Files.php 2016-01-01 12:00:00 Gear Magic $
 */
class GController_Profile_Files extends GController_Files
{
    /*
$css = "
#selector
{ display:block; 
width:100px; 
}
#selector a:hover div.asd #asd h1.asd { 
float:left; 
text-decoration:none; 
}
";
preg_match_all( '/(?ims)([a-z0-9\s\.\:#_\-@]+)\{([^\}]*)\}/', $css, $arr);

$result = array();
foreach ($arr[0] as $i => $x)
{
    $selector = trim($arr[1][$i]);
    $rules = explode(';', trim($arr[2][$i]));
    $result[$selector] = array();
    foreach ($rules as $strRule)
    {
        if (!empty($strRule))
        {
            $rule = explode(":", $strRule);
            $result[$selector][][trim($rule[0])] = trim($rule[1]);
        }
    }
}   

var_dump($result);
*/

    protected function parseCSS($css)
    {
        preg_match_all( '/(?ims)([a-z0-9\s\.\:#_\-@,]+)\{([^\}]*)\}/', $css, $arr);
        $result = array();
        $index = 0;
        foreach ($arr[0] as $i => $x){
            $selector = trim($arr[1][$i]);
            $rules = explode(';', trim($arr[2][$i]));
            $rules_arr = array();
            foreach ($rules as $strRule){
                if (!empty($strRule)){
                    $rule = explode(":", $strRule);
                    if (sizeof($rule) == 2)
                        //$rules_arr[trim($rule[0])] = trim($rule[1]);
                        $rules_arr[] = array('text' => trim($rule[0]) . ':' . trim($rule[1]), 'leaf' => true);
                }
            }
    
            $selectors = explode(',', trim($selector));
            foreach ($selectors as $strSel){
                $result[$index] = array('id' => $strSel, 'text' => $strSel, 'leaf' => false, 'children' => $rules_arr);
            }
            $index++;
        }
    
        return $result;
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @return void
     */
    protected function filesView()
    {
        parent::dataAccessView();

        try {
            $fileName = $this->_path . '/' . $this->_request->id;
            $ext =  strtoupper(pathinfo($this->_request->id, PATHINFO_EXTENSION));
            if (!file_exists($fileName))
                throw new GException('Error', 'File "%s" does not exist' . $fileName);
            if (($text = file_get_contents($fileName)) === false)
                throw new GException('Error', 'Can`t open file "%s" for reading#' . $fileName);
        } catch(GException $e) {}
        if ($ext == 'CSS') {
            //print '<pre>';
            //print_r($this->parseCSS($text));exit;
            $d = array();
            /*for ($i = 0; $i < 100; $i++)
                $d[] = array('text' => 'text' . $i, 'leaf' => true);*/
            $text = json_encode($this->parseCSS($text));
        }

        $this->_response->data = array('file_text' => $text, 'title' => $this->getTitle($this->_request->id));
        //sprintf(Language::$terms['profile_title_update'], 
        /*
        $data = array();
        $data['file_name'] = pathinfo($this->_request->id, PATHINFO_FILENAME);
        $data['file_size_bytes'] = filesize($fileName);
        $data['file_size'] = TUtils_File::fileSize($data['file_size_bytes']);
        $p = fileperms($fileName);
        $data['file_perms'] = TUtils_File::filePermsInfo($p);
        $data['file_perms_digit'] =  TUtils_File::filePermsDigit($p);
        $data['file_datea'] = date("d-m-Y H:i:s.", fileatime($fileName));
        $data['file_datem'] = date("d-m-Y H:i:s.", filemtime($fileName));
        $data['file_type'] = strtoupper(pathinfo($data['file_name'], PATHINFO_EXTENSION));
        $this->_response->data = $this->filesPreprocessing($data);
        */
    }


    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function filesUpdate($params = array())
    {
        parent::dataAccessUpdate();

         try {
            $fileName = $this->_path . '/' . $this->_request->id;
            if (!file_exists($fileName))
                throw new GException('Error', 'File "%s" does not exist#' . $this->_path);
            // если нет параметров по умолчанию
            if (empty($params))
                $params = $this->_request->put;
            // проверка входных данных
            $this->isFilesCorrect($params);
            if (file_put_contents($fileName, $params['file_text']) === false)
                throw new GException('Error', 'Can not open file "%s" for writing#' . $this->_path);
         } catch(GException $e) {}
    }

    /**
     * Возращает загаловок
     * 
     * @param  string $fileName название файла
     * @return string
     */
    protected function getTitle($fileName)
    {}
}
?>