<?php
/**
 * Gear Manager
 *
 * Контроллер полей профиля записи
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Controllers
 * @package    Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Data.php 2016-01-01 21:00:00 Gear Magic $
 */

Gear::controller('Data');

/**
 * Контроллер полей профиля записи
 * 
 * @category   Controllers
 * @package    Profile
 * @subpackage Field
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Data.php 2016-01-01 21:00:00 Gear Magic $
 */
class GController_Profile_Field extends GController_Data
{
    /**
     * Возвращает параметры запроса
     * 
     * @param array $fields - array fields
     * @return array
     */
    protected function dataIntersect($data, $fields, $afields = array(), $smartyField = false)
    {
        return parent::dataIntersect($data, $fields, $afields, true);
    }

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        if ($this->uri->action == 'field')
            // метод запроса
            switch ($this->uri->method) {
                // метод "PUT"
                case 'PUT': $this->dataUpdate(); return;
            }

        parent::init();
    }
}
?>