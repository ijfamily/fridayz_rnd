<?php
/**
 * Gear Manager
 *
 * Контроллер интерфейса профиля записи
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Controllers
 * @package    Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Interface.php 2016-01-01 21:00:00 Gear Magic $
 */

Gear::ext('Container/Panel/Window');
Gear::ext('Container/Panel/Form');
Gear::controller('Interface');


/**
 * Контроллер интерфейса профиля записи
 * 
 * @category   Controllers
 * @package    Profile
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Interface.php 2016-01-01 21:00:00 Gear Magic $
 */
class GController_Profile_Interface extends GController_Interface
{
    /**
     * Если состояние профиля "правка данных"
     * 
     * @return boolean
     */
    public $isUpdate = false;

    /**
     * Если состояние профиля "вставка данных"
     * 
     * @return boolean
     */
    public $isInsert = false;

    /**
     * Состояние профиля
     * 
     * @return string
     */
    public $state = 'insert';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // окно (ExtJS class "Manager.window.DataProfile")
        $this->_cmp = new Ext_Window(
            array('id'               => strtolower(__CLASS__),
                  'xtype'            => 'mn-wnd-dataprofile',
                  'closable'         => true,
                  'iconCls'          => 'icon-form-edit',
                  'resizable'        => true,
                  'modal'            => true,
                  'gridId'           => '',
                  'layout'           => 'fit',
                  'plain'            => false,
                  'bodyStyle'        => 'padding:5px;',
                  'closeAfterUpdate' => true)
        );
        // форма (ExtJS class "Manager.form.DataProfile")
        $this->_cmp->items->add(new Ext_Form_DataProfile());

        // определение состояния профиля
        $state = $this->uri->getVar('state', false);
        //var_dump($state);
        // если состояние профиля определяется через id
        if ($state === false) {
            $this->isInsert = empty($this->uri->id);
            if ($this->isInsert)
                $this->state = 'insert';
            $this->isUpdate = !$this->isInsert;
            if ($this->isUpdate)
                $this->state = 'update';
        // если состояние профиля определяется через url
        } else
            $this->state = $state;
    }

    /**
     * Возвращает дополнительные данные для формирования интерфейса
     * 
     * @return mixed
     */
    protected function getDataInterface()
    {
        // соединение с базой данных
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();
    }

    /**
     * Возвращает интерфейс
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataProfile")
        $state = $this->_cmp->state;
        if (($state === false) || empty($state)) {
            $this->_cmp->state = $this->state;
        }
        // если значок окна не указан, берется из состояния профиля
        if  ($this->_cmp->iconSrc == null) {
            $this->_cmp->iconSrc = $this->resourcePath . 'icon-form-' . $this->state .'.png';
        }

        // форма (ExtJS class "Manager.form.DataProfile")
        $form = $this->_cmp->items->get(0);
        $form->recordId = $this->uri->id;
        // если есть запрос (когда вызывается профиль через триггер кнопки выпадающего списка)
        if ($setTo = $this->uri->getVar('setTo', false))
            $form->urlQuery = 'setTo=' . $setTo;

        parent::getInterface();
    }
}


/**
 * Контроллер интерфейса транслятора записи
 * 
 * @category   Controllers
 * @package    Translation
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Interface.php 2016-01-01 21:00:00 Gear Magic $
 */
class GController_Translation_Interface extends GController_Profile_Interface
{
    /**
     * Идентификатор языка
     *
     * @var integer
     */
    protected $_languageId;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // идентификатор яыка
        $this->_languageId = $this->uri->getVar('lang', '');
    }

    /**
     * Возвращает интерфейс
     * 
     * @return void
     */
    protected function getInterface()
    {
        // если значок окна не указан, берется из состояния профиля
        if  ($this->_cmp->iconSrc == null) {
            $this->_cmp->iconSrc = $this->resourcePath . 'icon-form-' . ($this->uri->hasId() ? 'update' : 'insert'). '.png';
        }

        parent::getInterface();
    }
}
?>