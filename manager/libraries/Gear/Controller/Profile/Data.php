<?php
/**
 * Gear Manager
 *
 * Контроллер данных профиля записи
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Controllers
 * @package    Profile
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Data.php 2016-01-01 21:00:00 Gear Magic $
 */

Gear::controller('Data');

/**
 * Контроллер данных профиля записи
 * 
 * @category   Controllers
 * @package    Profile
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Data.php 2016-01-01 21:00:00 Gear Magic $
 */
class GController_Profile_Data extends GController_Data
{
    /**
     * Если состояние профиля "правка данных"
     * 
     * @return boolean
     */
    public $isUpdate = false;

    /**
     * Если состояние профиля "вставка данных"
     * 
     * @return boolean
     */
    public $isInsert = false;

    /**
     * Состояние профиля
     * 
     * @return string
     */
    public $state = 'insert';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // определение состояния профиля
        $state = $this->uri->getVar('state', false);
        // если состояние профиля определяется через id
        if ($state === false) {
            $this->isInsert = empty($this->uri->id);
            if ($this->isInsert)
                $this->state = 'insert';
            $this->isUpdate = !$this->isInsert;
            if ($this->isUpdate)
                $this->state = 'update';
        // если состояние профиля определяется через url
        } else
            $this->state = $state;
    }

    /**
     * Возвращает параметры запроса
     * 
     * @param array $fields - array fields
     * @return array
     */
    protected function dataIntersect($data, $fields, $afields = array(), $smartyField = false)
    {
        return parent::dataIntersect($data, $fields, $afields, true);
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param string $sql запрос SQL на вывод данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        parent::dataView($sql);

        // загаловок
        $this->response->data['title'] = $this->getTitle($this->_data);
    }

    /**
     * Возвращает заголовок окна
     * 
     * @param  array $record запись в запросе
     * @return string
     */
    protected function getTitle($record)
    {}
}


/**
 * Контроллер данных транслятора записи
 * 
 * @category   Controllers
 * @package    Translation
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Data.php 2016-01-01 21:00:00 Gear Magic $
 */
class GController_Translation_Data extends GController_Profile_Data
{
    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        parent::dataAccessUpdate();

        $table = new GDb_Table($this->tableName, $this->idProperty, $this->fields);

        if ($this->input->isEmpty('PUT'))
            throw new GException('Warning', 'To execute a query, you must change data!');
        // проверки входных данных
        $this->isDataCorrect($params);
        // проверка существования записи с одинаковыми значениями
        $this->isDataExist($params);
        // использование системных полей в запросе SQL
        if ($this->isSysFields) {
            $params['sys_date_update'] = date('Y-m-d');
            $params['sys_time_update'] = date('H:i:s');
            $params['sys_user_update'] = $this->session->get('user_id');
        }
        $langs = $this->session->get('language/list');
        $countl = sizeof($langs);
        $countf = sizeof($this->fields);
        for ($i = 0; $i < $countl; $i++) {
            $params = array();
            $langId = $langs[$i]['language_id'];
            for ($j = 0; $j < $countf; $j++) {
                $field = $this->fields[$j] . $langId;
                if ($this->input->has($field)) {
                    $params[$this->fields[$j]] = $this->input->get($field, null);
                }
            }
            if ($params) {
                $table->updateBy($params, array(
                    $this->idProperty => $this->uri->id,
                    'language_id'     => $langId)
                );
            }
        }
        // запись в журнал действий пользователя
        $this->logUpdate(
            array('log_query'        => '[SQL]',
                  'log_query_params' => '')
        );
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        parent::dataAccessView();

        $query = new GDb_Query();
        $sql = 'SELECT * FROM `' . $this->tableName . '` '
             . 'WHERE `' . $this->idProperty . '`=' . (int) $this->uri->id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $data = array();
        $countj = sizeof($this->fields);
        while (!$query->eof()) {
            $record = $query->next();
            for ($j = 0; $j < $countj; $j++) {
                if (isset($record[$this->fields[$j]]))
                    $data[$this->fields[$j] . $record['language_id']] = $record[$this->fields[$j]];
            }
        }
        if (sizeof($data) == 0)
            throw new GSqlException();
        $this->response->data = $this->dataPreprocessing($data);
        $this->response->data['title'] = $this->getTitle($data);
        // запись в журнал действий пользователя
        $this->logView(
            array('log_query'        => $query->getSQL(),
                  'log_error'        => $this->response->success === false ? $query->getError() : null,
                  'log_query_params' => $data)
        );
    }
}
?>