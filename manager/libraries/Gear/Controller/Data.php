<?php
/**
 * Gear
 *
 * Пакет контроллеров обработки данных
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Controllers
 * @package    Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * @see GRecord
 */
require_once('Gear/Record.php');

/**
 * Базовый класс контроллера обработки данных
 * 
 * @category   Controllers
 * @package    Data
 * @subpackage Base
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
class GController_Data_Base extends GController
{
    /**
     * Данные полученные из dataView
     *
     * @var array
     */
    protected $_data;

    /**
     * Выполнять проверку учетной записи пользователя
     *
     * @var boolean
     */
    public $checkAccount = true;

    /**
     * Выполнять проверку данных перед выводом
     *
     * @var boolean
     */
    public $checkDataView = true;

    /**
     * Список привилегий вызываемых при обработки данных
     * ("dataAccessClear", "dataAccessView", ...)
     *
     * @var boolean
     */
    public $accessPrivileges = array(
        'clear'  => array('root', 'clear'), // удаление всех данных
        'delete' => array('root', 'delete'), // удаление данных
        'export' => array('root', 'export'), // экспорт данных
        'insert' => array('root', 'insert'), // вставка данных
        'update' => array('root', 'update'), // правка данных
        'select' => array('root', 'select') // вывод данных
    );

    /**
     * Доступ на удаление всех данных
     * 
     * @return void
     */
    protected function dataAccessClear()
    {
        // проверка доступа к контроллеру класса
        if ($this->checkPrivilege)
            if (!$this->store->hasPrivilege($this->accessPrivileges['clear'])) {
                $this->response->set('dialog', array('icon' => 'icon-msg-access'));
                throw new GException('Error access', 'No privileges to perform this action');
            }
        // соединение с базой данных
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();
        $this->response->set('action', 'clear');
        $this->response->setMsgResult('Deleting data', 'Successfully deleted all records!', true);
    }

    /**
     * Событие наступает после успешного удаления всех данных
     * 
     * @return void
     */
    protected function dataClearComplete()
    {}

    /**
     * Доступ на удаление данных
     * 
     * @return void
     */
    protected function dataAccessDelete()
    {
        // проверка доступа к контроллеру класса
        if ($this->checkPrivilege)
            if (!$this->store->hasPrivilege($this->accessPrivileges['delete'])) {
                $this->response->set('dialog', array('icon' => 'icon-msg-access'));
                throw new GException('Error access', 'No privileges to perform this action');
            }
        // соединение с базой данных
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($this->uri->id, 'id');
        }
        $this->response->set('action', 'delete');
        $this->response->setMsgResult('Deleting data', 'Is successful record deletion!', true);
    }

    /**
     * Событие наступает после успешного удаления данных
     * 
     * @return void
     */
    protected function dataDeleteComplete()
    {}

    /**
     * Доступ на добавление данных
     * 
     * @return void
     */
    protected function dataAccessInsert()
    {
        // проверка доступа к контроллеру класса
        if ($this->checkPrivilege)
            if (!$this->store->hasPrivilege($this->accessPrivileges['insert'])) {
                $this->response->set('dialog', array('icon' => 'icon-msg-access'));
                throw new GException('Error access', 'No privileges to perform this action');
            }
        // соединение с базой данных
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();
        $this->response->set('action', 'insert');
        $this->response->setMsgResult('Adding data', 'Is successful append' , true);
    }

    /**
     * Событие наступает после успешного добавления данных
     * 
     * @param array $data сформированные данные полученные после запроса к таблице
     * @return void
     */
    protected function dataInsertComplete($data = array())
    {}

    /**
     * Доступ на обновление данных
     * 
     * @return void
     */
    protected function dataAccessUpdate()
    {
        // проверка доступа к контроллеру класса
        if ($this->checkPrivilege)
            if (!$this->store->hasPrivilege($this->accessPrivileges['update'])) {
                $this->response->set('dialog', array('icon' => 'icon-msg-access'));
                throw new GException('Error access', 'No privileges to perform this action');
            }
        // соединение с базой данных
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($this->uri->id, 'id');
        }
        $this->response->set('action', 'update');
        $this->response->setMsgResult('Updating data', 'Change is successful' , true);
    }

    /**
     * Доступ на экспорт данных
     * 
     * @return void
     */
    protected function dataAccessExport()
    {
        // проверка доступа к контроллеру класса
        if ($this->checkPrivilege)
            if (!$this->store->hasPrivilege($this->accessPrivileges['export'])) {
                $this->response->set('dialog', array('icon' => 'icon-msg-access'));
                throw new GException('Error access', 'No privileges to perform this action');
            }
        // соединение с базой данных
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($this->uri->id, 'id');
        }
        $this->response->set('action', 'export');
        $this->response->setMsgResult('Export data', 'Export data is successful' , true);
    }

    /**
     * Событие наступает после успешного обновления данных
     * 
     * @param array $data сформированные данные полученные после запроса к таблице
     * @return void
     */
    protected function dataUpdateComplete($data = array())
    {}

    /**
     * Доступ на вывод данных
     * 
     * @return void
     */
    protected function dataAccessView()
    {
        // проверка доступа к контроллеру класса
        if ($this->checkPrivilege)
            if (!$this->store->hasPrivilege($this->accessPrivileges['select'])) {
                $this->response->set('dialog', array('icon' => 'icon-msg-access'));
                throw new GException('Error access', 'No privileges to perform this action');
            }
        // соединение с базой данных
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($this->uri->id, 'id');
        }
        $this->response->set('action', 'select');
    }

    /**
     * Событие наступает после успешного вывода данных
     * 
     * @param array $data сформированные данные полученные после запроса к таблице
     * @return void
     */
    protected function dataViewComplete($data = array())
    {}

    /**
     * Предварительная обработка записи во время формирования массива JSON
     * 
     * @params array $record запись
     * @return array
     */
    protected function dataPreprocessing($record)
    {
        return $record;
    }

    /**
     * Предварительная обработка записи
     * 
     * @params array $record запись
     * @return array
     */
    protected function recordPreprocessing($record)
    {
        return $record;
    }

    /**
     * Если одно из полей записи содержит значение тогда "true"
     * 
     * @params array $record запись
     * @params array $exclusions исключения (field1, field2, ...)
     * @return boolean
     */
    protected function isEmptyRecord($record, $exclusions = array())
    {
        if (empty($record)) return true;

        foreach($record as $field => $value) {
            // если есть исключения
            if ($exclusions)
                // если есть поле
                if (in_array($field, $exclusions)) continue;
            if (!empty($value)) return false;
        }

        return true;
    }

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        if ($this->uri->action == 'data')
            // метод запроса
            switch ($this->uri->method) {
                // метод "POST"
                case 'POST': $this->dataInsert(); return;

                // метод "GET"
                case 'GET': $this->dataView(); return;

                // метод "PUT"
                case 'PUT': $this->dataUpdate(); return;

                // метод "DELETE"
                case 'DELETE':
                    // только у этого метода, выделенных записей может быть много
                    if (isset($_POST['id']))
                        $this->uri->id = $_POST['id'];
                    $this->dataDelete();
                return;

                // метод "CLEAR"
                case 'CLEAR': $this->dataClear(); return;
            }

        parent::init();
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isDataCorrect(&$params)
    {}

    /**
     * Проверка существования зависимых записей (в каскадное удаление)
     * 
     * @return void
     */
    protected function isDataDependent()
    {}

    /**
     * Проверка существования записи с одинаковыми значениями
     * 
     * @param  array $params array (field => value, ....)
     * @return void
     */
    protected function isDataExist($params)
    {}

    /**
     * Проверяет выбранные записи, являются ли они системными
     * 
     * @return boolean
     */
    protected function isDataSystem()
    {}

    /**
     * Проверка существования фильтр для данных
     * 
     * @return boolean
     */
    public function isSetFilter()
    {
        return $this->store->get('filter');
    }
}


/**
 * Контроллер обработки данных
 * 
 * @category   Controllers
 * @package    Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
class GController_Data extends GController_Data_Base
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    protected $fields = array();

    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * (для обработки записей таблицы)
     * 
     * @var boolean
     */
    public $isSysFields = false;

    /**
     * Первичное поле таблицы ($tableName)
     *
     * @var string
     */
    public $idProperty = '';

    /**
     * ID записи
     *
     * @var mixed
     */
    protected $_recordId;

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = '';

    /**
     * Запросы пользователей через форму
     *
     * @var object
     */
    public $input;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        $this->input = GFactory::getInput();
    }

    /**
     * Возращает значение для поля "comment" таблицы лога
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return string
     */
    protected function getLogComment($params = array())
    {
        return '';
    }

    /**
     * Добавление в журнал действий пользователей
     * (удаление всех данных)
     * 
     * @param array $params параметры журнала
     * @return void
     */
    protected function logClear($params = array())
    {
        // если invisible нечего не делать
        if ($this->session->set('invisible')) return;

        if ($comment = $this->getLogComment())
            $params['log_comment'] = $comment;
        $params['log_action'] = 'clear';
        $params['controller_id'] = $this->store->getFrom('params', 'id', 0, $this->accessId);
        $params['controller_class'] = $this->classId;
        $params['log_uri'] = $this->uri->getCntUri();
        $params['log_success'] = empty($params['log_error']);

        GFactory::getDb()->connect();
        GFactory::getDbDriver('log')->add($params);
    }

    /**
     * Проверяет выбранные записей с id являются ли они системными
     * 
     * @return boolean
     */
    protected function isDataSystem()
    {
        $query = new GDb_Query();
        $sql = 'SELECT COUNT(*) `count` FROM ' . $this->tableName
             . ' WHERE ' .  $this->idProperty .' IN (' . $this->uri->id . ') AND `sys_record`=1 ';
        if (($record = $query->getRecord($sql)) === false)
            throw new GSqlException();
        if (!empty($record['count']))
            throw new GException('Data processing error', 'Unable to delete records!');
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function dataClear()
    {
        parent::dataAccessClear();
    }

    /**
     * Добавление в журнал действий пользователей
     * (удаление данных)
     * 
     * @param array $params параметры журнала
     * @return void
     */
    protected function logDelete($params = array())
    {
        // если invisible нечего не делать
        if ($this->session->set('invisible')) return;

        if ($comment = $this->getLogComment())
            $params['log_comment'] = $comment;
        if (mb_strlen($params['log_query'], 'UTF-8') > 200)
            $params['log_query'] = '[SQL]';
        $params['log_action'] = 'delete';
        $params['log_query_id'] = $this->uri->id;
        $params['controller_id'] = $this->store->getFrom('params', 'id', 0, $this->accessId);
        $params['controller_class'] = $this->classId;
        $params['log_uri'] = $this->uri->getCntUri();
        $params['log_success'] = empty($params['log_error']);

        GFactory::getDb()->connect();
        GFactory::getDbDriver('log')->add($params);
    }

    /**
     * Удаление данных
     * 
     * @return void
     */
    protected function dataDelete()
    {
        parent::dataAccessDelete();

        $table = new GDb_Table($this->tableName, $this->idProperty, $this->fields);
        // определяем откуда id брать
        if (empty($this->uri->id)) {
            $input = GFactory::getInput();
            $this->uri->id = $input->get('id');
        }
        // если не указан id
        if (empty($this->uri->id))
            throw new GException('Error', 'Unable to delete records!');
        // если используются системные поля в запросе SQL
        if ($this->isSysFields)
            // проверяет выбранные записи, являются ли они системными
            $this->isDataSystem();
        // проверка существования зависимых записей
        $this->isDataDependent();
        // удалить запись по ID
        $success = $table->delete($this->uri->id);
        if ($success > 1)
            $this->response->setMsgResult('Deleting data', 'Successfully deleted %s records!#' . $success, true);
        // если выбрана 1-а запись и sys_record = 1
        if ($success == 0)
            throw new GException('Error', 'Unable to delete records!');
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($table->query->getSQL(), 'query');
        }
        // запись в журнал действий пользователя
        $this->logDelete(
            array('log_query'        => $table->query->getSQL(),
                  'log_error'        => $this->response->success === false ? $table->getError() : null,
                  'log_query_params' => null)
        );

        $this->dataDeleteComplete();
    }

    /**
     * Добавление в журнал действий пользователей
     * (вставка данных)
     * 
     * @param array $params параметры журнала
     * @return void
     */
    protected function logInsert($params = array())
    {
        // если invisible нечего не делать
        if ($this->session->set('invisible')) return;

        if ($comment = $this->getLogComment($params['log_query_params']))
            $params['log_comment'] = $comment;
        if (mb_strlen($params['log_query'], 'UTF-8') > 200)
            $params['log_query'] = '[SQL]';
        $params['log_action'] = 'insert';
        $params['log_query_params'] = json_encode($params['log_query_params']);
        $params['log_query_id'] = $this->_recordId;
        $params['controller_id'] = $this->store->getFrom('params', 'id', 0, $this->accessId);
        $params['controller_class'] = $this->classId;
        $params['log_uri_info'] = $this->uri->getCntUriInfo();
        $params['log_uri'] = $this->uri->getCntUri();
        $params['log_success'] = empty($params['log_error']);

        GFactory::getDb()->connect();
        GFactory::getDbDriver('log')->add($params);
    }

    /**
     * Вставка данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataInsert($params = array())
    {
        $this->dataAccessInsert();

        $table = new GDb_Table($this->tableName, $this->idProperty, $this->fields);
        if ($this->input->isEmpty('POST'))
            throw new GException('Warning', 'To execute a query, you must change data!');
        // массив полей с их соответствующими значениями
        if (empty($params))
            $params = $this->dataIntersect($this->input->post, $this->fields);
        // проверки входных данных
        $this->isDataCorrect($params);
        // проверка существования записи с одинаковыми значениями
        $this->isDataExist($params);
        // использование системных полей в запросе SQL
        if ($this->isSysFields) {
            $params['sys_date_insert'] = date('Y-m-d');
            $params['sys_time_insert'] = date('H:i:s');
            $params['sys_user_insert'] = $this->session->get('user_id');
        }
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($params, 'method "POST"');
        }
        // вставка данных
        if ($table->insert($params) === false)
            throw new GSqlException();
        $this->_recordId = $table->getLastInsertId();
        // запись в журнал действий пользователя
        $this->logInsert(
            array('log_query'        => $table->query->getSQL(),
                  'log_error'        => $this->response->success === false ? $table->getError() : null,
                  'log_query_params' => $params)
        );

        $this->dataInsertComplete($params);
    }

    /**
     * Добавление в журнал действий пользователей
     * (обновление данных)
     * 
     * @param array $params параметры журнала
     * @return void
     */
    protected function logUpdate($params = array())
    {
        // если invisible нечего не делать
        if ($this->session->set('invisible')) return;

        if ($comment = $this->getLogComment($params['log_query_params']))
            $params['log_comment'] = $comment;
        if (mb_strlen($params['log_query'], 'UTF-8') > 200)
            $params['log_query'] = '[SQL]';
        $params['log_action'] = 'update';
        $params['log_query_params'] = json_encode($params['log_query_params']);
        $params['log_query_id'] = $this->uri->id;
        $params['controller_id'] = $this->store->getFrom('params', 'id', 0, $this->accessId);
        $params['controller_class'] = $this->classId;
        $params['log_uri_info'] = $this->uri->getCntUriInfo();
        $params['log_uri'] = $this->uri->getCntUri();
        $params['log_success'] = empty($params['log_error']);

        GFactory::getDb()->connect();
        GFactory::getDbDriver('log')->add($params);
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function dataUpdate($params = array())
    {
        parent::dataAccessUpdate();

        $table = new GDb_Table($this->tableName, $this->idProperty, $this->fields);
        if ($this->input->isEmpty('PUT'))
            throw new GException('Warning', 'To execute a query, you must change data!');
        // массив полей с их соответствующими значениями
        if (empty($params))
            $params = $this->input->getBy($this->fields);
        // проверки входных данных
        $this->isDataCorrect($params);
        // проверка существования записи с одинаковыми значениями
        $this->isDataExist($params);
        // использование системных полей в запросе SQL
        if ($this->isSysFields) {
            $params['sys_date_update'] = date('Y-m-d');
            $params['sys_time_update'] = date('H:i:s');
            $params['sys_user_update'] = $this->session->get('user_id');
        }
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($params, 'method "PUT"');
        }
        if ($table->update($params, $this->uri->id) === false)
            throw new GSqlException();
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($table->query->getSQL(), 'query');
        }
        // запись в журнал действий пользователя
        $this->logUpdate(
            array('log_query'        => $table->query->getSQL(),
                  'log_error'        => $this->response->success === false ? $table->getError() : null,
                  'log_query_params' => $params)
        );

        $this->dataUpdateComplete($params);
    }

    /**
     * Добавление в журнал действий пользователей
     * (вывод данных)
     * 
     * @param array $params параметры журнала
     * @return void
     */
    protected function logView($params = array())
    {
        // если invisible нечего не делать
        if ($this->session->set('invisible')) return;

        if (mb_strlen($params['log_query'], 'UTF-8') > 200)
            $params['log_query'] = '[SQL]';
        $params['log_action'] = 'select';
        $params['log_query_params'] = json_encode($params['log_query_params']);
        $params['log_query_id'] =  $this->uri->id;
        $params['controller_id'] = $this->store->getFrom('params', 'id', 0, $this->accessId);
        $params['controller_class'] = $this->classId;
        $params['log_uri_info'] = $this->uri->getCntUriInfo();
        $params['log_uri'] = $this->uri->getCntUri();
        $params['log_success'] = empty($params['log_error']);
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function dataView($sql = '')
    {
        parent::dataAccessView();

        $query = new GDb_Query();
        if (empty($sql))
            $sql = 'SELECT * FROM ' . $this->tableName . ' WHERE ' . $this->idProperty . '=' . (int) $this->uri->id;
        $sql = str_replace('%id%', (int) $this->uri->id, $sql);
        $this->_data = $query->getRecord($sql);

        if ($this->_data === false)
            throw new GSqlException();
        if (empty($this->_data) && $this->checkDataView)
            throw new GException('Error', 'Selected record was deleted!');

        $this->_data = $this->recordPreprocessing($this->_data);
        $data = $this->dataIntersect($this->_data, $this->fields, $this->store->get('fields'));
        $this->response->data = $this->dataPreprocessing($data);
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($query->getSQL(), 'query');
            GFactory::getDg()->info($data, 'data');
        }
        // запись в журнал действий пользователя
        $this->logView(
            array('log_query'        => $query->getSQL(),
                  'log_error'        => $this->response->success === false ? $query->getError() : null,
                  'log_query_params' => json_encode($data))
        );

        $this->dataViewComplete($data);
    }

    /**
     * Предварительная обработка записи запроса
     * 
     * @params array $record запись
     * @return array
     */
    protected function recordPreprocessing($record)
    {
        return $record;
    }

    /**
     * Возращает данные полученнные при пересечении полей
     * 
     * @param  array $data данные записи
     * @param  array $fields поля контроллера
     * @param  array $afields поля доступные группе пользователя
     * @param  boolean $smartyField if TRUE запрещает доступп к полям профиля если они не доступны
     * @return array
     */
    protected function dataIntersect($data, $fields, $afields = array(), $smartyField = false)
    {
        if ($data === false)
            return array();
        $result = array();
        $count = sizeof($fields);
        for ($i = 0; $i < $count; $i++) {
            if (key_exists($fields[$i], $data))
                if ($afields) {
                    if (key_exists($fields[$i], $afields))
                        $result[$fields[$i]] = $data[$fields[$i]];
                    else {
                        if ($smartyField)
                            $result[$fields[$i]] = '!';
                    }
                } else
                    $result[$fields[$i]] = $data[$fields[$i]];
        }

        return $result;
    }

    /**
     * Возращает сообщение о зависимых записях в виде диалога
     * 
     * @param  array $data массив данных
     * @return string
     */
    protected function getMsgDataDependents($data)
    {
        $html = '<div class="x-msg-data-dependents"><table cellspacing="1" cellpadding="0" border="0">';
        $count = sizeof($data);
        for ($i = 0; $i < $count; $i++) {
            $html .= '<tr><td width="150" valign="center">'
                   . '<a href="#" onclick="javascript:Ext.Msg.hide();Ext.ux.ComponentLoader.load({url: \'' . $data[$i]['url'] . '\'});">' . $data[$i]['name'] . '</a></td>'
                   . '<td valign="center">' . $data[$i]['component'] . '</td>' 
                   . '<td width="30" align="center" valign="center">' . $data[$i]['count'] . '</td></tr>';
        }
        $html .= '</table><div>';

        return $html;
    }
}
?>