<?php
/**
 * Gear
 *
 * Контроллер обработки запросов к файлам
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Controllers
 * @package    Files
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Files.php 2016-01-01 12:00:00 Gear Magic $
 */

Gear::library('Db/Db');
Gear::library('File');

/**
 * Контроллер обработки запросов к файлам
 * 
 * @category   Controllers
 * @package    Files
 * @subpackage Base
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Files.php 2014-08-08 12:00:00 Gear Magic $
 */
class GController_Files_Base extends GController
{
   /**
     * Полный путь к файлам
     *
     * @var string
     */
    protected $_path = '';

   /**
     * Локальный путь
     *
     * @var string
     */
    protected $_localPath = '/application';

    /**
     * Данные полученные зи dataView
     *
     * @var array
     */
    protected $_files = array();

    /**
     * Лог действий пользователя (GDb_Table_Log)
     *
     * @var object
     */
    protected $_log;

    /**
     * Проверка аккаунта пользователя
     *
     * @var boolean
     */
    protected $_verifyAccount = true;

    /**
     * Конструктор
     * 
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        // инициализация базы данных
        $params = GConfig::getSettings();
        $this->_db = GDb::getInstance($params);
        // журнал дейтсвий пользователя
        $this->_log = new GDb_Table_Log();
        // полный путь к файлам
        $this->_path = $params['DOCUMENT_ROOT'] . $this->_localPath;
    }

    /**
     * Доступ на удаление всех данных
     * 
     * @return void
     */
    protected function dataAccessClear()
    {
        // проверка доступа к контроллеру класса
        if ($this->_checkPrivilege)
            TStore_Controller::checkPrivilege('clear');
        // соединение с базой данных
        $this->_db->reConnect();
        $this->_response->setParam('action', 'clear');
        $this->_response->setMsgResult('Deleting data', 'Successfully deleted all records!', true);
    }

    /**
     * Доступ на удаление данных
     * 
     * @return void
     */
    protected function dataAccessDelete()
    {
        // проверка доступа к контроллеру класса
        if ($this->_checkPrivilege)
            TStore_Controller::checkPrivilege('delete');
        // соединение с базой данных
        $this->_db->reConnect();
        // отладка
        if (TStore_Controller::isDebug()) {
            $this->_debugger->info($this->_request->id, 'id');
        }
        $this->_response->setParam('action', 'delete');
        $this->_response->setMsgResult('Deleting data', 'Is successful record deletion!', true);
    }

    /**
     * Доступ на добавление данных
     * 
     * @return void
     */
    protected function dataAccessInsert()
    {
        // проверка доступа к контроллеру класса
        if ($this->_checkPrivilege)
            TStore_Controller::checkPrivilege('insert');
        // соединение с базой данных
        $this->_db->reConnect();
        $this->_response->setParam('action', 'insert');
        $this->_response->setMsgResult('Adding data', 'Is successful append' , true);
    }

    /**
     * Доступ на обновление данных
     * 
     * @return void
     */
    protected function dataAccessUpdate()
    {
        // проверка доступа к контроллеру класса
        if ($this->_checkPrivilege)
            TStore_Controller::checkPrivilege('update');
        // соединение с базой данных
        $this->_db->reConnect();
        // отладка
        if (TStore_Controller::isDebug()) {
            $this->_debugger->info($this->_request->id, 'id');
        }
        $this->_response->setParam('action', 'update');
        $this->_response->setMsgResult('Updating data', 'Change is successful' , true);
    }

    /**
     * Доступ на вывод данных
     * 
     * @return void
     */
    protected function dataAccessView()
    {
        // проверка доступа к контроллеру класса
        if ($this->_checkPrivilege)
            TStore_Controller::checkPrivilege('select');
        // соединение с базой данных
        $this->_db->reConnect();
        // отладка
        if (TStore_Controller::isDebug()) {
            $this->_debugger->info($this->_request->id, 'id');
        }
        $this->_response->setParam('action', 'select');
    }

    /**
     * Предварительная обработка данных файла во время формирования массива JSON
     * 
     * @params array $file файл
     * @return array
     */
    protected function filesPreprocessing($file)
    {
        return $file;
    }

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        // redefine id, because id defined as string 
        // /{path}/{controller}/{action}/{id} -> /{path}/{controller}/{action}/0?id={id}
        $this->_request->id = GFile::trimPath($this->_request->getGet('id', ''));
        // метод запроса
        switch ($this->_request->method) {
            // метод "GET"
            case 'GET':
                // действие
                if ($this->_request->action == 'data') {
                    $this->filesView();
                    return;
                }
                break;

            // метод "PUT"
            case 'PUT':
                // действие
                if ($this->_request->action == 'data') {
                    $this->filesUpdate();
                    return;
                }
                break;

            // метод "DELETE"
            case 'DELETE':
                // действие
                switch ($this->_request->action) {
                    // delete data
                    case 'data':
                        $this->filesDelete();
                        return;

                    // clear data
                    case 'clear':
                        $this->filesClear();
                        return;
                }
                break;

            // метод "POST"
            case 'POST':
                if (isset($this->_request->params['action'])) {
                    // действие
                    switch ($this->_request->params['action']) {
                        // get files
                        case 'get':
                            $this->filesView();
                            return;

                        // put files
                        case 'put':
                            $this->filesUpdate();
                            return;

                        // post files
                        case 'post':
                            $this->filesInsert();
                            return;

                        // delete files
                        case 'delete':
                            $this->filesDelete();
                            return;
    
                        // clear files
                        case 'clear':
                            $this->filesClear();
                            return;
                    }
                }
                // действие
                if ($this->_request->action == 'data') {
                    $this->filesInsert();
                    return;
                }
                break;
        }

        parent::init();
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isFilesCorrect(&$params)
    {}

    /**
     * Проверка существования зависимых записей (в каскадное удаление)
     * 
     * @return void
     */
    protected function isFilesDependent()
    {}

    /**
     * Проверка существования записи с одинаковыми значениями
     * 
     * @param  array $params array (field => value, ....)
     * @return void
     */
    protected function isFilesExist($params)
    {}
}


/**
 * Контроллер обработки запросов к файлам
 * 
 * @category   Controllers
 * @package    Files
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Files.php 2014-08-08 12:00:00 Gear Magic $
 */
class GController_Files extends GController_Files_Base
{
   /**
     * Полный путь к файлам
     *
     * @var string
     */
    protected $_path = '';

   /**
     * Локальный путь
     *
     * @var string
     */
    protected $_localPath = '/application';

    /**
     * Данные полученные зи dataView
     *
     * @var array
     */
    protected $_files = array();

    /**
     * Лог действий пользователя (GDb_Table_Log)
     *
     * @var object
     */
    protected $_log;

    /**
     * Проверка аккаунта пользователя
     *
     * @var boolean
     */
    protected $_verifyAccount = true;

    /**
     * Конструктор
     * 
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        // инициализация базы данных
        $params = GConfig::getSettings();
        $this->_db = GDb::getInstance($params);
        // журнал дейтсвий пользователя
        $this->_log = new GDb_Table_Log();
        // полный путь к файлам
        $this->_path = $params['DOCUMENT_ROOT'] . $this->_localPath;
    }

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function filesClear()
    {
        $this->dataAccessClear();
    }

    /**
     * Удаление данных
     * 
     * @return boolean
     */
    protected function filesDelete()
    {
        $this->dataAccessDelete();
    }

    /**
     * Вставка данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function filesInsert($params = array())
    {
        $this->dataAccessInsert();
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function filesUpdate($params = array())
    {
        $this->dataAccessUpdate();
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @return void
     */
    protected function filesView()
    {
        $this->dataAccessView();
    }
}
?>