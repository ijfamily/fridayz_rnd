<?php
/**
 * Gear Manager
 *
 * Контроллер интерфейса поиска в списке
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Controllers
 * @package    Search
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Interface.php 2016-01-01 21:00:00 Gear Magic $
 */

Gear::ext('Container/Panel/Window');
Gear::controller('Interface');

/**
 * Контроллер интерфейса поиска в списке
 * 
 * @category   Controllers
 * @package    Search
 * @subpackage Interface
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Interface.php 2016-01-01 21:00:00 Gear Magic $
 */
class GController_Search_Interface extends GController_Interface
{
    /**
     * Идентификатор DOM компонента (списка)
     *
     * @var string
     */
    public $gridId = '';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // окно (ExtJS class "Manager.window.DataSearch")
        $this->_cmp = new Ext_Window(
            array('id'          => strtolower(__CLASS__),
                  'xtype'       => 'mn-wnd-datasearch',
                  'closable'    => true,
                  'width'       => 630,
                  'height'      => 400,
                  'resizable'   => false,
                  'stateful'    => false,
                  'modal'       => true,
                  'gridId'      => '',
                  'layout'      => 'fit',
                  'plain'       => false,
                  'titleEllipsis' => 60,
                  'bodyStyle'   => 'padding:5px;',
                  'buttonAlign' => 'right')
        );
    }

    /**
     * Возращает интерфейс компонента
     * 
     * @return void
     */
    protected function getInterface()
    {
        // окно (ExtJS class "Manager.window.DataSearch")
        if  ($this->_cmp->iconSrc == null)
            $this->_cmp->iconSrc = $this->resourcePath . 'icon.png';

        parent::getInterface();
    }
}?>