<?php
/**
 * Gear Manager
 *
 * Контроллер данных поиска в списке
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Controllers
 * @package    Search
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Контроллер данных поиска в списке
 * 
 * @category   Controllers
 * @package    Search
 * @subpackage Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Data.php 2016-01-01 12:00:00 Gear Magic $
 */
class GController_Search_Data extends GController
{
    /**
     * Действие с фильтром (clear, search)
     *
     * @var string
     */
    protected $_action = '';

    /**
     * Идентификатор компонента (списка) к которому применяется поиск
     *
     * @var string
     */
    public $gridId = '';

    /**
     * Список полей таблицы (array("dataIndex" => "...", "label" => "...", "as" => "..."))
     *
     * @var array
     */
    public $fields = array();

    /**
     * Кто отправил данные в фильтр (form, toolbar, ...)
     *
     * @var string
     */
    protected $_sender;

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // обработка запроса
        $this->input = GFactory::getInput();
        // кто отправил данные в фильтр
        $this->_sender = $this->input->get('sender', '');
        // тип действия
        $this->_action = $this->input->get('action', '');
        // фильтр
        $this->_filter = $this->input->get('filter', array());
    }

    /**
     * Применение фильтра
     * 
     * @return void
     */
    protected function dataAccept()
    {
        $this->dataAccessView();

        // если фильтр из вне
        if ($this->_sender) {
            // если фильтр не диалоговый
            if ($this->_sender != 'form') {
                $func = $this->_sender . 'Accept';
                $this->$func();
                return;
            }
        }

        // проверка входных данных
        if (empty($this->_filter) || empty($this->fields))
            throw new GException('Data processing error', 'Query error (Internal Server Error)');

        $filter = array();
        $count = sizeof($this->fields);
        for($i = 0; $i < $count; $i++) {
            $field = $this->fields[$i];
            $dataIndex = $field['dataIndex'];
            if (isset($this->_filter[$dataIndex])) {
                $filterItem = &$this->_filter[$dataIndex];
                if (empty($filterItem['search']))
                    if (is_numeric($filterItem['search']))
                        $filterItem['search'] = '0';
                $filterItem['field'] = isset($field['field']) ? $field['field'] : $dataIndex;
                if (isset($field['as']))
                    $filterItem['as'] = $field['as'];
                $filter[$dataIndex] = $filterItem;
            }
        }
        $this->store->set('filter', $filter);
    }

    /**
     * Доступ на вывод данных
     * 
     * @return void
     */
    protected function dataAccessView()
    {
        // проверка доступа к контроллеру класса
        if ($this->checkPrivilege)
            if (!$this->store->hasPrivilege(array('root', 'select')))
                throw new GException('Error access', 'No privileges to perform this action');
        // соединение с базой данных
        GFactory::getDb()->connect();
        $this->response->set('action', 'select');
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @return void
     */
    protected function dataView()
    {
        $this->dataAccessView();

        // проверка входных данных
        if (empty($this->fields))
            throw new GException('Data processing error', 'Query error (Internal Server Error)');

        // возращает фильтр
        $filter = $this->store->get('filter', $this->gridId);
        $fields = $this->getFields();
        $count = sizeof($fields);
        $data = array();
        for($i = 0; $i < $count; $i++) {
            // если поля существуют в фильтре
            if (!isset($filter[$fields[$i]['dataIndex']]))
                $filter[$fields[$i]['dataIndex']] = array('search' => '', 'type' => 1, 'field' => $fields[$i]['dataIndex']);
            $data[] = array(
                'field'  => $fields[$i]['dataIndex'],
                'label'  => $fields[$i]['label'],
                'search' => $filter[$fields[$i]['dataIndex']]['search'],
                'type'   => $filter[$fields[$i]['dataIndex']]['type']
            );
        }
        $this->response->data = $data;
        $this->response->set('totalCount', count($data));
    }

    /**
     * Возращает поля
     * 
     * @return array
     */
    protected function getFields()
    {
        $fields = $this->store->get('fields', $this->gridId);
        if ($fields) {
            $fieldsa = array();
            $count = sizeof($this->fields);
            for ($i = 0; $i < $count; $i++) {
                if (key_exists($this->fields[$i]['dataIndex'], $fields))
                    $fieldsa[] = $this->fields[$i];
            }
            return $fieldsa;
        }

        return $this->fields;
    }

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        // метод запроса
        switch ($this->uri->method) {
            // метод "GET"
            case 'GET':
                // действие
                if ($this->uri->action == 'data') {
                    $this->dataView();
                    return;
                }
                break;

            // метод "POST"
            case 'POST':
                // действие
                if ($this->uri->action == 'data') {
                    $this->dataAccept();
                    return;
                }
                break;
        }

        parent::init();
    }
}
?>