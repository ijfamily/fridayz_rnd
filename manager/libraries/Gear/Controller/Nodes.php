<?php
/**
 * Gear Manager
 *
 * Контроллер деревьев
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Controllers
 * @package    Nodes
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Nodes.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Базовый класс контроллера дерева
 * 
 * @category   Controllers
 * @package    Nodes
 * @subpackage Base
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Nodes.php 2016-01-01 12:00:00 Gear Magic $
 */
class GController_Nodes_Base extends GController
{
    /**
     * Данные полученные из dataView
     *
     * @var array
     */
    protected $_nodes;

    /**
     * Выполнять проверку учетной записи пользователя
     *
     * @var boolean
     */
    public $checkAccount = true;

    /**
     * Доступ на удаление всех данных
     * 
     * @return void
     */
    protected function nodesAccessClear()
    {
        // проверка доступа к контроллеру класса
        if ($this->checkPrivilege)
            if (!$this->store->hasPrivilege(array('root', 'clear')))
                throw new GException('Error access', 'No privileges to perform this action');
        // соединение с базой данных
         if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();
        $this->response->set('action', 'clear');
        $this->response->setMsgResult('Deleting data', 'Successfully deleted all records!', true);
    }

    /**
     * Доступ на удаление данных
     * 
     * @return void
     */
    protected function nodesAccessDelete()
    {
        // проверка доступа к контроллеру класса
        if ($this->checkPrivilege)
            if (!$this->store->hasPrivilege(array('root', 'delete')))
                throw new GException('Error access', 'No privileges to perform this action');
        // соединение с базой данных
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();
        $this->response->set('action', 'delete');
        $this->response->setMsgResult('Deleting data', 'Is successful record deletion!', true);
    }

    /**
     * Доступ на добавление данных
     * 
     * @return void
     */
    protected function nodesAccessInsert()
    {
        // проверка доступа к контроллеру класса
        if ($this->checkPrivilege)
            if (!$this->store->hasPrivilege(array('root', 'insert')))
                throw new GException('Error access', 'No privileges to perform this action');
        // соединение с базой данных
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();
        $this->response->set('action', 'insert');
        $this->response->setMsgResult('Adding data', 'Is successful append' , true);
    }

    /**
     * Доступ на обновление данных
     * 
     * @return void
     */
    protected function nodesAccessUpdate()
    {
        // проверка доступа к контроллеру класса
        if ($this->checkPrivilege)
            if (!$this->store->hasPrivilege(array('root', 'update')))
                throw new GException('Error access', 'No privileges to perform this action');
        // соединение с базой данных
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();
        $this->response->set('action', 'update');
        $this->response->setMsgResult('Updating data', 'Change is successful' , true);
    }

    /**
     * Доступ на вывод данных
     * 
     * @return void
     */
    protected function nodesAccessView()
    {
        // проверка доступа к контроллеру класса
        if ($this->checkPrivilege) {
            if (!$this->store->hasPrivilege(array('root', 'select')))
                throw new GException('Error access', 'No privileges to perform this action');
        }
        // соединение с базой данных
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();
        $this->response->set('action', 'select');
    }

    /**
     * Вызывается перед предварительной обработкой запис 
     * во время формирования массива JSON
     * 
     * @params array $record запись
     * @params array $node узел дерева
     * @return array
     */
    protected function nodesPreprocessing($node, $record)
    {
        return $node;
    }

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        if ($this->uri->action == 'nodes')
            // метод запроса
            switch ($this->uri->method) {
                // метод "GET"
                case 'GET': $this->nodesView(); return;

                // метод "PUT"
                case 'PUT': $this->nodesUpdate(); return;

                // метод "DELETE"
                case 'DELETE': $this->nodesDelete(); return;

                // метод "CLEAR"
                case 'CLEAR': $this->nodesClear(); return;
            }

        parent::init();
    }

    /**
     * Проверка входных данных
     * 
     * @param  array $params массив полей с их значениями (field => value, ....)
     * @return void
     */
    protected function isNodesCorrect(&$params)
    {}

    /**
     * Проверка существования зависимых записей (в каскадное удалении)
     * 
     * @return void
     */
    protected function isNodesDependent()
    {}

    /**
     * Проверка существования записи с одинаковыми значениями
     * 
     * @param  array $params array (field => value, ....)
     * @return void
     */
    protected function isNodesExist($params)
    {}
}


/**
 * Класс обработки узлов дерева
 * 
 * @category   Controllers
 * @package    Nodes
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Nodes.php 2016-01-01 12:00:00 Gear Magic $
 */
class GController_Nodes extends GController_Nodes_Base
{
    /**
     * Массив полей таблицы ($tableName)
     *
     * @var array
     */
    protected $fields = array();

    /**
     * Использование системных полей в SQL запросе
     * если таблица их содержит
     * (для обработки записей таблицы)
     * 
     * @var boolean
     */
    public $isSysFields = false;

    /**
     * Первичное поле таблицы ($_tableName)
     *
     * @var string
     */
    public $idProperty = '';

    /**
     * ID записи
     *
     * @var mixed
     */
    protected $_recordId;

    /**
     * Название таблицы
     *
     * @var string
     */
    public $tableName = '';

    /**
     * Удаление всех данных
     * 
     * @return void
     */
    protected function nodesClear()
    {
        parent::nodesAccessClear();

        // добавление действия в журнал пользователя
        GFactory::getDbDriver('log')->add(
            array('log_action'       => 'clear',
                  'controller_id'    => $this->store->get('id', null, 'params'),
                  'controller_class' => $this->classId)
        );
    }

    /**
     * Добавление в журнал действий пользователей
     * (удаление данных)
     * 
     * @param array $params параметры журнала
     * @return void
     */
    protected function logDelete($params = array())
    {
        if (mb_strlen($params['log_query'], 'UTF-8') > 200)
            $params['log_query'] = '[SQL]';
        $params['log_action'] = 'delete';
        $params['log_query_id'] = $this->uri->id;
        $params['controller_id'] = $this->accessId;
        $params['controller_class'] = $this->classId;
        GFactory::getDbDriver('log')->add($params);
    }

    /**
     * Удаление данных
     * 
     * @return void
     */
    protected function nodesDelete()
    {
        parent::nodesAccessDelete();

        $table = new GDb_Table($this->tableName, $this->idProperty, $this->fields);
        // проверка существования записи с одинаковыми значениями
        $this->isDataDependent();
        // определяем откуда id брать
        if ($this->uri->id)
            $id = $this->uri->id;
        else {
            $input = GFactory::getInput();
            $id = $input->get('id');
        }
        // если не указан $id
        if (empty($id))
            throw new GException('Error', 'Unable to delete records!');
        // если используются системные поля в запросе SQL
        if ($this->isSysFields)
            // удалить запись по ID
            $success = $table->deleteSys($id);
        else
            // удалить запись по ID
            $success = $table->delete($id);
        if ($success > 1)
            $this->response->setMsgResult('Deleting data', 'Successfully deleted %s records!#' . $success, true);
        // если выбрана 1-а запись и sys_record = 1
        if ($success == 0)
            throw new GException('Error', 'Unable to delete records!');
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($table->query->getSQL(), 'query');
        }
        // запись в журнал действий пользователя
        $this->logDelete(
            array('log_query'        => $table->query->getSQL(),
                  'log_error'        => $this->response->success === false ? $table->getError() : null,
                  'log_query_params' => null)
        );
    }

    /**
     * Добавление в журнал действий пользователей
     * (вставка данных)
     * 
     * @param array $params параметры журнала
     * @return void
     */
    protected function logInsert($params = array())
    {
        if (mb_strlen($params['log_query'], 'UTF-8') > 200)
            $params['log_query'] = '[SQL]';
        $params['log_action'] = 'insert';
        $params['log_query_params'] = json_encode($params['log_query_params']);
        $params['log_query_id'] = $this->_recordId;
        $params['controller_id'] = $this->accessId;
        $params['controller_class'] = $this->classId;
        GFactory::getDbDriver('log')->add($params);
    }

    /**
     * Вставка данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function nodesInsert($params = array())
    {
        parent::nodesAccessInsert();

        $table = new GDb_Table($this->tableName, $this->idProperty, $this->fields);
        if (empty($this->_request->post))
            throw new GException('Warning', 'To execute a query, you must change data!');
        // массив полей с их соответствующими значениями
        if (empty($params))
            $params = $this->dataIntersect($this->_request->post, $this->fields);
        // проверки входных данных
        $this->isDataCorrect($params);
        // проверка существования записи с одинаковыми значениями
        $this->isDataExist($params);
        // использование системных полей в запросе SQL
        if ($this->_isSysFields)
            $params['sys_date_insert'] = date('Y-m-d H:i:s');
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($params, 'method "POST"');
        }
        // вставка данных
        if ($table->insert($params) === false)
            throw new GSqlException();
        $this->_recordId = $table->getLastInsertId();
        // отладка
        if (TStore_Controller::isDebug()) {
            GFactory::getDg()->info($table->query->getSQL(), 'query');
            GFactory::getDg()->info($this->_recordId, 'last insert id');
        }
        // запись в журнал действий пользователя
        $this->logInsert(
            array('log_query'        => $table->query->getSQL(),
                  'log_error'        => $this->response->success === false ? $table->getError() : null,
                  'log_query_params' => $params)
        );
    }

    /**
     * Добавление в журнал действий пользователей
     * (обновление данных)
     * 
     * @param array $params параметры журнала
     * @return void
     */
    protected function logUpdate($params = array())
    {
        if (mb_strlen($params['log_query'], 'UTF-8') > 200)
            $params['log_query'] = '[SQL]';
        $params['log_action'] = 'update';
        $params['log_query_params'] = json_encode($params['log_query_params']);
        $params['log_query_id'] = $this->_recordId;
        $params['controller_id'] = $this->accessId;
        $params['controller_class'] = $this->classId;
        GFactory::getDbDriver('log')->add($params);
    }

    /**
     * Обновление данных
     * 
     * @param array $params массив полей с их соответствующими значениями
     * @return void
     */
    protected function nodesUpdate($params = array())
    {
        parent::nodesAccessUpdate();

        $table = new GDb_Table($this->tableName, $this->idProperty, $this->fields);
        if (empty($this->_request->put))
            throw new GException('Warning', 'To execute a query, you must change data!');
        // массив полей с их соответствующими значениями
        if (empty($params))
            $params = $this->dataIntersect($this->_request->put, $this->fields);
        // проверки входных данных
        $this->isDataCorrect($params);
        // проверка существования записи с одинаковыми значениями
        $this->isDataExist($params);
        // использование системных полей в запросе SQL
        if ($this->isSysFields)
            $params['sys_date_update'] = date('Y-m-d H:i:s');
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($params, 'method "PUT"');
        }
        if ($table->update($params, $this->uri->id) === false)
            throw new GSqlException();
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($table->query->getSQL(), 'query');
        }
        // запись в журнал действий пользователя
        $this->logUpdate(
            array('log_query'        => $table->query->getSQL(),
                  'log_error'        => $this->response->success === false ? $table->getError() : null,
                  'log_query_params' => $params)
        );
    }

    /**
     * Вывод данных в интерфейс
     * 
     * @param  string $sql запрос SQL на выборку данных
     * @return void
     */
    protected function nodesView($sql = '')
    {
        parent::nodesAccessView();

        $query = new GDb_Query();
        if (empty($sql))
            $sql = 'SELECT * '
                 . 'FROM `' . $this->_tableName . '` '
                 . 'WHERE `' . $this->_idProperty . '`=' . (int)$this->_request->id;
        $this->_data = $query->getRecord($sql);
        if (sizeof($this->_data) == 0)
            throw new GSqlException();

        $data = $this->dataIntersect($this->_data, $this->_fields, $this->store->getAt('fields'));
        $this->_response->data = $this->nodesPreprocessing($data);
        // отладка
        if ($this->store->getFrom('trace', 'debug')) {
            GFactory::getDg()->info($query->getSQL(), 'query');
            GFactory::getDg()->info($data, 'data');
        }
        // запись в журнал действий пользователя
        $this->logView(
            array('log_query'        => $query->getSQL(),
                  'log_error'        => $this->response->success === false ? $query->getError() : null,
                  'log_query_params' => json_encode($data))
        );
    }
}
?>