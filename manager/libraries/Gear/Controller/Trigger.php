<?php
/**
 * Gear Manager
 *
 * Контроллер "Триггер"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Controllers
 * @package    Trigger
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Контроллер "Триггер"
 * 
 * @category   Controllers
 * @package    Trigger
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Trigger.php 2016-01-01 12:00:00 Gear Magic $
 */
class GController_Trigger extends GController
{
    /**
     * Тип триггера
     *
     * @var string
     */
    public $type = '';

    /**
     * Название триггера
     *
     * @var string
     */
    protected $_name = '';

    /**
     * Конструктор
     * 
     * @param array $settings настройки контроллера
     * @return void
     */
    public function __construct($settings = array())
    {
        parent::__construct($settings);

        // название триггера
        $this->_name = $this->uri->getVar('name', '');
    }

    /**
     * Вывод данных в интерфейс компонента
     * 
     * @param  string $name название триггера
     * @return void
     */
    protected function dataView($name)
    {
        // соединение с базой данных
        if ($this->settings['conName'])
            GFactory::getDb()->connect($this->settings['conName']);
        else
            GFactory::getDb()->connect();
    }

    /**
     * Инициализация запроса RESTful
     * 
     * @return void
     */
    protected function init()
    {
        // метод запроса
        switch ($this->uri->method) {
            // метод "POST"
            case 'POST':
                    if ($this->uri->controller != $this->type)
                        throw new GException('Controller error', 'Controller is disabled');
                    if (empty($this->_name))
                        throw new GException('Controller error', 'Controller is disabled');
                    $this->dataView($this->_name);
            return;
        }

        parent::init();
    }
}
?>