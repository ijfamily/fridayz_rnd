<?php
/**
 * Gear Manager
 * 
 * Пакет установки компонентов
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Libraries
 * @package    Install
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Install.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * Класс установщика
 * 
 * @category   Libraries
 * @package    Install
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Install.php 2016-01-01 15:00:00 Gear Magic $
 */
class GInstall
{
    /**
     * Данные полученные от разбора JSON представления
     *
     * @var string
     */
    public $data;

    /**
     * Указатель на парсер
     *
     * @var resource
     */
    public $json;

    /**
     * Кого необходимо установить (module, component, components)
     *
     * @var string
     */
    protected $_install = '';

    /**
     * Данные полученные от разбора JSON представления
     *
     * @param string $json JSON представление
     * @var void
     */
    public function fromJson($json)
    {
        try {
            $this->data = json_decode($this->json = $json, true);
            switch (json_last_error()) {
                case JSON_ERROR_DEPTH:
                    throw new GException('Data processing error', 'Maximum stack depth exceeded');
                    break;
    
                case JSON_ERROR_CTRL_CHAR:
                    throw new GException('Data processing error', 'Unexpected control character found');
                    break;
    
                case JSON_ERROR_SYNTAX:
                case 2:
                    throw new GException('Data processing error', 'Syntax error, malformed JSON');
                    break;

                case JSON_ERROR_NONE:
                    break;
            }
        } catch(GException $e) {}

        return $this->data;
    }

    /**
     * Данные полученные от разбора JSON представления
     *
     * @param array $array массив полученный от разбора JSON представления
     * @var void
     */
    public function fromArray($array)
    {
        $this->data = $array;
    }

    /**
     * Проверка данных
     *
     * @var boolean
     */
    public function check()
    {}

    /**
     * Установка компонентов, групп компонентов, модулей
     *
     * @param string $assoc массив полученный из JSON представления
     * @var boolean
     */
    public function execute($assoc)
    {}
}


/**
 * Класс установщика компонента
 * 
 * @category   Libraries
 * @package    Install
 * @copyright  Copyright (c) 2013-2014 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Install.php 2014-08-01 12:00:00 Gear Magic $
 */
class GInstall_Components extends GInstall
{
    /**
     * Возращает привилигии доступа к компоненту
     * 
     * @param string $privileges привилигии доступа (select, delete, ...)
     * @return array
     */
    protected function getPrivileges($privileges)
    {
        $result = array();
        $list = explode(',', $privileges);
        for ($i = 0; $i < sizeof($list); $i++) {
            $result[] = array(
                'label'  => '',
                'action' => trim($list[$i])
            );
        }

        return $result;
    }

    /**
     * Проверка групп компонентов
     *
     * @var void
     */
    public function checkCmpGroups(&$groups)
    {
        // чтобы сделать из списк групп "index => array(...)" список "id => array(...)"
        $sets = array();
        for ($i = 0; $i < sizeof($groups); $i++) {
            // если не указан предок, тогда корень дерева
            if (empty($groups[$i]['parent']))
                $groups[$i]['parent'] = 'root';
            $sets[$groups[$i]['id']] = $groups[$i];
        }

        $groups = $sets;
    }

    /**
     * Установка компонента
     * 
     * @param string $assoc массив полученный из JSON представления
     * @return void
     */
    public function createCmp($assoc)
    {
        $tblCtrl = new GDb_Table('gear_controllers', 'controller_id');
        $tblCtrlL = new GDb_Table('gear_controllers_l', 'controller_lang_id');
        $tblCtrlAccess = new GDb_Table('gear_controller_access', 'access_id');

        // данные компонента
        $data = $this->getCmpData($assoc['component']);
        // создание компонента
        if ($tblCtrl->insert($data['gear_controllers']) === false)
            throw new GSqlException();
        // создание языков компонента
        $controllerId = $tblCtrl->getLastInsertId();
        $items = $data['gear_controllers_l'];
        for ($i = 0; $i < sizeof($items); $i++) {
            $items[$i]['controller_id'] = $controllerId;
            if ($tblCtrlL->insert($items[$i]) === false)
                throw new GSqlException();
        }
        // создание доступа к компоненту
        $items = $data['gear_controller_access'];
        for ($i = 0; $i < sizeof($items); $i++) {
            $items[$i]['controller_id'] = $controllerId;
            if ($tblCtrlAccess->insert($items[$i]) === false)
                throw new GSqlException();
        }
    }

    /**
     * Возращает данные для добавления групп компонентов
     * 
     * @param array $group группа компонентов
     * @return array
     */
    public function getCmpGroupsData($group)
    {
        $data = array(
            'gear_controller_groups'   => array(),
            'gear_controller_groups_l' => array()
        );

        $session = GFactory::getSession();
        $language = GLanguage::$instance;

        // данные группы компонента ("gear_controller_groups")
        $data['gear_controller_groups'] = array(
            'parent'          => $group['parent'] == 'root' ? 1 : $group['parent'],
            'module_id'       => $group['module'] == 'new' ? null : $group['module'],
            'sys_date_insert' => date('Y-m-d'),
            'sys_time_insert' => date('H:i:s'),
            'sys_user_insert' => $session->get('user_id')
        );

        // языки группы компонента ("gear_controller_groups_l")
        $langs = $language->getLanguages();
        foreach ($langs as $alias => $lang) {
            $data['gear_controller_groups_l'][] = array(
                'group_id'    => null,
                'language_id' => $lang['id'],
                'group_name'  => isset($group['name'][$alias]) ? $group['name'][$alias] : null,
                'group_about' => isset($group['description'][$alias]) ? $group['description'][$alias] : null
            );
        }

        return $data;
    }

    /**
     * Проверка данных компонента
     *
     * @var void
     */
    public function checkCmp(&$component)
    {
        // проверка тега "name"
        if (empty($component['name']))
            throw new GException('Data processing error', 'The key "%s" is not exist in JSON', 'component : name');
        // проверка тега "description"
        if (empty($component['description']))
            throw new GException('Data processing error', 'The key "%s" is not exist in JSON', 'component : description');
        // проверка тега "menu"
        if (empty($component['description']))
            throw new GException('Data processing error', 'The key "%s" is not exist in JSON', 'component : menu');
        // проверка тега "class"
        if (empty($component['class']))
            throw new GException('Data processing error', 'The key "%s" is not exist in JSON', 'component : class');
        // проверка тега module"
        if (empty($component['module']))
            throw new GException('Data processing error', 'The key "%s" is not exist in JSON', 'component : module');
        // проверка тега "group"
        if (empty($component['group']))
            throw new GException('Data processing error', 'The key "%s" is not exist in JSON', 'component : group');
        // проверка тега "clear"
        if (empty($component['clear']))
            $component['clear'] = 0;
        // проверка тега "statistics"
        if (empty($component['statistics']))
            $component['statistics'] = 0;
        // проверка тега "resource"
        if (empty($component['resource']))
            throw new GException('Data processing error', 'The key "%s" is not exist in JSON', 'component: resource');
        // проверка тега "resource/package"
        if (empty($component['resource']['package']))
            throw new GException('Data processing error', 'The key "%s" is not exist in JSON', 'component : resource : package');
        // проверка тега "resource/uri"
        if (empty($component['resource']['uri']))
            throw new GException('Data processing error', 'The key "%s" is not exist in JSON', 'component : resource : uri');
        // проверка тега "resource/action"
        if (empty($component['resource']['action']))
            throw new GException('Data processing error', 'The key "%s" is not exist in JSON', 'component : resource : action');
        // проверка тега "resource/menu"
        if (!isset($component['resource']['menu']))
            throw new GException('Data processing error', 'The key "%s" is not exist in JSON', 'component : resource : menu');
        // проверка тега "interface"
        if (empty($component['interface']))
            $component['interface'] = array(
                'dashboard' => 0,
                'enabled'   => 1,
                'visible'   => 1,
                'menu'      => 0
            );
        // проверка тега "privileges"
        if (empty($component['privileges']))
            $component['privileges'] = 'select, insert, update, delete, clear, export, root';
        else
            if ($component['privileges'] == 'all')
                $component['privileges'] = 'select, insert, update, delete, clear, export, root';

        // проверка тега "fields"
        if (empty($component['fields'])) {
            $component['fields'] = array();
        }
        // проверка тега "userGroups"
        if (empty($component['userGroups'])) {
            $component['userGroups'] = array(
                array('group' => 'all', 'privileges' => 'select,insert,update,delete')
            );
        } else {
            $isAll = false;
            for ($i = 0; $i < sizeof($component['userGroups']); $i++) {
                if ($component['userGroups'][$i]['group'] == 'all') {
                    $isAll = true;
                    break;
                }
            }
            if ($isAll)
                $component['userGroups'] = array(
                    array('group' => 'all', 'privileges' => 'select,insert,update,delete')
                );
        }
    }

    /**
     * Установка компонентов
     * 
     * @param string $assoc массив полученный из JSON представления
     * @return void
     */
    public function createCmps($assoc)
    {
        $tblCtrl = new GDb_Table('gear_controllers', 'controller_id');
        $tblCtrlL = new GDb_Table('gear_controllers_l', 'controller_lang_id');
        $tblCtrlAccess = new GDb_Table('gear_controller_access', 'access_id');

        for ($c = 0; $c < sizeof($assoc['components']); $c++) {
            // данные компонента
            $data = $this->getCmpData($assoc['components'][$c]);
            // создание компонента
            if ($tblCtrl->insert($data['gear_controllers']) === false)
                throw new GSqlException();
            // создание языков компонента
            $controllerId = $tblCtrl->getLastInsertId();
            $items = $data['gear_controllers_l'];
            for ($i = 0; $i < sizeof($items); $i++) {
                $items[$i]['controller_id'] = $controllerId;
                if ($tblCtrlL->insert($items[$i]) === false)
                    throw new GSqlException();
            }
            // создание доступа к компоненту
            $items = $data['gear_controller_access'];
            for ($i = 0; $i < sizeof($items); $i++) {
                $items[$i]['controller_id'] = $controllerId;
                if ($tblCtrlAccess->insert($items[$i]) === false)
                    throw new GSqlException();
            }
        }
    }

    /**
     * Возращает данные для добавления компонента
     * 
     * @param array $component поля компонента
     * @return array
     */
    public function getCmpData($component)
    {
        $data = array(
            'gear_controllers'       => array(),
            'gear_controllers_l'     => array(),
            'gear_controller_access' => array()
        );

        $session = GFactory::getSession();
        $language = GLanguage::$instance;

        // данные компонента ("gear_controllers")
        $data['gear_controllers'] = array(
            'module_id'             => $component['module'] == 'new' ? null : $component['module'],
            'group_id'              => $component['group'],
            'controller_class'      => $component['class'],
            'controller_uri'        => $component['resource']['uri'],
            'controller_uri_pkg'    => $component['resource']['package'],
            'controller_uri_action' => $component['resource']['action'],
            'controller_uri_menu'   => $component['resource']['menu'],
            'controller_dashboard'  => $component['interface']['dashboard'],
            'controller_enabled'    => $component['interface']['enabled'],
            'controller_visible'    => $component['interface']['visible'],
            'controller_menu'       => $component['interface']['menu'],
            'controller_clear'      => $component['clear'],
            'controller_statistics' => $component['statistics'],
            'controller_privileges' => json_encode($this->getPrivileges($component['privileges'])),
            'controller_fields'     => json_encode($component['fields']),
            'sys_date_insert'       => date('Y-m-d'),
            'sys_time_insert'       => date('H:i:s'),
            'sys_user_insert'       => $session->get('user_id')
        );
        $isShortcut = isset($component['shortcut']) ? $component['shortcut'] : 0;

        // языки компонента ("gear_controllers_l")
        $langs = $language->getLanguages();
        foreach ($langs as $alias => $lang) {
            $data['gear_controllers_l'][] = array(
                'controller_id'    => null,
                'language_id'      => $lang['id'],
                'controller_name'  => isset($component['name'][$alias]) ? $component['name'][$alias] : null,
                'controller_about' => isset($component['description'][$alias]) ? $component['description'][$alias] : null,
                'controller_profile' => isset($component['menu'][$alias]) ? $component['menu'][$alias] : null
            );
        }

        // права доступа к группам пользователей ("gear_controller_access")
        if ($component['userGroups'][0]['group'] == 'all') {
            $privileges = json_encode($this->getPrivileges($component['userGroups'][0]['privileges']));
            $query = new GDb_Query();
            $sql = 'SELECT * FROM `gear_user_groups`';
            if ($query->execute($sql) === false)
                throw new GSqlException();
            while (!$query->eof()) {
                $rec = $query->next();
                $data['gear_controller_access'][] = array(
                    'group_id'          => $rec['group_id'],
                    'controller_id'     => null,
                    'access_shortcut'   => $isShortcut,
                    'access_privileges' => $privileges,
                    'access_fields'     => null,
                    'access_log'        => 1,
                    'access_debug'      => 0,
                    'access_error'      => 0,
                    'sys_record'        => 0,
                    'sys_date_insert'   => date('Y-m-d'),
                    'sys_time_insert'   => date('H:i:s'),
                    'sys_user_insert'   => $session->get('user_id')
                );
            }
        } else {
            $groups = $component['userGroups'];
            for ($i = 0; $i < sizeof($groups); $i++) {
                $privileges = json_encode($this->getPrivileges($groups[$i]['privileges']));
                $data['gear_controller_access'][] = array(
                    'group_id'          => $groups[$i]['group'],
                    'controller_id'     => null,
                    'access_shortcut'   => $isShortcut,
                    'access_privileges' => $privileges,
                    'access_fields'     => null,
                    'access_log'        => 1,
                    'access_debug'      => 0,
                    'access_error'      => 0,
                    'sys_record'        => 0,
                    'sys_date_insert'   => date('Y-m-d'),
                    'sys_time_insert'   => date('H:i:s'),
                    'sys_user_insert'   => $session->get('user_id')
                );
            }
        }

        return $data;
    }

    /**
     * Проверка данных модуля
     *
     * @var void
     */
    public function checkModule(&$module)
    {
        // проверка тега "name"
        if (empty($module['name']))
            throw new GException('Data processing error', 'The key "%s" is not exist in JSON', 'module : name');
        // проверка тега "nameSettings"
        if (empty($module['nameSettings']))
            throw new GException('Data processing error', 'The key "%s" is not exist in JSON', 'module : nameSettings');
        // проверка тега "settings"
        if (!isset($module['settings']))
            throw new GException('Data processing error', 'The key "%s" is not exist in JSON', 'module : settings');
        // проверка тега "shortname"
        if (empty($module['shortname']))
            throw new GException('Data processing error', 'The key "%s" is not exist in JSON', 'module : shortname');
        // проверка тега "description"
        if (empty($module['description']))
            throw new GException('Data processing error', 'The key "%s" is not exist in JSON', 'module : description');
        // проверка тега version"
        if (empty($module['version']))
            throw new GException('Data processing error', 'The key "%s" is not exist in JSON', 'module : version');
        // проверка тега "date"
        if (empty($module['date']))
            throw new GException('Data processing error', 'The key "%s" is not exist in JSON', 'module : date');
        // проверка тега "author"
        if (empty($module['author']))
            throw new GException('Data processing error', 'The key "%s" is not exist in JSON', 'module : author');
        // проверка тега "tables"
        if (empty($module['tables']))
            $module['tables'] = null;
    }

    /**
     * Возращает данные для добавления модуля
     * 
     * @param array $module поля модуля
     * @return array
     */
    public function getModuleData($module)
    {
        $data = array(
            'gear_modules' => array()
        );

        $session = GFactory::getSession();

        // данные модуля ("gear_modules")
        $data['gear_modules'] = array(
            'module_name'        => $module['name'],
            'module_name_settings' => $module['nameSettings'],
            'module_shortname'   => $module['shortname'],
            'module_description' => $module['description'],
            'module_version'     => $module['version'],
            'module_date'        => date('Y-m-d', strtotime($module['date'])),
            'module_author'      => $module['author'],
            'module_tables'      => $module['tables'],
            'module_settings'    => $module['settings'],
            'sys_date_insert'    => date('Y-m-d'),
            'sys_time_insert'    => date('H:i:s'),
            'sys_user_insert'    => $session->get('user_id')
        );

        return $data;
    }

    /**
     * Установка модуля
     * 
     * @param string $assoc массив полученный из JSON представления
     * @return void
     */
    public function createModule($assoc)
    {
        // массив идент. созданных групп компонентов
        $groupsId = array();

        // данные модуля
        $dataModule = $this->getModuleData($assoc['module']);
        // создание модуля
        $tblModule = new GDb_Table('gear_modules', 'module_id');
        if ($tblModule->insert($dataModule['gear_modules']) === false)
            throw new GSqlException();
        // идент. нового модуля
        $moduleId = $tblModule->getLastInsertId();

        // если есть группы компонентов
        if ($assoc['componentGroups']) {
            Gear::library('Db/Drivers/MySQL/Tree/Tree');
            // указатель на базу данных
            $db = GFactory::getDb();
            $tblGroup = new GDb_Tree('gear_controller_groups', 'group', $db->getHandle());
            $tblGroupL = new GDb_Table('gear_controller_groups_l', 'group_lang_id');
            // список групп компонентов
            foreach ($assoc['componentGroups'] as $id => $group) {
                // данные группы компонентов
                $dataGroup =  $this->getCmpGroupsData($group);
                $dataGroup['gear_controller_groups']['module_id'] = $moduleId;
                $p = $dataGroup['gear_controller_groups']['parent'];
                if (isset($groupsId[$p]))
                    $parentId = $groupsId[$p];
                else
                    $parentId = 1;
                unset($dataGroup['gear_controller_groups']['parent']);
                // создание группы компонентов
                $groupId = $tblGroup->insert($parentId, '', $dataGroup['gear_controller_groups']);
                $groupsId[$id] = $groupId;
                if (!empty($tblGroup->ERRORS_MES)) {
                    $error = implode(',', $tblGroup->ERRORS_MES);
                    throw new GSqlException();
                }
                // данные языков групп компонентов
                $items = $dataGroup['gear_controller_groups_l'];
                for ($j = 0; $j < sizeof($items); $j++) {
                    $items[$j]['group_id'] = $groupId;
                    // создание групп компонентов
                    if ($tblGroupL->insert($items[$j]) === false)
                        throw new GSqlException();
                }
            }
        }

        // если есть компоненты
        if ($assoc['components']) {
            $tblCtrl = new GDb_Table('gear_controllers', 'controller_id');
            $tblCtrlL = new GDb_Table('gear_controllers_l', 'controller_lang_id');
            $tblCtrlAccess = new GDb_Table('gear_controller_access', 'access_id');

            $count = sizeof($assoc['components']);
            for ($i = 0; $i < $count; $i++) {
                // данные компонента
                $dataCmp = $this->getCmpData($assoc['components'][$i]);
                $dataCmp['gear_controllers']['module_id'] = $moduleId;
                // если идент. группы компонента указывается из созданной группы
                if (isset($groupsId[$dataCmp['gear_controllers']['group_id']]))
                    $dataCmp['gear_controllers']['group_id'] = $groupsId[$dataCmp['gear_controllers']['group_id']];
                // создание компонента
                if ($tblCtrl->insert($dataCmp['gear_controllers']) === false)
                    throw new GSqlException();
                $controllerId = $tblCtrl->getLastInsertId();
                // данные языка компонента
                $items = $dataCmp['gear_controllers_l'];
                for ($j = 0; $j < sizeof($items); $j++) {
                    $items[$j]['controller_id'] = $controllerId;
                    // создание языка компонента
                    if ($tblCtrlL->insert($items[$j]) === false)
                        throw new GSqlException();
                }
                // данные доступа к компоненту
                $items = $dataCmp['gear_controller_access'];
                for ($j = 0; $j < sizeof($items); $j++) {
                    $items[$j]['controller_id'] = $controllerId;
                    // создание доступа к компоненту
                    if ($tblCtrlAccess->insert($items[$j]) === false)
                        throw new GSqlException();
                }
            }
        }
    }

    /**
     * Проверка данных
     *
     * @var boolean
     */
    public function check()
    {
        try {
            // проверка тега "install"
            if (empty($this->data['install']))
                throw new GException('Data processing error', 'The key "%s" is not exist in JSON', 'install');
            // кого необходимо установить
            $this->_install = $this->data['install'];
            
            switch ($this->_install) {
                // установка компонента
                case 'component':
                    // проверка тега "component"
                    if (empty($this->data['component']))
                        throw new GException('Data processing error', 'The key "%s" is not exist in JSON', 'component');
                    $this->checkCmp($this->data['component']);
                    break;

                // установка модуля
                case 'module':
                    // проверка тега "module"
                    if (empty($this->data['module']))
                        throw new GException('Data processing error', 'The key "%s" is not exist in JSON', 'module');
                    $this->checkModule($this->data['module']);
                    // если есть компоненты у модуля
                    if (!empty($this->data['components'])) {
                        for ($i = 0; $i < sizeof($this->data['components']); $i++) {
                            $this->checkCmp($this->data['components'][$i]);
                        }
                    }
                    // если есть группы компонентов у модуля
                    if (!empty($this->data['componentGroups'])) {
                        $this->checkCmpGroups($this->data['componentGroups']);
                    }
                    break;

                // установка компонентов
                case 'components':
                    // проверка тега "components"
                    if (empty($this->data['components']))
                        throw new GException('Data processing error', 'The key "%s" is not exist in JSON', 'module');
                    for ($i = 0; $i < sizeof($this->data['components']); $i++) {
                            $this->checkCmp($this->data['components'][$i]);
                    }
                    break;

                default:
                    throw new GException('Install', 'Unknow install structure');
            }
        } catch(GException $e) {}
    }

    /**
     * Установка компонентов, групп компонентов, модулей
     *
     * @param string $assoc массив полученный из JSON представления
     * @var boolean
     */
    public function execute($assoc)
    {
        // соединение с базой данных
        GFactory::getDb()->connect();

        switch ($assoc['install']) {
            // установка компонента
            case 'component': $this->createCmp($assoc); break;

            // установка модуля
            case 'module': $this->createModule($assoc); break;

            // установка компонентов
            case 'components': $this->createCmps($assoc); break;
        }

        return true;
    }
}


/**
 * Класс установщика справки
 * 
 * @category   Libraries
 * @package    Install
 * @copyright  Copyright (c) 2013-2014 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Install.php 2014-08-01 12:00:00 Gear Magic $
 */
class GInstall_Guide extends GInstall
{
    /**
     * Путь к установачным файлам справки
     *
     * @var string
     */
    public $path = 'guide/data/';

    /**
     * Проверка данных
     *
     * @var boolean
     */
    public function check()
    {
        try {
            // проверка тега "install"
            if (empty($this->data['install']))
                throw new GException('Data processing error', 'The key "%s" is not exist in JSON', 'install');
            // проверка тега "path"
            if (empty($this->data['path']))
                throw new GException('Data processing error', 'The key "%s" is not exist in JSON', 'path');
            // проверка тега "articles"
            if (empty($this->data['articles']))
                throw new GException('Data processing error', 'The key "%s" is not exist in JSON', 'articles');
            // проверка статей
            $articles = &$this->data['articles'];
            for ($i = 0; $i < sizeof($articles); $i++) {
                $articles[$i] = array_merge(array(
                    'to'          => null,
                    'label'       => null,
                    'class'       => null,
                    'folder'      => null,
                    'notice'      => null,
                    'name'        => '',
                    'description' => '',
                    'file'        => ''
                ), $articles[$i]);
            }
        } catch(GException $e) {}
    }


    /**
     * Создание справки
     * 
     * @param string $assoc массив полученный из JSON представления
     * @return void
     */
    public function createGuide($assoc)
    {
        try {
            $tbl = new GDb_Table('gear_guide', 'guide_label');
            $tblL = new GDb_Table('gear_guide_l', 'guide_lang_id');
            $articles = $assoc['articles'];
            $from = array();
            $langs = TLanguage::$instance->getLanguages();
            $path = $assoc['path'];
            for ($i = 0; $i < sizeof($articles); $i++) {
                $article = $articles[$i];
                if ($article['to'] == null)
                    $rec = array('guide_id' => null);
                else
                    if (isset($from[$article['to']]))
                        $rec = $from[$article['to']];
                    else
                        if (($rec = $tbl->getRecord($article['to'])) === false)
                            throw new GSqlException();
                // статья
                if ($article['file']) {
                    if (($file = file_get_contents($this->path . $path . $article['file'])) === false)
                        throw new GException('Data processing error', 'Can`t open file "%s" for reading', $article['file']);
                } else
                    $file = null;
                // вставка данных
                $params = array(
                    'guide_parent_id' => $rec['guide_id'],
                    'guide_label'     => $article['label'],
                    'guide_class'     => $article['class'],
                    'guide_folder'    => $article['folder'],
                    'guide_notice'    => $article['notice']
                );
                if ($tbl->insert($params) === false)
                    throw new GSqlException();
                $guideId = $tbl->getLastInsertId();
                if (!empty($article['name'])) {
                    // вставка языка
                    foreach($langs as $alias => $lang) {
                        $params = array(
                            'language_id'       => $lang['id'],
                            'guide_id'          => $guideId,
                            'guide_name'        => $article['name'],
                            'guide_description' => $article['description'],
                            'guide_html'        => $lang['id'] == 1 ? $file : null,
                        );
                        if ($tblL->insert($params) === false)
                            throw new GSqlException();
                    }
                }
            } // end for
        } catch(GException $e) {}
    }

    /**
     * Установка компонентов, групп компонентов, модулей
     *
     * @param string $assoc массив полученный из JSON представления
     * @var boolean
     */
    public function execute($assoc)
    {
        // соединение с базой данных
        GFactory::getDb()->connect();

        switch ($assoc['install']) {
            // установка компонента
            case 'guide': $this->createGuide($assoc); break;
        }

        return true;
    }
}
?>