<?php
/**
 * Gear Manager
 * 
 * Пакет формирования ответов клиенту от сервера
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Libraries
 * @package    Response
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Response.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * Класс ответа сервера на запрос клиента
 * 
 * @category   Libraries
 * @package    Response
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Response.php 2016-01-01 15:00:00 Gear Magic $
 */
class GResponse
{
    /**
     * Преобразовать в JSON
     *
     * @var boolean
     */
    public $toJSON = true;

    /**
     * Параметры ответа от сервера
     *
     * @var array
     */
    protected $_params;

    /**
     * Данные
     *
     * @var array
     */
    public $data = array();

    /**
     * Если пользователь авторизирован
     *
     * @var boolean
     */
    public $isAuthorized = true;

    /**
     * Сообщение
     *
     * @var string
     */
    public $message = '';

    /**
     * Успех при удачном запросе
     *
     * @var boolean
     */
    public $success = true;

    /**
     * Конструктор
     * 
     * @return void
     */
    public function __construct()
    {
        $this->_params = array(
            'success'      => $this->success,
            'message'      => $this->message,
            'isAuthorized' => $this->isAuthorized,
            'action'       => '',
            'data'         => $this->data
        );
    }

    /**
     * Добавление параметров в ответ
     * 
     * @param  mixed $value значение параметра
     * @param  string $name имя парметра
     * @return void
     */
    public function add($name, $value, $arr = false)
    {
        if (is_null($value))
            unset($this->_params);
        else
        if ($arr) {
            if (!isset($this->_params[$name]))
                $this->_params[$name] = array();
            $this->_params[$name][] = $value;
        } else
            $this->_params[$name] = $value;
    }

    /**
     * Получение из пакета локализации языка
     * 
     * @param  string $name имя пакатета локализации
     * @return void
     */
    public function getText($name)
    {
        if (!isset($GLOBALS['translator']))
            return false;
        if (isset($GLOBALS['translator']))
            return false;
    }

    /**
     * Возврат параметры
     * 
     * @return array
     */
    public function getParams()
    {
        if (!$this->success && empty($this->message))
            $this->message = GText::get('Controller inner error', 'exception');
        $this->_params['success'] = $this->success;
        $this->_params['message'] = $this->message;
        $this->_params['data'] = $this->data;
        $this->_params['isAuthorized'] = $this->isAuthorized;

        return $this->_params;
    }

    /**
     * Установка параметра
     * 
     * @param  string $param название параметра
     * @param  mixed $value значение параметра
     * @return void
     */
    public function set($param, $value, $add = false)
    {
        if ($add) {
            if (!isset($this->_params[$param]))
                $this->_params[$param] = array();
            $this->_params[$param][] = $value;
        } else
            $this->_params[$param] = $value;
    }

    /**
     * Установка параметров
     * 
     * @param  mixed $params массив параметров
     * @return void
     */
    public function setParams($params = array())
    {
        $this->_params = $params;
        $this->success = isset($this->_params['success']) ? $this->_params['success'] : $this->success;
        $this->message = isset($this->_params['message']) ? $this->_params['message'] : $this->message;
        $this->isAuthorized = isset($this->_params['isAuthorized']) ? $this->_params['isAuthorized'] : $this->isAuthorized;
        $this->data = isset($this->_params['data']) ? $this->_params['data'] : $this->data;
    }


    /**
     * Наиболее частые сообщения от контроллера
     * 
     * @param  string $status статус ответа
     * @param  string $message сообщение
     * @param  boolean $success успех
     * @return void
     */
    public function setMsgResult($status, $message, $success)
    {
        $this->set('status', GText::get($status, 'exception'));
        $this->message = GText::get($message, 'exception');
        $this->success = $success;
    }

    /**
     * Сообщение о ошибке
     * 
     * @param  string $message текст сообщения
     * @param  string $status статус сообщения
     * @param  string $detail детали сообщения
     * @param  boolean format форматированный вывод сообщения
     * @return void
     */
    public function setMsgError($status, $message, $detail = '', $format = true)
    {
        $this->set('status', GText::_($status, 'exception'));
        if ($format)
            $this->message = GText::_($message, 'exception', $detail);
        else
            $this->message = GText::_($message, 'exception') . '<br><br>' . $detail;
        $this->success = false;
    }

    /**
     * Кодирование данных в JSON
     * 
     * @return string
     */
    public function toJSON()
    {
        if ($this->toJSON)
            return json_encode($this->getParams());
    }
}
?>