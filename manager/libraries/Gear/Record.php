<?php
/**
 * Gear Manager
 * 
 * Класс обработки записей полученных из запроса
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Libraries
 * @package    Record
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Record.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * Запись
 * 
 * @category   Libraries
 * @package    Record
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Record.php 2016-01-01 15:00:00 Gear Magic $
 */
class GRecord
{
    /**
     * Если одно из полей записи содержит значение тогда "true"
     * 
     * @params array $record запись
     * @params array $exclusions исключения (field1, field2, ...)
     * @return boolean
     */
    public static function isEmpty($record, $exclusions = array())
    {
        if (empty($record)) return true;

        foreach($record as $field => $value) {
            // если есть исключения
            if ($exclusions)
                // если есть поле
                if (in_array($field, $exclusions)) continue;
            if (!empty($value)) return false;
        }

        return true;
    }

    /**
     * Возращает значение поля из записи
     * 
     * @params array $record запись
     * @params array $field поля
     * @params array $default значение по умолчанию
     * @return mixed
     */
    public static function get($record, $field, $default = null)
    {
        if (isset($record[$field]))
            return $record[$field];
        else
            return $default;
    }

    /**
     * Возращает значение поля из записи
     * 
     * @params array $record запись
     * @params array $field поля
     * @params array $default значение по умолчанию
     * @return mixed
     */
    public static function setTo(&$result, $record, &$field, $value)
    {
        if (isset($record[$field]))
            return $record[$field];
        else
            return $default;
    }

    /**
     * Возращает значение поля из записи
     * 
     * @params array $record запись
     * @params array $field поля
     * @params array $default значение по умолчанию
     * @return mixed
     */
    public static function setCellCls(&$record, $field, $cls)
    {
        if (!isset($record['cellCls']))
            $record['cellCls'] = array();
        $record['cellCls'][$field] = $cls;
    }

    /**
     * Типизация значения
     * 
     * @param  mixed $value значение
     * @param  string $type тип
     * @return mixed
     */
    public static function toType($value, $type)
    {
        switch ($type) {
            case 'int': return (int) $value;

            case 'bool': return (bool) $value;

            case 'string':return (string) $value;

            case 'float': return (float) $value;

            case 'price': 
                $value = (float) $value;
                if (empty($value))
                    return $value;
                else
                    return number_format($value, 2, ',', ' ');

            case 'date':
                if ($value)
                    return date('Y-m-d', strtotime($value));
                else
                    return '';
        }
    }
}
?>