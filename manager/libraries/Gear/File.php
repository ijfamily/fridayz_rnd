<?php
/**
 * Gear Manager
 * 
 * Пакет обработки файлов
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Libraries
 * @package    File
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: File.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * Класс отбработки файлов
 * 
 * @category   Libraries
 * @package    File
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: File.php 2016-01-01 15:00:00 Gear Magic $
 */
class GFile
{
    /**
     * Типы файлов
     * 
     * @var array
     */
    public static $types = array(
        'pdf'  => 'Portable Document Format (.PDF)',
        'djv'  => 'DjVu',
        'txt'  => 'Text document (.TXT)',
        'jpg'  => 'Joint Photographic Experts Group (.JPG)',
        'png'  => 'Portable network graphics (.PNG)',
        'gif'  => 'Graphics Interchange Format (.GIF)',
        'rar'  => 'Archive WinRAR (.RAR)',
        'zip'  => 'Archive ZIP - WinRAR (.ZIP)',
        'doc'  => 'Document Microsoft Word (.DOC)',
        'docx' => 'Document Microsoft Word (.DOCX)',
        'xls'  => 'Document Microsoft Excel (.XLS)',
        'xlsx' => 'Document Microsoft Excel (.XLSX)',
        'flv'  => 'Flash Video (.FLV)',
        'mp4'  => 'MPEG-4 Part 14 (.MP4)'
    );

    /**
     * Возращает полный тип файла
     * 
     * @param  string $type тип файла
     * @return string
     */
    public static function getType($type)
    {
        if (isset(self::$types[$type]))
            return self::$types[$type];

        return 'File "' . strtoupper($type) . '"';
    }

    /**
     * Удаляет часть пути к файлу
     * 
     * @param  string название файла (с каталогом)
     * @return string
     */
    public static function trimPath($fileName)
    {
        return str_replace(array('../', './'), '', $fileName);
    }

    /**
     * Возращает права доступа к файлу ввиде цифр
     * 
     * @param  integer $perms права доступа к файлу
     * @return integer
     */
    public static function filePermsDigit($perms)
    {
        $p = substr(decoct($perms), 2, 6);
        if (strlen($p) == '3')
            return '0' . $p;
        else
            return $p;
    }

    /**
     * Возращает информацию о правах доступа к файлу
     * 
     * @param  integer $perms права доступа к файлу
     * @return string
     */
    public static function filePermsInfo($perms)
    {
        if (($perms & 0xC000) == 0xC000) {
            // Socket
            $info = 's';
        } elseif (($perms & 0xA000) == 0xA000) {
            // Symbolic Link
            $info = 'l';
        } elseif (($perms & 0x8000) == 0x8000) {
            // Regular
            $info = '-';
        } elseif (($perms & 0x6000) == 0x6000) {
            // Block special
            $info = 'b';
        } elseif (($perms & 0x4000) == 0x4000) {
            // Directory
            $info = 'd';
        } elseif (($perms & 0x2000) == 0x2000) {
            // Character special
            $info = 'c';
        } elseif (($perms & 0x1000) == 0x1000) {
            // FIFO pipe
            $info = 'p';
        } else {
            // Unknown
            $info = 'u';
        }
        // Owner
        $info .= (($perms & 0x0100) ? 'r' : '-');
        $info .= (($perms & 0x0080) ? 'w' : '-');
        $info .= (($perms & 0x0040) ?
                    (($perms & 0x0800) ? 's' : 'x' ) :
                    (($perms & 0x0800) ? 'S' : '-'));
        
        // Group
        $info .= (($perms & 0x0020) ? 'r' : '-');
        $info .= (($perms & 0x0010) ? 'w' : '-');
        $info .= (($perms & 0x0008) ?
                    (($perms & 0x0400) ? 's' : 'x' ) :
                    (($perms & 0x0400) ? 'S' : '-'));
        
        // World
        $info .= (($perms & 0x0004) ? 'r' : '-');
        $info .= (($perms & 0x0002) ? 'w' : '-');
        $info .= (($perms & 0x0001) ?
                    (($perms & 0x0200) ? 't' : 'x' ) :
                    (($perms & 0x0200) ? 'T' : '-'));

        return $info;
    }

    /**
     * Возращает размер файла
     * 
     * @param  integer $size размер файла в байтах
     * @return string
     */
    public static function fileSize($size)
    {
        $kb = 1024;
        $mb = 1024 * $kb;
        $gb = 1024 * $mb;
        $tb = 1024 * $gb;
        if ($size < $kb) {
          return $size .' Kb';
        } else if ($size < $mb) {
          return round($size / $kb, 2).' Kb';
        } else if ($size < $gb) {  
          return round($size / $mb, 2).' Mb';
        } else if ($size < $tb) {
          return round($size / $gb, 2).' Gb';
        } else {
          return round($size / $tb, 2).' Tb';
        }
    }

    /**
     * Возращает размер файла
     * 
     * @param  string $filename имя файла
     * @return string
     */
    public static function getFileSize($filename)
    {
        $size = filesize($filename);
        if ($size === false) return 0;

        return self::fileSize($size);
    }

    /**
     * Создание файла
     * 
     * @param  string $filename название файла
     * @param  string $mode режим работы с файлом
     * @param  mixed $content данные файла
     * @return mixed
     */
    public static function write($filename, $mode, $content)
    {
        if (!$handle = fopen($filename, $mode))
            throw new GException('Error', 'Can not open file "%s" for writing', $filename);
        if (fwrite($handle, $content) === false)
            throw new GException('Error', 'Unable to write data to a file "%s"', array($filename));
        fclose($handle);
    }

    /**
     * Удаление файла
     * 
     * @param  string $filename название файла (с каталогом)
     * @param  boolean $checkExist проверка существования файла
     * @return mixed
     */
    public static function delete($filename, $checkExist = true)
    {
        try {
            if ($checkExist) {
                // если файл не существует
                if (!file_exists($filename))
                    throw new GException('Deleting data', 'Can`t perform file deletion "%s"', $filename);
                // если файл не удаляется
                if (@unlink($filename) === false)
                    throw new GException('Deleting data', 'Can not delete file "%s"', $filename);
            } else {
                // если файл не существует
                if (!file_exists($filename))
                    return true;
                // если файл не удаляется
                if (@unlink($filename) === false)
                    throw new GException('Deleting data', 'Can not delete file "%s"', $filename);
            }
        } catch (GException $e) {}

        return true;
    }

    /**
     * Удаление файла
     * 
     * @param  string $filename название файла (с каталогом)
     * @param  boolean $checkExist проверка существования файла
     * @return mixed
     */
    public static function copy($source , $dest)
    {
        try {
            // если файл не существует
            if (copy($source, $dest) === false)
                throw new GException('Deleting data', 'Can`t perform file deletion "%s"#' . $source);
        } catch (GException $e) {}

        return true;
    }

    /**
     * Переименовывать файла
     * 
     * @param  string $filename название файла (с каталогом)
     * @param  boolean $checkExist проверка существования файла
     * @return mixed
     */
    public static function rename($source , $dest)
    {
        try {
            // если файл не существует
            if (rename($source, $dest) === false)
                throw new GException('Loading data', 'Could not write the file to disk');
        } catch (GException $e) {}

        return true;
    }

    public static function getNewFilename($filename, $name)
    {
        return $name . '.' . strtolower(pathinfo($filename, PATHINFO_EXTENSION));
    }

    /**
     * Возращает загруженный контент файла
     * 
     * @param  string $name название переменной файла из метода POST
     * @param  array $exts допустимые расширения файла
     * @return mixed
     */
    public static function getUploadedContent($name, $exts)
    {
        // если файл не загружен
        if (empty($_FILES[$name]))
            throw new GException('Error', 'File was not loaded');
        // если файл не загружен
        if (empty($_FILES[$name]['tmp_name']))
            throw new GException('Error', 'File was not loaded');
        // проверка расширения файла
        $ext = strtolower(pathinfo($_FILES[$name]['name'], PATHINFO_EXTENSION));
        if (!in_array($ext, $exts))
            throw new GException('Error', 'The download does not match the expansion');
        // проверка оишбки
        $error = (int)$_FILES[$name]['error'];
        switch ($error) {
            case UPLOAD_ERR_INI_SIZE:
                throw new GException('Error', 'Upload file size exceeded the UPLOAD_MAX_FILESIZE');
                break;

            case UPLOAD_ERR_FORM_SIZE:
                throw new GException('Error', 'Upload file size exceeded the MAX_FILE_SIZE');
                break;

            case UPLOAD_ERR_PARTIAL:
                throw new GException('Error', 'The uploaded file was only partially loaded');
                break;

            case UPLOAD_ERR_NO_FILE:
                throw new GException('Error', 'File was not loaded');
                break;

            case UPLOAD_ERR_OK:
                return file_get_contents($_FILES[$name]['tmp_name'], true);
                break;
        }
    }

    public static function getUploadStatus($file, $exts = array())
    {
        // если файл не загружен
        if (empty($file))
            return 'File was not loaded';
        // если файл не загружен
        if (empty($file['tmp_name']))
            return 'File was not loaded';
        // проверка расширения файла
        $ext = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
        if ($exts)
            if (!in_array($ext, $exts))
                return 'The download does not match the expansion';
        // проверка оишбки
        $error = (int)$file['error'];
        switch ($error) {
            case UPLOAD_ERR_INI_SIZE:
                return 'Upload file size exceeded the UPLOAD_MAX_FILESIZE';

            case UPLOAD_ERR_FORM_SIZE:
                return 'Upload file size exceeded the MAX_FILE_SIZE';

            case UPLOAD_ERR_PARTIAL:
                return 'The uploaded file was only partially loaded';

            case UPLOAD_ERR_NO_FILE:
                return 'File was not loaded';
        }

        return true;
    }

    /**
     * Загрузка файла на сервер
     * 
     * @param  array $file файл
     * @param  string $filename название файла
     * @param  array $exts расширения файлов
     * @return void
     */
    public static function upload($file, $filename, $exts = array())
    {
        try {
            if (($status = self::getUploadStatus($file, $exts)) !== true)
                throw new GException('Loading data', $status);
            if (is_dir($filename))
                $filename .= basename($file['name']);
            if (move_uploaded_file($file['tmp_name'], $filename) === false)
                throw new GException('Loading data', 'Unable to move file "%s"#' . $filename);
        } catch(GException $e) {}
    }

    /**
     * Возращает сгенерированное файла кэша
     * 
     * @param  string $ext расшир-я файла
     * @return string
     */
    public static function genFileCache($ext = '')
    {
        $prefix = uniqid() . '_tmp.';

        if ($ext)
            return $prefix . $ext;
        else
            return $prefix;
    }

    /**
     * Перенос файла
     * 
     * @param  string $source исходное название файла (с каталогом)
     * @param  string $dest новое название файла (с каталогом)
     * @return void
     */
    public static function move($source , $dest)
    {
        self::copy($source , $dest);
        self::delete($source);
    }
}


/**
 * Класс отбработки каталогов
 * 
 * @category   Libraries
 * @package    File
 * @subpackage Dir
 * @copyright  Copyright (c) 2013-2016 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: File.php 2016-01-01 15:00:00 Gear Magic $
 */
class GDir
{
    /**
     * Удаление каталога
     * 
     * @param  string $path каталог
     * @return mixed
     */
    public static function getStat($path)
    {
        $arr = array('dirs' => 0, 'files' => 0, 'size' => 0);

        try {
            if (!is_dir($path))
                throw new GException('Error', 'The directory "%s" can not be opened', $path);
            $handle = @opendir($path);
            if ($handle === false)
                throw new GException('Error', 'The directory "%s" can not be opened', $path);
            while(false !== ($file = readdir($handle))) {
                if($file != '.' && $file != '..') {
                    $filename = $path . $file;
                    
                    // если каталог
                    if (is_dir($filename . '/')) {
                        $arr['dir'] = $arr['dir'] + 1;
                        $res = self::getStat($filename .'/');
                        $arr['dir'] = $arr['dir'] + $res['dir'];
                        $arr['files'] = $arr['files'] + $res['files'];
                        $arr['size'] = $arr['size'] + $res['size'];
                    } else {
                        $arr['files'] = $arr['files'] + 1;
                        $arr['size'] = $arr['size'] + filesize($filename);
                    }
                }
            }
        } catch (GException $e) {}
        closedir($handle);

        return $arr;
    }

    /**
     * Удаление файлов в каталоге
     * 
     * @param  string $path каталог
     * @param  array $exception расширения файлов (array('html', ...))
     * @param  boolean $deep рекурсивное удаление
     * @return mixed
     */
    public static function clear($path, $exception = array(), $deep = false)
    {
        try {
            if (!is_dir($path))
                throw new GException('Deleting data', 'The directory "%s" can not be opened', $path);
            $handle = @opendir($path);
            if ($handle === false)
                throw new GException('Deleting data', 'The directory "%s" can not be opened', $path);
            while(false !== ($file = readdir($handle))) {
                if ($exception)
                    if (in_array($file, $exception))
                        continue;
                if($file != '.' && $file != '..') {
                    $filename = $path . $file;
                    // если каталог
                    if (is_dir($filename ) && $deep) {
                        self::clear($filename, $exception, $deep);
                        if (!rmdir($filename))
                            throw new GException('Deleting data', 'Can`t perform dir deletion "%s"', $filename);
                        continue;
                    }
                    if (unlink($filename) !== true)
                        throw new GException('Deleting data', 'Can`t perform file deletion "%s"', $filename);
                }
            }
        } catch (GException $e) {}
        closedir($handle);
    }

    /**
     * Удаление каталога
     * 
     * @param  string $path каталог
     * @return mixed
     */
    public static function remove($path)
    {
        try {
            if (!is_dir($path))
                throw new GException('Deleting data', 'The directory "%s" can not be opened', $path);
            $handle = @opendir($path);
            if ($handle === false)
                throw new GException('Deleting data', 'The directory "%s" can not be opened', $path);
            while(false !== ($file = readdir($handle))) {
                if($file != '.' && $file != '..') {
                    $filename = $path . $file;
                    // если каталог
                    if (is_dir($filename . '/')) {
                        self::remove($filename . '/');
                        continue;
                    }
                    if (unlink($filename) !== true)
                        throw new GException('Deleting data', 'Can`t perform file deletion "%s"', $filename);
                }
            }
            if (is_dir($path))
                if (!rmdir($path))
                    throw new GException('Deleting data', 'Can`t perform dir deletion "%s"', $path);
        } catch (GException $e) {}
        closedir($handle);
    }

    /**
     * Удаление всех файлов в каталоге по их расширению
     * 
     * @param  string $path каталог
     * @param  array $extensions расширения файлов (array('html', ...))
     * @param  boolean $deep рекурсия
     * @return mixed
     */
    public static function clearByExt($path, $extensions = array(), $deep = false)
    {
        try {
            if (!is_dir($path))
                throw new GException('Deleting data', 'The directory "%s" can not be opened', $path);
            $handle = @opendir($path);
            if ($handle === false)
                throw new GException('Deleting data', 'The directory "%s" can not be opened', $path);
            while(false !== ($file = readdir($handle))) {
                if($file == '.' || $file == '..') continue;
                // если каталог
                if (is_dir($path . $file . '/') && $deep)
                    self::clearByExt($path . $file . '/', $extensions, $deep);
                // если есть расширения
                if ($extensions) {
                    $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                    if (!in_array($ext, $extensions)) continue;
                }
                // удалить файл
                if (unlink($path . $file) !== true)
                    throw new GException('Deleting data', 'Can`t perform file deletion "%s"', $path . $file);
            }
        } catch (GException $e) {}
        closedir($handle);
    }

    /**
     * Возращает список файлов
     * 
     * @param  string $path каталог
     * @return array
     */
    public static function getFiles($path, $extension = array())
    {
        $files = array();
        try {
            if (!is_dir($path))
                throw new GException('Deleting data', 'The directory "%s" can not be opened', $path);
            $handle = @opendir($path);
            if ($handle === false)
                throw new GException('Deleting data', 'The directory "%s" can not be opened', $path);
            while(false !== ($file = readdir($handle))) {
                if($file != '.' && $file != '..') {
                    if ($extension) {
                        $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                        if (in_array($ext, $extension))
                            $files[] = $file;
                    } else
                        $files[] = $file;
                }
            }
        } catch (GException $e) {}
        closedir($handle);

        return $files;
    }

    /**
     * Удаление файлов в каталоге
     * 
     * @param  string $path каталог
     * @param  array $exception расширения файлов (array('html', ...))
     * @param  boolean $deep рекурсивное удаление
     * @return mixed
     */
    public static function getListFiles(&$list, $path, $deep = false, $exception = array())
    {
        
        try {
            if (!is_dir($path))
                throw new GException('Data processing error', 'The directory "%s" can not be opened', $path);
            $handle = @opendir($path);
            if ($handle === false)
                throw new GException('Data processing error', 'The directory "%s" can not be opened', $path);
            while(false !== ($file = readdir($handle))) {
                if($file == '.' || $file == '..' || $file == '.svn' || $file == '.DS_store') continue;
                $filename = $path . $file;
                $isDir = is_dir($filename);
                if ($exception) {
                    if (in_array($isDir ? $filename . '/' : $filename, $exception)) continue;
                }
                if ($isDir)
                    $list[] = array('dirname' => $filename . '/', 'basename' => '', 'filename' => $filename, 'is-dir' => true);
                else
                    $list[] = array('dirname' => $path, 'basename' => $file, 'filename' => $filename, 'is-dir' => false);
                // если каталог
                if ($isDir && $deep) {
                    self::getListFiles($list, $filename . '/', $deep, $exception);
                }
            }
        } catch (GException $e) {}
        closedir($handle);

        return $list;
    }
}
?>