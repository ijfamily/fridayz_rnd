<?php
/**
 * Gear Manager
 * 
 * Пакет обработки сессий
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Libraries
 * @package    Session
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Session.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * Класс обработки сессий
 * 
 * @category   Libraries
 * @package    Session
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Session.php 2016-01-01 15:00:00 Gear Magic $
 */
class GSession
{
   /**
     * Текущие состояние сессии
     * 'inactive'|'active'|'expired'|'destroyed'|'error'
     * 
     * @var string
     */
    protected $_state = 'inactive';

    /**
     * Конструктор
     * 
     * @return void
     */
    public function __construct()
    {
        // удалить сессиюю если она стартовала из session.auto_start
        if (session_id()) {
            session_unset();
            session_destroy();
        }

        // отключение прозрачной поддержки sid
        ini_set('session.use_trans_sid', '0');
        // позволить принимать Id сессии только из cookies
        ini_set('session.use_only_cookies', '1');

        $this->_state = 'inactive';
    }

    /**
     * Возращает состояние сессии
     * 
     * @return string 'inactive'|'active'|'expired'|'destroyed'|'error'
     */
    public function getState()
    {
        return $this->_state;
    }

    /**
     * Возращает жизнь сессии в минутах
     * 
     * @return integer
     */
    public function getExpire()
    {
        return GFactory::getConfig()->get('SESSION/EXPIRE');
    }
 
     /**
     * Старт сессии
     * 
     * @return mixed если сессия уже стартовала false, иначе true
     */
    public function start()
    {
        if ($this->_state === 'active')
            return false;

        $name = GFactory::getConfig()->get('SESSION/NAME');
        session_name($name);
        $this->_state = 'active';

        return session_start();
    }

     /**
     * Рестарт сессии
     * 
     * @return mixed если сессия уже уничтожена false, иначе true
     */
    public function reStart()
    {
        $this->destroy();
        if ($this->_state !== 'destroyed')
            return false;

        session_regenerate_id(true);
        $this->_start();
        $this->_state = 'active';

        return true;
    }

     /**
     * Возращает идентификатор сессии
     * 
     * @return string если сессия уничтожена false, иначе id
     */
    public function getId()
    {
        if ($this->_state === 'destroyed')
            return false;

        return session_id();
    }

     /**
     * Проверка активности сессии
     * 
     * @return string true если активна, иначе - false
     */
    public function isActive()
    {
        return $this->_state == 'active';
    }

     /**
     * Проверка активности сессии
     * 
     * @return string true если активна, иначе - false
     */
    public function active()
    {
        $this->_state = 'active';
    }

     /**
     * Завершить текущий сеанс сессии и сохранить данные сеанса
     * 
     * @return void
     */
    public function close()
    {
        session_write_close();
    }

     /**
     * Удаление переменной $name в сессии
     * 
     * @param string $name имя переменной
     * @param string $namespace пространство имен чтобы не было конфликтов с переменными
     * @return mixed если сессия не активна - false, иначе возращает старое значение переменной
     */
    public function clear($name, $namespace = '')
    {
        if ($this->_state !== 'active')
            return false;

        $value = null;
        if ($namespace) {
            if (isset($_SESSION[$namespace][$name])) {
                $value = $_SESSION[$namespace][$name];
                unset($_SESSION[$namespace][$name]);
            }
        } else {
            if (isset($_SESSION[$name])) {
                $value = $_SESSION[$name];
                unset($_SESSION[$name]);
            }
        }

        return $value;
    }

     /**
     * Уничтожение сессии
     * 
     * @return mixed если сессия уже уничтожена - false, иначе true
     */
    public function destroy()
    {
        if ($this->_state === 'destroyed')
            return true;

        if (isset($_COOKIE[session_name()])) {
            $cfg = GFactory::getConfig();
            $cfgDomain = $cfg->get('COOKIE/DOMAIN', '');
            $cfgPath = $cfg->get('COOKIE/PATH', '/');
            setcookie(session_name(), '', time() - 42000, $cfgPath, $cfgDomain);
        }

        session_unset();
        session_destroy();
        $this->_state = 'destroyed';

        return true;
    }

     /**
     * Установка значения $value переменной $name сессии
     * 
     * @param  string $name имя переменной
     * @param  mixed $value значение
     * @param  string $namespace пространство имен чтобы не было конфликтов с переменными
     * @return mixed если сессия не активна - false, иначе void
     */
    public function set($name, $value = null, $namespace = '')
    {
        if ($this->_state !== 'active')
            return false;

        if ($namespace)
            $old = isset($_SESSION[$namespace][$name]) ? $_SESSION[$namespace][$name] : null;
        else
            $old = isset($_SESSION[$name]) ? $_SESSION[$name] : null;

        if ($value === null) {
            if ($namespace)
                unset($_SESSION[$namespace][$name]);
            else
                unset($_SESSION[$name]);
        } else {
            if ($namespace)
                $_SESSION[$namespace][$name] = $value;
            else
                $_SESSION[$name] = $value;
        }
    }

     /**
     * Возращает значения переменной $name сессии
     * 
     * @param  string $name имя переменной
     * @param  mixed $default значение поумолчанию если нет переменной $name в сессии
     * @param  string $namespace пространство имен чтобы не было конфликтов с переменными
     * @return mixed если сессия не активна - false, иначе значение переменной
     */
    public function &get($name, $default = '', $namespace = '')
    {
        if ($this->_state !== 'active' && $this->_state !== 'expired') return null;

        if ($namespace) {
            if (isset($_SESSION[$namespace][$name]))
                return $_SESSION[$namespace][$name];
            else
                return $default;
        } else {
            if (isset($_SESSION[$name]))
                return $_SESSION[$name];
            else
                return $default;
        }
    }

     /**
     * Проверка существования переменной $name сессии
     * 
     * @param  string $name имя переменной
     * @param  string $namespace пространство имен чтобы не было конфликтов с переменными
     * @return mixed если сессия не активна - null
     */
    public function has($name, $namespace = '')
    {
        if ($this->_state !== 'active')
            return null;

        if ($namespace) 
            return isset($_SESSION[$namespace][$name]);
        else
            return isset($_SESSION[$name]);
    }

     /**
     * Возращает название сессии
     * 
     * @return mixed если сессия уже уничтожена - false, иначе название
     */
    public function getName()
    {
        if ($this->_state === 'destroyed')
            return false;

        return session_name();
    }

    /**
     * Добавить язык по умолчанию
     * 
     * @param  string $alias псевдоним языка
     * @return void
     */
    public function setSysLanguage($alias)
    {
        $list = $this->get('language/list');
        $count = sizeof($list);
        for ($i = 0; $i < $count; $i++)
            if ($list[$i]['language_alias'] == $alias) {
                $this->set('language/default/id', $list[$i]['language_id']);
                $this->set('language/default/name', $list[$i]['language_name']);
                $this->set('language/default/alias', $list[$i]['language_alias']);
                break;
            }
    }

    /**
     * Установливает для каждого элемента списка языков путь к маршрутизатору
     * 
     * @param  string $routerpath путь к маршрутизатору
     * @return array список языков
     */
    public function getSysLaguageItems($routerpath)
    {
        $items = $this->get('language/items');
        $count = sizeof($items);
        for ($i = 0; $i < $count; $i++)
            $items[$i]['url'] = $routerpath;

        return $items;
    }

    /**
     * Установливает для каждого элемента списка языков CMS путь к маршрутизатору
     * 
     * @param  string $routerpath путь к маршрутизатору
     * @return array список языков
     */
    public function getTranslateItems($langId)
    {
        $items = $this->get('cms/language/translator');
        $cur = $items[$langId];
        $menu = array();
        foreach ($items as $key => $item) {
            if ($key != $langId)
                $menu[] = array(
                    'langFrom' => $item['alias'],
                    'langTo'   => $cur['alias'],
                    'text'     => $item['text'] . ' <b>&raquo;</b> ' . $cur['text']
                );
        }

        return $menu;
    }
}


/**
 * Класс сессии контроллера
 * 
 * @category   Libraries
 * @package    Session
 * @subpackage Controller
 * @copyright  Copyright (c) 2013-2014 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Session.php 2014-08-01 12:00:00 Gear Magic $
 */
class GSession_Controller extends GSession
{
    /**
     * Идентификатор класса контроллера
     * 
     * @var string
     */
    public $classId = '';

    /**
     * Префикс для хранения данных контроллера в хранилище
     * 
     * @var string
     */
    public $prefix = 'controller/';

    /**
     * Конструктор
     * 
     * @param string $classId идентификатор контроллера
     * @return void
     */
    public function __construct($classId = '')
    {
        $this->classId = $classId;
        $this->namespace = $this->prefix . $this->classId;
    }

     /**
     * Установка значения $value переменной контроллера
     * 
     * @param  string $name имя переменной
     * @param  mixed $value значение
     * @param  string $classId идентификатор класса контроллера
     * @return mixed если сессия не активна - false, иначе void
     */
    public function set($name, $value = null, $classId = '')
    {
        if (empty($classId))
            $namespace = $this->namespace;
        else
            $namespace = $this->prefix . $classId;

        return parent::set($name, $value, $namespace);
    }

     /**
     * Возращает значения переменной контроллера
     * 
     * @param  string $name переменная
     * @param  mixed $default значение поумолчанию если нет переменной
     * @param  string $classId идентификатор класса контроллера
     * @return mixed если переменной нет - $default
     */
    public function &get($name, $default = '', $classId = '')
    {
        if (empty($classId))
            $namespace = $this->namespace;
        else
            $namespace = $this->prefix . $classId;

        return parent::get($name, $default, $namespace);
    }

     /**
     * Возращает все переменные контроллера
     * 
     * @param  string $classId идентификатор класса контроллера
     * @return mixed если переменной нет - $default
     */
    public function getAll($classId = '')
    {
        if (empty($classId))
            $namespace = $this->namespace;
        else
            $namespace = $this->prefix . $classId;

        return parent::get($namespace, false);
    }

     /**
     * Установка параметра контроллера
     * 
     * @param  string $name имя переменной
     * @param  mixed $value значение
     * @param  string $classId идентификатор класса контроллера
     * @return mixed если успех возращает значение, иначе void
     */
    public function setTo($section, $name, $value = '', $classId = '')
    {
        if (($params = &$this->get($section, false, $classId)) !== false) {
            if ($value == null) {
                $value = $params[$name];
                unset($params[$name]);
                return $value;
            } else
                return $params[$name] = $value;
        }

        return false;
    }

     /**
     * Возращает значения переменной $name сессии
     * 
     * @param  string $section название раздела
     * @param  string $name имя переменной в разделе
     * @param  mixed $default значение поумолчанию если нет переменной в разделе
     * @param  string $classId идентификатор класса контроллера
     * @return mixed если сессия не активна - false, иначе значение переменной
     */
    public function getFrom($section, $name, $default = false, $classId = '')
    {
        if (($params = &$this->get($section, false, $classId)) !== false) {
            if (isset($params[$name]))
                return $params[$name];
        }

        return $default;
    }

     /**
     * Проверка существования переменной в среде контроллера
     * 
     * @param  string $name имя переменной
     * @param  string $classId идентификатор класса контроллера
     * @return mixed если сессия не активна - null
     */
    public function has($name, $classId = '')
    {
        if (empty($classId))
            $namespace = $this->namespace;
        else
            $namespace = $this->prefix . $classId;

        return parent::has($name, $namespace);
    }

     /**
     * Создание среды контроллера
     * 
     * @param  string $classId идентификатор класса контроллера
     * @return mixed
     */
    public function create($classId = '')
    {
        if (empty($classId))
            $namespace = $this->namespace;
        else
            $namespace = $this->prefix . $classId;

        $settings = array(
            'trace' => array(), 'params' => array(), 'privileges' => array(), 'fields' => array(),
            'filter' => array()
        );

        return parent::set($namespace, $settings);
    }

     /**
     * Удаление переменной $name в сессии
     * 
     * @param string $name имя переменной
     * @param string $namespace пространство имен чтобы не было конфликтов с переменными
     * @return mixed если сессия не активна - false, иначе возращает старое значение переменной
     */
    public function clear($name, $namespace = '')
    {
        return $this->create($classId);
    }

     /**
     * Удаление среды контроллера
     * 
     * @param  string $classId идентификатор класса контроллера
     * @return void
     */
    public function remove($classId = '')
    {
        if (empty($classId))
            $namespace = $this->namespace;
        else
            $namespace = $this->prefix . $classId;

        parent::set($namespace, null);
    }

    /**
     * Возращает список привилегий контроллера
     * 
     * @return mixed
     */
    public function getPrivileges()
    {
        return $this->get('privileges', false);
    }

    /**
     * Проверка привилегий контроллера
     * 
     * @param  string $name название привилегий
     * @return boolean
     */
    public function hasPrivilege($name)
    {
        $privileges = $this->get('privileges', false);
        // если нет раздела привилегий
        if ($privileges === false || $privileges == null) return false;
        // если список привилегий
        if (is_array($name)) {
            $count = sizeof($name);
            for ($i = 0; $i < $count; $i++) {
                if (key_exists($name[$i], $privileges))
                    return true;
            }
        } else
            return key_exists($name, $privileges);
    }
}
?>