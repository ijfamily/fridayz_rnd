<?php
/**
 * Gear Manager
 * 
 * Пакет версий системы
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Libraries
 * @package    Version
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Version.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * Версия системы
 * 
 * @category   Libraries
 * @package    Version
 * @subpackge  Version
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Version.php 2016-01-01 15:00:00 Gear Magic $
 */
class GVersion
{
    /**
     * Псевдоним
     */
    public $alias = '';

    /**
     * Название приложения
     */
    public $name = '';

    /**
     * Версия
     */
    public $version = '';

    /**
     * Дата последнего обновления
     */
    public $update = '';

    /**
     * Возращает номер версии c псевдонимом
     *
     * @return string
     */
    public function getVersionAlias()
    {
        if ($this->alias)
            return $this->version . ' / ' . $this->alias;
        else
            return $this->version;
    }

    /**
     * Возращает развёрнутую версию системы
     *
     * @return string
     */
    public function getFullVersion()
    {
        if (empty($this->alias))
            return $this->version . ' (' . $this->update . ') / ' . $this->name;
        else
            return $this->version . ' / ' . $this->alias . ' (' . $this->update . ')';
    }

    /**
     * Возращает сокр. название приложения
     *
     * @return string
     */
    public function getShortName()
    {
        return $this->name . ' ' . $this->version;
    }

    /**
     * Возращает сокр. название приложения
     *
     * @return string
     */
    public function getPoweredBy()
    {
        return $this->name . '/' . $this->version . ' build/' . $this->update;
    }
}


/**
 * Класс версий системы
 * 
 * @category   Libraries
 * @package    Version
 * @subpackage Core
 * @copyright  Copyright (c) 2011-2014 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Version.php 2014-08-01 12:00:00 Gear Magic $
 */
final class GVersion_Core extends GVersion
{
    /**
     * Название приложения
     */
    public $name = 'Gear';

    /**
     * Версия
     */
    public $version = '1.7';

    /**
     * Дата последнего обновления
     */
    public $update = '01/01/2016';
}


/**
 * Версия приложения
 * 
 * @category   Libraries
 * @package    Version
 * @subpackage Application
 * @copyright  Copyright (c) 2011-2014 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Version.php 2014-08-18 12:00:00 Gear Magic $
 */
final class GVersion_Shell extends GVersion
{
    /**
     * Псевдоним
     */
    public $alias = 'green';

    /**
     * Название приложения
     */
    public $name = 'Gear Manager';

    /**
     * Версия
     */
    public $version = '1.7';

    /**
     * Дата последнего обновления
     */
    public $update = '01/01/2016';
}


/**
 * Версия приложения
 * 
 * @category   Libraries
 * @package    Version
 * @subpackage Application
 * @copyright  Copyright (c) 2011-2014 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Version.php 2014-08-18 12:00:00 Gear Magic $
 */
final class GVersion_Application extends GVersion
{
    /**
     * Название приложения
     */
    public $name = 'Gear Manager';

    /**
     * Версия
     */
    public $version = '1.7';

    /**
     * Дата последнего обновления
     */
    public $update = '01/01/2016';
}
?>