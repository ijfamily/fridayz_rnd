<?php
/**
 * Gear Manager
 * 
 * Пакет информации о сервере
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Libraries
 * @package    Server
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Server.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * Класс информации о сервере
 * 
 * @category   Libraries
 * @package    Server
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Server.php 2016-01-01 15:00:00 Gear Magic $
 */
class GServer
{
    /**
     * Сылка на экземпляр класса
     *
     * @var mixed
     */
    protected static $_instance = null;

    /**
     * Конструктор
     * 
     * @return void
     */
    public function __construct()
    {
        // переменные запроса
        $this->_data = &$_SERVER;
    }

    /**
     * Возращает указатель на созданный экземпляр класса (если класс не создан, создаст его)
     * 
     * @return mixed
     */
    public static function getInstance()
    {
        if (null === self::$_instance)
            self::$_instance = new self();

        return self::$_instance;
    }

    /**
     * Возращает значение ключа из метода GET, если ключ
     * не найден - возращает значение по умолчанию
     * 
     * @param  string $key ключ в массиве GET
     * @param  string $default значение по умолчанию
     * @return mixed
     */
    public function get($name, $default = '')
    {
        if (isset($this->_data[$name]))
            return $this->_data[$name];
        else
            return $default;
    }

     /**
     * Проверка существования переменной $name сессии
     * 
     * @param  string $name имя переменной
     * @return boolean
     */
    public function has($name)
    { 
        return isset($this->_data[$name]);
    }

    /**
     * Возращает IP адрес
     * 
     * @return string
     */
    public static function getRemoteIp()
    {
        if ($this->has('HTTP_X_FORWARDED_FOR')) return $this->_data['HTTP_X_FORWARDED_FOR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) return $_SERVER['HTTP_X_FORWARDED_FOR'];
        if (isset($_SERVER['HTTP_CLIENT_IP'])) return $_SERVER['HTTP_CLIENT_IP'];
        if (isset($_SERVER['REMOTE_ADDR'])) return $_SERVER['REMOTE_ADDR'];
    }
}
?>