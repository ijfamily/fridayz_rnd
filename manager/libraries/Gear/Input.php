<?php
/**
 * Gear Manager
 * 
 * Пакет обработки запросов форм
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Libraries
 * @package    Input
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Input.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * Класс обработки запросов форм
 * 
 * @category   Libraries
 * @package    Input
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Input.php 2016-01-01 15:00:00 Gear Magic $
 */
class GInput
{
    /**
     * Данные полученные по методу "PUT"
     *
     * @var array
     */
    public $put;

    /**
     * Данные полученные по методу "DELETE"
     *
     * @var array
     */
    public $delete;

    /**
     * Данные полученные по методу "POST"
     *
     * @var array
     */
    public $post;

    /**
     * Сылка на экземпляр класса
     *
     * @var mixed
     */
    protected static $_instance = null;

    /**
     * Данные запроса
     *
     * @var string
     */
    protected $_data = array();

    /**
     * Метод запроса
     *
     * @var string
     */
    public $method = '';

    /**
     * Указатель на экземпляр класса файла
     *
     * @var object
     */
    public $file = null;


    /**
     * Если используюется RESTful
     *
     * @var boolean
     */
    public $RESTful = false;

    /**
     * Конструктор
     * 
     * @return void
     */
    public function __construct()
    {
        // определение метода запроса
        $this->defineMethod();
        if (sizeof($_FILES) > 0)
            $this->file = new GInput_File();
    }

    /**
     * Возращает указатель на созданный экземпляр класса (если класс не создан, создаст его)
     * 
     * @return mixed
     */
    public static function getInstance()
    {
        if (null === self::$_instance)
            self::$_instance = new self();

        return self::$_instance;
    }

    /**
     * Вызов метода класса
     * 
     * @param  array $name название метода
     * @param  array $arguments аргументы метода
     * @return void
     */
    public function __call($name, $arguments)
    {
        if (sizeof($arguments) == 1)
            $arguments[1] = '';
        switch ($name) {
            case 'getInt': return (int) $this->get($arguments[0], $arguments[1]);

            case 'getBool': return (bool) $this->get($arguments[0], $arguments[1]);

            case 'getString':return (string) $this->get($arguments[0], $arguments[1]);

            case 'getFloat': return (float) $this->get($arguments[0], $arguments[1]);

            case 'getDate':
                $str = $this->get($arguments[0], $arguments[1]);
                if ($str)
                    return date('Y-m-d', strtotime($str));
                else
                    return '';
        }
    }

    /**
     * Определение метода запроса
     * 
     * @return void
     */
    protected function defineMethod()
    {
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->_data = &$_REQUEST;
        // если RESTful установлен
        if (!$this->RESTful) {
            if ($this->method != 'GET') {
                if (isset($_POST['method'])) {
                    $this->method = $_SERVER['REQUEST_METHOD'] = $_POST['method'];
                    unset($_POST['method']);
                    unset($this->_data['method']);
                }
            }
        }

        // метод
        switch ($this->method) {
            // метод "PUT"
            case 'PUT': $this->put = $_POST; break;

            // метод "POST"
            case 'POST': $this->post = $_POST; break;

            // метод "DELETE"
            case 'DELETE': $this->delete = $_POST; break;
        }
    }

    /**
     * Возращает количество полей в запросе
     * 
     * @return integer
     */
    public function count()
    { 
        return count($this->_data);
    }

     /**
     * Проверка существования переменной $name в запросе
     * 
     * @param  string $name имя переменной
     * @return boolean
     */
    public function has($name)
    { 
        return isset($this->_data[$name]);
    }

     /**
     * Проверка существования переменной $name в запросе
     * 
     * @param  string $name имя переменной
     * @return boolean
     */
    public function hasFile()
    { 
        return $this->file != null;
    }

     /**
     * Возращает true если есть данные в запросе
     * 
     * @return boolean
     */
    public function hasData()
    {
        return (bool)(sizeof($this->_data) > 0);
    }

     /**
     * Проверка метода запроса (POST, GET)
     * 
     * @return boolean
     */
    public function isMethod($method)
    {
        return $this->method == $method;
    }

     /**
     * Возращает данные по одному из указанные методов запроса
     * 
     * @param  string $method метод запроса
     * @return array
     */
    public function getMethodData($method = 'POST')
    {
        // метод
        switch ($this->method) {
            // метод "PUT"
            case 'PUT': return $this->put;

            // метод "POST"
            case 'POST': return $this->post;

            // метод "DELETE"
            case 'DELETE': return $this->delete;
        }
    }

    /**
     * Есть ли в запросе данные по указанному методу
     * 
     * @param  string $method метод запроса
     * @param  string $keys список ключей
     * @return boolean
     */
    public function isEmpty($method = 'POST', $keys = array())
    {
        if (!$this->isMethod($method))
            return true;
        $count = sizeof($keys);
        $data = $this->getMethodData($method);
        if ($count > 0) {
            for ($i = 0; $i < $count; $i++)
                if (!empty($data[$keys[$i]]))
                    return false;
        } else
            return sizeof($data) == 0;

        return true;
    }

    /**
     * Возращает значение ключа из метода POST, если ключ
     * не найден - возращает значение по умолчанию
     * 
     * @param  string $key ключ в массиве GET
     * @param  string $default значение по умолчанию
     * @return mixed
     */
    public function get($name, $default = '')
    {
        if (isset($this->_data[$name]))
            return $this->_data[$name];
        else
            return $default;
    }

    /**
     * Возращает значение ключа из метода POST, если ключ
     * не найден - возращает значение по умолчанию
     * 
     * @param  string $key ключ в массиве GET
     * @param  string $default значение по умолчанию
     * @return mixed
     */
    public function getDate($field, $format = '', $default = '')
    {
       if (($date = $this->get($field, false)) === false)
           return $default;

       if (empty($date) || $date == 'NaN-NaN-0NaN')
           return null;
        if ($format) {
            return date($format, strtotime($date));
        }

      return $date;
    }

    public function getBy($fields)
    {
        $count = sizeof($fields);
        $result = array();
        for ($i = 0; $i < $count; $i++) {
            if ($this->has($fields[$i]))
                $result[$fields[$i]] = $this->_data[$fields[$i]];
        }

        return $result;
    }

    /**
     * Устанавливает значение ключа для $_REQUEST
     * 
     * @param  string $name ключ
     * @param  string $value значение ключа, если null - удаляет ключ
     * @return mixed
     */
    public function set($name, $value = null)
    {
        if ($value == null)
            unset($this->_data[$name]);
        else
            $this->_data[$name] = $value;
    }

    /**
     * Возращает метод запроса
     * 
     * @return string
     */
    public function getMethod()
    {
        return strtoupper($_SERVER['REQUEST_METHOD']);
    }

    /**
     * Возращает часть sql запроса для проверки ip адреса в базе
     * 
     * @param  string $ipAddr ip адрес
     * @param  string $fieldIp поле с ip адресом
     * @param  string $fieldMask поле с маской
     * @return string
     */
    public function getQueryIp($ipAddr, $fieldIp, $fieldMask)
    {
        $ipInINT = ip2long($ipAddr);
        $filter  = '';
        $mask      = 0;
        $separator = '';
        for ($bits = 0; $bits <= 32; $bits++ ) {
            $network = $ipInINT & $mask;
            $filter .= $separator . sprintf("($fieldIp = %u AND $fieldMask = %u)", $network, $mask);
            $separator = ' OR ';
            $mask = ($mask >> 1) | 2147483648;
        }

        return $filter;
    }
}


/**
 * Класс загрузки файлов из формы
 * 
 * @category   Libraries
 * @package    Input
 * @subpackage File
 * @copyright  Copyright (c) 2013-2014 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Input.php 2014-08-01 12:00:00 Gear Magic $
 */
class GInput_File
{
    /**
     * Конструктор
     * 
     * @return void
     */
    public function __construct()
    {
        // данные запроса
        $this->_data = &$_FILES;
    }

    /**
     * Возращает значение ключа из $_FILES, если $name
     * не найден - возращает false
     * 
     * @param  string $name имя поля выбора файла
     * @return mixed
     */
    public function get($name)
    {
        if (isset($this->_data[$name])) {
            if (empty($this->_data[$name]['name']) && empty($this->_data[$name]['tmp_name']))
                return false;
            else
                return $this->_data[$name];
        } else
                return false;
    }

    /**
     * Возращает Mime-тип файла
     * 
     * @param  string $name имя поля выбора файла
     * @return mixed
     */
    public function getType($name)
    {
        if (($file = $this->get($name)) === false)
            return false;
        else
            return $file['type'];
    }

    /**
     * Возращает размер в байтах принятого файла
     * 
     * @param  string $name имя поля выбора файла
     * @return mixed
     */
    public function getSize($name)
    {
        if (($file = $this->get($name)) === false)
            return false;
        else
            return $file['size'];
    }

    /**
     * Возращает имя загружаемого файла
     * 
     * @param  string $name имя поля выбора файла
     * @return mixed
     */
    public function getName($name)
    {
        if (($file = $this->get($name)) === false)
            return false;
        else
            return $file['name'];
    }

    /**
     * Возращает временное имя
     * 
     * @param  string $name имя поля выбора файла
     * @return mixed
     */
    public function getTmpName($name)
    {
        if (($file = $this->get($name)) === false)
            return false;
        else
            return $file['tmp_name'];
    }

    /**
     * Возращает ошибку которая может возникнуть при загрузке файла
     * 
     * @param  string $name имя поля выбора файла
     * @return mixed
     */
    public function getError($name)
    {
        if (($file = $this->get($name)) === false)
            return false;
        else
            return $file['error'];
    }

    /**
     * Возращает расширение файла
     * 
     * @param  string $name имя поля выбора файла
     * @return mixed
     */
    public function getExtension($name)
    {
        if (($file = $this->get($name)) === false)
            return false;
        else
            return strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
    }

    /**
     * Проверка существования расширения
     * 
     * @param  string $name имя поля выбора файла
     * @param  array $exts расширения
     * @return mixed
     */
    public function isValidExtension($name, $exts)
    {
        if (($ext = $this->getExtension($name)) === false) return false;

        return in_array($ext, $exts);
    }

    /**
     * Возращает ошибку которая может возникнуть при загрузке файла
     * 
     * @param  string $name имя поля выбора файла
     * @return mixed
     */
    public function getErrorMsg($name)
    {
        if ($code = $this->getError($name) == false)
            return false;
        switch ($code) {
            case UPLOAD_ERR_INI_SIZE: return 'Upload file size exceeded the UPLOAD_MAX_FILESIZE';
            case UPLOAD_ERR_FORM_SIZE: return 'Upload file size exceeded the MAX_FILE_SIZE';
            case UPLOAD_ERR_PARTIAL: return 'The uploaded file was only partially loaded';
            case UPLOAD_ERR_NO_FILE: return 'File was not loaded';
            case UPLOAD_ERR_OK: return '';
        }
        

        return 'Unknow file error';
    }

    /**
     * Был ли выбран файл клиентом
     * 
     * @param  string $name имя поля выбора файла
     * @return boolean
     */
    public function has($name)
    {
        if (empty($this->_data[$name])) return false;
        if (empty($this->_data[$name]['tmp_name'])) return false;

        return true;
    }

    /**
     * Проверка на наличие ошибки в загрузке файла
     * 
     * @param  string $name имя поля выбора файла
     * @return boolean
     */
    public function hasError($name)
    {
        if (($code = $this->getError($name)) === false)
            return true;

        if ($code != UPLOAD_ERR_OK)
            return true;

        return false;
    }

    /**
     * Возращает количество файлов
     * 
     * @return integer
     */
    public function count()
    { 
        return count($this->_data);
    }

    /**
     * Возращает загруженный контент файла
     * 
     * @param  string $name имя поля выбора файла
     * @param  array $exts допустимые расширения файла
     * @return string
     */
    public function getContent($name, $exts)
    {
        try {
            if (!$this->has($name))
                throw new GException('Loading data', 'File was not loaded');
            if ($error = $this->getErrorMsg($name))
                throw new GException('Loading data', $error);
            if (!$this->isValidExtension($name, $exts))
                throw new GException('Loading data', 'The download does not match the expansion');
        } catch(GException $e) {}

         return file_get_contents($this->getTmpName($name), true);
    }

    /**
     * Возращает имя файла
     * 
     * @param  string $name имя поля выбора файла
     * @param  string $prefix префикс имени
     * @param  boolean $ext добавлять расширение файла
     * @param  string $method метод генерации файла ("time", "microtime", "uniqid")
     * @return string
     */
    public function createName($name, $prefix, $ext = true, $method = 'time')
    {
        $file = $this->get($name);
        if ($file === false) return '';

        if ($method == 'time')
            $fileId = $prefix . date('Ymd') . '_' . date('Hi');
        else
        if ($method == 'microtime')
            $fileId = microtime();
        else
        if ($method == 'uniqid')
            $fileId = uniqid(rand(), true);

        $fileExt = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
        

        if ($ext)
            return $fileId . '.' . $fileExt;
        else
            return $fileId;
    }

    /**
     * Возращает загруженный контент файла
     * 
     * @param  string $name имя поля выбора файла
     * @param  string $path путь или новое название файла
     * @return void
     */
    public function uploadTo($name, $path, $exts = array())
    {
        try {
            if (!$this->has($name))
                throw new GException('Error', 'File was not loaded');
            if ($this->hasError($name))
                throw new GException('Error', $this->getErrorMsg($name));
            if ($exts)
                if (!$this->isValidExtension($name, $exts))
                    throw new GException('Error', 'The download does not match the expansion');
            $file = $this->get($name);
            if (is_dir($path))
                $path .= basename($file['name']);
            if (!move_uploaded_file($file['tmp_name'], $path))
                throw new GException('Error', 'Unable to move file "%s"', $path);
        } catch(GException $e) {}
    }
}
?>