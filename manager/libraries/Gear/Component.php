<?php
/**
 * Gear Manager
 *
 * Компоненты
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Library
 * @package    Component
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Component.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * Класс компонента
 * 
 * @category   Library
 * @package    Component
 * @subpackage Base
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Component.php 2016-01-01 15:00:00 Gear Magic $
 */
class GComponent
{
    /**
     * Возращает компонент
     * 
     * @param array $data данные для генерации компонента
     * @return void
     */
    public static function get($name, $data)
    {
        $type = isset($data['type_xtype']) ? $data['type_xtype'] : '';
        $arr = array();
        switch ($type) {
            case 'textfield':
                $arr = array(
                    'xtype' => $type
                );
                break;

            case 'combo':
                $items = array();
                if ($data['property_list']) {
                     $list = explode(';', $data['property_list']);
                     for ($i = 0; $i < sizeof($list); $i++) {
                        $items[] = array($list[$i], $list[$i]);
                     }
                }
                $arr = array(
                    'xtype'         => 'combo',
                    'editable'      => false,
                    'typeAhead'     => true,
                    'triggerAction' => 'all',
                    'mode'          => 'local',
                    'store'         => array(
                        'xtype'  => 'arraystore',
                        'fields' => array('value', 'display'),
                        'data'   => $items
                    ),
                    'hiddenName'    => $name,
                    'valueField'    => 'value',
                    'displayField'  => 'display'
                );
                break;

            case 'textarea':
                $arr = array(
                    'xtype' => $type
                );
                break;
        }
        if (!empty($data['property_width']))
            $arr['width'] = $data['property_width'];
        if (!empty($data['property_height']))
            $arr['height'] = $data['property_height'];
        if (!empty($data['property_allowblank']))
            $arr['allowBlank'] = (bool) $data['property_allowblank'];
        if (!empty($data['property_default']))
            $arr['default'] = $data['property_default'];
        $arr['fieldLabel'] = $data['property_name'];
        $arr['name'] = $name;

        return $arr;
    }

    /**
     * Запись настроек компонента в файла
     * 
     * @param string $id идент. компонента
     * @param array $data данные компонента
     * @return void
     */
    public static function saveToConfig($components)
    {
        /*
        foreach ($components as $id => $component) {
            $path = '../cache/components/' . $id . '.tmp';
            if (file_put_contents($path, serialize($component), FILE_TEXT) === false)
                throw new GException('Data processing error', 'Could not write the file to disk');
        }
        */
    }
}
?>