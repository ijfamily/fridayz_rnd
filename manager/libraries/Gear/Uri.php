<?php
/**
 * Gear Manager
 * 
 * Пакет обработки запросов клиентов
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Libraries
 * @package    Uri
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Uri.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * Класс URI ресурса
 * 
 * @category   Libraries
 * @package    Uri
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Uri.php 2016-01-01 15:00:00 Gear Magic $
 */
class GUri
{
    /**
     * Использовать в запросах последний символ "/"
     *
     * @var boolean
     */
    public $slash = true;

    /**
     * Путь (полученный из URL)
     *
     * @var string
     */
    static public $path = '/';

    /**
     * Запрос клиента url ("/{path}/{file}")
     *
     * @var string
     */
    static protected $requestPath = '';

    /**
     * Путь разбитый на сегменты
     *
     * @var array
     */
    protected $_segments = array();

    /**
     * URI полученный из $_SERVER['REQUEST_URI']
     *
     * @var string
     */
    static public $request = '';

    /**
     * Разобранный URL запрос
     * scheme - e.g. http, host, port, user, pass, path, query, fragment, prefix, filename, ext
     * 
     * @var array
     */
    static public $components = array();

    /**
     * Сылка на экземпляр класса
     *
     * @var mixed
     */
    protected static $_instance = null;

    /**
     * Использование в запросах RESTful
     *
     * @var boolean
     */
    public $RESTful = false;

    /**
     * Идентификатор записи(ей) в запросе
     *
     * @var mixed
     */
    public $id = '';

    /**
     * Название web сервера
     *
     * @var string
     */
    public $webServer = '';

    /**
     * Запрос клиента
     *
     * @var string
     */
    protected $_uri = '';

    /**
     * Путь к контроллеру ("Interface", "Data", ...)
     *
     * @var string
     */
    public $controller = '';

    /**
     * Действие над контроллером в запросе ("Interface", "Data", ...)
     *
     * @var string
     */
    public $action = '';

    /**
     * Конструктор
     * 
     * @param string $uri запрос клиента
     * @return void
     */
    public function __construct($uri = '')
    {
        $this->_uri = $uri;
        // метода запроса
        $this->method = strtoupper($_SERVER['REQUEST_METHOD']);
        // переменные запроса
        $this->_vars = &$_GET;
        // определение метода запроса
        $this->defineMethod();
        $this->RESTful = GFactory::getConfig()->get('RESTFUL', false);
        $this->RESTful = (isset($params['restful'])) ? $params['restful'] : false;
        // определение запроса пользователя
        $this->defineRequest();
    }

    /**
     * Повторное определение параметров запроса
     * 
     * @param string $uri запрос клиента
     * @return void
     */
    public function define($uri = '')
    {
        $this->_uri = $uri;
        $this->defineRequest();
    }

    /**
     * Возращает указатель на созданный экземпляр класса (если класс не создан, создаст его)
     * 
     * @return mixed
     */
    public static function getInstance()
    {
        if (null === self::$_instance)
            self::$_instance = new self();

        return self::$_instance;
    }

    /**
     * Проверка на ssl
     * 
     * @return boolean
     */
    public function isSSL()
    {
        if (isset($_SERVER['HTTPS'])) {
            return !empty($_SERVER['HTTPS']) && stristr($_SERVER['HTTPS'], 'off') === false;
        } else
            if (isset($_SERVER['SERVER_PORT'])) {
                return '443' == $_SERVER['SERVER_PORT'];
            }

        return false;
    }

    /**
     * Возращает название хоста
     * 
     * @return string
     */
    public function getHost()
    {
        return ($this->isSSL() ? 'https://' : 'http://') . $_SERVER['SERVER_NAME'];
    }

    /**
     * Получение параметров для каждого метода запроса
     * 
     * @return void
     */
    protected function defineMethod()
    {
        // если не установлен RESTful
        if (!$this->RESTful)
            if ($this->method != 'GET')
                if (isset($_POST['method'])) {
                    $this->method = $_SERVER['REQUEST_METHOD'] = $_POST['method'];
                    unset($_POST['method']);
                }
    }

    /**
     * Возращает один из компонентов запроса
     * 
     * @param  string $name параметр запроса
     * @return void
     */
    public static function request($name = '')
    {
        if (empty($name))
            return self::$requestPath;

        switch ($name) {
            case '/': return self::$requestPath;
            case 'full': return self::$request;
            case 'url': return self::$components;

            default:
                if (isset(self::$components[$name]))
                    return self::$components[$name];
                else
                    return self::$request;
        }
    }

    /**
     * Возращает один из компонентов запроса
     * 
     * @param  string $name параметр запроса
     * @return void
     */
    public function hasId()
    {
        return !empty($this->id);
    }

    /**
     * Возращает один из компонентов запроса
     * 
     * @param  string $name параметр запроса
     * @return void
     */
    public static function get($name, $default = '')
    {
        if (empty($name))
            return false;

        switch ($name) {

            default:
                if (isset(self::$components[$name]))
                    return self::$components[$name];
                else
                    return $default;
        }
    }

    /**
     * Определение запроса пользователя
     * 
     * @return void
     */
    protected function defineRequest()
    {
        if (strlen($this->_uri)) {
            $router = explode(ROUTER_DELIMITER, $this->_uri);
        } else {
            if ($this->webServer == 'apache') {
                if (!isset($_SERVER['PATH_INFO']))
                    throw new GException('Error', 'System can not handle expected by requests from your Web server');
                $this->pathInfo = $_SERVER['PATH_INFO'];
                if (!isset($_SERVER['SCRIPT_NAME']))
                    throw new GException('Error', 'System can not handle expected by requests from your Web server');
                $this->scriptName = $_SERVER['SCRIPT_NAME'];
            // nginx ...
            } else {
                list($this->scriptName, $this->pathInfo) = explode('.php', $_SERVER['PHP_SELF']);
                $this->scriptName = $this->scriptName . '.php';
            
            }
            $router = explode(ROUTER_DELIMITER, $this->pathInfo);
        }
        if (isset($router[1]))
            $routerPrefix = $router[1];
        else
            $routerPrefix = '';

        if (!empty($routerPrefix)){
            if ($this->slash) {
                // /controller/action[0..9]/1,2,...
                $cai = '/^([a-zA-Z]+\w)\/([a-zA-Z0-9]+\w)\/([0-9,]+)$/';
                // /controller/action[0..9]
                $ca =  '/^([a-zA-Z]+\w)\/([a-zA-Z0-9]+)\/$/';
                // /controller/1,2,...
                $ci = '/^([a-zA-Z]+\w)\/([0-9,]+)$/';
                // /controller
                $c =  '/^([a-zA-Z]+\w)\/$/';
                // /1,2,...
                $i =  '/^([0-9,]+)$/';
            } else {
                // /controller/action[0..9]/1,2,...
                $cai = '/^\/([a-zA-Z]+\w)\/([a-zA-Z0-9]+\w)\/([0-9,]+)$/';
                // /controller/action[0..9]
                $ca =  '/^\/([a-zA-Z]+\w)\/([a-zA-Z0-9]+)$/';
                // /controller/1,2,...
                $ci = '/^\/([a-zA-Z]+\w)\/([0-9,]+)$/';
                // /controller
                $c =  '/^\/([a-zA-Z]+\w)$/';
                // /1,2,...
                $i =  '/^\/([0-9,]+)$/';
            }
            $matches = array();
            if (preg_match($cai, $routerPrefix, $matches)) {
                $this->controller = $matches[1];
                $this->action = $matches[2];
                $this->id = $matches[3];
            } else if (preg_match($ca, $routerPrefix, $matches)) {
                $this->controller = $matches[1];
                $this->action = $matches[2];
            } else if (preg_match($ci, $routerPrefix, $matches)) {
                $this->controller = $matches[1];
                $this->id = $matches[2];
            } else if (preg_match($c, $routerPrefix, $matches)) {
                $this->controller = $matches[1];
            } else if (preg_match($i, $routerPrefix, $matches)) {
                $this->id = $matches[1];
            }
        }
        // "path/.../path/"
        $this->component = ltrim($router[0], '/');
    }

    /**
     * Возращает сегмента пути полученного из URI для 
     * формирования интерфейса контроллера из логов запроса
     * 
     * @return string
     */
    public function getCntUriInfo()
    {
        if ($this->action == 'data')
            return $this->component . ROUTER_DELIMITER . $this->controller . '/interface/' . $this->id;
        else
            return $this->pathInfo;
    }

    /**
     * Возращает сегмента пути полученного из URI для 
     * формирования интерфейса контроллера из логов запроса
     * 
     * @return string
     */
    public function getCntUri()
    {
        return $this->component . ucfirst($this->controller) . '/';
    }

    /**
     * Возращает часть пути сегмента полученного из URI
     * 
     * @param  integer $index номер сегмента
     * @param  string $default значение по умолчанию если сегмента нет
     * @return mixed
     */
    public function getSeg($index = 0, $default = '')
    {
        // если есть выбранный сегмент
        if (isset($this->_segments[$index]))
            return $this->_segments[$index];

        return $default;
    }

    /**
     * Проверка сегмента URL на валидность
     * 
     * @param  integer $index номер сегмента (от 1 и до ...)
     * @param  string $path название сегмента
     * @param  integer $size кол-о всех сегментов
     * @return boolean
     */
    public function checkSeg($index, $path, $size = 0)
    {
        if ($size)
            return $this->getSeg($index) == $path && $this->getSegtSize() == $size;

        return $this->getSegment($index) == $path;
    }

    /**
     * Проверка сегмента URL на валидность
     * 
     * @param  string $name название сегмента (/path, /path/path1, ...)
     * @param  integer $check тип проверки
     * @return boolean
     */
    public function isSeg($name, $check = 'equal')
    {
        switch ($check) {
            // если сегмент это файл
            case 'file': return $name == self::$components['filename'];

            // если сегмент равный части URL
            case 'equal': return $name == self::$path;

            // если сегмент вначале URL
            case 'first':
                $p = strpos(self::$path, $name);
                return $p !== false && $p == 0;

            // если сегмент это часть URL
            case 'part':
                $p = strpos(self::$path, $name);
                if ($p !== false && $p != 0) {
                    if ($p !== false) {
                        $l = strlen($name);
                        $lp = strlen(self::$path);
                        if ($lp - $p != $l)
                            return true;
                    }
                }
                return false;

            // если сегмент вконце URL
            case 'last':
                $p = strpos(self::$path, $name);
                if ($p !== false) {
                    $l = strlen($name);
                    $lp = strlen(self::$path);
                    if ($lp - $p == $l)
                        return true;
                }
                return false;

            default:
                return false;
        }
    }

    /**
     * Возращает колич-о сегментов
     * 
     * @return integer
     */
    public function getSegSize()
    {
        return sizeof($this->_segments);
    }

    /**
     * Возращает все сегменты
     * 
     * @return array
     */
    public function getSegments()
    {
        return $this->_segments;
    }

     /**
     * Проверка существования переменной $name сессии
     * 
     * @param  string $name имя переменной
     * @return boolean
     */
    public function hasVar($name)
    { 
        return isset($this->_vars[$name]);
    }

     /**
     * Установка значения $value для ключа $name в GET
     * 
     * @param  string $name имя переменной
     * @param  mixed $value значение
     * @return void
     */
    public function setVar($name, $value = null)
    {
        $old = isset($this->_vars[$name]) ? $this->_vars[$name] : null;

        if ($value === null)
            unset($this->_vars[$name]);
        else
            $this->_vars[$name] = $value;
    }

    /**
     * Возращает значение ключа из метода GET, если ключ
     * не найден - возращает значение по умолчанию
     * 
     * @param  string $key ключ в массиве GET
     * @param  string $default значение по умолчанию
     * @return mixed
     */
    public function getVar($name, $default = '')
    {
        if (isset($this->_vars[$name]))
            return $this->_vars[$name];
        else
            return $default;
    }

    /**
     * Проверка метода запроса
     * 
     * @return boolean
     */
    public function isMethod($method)
    {
        return $this->method == $method;
    }
}
?>