<?php
/**
 * Gear Manager
 * 
 * Пакет классов для сборки лендинг страниц
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Libraries
 * @package    Landing
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Landing.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * Класс работы с лендинг страницей
 * 
 * @category   Libraries
 * @package    Landing
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Landing.php 2016-01-01 15:00:00 Gear Magic $
 */
final class GLanding
{
    /**
     * Удаление блоков страницы лендинга
     * 
     * @param integer $articleId идент. статьи
     * @param object $query указатель на запрос
     * @return void
     */
    public static function removePage($articleId, $query = null)
    {
        if (is_null($query))
            $query = new GDb_Query();

        // удалить блоки
        $sql = 'DELETE FROM `site_landing_pages` WHERE `article_id`=' . $articleId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        // удаление записей таблицы "site_landing_pages" (блоки)
        if ($query->dropAutoIncrement('site_landing_pages') === false)
            throw new GSqlException();
        // обновить статью
        $sql = 'UPDATE `site_articles` SET `article_landing`=0 WHERE `article_id`=' . $articleId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        // обновить страницы
        $sql = 'UPDATE `site_pages` SET `page_html`=null, `page_html_plain`=null, `page_html_length`=0, `page_parsing`=0 WHERE `article_id`=' . $articleId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Возращает последний индекс блока лендинга
     * 
     * @param integer $articleId идент. статьи
     * @param object $query указатель на запрос
     * @return integer индекс
     */
    public static function getLastBlockIndex($articleId, $query = null)
    {
        if (is_null($query))
            $query = new GDb_Query();

        // последний индекс
        $sql = 'SELECT MAX(`block_index`) `max` FROM `site_landing_pages` WHERE `article_id`=' . $articleId;
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();

        return $rec['max'];
    }

    /**
     * Сборка блоков страницы лендинга
     * 
     * @param integer $articleId идент. статьи
     * @param object $query указатель на запрос
     * @return integer колю блоков на странице
     */
    public static function updatePage($articleId, $query = null)
    {
        if (is_null($query))
            $query = new GDb_Query();

        // выбираем все блоки лендинга
        $sql = 'SELECT * FROM `site_landing_pages` WHERE `block_visible`=1 AND `article_id`=' . $articleId . ' ORDER BY `block_index` ASC';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $blocks = array();
        while (!$query->eof()) {
            $rec = $query->next();
            $blocks[$rec['block_index']] = $rec;
        }
        // перебор блоков
        $index = 1;
        foreach ($blocks as $id => $rec) {
            $sql = 'UPDATE `site_landing_pages` SET `block_index`=' . $index . ' WHERE `block_id`=' . $rec['block_id'];
            if ($query->execute($sql) === false)
                throw new GSqlException();
            $index++;
        }

        return sizeof($blocks);
    }

    /**
     * Сборка блоков страницы лендинга
     * 
     * @param integer $article статья
     * @param object $query указатель на запрос
     * @return void
     */
    public static function collectPage($article, $componentsAttr, $query = null)
    {
        if (is_null($query))
            $query = new GDb_Query();

        // язык поумолчанию
        $languageId = GSettings::getModule('site', 'language/default/id');
        // атрибуты компонентов для выбранного языка
        if (isset($componentsAttr[$languageId]))
            $componentsAttr = $componentsAttr[$languageId];
        // заново выбираем статью, т.к. могла измениться
        $sql = 'SELECT `p`.*, `a`.* '
             . 'FROM `site_articles` `a` JOIN `site_pages` `p` ON `p`.`article_id`=`a`.`article_id` AND '
             . '`p`.`language_id`=' . $languageId . ' AND `a`.`article_id`=' . $article['article_id'];
        if (($article = $query->getRecord($sql)) === false)
            throw new GSqlException();
        if (empty($article))
            throw new GException('Warning', 'The article not exist!');

        // выбираем все блоки лендинга
        $sql = 'SELECT * FROM `site_landing_pages` WHERE `block_visible`=1 AND `article_id`=' . $article['article_id'] . ' ORDER BY `block_index` ASC';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $text = '';
        while (!$query->eof()) {
            $rec = $query->next();
            $blockText = trim($rec['block_text']);
            if (!empty($blockText))
                $text .= $blockText . "\r\n";
        }
        $table = new GDb_Table('site_pages', 'article_id');
        // если в блоках есть текст
        if ($text) {
            // если нет компонентов в статье
            if (empty($article['component_attributes']))
                $cmps = array();
            else
                $cmps = json_decode($article['component_attributes'], true);
            $cmps = array_merge($cmps, $componentsAttr);
            $plain = strip_tags($text);
            $plain = preg_replace("/\{image[0-9]\}/", '', $plain);
            $data = array(
                'page_html'        => $text,
                'page_html_plain'  => trim($plain),
                'page_html_length' => mb_strlen($text, 'UTF-8')
            );
            // поиск динамических компонентов в статье
            if (mb_strpos($text, '<component', 0, 'UTF-8') !== false) {
                $data['page_parsing'] = 1;
                // обновление атрибутов компонентов в статье
                if ($cmps) {
                    // проверка существования компонентов в статье
                    foreach ($cmps as $id => $attr) {
                        if (mb_strpos($text, $id, 0, 'UTF-8') === false)
                            unset($cmps[$id]);
                    }
                    if (empty($cmps))
                        $data['component_attributes'] = null;
                    else
                        $data['component_attributes'] = json_encode($cmps);
                }
            } else {
                $data['page_parsing'] = 0;
                $data['component_attributes'] = null;
            }
            // обновить статью
            if ($table->update($data, $article['article_id']) === false)
                throw new GSqlException();
        // если нет текста, сбросить параметры страницы
        } else {
            $data = array(
                'page_html'        => null,
                'page_html_plain'  => null,
                'page_html_length' => 0,
                'page_parsing'     => 0
            );
            // обновить статью
            if ($table->update($data, $article['article_id']) === false)
                throw new GSqlException();
        }
    }
}
?>