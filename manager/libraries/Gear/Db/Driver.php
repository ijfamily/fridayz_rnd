<?php
/**
 * Gear Manager
 *
 * ������� ���� ������
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Database
 * @package    Driver
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Driver.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * ����� �������� ����������� � ������� ���� ������
 * 
 * @category   Database
 * @package    Driver
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Driver.php 2016-01-01 12:00:00 Gear Magic $
 */
class GDb_Driver
{
    /**
     * ������� ����������� � "MySQL"
     */
    const MYSQL   = 'MySQL';

    /**
     * ������� ����������� � "MSSQL"
     */
    const MSSQL   = 'MSSQL';

    /**
     * ������� ����������� � "MYSQLi"
     */
    const MYSQLI  = 'MYSQLI';

    /**
     * ������� ����������� � "ODBC"
     */
    const ODBC    = 'ODBC';

    /**
     * ������� ����������� � "SQLite"
     */
    const SQLITE  = 'SQLite';

    /**
     * ������� ����������� � "Oracle server"
     */
    const OCI8    = 'OCI8';

    /**
     * ������� ����������� � "PostgreSQL"
     */
    const POSTGRE = 'PostgreSQL';

    /**
     * �������� ��������
     *
     * @var string
     */
    public static $name = '';

    /**
     * ��������� �����������
     *
     * @var array
     */
    public static $config = array();

    /**
     * ����� �� ��������� ������ ���� ������
     *
     * @var object
     */
    public static $db = null;

    /**
     * ����� �� ��������� ������ ���� ������
     *
     * @var object
     */
    protected static $log = null;

    protected static $_isLoaded = false;

    /**
     * ������������� ������� ��� ������ � ����� ������
     * 
     * @param  string $driver ������� ����������� � ������� ���� ������
     * @return void
     */
    public static function loadDriver($driver, $name = '')
    {
        if ($name)
            require_once('Drivers/' . $driver . '/' . $name . '.php');
        else {
            // ����������� �������� ��� ������ � �������� ���� ������
            require_once('Drivers/' . $driver . '/Db.php');
            // ����������� �������� ��� ������ � SQL ��������� ���� ������
            require_once('Drivers/' . $driver . '/Query.php');
            // ����������� �������� ��� ������ � SQL ��������� ���� ������
            require_once('Drivers/' . $driver . '/Table.php');
        }
    }

    /**
     * ��������� ��������� �� ��������� ������ ���� ������
     * 
     * @param  array $config ��������� �����������
     * @param  string $driver �������� �������� ���� �����
     * @return object
     */
    public static function factory($name)
    {
        if (self::$$name !== null)
            return self::$$name;
        $file = ucfirst($name);
        $class = 'GDb_' . $file;
        self::loadDriver(self::$name, $file);
        return self::$$name = new $class();
    }

    /**
     * ��������� ��������� �� ��������� ������ ���� ������
     * 
     * @param  array $config ��������� �����������
     * @param  string $driver �������� �������� ���� �����
     * @return object
     */
    public static function getDb($config, $conName = 'default', $driver = self::MYSQL)
    {
        if (self::$db != null)
            return self::$db;

        // �������� ��������
        self::$name = isset($config['DB/DRIVER']) ? $config['DB/DRIVER'] : $driver;
        // ��������� �����������
        self::$config = $config;
        // ������������� ������� ��� ������ � ����� ������
        self::loadDriver(self::$name);
        // ������������� �������� ����������� � ������� ���� ������
        return self::$db = new GDb($config['DB'], $conName);
    }
}
?>