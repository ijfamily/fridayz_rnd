<?php
/**
 * Gear Manager
 * 
 * ������� ����������� � ������� ���� ������ "MySQL"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   MySQL
 * @package    Database
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Db.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * @see GDb_Abstract
 */
require_once('Gear/Db/Drivers/Db.php');

/**
 * ����� �������� ����������� � ������� ���� ������ "MySQL"
 * 
 * @category   MySQL
 * @package    Database
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Db.php 2016-01-01 12:00:00 Gear Magic $
 */
class GDb extends GDb_Abstract
{
    /**
     * �������� ���������� � ��������
     * 
     * @param  integer $type ��� ���������� � ��������
     * @return void
     */
    public function connect($conName = 'default')
    {
        // ����������� �� ���������� � ��������
        if ($this->conName == $conName)
            if ($this->isConnected()) return;

        $this->initBy($conName);
        try {
            $this->_handle = @mysql_connect($this->server . ($this->port ? ':' . $this->port : ''), $this->username, $this->password);
            $error = iconv("cp1251", "UTF-8", $this->getError());
            if ($this->_handle === false)
                throw new GException('Data processing error', 'Error when making a connection to the server', array($error));
            // ����� ����� ���� ������
            if ($this->select() === false)
                throw new GException('Data processing error',  'Error when making a connection to the server', array($error));
            // ��������� ���������
            if ($this->setCharset() === false)
                throw new GException('Data processing error',  'Error when making a connection to the server', array($error));
        } catch(GException $e) {}
        // �������� ���������� � ��������
        $this->_connected = true;
    }

    /**
     * �������� ���������� � ��������
     * 
     * @return void
     */
    public function disconnect()
    {
        // ���� ���������� ���������� ������
        if (is_resource($this->_handle))
            return mysql_close($this->_handle);

        return false;
    }

    /**
     * ���������� ������ ��� ��������� ��������� SQL �������
     * 
     * @param  boolean $details ������ ��������� �� ������
     * @return mixed
     */
    public function getError($details = false)
    {
        if ($details) {
            if (!$this->_handle)
                return array('error' => mysql_error(), 'code' => mysql_errno());
            else
                return array('error' => mysql_error($this->_handle), 'code' => mysql_errno($this->_handle));
        } else
            if (!$this->_handle)
                return mysql_error();
            else
                return mysql_error($this->_handle);
    }

    /**
     * ����� ����� ���� ������
     * 
     * @param  string $schema ����� ���� ������
     * @return boolean
     */
    public function select($schema = '')
    {
        // ���� ����� �� �������, �� ������� �� �������� �����������
        if (empty($schema))
            $schema = $this->schema;

        return mysql_select_db($schema);
    }

    /**
     * ��������� ���������
     * 
     * @param  string $charset ��� ���������
     * @return boolean
     */
    public function setCharset($charset = '')
    {
        // ���� ��������� �� �������, �� ������� �� �������� �����������
        if (empty($charset))
            $charset = $this->charset;

         return mysql_set_charset($charset, $this->_handle);
    }

    /**
     * ���������� � ������� ���� ������
     * 
     * @return integer
     */
    public function getServerInfo()
    {
        return mysql_get_server_info($this->_handle);
    }

    /**
     * ���������� � ��������� ���� ������
     * 
     * @return integer
     */
    public function getProtoInfo()
    {
        return mysql_get_proto_info($this->_handle);
    }

    /**
     * ���������� � �����
     * 
     * @return srting
     */
    public function getHostInfo()
    {
        return mysql_get_host_info($this->_handle);
    }

    /**
     * ���������� � �������
     * 
     * @return string
     */
    public function getClientInfo()
    {
        return mysql_get_client_info();
    }
}
?>