<?php
/**
 * Gear Manager
 *
 * Журнал обработки SQL запросов сервера базы данных "MySQL"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   MySQL
 * @package    Log
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Db.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Журнал обработки SQL запросов сервера базы данных "MySQL"
 * 
 * @category   MySQL
 * @package    Log
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Db.php 2016-01-01 12:00:00 Gear Magic $
 */
class GDb_Log extends GDb_Table
{
    /**
     * Очистка данных
     */
    const LOG_CLEAR   = 'clear';

    /**
     * Удаление данных
     */
    const LOG_DELETE  = 'delete';

    /**
     * Вставка данных
     */
    const LOG_INSERT  = 'insert';

    /**
     * Выюорка данных
     */
    const LOG_SELECT  = 'select';

    /**
     * Обновления данных
     */
    const LOG_UPDATE  = 'update';

    /**
     * Создания бланков
     */
    const LOG_BLANK   = 'blank';

    /**
     * Экспорт данных
     */
    const LOG_EXPORT   = 'export';

    /**
     * Активность протокола
     * @var string
     */
    public $active = true;

    /**
     * Название таблицы
     * @var string
     */
    protected $_table = 'gear_log';

    /**
     * Первичный ключ таблицы
     * @var string
     */
    protected $_primary = 'log_id';

    /**
     * Конструктор
     * 
     * @return void
     */
    public function __construct()
    {
        parent::__construct($this->_table, $this->_primary, array());
    }

    /**
     * Конвертация данных в JSON
     * 
     * @param  boolean $v массив значений
     * @return mixed
     */
    public function toJSON($v = array())
    {
        $data = array();
        foreach ($v as $key => $value) {
            if (is_string($value)) {
                $data[$key] = $value;
            }
        }

        return $data;
    }

    /**
     * Добавление записи протокола
     * 
     * @param boolean $data массив значений
     * @return mixed
     */
    public function add($data = array())
    {
        if (!$this->active) return false;

        $params = array(
            'log_date' => date('Y-m-d'),
            'log_time' => date('H:i:s'),
            'log_url'  => $_SERVER['REQUEST_URI'],
            'user_id'  => GFactory::getSession()->get('user_id')
        );
        if ($this->insert(array_merge($data, $params)) === false)
            throw new GException('Data processing error', 'Query error (Internal Server Error)');
    }

    /**
     * Активность протокола
     * 
     * @return void
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Активация протокола
     * 
     * @param boolean $active активность протокола
     * @return void
     */
    public function setActive($active)
    {
        $this->active = $active;
    }
}
?>