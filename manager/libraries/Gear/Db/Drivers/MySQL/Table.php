<?php
/**
 * Gear Manager
 * 
 * Обработка SQL запросов к таблицам базы данных
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   MySQL
 * @package    Table
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Table.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * @see GDb_Table_Abstract
 */
require_once('Gear/Db/Drivers/Table.php');

/**
 * Абстрактный класс обработки SQL запросов к таблицам базы данных
 * 
 * @category   MySQL
 * @package    Table
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Table.php 2016-01-01 12:00:00 Gear Magic $
 */
class GDb_Table extends GDb_Table_Abstract
{
    /**
     * Обновление записей таблицы
     * 
     * @param  string $table название таблицы базы данных
     * @param  array $where ассоциативный массив полей таблицы и их значений ("field1" => "value1", ...)
     * @param  string $primary первичный ключ (или любое поле) таблицы
     * @param  mixed $id идентификатор записи(ей) (например: "1" или "1,2,3,4")
     * @return mixed
     */
    protected function _isDataExist($table, $where = array(), $primary = '', $id = '')
    {
        // формирование полей и значений для условия в SQL запросе
        $conditions = '';
        if (is_array($where)) {
            $values = array();
            foreach ($where as $field => $value)
                $conditions .= ' AND ' . $field . '=' . $this->query->escapeStr($value);
        } else 
            $conditions = ' AND ' . $where;
        if ($id) {
            // если число
            if (is_integer($id))
                $conditions .= " \nAND `$primary`<>$id ";
            // если строка
            else
                $conditions .= " \nAND `$primary` NOT IN ($id) ";
        }
        // формирование SQL запроса
        $sql = 'SELECT COUNT(*) `total` FROM ' . $table . ' WHERE 1' . $conditions;
        $rec = $this->query->getRecord($sql);
        if (empty($rec))
            return false;
        else
            return $rec['total'] > 0;
    }

    /**
     * Обновление записей таблицы
     * 
     * @param  string $table название таблицы базы данных
     * @param  array $data ассоциативный массив полей таблицы и их значений ("field1" => "value1", ...)
     * @param  string $primary первичный ключ (или любое поле) таблицы
     * @param  mixed $id идентификатор записи(ей) (например: "1" или "1,2,3,4")
     * @param  array $orderby ассоциативный массив полей в сортеровке таблицы ("field1", "field2", ...)
     * @return mixed
     */
    protected function _update($table, $data = array(), $primary = '', $id = '',  $orderby = array())
    {
        // если нет данных для обновления
        if (empty($data))
            return false;
        // формирование условий в SQL запросе
        $conditions = '';
        if ($id) {
            // если число
            if (is_integer($id))
                $conditions = " \nWHERE `$primary`=$id ";
            // если строка
            else
                $conditions = " \nWHERE `$primary` IN ($id) ";
        }
        // формирование полей и значений для SQL запроса
        $values = array();
        foreach ($data as $field => $value)
            $values[] = $field . '=' . $this->query->escapeStr($value);
        // формирование SQL запроса
        $sql = 'UPDATE ' . $table . ' SET ' . implode(', ', $values) . $conditions;
        //die($sql);
        // если есть сортировка
        if (sizeof($orderby) > 0)
            $sql .= " \nORDER BY " . implode(', ', $orderby);

        return $this->query->execute($sql);
    }

    /**
     * Обновление записей таблицы по условию
     * 
     * @param string $table название таблицы базы данных
     * @param  array $data ассоциативный массив полей таблицы и их значений ("field1" => "value1", ...)
     * @param  mixed $where ассоциативный массив ("field1" => "value1", ...) или строка ("field1" = "value1" AND ...)
     * @param  array $orderby ассоциативный массив полей в сортеровке таблицы ("field1", "field2", ...)
     * @return mixed
     */
    protected function _updateBy($table, $data = array(), $where = array(), $orderby = array())
    {
        // если нет данных для обновления
        if (empty($data))
            return false;
        // формирование условий в SQL запросе
        $conditions = '';
        // если массив
        if (is_array($where)) {
            // формирование полей и значений для SQL запроса
            $values = array();
            foreach ($where as $field => $value)
                $values[] = $field . '=' . $this->query->escapeStr($value);
            $conditions = " \nWHERE " . implode(' AND ', $values);
        // если строка
        } else
            $conditions = " \nWHERE " . $where;
        // формирование полей и значений для SQL запроса
        $values = array();
        foreach ($data as $field => $value)
            $values[] = $field . '=' . $this->query->escapeStr($value);
        // формирование SQL запроса
        $sql = 'UPDATE ' . $table . ' SET ' . implode(', ', $values) . $conditions;
        // если есть сортировка
        if (sizeof($orderby) > 0)
            $sql .= " \nORDER BY " . implode(', ', $orderby);

        return $this->query->execute($sql);
    }

    /**
     * Удаление записей из таблицы
     * 
     * @param string $table название таблицы базы данных
     * @param  string $primary первичный ключ (или любое поле) таблицы
     * @param  mixed $id идентификатор записи(ей) (например: "1" или "1,2,3,4")
     * @param  mixed $limit количество записей, если нет необходимости - false
     * @return mixed
     */
    protected function _delete($table, $primary = '', $id = '', $limit = false)
    {
        // формирование условий в SQL запросе
        $conditions = '';
        if ($id) {
            // если число
            if (is_integer($id))
                $conditions = " \nWHERE `$primary`=$id ";
            // если строка
            else
                $conditions = " \nWHERE `$primary` IN ($id) ";
        }
        $limit = $limit === false ? '' : ' LIMIT ' . $limit;
        // формирование SQL запроса
        $sql = 'DELETE FROM ' . $table . $conditions . $limit;

        return $this->query->execute($sql);
    }

    /**
     * Удаление записей из таблицы по условию
     * 
     * @param string $table название таблицы базы данных
     * @param  mixed $where ассоциативный массив ("field1" => "value1", ...) или строка ("field1" = "value1" AND ...)
     * @param  mixed $limit количество записей, если нет необходимости - false
     * @return boolean
     */
    protected function _deleteBy($table, $where = array(), $limit = false)
    {
        // формирование условий в SQL запросе
        $conditions = '';
        // если массив
        if (is_array($where)) {
            // формирование полей и значений для SQL запроса
            $values = array();
            foreach ($where as $field => $value)
                $values[] = $field . '=' . $this->query->escapeStr($value);
            $conditions = " \nWHERE " . implode(' AND ', $values);
        // если строка
        } else
            $conditions = " \nWHERE " . $where;
        $limit = $limit === false ? '' : ' LIMIT ' . $limit;
        // формирование SQL запроса
        $sql = 'DELETE FROM ' . $table . $conditions . $limit;

        return $this->query->execute($sql);
    }

    /**
     * Добавление записей в таблицу
     * 
     * @param string $table название таблицы базы данных
     * @param  array $data ассоциативный массив полей таблицы и их значений ("field1" => "value1", ...)
     * @return mixed
     */
    protected function _insert($table, $data = array())
    {
        // формирование полей и значений для SQL запроса
        $values = $fields = array();
        foreach ($data as $field => $value) {
            $values[] = $this->query->escapeStr($value);
            $fields[] = $field;
        }
        // формирование SQL запроса
        $sql = 'INSERT INTO ' . $table . ' ( ' . implode(', ', $fields) . ') VALUES (' . implode(', ', $values) . ')';

        return $this->query->execute($sql);
    }

    /**
     * Возращает запись из таблицы по ее идент.
     * 
     * @param string $table название таблицы базы данных
     * @param  string $primary первичный ключ (или любое поле) таблицы
     * @param  mixed $id идентификатор записи(ей) (например: "1" или "1,2,3,4")
     * @return mixed
     */
    protected function _getRecord($table, $primary = '', $id = '')
    {
        // формирование SQL запроса
        $sql = 'SELECT * FROM ' . $table . ' WHERE ' . $primary . '=' . $this->query->escapeStr($id);

        return $this->query->getRecord($sql);
    }

    /**
     * Выполнение SQL запроса
     * 
     * @param  string $sql запрос
     * @return integer
     */
    public function query($sql)
    {
        $start = $this->getStart();
        $sort = $this->_sort . ' ' . $this->_dir;
        $filter = $this->_filterQuery;
        $isUseFilter = strlen($filter) > 1;
        $from = array('%filter', '%sort');
        $to = array($filter, $sort);
        if ($this->_sqlSmart) {
            $from[] = '%range';
            if ($isUseFilter)
                $to[] = '';
            else
                $to[] = 'BETWEEN ' . ($start + 1) . ' AND ' . ($start + $this->_limit);
            $from[] = '%limit';
            //if ($isUseFilter)
                //$to[] = 'LIMIT ' . $start . ',' . $this->_limit;
                $to[] = $start . ',' . $this->_limit;
            /*else
                $to[] = '';*/
        } else {
            $from[] = '%limit';
            $to[] = $start . ',' . $this->_limit;
        }

        if ($this->fastFilter) {
            $from[] = '%fastfilter';
            $to[] = $this->fastFilter;
        } else {
            $from[] = '%fastfilter';
            $to[] = '';
        }

        $sql = str_replace($from, $to, $sql);

        return $this->query->execute($sql);
    }
}
?>