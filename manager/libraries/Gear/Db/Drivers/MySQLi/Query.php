<?php
/**
 * Gear Manager
 * 
 * Обработка SQL запросов сервера базы данных "MySQL"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   MySQL
 * @package    Query
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Query.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * @see GDb_Query_Abstract
 */
require_once('Gear/Db/Drivers/Query.php');

/**
 * Класс обработки SQL запросов сервера базы данных "MySQL"
 * 
 * @category   MySQL
 * @package    Query
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Query.php 2016-01-01 12:00:00 Gear Magic $
 */
class GDb_Query extends GDb_Query_Abstract
{
    /**
     * Возращает запись SQL запроса по указаному индексу
     * 
     * @param  integer $recordIndex идекс записи
     * @param  integer $typeRecord тип записи
     * @return mixed
     */
    protected function recordByIndex($recordIndex, $typeRecord = self::SQL_ASSOC)
    {
        // если записей нет в последнем запросе
        if ($this->_countRecords == 0)
            return array();
        // если тип записи не указан, то берется из конструктора класса
        if (empty($typeRecord))
            $typeRecord = $this->_typeRecord;
        // поиск записи
        if (mysqli_data_seek($this->_handle, $recordIndex)) {
            if ($typeRecord == self::SQL_OBJECT)
                $data = mysqli_fetch_object($this->_handle);
            else
                $data = mysqli_fetch_array($this->_handle, $this->_typeRecord);
        }

        return $data;
    }

    /**
     * Выполнение SQL запроса
     * 
     * @param  string $sql SQL запрос
     * @return boolean
     */
    public function execute($sql)
    {
        $this->_executed = false;
        // збрасываем счётчики записей
        $this->_recordIndex = $this->_countRecords = $this->_countFields = $this->_handle = 0;
        // чтобы знать последний запрос
        $this->_sql = $sql;
        // если SQL запрос не указан
        if (empty($sql))
            return false;
        // ресурс последнего запроса
        $this->_handle = mysqli_query($this->_db->getHandle(), $sql);
        // если запрос был успешный, возращает кол-о записей и полей
        if (!$this->_handle) return false;
        if (is_object($this->_handle)) {
            $this->_countRecords = $this->_handle->num_rows;
            $this->_countFields = $this->_handle->field_count;
        }

        return $this->_executed = true;
    }

    /**
     * Возращает количество обработанных записей в последнем SQL запросе
     * 
     * @return integer
     */
    public function getAffectedRows()
    {
        return mysqli_affected_rows($this->_db->getHandle());
    }

    /**
     * Возращает ошибку при обработки SQL запроса
     * 
     * @param  boolean $details детальный вывод ошибки
     * @return mixed
     */
    public function getError($details = false)
    {
        if ($details)
            return array('error' => mysqli_error($this->_db->getHandle()), 'code' => mysqli_errno($this->_db->getHandle()));
        else
            return mysqli_error($this->_db->getHandle());
    }

    /**
     * Возращает общее количество записей в последнем SQL запросе
     * 
     * @return mixed
     */
    public function getFoundRows()
    {
        if ($this->execute('SELECT FOUND_ROWS()')) {
            $arr = mysqli_fetch_array($this->_handle, self::SQL_NUM);

            return $arr[0];
        }

        return false;
    }

    /**
     * Возращает последний идин-к добавленной зиписи
     * 
     *@return integer
     */
    public function getLastInsertId()
    {
        if (is_resource($this->_db->getHandle()))
            return mysqli_insert_id($this->_db->getHandle());

        return false;
    }

    /**
     * Возращает запись SQL запроса
     * 
     * @param  string $sql SQL запрос
     * @return mixed
     */
    public function getRecord($sql = '')
    {
        if (empty($sql))
            return $this->first();

        if ($this->execute($sql))
            return $this->first();

        return false;
    }

    /**
     * Возращает записи SQL запроса
     * 
     * @param  string $sql SQL запрос
     * @return mixed
     */
    public function getRecords($sql = '')
    {
        $data = array();
        if ($sql)
            if (!$this->execute($sql))
                return false;
        while (!$this->eof())
            $data[] = $this->next();

        return $data;
    }

    /**
     * Сбросить аутоинкремент таблицы
     * 
     * @param  string $table таблица
     * @return boolean
     */
    public function dropAutoIncrement($table)
    {
        return $this->execute('ALTER TABLE `' . $table . '` auto_increment=1');
    }

    /**
     * Выполнение SQL запроса
     * 
     * @param  string $sql запрос
     * @return integer
     */
    public function query($sql)
    {
        $start = $this->getStart();
        $sort = $this->_sort . ' ' . $this->_dir;
        $filter = $this->_filterQuery;
        $isUseFilter = strlen($filter) > 1;
        $from = array('%filter', '%sort');
        $to = array($filter, $sort);
        if ($this->_sqlSmart) {
            $from[] = '%range';
            if ($isUseFilter)
                $to[] = '';
            else
                $to[] = 'BETWEEN ' . ($start + 1) . ' AND ' . ($start + $this->_limit);
            $from[] = '%limit';
            //if ($isUseFilter)
                //$to[] = 'LIMIT ' . $start . ',' . $this->_limit;
                $to[] = $start . ',' . $this->_limit;
            /*else
                $to[] = '';*/
        } else {
            $from[] = '%limit';
            $to[] = $start . ',' . $this->_limit;
        }
        $sql = str_replace($from, $to, $sql);

        return $this->query->execute($sql);
    }

    /**
     * Обработка escape последовательности символов строки
     * 
     * @param  string $str строка
     * @param  boolean $addQuote добавлять кавычки "'" в начало и в конец строки
     * @return mixed
     */
    public function escapeStr($str, $addQuote = true)
    {
        if ($str === 0 || $str === false) return '0';
        if ($str === true) return '1';
        if ($str == null) return 'NULL';
        if (is_int($str) || empty($str)) return $str;
        if(get_magic_quotes_gpc())
            $str = stripslashes($str);
        if ($addQuote)
            return '\'' . mysqli_real_escape_string($this->_db->getHandle(), $str) . '\'';
        else
            return mysqli_real_escape_string($this->_db->getHandle(), $str);
    }
}
?>