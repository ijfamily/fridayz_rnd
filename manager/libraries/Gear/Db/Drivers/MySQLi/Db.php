<?php
/**
 * Gear Manager
 * 
 * ������� ����������� � ������� ���� ������ "MySQL"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   MySQL
 * @package    Database
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Db.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * @see GDb_Abstract
 */
require_once('Gear/Db/Drivers/Db.php');

/**
 * ����� �������� ����������� � ������� ���� ������ "MySQL"
 * 
 * @category   MySQL
 * @package    Database
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Db.php 2016-01-01 12:00:00 Gear Magic $
 */
class GDb extends GDb_Abstract
{
    /**
     * ���� ����������� � �������
     * 
     * @var integer
     */
    protected $_port = 3306;

    /**
     * �������� ���������� � ��������
     * 
     * @param  integer $type ��� ���������� � ��������
     * @return void
     */
    public function connect($type = 'persistent')
    {
        // ����������� �� ���������� � ��������
        if ($this->isConnected()) return;

        try {
            if ($type == 'persistent')
                $this->_handle = mysqli_connect($this->_server, $this->_username, $this->_password, $this->_schema);
            // �������� ����������
            if (!$this->_handle)
                throw new GException('Data processing error', 'Error when making a connection to the server', $this->getError());
            // ����� ����� ���� ������
            if ($this->select() === false)
                throw new GException('Data processing error',  'Error when making a connection to the server', $this->getError());
            // ��������� ���������
            if ($this->setCharset() === false)
                throw new GException('Data processing error',  'Error when making a connection to the server', $this->getError());
        } catch(GException $e) {
            $e->renderException(array(), true);
        }
        // �������� ���������� � ��������
        $this->_connected = true;
        // ��� ���������� � ��������
        $this->_type = $type;
    }

    /**
     * �������� ���������� � ��������
     * 
     * @return void
     */
    public function disconnect()
    {
        // ���� ���������� ���������� ������
        if (is_resource($this->_handle))
            return mysqli_close($this->_handle);

        return false;
    }

    /**
     * ���������� ������ ��� ��������� ��������� SQL �������
     * 
     * @param  boolean $details ������ ��������� �� ������
     * @return mixed
     */
    public function getError($details = false)
    {
        if ($details)
            return array('error' => mysqli_connect_error(), 'code' => mysqli_connect_errno());
        else
            return mysqli_connect_error();
    }

    /**
     * ����� ����� ���� ������
     * 
     * @param  string $schema ����� ���� ������
     * @return boolean
     */
    public function select($schema = '')
    {
        return true;
    }

    /**
     * ��������� ���������
     * 
     * @param  string $charset ��� ���������
     * @return boolean
     */
    public function setCharset($charset = '')
    {
        return true;
    }

    /**
     * ���������� � ������� ���� ������
     * 
     * @return integer
     */
    public function getServerInfo()
    {
        return mysqli_get_server_info($this->_handle);
    }

    /**
     * ���������� � ��������� ���� ������
     * 
     * @return integer
     */
    public function getProtoInfo()
    {
        return mysqli_get_proto_info($this->_handle);
    }

    /**
     * ���������� � �����
     * 
     * @return srting
     */
    public function getHostInfo()
    {
        return mysqli_get_host_info($this->_handle);
    }

    /**
     * ���������� � �������
     * 
     * @return string
     */
    public function getClientInfo()
    {
        return array();
    }
}
?>