<?php
/**
 * Gear Manager
 * 
 * Обработка SQL запросов к таблицам базы данных
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Database
 * @package    Table
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Table.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Абстрактный класс обработки SQL запросов к таблицам базы данных
 * 
 * @category   Database
 * @package    Table
 * @subpackage Abstract
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Table.php 2016-01-01 12:00:00 Gear Magic $
 */
abstract class GDb_Table_Abstract
{
    /**
     * Название таблицы
     * @var string
     */
    protected $_table = '';

    /**
     * Первичный ключ таблицы
     * @var string
     */
    protected $_primary = '';

    /**
     * Поля таблицы
     * @var array
     */
    protected $_fields = array();

    /**
     * Сылка на экземпляр класса обработки SQL запросов
     *
     * @var mixed
     */
    public $query = null;

    /**
     * Сылка на экземпляр класса обработки SQL запросов
     *
     * @var mixed
     */
    protected $_filter = array();

    /**
     * Запрос для фильтрации записей таблицы
     *
     * @var mixed
     */
    protected $_filterQuery = '';

    /**
     * Простой фильтр для поиска записей (array("field" => "value"))
     *
     * @var mixed
     */
    public $fastFilter = array();

    /**
     * Страница
     * @var integer
     */
    protected $_page = 1;

    /**
     * Количе-о записей на странице
     * @var integer
     */
    protected $_limit = 3;

    /**
     * Поле сортировки
     * @var string
     */
    protected $_sort = '';

    /**
     * Вид сортировки
     * @var string
     */
    protected $_dir = 'ASC';

    /**
     * Construction "ORDER BY" for large query
     * array('alias' => array('%orderby1', ...), 'fields' => array('field1' => '%orderby1' ...))
     * @var array
     */
    protected $_sqlSmart = false;

    /**
     * Диапазон лимита (если $_start = null,
     * то в расчетах будет использоваться $_page)
     * @var integer
     */
    protected $_start = null;

    /**
     * Конструктор
     * 
     * @param  string $table название тыблицы
     * @param  string $primary название первичного поля
     * @param  array $fields поля таблицы
     * @return void
     */
    public function __construct($table, $primary, $fields = array())
    {
        // название таблицы
        $this->_table = $table;
        // первичный ключ таблицы
        $this->_primary = $primary;
        // поля таблицы
        $this->_fields = $fields;
        // экземпляр класса обработки SQL запросов
        $this->query = new GDb_Query();
    }

    /**
     * Обновление записей таблицы
     * 
     * @param  string $table название таблицы базы данных
     * @param  array $where ассоциативный массив полей таблицы и их значений ("field1" => "value1", ...)
     * @param  string $primary первичный ключ (или любое поле) таблицы
     * @param  mixed $id идентификатор записи(ей) (например: "1" или "1,2,3,4")
     * @return mixed
     */
    protected function _isDataExist($table, $where = array(), $primary = '', $id = '')
    {}

    /**
     * Обновление записей таблицы
     * 
     * @param  array $data ассоциативный массив полей таблицы и их значений ("field1" => "value1", ...)
     * @param  mixed $id идентификатор записи(ей) (например: "1" или "1,2,3,4")
     * @return mixed
     */
    public function isDataExist($where = array(), $id = '')
    {
        return $this->_isDataExist($this->_table, $where, $this->_primary, $id);
    }

    /**
     * Обновление записей таблицы
     * 
     * @param  string $table название таблицы базы данных
     * @param  array $data ассоциативный массив полей таблицы и их значений ("field1" => "value1", ...)
     * @param  string $primary первичный ключ (или любое поле) таблицы
     * @param  mixed $id идентификатор записи(ей) (например: "1" или "1,2,3,4")
     * @param  array $orderby ассоциативный массив полей в сортеровке таблицы ("field1", "field2", ...)
     * @return mixed
     */
    protected function _update($table, $data = array(), $primary = '', $id = '', $orderby = array())
    {}

    /**
     * Обновление записей таблицы
     * 
     * @param  array $data ассоциативный массив полей таблицы и их значений ("field1" => "value1", ...)
     * @param  mixed $id идентификатор записи(ей) (например: "1" или "1,2,3,4")
     * @param  array $orderby ассоциативный массив полей в сортеровке таблицы ("field1", "field2", ...)
     * @return mixed
     */
    public function update($data = array(), $id = '', $orderby = array())
    {
        $this->_update($this->_table, $data, $this->_primary, $id, $orderby);
    }

    /**
     * Обновление записей таблицы по условию
     * 
     * @param string $table название таблицы базы данных
     * @param  array $data ассоциативный массив полей таблицы и их значений ("field1" => "value1", ...)
     * @param  mixed $where ассоциативный массив ("field1" => "value1", ...) или строка ("field1" = "value1" AND ...)
     * @param  array $orderby ассоциативный массив полей в сортеровке таблицы ("field1", "field2", ...)
     * @return mixed
     */
    protected function _updateBy($table, $data = array(), $where = array(), $orderby = array())
    {}

    /**
     * Обновление записей таблицы по условию
     * 
     * @param  array $data ассоциативный массив полей таблицы и их значений ("field1" => "value1", ...)
     * @param  mixed $where ассоциативный массив ("field1" => "value1", ...) или строка ("field1" = "value1" AND ...)
     * @param  array $orderby ассоциативный массив полей в сортеровке таблицы ("field1", "field2", ...)
     * @return mixed
     */
    public function updateBy($data = array(), $where = array(), $orderby = array())
    {
        $this->_updateBy($this->_table, $data, $where, $orderby);
    }

    /**
     * Удаление записей из таблицы
     * 
     * @param string $table название таблицы базы данных
     * @param  string $primary первичный ключ (или любое поле) таблицы
     * @param  mixed $id идентификатор записи(ей) (например: "1" или "1,2,3,4")
     * @param  mixed $limit количество записей, если нет необходимости - false
     * @return mixed
     */
    protected function _delete($table, $primary = '', $id = '', $limit = false)
    {}

    /**
     * Удаление записей из таблицы
     * 
     * @param  mixed $id идентификатор записи(ей) (например: "1" или "1,2,3,4")
     * @param  mixed $limit количество записей, если нет необходимости - false
     * @return mixed
     */
    public function delete($id = '', $limit = false)
    {
        return $this->_delete($this->_table, $this->_primary, $id, $limit);
    }

    /**
     * Удаление записей из таблицы по условию
     * 
     * @param string $table название таблицы базы данных
     * @param  mixed $where ассоциативный массив ("field1" => "value1", ...) или строка ("field1" = "value1" AND ...)
     * @param  mixed $limit количество записей, если нет необходимости - false
     * @return boolean
     */
    protected function _deleteBy($table, $where = array(), $limit = false)
    {}

    /**
     * Удаление записей из таблицы по условию
     * 
     * @param  mixed $where ассоциативный массив ("field1" => "value1", ...) или строка ("field1" = "value1" AND ...)
     * @param  mixed $limit количество записей, если нет необходимости - false
     * @return boolean
     */
    public function deleteBy($where = array(), $limit = false)
    {
        return $this->_deleteBy($this->_table, $where, $limit);
    }

    /**
     * Удаление записей из таблицы по условию
     * 
     * @return boolean
     */
    public function deleteSys($id = '')
    {
        // формирование условий в SQL запросе
        $conditions = '';
        if ($id) {
            // если число
            if (is_integer($id))
                $conditions = " \nWHERE `" . $this->_primary . "`=$id ";
            // если строка
            else
                $conditions = " \nWHERE " . $this->_primary . " IN ($id) ";
        }
        $conditions .= ' AND `sys_record`<>1';
        // формирование SQL запроса
        $sql = 'DELETE FROM ' . $this->_table . $conditions;

        return $this->query->execute($sql);
    }

    /**
     * Добавление записей в таблицу
     * 
     * @param string $table название таблицы базы данных
     * @param  array $data ассоциативный массив полей таблицы и их значений ("field1" => "value1", ...)
     * @return mixed
     */
    protected function _insert($table, $data = array())
    {}

    /**
     * Добавление записей в таблицу
     * 
     * @param  array $data ассоциативный массив полей таблицы и их значений ("field1" => "value1", ...)
     * @return mixed
     */
    public function insert($data = array())
    {
        return $this->_insert($this->_table, $data);
    }

    /**
     * Возращает последний идин-к добавленной зиписи
     * 
     *@return integer
     */
    public function getLastInsertId()
    {
        return $this->query->getLastInsertId();
    }

    /**
     * Удалить все записи из таблицы
     * @param boolean $drop сбросить autoinc
     * 
     * @return boolean
     */
    public function clear($drop = false)
    {
        if ($this->query->clear($this->_table) !== true)
            return false;
        if ($drop)
             return $this->query->dropAutoIncrement($this->_table);
    }

    /**
     * Сбросить аутоинкремент таблицы
     * 
     * @return boolean
     */
    public function dropAutoIncrement()
    {
        return $this->query->dropAutoIncrement($this->_table);
    }

    /**
     * Возращает запись из таблицы по ее идент.
     * 
     * @param string $table название таблицы базы данных
     * @param  string $primary первичный ключ (или любое поле) таблицы
     * @param  mixed $id идентификатор записи(ей) (например: "1" или "1,2,3,4")
     * @return mixed
     */
    protected function _getRecord($table, $primary = '', $id = '')
    {}

    /**
     * Возращает запись из таблицы по ее идент.
     * 
     * @param  mixed $id идентификатор записи(ей) (например: "1" или "1,2,3,4")
     * @return mixed
     */
    public function getRecord($id = '')
    {
        return $this->_getRecord($this->_table, $this->_primary, $id);
    }

    /**
     * Фильтр
     * 
     * @param  object $filter фильтр
     * @return void
     */
    public function setFilter($filter)
    {
        $this->_filter = $filter;
        $this->_filterQuery = $this->_filter->getQuery();
    }

    /**
     * Установка количество записей на странице
     * 
     * @param  integer $limit количество записей
     * @return void
     */
    public function setLimit($limit)
    {
        $this->_limit = $limit;
    }

    /**
     * Установка страницы
     * 
     * @param  integer $page стрница
     * @return void
     */
    public function setPage($page)
    {
        $this->_page = $page;
    }

    /**
     * Установка сортировки
     * 
     * @param  string $sort сортирумое поле
     * @param  string $dir вид сортировки
     * @return void
     */
    public function setSort($sort, $dir = 'ASC')
    {
        $this->_sort = $sort;
        $this->_dir = $dir;
    }

    /**
     * Установка сортировки
     * 
     * @param  boolean $value сортирумое поле
     * @return void
     */
    public function setSmart($value)
    {
        $this->_sqlSmart = $value;
    }

    /**
     * Установка диапазона лимита записей
     * 
     * @param  integer $start начало диапазона
     * @return integer
     */
    public function setStart($start)
    {
        $this->_start = $start;
    }

    /**
     * Расчёт диапазона лимита записей
     * 
     * @return integer
     */
    public function getStart()
    {
        //используется _start или _page для расчета лимитов
        if ($this->_start)
            return $this->_start;
        else
            return $this->_page * $this->_limit - $this->_limit;
    }

    /**
     * Выполнение SQL запроса
     * 
     * @param  string $sql запрос
     * @return integer
     */
    public function query($sql)
    {}
}
?>