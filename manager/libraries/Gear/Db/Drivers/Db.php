<?php
/**
 * Gear Manager
 * 
 * ���� ������
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Database
 * @package    Database
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Db.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * ����� ������������� ��������� ������ � ���� ������
 * 
 * @category   Database
 * @package    Database
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Db.php 2016-01-01 12:00:00 Gear Magic $
 */
abstract class GDb_Result
{
    /**
     * ��������� SQL ������
     * 
     * @var string
     */
    static public $sql = '';

    /**
     * ������ ���������� SQL �������
     * 
     * @var string
     */
    static public $error = '';

    /**
     * ��� ������ ���������� SQL �������
     * 
     * @var integer
     */
    static public $errorCode = null;

    /**
     * ���������� ������� � ��������� �������
     * 
     * @var integer
     */
    static public $countRecords = 0;

    /**
     * ���������� ����� � ��������� �������
     * 
     * @var integer
     */
    static public $countFields = 0;
}


/**
 * ����������� ����� ����������� � ������� ���� ������
 * 
 * @category   Database
 * @package    Database
 * @subpackage Abstract
 * @copyright  Copyright (c) 2013-2014 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Db.php 2013-12-30 12:00 Gear Magic $
 */
abstract class GDb_Abstract
{
    /**
     * C��������� � ��������
     * 
     * @var bool
     */
    protected $_connected = false;

    /**
     * ��������� �� ���������� � ��������
     * 
     * @var integer
     */
    protected $_handle = null;

    /**
     * ������ ����������� � �������
     * 
     * @var string
     */
    public $password = '';

    /**
     * ���� ����������� � �������
     * 
     * @var integer
     */
    public $port = 0;

    /**
     * ����� ���� ������
     * 
     * @var string
     */
    public $schema = '';

    /**
     * IP ��� DNS ��� �������
     * 
     * @var string
     */
    public $server = '';

    /**
     * ��� ������������
     * 
     * @var string
     */
    public $username = '';

    /**
     * ���������
     * 
     * @var string
     */
    public $charset = '';

    /**
     * ������ ����������
     * 
     * @var array
     */
    public $connections = array();

    /**
     * C���������
     * 
     * @var array
     */
    public $connection = array();

    /**
     * ��� ����������
     * 
     * @var string
     */
    public $conName = 'default';

    /**
     * �����������
     * 
     * @param  array $connections ������ �������� ����������� � ������� ���� ������
     * @param  string $conName �������� ����������
     * @return void
     */
    public function __construct($connections, $conName)
    {
        $this->connections = $connections;
        $this->initBy($conName);
    }

    /**
     * ������������ ��������� �����������
     * 
     * @param  array $connection ��������� ����������� � ������� ���� ������
     * @return void
     */
    public function init($connection)
    {
        // IP ��� DNS ��� �������
        $this->server = isset($connection['SERVER']) ? $connection['SERVER'] : $this->server;
        // ���� ����������� � �������
        $this->port = isset($connection['PORT']) ? $connection['PORT'] : $this->port;
        // ����� ���� ������
        $this->schema = isset($connection['SCHEMA']) ? $connection['SCHEMA'] : $this->schema;
        // ��� ������������
        $this->username = isset($connection['USERNAME']) ? $connection['USERNAME'] : $this->username;
        // ������ ����������� � �������
        $this->password = isset($connection['PASSWORD']) ? $connection['PASSWORD'] : $this->password;
        // ��������� ���� ������
        $this->charset = isset($connection['CHARSET']) ? $connection['CHARSET'] : $this->charset;
    }

    /**
     * ������������ ��������� ����������� �� �������� ����������
     * 
     * @param  string $conName �������� ����������
     * @return void
     */
    public function initBy($conName)
    {
        if (isset($this->connections[$conName])) {
            $this->conName = $conName;
            $this->connection = $this->connections[$conName];
            $this->init($this->connection);
            return true;
        }

        return false;
    }

    /**
     * ����������
     * 
     * @return void
     */
    public function __destruct()
    {
        // �������� ���������� � ��������
        $this->disconnect();
    }

    /**
     * �������� ���������� � ��������
     * 
     * @return void
     */
    public function connect($conName = 'default')
    {}

    /**
     * �������� ���������� � ��������
     * 
     * @return void
     */
    public function disconnect()
    {}

    /**
     * ���������� ������ ��� ��������� ��������� SQL �������
     * 
     * @param  boolean $details ������ ��������� �� ������
     * @return mixed
     */
    public function getError($details = false)
    {}

    /**
     * ��������� ��������� �� ���������� � �������� ���� ������
     * 
     * @return resource
     */
    public function getHandle()
    {
        return $this->_handle;
    }

    /**
     * ��������� ��� ���������� � ��������
     * 
     * @return integer
     */
    public function getType()
    {
        return $this->_type;
    }

    /**
     * �������� ���������� � ��������
     *  
     * @return boolean
     */
    public function isConnected()
    {
        return $this->_connected;
    }

    /**
     * ����� ����� ���� ������
     * 
     * @param  string $schema ����� ���� ������
     * @return void
     */
    public function select($schema = '')
    {}

    /**
     * ��������� ���������
     * 
     * @param  string $charset ��� ���������
     * @return boolean
     */
    public function setCharset($charset = '')
    {}

    /**
     * ���������� � ������� ���� ������
     * 
     * @return integer
     */
    public function getServerInfo()
    {}

    /**
     * ���������� � ��������� ���� ������
     * 
     * @return integer
     */
    public function getProtoInfo()
    {}

    /**
     * ���������� � �����
     * 
     * @return srting
     */
    public function getHostInfo()
    {}

    /**
     * ���������� � �������
     * 
     * @return string
     */
    public function getClientInfo()
    {}

    /**
     * ���������� �������� �������� ����������� � �������
     * 
     * @return string
     */
    public function getDriverName()
    {
        return $this->_driver;
    }

    /**
     * ���������� ��������� ������� ����������� � �������
     * 
     * @return mixed
     */
    public function getDriver()
    {
        return self::$db;
    }
}
?>