<?php
/**
 * Gear Manager
 * 
 * Пакет хэш функций
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Libraries
 * @package    Hash
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Hash.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * Класс работы с хэшем
 * 
 * @category   Libraries
 * @package    Hash
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Hash.php 2016-01-01 15:00:00 Gear Magic $
 */
class GHash
{
    /**
     * Создание хэш значения
     * 
     * @param  string $hash дополнительный ключ
     * @param  string $method метод получения хеша
     * @return string
     */
    public static function create($hash = '', $method = 'agent')
    {
        switch ($method) {
            case 'agent': return md5($_SERVER['HTTP_USER_AGENT'] . $hash);

            case 'ip': return md5($_SERVER['SERVER_ADDR'] . $_SERVER['REMOTE_ADDR'] . $hash);
        }
    }

    /**
     * Возращает значение хэш
     * 
     * @param  string $key дополнительный ключ (если нет генерация ключа)
     * @param  string $method метод получения хеша
     * @return string
     */
    public static function get($hash = '', $method = 'agent')
    {
        if (empty($hash))
            $hash = uniqid('');

        return array('token' => self::create($hash, $method), 'hash' => $hash);
    }

    /**
     * Проверка хэш по ключу и значению хэш
     * 
     * @param  string $hash значение хэш
     * @param  string $token ключ
     * @param  string $method метод получения хеша
     * @return string
     */
    public static function check($token, $hash, $method = 'agent')
    {
        return self::create($hash, $method) == $token;
    }
}
?>