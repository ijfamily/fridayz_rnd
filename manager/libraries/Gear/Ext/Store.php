<?php
/**
 * Gear Manager
 *
 * Источник данных
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Ext
 * @package    Data
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Store.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * @see Ext_Component
 */
require_once('Component.php');

/**
 * Класс компонента "Источник данных"
 * Класс ExtJS Ext.data.Store
 * Класс Store инкапсулирует кэш объектов Record на стороне клиента, которые 
 * обеспечивают входные данные для компонентов, таких как GridPanel, ComboBox, или DataView
 *
 * @category   Ext
 * @package    Data
 * @subpackage Store
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Store.php 2016-01-01 15:00:00 Gear Magic $
 */
class Ext_Data_Store extends Ext_Component
{
    /**
     * Cвойства компонента
     *
     * @var array
     */
    protected $_properties = array(
        'xtype'           => 'jsonstore',
        'url'             => 'store.php',
        'root'            => 'data',
        'totalProperty'   => 'totalCount',
        'messageProperty' => 'message',
        'remoteSort'      => true,
        'autoLoad'        => true,
        'batch'           => false,
        'autoSave'        => true,
        'fields'          => array()
    );
}
?>