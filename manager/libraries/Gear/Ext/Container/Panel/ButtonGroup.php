<?php
/**
 * Gear Manager
 *
 * Группы кнопок
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Ext
 * @package    ButtonGroup
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: TreeGrid.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * @see Ext_Panel
 */
require_once('Gear/Ext/Container/Panel.php');

/**
 * Класс компонента "Группа кнопок"
 * 
 * @category   Ext
 * @package    ButtonGroup
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: TreeGrid.php 2016-01-01 15:00:00 Gear Magic $
 */
class Ext_ButtonGroup extends Ext_Panel
{
    /**
     * Свойства компонента
     *
     * @var array
     */
    protected $_properties = array(
        'xtype' => 'buttongroup'
    );
}


/**
 * Класс компонента "Группа кнопок - редактирование списка"
 * 
 * @category   Ext
 * @package    ButtonGroup
 * @subpackage Edit
 * @copyright  Copyright (c) 2011-2014 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: TreeGrid.php 2014-08-01 12:00:00 Gear Magic $
 */
class Ext_ButtonGroup_Edit extends Ext_ButtonGroup
{
    /**
     * Конструктор
     * 
     * @param  array $property массив свойств
     * @return void
     */
    public function __construct($properties = array())
    {
        parent::__construct($properties);

        $this->items->addItems(array(
            array('xtype' => 'mn-btn-insert-data', 'gridId' => $this->_properties['gridId'], 'rowspan' => 3),
            array('xtype' => 'mn-btn-delete-data', 'gridId' => $this->_properties['gridId'], 'rowspan' => 3),
            array('xtype' => 'mn-btn-edit-data', 'gridId'  => $this->_properties['gridId'], 'rowspan' => 3),
            array('xtype' => 'mn-btn-select-data', 'gridId' => $this->_properties['gridId'], 'rowspan' => 3),
            array('xtype' => 'mn-btn-refresh-data', 'gridId' => $this->_properties['gridId'], 'rowspan' => 3)
        ));
    }
}


/**
 * Класс компонента "Группа кнопок - редактирование списка"
 * 
 * @category   Ext
 * @package    ButtonGroup
 * @subpackage NEdit
 * @copyright  Copyright (c) 2011-2014 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: TreeGrid.php 2014-08-01 12:00:00 Gear Magic $
 */
class Ext_ButtonGroup_NEdit extends Ext_ButtonGroup
{
    /**
     * Конструктор
     * 
     * @param  array $property массив свойств
     * @return void
     */
    public function __construct($properties = array())
    {
        parent::__construct($properties);

        $this->items->add(
            array('xtype'      => 'mn-btn-refresh-node',
                  'treeGridId' => $this->_properties['treeGridId'],
                  'rowspan'    => 3)
        );
    }
}


/**
 * Класс компонента "Группа кнопок - управление столбцами списка"
 * 
 * @category   Ext
 * @package    ButtonGroup
 * @subpackage Columns
 * @copyright  Copyright (c) 2011-2014 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: TreeGrid.php 2014-08-01 12:00:00 Gear Magic $
 */
class Ext_ButtonGroup_Columns extends Ext_ButtonGroup
{
    /**
     * Конструктор
     * 
     * @param  array $property массив свойств
     * @return void
     */
    public function __construct($properties = array())
    {
        parent::__construct($properties);

        $this->items->addItems(array(
            array('xtype' => 'mn-btn-help-data', 'fileName' => 'none.html', 'rowspan' => 3),
            array('xtype'  => 'mn-btn-cols-state', 'cls' => 'mn-btn-slim', 'gridId' => $this->_properties['gridId']),
            array('xtype'  => 'mn-btn-cols-data', 'cls' => 'mn-btn-slim', 'gridId' => $this->_properties['gridId']),
            array('xtype'  => 'mn-btn-sort-data', 'cls' => 'mn-btn-slim', 'gridId' => $this->_properties['gridId'])
        ));
    }
}


/**
 * Класс компонента "Группа кнопок - управление столбцами списка"
 * 
 * @category   Ext
 * @package    ButtonGroup
 * @subpackage Filter
 * @copyright  Copyright (c) 2011-2014 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: TreeGrid.php 2014-08-01 12:00:00 Gear Magic $
 */
class Ext_ButtonGroup_Filter extends Ext_ButtonGroup
{
    /**
     * Конструктор
     * 
     * @param  array $property массив свойств
     * @return void
     */
    public function __construct($properties = array())
    {
        parent::__construct($properties);

        $this->items->add(array('xtype' => 'mn-btn-filter-data', 'fileName' => 'none.html'));
    }
}
?>