<?php
/**
 * Gear Manager
 *
 * Список дерева
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Ext
 * @package    Panel
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: TreeGrid.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * @see Ext_Panel
 */
require_once('Gear/Ext/Container/Panel.php');

/**
 * Класс компонента "GridPanel"
 * Этот класс представляет основной интерфейс компонента на основе управления 
 * списком для представления данных в табличном формате строк и столбцов
 * 
 * @category   Ext
 * @package    Panel
 * @subpackage TreeGrid
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: TreeGrid.php 2016-01-01 15:00:00 Gear Magic $
 */
class Ext_Tree_TreeGrid extends Ext_Panel
{
    /**
     * Свойства компонента
     *
     * @var array
     */
    protected $_properties = array(
        'xtype'    => 'treegrid',
        'closable'  => true,
        'columns'   => array(),
        'loader'    => array('xtype' => 'treegridloader', 'requestMethod' => 'POST'),
        'component' => array('container' => 'content-tab', 'destroy' => true)
    );

    /**
     * Возращает свойства компонента
     * 
     * @return mixed
     */
    public function getProperties()
    {
        if ($this->loader != null)
            $this->loader = $this->loader->getProperties();

        return parent::getProperties();
    }
}
?>