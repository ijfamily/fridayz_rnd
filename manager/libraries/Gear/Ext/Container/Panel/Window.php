<?php
/**
 * Gear Manager
 *
 * Компонент "Окно"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Ext
 * @package    Panel
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Panel.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * @see Ext_Panel
 */
require_once ('Gear/Ext/Container/Panel.php');

/**
 * Класс компонента "Окно"
 * Специализированный панель предназначена для использования в качестве окна приложения
 * 
 * @category   Ext
 * @package    Panel
 * @subpackage Window
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Panel.php 2016-01-01 15:00:00 Gear Magic $
 */
class Ext_Window extends Ext_Panel
{
    /**
     * Cвойства компонента
     *
     * @var array
     */
    protected $_properties = array(
        'xtype'       => 'window',
        'iconCls'     => 'icon-tabs',
        'closable'    => true,
        'resizable'   => false,
        'modal'       => true,
        'layout'      => 'fit',
        'plain'       => false,
        'width'       => 100,
        'bodyStyle'   => 'padding:5px;'
    );
}
?>