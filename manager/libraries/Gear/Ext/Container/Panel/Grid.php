<?php
/**
 * Gear Manager
 *
 * Список данных
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Ext
 * @package    Panel
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: TreeGrid.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * @see Ext_Panel
 */
require_once('Gear/Ext/Container/Panel.php');

/**
 * @see Ext_Panel
 */
require_once('Gear/Ext/Store.php');

/**
 * Класс компонента "Список данных"
 * Этот класс представляет основной интерфейс компонента на основе управления 
 * списком для представления данных в табличном формате строк и столбцов
 * 
 * @category   Ext
 * @package    Panel
 * @subpackage GridPanel
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: TreeGrid.php 2016-01-01 15:00:00 Gear Magic $
 */
class Ext_Grid_GridPanel extends Ext_Panel
{
    /**
     * Свойства компонента
     *
     * @var array
     */
    protected $_properties = array(
        'collapsible' => false,
        'xtype'      => 'mn-grid',
        'baseCls'    => 'base-body',
        'stateful'   => false,
        'closable'   => true,
        'border'     => false,
        'columns'    => array(),
        'rowMenu'    => null,
        'cellTips'   => array(),
        'isReadOnly' => false,
        'component'  => array('container' => 'content-tab',
        'destroy'    => true)
    );

    /**
     * Конструктор
     * 
     * @param  array $property массив свойств
     * @return void
     */
    public function __construct($properties = array())
    {
        parent::__construct($properties);

        if (!isset($this->_properties['store']))
            $this->_properties['store'] = new Ext_Data_Store();
        else
            if (!is_resource($this->_properties['store']))
                $this->_properties['store'] = new Ext_Data_Store($this->_properties['store']);
    }

    /**
     * Возращает свойства компонента
     * 
     * @return mixed
     */
    public function getProperties()
    {
        if ($this->tabTip != null) {
            if (empty($this->tabTip))
                $this->tabTip = $this->title;
        } else
            $this->tabTip = $this->title;

        return parent::getProperties();
    }
}
?>