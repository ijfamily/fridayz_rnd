<?php
/**
 * Gear Manager
 *
 * Формы обработки данных
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Ext
 * @package    Panel
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: TreeGrid.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * @see Ext_Panel
 */
require_once('Gear/Ext/Container/Panel.php');

/**
 * Класс компонента "Форма"
 * 
 * @category   Ext
 * @package    Panel
 * @subpackage FormPanel
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: TreeGrid.php 2016-01-01 15:00:00 Gear Magic $
 */
class Ext_Form_FormPanel extends Ext_Panel
{
    /**
     * Свойства компонента
     *
     * @var array
     */
    protected $_properties = array(
        'xtype'      => 'form',
        'baseCls'    => 'x-plain',
        'labelWidth' => 70,
        'url'        => '',
        'defaults'   => array('xtype' => 'textfield')
    );
}


/**
 * Класс компонента "Профиль данных"
 * 
 * @category   Ext
 * @package    Panel
 * @subpackage DataProfile
 * @copyright  Copyright (c) 2011-2014 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: TreeGrid.php 2014-08-01 12:00:00 Gear Magic $
 */
class Ext_Form_DataProfile extends Ext_Panel
{
    /**
     * Свойства компонента
     *
     * @var array
     */
    protected $_properties = array(
        'xtype'      => 'mn-form-profile',
        'baseCls'    => 'x-plain',
        'labelWidth' => 70,
        'url'        => '',
        'defaults'   => array('xtype' => 'textfield')
    );
}


/**
 * Класс компонента "Форма фильтрации списка"
 * 
 * @category   Ext
 * @package    Panel
 * @subpackage GridFilter
 * @copyright  Copyright (c) 2011-2014 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: TreeGrid.php 2014-08-01 12:00:00 Gear Magic $
 */
class Ext_Form_GridFilter extends Ext_Panel
{
    /**
     * Свойства компонента
     *
     * @var array
     */
    protected $_properties = array('xtype' => 'uxgridfilter');
}
?>