<?php
/**
 * Gear Manager
 *
 * Панель
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Ext
 * @package    Container
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Panel.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * @see Ext_Container
 */
require_once('Gear/Ext/Container.php');

/**
 * Класс компонента "Панель"
 * Панель представляет собой контейнер, который имеет определенные функциональные и структурные компоненты,
 * которые делают его идеальным строительным блоком для пользовательских прикладных интерфейсов
 *
 * @category   Ext
 * @package    Container
 * @subpackage Panel
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Panel.php 2016-01-01 15:00:00 Gear Magic $
 */
class Ext_Panel extends Ext_Container
{
    /**
     * Cвойства компонента
     *
     * @var array
     */
    protected $_properties = array(
        'xtype'        => 'panel',
        'layout'       => 'fit',
        'plain'        => false,
        'buttonAlign'  => 'right'
    );

    /**
     * Truncates the string and add ellipsis ('...') to the end, if it
     * exceeds the specified length
     * 
     * @param  integer $value value
     * @param  integer $length required length
     * @return string
     */
    protected function ellipsis($value, $length)
    {
        $end = '...';
        if (mb_strlen($value, 'UTF-8') > $length) {
            $s = mb_substr($value, 0, $length - strlen($end), 'UTF-8');
            return $s . $end;
        } else
            return $value;
    }

    /**
     * Возращает все свойства компонента
     * 
     * @return mixed
     */
    public function getProperties()
    {
        if ($this->titleEllipsis != null)
            $this->title = $this->ellipsis($this->title, $this->titleEllipsis);

        return parent::getProperties();
    }
}
?>