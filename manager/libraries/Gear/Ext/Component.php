<?php
/**
 * Gear Manager
 * 
 * Пакет формирования интерфейсов для ExtJS компонентов
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Ext
 * @package    Component
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Component.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * @see Ext_MixedCollection
 */
require_once('Gear/Ext/Utils/MixedCollection.php');

/**
 * Базовый класс для Ext компонентов
 * 
 * @category   Ext
 * @package    Component
 * @subpackage Component
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Component.php 2016-01-01 15:00:00 Gear Magic $
 */
class Ext_Component {
    /**
     * Cвойства компонента
     *
     * @var array
     */
    protected $_properties = array();

    /**
     * Конструктор
     * 
     * @param  array $property массив свойств компонента
     * @return void
     */
    public function __construct($properties = array())
    {
        if ($properties)
            $this->_properties = array_merge($this->_properties, $properties);
    }

    /**
     * Установка свойства компонента через атрибут класса
     * 
     * @param  string $name свойство
     * @param  mixed $value значение
     * @return void
     */
    public function __set($name, $value) {
        $this->_properties[$name] = $value;
    }

    /**
     * Установка свойства компонента
     * 
     * @param  string $name свойство
     * @param  mixed $value значение
     * @return void
     */
    public function set($name, $value)
    {
         $this->_properties[$name] = $value;
    }

    /**
     * Установка свойств компонента
     * 
     * @param  array $items массив свойств компонента
     * @return void
     */
    public function setProps($items)
    {
        foreach ($items as $name => $value)
            $this->_properties[$name] = $value;
    }

    /**
     * Возращает свойство компонента через атрибут класса
     * 
     * @param  string $name свойств компонента
     * @return mixed
     */
    public function __get($name) {
        if (array_key_exists($name, $this->_properties)) {
            return $this->_properties[$name];
        }

        return null;
    }

    /**
     * Возращает свойство компонента
     * 
     * @param  string $name свойств компонента
     * @return mixed
     */
    public function get($name)
    {
        if (array_key_exists($name, $this->_properties)) {
            return $this->_properties[$name];
        }

        return null;
    }

    /**
     * Проверка существования свойства компонента
     * 
     * @param  string $name свойств компонента
     * @return boolean
     */
    public function __isset($name) {
        return isset($this->_properties[$name]);
    }

    /**
     * Удаление свойства компонента через атрибут класса
     * 
     * @param  string $name свойств компонента
     * @return boolean
     */
    public function __unset($name) {
        unset($this->_properties[$name]);
    }

    /**
     * Возращает все свойства компонента
     * 
     * @return mixed
     */
    public function getProperties()
    {
        foreach ($this->_properties as $name => $value) {
            if (is_object($value))
                $this->_properties[$name] = $value->getProperties();
        }

        return $this->_properties;
    }

    /**
     * Возращает все свойства компонента
     * (псевдоним от getProperties)
     * 
     * @return mixed
     */
    public function getInterface()
    {
        return $this->getProperties();
    }
}
?>