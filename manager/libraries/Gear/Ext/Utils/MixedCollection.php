<?php
/**
 * Gear Manager
 * 
 * Коллекция компонентов
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Ext
 * @package    Utils
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: MixedCollection.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * Коллекция компонентов
 * Класс ExtJS Ext.util.MixedCollection
 *
 * @category   Ext
 * @package    Utils
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: MixedCollection.php 2016-01-01 15:00:00 Gear Magic $
 */
class Ext_MixedCollection
{
    /**
     * Массив компонентов
     *
     * @var array
     */
    protected $_items = array();

    /**
     * Конструктор
     * 
     * @param  array $property массив свойств
     * @return void
     */
    public function __construct($items = array())
    {
        $this->_items = $items;
    }

    /**
     * Добавление компонента в список компонентов
     * 
     * @param  mixed $item компонент
     * @return void
     */
    public function add($item)
    {
        $this->_items[] = $item;
    }

    /**
     * Добавление компонента в список компонентов
     * 
     * @param  mixed $item компонент
     * @return void
     */
    public function addItems($items)
    {
        for ($i = 0; $i < sizeof($items); $i++)
            $this->_items[] = $items[$i];
    }

    /**
     * Удаляет все компоненты из списка
     *
     * @var void
     */
    public function clear()
    {
        $this->_items = array();
    }

    /**
     * Возращает указатель на компонент
     * 
     * @param  integer $index индекс компонента
     * @return mixed
     */
    public function &get($index)
    {
        return $this->_items[$index];
    }

    /**
     * Возращает количество компонентов в массиве
     * 
     * @return integer
     */
    public function getCount()
    {
        return count($this->_items);
    }

    /**
     * Возращает интерфейсы компонентов
     * 
     * @return array
     */
    public function getProperties()
    {
        $items = array();
        $count  = count($this->_items);
        for ($i = 0; $i < $count; $i++)
            if (is_object($this->_items[$i]))
                $items[] = $this->_items[$i]->getProperties();
            else
                $items[] = $this->_items[$i];

        return $items;
    }

    /**
     * Возращает компоненты
     * 
     * @return array
     */
    public function getItems()
    {
        return $this->_items;
    }

    /**
     * Установка компонента в массив
     * 
     * @param  array $item компонент
     * @param  integer $index индекс массива
     * @return void
     */
    public function set($index, $item)
    {
        $this->_items[$index] = $item;
    }

    /**
     * Добавления компонента в начало массива
     * 
     * @param  array $item компонент
     * @return void
     */
    public function addFirst($item)
    {
        array_unshift($this->_items, $item);
    }

    /**
     * Добавление компонента в конец массива
     * 
     * @param  array $item компонент
     * @param  integer $index индекс массива
     * @return void
     */
    public function addEnd($item)
    {
        array_unshift($this->_items, $item);
    }
}
?>