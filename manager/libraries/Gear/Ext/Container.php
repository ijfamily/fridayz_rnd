<?php
/**
 * Gear Manager
 *
 * Контейнер компонентов
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Ext
 * @package    Container
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Container.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * @see Ext_Component
 */
require_once('Component.php');

/**
 * Класс компонента "Контейнер"
 *
 * @category   Ext
 * @package    Container
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Container.php 2016-01-01 15:00:00 Gear Magic $
 */
class Ext_Container extends Ext_Component
{
    /**
     * Массив компонентов контейнера
     *
     * @var object
     */
    public $items;

    /**
     * Конструктор
     * 
     * @param  array $property массив свойств
     * @return void
     */
    public function __construct($properties = array())
    {
        parent::__construct($properties);

        if (!isset($this->_properties['items']))
            $this->_properties['items'] = array();

        $this->items = new Ext_MixedCollection($this->_properties['items']);
    }

    public function getProperties()
    {
        if ($this->items->getCount() > 0)
            $this->_properties['items'] = $this->items->getProperties();
        else
            unset($this->_properties['items']);

        return parent::getProperties();
    }
}
?>