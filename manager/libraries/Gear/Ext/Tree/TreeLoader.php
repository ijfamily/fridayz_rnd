<?php
/**
 * Gear Manager
 *
 * Загрузчик данных дерева
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Ext
 * @package    Tree
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: TreeLoader.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * @see Ext_Component
 */
require_once('Gear/Ext/Component.php');

/**
 * Класс компонента "Data Source"
 * Класс Store инкапсулирует кэш объектов Record на стороне клиента, которые 
 * обеспечивают входные данные для компонентов, таких как GridPanel, ComboBox, или DataView
 *
 * @category   Ext
 * @package    Tree
 * @subpackage TreeGridLoader
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: TreeLoader.php 2016-01-01 15:00:00 Gear Magic $
 */
class Ext_Tree_TreeGridLoader extends Ext_Component
{
    /**
     * Свойства компонента
     *
     * @var array
     */
    protected $_properties = array(
        'xtype'         => 'treegridloader',
        'requestMethod' => 'GET',
        'dataUrl'       => ''
    );
}
?>