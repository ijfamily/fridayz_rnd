<?php
/**
 * Gear Manager
 * 
 * Пакет обработки статей
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Libraries
 * @package    Article
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Article.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * Класс обработки статей
 * 
 * @category   Libraries
 * @package    Article
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Article.php 2016-01-01 15:00:00 Gear Magic $
 */
final class GArticle
{
    /**
     * Получение URL статьи
     * 
     * @param  integer $typeId (0 - статья, 1 - новость)
     * @param  integer $artId идент. статьи
     * @param  string $artUri uri статьи
     * @param  integer $catId идент. категории
     * @param  string $catUri uri категории
     * @param  boolean $sef если работает ЧПУ
     * @param  boolean $scheme подстановка схемы
     * @return boolean
     */
    public static function getUrl($domain, $typeId, $artId, $artUri, $catUri = '', $sef = true, $scheme = true)
    {
        if ($scheme)
            $url = 'http://' . $domain . '/';
        else
            $url = '';
        // если работает ЧПУ
        if ($sef) {
            // если главная страница в категории
            if (self::isIndex($artUri))
                $artUri = '';
            // если есть категория
            if ($catUri && $catUri != '/')
                $url .= $catUri;
            // если статическая страница
            if ($typeId == 1) {
                $url .= $artUri;
            } else 
            // если тип не статическая страница
            if ($artUri)
                $url .= $artId . '-' . $artUri;
        } else {
            $url .= '?a=' . $artId;
        }

        return $url;
    }

    /**
     * Является ли статья главной в категории
     * 
     * @return boolean
     */
    public static function isIndex($url)
    {
        return $url == 'index.html' || $url == 'index.htm' || $url == 'index';
    }

    /**
     * Удаление кэша статьи
     * 
     * @return void
     */
    public static function deleteCache($id, $query = null)
    {
        Gear::library('File');

        if (is_null($query))
            $query = new GDb_Query();

        // данные кэша страницы
        $sql = 'SELECT * FROM `site_cache` WHERE `cache_generator`=\'article\' AND `cache_generator_id` IN (' . $id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $items = array();
        while (!$query->eof()) {
            $cache = $query->next();
            // если есть кэш
            if ($cache['cache_filename']) {
                GFile::delete(DOCUMENT_ROOT . 'cache/' . $cache['cache_filename']);
            }
        }
        // удаление записей кэша
        $sql = 'DELETE FROM `site_cache` WHERE `cache_generator`=\'article\' AND `cache_generator_id` IN (' . $id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Удаление документов статьи
     * 
     * @param  integer $id идинд. статьи
     * @return void
     */
    public static function deleteDocs($id, $query = null)
    {
        // документы статьи
        $sql = 'SELECT * FROM `site_documents` WHERE `article_id`=' . $id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $config = GFactory::getConfig();
        $path = '..' . $config->getFromCms('Site', 'DIR/DOCS');
        while (!$query->eof()) {
            $doc = $query->next();
            // если есть документ
            if ($doc['document_filename'])
                GFile::delete($path . $doc['document_filename'], false);
        }
        // удаление записей таблицы "site_documents_l" (тектс документов статьи)
        $sql = 'DELETE `dl` FROM `site_documents_l` `dl`, `site_documents` `d` '
             . 'WHERE `dl`.`document_id`=`d`.`document_id` '
             . 'AND `d`.`article_id`=' . $id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        // удаление записей таблицы "site_documents" (документы статьи)
        $sql = 'DELETE FROM `site_documents` WHERE `article_id`=' . $id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Удаление видео файлов статьи
     * 
     * @param  integer $id идинд. статьи
     * @return void
     */
    public static function deleteVideo($id, $query = null)
    {
        // видео файлы статьи
        $sql = 'SELECT * FROM `site_video` WHERE `article_id`=' . $id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $config = GFactory::getConfig();
        $path = '..' . $config->getFromCms('Site', 'DIR/VIDEO');
        while (!$query->eof()) {
            $video = $query->next();
            // если есть видео
            if ($video['video_filename'])
                GFile::delete($path . $video['video_filename'], false);
        }
        // удаление записей таблицы "site_video_l" (текст видео файлов статьи)
        $sql = 'DELETE `vl` FROM `site_video_l` `vl`, `site_video` `v` '
             . 'WHERE `vl`.`video_id`=`v`.`video_id` AND `v`.`article_id`=' . $id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        // удаление записей таблицы "site_video" (видео файлы статьи)
        $sql = 'DELETE FROM `site_video` WHERE `article_id`=' . $id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Удаление аудио файлов
     * 
     * @param  integer $id идинд. статьи
     * @return void
     */
    public static function deleteAudtio($id, $query = null)
    {
        // аудио файлы статьи
        $sql = 'SELECT * FROM `site_audio` WHERE `article_id`=' . $id;
        if ($query->execute($sql) === false)
            throw new GSqlException();

        $config = GFactory::getConfig();
        $path = '..' . $config->getFromCms('Site', 'DIR/AUDIO');
        while (!$query->eof()) {
            $audio = $query->next();
            // если есть аудио статьи
            if ($audio['audio_filename'])
                GFile::delete($path . $audio['audio_filename'], false);
        }
        // удаление записей таблицы "site_audio_l" (текст аудио файлов статьи)
        $sql = 'DELETE `al` FROM `site_audio_l` `al`, `site_audio` `a` '
             . 'WHERE `al`.`audio_id`=`a`.`audio_id` AND `a`.`article_id`=' . $id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        // удаление записей таблицы "site_audio" (аудио файлы статьи)
        $sql = 'DELETE FROM `site_audio` WHERE `article_id`=' . $id;
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Удаление тегов статьи
     * 
     * @param  integer $id идинд. статьи
     * @return void
     */
    public static function deleteTags($id, $query = null)
    {
        if (is_null($query))
            $query = new GDb_Query();

        // удаление записей таблицы "site_tags" (комментарии статьи)
        $sql = 'DELETE FROM `site_tags` WHERE `article_id` IN (' . $id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Удаление комментариев статьи
     * 
     * @param  integer $id идинд. статьи
     * @return void
     */
    public static function deleteComments($id, $query = null)
    {
        if (is_null($query))
            $query = new GDb_Query();

        // удаление записей таблицы "site_comments" (комментарии статьи)
        $sql = 'DELETE FROM `site_comments` WHERE `article_id` IN (' . $id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Удаление страниц статьи
     * 
     * @param  integer $id идинд. статьи
     * @return void
     */
    public static function deletePages($id, $query = null)
    {
        if (is_null($query))
            $query = new GDb_Query();

        // удаление записей таблицы "site_pages" (страницы статьи)
        $sql = 'DELETE FROM `site_pages` WHERE `article_id` IN (' . $id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Удаление целевых страниц
     * 
     * @param  integer $id идинд. статьи
     * @return void
     */
    public static function deleteLandingPages($id, $query = null)
    {
        if (is_null($query))
            $query = new GDb_Query();

        // удаление записей таблицы "site_landing_pages" (целевые страницы)
        $sql = 'DELETE FROM `site_landing_pages` WHERE `article_id` IN (' . $id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
    }

    /**
     * Возращает статью
     * 
     * @param  integer $articleId идинд. статьи
     * @return void
     */
    public static function get($articleId, $query = null)
    {
        if (is_null($query))
            $query = new GDb_Query();
        // статья
        $sql = 'SELECT * FROM `site_articles` WHERE `article_id`=' . $articleId;
        if (($rec = $query->getRecord($sql)) === false)
            throw new GSqlException();

        if (empty($rec))
            return false;
        else
            return $rec;
    }

    /**
     * Удаление статьи (ей)
     * 
     * @param  mixed $id идинд. статьи (ей)
     * @param  object $query указатель на 
     * @return void
     */
    public static function delete($id, $query = null)
    {
        if (is_null($query))
            $query = new GDb_Query();

        Gear::library('File');

        $config = GFactory::getConfig();
        $dirDocs = DOCUMENT_ROOT . $config->getFromCms('Site', 'DIR/DOCS');
        $dirImages = DOCUMENT_ROOT . $config->getFromCms('Site', 'DIR/IMAGES');

        $id = $query->escapeStr($id, false);
        // список выделенных статей
        $sql = 'SELECT * FROM `site_articles` WHERE `article_id` IN (' . $id . ')';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        while (!$query->eof()) {
            $rec = $query->next();
            // удаление изображений и документов статей
            if (!empty($rec['article_folder'])) {
                $dir = $dirDocs . $rec['article_folder'] . '/';
                if (file_exists($dir))
                    GDir::remove($dir);
                $dir = $dirImages . $rec['article_folder'] . '/';
                if (file_exists($dir))
                    GDir::remove($dir);
            }
        }
        // удаление кэша статьи
        self::deleteCache($id, $query);
        // удаление комментариев
        self::deleteComments($id, $query);
        // удаление страниц сайта
        self::deletePages($id, $query);
        // удаление целевых страниц сайта
        self::deleteLandingPages($id, $query);
    }

    /**
     * Удаление всех статей
     * 
     * @param  object $query указатель на 
     * @return void
     */
    public static function deleteAll($query = null)
    {
        if (is_null($query))
            $query = new GDb_Query();

        Gear::library('File');

        $config = GFactory::getConfig();
        // удаление изображений статьи
        GDir::clear('..' . $config->getFromCms('Site', 'DIR/IMAGES'), array('index.html'), true);
        // удаление кэша
        GDir::clear('../cache/', array('index.html'), true);

        // удаление комментариев статей
        if ($query->clear('site_comments') === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_comments') === false)
            throw new GSqlException();

        // удаление статей
        if ($query->clear('site_articles') === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_articles') === false)
            throw new GSqlException();

        // удаление страниц
        if ($query->clear('site_pages') === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_pages') === false)
            throw new GSqlException();

        // удаление целевых страниц
        if ($query->clear('site_landing_pages') === false)
            throw new GSqlException();
        if ($query->dropAutoIncrement('site_landing_pages') === false)
            throw new GSqlException();
    }
}
?>