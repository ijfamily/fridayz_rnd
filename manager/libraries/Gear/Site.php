<?php
/**
 * Gear Manager
 * 
 * Пакет работы с модулем сайта
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Libraries
 * @package    Site
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Site.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * Класс работы с модулем сайта
 * 
 * @category   Libraries
 * @package    Site
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Site.php 2016-01-01 15:00:00 Gear Magic $
 */
final class GSite
{
    /**
     * Возращает список компонентов для HTML редактора
     * 
     * @param boolean $forArticle компоненты только для статьи
     * @return array
     */
    public static function getEditorComponents($iconPath = '', $forArticle = true)
    {
        $query = new GDb_Query();
        // компоненты сайта
        $sql = 'SELECT * FROM `site_components` '
             . 'WHERE `cmp_for_article`=' . ($forArticle ? 1: 0) . ' AND `cmp_hidden`=0 AND `cmp_parent_id` IS NOT NULL ORDER BY `cmp_parent_id`';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $items = array();
        while (!$query->eof()) {
            $rec = $query->next();
            $item = array(
                'text'      => $rec['cmp_name'],
                'cmpConfig' => array('className' => $rec['cmp_class'])
            );
            if ($rec['cmp_icon'])
                $item['icon'] = $iconPath . $rec['cmp_icon'];
            else
                $item['iconCls'] = 'icon-item-component';
            $items[] = $item;
        }

        return $items;
    }

    /**
     * Возращает список компонентов для HTML редактора
     * 
     * @param boolean $forArticle компоненты только для статьи
     * @return array
     */
    public static function getEditorComponents1($forArticle = true, $query = null)
    {
        if (is_null($query))
            $query = new GDb_Query();
        // компоненты сайта
        $sql = 'SELECT * FROM `site_components` '
             . 'WHERE `cmp_for_article`=' . ($forArticle ? 1: 0) . ' AND `cmp_hidden`=0 AND `cmp_parent_id` IS NOT NULL ORDER BY `cmp_parent_id`';
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $items = array();
        while (!$query->eof()) {
            $rec = $query->next();
            $items[] = array(
                'text'      => $rec['cmp_name'],
                'image'     => empty($rec['cmp_icon']) ? 'icon-none' : $rec['cmp_icon'],
                'cmpConfig' => array('className' => $rec['cmp_class'])
            );
        }

        return $items;
    }

    /**
     * Удаление изображений статьи
     * 
     * @param object $query
     * @return array
     */
    public static function getComponents($query = null)
    {
        if (is_null($query))
            $query = new GDb_Query();
        // изображения статьи
        $sql = 'SELECT * FROM `site_components` WHERE `article_id`=' . $articleId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $items = array();
        while (!$query->eof()) {
            $rec = $query->next();
            $items[] = $rec;
        }

        return  $items;
    }

    /**
     * Удаление изображений статьи
     * 
     * @param integer $articleId
     * @param object $query
     * @return array
     */
    public static function getArticleComponents($articleId, $query = null)
    {
        if (is_null($query))
            $query = new GDb_Query();
        // компоненты на всех страницах
        $sql = 'SELECT * FROM `site_pages` WHERE `article_id`=' . $articleId;
        if ($query->execute($sql) === false)
            throw new GSqlException();
        $items = array();
        while (!$query->eof()) {
            $rec = $query->next();
            // если нет данных компонентов в статье
            if (empty($rec['component_attributes'])) continue;
            // компоненты статьи
            $components = json_decode($rec['component_attributes'], true);
            switch (json_last_error()) {
                case JSON_ERROR_DEPTH:
                    throw new GException('Data processing error', 'Maximum stack depth exceeded');
                    break;

                case JSON_ERROR_CTRL_CHAR:
                    throw new GException('Data processing error', 'Unexpected control character found');
                    break;

                case JSON_ERROR_SYNTAX:
                case 2:
                    throw new GException('Data processing error', 'Syntax error, malformed JSON');
                    break;

                case JSON_ERROR_NONE:
                    break;
            }
            $items[$rec['language_id']] = $components;
        }

        return  $items;
    }

    /**
     * Удаление изображений статьи
     * 
     * @param integer $cmpId
     * @param string $cmpClass
     * @param object $query
     * @return void
     */
    public static function getComponentBy($cmpId = 0, $cmpClass = '', $query = null)
    {
        if ($cmpId == 0 && empty($cmpClass)) return array();
        if ($cmpId)
            $sql = '`cmp_id`=' . $cmpId;
        else
        if ($cmpClass)
            $sql = "`cmp_class`='$cmpClass'";

        if (is_null($query))
            $query = new GDb_Query();
        // свойства компонента для канкретного класса
        $sql = 'SELECT * FROM `site_components` WHERE ' . $sql;
        if (($cmp = $query->getRecord($sql)) === false)
            throw new GSqlException();
        if (empty($cmp))
            throw new GException('Data processing error', 'Component not exist in stack components');
        $data = array();
        // свойства компонента
        if ($cmp['cmp_settings']) {
            //GFactory::getDg()->log($cmp['cmp_settings']);
            $data = json_decode($cmp['cmp_settings'], true);
            switch (json_last_error()) {
                case JSON_ERROR_DEPTH:
                    throw new GException('Data processing error', 'Maximum stack depth exceeded');
                    break;
    
                case JSON_ERROR_CTRL_CHAR:
                    throw new GException('Data processing error', 'Unexpected control character found');
                    break;
    
                case JSON_ERROR_SYNTAX:
                case 2:
                    throw new GException('Data processing error', 'Syntax error, malformed JSON');
                    break;
    
                case JSON_ERROR_NONE:
                    break;
            }
        }

        return array('data' => $data, 'component' => $cmp);;
    }
}
?>