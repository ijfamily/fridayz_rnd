<?php
/**
 * Gear Manager
 * 
 * Пакет справочника
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Libraries
 * @package    Reference
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Reference.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * Класс справочников
 * 
 * @category   Libraries
 * @package    Reference
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Reference.php 2016-01-01 15:00:00 Gear Magic $
 */
class GReference
{
   /**
     * Кэшировать справочники
     * 
     * @var boolean
     */
    public $caching = true;

    /**
     * Префикс для хранения данных справочников в сессии
     * 
     * @var string
     */
    public $prefix = 'reference/';

   /**
     * Справочники array("name" => "table")
     * 
     * @var array
     */
    protected $_settings = array();

   /**
     * Сессия
     * 
     * @var object
     */
    protected $_session;

    /**
     * Конструктор
     * 
     * @return void
     */
    public function __construct($settings = array())
    {
        if ($settings)
            $this->_settings = $settings;

        $this->_session = GFactory::getSession();
        $this->_session->start();
    }

    /**
     * Инициализация справочников
     * 
     * @param array $settings cправочники array("name" => "table")
     * @return void
     */
     public function init($settings)
     {
        $this->_settings = $settings;
     }

    /**
     * Возращает SQL запрос
     * 
     * @param string $name название справочника
     * @return string
     */
    protected function getQuery($refe)
    {
        if (isset($refe['condition']))
            $cond = ' AND ' . $refe['condition'];
        else
            $cond = '';

        return 'SELECT * FROM ' . $refe['table'] . ' WHERE `reference_hidden` =0' . $cond;
    }

    /**
     * Возращает SQL запрос по идент. записи
     * 
     * @param string $name название справочника
     * @param integer $id идент. записи справочника
     * @return string
     */
    protected function getQueryById($refe, $id)
    {
        if (isset($refe['condition']))
            $cond = ' AND ' . $refe['condition'];
        else
            $cond = '';

        return 'SELECT * FROM ' . $refe['table'] . ' WHERE `reference_hidden` =0 AND `reference_id`=' . $id . $cond;
    }

    /**
     * Добавление справочника
     * 
     * @param string $name название справочника
     * @param array $attr настройки справочника
     * @return array
     */
    public function add($name, $attr)
    {
        return $this->_settings[$name] = $attr;
    }

    /**
     * Удаление справочника из кэша
     * 
     * @param string $name название справочника
     * @return void
     */
    public function remove($name)
    {
        // если указанный справочник не существует
        if (($refe = $this->isExist($name)) === false) return false;

        // если справочник кэшируется
        if ($refe['caching'])
            // если есть кэш
             if ($this->_session->has($this->prefix . $name))
                $this->_session->set($this->prefix . $name, null);
        // удалить из списка справочников
        unset($this->_settings[$name]);
    }

    /**
     * Удаление всех справочников
     * 
     * @return void
     */
    public function clear()
    {
        foreach($this->_settings as $name => $attr)
            $this->remove($name);
    }

    /**
     * Возращает запись ($id) справочника ($name)
     * 
     * @param string $id идентификатор записи
     * @param string $name название справочника
     * @param string $default значение поумочланию
     * @return mixed
     */
    public function getFromCache($id, $field, $name, $default = '')
    {
        $rec = $this->_session->get($id, $default, $this->prefix . $name);
        if ($rec !== false) {
            
            if (isset($rec[$field]))
                return $rec[$field];
        }
        return $default;
    }

    /**
     * Возращает запись ($id) справочника ($name)
     * 
     * @param string $id идентификатор записи
     * @param string $name название справочника
     * @param boolean $fromCache если на прямую надо брать из кэша
     * @param string $default значение поумочланию
     * @return mixed
     */
    public function getRec($id, $name, $field = '', $default = '', $fromCache = true)
    {
        if ($fromCache) {
            $rec = $this->_session->get($id, $default, $this->prefix . $name);
            if ($rec) {
                if ($field)
                    return $rec[$field];
                else
                    return $rec;
            } else
                return $default;
        }

        // если указанный справочник не существует
        if (($refe = $this->isExist($name)) === false) return $default;

        // если есть справочник в кэше
        if ($this->_session->has($this->prefix . $name)) {
            return $this->_session->get($id, $default, $this->prefix . $name);
        } else {
            $data = $this->getFromDb($name);
            // если есть данные и справочник надо кэшировать
            if ($data && $refe['caching'])
                $this->_session->set($this->prefix . $name, $data);
            // если есть запись
            if (isset($data[$id]))
                return $data;
        }

        return $default;
    }

    /**
     * Возращает указанные справочники
     * 
     * @param string имена ("name1", ...)
     * @return array
     */
    public function get()
    {
        $result = '';

        try {
            // все имена из списка аргументов
            $size = func_num_args();
            $query = null;
            for ($i = 0; $i < $size; $i++) {
                $name = func_get_arg($i);
                if (!isset($this->_settings[$name]))
                     continue;
                $refe = $this->_settings[$name];
                // если есть в кэше
                 if ($this->_session->has($this->prefix . $name)) {
                    $data = $this->_session->get($this->prefix . $name);
                    $result[$name] = $data;
                 // если нет в кэше
                 } else {
                    // если не было запросов
                    if ($query == null) {
                        // соединение с базой данных
                        GFactory::getDb()->connect();
                        $query = new GDb_Query();
                    }
                    $sql = $this->getQuery($refe);
                    if ($query->execute($sql) === false)
                        throw new GSqlException();
                    $data = array();
                    while (!$query->eof()) {
                        $rec = $query->next();
                        $data[$rec['reference_id']] = $rec;
                    }
                    // если есть данные и справочник надо кэшировать
                    if ($data && $refe['caching'])
                        $this->_session->set($this->prefix . $name, $data);
                    $result[$name] = $data;
                 }
            }
        } catch(GException $e) {}

        if ($size == 1) 
            return $data;
        else
            return $result;
    }

    /**
     * Кэшировать указанные справочники
     * 
     * @param string имена ("name1", ...)
     * @return array
     */
    public function caching()
    {
        try {
            // все имена из списка
            $size = sizeof($this->_settings);
            // если есть кэширование
            $query = null;
            foreach($this->_settings as $name => $attr) {
                // если нет кэширования
                if (!$attr['caching']) continue;
                // если не было запросов
                if ($query == null) {
                    // соединение с базой данных
                    GFactory::getDb()->connect();
                    $query = new GDb_Query();
                }
                $sql = $this->getQuery($attr);
                if ($query->execute($sql) === false)
                    throw new GSqlException();
                $data = array();
                while (!$query->eof()) {
                    $rec = $query->next();
                    $data[$rec['reference_id']] = $rec;
                }
                // если есть данные и справочник надо кэшировать
                if ($data)
                    $this->_session->set($this->prefix . $name, $data);
            }
        } catch(GException $e) {}
    }

    /**
     * Кэшировать указанные справочники
     * 
     * @param string имена ("name1", ...)
     * @return array
     */
    public function toCache()
    {
        $result = '';

        try {
            // соединение с базой данных
            GFactory::getDb()->connect();
            $query = new GDb_Query();
            // все имена из списка аргументов
            $size = func_num_args();
            for ($i = 0; $i < $size; $i++) {
                $name = func_get_arg($i);
                // если справочник существует
                if (isset($this->_settings[$name])) {
                    $refe = $this->_settings[$name];
                    // если справочник нуждается в кэшировании
                    if ($refe['caching']) {
                        $sql = $this->getQuery($refe);
                        if ($query->execute($sql) === false)
                            throw new GSqlException();
                        $data = array();
                        while (!$query->eof()) {
                            $rec = $query->next();
                            $data[$rec['reference_id']] = $rec;
                        }
                        // если есть в запросе
                        if ($data)
                            $this->_session->set($this->prefix . $name, $data);
                        $result[$name] = $data;
                    }
                }
            }
        } catch(GException $e) {}

        return $result;
    }

    /**
     * Проверка существования справочника
     * 
     * @return mixed
     */
    public function isExist($name)
    {
        if (isset($this->_settings[$name]))
            return $this->_settings[$name];
        else
            return false;
    }

    /**
     * Возращает все справочники
     * 
     * @return array
     */
    public function getFromDb($name)
    {
        $result = array();

        // если указанный справочник не существует
        if (($refe = $this->isExist($name)) === false) return false;

        // соединение с базой данных
        GFactory::getDb()->connect();
        $query = new GDb_Query();
        $sql = $this->getQuery($refe);
        if ($query->execute($sql) === false)
            throw new GSqlException();
        while (!$query->eof()) {
            $rec = $query->next();
            $result[$rec['reference_id']] = $rec;
        }

        return $result;
    }

    /**
     * Возращает все справочники
     * 
     * @return array
     */
    public function getAll()
    {
        $result = '';

        try {
            // все имена из списка
            $size = sizeof($this->_settings);
            // если есть кэширование
            $query = null;
            foreach($this->_settings as $name => $refe) {
                // если есть в кэше
                 if ($this->_session->has($this->prefix . $name)) {
                    $result[$name] = $this->_session->get($this->prefix . $name);
                 // если нет в кэше
                 } else {
                    // если не было запросов
                    if ($query == null) {
                        // соединение с базой данных
                        GFactory::getDb()->connect();
                        $query = new GDb_Query();
                    }
                    $sql = $this->getQuery($refe);
                    if ($query->execute($sql) === false)
                        throw new GSqlException();
                    $data = array();
                    while (!$query->eof()) {
                        $rec = $query->next();
                        $data[$rec['reference_id']] = $rec;
                    }
                    // если есть данные и справочник надо кэшировать
                    if ($data && $refe['caching'])
                        $this->_session->set($this->prefix . $name, $data);
                    $result[$name] = $data;
                 }
            }
        } catch(GException $e) {}

        return $result;
    }
}
?>