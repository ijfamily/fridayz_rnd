<?php
/**
 * Gear Manager
 * 
 * Пакет строк
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Libraries
 * @package    String
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: String.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * Класс строк
 * 
 * @category   Libraries
 * @package    String
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: String.php 2016-01-01 15:00:00 Gear Magic $
 */
class GString
{
    /**
     * Массив символов языков для транслита
     *
     * @var array
     */
    static $langs = array(
        'ru' => array(
            'а' => 'a', 'б' => 'b', 'в' => 'v',
            'г' => 'g', 'д' => 'd', 'е' => 'e',
            'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
            'и' => 'i', 'й' => 'y', 'к' => 'k',
            'л' => 'l', 'м' => 'm', 'н' => 'n',
            'о' => 'o', 'п' => 'p', 'р' => 'r',
            'с' => 's', 'т' => 't', 'у' => 'u',
            'ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch','ш' => 'sh', 'щ' => 'sch',
            'ь' => '','ы' => 'y', 'ъ' => '',
            'э' => 'e', 'ю' => 'yu', 'я' => 'ya',
            'А' => 'A', 'Б' => 'B', 'В' => 'V',
            'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
            'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z',
            'И' => 'I', 'Й' => 'Y', 'К' => 'K',
            'Л' => 'L', 'М' => 'M', 'Н' => 'N',
            'О' => 'O', 'П' => 'P', 'Р' => 'R',
            'С' => 'S', 'Т' => 'T', 'У' => 'U',
            'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
            'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch',
            'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya'
        ),
        'uk' => array(
            'а' => 'a', 'б' => 'b', 'в' => 'v',
            'ґ' => 'g',
            'г' => 'g', 'д' => 'd', 'е' => 'e',
            'ё' => 'yo', 'ж' => 'zh', 'з' => 'z',
            'і' => 'i', 'ї' => 'yi',
            'и' => 'i', 'й' => 'j', 'к' => 'k',
            'л' => 'l', 'м' => 'm', 'н' => 'n',
            'о' => 'o', 'п' => 'p', 'р' => 'r',
            'с' => 's', 'т' => 't', 'у' => 'u',
            'ф' => 'f', 'х' => 'h', 'ц' => 'ts',
            'ч' => 'ch','ш' => 'sh', 'щ' => 'sch',
            'ь' => '','ы' => 'y', 'ъ' => '',
            'э' => 'e', 'ю' => 'ju', 'я' => 'ya',
            'А' => 'A', 'Б' => 'B', 'В' => 'V',
            'Ґ' => 'G',
            'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
            'Ё' => 'YO', 'Ж' => 'Zh', 'З' => 'Z',
            'І' => 'I', 'Ї' => 'Yi',
            'И' => 'I', 'Й' => 'J', 'К' => 'K',
            'Л' => 'L', 'М' => 'M', 'Н' => 'N',
            'О' => 'O', 'П' => 'P', 'Р' => 'R',
            'С' => 'S', 'Т' => 'T', 'У' => 'U',
            'Ф' => 'F', 'Х' => 'H', 'Ц' => 'Ts',
            'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch',
            'Э' => 'E', 'Ю' => 'Ju', 'Я' => 'Ya'
        )
    );

    /**
     * Транслит строки
     * 
     * @params string $str строка
     * @params string $lang язык ("ru", "uk")
     * @return string
     */
    public static function convert($str, $lang)
    {
        if (!isset(self::$langs[$lang])) return false;

        return strtr($str, self::$langs[$lang]);
    }

    /**
     * Генерация URL адреса из транслита строки
     * 
     * @params string $str строка
     * @params string $lang язык ("ru", "uk")
     * @return string
     */
    public static function toUrl($str, $lang)
    {
        if (($str = self::convert($str, $lang)) === false) return $str;

        // в нижний регистр
        $str = strtolower(trim($str));
        // заменям все ненужное нам на "-"
        $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
        $str = trim($str, '-');

        return $str;
    }

    /**
     * Возращает название файла в транслите
     * 
     * @params string $filename название файла
     * @params string $lang язык ("ru", "uk")
     * @return string
     */
    public static function fileToUrl($filename, $lang)
    {
        $info = pathinfo($filename);

        return self::toUrl($info['filename'], $lang) . '.' . strtolower($info['extension']);
    }

    /**
     * Убрать символы из строки
     * 
     * @params string $str строка
     * @params boolean $tags уберать теги
     * @return string
     */
    public static function stripStr($str, $tags = true)
    {
        if ($tags)
            $str = strip_tags($str);
        $str = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $str);

        return trim($str);
    }

    /**
     * Убрать символы из строки
     * 
     * @params string $str строка
     * @params boolean $tags уберать теги
     * @return string
     */
    public static function stripTitle($str, $tags = true)
    {
        $str = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $str);
        $str = str_replace(array('<br>', '<br/>', '<br />'), ' ', $str);
        if ($tags)
            $str = strip_tags($str);
        $str = trim($str);
        if ($str)
            $str = self::cutChar($str, 194);

        return $str;
    }

    /**
     * Убрать символы из строки
     * 
     * @params string $str строка
     * @params boolean $tags уберать теги
     * @return string
     */
    public static function cutChar($str, $ascii)
    {
        if (($pos1 = mb_strpos($str, chr($ascii))) === false)
            return $str;
        if (($pos2 = mb_strpos($str, chr($ascii), $pos1)) === false)
            return mb_substr($str, $pos1 + 1, mb_strlen($str) - 1);

        return mb_substr($str, $pos2 + 1, mb_strlen($str) - 1);
    }

    /**
     * Копирование строки
     * 
     * @params string $subject строка
     * @params string $start начальная позиция
     * @params string $end конечная позиция
     * @params boolean $strip убрать символы
     * @return string
     */
    public static function copyStr($subject, $start, $end, $strip = false)
    {
        $str = mb_substr($subject, $start, $end - $start);
        if ($strip)
            return self::stripStr($str, false);
        else
            return $str;
    }

    /**
     * Копирование строки
     * 
     * @params string $subject строка
     * @params string $length длина
     * @return string
     */
    public static function ellipsis($subject, $length)
    {
        if (mb_strlen($subject) > $length)
            return mb_substr($subject, 0, $length) . ' ...';
        else
            return $subject;
    }

    /**
     * Возращает строку из чисел и литер
     * 
     * @params string $str строка
     * @return string
     */
    public static function toSimpleStr($str)
    {
        return preg_replace('%[^A-Za-zА-Яа-я0-9]%', '', $str);
    }

    /**
     * Возращает слово соответствующие диапазону значений $num
     * 
     * @params integer $num число, от которого будет зависеть форма слова
     * @params string $arr массив слов
     * $arr[0] первая форма слова, например Товар
     * $arr[1] вторая форма слова - Товара
     * $arr[2] третья форма множественного числа слова - Товаров
     * @return string
     */
    public static function wordForm($num, $arr){
        $num = abs($num) % 100; // берем число по модулю и сбрасываем сотни (делим на 100, а остаток присваиваем переменной $num)
        $num_x = $num % 10; // сбрасываем десятки и записываем в новую переменную
        if ($num > 10 && $num < 20) // если число принадлежит отрезку [11;19]
            return $arr[2];
        if ($num_x > 1 && $num_x < 5) // иначе если число оканчивается на 2,3,4
            return $arr[1];
        if ($num_x == 1) // иначе если оканчивается на 1
            return $arr[0];
        return $arr[2];
    }
}
?>