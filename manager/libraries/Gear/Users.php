<?php
/**
 * Gear Manager
 * 
 * Пакет справочника
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Libraries
 * @package    Reference
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Reference.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * Класс справочников
 * 
 * @category   Libraries
 * @package    Reference
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Reference.php 2016-01-01 15:00:00 Gear Magic $
 */
class GUsers
{
   /**
     * Кэшировать справочники
     * 
     * @var boolean
     */
    public $caching = true;

    /**
     * Префикс для хранения данных справочников в сессии
     * 
     * @var string
     */
    public static $prefix = 'users/data';

   /**
     * Сессия
     * 
     * @var object
     */
    protected $_session;

    /**
     * Возращает все справочники
     * 
     * @return array
     */
    public function getAll()
    {
        $result = array();

        try {
            // соединение с базой данных
            GFactory::getDb()->connect();
            $query = new GDb_Query();
            $sql = 'SELECT * FROM `gear_users` `u` JOIN `gear_user_profiles` `p` USING (`user_id`) ';
            if ($query->execute($sql) === false)
                throw new GSqlException();
            $data = array();
            while (!$query->eof()) {
                $rec = $query->next();
                $result[$rec['user_id']] = array(
                    'user_id'        => $rec['user_id'],
                    'user_name'      => $rec['user_name'],
                    'profile_id'     => $rec['profile_id'],
                    'profile_name'   => $rec['profile_name'],
                    'profile_photo'  => $rec['profile_photo'],
                    'profile_gender' => $rec['profile_gender'],
                    'contact_email'  => $rec['contact_email'],
                    'photo'          => 'data/photos/' . (empty($rec['profile_photo']) ? 'none_s.png' : $rec['profile_photo'])
                );
            }
        } catch(GException $e) {}

        return $result;
    }

    public function toCache()
    {
        $_SESSION[self::$prefix] = $this->getAll();
    }

    public static function get($userId, $field = '', $default = '')
    {
        if (isset($_SESSION[self::$prefix][$userId])) {
            if ($field)
                return $_SESSION[self::$prefix][$userId][$field];
            else
                return $_SESSION[self::$prefix][$userId];
        }

        return $default;
    }
}
?>