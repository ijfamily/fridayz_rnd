<?php
/**
 * Gear Manager
 * 
 * Пакет обработки дат
 * 
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Libraries
 * @package    Date
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Date.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * Класс обработки дат
 * 
 * @category   Libraries
 * @package    Date
 * @copyright  Copyright (c) 2011-2013 <gearmagic.ru@gmail.com>
 * @license    http://www.gnu.org/licenses/lgpl.html
 * @version    $Id: Date.php 2012-07-30 15:00:00 Gear Magic $
 */
class GDate
{
    /**
     * Flips date xx-xx-xxxx -> xxxx-xx-xx
     * 
     * @param  string $delimiter separator
     * @param  string $date date
     * @return string
     */
    public static function flipDate($delimiter = '-', $date)
    {
        if ($date) {
            $arr = explode($delimiter, $date);
            return $arr[2] . $delimiter . $arr[1] . $delimiter . $arr[0];
        } else
            return $date;
    }

    /**
     * Сalculate date difference
     * 
     * @param  string $interval type output
     * @param  integer $date1 timestamp
     * @param  integer $date2 timestamp
     * @return mixed
     */
    public static function dateDiff($interval,$date1,$date2)
    {
        $timedifference = $date2 - $date1;
        switch ($interval) {
            case 'w':
                    return bcdiv($timedifference,604800);

            case 'D':
                    return bcdiv($timedifference,86400);

            case 'd':
                    return bcmod($timedifference,86400);

            case 'h':
                $h = bcdiv($timedifference,3600);
                return bcmod($h,3600);

            case 'hh':
                $h = bcdiv($timedifference,3600);
                if (($v = bcmod($h,3600)) < 10)
                    return $v = '0' . $v;
                else
                    return $v;
                break;

            case 'H':
                return bcdiv($timedifference,3600);

            case 'M':
                return bcdiv($timedifference,60);

            case 'm':
                $m = bcdiv($timedifference,60);
                return bcmod($m,60);

            case 'mm':
                $m = bcdiv($timedifference,60);
                if (($v = bcmod($m,60)) < 10)
                    return $v = '0' . $v;
                else
                    return $v;

            case 'S':
                return $timedifference;

            case 's':
                return date('s', $timedifference);
        }
    }
}
?>