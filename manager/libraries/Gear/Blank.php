<?php
/**
 * Gear Manager
 *
 * Пакет формирования отчетов
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Libraries
 * @package    Blank
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: Blank.php 2016-01-01 15:00:00 Gear Magic $
 */

/**
 * Morphological analysis
 * 
 * @param  integer $n
 * @param  integer $f1
 * @param  integer $f2
 * @param  integer $f5
 * @return integer
 */
function str_money_morph($n, $f1, $f2, $f5)
{
    $n = abs($n) % 100;
    $n1= $n % 10;
    if ($n>10 && $n<20) return $f5;
    if ($n1>1 && $n1<5) return $f2;
    if ($n1==1) return $f1;
    return $f5;
}

/**
 * Convert number to string words
 * 
 * @param  integer $number number
 * @param  boolean $stripePenny convert a penny
 * @param  boolean $stripeMoney user money
 * @return string
 */
function str_money($number, $stripePenny = false, $stripeMoney = false)
{
    $nol = 'нуль';
    $str[100]= array('','сто', 'двісті', 'триста', 'чотиреста', 'п\'ятсот', 'шістсот', 'сімсот', 'вісімсот', 'дев\'ятсот');
    $str[11] = array('','десять', 'одинадцять', 'дванадцять', 'тринадцять', 'чотирнадцять', 'п\'ятнадцять', 'шістнадцять', 'сімнадцять', 'вісімнадцять', 'дев\'ятнадцять', 'двадцять');
    $str[10] = array('','десять', 'двадцять', 'тридцять', 'сорок', 'п\'ятьдесят', 'шістьдесят', 'сімдесят', 'вісімдесят', 'дев\'яносто');
    $nums = array(
        array('','один', 'дві', 'три', 'чотири', 'п\'ять', 'шість', 'сім', 'вісім', 'дев\'ять'),// m
        array('','одна', 'дві', 'три', 'чотири', 'п\'ять', 'шість', 'сім', 'вісім', 'дев\'ять') // f
    );
    $forms = array(
        array('копійка', 'копійки', 'копійок', 1), // 10^-2
        array('гривня', 'гривні', 'гривень',  0), // 10^ 0
        array('тисяч',' тисячі',' тисяч', 1), // 10^ 3
        array('мільйон', 'мільйона', 'мільйонів',  0), // 10^ 6
        array('мільярд', 'мільярда', 'мільярдів',  0), // 10^ 9
        array('трильйон', 'трильйона', 'трильйонів',  0), // 10^12
    );
    if ($stripeMoney) {
        $stripePenny = true;
        $forms[1] = array('', '', '', 0);
    }
        
    $out = $tmp = array();
    // Поехали!
    $tmp = explode('.', str_replace(',','.', $number));
    $rub = number_format($tmp[ 0], 0,'','-');
    if ($rub== 0) $out[] = $nol;
    // нормализация копеек
    $kop = isset($tmp[1]) ? substr(str_pad($tmp[1], 2, '0', STR_PAD_RIGHT), 0,2) : '00';
    $segments = explode('-', $rub);
    $offset = sizeof($segments);
    if ((int)$rub== 0) { // если 0 рублей
        $o[] = $nol;
        $o[] = str_money_morph( 0, $forms[1][ 0],$forms[1][1],$forms[1][2]);
    }
    else {
        foreach ($segments as $k=>$lev) {
            $numsi = (int) $forms[$offset][3]; // определяем род
            $ri = (int) $lev; // текущий сегмент
            if ($ri== 0 && $offset>1) {// если сегмент==0 & не последний уровень(там Units)
                $offset--;
                continue;
            }
            // нормализация
            $ri = str_pad($ri, 3, '0', STR_PAD_LEFT);
            // получаем циферки для анализа
            $r1 = (int)substr($ri, 0,1); //первая цифра
            $r2 = (int)substr($ri,1,1); //вторая
            $r3 = (int)substr($ri,2,1); //третья
            $r22= (int)$r2.$r3; //вторая и третья
            // разгребаем порядки
            if ($ri>99) $o[] = $str[100][$r1]; // Сотни
            if ($r22>20) {// >20
                $o[] = $str[10][$r2];
                $o[] = $nums[ $numsi ][$r3];
            }
            else { // <=20
                if ($r22>9) $o[] = $str[11][$r22-9]; // 10-20
                elseif($r22> 0) $o[] = $nums[ $numsi ][$r3]; // 1-9
            }
            // Рубли
            $o[] = str_money_morph($ri, $forms[$offset][ 0],$forms[$offset][1],$forms[$offset][2]);
            $offset--;
        }
    }
    // Копейки
    if (!$stripePenny) {
        $o[] = $kop;
        $o[] = str_money_morph($kop,$forms[ 0][ 0],$forms[ 0][1],$forms[ 0][2]);
    }
    return preg_replace("/\s{2,}/",' ',implode(' ',$o));
}

/**
 * Condition for repeating row in repeating groups
 * 
 * @param  integer $index index of list
 * @param  integer $page page of list
 * @param  array $data data from query
 * @return boolean
 */
function smarty_clear($smarty)
{
    foreach ($smarty as $key => $value)
        $smarty[$key] = 0;

    return $smarty;
}

/**
 * Condition for repeating row in repeating groups
 * 
 * @param  integer $index index of list
 * @param  integer $page page of list
 * @param  array $data data from query
 * @return boolean
 */
// $smartyA = array('[alias]' => 'field', ...)
// $smartyB = array()
// $data = array('field' => 'value', ...)
function smarty_ireplaces($smartyA, $smartyB, $data, $values, $text)
{
    $smarty = array();
    foreach ($smartyA as $alias => $field)
        if (isset($data[$field])) {
            $values[] = $data[$field];
            $smartyB[] = $alias;
        } else {
            $values[] = '';
            $smartyB[] = $alias;
        }

    return str_ireplace($smartyB, $values, $text);
}

/**
 * Condition for repeating row in repeating groups
 * 
 * @param  integer $index index of list
 * @param  integer $page page of list
 * @param  array $data data from query
 * @return boolean
 */
function smarty_ireplace($smarty, $text)
{
    return str_ireplace(array_keys($smarty), array_values($smarty), $text);
}

/**
 * Condition for repeating row in repeating groups
 * 
 * @param  array $haystack index of list
 * @param  string $needly needly to find
 * @param  integer $pos data from query
 * @return integer
 */
function strpos_left($haystack, $needly, $pos)
{
    $tag = '';
    $is_end = false;
    while (!$is_end) {
        $pos--;
        if ($pos == 0 || $tag == $needly)
            $is_end = true;
        $tag = $haystack[$pos] . $tag;
        if ($haystack[$pos] == ' ')
            $tag = '';
    }

    return $pos;
}


class GBlank
{
    /**
     * Render global variables to blank
     */
    const SMARTY_GLOBAL     = 1;

    /**
     * Render repeating row
     */
    const SMARTY_RAW        = 2;

    /**
     * Render repeating rows
     */
    const SMARTY_RAWS       = 3;

    /**
     * Render repeating row in repeating groups
     */
    const SMARTY_GROUP_RAWS = 4;

    /**
     * Smarty
     *
     * @var array
     */
    protected $_smarty = array();

    /**
     * Filename of blank
     *
     * @var string
     */
    protected $_fileName = '';

    /**
     * Template of blank
     *
     * @var string
     */
    protected $_template = '';

    /**
     * Constructor
     * 
     * @return void
     */
    public function __construct($fileName = '')
    {
        $this->_fileName = $fileName;
    }

    /**
     * Set filename
     * 
     * @param  string $fileName filename of blank
     * @return void
     */
    public function setFileName($fileName)
    {
        $this->_fileName = $fileName;
    }

    /**
     * Get filename
     * 
     * @return string
     */
    public function getFileName()
    {
        return $this->_fileName;
    }

    /**
     * Add smarty
     * 
     * @param  array $params smarty params
     * @param  array $data data from query
     * @param  integer $type type of smarty
     * @return void
     */
    public function addSmarty($data, $params = array(), $type = self::SMARTY_RAWS)
    {
        $params['data'] = $data;
        $params['type'] = $type;
        $this->_smarty[] = $params;
    }

    /**
     * Set blank template
     * 
     * @param  srting $template blank template
     * @return void
     */
    protected function setTemplate($template)
    {
        $this->_template;
    }

    /**
     * Get blank template
     * 
     * @return string
     */
    protected function getTemplate()
    {
        $this->_template;
    }

    /**
     * Render global variables to blank
     * 
     * @param  array $params smarty params (aliasData)
     * @param  array $data data from query
     * @return boolean
     */
    protected function replaceGlobal($data, $params)
    {
        if (empty($data))
            return false;
        if (is_array($data)) {
            $values = $smarty = array();
            foreach ($params['aliasData'] as $alias => $field)
                if (isset($data[$field])) {
                    $values[] = $data[$field];
                    $smarty[] = $alias;
                }
        }
        if (is_object($data)) {
            $record = $data->first();
            //print_r($record);
            $values = $smarty = array();
            foreach ($params['aliasData'] as $alias => $field)
                if (isset($record[$field])) {
                    $values[] = $record[$field];
                    $smarty[] = $alias;
                }
        }
        // update blank
        $this->_template = str_ireplace($smarty, $values, $this->_template);

        return true;
    }

    /**
     * Get structure of repeating row
     * 
     * @param  string $alias alias of tags
     * @param  string $text text of row
     * @return array
     */
    protected function getRepeatRow($alias, $text)
    {
        if (($p = strpos($text, $alias)) === false)
            return false;
        $s = strpos_left($text, '<tr', $p);
        $e = strpos($text, 'tr>', $p) + 3;

        return array('start' => $s, 'end' => $e, 'text' => substr($text, $s, $e - $s));
    }

    /**
     * Get structure of repeating rows
     * 
     * @param  string $aliasStart alias tag of start row
     * @param  string $aliasEnd alias tag of end row
     * @param  string $text text of rows
     * @return array
     */
    protected function getRepeatRows($aliasStart, $aliasEnd, $text)
    {
        if (($p = strpos($text, $aliasStart)) === false)
            return false;
        $s = strpos_left($text, '<tr', $p);
        if (($p = strpos($text, $aliasEnd)) === false)
            return false;
        $e = strpos($text, 'tr>', $p) + 3;

        return array('start' => $s, 'end' => $e, 'text' => substr($text, $s, $e - $s));
    }

    /**
     * Render repeating rows
     * 
     * @param  array $params smarty params (aliasTags, aliasData)
     * @param  array $data data from query
     * @return boolean
     */
    protected function callRepeatRows($data, $params)
    {
        // if exist alias in template get text of rows
        $rows = $this->getRepeatRows($params['aliasTags']['start'],
                                     $params['aliasTags']['end'],
                                     $this->_template);
        if ($rows === false)
            return false;
        $rowsText = '';
        while (!$data->eof()) {
            $record = $data->next();
            $rowsText = $rowsText . smarty_ireplaces($params['aliasData'],
                                                     array($params['aliasTags']['start'],
                                                     $params['aliasTags']['end']),
                                                     $record, array('', ''),
                                                     $rows['text']);
        }
        // update blank
        $this->_template = substr_replace($this->_template, $rowsText, $rows['start'], $rows['end'] - $rows['start']);

        return true;
    }

    /**
     * Render repeating row in repeating groups
     * 
     * @param  array $params smarty params (aliasTags, aliasTotals, aliasTotalsFunc, condition, aliasData)
     * @param  array $data data from query
     * @return boolean
     */
    protected function callRepeatGroupRows($data, $params)
    {
        if (!isset($params['dropTotals']))
            $params['dropTotals'] = true;
        if (!isset($params['aliasTotals']))
            $params['aliasTotals'] = '';
        $smarty = $params['smarty'];
        if (empty($data))
            return false;
        // get text of rows, if exist in template alias $aliasTagStart or $aliasTagEnd
        if (($rows = $this->getRepeatRows($params['aliasTags']['start'], $params['aliasTags']['end'], $this->_template)) === false)
            return false;
        // drops alias in text of rows
        $rows['text'] = str_ireplace(array($params['aliasTags']['start'], $params['aliasTags']['end']), array('', ''), $rows['text']);
        // get row text from rows template, if exist in rows alias [repeatRow]
        if (($row = $this->getRepeatRow($smarty['aliasTags']['row'], $rows['text'])) === false)
            return false;
        $rowText = $rowsText = '';
        // index for numeartion
        $index = $page = 1;
        // if function exist
        if (!function_exists($params['condition']))
            return false;
        while (!$data->eof()) {
            // data record
            $record = $data->next();
            //print $record['list_name'] . '<br>';
            if (isset($params['preprocessingFunc']))
                $record = call_user_func($params['preprocessingFunc'], $record);

            if (call_user_func($params['condition'], $index, $page, $record)) {
                // replacing recurring row (JUST FOR NEXT PAGE)
                $index = 1;
                $replaceRow = smarty_ireplaces($smarty['aliasData'],
                                               array('[#]', $smarty['aliasTags']['row']),
                                               $record,
                                               array($index, ''),
                                               $row['text']);

                $replaceRows = substr_replace($rows['text'], $rowText, $row['start'], $row['end'] - $row['start']);
                if ($params['aliasTotals']) {
                    $replaceRows = smarty_ireplace($params['aliasTotals'], $replaceRows);
                    if ($params['dropTotals'])
                        $params['aliasTotals'] = smarty_clear($params['aliasTotals']);
                }
                if ($params['aliasData'])
                    $replaceRows = smarty_ireplaces($params['aliasData'], array(), $record1, array(), $replaceRows);
                $rowsText = $rowsText . $replaceRows;
                //$rowText = $replaceRow;
                $rowText = '';
                $index = 1;
                $page++;
            } 
            //else {
                // calculate totals alias
                if ($params['aliasTotals']) {
                    $params['aliasTotals'] = call_user_func($params['aliasTotalsFunc'], $record, $params['aliasTotals']);
              //  }
                // replacing recurring row
                $replaceRow = smarty_ireplaces($smarty['aliasData'],
                                               array('[#]', $smarty['aliasTags']['row']),
                                               $record,
                                               array($index, ''),
                                               $row['text']);
                $rowText = $rowText . $replaceRow;
            }

            $index++;
            $record1 = $record;
        }
        // update rows
        if ($index > 1) {
                $replaceRows = substr_replace($rows['text'], $rowText, $row['start'], $row['end'] - $row['start']);
                if ($params['aliasTotals']) {
                    $replaceRows = smarty_ireplace($params['aliasTotals'], $replaceRows);
                    if ($params['dropTotals'])
                        $params['aliasTotals'] = smarty_clear($params['aliasTotals']);
                }
                if ($params['aliasData'])
                    $replaceRows = smarty_ireplaces($params['aliasData'], array(), $record1, array(), $replaceRows);
                $rowsText = $rowsText . $replaceRows;
        }
        // update blank
        $this->_template = substr_replace($this->_template, $rowsText, $rows['start'], $rows['end'] - $rows['start']);

        return true;
    }

    /**
     * Render repeating row
     * 
     * @param  array $params smarty params (aliasTags)
     * @param  array $data data from query
     * @return boolean
     */
    protected function callRepeatRow($data, $params)
    {
        if (empty($data))
            return false;
        // if exist alias in template get text of row
        if (($row = $this->getRepeatRow($params['aliasTags']['row'], $this->_template)) === false)
            return false;
        $text = '';
        if (is_object($data)) {
            $index = 1;
            while (!$data->eof()) {
                $record = $data->next();
                $values = $smarty = array();
                foreach ($params['aliasData'] as $alias => $field)
                    if (isset($record[$field])) {
                        $values[] = $record[$field];
                        $smarty[] = $alias;
                    } else {
                        $values[] = '';
                        $smarty[] = $alias;
                    }
                $values[] = '';
                $smarty[] = $params['aliasTags']['row'];
                $values[] = $index++;
                $smarty[] = '[#]';
                $text .= str_ireplace($smarty, $values, $row['text']);
            }
        }
        if (is_array($data)) {
            $count = count($data);
            for ($i = 0; $i < $count; $i++) {
                $record = $data[$i];
                $values = $smarty = array();
                foreach ($aliasData as $alias => $field)
                    if (isset($record[$field])) {
                        $values[] = $record[$field];
                        $smarty[] = $alias;
                    } else {
                        $values[] = '';
                        $smarty[] = $alias;
                    }
                $values[] = '';
                $smarty[] = $params['aliasTags']['row'];
                $values[] = $i+1;
                $smarty[] = '[#]';
                $text .= str_ireplace($smarty, $values, $row['text']);
            }
        }
        // update blank
        $this->_template = substr_replace($this->_template, $text, $row['start'], $row['end'] - $row['start']);

        return true;
    }

    /**
     * Export data
     * 
     * @return void
     */
    public function export()
    {
        // load blank
        $this->_template = file_get_contents($this->_fileName, true);

        $count = count($this->_smarty);
        for ($i = 0; $i < $count; $i++) {
            $smarty = $this->_smarty[$i];
            switch ($smarty['type']) {
                // render data to blank
                case Blank::SMARTY_GLOBAL:
                    $this->replaceGlobal($smarty['data'], $smarty);
                    break;

                // render repeating rows
                case Blank::SMARTY_RAWS:
                    $this->callRepeatRows($smarty['data'], $smarty);
                    break;

                // render repeating row
                case Blank::SMARTY_RAW:
                    $this->callRepeatRow($smarty['data'], $smarty);
                    break;

                // render repeating row in repeating groups
                case Blank::SMARTY_GROUP_RAWS:
                    $this->callRepeatGroupRows($smarty['data'], $smarty);
                    break;
            }
        }

        // output of data blank
        print $this->_template;
    }
}
?>