<?php
/**
 * Gear Manager
 *
 * Шаблона рассылки писем"
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Templates
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: posted_mail.tpl.php 2016-01-01 12:00:00 Gear Magic $
 */

if (!($tpl = &Gear::tpl('data-mail'))) return;
?>
<html>
<body>
<?php echo $tpl['text'];?>
</body>
</html>