<?php
/**
 * Gear Manager
 *
 * Шаблон восстановления учетной записи пользователя
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Templates
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: restore-ru-RU.php 2016-01-01 12:00:00 Gear Magic $
 */

if (!($d = &Gear::tpl('data-mail'))) return;
?>
<html><head><title>Восстановление учётной записи <? print $d['version'];?> (<? print $d['host'];?>)</title></head>
<body style="font-size:13px;font-family:arial, Geneva, sans-serif; ;">
Для восстановления учётной записи, Вам необходимо перейти по указанной ссылке:<br /><br />
<a href="<? print $d['link'];?>"><? print $d['link'];?></a><br /><br />
Ссылка действительна в течении 24ч с момента получения письма.<br />
<div style="margin-top: 5px;">
<font style="text-decoration: underline;font-size: 14px;">Новая учётная запись</font><br />
<b>логин:</b> <? print $d['login'];?><br />
<b>пароль:</b> <? print $d['password'];?><br />
</div>
</body>
</html>