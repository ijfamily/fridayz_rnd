<?php
/**
 * Gear Manager
 *
 * Шаблон восстановления учетной записи пользователя
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Templates
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: restore-en-GB.php 2016-01-01 12:00:00 Gear Magic $
 */

if (!($d = &Gear::tpl('data-mail'))) return;
?>
<html><head><title>Restore user account for <? print $d['version'];?> (<? print $d['host'];?>)</title></head>
<body style="font-size:13px;font-family:arial, Geneva, sans-serif; ;">
To restore your account, you will need to click on the link:<br /><br />
<a href="<? print $d['link'];?>"><? print $d['link'];?></a><br /><br />
Link is valid within 24 hours of receipt of the letter.<br />
<div style="margin-top: 5px;">
<font style="text-decoration: underline;font-size: 14px;">New Account</font><br />
<b>login:</b> <? print $d['login'];?><br />
<b>password:</b> <? print $d['password'];?><br />
</div>
</body>
</html>