<?php
/**
 * Gear Manager
 *
 * Шаблон восстановления учетной записи пользователя
 *
 * LICENSE
 * 
 * Gear Manager is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Templates
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: restore-uk-UA.php 2016-01-01 12:00:00 Gear Magic $
 */

if (!($d = &Gear::tpl('data-mail'))) return;
?>
<html><head><title>Відновлення облікового запису <? print $d['version'];?> (<? print $d['host'];?>)</title></head>
<body style="font-size:13px;font-family:arial, Geneva, sans-serif; ;">
Для відновлення облікового запису, Вам необхідно перейти за вказаним URL:<br /><br />
<a href="<? print $d['link'];?>"><? print $d['link'];?></a><br /><br />
Посилання дійсна протягом 24 год з моменту одержання листа.<br />
<div style="margin-top: 5px;">
<font style="text-decoration: underline;font-size: 14px;">Нова обліковий запис</font><br />
<b>логін:</b> <? print $d['login'];?><br />
<b>пароль:</b> <? print $d['password'];?><br />
</div>
</body>
</html>