<?php
/**
 * Gear CMS
 *
 * Главный сценарий
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Index
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: index.php 2016-01-01 12:00:00 Gear Magic $
 */

/**
 * Установите уровень отчетов об ошибках в PHP, только не в php.ini.
 * @see  http://php.net/error_reporting
 *
 * При разработке приложения, настоятельно рекомендуется включить уведомления
 * и строгие предупреждения. Включить их с помощью: REPORTING = development
 *
 * При работе в стандартном режиме используйте REPORTING = production
 */

/**
 * Константа для подключаемых файлов, чтобы предотвратить прямой доступ
 */
define('_INC', 1);

/**
 * Корень сайта (если работает "open_basedir" с результатом вида "/home/site:/usr/lib/php:/usr/local/lib/php:/tmp",
 * то необходимо вписать значение "/home/site/www/", если "open_basedir" отключен то "")
 */
define('DOCUMENT_ROOT', '');

/**
 * Путь к приложению
 */
define('PATH_APPLICATION', 'application/');

/**
 * Константа для вывода уровня ошибок:
 * development - режим разработки (вывод всех ошибок)
 * production - режим работы сайта (без вывода ошибок)
 */
define('REPORTING', 'development');
// уровень ошибок
define('REPORTING_ERRORS', 0/* ^ E_DEPRECATED*/);

/**
 * Блокировка IP адресов (сайт для них будет недоступен)
 */
$lock = require(PATH_APPLICATION . 'config/Locking.php');
// если есть таблица ip адресов
if ($lock !== false) {
    if ($lock['ACTIVE']) {
        if (isset($lock['IP'][$_SERVER['REMOTE_ADDR']])) {
            header('HTTP/1.1 503 Service Temporarily Unavailable');
            if ($lock['TEMPLATE'])
                require(PATH_APPLICATION . 'templates/system/error_503.php');
            exit;
        }
    }
}

/**
 * Загрузка базовых настроек системы
 */
require(PATH_APPLICATION . 'core/Gear.php');

GTimer::start('app', 'application');

// инициализация обработчика исключений
GException::initialise();

try {
    // инициализация и вывод данных приложения
    Gear::$app = GFactory::getApplication();
    Gear::$app->initialise();
    Gear::$app->render();
} catch(GException $e) {
    $e->renderDialog();
}

// если отладка
if (Gear::$app->debug) {
    GTimer::stop('app');
    Gear::$app->debug->info('Время загрузки скрипта');
    foreach (GTimer::$times as $name => $time) {
        $report = GTimer::getReport($name);
        Gear::$app->debug->log($report[0], $report[1]);
    }
}

// вывод исключений
if (!Gear::$app->loadedFromCache)
    GException::render();
?>