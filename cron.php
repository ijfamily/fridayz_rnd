<?php
/**
 * Gear CMS
 *
 * Cron
 * классический демон-планировщик задач в UNIX-подобных операционных системах, использующийся для периодического 
 * выполнения заданий в определённое время
 *
 * LICENSE
 * 
 * Gear CMS is made available under Commercial License or the GNU General Public License version 3 (GPLv3)
 * http://www.gnu.org/licenses/quick-guide-gplv3.ru.html
 * 
 * @category   Gear
 * @package    Cron
 * @copyright  Copyright (c) 2013-2016 Gear Magic <manager@gearmagic.ru>
 * @license    http://www.gearmagic.ru/license/
 * @version    $Id: cron.php 2016-01-01 12:00:00 Gear Magic $
 */

if (!empty($_POST) || defined('_CRON') || empty($_GET['token']) || empty($_GET['action'])) die();

/**
 * Константа для работы крона, чтобы предотвратить прямой доступ
 */
define('_CRON', true);

/**
 * Загрузка базовых настроек системы
 */
require('application/core/Gear.php');

// инициализация обработчика исключений
GException::initialise();

try {
    // конфигурации системы
    $config = GFactory::getConfig(PATH_CONFIG);
    if ($config->get('CMS/TOKEN') != $_GET['token']) die();
    // действие
    $action = $_GET['action'];
    switch ($action) {
    }
    // action
} catch(GException $e) {
    $e->renderDialog();
}

// вывод исключений
GException::render();
?>