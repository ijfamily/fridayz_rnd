/*!
 * Gear CMS (http://gearmagic.ru/)
 * Copyright 2013-2016 Gear Magic <manager@gearmagic.ru>
 * Licensed under GPLv3 (http://www.gearmagic.ru/license/)
 */

gear = new function(){
    var config = {
            sef: true,
            path: '/themes/gear/'
        },
        isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
        forms = [],
        galleries = [],
        components = [];

    // Convert date
    function fDate(date){
        var dd = date.getDate();
        if (dd < 10) dd = '0' + dd;
        var mm = date.getMonth() + 1;
        if (mm < 10) mm = '0' + mm;

        return date.getFullYear() + '-' + mm + '-' + dd;
    }

    // Add scripts
    function js(filename, success){
        $.ajax({
            type: "GET",
            url: /*config.path + 'js/' + */filename,
            dataType: "script",
            cache: true,
            success: success
        });
    }

    // Add stylesheets
    function css(filename){
        $('head').append('<link rel="stylesheet" type="text/css" href="' + filename +'" />');
    }

    // Counting the number of users
    function callVisor(){
        // article
        var a = $('article'),
            data = { id: '', cls: '', url: document.location.href, get: { article: 0, banners: 0 } };
        if (a.length) {
            data.id = a.attr('id');
            data.cls = a.attr('class');
        }
        data.get.article = a.length;
        // banners
        var b = [];
        $('.banner').each(function(index, item){ b.push($(item).attr('id')); });
        if (b.length)
            data.get.banners = b;

        $.ajax({
            url: '/request/visor/',
            type: 'POST',
            cache: false,
            dataType: 'json',
            data: data,
            success: function(data, status, xhr){
                if (!data.success) return;
                if (typeof data.setTo != 'undefined') {
                    for (var i = 0; i < data.setTo.length; i++) {
                        $('#' + data.setTo[i]).html(data.setTo[i].html);
                    }
                }
                if (typeof data.banners != 'undefined')
                    initBanners(data.banners);
            }
        });
    }

    // Return Site Manager control
    function getSite(){
        if (typeof window.top.Site != 'undefined')
            return window.top.Site;
        else
            return false;
    }

    // Return Manager control
    function getManager(){
        if (typeof window.top.Manager != 'undefined')
            return window.top.Manager;
        else
            return false;
    }

    // Return all forms
    function getForms(){
        if (typeof window.gearForms != "undefined")
            return window.gearForms;
        else
            return 0;
    }

    // Return all galleries
    function getGalleries(){
        return $('img[data-zoom=true]');
    }

    // Return all Gear components at design mode
    function getComponents(){
        if (typeof window.gearComponents != 'undefined')
            return window.gearComponents;
        else
            return 0;
    }

    // Check if frame
    function isFrame(){ return getManager() ? true : false; }

    // Switch to design view
    function designMode(){
        var components = getComponents();
        if (!components) return false;
        if (isFrame()) {
            $('head').append('<link rel="stylesheet" type="text/css" href="/themes/gear/css/jquery.contextMenu.css" />');
            $('head').append('<script type="text/javascript" src="/themes/gear/js/jquery.contextMenu.js"></script>');

            var gc;
            for (var i = 0; i < components.length; i++) {
                $.contextMenu({
                    gc: components[i],
                    trigger: 'left',
                    selector: '#ctrl-' + components[i].id ,
                    callback: function(key, options) {
                        var item = options.items[key], url = '?to=frame';
                        if (key == 'property') {
                            if (options.gc.belongTo == 'template') {
                                options.gc.pageId = options.gc.templateId;
                            }
                            url = options.gc.pageId + url + '&bTo=' + options.gc.belongTo + '&tId=' + options.gc.templateId + '&cId=' + options.gc.id + '&cClass=' + options.gc.className;
                        } else
                        if (key == 'add') {
                            url += '&parent=' + options.gc.dataId;
                        } else
                            url = options.gc.dataId + url;
                        getSite().load(item.url + url);
                    },
                    items: components[i].menu
                });
            }

            return true;
        } else
            $('section[role=component]').addClass('no-design');

        return false;
    }

    // Initialization banners
    function initBanners(data){
        var b, bt, d, index = 0, notShow = false;
        for (var i = 0; i < data.length; i++) {
            b = data[i];
            bt = $('#' + b.id);
            if (bt.length > 0) {
                if (b.items.length > 1)
                    index = Math.floor(Math.random() * b.items.length);
                else
                    index = 0;
                d = b.items[index];
                notShow = false;
                if (d.exceptions.length > 0 && document.location.pathname != '/')
                    notShow = d.exceptions.indexOf(document.location.pathname) > 0;
                if (!notShow)
                    switch (d.type) {
                        case 1: bt.attr('style', d.style); $(body).append('<a class="gear-banner-bg-a" href="' + d.url + '" target="_blank"></a>'); break;
                        case 2: bt.html(d.html); break;
                        case 3: bt.html(d.code); break;
                    }
            }
        }
    }

    // Initialization all galleries
    function initGalleries(galleries){
        var img;
        galleries.each(function(index, item){
            img = $(item);
            var atrA = '', atrI = '', dt = img.attr('data-type'), t = typeof img.attr('title') == 'undefined' ? '' : img.attr('title');
            if (typeof dt != 'undefined')
                atrA += ' class="pirobox_gall" rel="gallery"';
            else
                atrA += ' class="pirobox" rel="single"';
                atrA += ' title="'+ t + '"';
                atrI += ' title="' + t + '" alt="' + t + '"';
            if (typeof img.attr('class') != 'undefined')
                atrI += ' class="' + img.attr('class') + '"';
            if (typeof img.attr('style') != 'undefined')
                atrI += ' style="' + img.attr('style') +'"';
            img.replaceWith('<a href="'+ img.attr('data-zoom-src') + '"' + atrA + '><img src="' + img.attr('src') + '"' + atrI + '/></a>');
        });
        if (galleries.length || $('.pirobox_gall').length > 0 ||  $('.pirobox').length > 0) {
            $('head').append('<link rel="stylesheet" type="text/css" href="/themes/gear/css/pirobox/style2/style.css" />');
            $('head').append('<script type="text/javascript" src="/themes/gear/js/jquery-ui.custom.min.js?v=1.8.2"></script>');
            $('head').append('<script type="text/javascript" src="/themes/gear/js/jquery.pirobox.ext.min.js?v=1.0"></script>');
            $().piroBox_ext({
                piro_speed : 700,
                bg_alpha : 0.5,
                piro_scroll : true
            });
        }
    }

    // Initialization all forms
    function initForms(forms){
        function gearFormAjax(event){
            event.preventDefault();
            var $form = $(event.target), fv = $form.data('formValidation'), wrap = $form.find('.gear-form-wrap'),
                gForm = forms[$form.attr('id')];

            $form.css('position', 'relative');
            if (wrap.length == 0)
                wrap = $('<div class="gear-form-wrap"></div>').appendTo($form);
            $.ajax({
                url: $form.attr('action'),
                type: 'POST',
                cache: false,
                dataType: 'json',
                data: $form.serialize(),
                beforeSend: function(){ wrap.fadeIn('slow'); },
                error: function(res, tr){
                    if (typeof json.redirect != 'undefined')
                        if (json.redirect.length > 0) { document.location.href = json.redirect; return; }
                    wrap.fadeOut();
                    $form.find('.alert').remove();
                    if (res.status != 200)
                        $form.prepend('<div class="alert alert-danger"><strong>Error ' + res.status + '</strong>: ' + res.statusText + '!</div>');
                    else
                        $form.prepend(res.responseText);
                    fv.resetForm();

                    if (typeof gForm.valid.handler != 'undefined')
                        $.fn[gForm.valid.handler](res, false);
                },
                 success: function(json){
                    if (typeof json.redirect != 'undefined')
                        if (json.redirect.length > 0) { document.location.href = json.redirect; return; }
                    wrap.fadeOut();
                    $form.find('.alert').remove();
                    $form.prepend('<div class="alert ' + (json.success ? 'alert-success' : 'alert-danger') + '">' + json.message + '</div>');
                    if (json.success) $form[0].reset();
                    fv.resetForm();

                    if (typeof gForm.valid.handler != 'undefined')
                        $.fn[gForm.valid.handler](json, $form, true);
                }
            });
        }

        var form, require = [];
        jQuery.each(forms, function(id, item){
            if (jQuery.inArray(item.valid.name, require) == -1) {
                if (item.valid.css)
                    $('head').append('<link rel="stylesheet" type="text/css" href="/themes/gear/css/jquery.' + item.valid.name + '.min.css" />');
                $('head').append('<script type="text/javascript" src="/themes/gear/js/jquery.' + item.valid.name + '.min.js"></script>');
                require.push(item.valid.name);
            }
            form = $('#' + id);
            if (item.ajax && item.valid.name == 'formValidation')
                form[item.valid.name](item.data).on('success.form.fv', gearFormAjax);
            else
                form[item.valid.name](item.data);
         });
    }

    // Gear initialization
    function init(initConfig){
        // options
        $.extend(config, initConfig);
        // initialization all galleries
        galleries = getGalleries();
        if (galleries)
            initGalleries(galleries);
        // initialization all forms
        forms = getForms();
        if (forms)
            initForms(forms);
        // set design mode if need
        designMode();
        // call visor
        callVisor();
    }

    // ���������� cookie ���� ���� ��� undefined
    function getCookie(name){
        var matches = document.cookie.match(new RegExp(
          "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ))
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }
    
    // �c����������� cookie
    function setCookie(name, value, props) {
        props = props || {};
        var exp = props.expires;
        if (typeof exp == "number" && exp) {
            var d = new Date();
            d.setTime(d.getTime() + exp*1000)
            exp = props.expires = d;
        }

        if(exp && exp.toUTCString) { props.expires = exp.toUTCString() }
        value = encodeURIComponent(value);
        var updatedCookie = name + "=" + value;
        for(var propName in props){
            updatedCookie += "; " + propName;
            var propValue = props[propName];
            if(propValue !== true){ updatedCookie += "=" + propValue }
        }
        document.cookie = updatedCookie;
    }

    function isSignIn(){ return getCookie('user_signin') == 1; }

    function getUserName(){ return getCookie('user_name'); }

    return {
        getUserName: getUserName,

        isSignIn: isSignIn,

        getCookie: getCookie,

        setCookie: setCookie,

        isMobile: isMobile,

        forms: forms,

        galleries: galleries,

        init: init,

        js: js,

        css: css,

        // calendar jquery ui widget
        calendarUi: function(settings){},

        // calendar bootstrap
        calendarBs: function(settings){
            function _highlight(days){
                $(settings.selector + ' .datepicker td').removeClass('highlight');
                for (var i = 0; i < days.length; i++)
                    $('#date-day-' + days[i].day).addClass('highlight');
            }

            function _select(){
                $(settings.selector + ' .datepicker td[class=day]').each(function(index, item){
                        var day = $(item); day.attr('id', 'date-day-' + day.html()).attr('data-day', day.html());
                });
                if (settings.dateOn)
                     $('#date-day-' + settings.dateOn.day).addClass('active');
            }

            function _load(date, category, wrap){
                $.ajax({
                    url: '/?request=calendar',
                    type: 'POST',
                    cache: false,
                    dataType: 'json',
                    data: {date: date, category: category},
                    beforeSend: function(){ wrap.fadeIn('slow'); },
                    error: function(res, tr){
                        if (res.status == 200)
                            wrap.html(res.responseText);
                        else
                            wrap.html('<div class="alert alert-danger"><strong>Error ' + res.status + '</strong>: ' + res.statusText + '!</div>');
                    },
                     success: function(json) {
                        if (!json.success) {
                            wrap.html('<div class="alert alert-danger">' + json.message + '</div>'); return;
                        }
                        wrap.fadeOut();
                        if (json.html.length) { _select(); _highlight(json.html); }
                    }
                });
            }

            css(config.path + 'css/bootstrap.datepicker3.min.css');
            js(config.path + 'js/bootstrap.datepicker.' + settings.language + '.min.js', function(){
                var wrap, dp = $(settings.selector);
                if (dp.length > 0) {
                    dp.datepicker({ language: settings.language, format: 'yyyy-mm-dd' }).
                    on('changeDate', function(ed) { document.location = document.location.pathname + '?date-on=' + fDate(ed.date); }).
                    on('changeMonth', function(em) { if (settings.highlight) _load(fDate(em.date), settings.category, wrap); })
                    _select();
                    wrap = $('<div class="datepicker loader"></div>').appendTo(dp);
                    if (settings.highlight)
                        _load(fDate(new Date()), settings.category, wrap);
                }
            });
        }
    }
}


$(document).ready(function($){
    // fixed JQuery version
    $.browser = {};
    $.browser.mozilla = /mozilla/.test(navigator.userAgent.toLowerCase()) && !/webkit/.test(navigator.userAgent.toLowerCase());
    $.browser.webkit = /webkit/.test(navigator.userAgent.toLowerCase());
    $.browser.opera = /opera/.test(navigator.userAgent.toLowerCase());
    $.browser.msie = /msie/.test(navigator.userAgent.toLowerCase());

    // gear initialization
    gear.init(gearInit);
});