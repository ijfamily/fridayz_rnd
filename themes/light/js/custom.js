/*Add swipe in bootstrap carousel*/

(function ($) {
	
    var touchStartX = null;

    $('.carousel').each(function () {
        var $carousel = $(this);
        $(this).on('touchstart', function (event) {
            var e = event.originalEvent;
            if (e.touches.length == 1) {
                var touch = e.touches[0];
                touchStartX = touch.pageX;
            }
        }).on('touchmove', function (event) {
            var e = event.originalEvent;
            if (touchStartX != null) {
                var touchCurrentX = e.changedTouches[0].pageX;
                if ((touchCurrentX - touchStartX) > 60) {
                    touchStartX = null;
                    $carousel.carousel('prev');
                } else if ((touchStartX - touchCurrentX) > 60) {
                    touchStartX = null;
                    $carousel.carousel('next');
                }
            }
        }).on('touchend', function () {
            touchStartX = null;
        });
    });

})(jQuery);

$(window).on("load resize",function(e){
	//console.log("load resize");
	
	if($('#home-events-grid').length>0){
		horizGrid();
	}
	
	//var pageWidth=$('document').width();
	
	//var elPos=$('.horizontal-grid_item:last');
	//var elPosoffset = elPos.offset();
	//var sliderW =elPosoffset.left+elPos.outerWidth();
	//console.log('offsetLeft='+elPosoffset.left);
	//console.log('offsetRight='+elPosoffset.right);
	//console.log('sliderW='+sliderW);
	
});

function stickTopMenu(){
		
	//var headerHeight = $('header').height();
	var menuHeight = $('#top-navigation').height();
	var bannerHeight = $('#top-page-banner').innerHeight();
	//var height = $('.mainContent').height()-$('.feedback').height()-$('.clearFooter').height()-menuHeight;

	
	if ((!bannerHeight)|| ($.cookie('noBanner'))){
		bannerHeight=0;
	}
	
	console.log('bannerHeight='+bannerHeight+' scrollTop()='+$(window).scrollTop());
	
	if( $(window).scrollTop() >= bannerHeight)
	{
		$('#top-navigation').removeClass('affix-top').addClass('affix');
		/* $('.page-header').css('margin-top',menuHeight); */
	}else{
		$('#top-navigation').removeClass('affix').addClass('affix-top');
		/* $('.page-header').css('margin-top','0px'); */
	}	 
}

$(document).ready(stickTopMenu);

$(window).on("scroll", stickTopMenu);


//news grid slider 
function horizGrid(){
	var numItems = $('.horizontal-grid_item').length; 
	//console.log(numItems);
	var cnt=numItems/2;
	//console.log(cnt);
	var q=0;
	var h=0;
	
	$('.horizontal-grid_item').each(function(index){
		
		var elW=$(this).outerWidth();
		var elH=$(this).outerHeight();
	
		if(Math.round(cnt)>4){
			if(q==Math.round(cnt)){
				q=0;
			}

			if(h>Math.round(cnt)-1){
				var topPos=elH;
			}else{
				var topPos=0;
			}
		}else{
			var topPos=0;
			$(this).parent().height( elH);
		}
		
		var leftPos=q*elW;
		//console.log('q='+q);
		//console.log('h='+h);
		//console.log('top='+topPos);
		
		q=q+1;
		h=h+1;

		$(this).css('left',leftPos); 
		$(this).css('top',topPos); 
		$(this).removeClass('horizontal-grid-hidden');
		$(this).parent().removeClass('loading');
	});
}



$(document).ready(function(){
	
	var is_mobile = ((/Mobile|iPhone|iPod|BlackBerry|Windows Phone/i).test(navigator.userAgent || navigator.vendor || window.opera) ? true : false);

	if ( !is_mobile ) {
	  // whatever the init call is
		/* $('#horizontal-grid').perfectScrollbar({
			suppressScrollY:true,
			scrollbarPosition:"outside",
			documentTouchScroll: false
		}); */
	}else{
		$('#horizontal-grid').addClass('on-mobile');
		$('#home-events-grid').addClass('on-mobile');
	}
 
	
 //news grid slider  buttons	 
	 $('#grid-btn-to-next').click(function(event) {
		var bl_w=$(document).width();
		var cn_w=$('.home-events-grid .container').width()+30;
		var cn_bl=(bl_w-cn_w)/2 ;
		
		if(cn_bl!=0){
			var scr=cn_w-cn_bl+15;
		}else{
			var scr=cn_w-10;
		}
		
		
		
		console.log('bl_w='+bl_w+' cn_w='+cn_w+' cn_bl='+cn_bl);
			event.preventDefault();
			$('#home-events-grid').animate({
			scrollLeft: "+="+scr
			}, "slow");
			//console.log($('#home-events-grid').scrollLeft());
		});
		
	$('#grid-btn-to-prev').click(function(event) {
		var bl_w=$(document).width();
		var cn_w=$('.home-events-grid .container').width()+30;
		var cn_bl=(bl_w-cn_w)/2 ;
		
		if(cn_bl!=0){
			var scr=cn_w-cn_bl+15;
		}else{
			var scr=cn_w-10;
		}
		
		
			event.preventDefault();
			$('#home-events-grid').animate({
			scrollLeft: "-="+scr
		}, "slow");
		//console.log($('#home-events-grid').scrollLeft());
	});
		
	 if ($.cookie('noBanner')) { //if cookie isset
		
        //console.log($.cookie('noBanner') + 'noBanner');
     }else{
		 //console.log($.cookie('noBanner') + 'yesBanner');
		 $('#top-page-banner').fadeIn('slow');
     }
	
	var expire = new Date();
	
	expire.setTime(expire.getTime() + (300 * 60 * 1000));//300 min
	
	$('#banner_close-area').click(function(){
		 var CookieSet = $.cookie('noBanner', '1', { expires: expire }); //set cookie
		  $('#top-page-banner').fadeOut('slow');
		 // console.log('hide banner');
	});
	
	$(function() {
		var $el = $('.section-img-title-bg');
		$(window).on('scroll', function () {
			var scroll = $(document).scrollTop();
			$el.css({
				'background-position':'50% '+(-.5*scroll)+'px'
			});
		});
	});	
});

function gridMoveLeft(){
	
}

