/*
 * #Fridayz - первый медийный онлайн журнал
 *
 * Copyright(c) 2017, Fridayz
 *
 * Date: Jun 4 2017
 */

function showPopup(message){
    $.blockUI({
        onUnblock: function(){ $('body').css('overflow', 'auto'); },
        message: message,
         onOverlayClick: $.unblockUI,
         blockMsgClass: 'container-popup'
    });
}

function setStatVideo(id){
    $.ajax({
        url: 'http://vimeo.com/api/v2/video/' + id + '.json',
        cache: false,
        success: function(data, status, xhr){
            var vd, data = data[0];
            vd = $('#video-' + id);
            if (vd.length > 0) {
                vd.find('.count').html(data.stats_number_of_plays);
            }
            vd.show();
        }
    });
}

$(document).ready(function($){
    if (gear.isMobile) $('body').addClass('mobile');

    /* Highlight location */
    var href, sg = document.location.pathname.split('/');
    if (sg.length < 2)
        sg = '';
    else
        if (sg[1].length == 0)
            sg = '';
        else
            sg = sg[1];
    var isActiveMenu = false;
    if (sg.length > 0) {
        $('nav a').each(function(o, i){
            href = $(this).attr('href');
            if (href.indexOf(sg) > -1) {
                 $(this).parent().addClass('active');
                 isActiveMenu = true;
            }
        });
    }

    /* Main slider */
     var ca = $('#main-slider');
     ca.fadeOut();
     if (ca.length > 0) {
        ca.slick({
            dots: true,
            arrows: true,
            slidesToShow: 1,
            autoplay: true,
			fade: true,
			cssEase: 'linear'
        });
     }
     ca.fadeIn(2000);
     ca.find('.slide-subtitle').each(function(){
        var str = $(this).html();
        $(this).html(str.replace('Луганск,', 'Луганск<br>'));
     });
	 
	  /* albums page slider */
     var ca = $('#page-albums_main-slider');
     ca.fadeOut();
     if (ca.length > 0) {
        ca.slick({
            dots: true,
            arrows: false,
            slidesToShow: 1,
            autoplay: true,
			fade: true,
			cssEase: 'linear'
        });
     }
     ca.fadeIn(2000);
     ca.find('.slide-subtitle').each(function(){
        var str = $(this).html();
        $(this).html(str.replace('Луганск,', 'Луганск<br>'));
     });

    /* Afisha carousel */
     var ca = $('#afisha-slider');
     ca.fadeOut();
     if (ca.length > 0) {
        ca.slick({
            dots: true,
            arrows: false,
            slidesToShow: 5,
            slidesToScroll: 1,
            autoplay: true,
            responsive: [
                { breakpoint: 1050, settings: { slidesToShow: 4, slidesToScroll: 1, infinite: true, dots: true } },
                { breakpoint: 600, settings: { slidesToShow: 2, slidesToScroll: 2 } },
                { breakpoint: 480, settings: { slidesToShow: 1, slidesToScroll: 1 } }
            ]
        });
     }
     ca.fadeIn(2000);
	 
	 /*place fotos*/
 
     var ps = $('#partner-photos');
	 
	 ps.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
		//currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
		var i = (currentSlide ? currentSlide : 0) + 1;
		$('.cur-sl').text(i + ' из ' + slick.slideCount);
		console.log('111');
	});

      ps.fadeOut();
     if ( ps.length > 0) {
         ps.slick({
			adaptiveHeight:true,
            dots: false,
            arrows: true,
            slidesToShow: 1,
            autoplay: false,
			fade: true,
			cssEase: 'linear',
			nextArrow: '.arr-right',
			prevArrow: '.arr-left' 
        });
     }
	 ps.fadeIn(2000);
    /* Video */
    var svideo = $('section.video .video-i');
    if (svideo.length > 0) {
        /*
        $('.video-i-footer').each(function(){
                setStatVideo($(this).attr('data-id'));
        });
        */
        $('.video-i-play').click(function(){
            var mdl = $('#modal-video'), ftr = $('#video' + $(this).attr('data-code')), type = $(this).attr('data-type');
            if (type == 'youtube')
                mdl.find('iframe').attr('src', 'https://www.youtube.com/embed/' + $(this).attr('data-code') + '?rel=0');
            else
                mdl.find('iframe').attr('src', 'https://player.vimeo.com/video/' + $(this).attr('data-code'));
            mdl.find('.modal-title').html($(this).attr('title'));
            mdl.find('.modal-body-footer').html( ftr.html() );
            mdl.modal();
            return false;
        });
    }
    // basically stops and starts the video on modal open/close
    $('#modal-video').on('hidden.bs.modal', function (e) {
        $('#modal-video').find('iframe').attr('src', '');
    });

    /* Album photos */
    gallery = $('.album-photo-body ul');
    if (gallery.length > 0) gallery.lightGallery();

    /* Rating */
    /*
    $('.barrating').barrating({
        theme: 'css-stars',
        deselectable: false,
        onSelect: function(value, text, event) {
            var self = this, ps = this.$elem.parents('.rating'), id = this.$elem.attr('id');
            this.readonly(true);
            $.ajax({
                url: '/request/rating/',
                type: 'POST',
                cache: false,
                dataType: 'json',
                data: { id: id, type: 'place', value: value },
                success: function(data, status, xhr){
                    if (!data.success)
                        ps.find('.rating-msg').html(data.html);
                }
            });
        }
    });
    */

    /* Like */
    $('.place-i-like a').click(function () {
		
        var me = $(this), parent = me.parent().parent();
        if (parent.hasClass('disabled')) return false;
        parent.addClass('disabled');
        $.ajax({
            url: '/request/like/',
            type: 'POST',
            cache: false,
            dataType: 'json',
            data: { id: me.attr('id'), type: 'place' },
            success: function(data, status, xhr){
				console.log('success');
                if (!data.success) {
                    parent.find('.like-msg').html(data.html);
                } else
                    me.find('.like-count').html(data.votes);
            }
        });
        return false;
    });

    /* Yandex Map */
    if ($('#map-places').length > 0) {
        gear.js('http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU', function(){
            ymaps.ready(init);
            var mapPlaces;
            function init(){
                mapPlaces = new ymaps.Map('map-places', { center: [47.222078, 39.720349], behaviors: ['default', 'scrollZoom'], zoom: 12 });
                mapPlaces.behaviors.disable('scrollZoom');
				mapPlaces.controls.add('zoomControl');
				if($(document).innerWidth()<1024){
					//mapPlace.behaviors.disable('drag');
				}
				
                for (var i = 0; i < partnerPlaces.length; i++) {
                    mapPlaces.geoObjects.add(new ymaps.Placemark(partnerPlaces[i][0], partnerPlaces[i][1], partnerPlaces[i][2]));
                }
            }
        });
    }

    /* Slide navigation */
    var snOverlay = $('.sidenav-overlay'),
        body = $('body'),
        hideSlidenav = function() {
            $('#sidenav').removeClass('open');
            snOverlay.hide();
            body.removeClass('overlay');
        }
    $( "#btn-start-sidenav").click(function() {
        if ($('#sidenav').toggleClass('open').hasClass('open')) {
            snOverlay.show();
            body.addClass('overlay');
        } else
            snOverlay.hide();
            body.addClass('overlay');
        return false;
    } );
    snOverlay.click(hideSlidenav);
    $('.sidenav-header-close').click(hideSlidenav);

    /* VK */
    // if ($('#vk_groups').length > 0) {
        /* var width = $('section.vk').width(); */
		
  /*   } */
  
		var md = $('#modal-vk');
		//console.log(md);
		if(md.length>0){
			var width = 300;
			$('#vk_groups').css('width', width + 'px');
			gear.js('http://vk.com/js/api/openapi.js?126', function(){
				if (typeof VK != 'undefined') {
					/* VK.Widgets.Group("vk_groups", {redesign: 0, mode: 0, width: width, height: "500", color1: 'FFFFFF', color2: '000000', color3: '5E81A8'}, 23474857); */

					// modal
					
					md.modal(); 
					md.on('shown.bs.modal', function() {
						VK.Widgets.Group("modal-body-vk", {redesign: 1, mode: 0, width: "500", height: "auto", color1: 'FFFFFF', color2: '000000', color3: '5E81A8'}, 23474857);
					});  
				} else{			
					var md_banner = $('#modal-banner-image');
					md_banner.modal();
				}
			});
		}else{
			var md_banner = $('#modal-banner-image');
			if(md_banner.length>0){
				md_banner.modal();
			}
		}
		
		
    // Scroll to
    var scrollTop = 0;
    $(window).scroll(function(){
        scrollTop = $(this).scrollTop();
        if(scrollTop != 0)
            //$('.scroll-to').fadeIn();
			$('.scroll-to').addClass('scrolled');
        else
           //$('.scroll-to').fadeOut();
			$('.scroll-to').removeClass('scrolled');
    });
     $('.scroll-to a').click(function(){ $('body,html').animate({scrollTop:0},800); });

    // Search
    var inp = $('.navbar-search .input-search');
    $('#btn-search').click(function(){ inp.toggleClass('show'); });

    // Popovers
    /*$('a.popovers').popover({
        placement: 'top',
        trigger: 'focus'
    });*/
    var popover, oPopover;
    $('article a').each(function(o, i){
        item = $(this);
        href = item.attr('href');
        if (typeof href != 'undefined')
        if (href.indexOf('#popover') > -1) {
            item.attr('tabindex', 0);
            item.attr('href', null);
            item.attr('role', 'button');
            $(this).popover({
                placement: 'top',
                html: true,
                trigger: 'focus',
                title: item.text(),
                content: $(href).html()
            });
        }
    });
});