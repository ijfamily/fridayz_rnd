/*! lightgallery - v1 - 2016-09-27
* http://sachinchoolur.github.io/lightGallery/
* Copyright (c) 2016 Gear Magic */
(function($, window, document, undefined) {
    'use strict';

    var defaults = { social: false };

    function getButtons(title, href){
        var t = encodeURIComponent(title), u = encodeURIComponent(href);

        return '<a class="soc-v" title="&#1042; &#1050;&#1086;&#1085;&#1090;&#1072;&#1082;&#1090;&#1077;" href=http://vkontakte.ru/share.php?url=' + u + '></a>' + 
            '<a class="soc-f" title="Facebook" href=http://www.facebook.com/sharer.php?u=' + u +'></a>' + 
            '<a class="soc-t" title="Twitter" href=http://twitter.com/share?text=' + t + '&amp;url=' + u + '></a>' + 
            '<a class="soc-o" title="&#1054;&#1076;&#1085;&#1086;&#1082;&#1083;&#1072;&#1089;&#1089;&#1085;&#1080;&#1082;&#1080;" href=http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl=' + u + '></a>' + 
            '<a class="soc-l" title="LiveJournal" href=http://www.livejournal.com/update.bml?event=' + u + '&subject='+t+'></a>' + 
            '<a class="soc-g" title="&#1047;&#1072;&#1082;&#1083;&#1072;&#1076;&#1082;&#1080; Google" href=http://www.google.com/bookmarks/mark?op=add&bkmk=' + u + '&title=' + t + '></a>' + 
            '<a class="soc-y" title="&#1071;&#1085;&#1076;&#1077;&#1082;&#1089;.&#1047;&#1072;&#1082;&#1083;&#1072;&#1076;&#1082;&#1080;" href=http://zakladki.yandex.ru/userarea/links/addfromfav.asp?bAddLink_x=1&lurl=' + u + '&lname=' + t +'></a>';
    }

    var Social = function(element){
        this.core = $(element).data('lightGallery');
        this.init();

        return this;
    };

    Social.prototype.init = function() {
        var _this = this, ctSocial, slide, img, title;

        _this.core.$outer.find('.lg-toolbar').append('<div class="lg-social"></div>');
        ctSocial = _this.core.$outer.find('.lg-social');

        _this.core.$el.on('onAfterSlide.lg', function(e, prevIndex, index) {
            slide = _this.core.$slide[index];
            img = $(slide).find('img');
            if (typeof img.attr('title') == 'undefined')
                title = img.attr('title');
            else
                title = 'Fridayz';
            ctSocial.html(getButtons(title, img.attr('src')));
        });
    };

    Social.prototype.destroy = function(){};

    $.fn.lightGallery.modules.social = Social;
})(jQuery, window, document);
