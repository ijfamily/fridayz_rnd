/*
 * #Fridayz - первый медийный онлайн журнал
 *
 * Copyright(c) 2016, Fridayz
 *
 * Date: Jun 4 2016
 */

function showPopup(message){
    $.blockUI({
        onUnblock: function(){ $('body').css('overflow', 'auto'); },
        message: message,
         onOverlayClick: $.unblockUI,
         blockMsgClass: 'container-popup'
    });
}

function setStatVideo(id){
    $.ajax({
        url: 'http://vimeo.com/api/v2/video/' + id + '.json',
        cache: false,
        success: function(data, status, xhr){
            var vd, data = data[0];
            vd = $('#video-' + id);
            if (vd.length > 0) {
                vd.find('.video-footer-plays .video-footer-count').html(data.stats_number_of_plays);
                vd.find('.video-footer-likes .video-footer-count').html(data.stats_number_of_likes);
                vd.find('.video-footer-comments .video-footer-count').html(data.stats_number_of_comments);
            }
            vd.show();
        }
    });
}

$(document).ready(function($){

    if (gear.isMobile) $('body').addClass('mobile');

    /* Scroll to */
    var st = $('.icon-bar.icon-bar-up'), nav = $('nav');

    /* Highlight location */
    var href, sg = document.location.pathname.split('/');
    if (sg.length < 2)
        sg = '';
    else
        if (sg[1].length == 0)
            sg = '';
        else
            sg = sg[1];
    var isActiveMenu = false;
    if (sg.length > 0) {
        $('nav a').each(function(o, i){
            href = $(this).attr('href');
            if (href.indexOf(sg) > -1) {
                 $(this).parent().addClass('active');
                 isActiveMenu = true;
            }
        });
    }
    if (isActiveMenu)
        $('.navbar-menu').addClass('pale');

    /* Search */
    $('.navbar-search-close').click(function(){ $('.navbar-search').fadeOut(); });
    $('.button-search').click(function(){ $('.navbar-search').fadeIn(); });

    /* Afisha carousel */
     var ca = $('#carousel-afisha');
     if (ca.length > 0) {
        ca.slick({
          dots: true,
          slidesToShow: 5,
          slidesToScroll: 1,
          autoplay: true,
          autoplaySpeed: 2000,
          arrows: false,
          responsive: [
            { breakpoint: 1050, settings: { slidesToShow: 4, slidesToScroll: 1, infinite: true, dots: true } },
            { breakpoint: 600, settings: { slidesToShow: 2, slidesToScroll: 2 } },
            { breakpoint: 480, settings: { slidesToShow: 1, slidesToScroll: 1 } }
          ]
        });
        // feed afisha
        if ($('.s-afisha').length > 0)
            $().piroBox_ext({
                piro_speed : 700,
                bg_alpha : 0.5,
                piro_scroll : true
            });
        $('.s-afisha').show();
     }

    /* Albums carousel */
    if (gear.isMobile) {
         var ca = $('.s-albums.feed .s-albums-body .row');
         if (ca.length > 0) {
            ca.slick({
              centerMode: true,
              dots: true,
              slidesToShow: 1,
              slidesToScroll: 1,
              autoplay: true,
              autoplaySpeed: 2000,
              variableWidth: true,
              arrows: false,
              responsive: [
                { breakpoint: 1024, settings: { slidesToShow: 1, slidesToScroll: 1, dots: true } },
                { breakpoint: 600, settings: { slidesToShow: 1, slidesToScroll: 1, dots: true } },
                { breakpoint: 480, settings: { slidesToShow: 1, slidesToScroll: 1, dots: true } }
              ]
            });
         }
    }

    /* Album photos */
    var gallery = $('#album-photos');
    if (gallery.length > 0) gallery.lightGallery();
    gallery = $('.place-photos');
    if (gallery.length > 0) gallery.lightGallery();

    // Sidebar fixing
    $('.sidebar-right').stickyMojo({footerID: 'footer', contentID: 'article', offsetTop: 53 });

    /* Map */
    $('.s-map .places a').click(function(){
        var c = $(this).attr('data-coord').split(',');
        mapPlaces.panTo([parseFloat(c[0]), parseFloat(c[1]) ], {flying: 1});
        return false;
    });

    /* Rating */
    $('.barrating').barrating({
        theme: 'css-stars',
        deselectable: false,
        onSelect: function(value, text, event) {
            var self = this, ps = this.$elem.parents('.rating'), id = this.$elem.attr('id');
            this.readonly(true);
            $.ajax({
                url: '/request/rating/',
                type: 'POST',
                cache: false,
                dataType: 'json',
                data: { id: id, type: 'place', value: value },
                success: function(data, status, xhr){
                    if (!data.success)
                        ps.find('.rating-msg').html(data.html);
                }
            });
        }
    });

    /* Button SignIn */
    $('.button-user').click(function(){
        $('body').css('overflow', 'hidden');
        $.blockUI({
            onUnblock: function(){ $('body').css('overflow', 'auto'); },
            message: $('#s-form-sign'),
             onOverlayClick: $.unblockUI,
             blockMsgClass: 'container-popup'
        });

        return false;
    });

    /* Button toogle */
    $( ".navbar-toggle" ).click(function() {
        //
        if ($('#sidenav').toggleClass('open').hasClass('open')) {
            //alert('dd');
            $('.sidenav-overlay').show();
            $('body').css('overflow', 'hidden');
        } else
            $('.sidenav-overlay').hide();
            $('body').css('overflow', 'hidden');
        return false;
    } );
    $('.sidenav-header-close').click(function(){
        $('#sidenav').removeClass('open');
        $('.sidenav-overlay').hide();
        $('body').css('overflow', 'hidden');
    });

    /* Yandex Map */
    if ($('#map-places').length > 0) {
        gear.js('http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU', function(){
            ymaps.ready(init);
            var mapPlaces;
            function init(){ 
                mapPlaces = new ymaps.Map('map-places', { center: [48.5689472,39.3154929], behaviors: ['default', 'scrollZoom'], zoom: 16 });
                for (var i = 0; i < partnerPlaces.length; i++) {
                    mapPlaces.geoObjects.add(new ymaps.Placemark(partnerPlaces[i][0], partnerPlaces[i][1], partnerPlaces[i][2]));
                }
            }
        });
    }

    /* Video */
    var svideo = $('.s-video');
    if (svideo.length > 0) {
        $('.video-footer').each(function(){
            //if (typeof setStatVideo != 'undefined')
                setStatVideo($(this).attr('data-id'));
        });
        $('.s-video-play').click(function(){
            var mdl = $('#modal-video'), ftr = $('#video' + $(this).attr('data-code'));
            mdl.find('iframe').attr('src', 'https://player.vimeo.com/video/' + $(this).attr('data-code'));
            mdl.find('.modal-title').html($(this).attr('title'));
            mdl.find('.modal-body-footer').html( ftr.html() );
            mdl.modal();
            return false;
        });
        svideo.find('.s-video-more').click(function(){
            svideo.find('.s-video-i-slide').fadeIn();
            $(this).hide();
        });
    }

    if ($('.s-afisha').length > 0) {
        /*
        if (typeof VK != 'undefined') {
            var md = $('#modal-vk');
            md.modal();
            md.on('shown.bs.modal', function() {
                VK.Widgets.Group("modal-body-vk", {redesign: 1, mode: 0, width: "auto", height: "274", color1: 'FFFFFF', color2: '000000', color3: '5E81A8'}, 23474857);
            });
        }
        */
    }

    // Scroll to
    var nav = $('nav.navbar'), scrollTop = 0;
    $(window).scroll(function(){
        scrollTop = $(this).scrollTop();
        if (scrollTop >= 5) {
                nav.addClass('navbar-slideup');
        } else {
                nav.removeClass('navbar-slideup');
        }

        if(scrollTop != 0)
            $('.scroll-to').fadeIn();
        else
            $('.scroll-to').fadeOut();
    });
    nav.mouseover(function(){
        $(this).removeClass('navbar-slideup');
    });
     $('.scroll-to a').click(function(){ $('body,html').animate({scrollTop:0},800); });

});